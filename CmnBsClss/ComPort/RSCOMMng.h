#pragma once

#include <windows.h>

#define DefaultPortName COM1
#define DefaultReadMultiplier 2
#define DefaultWriteMultiplier 2
#define DefaultReadInterval 0
#define DefineReadConstant 0
#define DefineWriteConstant 0

// ������ ������ � ��������� �� RS-232 � ����������� ������
// ���������� ��������� ������-�������� ������

#define BaudRate_110  			CBR_110
#define BaudRate_300 			CBR_300
#define BaudRate_600  			CBR_600
#define BaudRate_1200 	        CBR_1200
#define BaudRate_2400           CBR_2400
#define BaudRate_4800           CBR_4800
#define BaudRate_9600           CBR_9600
#define BaudRate_14400          CBR_14400
#define BaudRate_19200          CBR_19200
#define BaudRate_38400          CBR_38400
#define BaudRate_56000          CBR_56000
#define BaudRate_57600          CBR_57600
#define BaudRate_115200 		CBR_115200
#define BaudRate_128000         CBR_128000
#define BaudRate_256000 	    CBR_256000

// ����� �������������� ��� � ������������ - ����������� �������
#define    Data5Bit             DATABITS_5
#define    Data6Bit             DATABITS_6
#define    Data7Bit             DATABITS_7
#define    Data8Bit             DATABITS_8

// ��������� ��������
#define    NonParity            0//PARITY_NONE
#define    OddParity            1//PARITY_ODD // ��������
#define    EvenParity           2//PARITY_EVEN
#define    MarkParity           3//PARITY_MARK
#define    SpaceParity         4//PARITY_SPACE

// ���������� �������� �����
#define    OneStopBit           0//STOPBITS_10 // ���� ���
#define    One5StopBit          STOPBITS_15 // ������� ����
#define    TwoStopBit           STOPBITS_20 // ��� ����

// ���������� �������
#define defFRtsControl  2

struct COM_PROP
{
  const CString sProtocol = L"IEC 61107-2011"; // ���� ��� �������� ����
  CString sDeviceAddress = L"1";

  CString sPortName;
  UINT nPortNumber = 0;
  int nPortType = 0; // ������������� mdl::tPortType
  DWORD nBaudRate = 300;
  WORD nParity = 1;
  WORD nStopBits = 0;
  WORD nByteSize = 7;// Data7Bit;
  BYTE nThreadCtrl = defFRtsControl;
  DWORD nReadIntervalTimeout = DefaultReadInterval; // ���� != 0, �� ��� ����� �� ���������� 1 ������� � �������������� ���������, ���� ����� ������, �� �������� ���������, ��� ����
  DWORD nReadTotalTimeoutMultiplier = DefaultReadMultiplier; // ��. ������
  DWORD nReadTotalTimeoutConstant = DefineReadConstant; // ��. ������
  /*
  http://www.delphikingdom.ru/asp/viewitem.asp?catalogid=399&mode=print
  ������ ��������� ������.
  ReadTotalTimeoutMultiplier = 2, ReadTotalTimeoutConstant = 1, ReadIntervalTimeout = 1,
  ����������� 250 ��������. ���� �������� ������ ���������� �� 250 * 2 + 1 = 501 ������������,
  �� ����� ������� ��� ���������. ���� �������� ������ �� ���������� �� 501 ������������, �� ��� ��� ����� ����� ���������.
  ��� ���� ����� ���������� �������, ����� ������� ���������� �� ��������� ����-���� ��������.
  ��������� ������� ����� ���� �������� ��������� ��������� ������. ���� ����� �������� ���� ���������������� �������� ������� �����
  1 ������������, �� �������� ������ ��� �� ����� ���������.
  ���� ����� ��������� � ������ - nWriteTotalTimeoutMultiplier � nWriteTotalTimeoutConstant.

  ///- ������� ������������� COM_PROP -///
  ��� ������ - ������� ��� ������, ��������, �����������.
  ���� ���� ReadIntervalTimeout � ReadTotalTimeoutMultiplier ����������� � MAXDWORD, � ReadTotalTimeoutConstant ������ ���� � ������ MAXDWORD,
  �� ���������� �������� ������ ����������� ��������� ��������:
  - ���� � ������ ���� �������, �� ������ ���������� ����������� � ������������ ������� �� ������;
  - ���� � ������ ��� ��������, �� �������� ������ ����� ������� ��������� ������ �������, ����� ���� ��� ���������� ����������;
  - ���� � ������� �������, �������� ����� ReadTotalTimeoutConstant, �� ����� ������� �� ������ �������, ������� ������ ���������� �� ����-����.
  */

  DWORD nWriteTotalTimeoutMultiplier = DefaultWriteMultiplier;
  DWORD nWriteTotalTimeoutConstant = DefineWriteConstant;

  int nMultyValue = 20; // ���������, ��� ����������� ����������� ��������� �� ������� ��� ���������� nReadIntervalTimeout (��. � ����)

  BYTE nIsEcho = 0;
};

extern COM_PROP gRSCOMProp;

class CRSCOMMng
{
public:
  CRSCOMMng();
  ~CRSCOMMng();

  bool Connect();
  bool Disconnect();

  bool SetPortProp();

private:
  HANDLE m_hndComPort;
  COMSTAT m_comState;

  void ResetRB();
  void ResetWB();
  int GetSetThreadCtrlParam(DCB* pDCB, int nFlag, bool bSet);

public:
  unsigned short Write(unsigned char *buffer, int nSize);

  DWORD GetDataInBuff();
  unsigned short Read(BYTE *buffer, int nSize);
};