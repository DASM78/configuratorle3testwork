#include "StdAfx.h"
#include "RSCOMMng.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// ������ � �������, � �� � ����������� ���������
#define NOSIMLAB

COM_PROP gRSCOMProp{};
DCB dcbMasterInitState;

CRSCOMMng::CRSCOMMng()
{

}

CRSCOMMng::~CRSCOMMng()
{

}

bool CRSCOMMng::Connect()
{
  CString sPort;
  sPort.Format(_T("\\\\.\\%s"), gRSCOMProp.sPortName);
  #ifdef NOSIMLAB
  m_hndComPort = CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL/*|FILE_FLAG_OVERLAPPED*/, 0);
  #else
  m_hndComPort = CreateFile(sPort, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0);
  #endif

  if (m_hndComPort == INVALID_HANDLE_VALUE)
    return false;

  #ifdef NOSIMLAB
  
  BOOL Status = SetupComm(m_hndComPort, 256, 0);

  if (!SetPortProp())
    return false;

  COMMTIMEOUTS commTimeOuts;
  Status = GetCommTimeouts(m_hndComPort, &commTimeOuts);

  // Timeouts by baudrate

  // http://citforum.ru/hardware/articles/comports/

  gRSCOMProp.nReadIntervalTimeout = (int)(((10 / (double)gRSCOMProp.nBaudRate) * gRSCOMProp.nMultyValue) * 1000);
  commTimeOuts.ReadIntervalTimeout = gRSCOMProp.nReadIntervalTimeout;
  commTimeOuts.ReadTotalTimeoutMultiplier = 0;
  commTimeOuts.ReadTotalTimeoutConstant = 10000;
  commTimeOuts.WriteTotalTimeoutMultiplier = 0;// 20;
  commTimeOuts.WriteTotalTimeoutConstant = 0;
  Status = SetCommTimeouts(m_hndComPort, &commTimeOuts);
  Status = PurgeComm(m_hndComPort, PURGE_TXCLEAR | PURGE_RXCLEAR);

  #else

  ASSERT(false);
  GetCommState(m_hndComPort, &dcbMasterInitState);
  DCB dcbMaster = dcbMasterInitState;
  dcbMaster.BaudRate = 57600;
  dcbMaster.Parity = NOPARITY;
  dcbMaster.ByteSize = 8;
  dcbMaster.StopBits = ONESTOPBIT;
  SetCommState(m_hndComPort, &dcbMaster);

  PurgeComm(m_hndComPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

  #endif

  if (!(m_hndComPort == INVALID_HANDLE_VALUE || m_hndComPort == nullptr))
  {
    ResetRB();
    ResetWB();
  }

  return true;
}

bool CRSCOMMng::Disconnect()
{
  if (CloseHandle(m_hndComPort))
  {
    m_hndComPort = 0;
    return true;
  }
  return false;
}

bool CRSCOMMng::SetPortProp()
{
  DCB dcb{};
  if (GetCommState(m_hndComPort, &dcb))
  {
    dcb.BaudRate = gRSCOMProp.nBaudRate;
    //dcb.fBinary = true;
    //dcb.fParity = false;
    dcb.ByteSize = (BYTE)gRSCOMProp.nByteSize;
    dcb.Parity = (BYTE)gRSCOMProp.nParity;
    dcb.StopBits = (BYTE)gRSCOMProp.nStopBits;
    GetSetThreadCtrlParam(&dcb, gRSCOMProp.nThreadCtrl, true);
    if (SetCommState(m_hndComPort, &dcb))
    {
      GetCommState(m_hndComPort, &dcb);
      return true;
    }
  }
  return false;
}

void CRSCOMMng::ResetRB()
{
  #ifdef NOSIMLAB
  //if (!m_bOpening)
  return;
  #endif
  PurgeComm(m_hndComPort, PURGE_RXCLEAR);
}

void CRSCOMMng::ResetWB(void)
{
  #ifdef NOSIMLAB
  //if (!m_bOpening)
  return;
  #endif
  PurgeComm(m_hndComPort, PURGE_TXCLEAR);
}

int CRSCOMMng::GetSetThreadCtrlParam(DCB* pDCB, int nFlag, bool bSet)
{
  if (bSet)
  {
    if (nFlag == 0) // set "Xon/Xoff"
    {
      pDCB->fOutxCtsFlow = 0;
      pDCB->fOutX = pDCB->fInX = 1;
    }
    else
      if (nFlag == 1) // set "��������."
      {
        pDCB->fOutxCtsFlow = 1;
        pDCB->fOutX = pDCB->fInX = 0;
      }
      else
        if (nFlag == 2) // set "���"
          pDCB->fOutxCtsFlow = pDCB->fOutX = pDCB->fInX = 0;
    return nFlag;
  }
  if (pDCB->fOutxCtsFlow == 0 && pDCB->fOutX == 1 && pDCB->fInX == 1)
    return 0;
  if (pDCB->fOutxCtsFlow == 1 && pDCB->fOutX == 0 && pDCB->fInX == 0)
    return 1;
  if (pDCB->fOutxCtsFlow == 0 && pDCB->fOutX == 0 && pDCB->fInX == 0)
    return 2;
  return 3;
}

unsigned short CRSCOMMng::Write(unsigned char *buffer, int nSize)
{
  DWORD temp = 0;
  ResetRB();
  ResetWB();

  if (nSize)
  {
    ClearCommError(m_hndComPort, &temp, &m_comState);
#ifdef NOSIMLAB    
    if (WriteFile(m_hndComPort, buffer, nSize, &temp, nullptr))
#else
    if (WriteData(m_hndComPort, buffer, nSize, &temp))
#endif
      return (unsigned short)temp;
  }
  return 0;
}

DWORD CRSCOMMng::GetDataInBuff()
{
  DWORD Errors;
  COMSTAT ComState;

  if (!ClearCommError(m_hndComPort, &Errors, &ComState))
    return 0;
  return ComState.cbInQue;
}

unsigned short CRSCOMMng::Read(BYTE *buffer, int nSize)
{
  DWORD temp = 0;

#ifdef NOSIMLAB  
  if (ReadFile(m_hndComPort, buffer, nSize, &temp, nullptr))
#else
  if (ReadData(m_hndComPort, buffer, nSize, &temp, 1000))
#endif
    return (unsigned short)temp;

  return 0;
}