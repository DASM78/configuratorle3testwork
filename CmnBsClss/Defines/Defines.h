#pragma once

// MACROSes

#define B2I(v) v ? 1 : 0 // bool to int
#define ASSERT_AND_RETURN(p) if (!p) { ASSERT(p); return; }

#define false_d(s) (false)
#define FALSE_d(s) (FALSE)
#define true_d(s) (true)

// Colors

#define COLOR_LIGHT_BLUE RGB(185, 205, 251)
#define COLOR_LIGHT_LIGHT_BLUE RGB(230, 240, 251)

#define COLOR_DISABLE_EDITABLE_CELL_TEXT RGB(0, 0, 0) // - black; RGB(160, 160, 160) // - gray
#define COLOR_EDITABLE_CELL_TEXT RGB(60, 180, 60) // -green
