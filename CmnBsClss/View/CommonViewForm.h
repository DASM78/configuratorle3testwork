#pragma once

#include "afxwin.h"
#include <vector>

//#ifdef PREP_DEF_APP1
typedef CFormView CCmnViewFrm;
//#else
//typedef CDialogEx CCmnViewFrm;
//#endif

class CCommonViewForm: public CCmnViewFrm
{
protected:
  DECLARE_DYNCREATE(CCommonViewForm)

  CCommonViewForm();
  CCommonViewForm(UINT IDD_FormView, CString sClassName);
  ~CCommonViewForm();

  DECLARE_MESSAGE_MAP()
  
  // For PostMessage/SendMessage

public:
  void SetGlobalParent(HWND m_hWnd) { m_hWndGlobParent = m_hWnd; }
private:
  HWND m_hWndGlobParent = nullptr;

  // ******************************************
  //  Current dialog (view) identifications
  // ******************************************

private:
  const UINT m_nID;
  const CString m_sClassName;

public:
  UINT GetID();

  // ******************************************
  //
  // ******************************************

public:
  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);

public:
  void SetInitialUpdateFlag(bool bSet);
  bool GetInitialUpdateFlag();
private:
  bool m_bInitialUpdate = false;

private:
  afx_msg void OnSize(UINT nType, int cx, int cy);
  struct ONSIZE_DATA
  {
    UINT nType = 0;
    int cx = 0;
    int cy = 0;
  }
  m_onSizeData;
public:
  void OnSize(void* pOnSizeData);
  virtual void OnSize_() {}

  // ��������� �������� ����������� �������, ���� ��� ������ ��� ��������� ��������������� �/��� ������������� ScrollBar
  void GetFactViewSizes(
    CRect& rctClient,
    CRect& rctWindow,
    int& nHideOverLeft,
    int& nHideOverRight,
    int& nHideOverTop,
    int& nHideOverBottom
    );

  // ******************************************
  // Ctrl position when parent view changes size
  // ******************************************

  /*
  ���������� ��������� �������� �� ������������ ���� (��� ���������� �������� ����) ������������ ��������� (��-����������� ��� ���������)
  ������� ������� ����, ������� ����� ����� ����������� ���������� ���������:
  0 - ���������� �� ����� (�� ���������) �������� ���� �������, �.�. ��������� ���������� top margin � ������������ �������;
  1 - �������������/��������� ������ ������, �.�. ��������� ����������� top and bottom margins;
  2 - ������������ ����/����� �� ���������, �.�. ��������� ���������� bottom margin � ������������ �������;  
  */
public:
  enum ECtrlsBehaviorWhenMoving_v // ��������� ��� �����������
  {
    movbehConstTopMarginAndVertSizes = 0,
    movbehConstTopAndBottomMargins = 1,
    movbehConstBottomMarginAndVertSizes = 2,
  };
  enum ECtrlsBehaviorWhenMoving_h // ��������� ��� �����������
  {
    movbehConstLeftMarginAndHorizSizes,
    movbehConstLeftAndRigthMargins,
    movbehConstRightMarginAndHorizSizes,
  };
  
  // !!!
  // FollowUp - ��� "��������� ��", � �� "��������� �� ������"
  
  void AddCtrlForPositioning(CWnd* pCtrl, ECtrlsBehaviorWhenMoving_v VMoving, ECtrlsBehaviorWhenMoving_h HMoving);
  
  // VMoving = CCommonViewForm::movbehConstTopAndBottomMargins, HMoving = CCommonViewForm::movbehConstLeftAndRigthMargins
  // ����������� ������ � ������ - margins ������ � ������ ������ ������ ���������� �����������
  void AddCtrlForPositioning_Stretchable(CWnd* pCtrl);
  
  // VMoving = CCommonViewForm::movbehConstBottomMarginAndVertSizes, HMoving = CCommonViewForm::movbehConstLeftMarginAndHorizSizes
  // ������������ � ���� - margin ������ ������ ������ ���������� ����������, ������ ctrl ���������
  void AddCtrlForPositioning_FollowUpBottom(CWnd* pCtrl);
  
  // VMoving = CCommonViewForm::movbehConstTopMarginAndVertSizes, HMoving = CCommonViewForm::movbehConstRightMarginAndHorizSizes
  // ������������ � ������ ������� - margin ������ ������ ������ ���������� ����������, ������ ctrl ���������
  void AddCtrlForPositioning_FollowUpRight(CWnd* pCtrl);
  
  // VMoving = CCommonViewForm::movbehConstBottomMarginAndVertSizes, HMoving = CCommonViewForm::movbehConstRightMarginAndHorizSizes
  // ������������ � ������ ������� � ���� - margins ������ ������ � ���� ������ ���������� �����������, ������ � ������ ctrl ���������
  void AddCtrlForPositioning_FollowUpRightAndBottom(CWnd* pCtrl);

private:
  struct CTRL_POS
  {
    CWnd* pCtrl = nullptr;
    ECtrlsBehaviorWhenMoving_v VMoving = movbehConstTopMarginAndVertSizes;
    ECtrlsBehaviorWhenMoving_h HMoving = movbehConstLeftMarginAndHorizSizes;

    // Begin sizes

    int nBeginLeftMargin = 0;
    int nBeginTopMargin = 0;
    int nBeginRightMargin = 0;
    int nBeginBottomMargin = 0;
    int nMin_cy = 0;
    int nMin_cx = 0;
  };

  std::vector<CTRL_POS> m_positioningCtrs;
  // ����������� ��������� Ctrls
  void CtrlPos_Move();

  // ������� ������ ��� ������������ ���������� �������� �� ������ ������� �

public:
  void SpreadCtrlToRightSide(CWnd* pWin, int nMinWidth);
  void SpreadCtrlToRightSide(CWnd* pWin, int nMinWidth, int nAddRightMargin);
    
    // ... �� ����

  void SpreadCtrlToBottomSide(CWnd* pWin, int nMinHeight);
  void SpreadCtrlToBottomSide(CWnd* pWin, int nMinHeight, int nAddBottomMargin);

  // ******************************************
  //
  // ******************************************

protected:
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

public:

  COLORREF GetTaskCaptionColor();

  virtual void BeforeClose();

  // ******************************************
  // CMFCTabCtrl and problem with message AFX_WM_CHANGE_ACTIVE_TAB (message signals twice in itself CView (in the inlay) and in CMainFrame
  // ******************************************

public:
  bool If_AFX_WM_CHANGE_ACTIVE_TAB_Was();
  void Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bool bSet);
private:
  bool m_bAFX_WM_CHANGE_ACTIVE_TAB = false;
};

