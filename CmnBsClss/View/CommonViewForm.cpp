#include "stdafx.h"
#include <typeinfo>
#include "CommonViewForm.h"

using namespace std;

#define UM_RESIZE_MSG_FROM_CVIEW        930

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////

#define TASK_CAPTION_COLOR RGB(0xC0, 0xD9, 0xFB)

UINT gnIDDTmp = 0;

////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(CCommonViewForm, CCmnViewFrm)

BEGIN_MESSAGE_MAP(CCommonViewForm, CCmnViewFrm)
  ON_WM_WINDOWPOSCHANGING()
  ON_WM_SIZE()
END_MESSAGE_MAP()

CCommonViewForm::CCommonViewForm()
  : CCmnViewFrm(gnIDDTmp)
  , m_nID(0)
{
}

CCommonViewForm::CCommonViewForm(UINT IDD_FormView, CString sClassName)
    : CCmnViewFrm(IDD_FormView)
    , m_nID(IDD_FormView)
    , m_sClassName(sClassName)
{
}

CCommonViewForm::~CCommonViewForm()
{
}

BOOL CCommonViewForm::PreCreateWindow(CREATESTRUCT& cs)
{
  return CCmnViewFrm::PreCreateWindow(cs);
}

void CCommonViewForm::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos)
{
  CCmnViewFrm::OnWindowPosChanging(lpwndpos);
}

void CCommonViewForm::SetInitialUpdateFlag(bool bSet)
{
  m_bInitialUpdate = bSet;
}

bool CCommonViewForm::GetInitialUpdateFlag()
{
  return m_bInitialUpdate;
}

void CCommonViewForm::OnSize(UINT nType, int cx, int cy)
{
  m_onSizeData.nType = nType;
  m_onSizeData.cx = cx;
  m_onSizeData.cy = cy;

  if (m_hWndGlobParent)
    ::PostMessage(m_hWndGlobParent, UM_RESIZE_MSG_FROM_CVIEW, 0, (LPARAM)&m_onSizeData);
  else
    OnSize(&m_onSizeData);
}

void CCommonViewForm::OnSize(void* pOnSizeData)
{
  ONSIZE_DATA* pOnSizeData_ = (ONSIZE_DATA*)pOnSizeData;
  if (pOnSizeData_)
    CCmnViewFrm::OnSize(pOnSizeData_->nType, pOnSizeData_->cx, pOnSizeData_->cy);
 
  CtrlPos_Move();
  OnSize_();
}


// ��������� �������� ����������� �������, ���� ��� ������ ��� ��������� ��������������� �/��� ������������� ScrollBar
void CCommonViewForm::GetFactViewSizes(
  CRect& rctFactClient,
  CRect& rctFactWindow,
  int& nHideOverLeft,
  int& nHideOverRight,
  int& nHideOverTop,
  int& nHideOverBottom
  )
{
  CSize sizeVisible; // ������ ������� ������� ���������� �����
  CSize sizeSb;

#ifdef PREP_DEF_APP1
    GetTrueClientSize(sizeVisible, sizeSb);
#else
  CRect rctTmp{};
  GetClientRect(&rctTmp);
  sizeVisible.cx = sizeSb.cx = rctTmp.Width();
  sizeVisible.cy = sizeSb.cy = rctTmp.Height();
#endif

  int nMapMode = 0;
  SIZE sizeTotal{}; // ����������� (�������) ������ ���������� ����� (������� ��� ��������� ��� ��������� ������� � �������� Visual Studio)

  BOOL bHasHorzBar = FALSE;
  BOOL bHasVertBar = FALSE;

#ifdef PREP_DEF_APP1
  SIZE sizePage{};
  SIZE sizeLine{};
  GetDeviceScrollSizes(nMapMode, sizeTotal, sizePage, sizeLine);
  ASSERT(nMapMode == 1);
  CPoint pntPos = GetDeviceScrollPosition();
  CheckScrollBars(bHasHorzBar, bHasVertBar);
#else
  CPoint pntPos = GetScrollPos(0);
#endif

  if (!bHasHorzBar && !bHasVertBar)
  {
    if (!sizeVisible.cx && !sizeVisible.cy)
      sizeVisible = sizeTotal;
    rctFactWindow = rctFactClient = CRect(0, 0, sizeVisible.cx, sizeVisible.cy);
    ClientToScreen(&rctFactWindow);
    return;
  }

  int nVisibleClientWidth = sizeVisible.cx;
  if (bHasVertBar)
    nVisibleClientWidth -= sizeSb.cy;

  int nVisibleClientHeight = sizeVisible.cy;
  if (bHasHorzBar)
  {
    nVisibleClientHeight -= sizeSb.cx;
    nHideOverLeft = pntPos.x;
    nHideOverRight = sizeTotal.cx - (nVisibleClientWidth + nHideOverLeft);
  }

  if (bHasVertBar)
  {
    nHideOverTop = pntPos.y;
    nHideOverBottom = sizeTotal.cy - (nVisibleClientHeight + nHideOverTop);
  }

  rctFactClient = CRect(
                        0 - nHideOverLeft,
                        0 - nHideOverTop,
                        (0 - nHideOverLeft) + nHideOverLeft + nVisibleClientWidth + nHideOverRight,
                        (0 - nHideOverTop) + nHideOverTop + nVisibleClientHeight + nHideOverBottom);

  CRect rctWindow{};
  GetWindowRect(&rctWindow);
  rctFactWindow = CRect(
                        rctWindow.left - nHideOverLeft,
                        rctWindow.top - nHideOverTop,
                        (rctWindow.left - nHideOverLeft) + nHideOverLeft + nVisibleClientWidth + nHideOverRight,
                        (rctWindow.top - nHideOverTop) + nHideOverTop + nVisibleClientHeight + nHideOverBottom);

  int nFc = rctFactClient.Width();
  int nFw = rctFactWindow.Width();
  ASSERT(nFc == nFw);
  ASSERT(rctFactClient.Height() == rctFactWindow.Height());
}

void CCommonViewForm::AddCtrlForPositioning(CWnd* pCtrl, ECtrlsBehaviorWhenMoving_v VMoving, ECtrlsBehaviorWhenMoving_h HMoving)
{
  ASSERT(pCtrl != nullptr);

  CTRL_POS cp;
  cp.pCtrl = pCtrl;
  cp.VMoving = VMoving;
  cp.HMoving = HMoving;

  CRect rctFactClient{};
  CRect rctFactWindow{};
  int nHideOverLeft = 0;
  int nHideOverRight = 0;
  int nHideOverTop = 0;
  int nHideOverBottom = 0;
  GetFactViewSizes(rctFactClient, rctFactWindow, nHideOverLeft, nHideOverRight, nHideOverTop, nHideOverBottom);

  CRect rctCtrl{};
  cp.pCtrl->GetWindowRect(&rctCtrl);

  cp.nBeginLeftMargin = rctCtrl.left - rctFactWindow.left;
  cp.nBeginTopMargin = rctCtrl.top - rctFactWindow.top;
  cp.nBeginRightMargin = rctFactWindow.right - rctCtrl.right;
  cp.nBeginBottomMargin = rctFactWindow.bottom - rctCtrl.bottom;

  cp.nMin_cy = rctCtrl.Height();
  cp.nMin_cx = rctCtrl.Width();

  m_positioningCtrs.push_back(cp);
}

void CCommonViewForm::AddCtrlForPositioning_Stretchable(CWnd* pCtrl)
{
  AddCtrlForPositioning(pCtrl, movbehConstTopAndBottomMargins, movbehConstLeftAndRigthMargins);
}

void CCommonViewForm::AddCtrlForPositioning_FollowUpBottom(CWnd* pCtrl)
{
  AddCtrlForPositioning(pCtrl, movbehConstBottomMarginAndVertSizes, movbehConstLeftMarginAndHorizSizes);
}

void CCommonViewForm::AddCtrlForPositioning_FollowUpRight(CWnd* pCtrl)
{
  AddCtrlForPositioning(pCtrl, movbehConstTopMarginAndVertSizes, movbehConstRightMarginAndHorizSizes);
}

void CCommonViewForm::AddCtrlForPositioning_FollowUpRightAndBottom(CWnd* pCtrl)
{
  AddCtrlForPositioning(pCtrl, movbehConstBottomMarginAndVertSizes, movbehConstRightMarginAndHorizSizes);
}

void CCommonViewForm::CtrlPos_Move()
{
  for (size_t i = 0; i < m_positioningCtrs.size(); ++i)
  {
    CTRL_POS* pc = &m_positioningCtrs[i];
    CRect rctFactClient{};
    CRect rctFactWindow{};
    int nHideOverLeft = 0;
    int nHideOverRight = 0;
    int nHideOverTop = 0;
    int nHideOverBottom = 0;
    GetFactViewSizes(rctFactClient, rctFactWindow, nHideOverLeft, nHideOverRight, nHideOverTop, nHideOverBottom);
    
    //

    CRect rctCtrlCurrWindow{};
    pc->pCtrl->GetWindowRect(&rctCtrlCurrWindow);

    switch (pc->VMoving)
    {
      case movbehConstTopMarginAndVertSizes:
        break;

      case movbehConstTopAndBottomMargins: // ���������/����� ��� �� pc->nBeginBottomMargin
      {
        CRect rctCtrlNewWindow(
          rctCtrlCurrWindow.left,
          rctFactWindow.top + pc->nBeginTopMargin,
          rctCtrlCurrWindow.left + rctCtrlCurrWindow.Width(),
          rctFactWindow.bottom - pc->nBeginBottomMargin
          );

        if (rctCtrlNewWindow.Height() < pc->nMin_cy) // ������� ����� ������ ���������� ��������
          rctCtrlNewWindow.bottom = rctCtrlNewWindow.top + pc->nMin_cy; // ��������� �������������� ������ �� cy

        ScreenToClient(&rctCtrlNewWindow);
        pc->pCtrl->MoveWindow(&rctCtrlNewWindow);
      }
      break;

      case movbehConstBottomMarginAndVertSizes: // �������� ������� �� pc->nBeginBottomMargin
        {
          CRect rctCtrlNewWindow(
            rctCtrlCurrWindow.left,
            rctFactWindow.bottom - pc->nBeginBottomMargin - pc->nMin_cy,
            rctCtrlCurrWindow.right,
            rctFactWindow.bottom - pc->nBeginBottomMargin
            );

          if (rctFactWindow.top + pc->nBeginTopMargin > rctCtrlNewWindow.top) // ���� ������� ����� � ctrl ��������� ������� �� ������� ������
          {
            rctCtrlNewWindow.top = rctFactWindow.top + pc->nBeginTopMargin;
            rctCtrlNewWindow.bottom = rctCtrlNewWindow.top + pc->nMin_cy;
          }

          ScreenToClient(&rctCtrlNewWindow);
          pc->pCtrl->MoveWindow(&rctCtrlNewWindow);
        }
        break;
    }


    pc->pCtrl->GetWindowRect(&rctCtrlCurrWindow);

    switch (pc->HMoving)
    {
      case movbehConstLeftMarginAndHorizSizes:
        break;

      // ���������/����� ������ ������� �� pc->nBeginRightMargin
      case movbehConstLeftAndRigthMargins:
      {
        CRect rctCtrlNewWindow(
          rctFactWindow.left + pc->nBeginLeftMargin,
          rctCtrlCurrWindow.top,
          rctFactWindow.right - pc->nBeginRightMargin,
          rctCtrlCurrWindow.top + rctCtrlCurrWindow.Height()
          );

        if (rctCtrlNewWindow.Width() < pc->nMin_cx) // ������� ����� ������ ���������� ��������
          rctCtrlNewWindow.right = rctCtrlNewWindow.left + pc->nMin_cx; // ��������� �������������� ������ �� cx

        ScreenToClient(&rctCtrlNewWindow);
        pc->pCtrl->MoveWindow(&rctCtrlNewWindow);
      }
      break;

      // �������� ������� �� pc->nBeginRightMargin
      case movbehConstRightMarginAndHorizSizes:
      {
        CRect rctCtrlNewWindow(
          rctFactWindow.right - pc->nBeginRightMargin - pc->nMin_cx,
          rctCtrlCurrWindow.top,
          rctFactWindow.right - pc->nBeginRightMargin,
          rctCtrlCurrWindow.bottom
          );

        if (rctFactWindow.left + pc->nBeginLeftMargin > rctCtrlNewWindow.left) // ���� ������� ����� � ctrl ��������� ������� �� ������� ������
        {
          rctCtrlNewWindow.left = rctFactWindow.left + pc->nBeginLeftMargin;
          rctCtrlNewWindow.right = rctCtrlNewWindow.left + pc->nMin_cx;
        }

        ScreenToClient(&rctCtrlNewWindow);
        pc->pCtrl->MoveWindow(&rctCtrlNewWindow);
      }
      break;
    }
  }
}

#ifdef _DEBUG
void CCommonViewForm::AssertValid() const
{
  CCmnViewFrm::AssertValid();
}

void CCommonViewForm::Dump(CDumpContext& dc) const
{
  CCmnViewFrm::Dump(dc);
}
#endif //_DEBUG

UINT CCommonViewForm::GetID()
{
  return m_nID;
}

COLORREF CCommonViewForm::GetTaskCaptionColor()
{
  return TASK_CAPTION_COLOR;
}

void CCommonViewForm::SpreadCtrlToRightSide(CWnd* pWin, int nMinWidth)
{
  SpreadCtrlToRightSide(pWin, nMinWidth, 0);
}

void CCommonViewForm::SpreadCtrlToRightSide(CWnd* pWin, int nMinWidth, int nAddRightMargin)
{
  CSize size{};
#ifdef PREP_DEF_APP1
  size = GetTotalSize();
#else
#endif

  CRect rectCtrlWin{};
  pWin->GetWindowRect(rectCtrlWin);
  int nCtrlWitdh = rectCtrlWin.Width();

  CRect rectParentClient{};
  GetClientRect(rectParentClient);
  int nParentWitdh = max(size.cx, rectParentClient.Width());

  CRect rectParentWin{};
  GetWindowRect(rectParentWin);
  int nLeftMargin = rectCtrlWin.left - rectParentWin.left;
  int nRightMargin = 10 + nAddRightMargin;
  int nTopMargin = rectCtrlWin.top - rectParentWin.top;

  int nConst = 2;
  int nCtrlWithNeed = nParentWitdh - ((nLeftMargin + nRightMargin) - (nConst * 2)); // * 2 - for left and right sides

  if (nCtrlWitdh != nCtrlWithNeed)
  {
    rectCtrlWin.bottom -= rectCtrlWin.top;
    rectCtrlWin.top = 0;
    rectCtrlWin.right -= rectCtrlWin.left;
    rectCtrlWin.left = 0;

    rectCtrlWin.left = nLeftMargin - nConst;
    rectCtrlWin.top += nTopMargin - nConst;
    rectCtrlWin.bottom += nTopMargin - nConst;

    if (nCtrlWithNeed < nMinWidth)
      rectCtrlWin.right = rectCtrlWin.left + nMinWidth;
    else
      rectCtrlWin.right = rectCtrlWin.left + nCtrlWithNeed;
    pWin->MoveWindow(rectCtrlWin);
  }
}

void CCommonViewForm::SpreadCtrlToBottomSide(CWnd* pWin, int nMinHeight)
{
  SpreadCtrlToBottomSide(pWin, nMinHeight, 0);
}

void CCommonViewForm::SpreadCtrlToBottomSide(CWnd* pWin, int nMinHeight, int nAddBottomMargin)
{
  CSize size{};
#ifdef PREP_DEF_APP1
  size = GetTotalSize();
#else
#endif

  CRect rectCtrlWin{};
  pWin->GetWindowRect(rectCtrlWin);
  int nCtrlHeight = rectCtrlWin.Height();

  CRect rectParentClient{};
  GetClientRect(rectParentClient);
  int nParentHeight = max(size.cy, rectParentClient.Height());

  CRect rectParentWin{};
  GetWindowRect(rectParentWin);
  int nTopMargin = rectCtrlWin.top - rectParentWin.top;
  int nBottomMargin = 10 + nAddBottomMargin;
  int nLeftMargin = rectCtrlWin.left - rectParentWin.left;

  int nConst = 2;
  int nCtrlHeightNeed = nParentHeight - ((nTopMargin + nBottomMargin) - (nConst * 2)); // * 2 - for top and bottom sides

  if (nCtrlHeight != nCtrlHeightNeed)
  {
    rectCtrlWin.bottom -= rectCtrlWin.top;
    rectCtrlWin.top = 0;
    rectCtrlWin.right -= rectCtrlWin.left;
    rectCtrlWin.left = 0;

    rectCtrlWin.top = nTopMargin - nConst;
    rectCtrlWin.left += nLeftMargin - nConst;
    rectCtrlWin.right += nLeftMargin - nConst;

    if (nCtrlHeightNeed < nMinHeight)
      rectCtrlWin.bottom = rectCtrlWin.top + nMinHeight;
    else
      rectCtrlWin.bottom = rectCtrlWin.top + nCtrlHeightNeed;
    pWin->MoveWindow(rectCtrlWin);
  }
}

void CCommonViewForm::BeforeClose()
{
}

bool CCommonViewForm::If_AFX_WM_CHANGE_ACTIVE_TAB_Was()
{
  return m_bAFX_WM_CHANGE_ACTIVE_TAB;
}

void CCommonViewForm::Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bool bSet)
{
  m_bAFX_WM_CHANGE_ACTIVE_TAB = bSet;
}