#pragma once

#include "ExportDef.h"

class PRIVATELIBS_API CVersionInterface
{
public:

  // ����������: -1 ���� sVersion1 ����� ������ ��� sVersion2,
  // 1 ���� sVersion1 ����� ������� ��� sVersion2,
  // 0 ���� ������ ���������
  static int CompareVersions(CString sVersion1, CString sVersion2);

  // ��������, ��� sVersion= 1.2.3.4 
  // GetNumVersionByIndex(0, sVersion)==1
  // GetNumVersionByIndex(3, sVersion)==4
  static int GetNumVersionByIndex(int nIndex, CString sVersion);
};
