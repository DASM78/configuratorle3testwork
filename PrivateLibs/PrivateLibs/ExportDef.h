#pragma once

#ifndef __AFXWIN_H__
#error "include 'stdafx.h' before including this file for PCH"
#endif

#ifdef PRIVATELIBS_EXPORTS
#define PRIVATELIBS_API __declspec(dllexport)
#else
#define PRIVATELIBS_API __declspec(dllimport)
#endif
