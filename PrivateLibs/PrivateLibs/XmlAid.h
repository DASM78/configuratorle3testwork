#pragma once

#include <msxml2.h>
#include <list>
#include <map>
#include "ExportDef.h"

class PRIVATELIBS_API CParamsTreeNode
{
public:
  CParamsTreeNode(LPCTSTR pName, LPCTSTR pValue = nullptr);
  ~CParamsTreeNode();

  CString GetName() const
  {
    return m_sName;
  };
  void SetName(CString sName)
  {
    m_sName = sName;
  }
  CString GetValue() const
  {
    return m_sValue;
  }
  void SetValue(CString sValue)
  {
    m_sValue = sValue;
  }
  //  CParamsTreeNode * GetParent() const { return m_pParent; }
  std::list<CParamsTreeNode *> & GetListChildNodes()
  {
    return m_listChildNodes;
  }
  std::map<CString, CParamsTreeNode *> & GetAttributes()
  {
    return m_mapAttributes;
  }

  // return pointer to new CParamsTreeNode
  CParamsTreeNode * AddNode(LPCTSTR pName, LPCTSTR pValue);
  CParamsTreeNode * AddNode(LPCTSTR pName)
  {
    return AddNode(pName, _T(""));
  }

  bool AddAttribute(LPCTSTR pName, LPCTSTR pValue); // return false if attribute pName already exist

  // Return count of deleted nodes. Normally 1 or 0.
  int DeleteNodes(LPCTSTR pName, LPCTSTR pValue);

  // Return count of deleted nodes.
  int DeleteNodes(LPCTSTR pName);

  // Return false if such attribute not exist
  bool DeleteAttribute(LPCTSTR pName);

  void DeleteAllNodes();
  void DeleteAllAttributes();

  // Return nullptr if no such node
  CParamsTreeNode * FindFirstNode(LPCTSTR pName);

  void FindNodes(LPCTSTR pName, std::list<CParamsTreeNode*> & lst);

  // Return nullptr if no such node
  CParamsTreeNode * FindFirstNode(LPCTSTR pName, LPCTSTR pValue);

  // Return nullptr if no such attribute
  CParamsTreeNode * FindAttribute(LPCTSTR pName);

  // return empty string if no such attribute
  CString GetAttributeValue(LPCTSTR pName);

  // Return nullptr if no such node
  CParamsTreeNode * FindFirstNodeByNodeSequence(const CStringArray & arrNodeNames);

protected:
  CString m_sName;
  CString m_sValue;
  //  CParamsTreeNode * m_pParent;

  std::list<CParamsTreeNode *> m_listChildNodes;
  std::map<CString, CParamsTreeNode *> m_mapAttributes;

};

class PRIVATELIBS_API CXmlAid
{
public:
  // return nullptr if can't parse xml
  // bFirstSymbolIsSpec==true means that 1-st symbol in pBuf is special XML Unicode simbol 0xFEFF
  static CParamsTreeNode * CreateTreeFromXmlBuffer(LPCWSTR pBuf, bool bFirstSymbolIsSpec = true);

  // Return nullptr if can't parse xml or can't read file
  // NOTE! File in UTF16
  static CParamsTreeNode * CreateTreeFromXmlFile(LPCTSTR pFileName);

  // return nullptr if can't create xml
  static LPWSTR CreateXmlBufferFromTree(CParamsTreeNode * pTreeNode, int & nLength);

  // return false if can't create xml
  static bool CreateXmlFileFromTree(LPCTSTR pFileName, CParamsTreeNode * pTreeNode);

protected:

  // return false if error occurs
  static bool EnumReadNode(CParamsTreeNode * pTreeNode, IXMLDOMNode* pXmlNode);

  // return false if error occurs
  static bool EnumWriteXmlElem(IXMLDOMDocument * pDoc, IXMLDOMElement* pXmlElem, CParamsTreeNode * pTreeNode);

  static CString GetNodeValue(IXMLDOMNode* pXmlNode);
  static void SetXmlElemValue(IXMLDOMElement* pXmlElem, LPCTSTR pValue, bool bHasChild);

  // Fill pTreeNode attributes from pXmlNode attributes
  static void FillTreeNodeAttributes(CParamsTreeNode * pTreeNode, IXMLDOMNode* pXmlNode);

  // Fill pXmlElem attributes from pTreeNode attributes
  static void FillXmlElemAttributes(IXMLDOMElement* pXmlElem, CParamsTreeNode * pTreeNode);

  // return false if can't create
  static bool CreateIXmlDomDocFromTreeFile(CComPtr<IXMLDOMDocument2> & spDoc, CParamsTreeNode * pTreeNode);

  static void AppendNewline(IXMLDOMDocument * pDoc, IXMLDOMNode * pXmlNode);
};