#pragma once

#include "ExportDef.h"

const UINT WM_DATETIME_EDIT = ::RegisterWindowMessage(L"WM_DATETIME_EDIT");
#define DTE_DATETIMECHANGED 0x0001

class PRIVATELIBS_API CDateTimeEdit: public CEdit
{
  // Construction
public:
  CDateTimeEdit();

  DECLARE_DYNCREATE(CDateTimeEdit)

  DECLARE_MESSAGE_MAP()
  
  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CDateTimeEdit)
protected:
  virtual void PreSubclassWindow();
  //}}AFX_VIRTUAL

  // Implementation
public:
  CString GetMask() const
  {
    return m_strMask;
  }
  void SetMask(CString mask, bool bCtrlVisible = true);
  COleDateTime GetDateTime();
  COleDateTime GetDateTime(CString str);
  void SetDateTime(COleDateTime dt);

  virtual ~CDateTimeEdit();

  // Generated message map functions
protected:
  afx_msg void OnContextMenu(CWnd*, CPoint point);

  CString m_strMask;
  //{{AFX_MSG(CDateTimeEdit)
  afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnKillFocus(CWnd* pNewWnd);
  //}}AFX_MSG

public:
  virtual bool OnKeyUp_Enter();
  virtual bool OnKeyUp_Esc();
  virtual void SaveEditableData();
};


