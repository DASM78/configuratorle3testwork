#include "stdafx.h"
#include <comdef.h>
#include "XmlAid.h"

//#import <msxml2.dll>  named_guids

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TYPE_VALUE  L"value"
#define TYPE_VALUE_T  _T("value")

CParamsTreeNode::CParamsTreeNode(LPCTSTR pName, LPCTSTR pValue /*=nullptr*/)
  : m_sName(pName), m_sValue(pValue ? pValue : _T(""))//, m_pParent(nullptr)
{
}

CParamsTreeNode::~CParamsTreeNode()
{
  list<CParamsTreeNode *>::iterator iter;
  map<CString, CParamsTreeNode *>::iterator iterAttr;
  for (iter = m_listChildNodes.begin(); iter != m_listChildNodes.end(); ++iter)
    delete *iter;
  for (iterAttr = m_mapAttributes.begin(); iterAttr != m_mapAttributes.end(); ++iterAttr)
    delete iterAttr->second;
}

CParamsTreeNode * CParamsTreeNode::AddNode(LPCTSTR pName, LPCTSTR pValue)
// return pointer to new CParamsTreeNode
{
  CParamsTreeNode * pNode = new CParamsTreeNode(pName, pValue);
  m_listChildNodes.push_back(pNode);
  return pNode;
}

bool CParamsTreeNode::AddAttribute(LPCTSTR pName, LPCTSTR pValue)
// return false if attribute pName already exist
{
  if (m_mapAttributes.find(pName) != m_mapAttributes.end())
    return false;
  CParamsTreeNode * pNode = new CParamsTreeNode(pName, pValue);
  m_mapAttributes[pName] = pNode;
  return true;
}

int CParamsTreeNode::DeleteNodes(LPCTSTR pName, LPCTSTR pValue)
// Return count of deleted nodes. Normally 1 or 0.
{
  int nRet = 0;
  list<CParamsTreeNode *>::iterator iter = m_listChildNodes.begin();
  while (iter != m_listChildNodes.end())
  {
    if ((*iter)->GetName() == pName && (*iter)->GetValue() == pValue)
    {
      delete *iter;
      iter = m_listChildNodes.erase(iter);
      ++nRet;
    }
    else
      ++iter;
  }
  return nRet;
}

// Return count of deleted nodes.
int CParamsTreeNode::DeleteNodes(LPCTSTR pName)
{
  int nRet = 0;
  list<CParamsTreeNode *>::iterator iter = m_listChildNodes.begin();
  while (iter != m_listChildNodes.end())
  {
    if ((*iter)->GetName() == pName)
    {
      delete *iter;
      iter = m_listChildNodes.erase(iter);
      ++nRet;
    }
    else
      ++iter;
  }
  return nRet;
}

// Return false if such attribute not exist
bool CParamsTreeNode::DeleteAttribute(LPCTSTR pName)
{
  map<CString, CParamsTreeNode *>::iterator iter = m_mapAttributes.find(pName);
  if (iter != m_mapAttributes.end())
  {
    delete iter->second;
    m_mapAttributes.erase(iter);
    return true;
  }
  return false;
}

// Return nullptr if no such node
CParamsTreeNode * CParamsTreeNode::FindFirstNode(LPCTSTR pName)
{
  list<CParamsTreeNode *>::iterator iter;
  for (iter = m_listChildNodes.begin(); iter != m_listChildNodes.end(); ++iter)
  {
    if ((*iter)->GetName() == pName)
      return *iter;
  }
  return nullptr;
}

void CParamsTreeNode::FindNodes(LPCTSTR pName, list<CParamsTreeNode*> & lst)
{
  lst.clear();
  list<CParamsTreeNode *>::iterator iter;
  for (iter = m_listChildNodes.begin(); iter != m_listChildNodes.end(); ++iter)
  {
    if ((*iter)->GetName() == pName)
      lst.push_back(*iter);
  }
}

// Return nullptr if no such node
CParamsTreeNode * CParamsTreeNode::FindFirstNode(LPCTSTR pName, LPCTSTR pValue)
{
  list<CParamsTreeNode *>::iterator iter = m_listChildNodes.begin();
  while (iter != m_listChildNodes.end())
  {
    if ((*iter)->GetName() == pName && (*iter)->GetValue() == pValue)
      return *iter;
    ++iter;
  }
  return nullptr;
}

// Return nullptr if no such attribute
CParamsTreeNode * CParamsTreeNode::FindAttribute(LPCTSTR pName)
{
  map<CString, CParamsTreeNode *>::iterator iter = m_mapAttributes.find(pName);
  if (iter != m_mapAttributes.end())
    return iter->second;
  return nullptr;
}

// return empty string if no such attribute
CString CParamsTreeNode::GetAttributeValue(LPCTSTR pName)
{
  CString str;
  CParamsTreeNode * pNode = FindAttribute(pName);
  if (pNode)
    str = pNode->GetValue();
  return str;
}

// Return nullptr if no such node
CParamsTreeNode * CParamsTreeNode::FindFirstNodeByNodeSequence(const CStringArray & arrNodeNames)
{
  int nNodeNamesCount = arrNodeNames.GetCount();
  if (nNodeNamesCount)
  {
    list<CParamsTreeNode*> lstNodes;
    FindNodes(arrNodeNames[0], lstNodes);
    if (lstNodes.size())
    {
      if (nNodeNamesCount == 1)
      {
        return *lstNodes.begin();
      }
      else
      {
        CStringArray arr;
        arr.Append(arrNodeNames);
        arr.RemoveAt(0);
        list<CParamsTreeNode*>::iterator iter;
        for (iter = lstNodes.begin(); iter != lstNodes.end(); ++iter)
        {
          CParamsTreeNode * pNode = (*iter)->FindFirstNodeByNodeSequence(arr);
          if (pNode)
            return pNode;
        }
      }
    }
  }
  return nullptr;
}

void CParamsTreeNode::DeleteAllNodes()
{
  list<CParamsTreeNode *>::iterator iter;
  for (iter = m_listChildNodes.begin(); iter != m_listChildNodes.end(); ++iter)
    delete *iter;
  m_listChildNodes.clear();
}

void CParamsTreeNode::DeleteAllAttributes()
{
  map<CString, CParamsTreeNode *>::iterator iterAttr;
  for (iterAttr = m_mapAttributes.begin(); iterAttr != m_mapAttributes.end(); ++iterAttr)
    delete iterAttr->second;
  m_mapAttributes.clear();
}

// return false if error occurs
bool CXmlAid::EnumReadNode(CParamsTreeNode * pTreeNode, IXMLDOMNode* pXmlNode)
{
  CComPtr<IXMLDOMNodeList> spList;
  if (pXmlNode->get_childNodes(&spList) == S_OK)
  {
    long l;
    HRESULT h = spList->get_length(&l);
    if (h == S_OK)
    {
      for (long i = 0; i < l; ++i)
      {
        CComPtr<IXMLDOMNode> spItem;
        h = spList->get_item(i,&spItem);
        if (h != S_OK)
          break;
        DOMNodeType t;
        h = spItem->get_nodeType(&t);
        if (h == S_OK && t == NODE_ELEMENT)
        {
          BSTR bstrName=nullptr;
          spItem->get_nodeName(&bstrName);

          CString sValue = GetNodeValue(spItem);

          CParamsTreeNode * pNode = pTreeNode->AddNode(bstrName, sValue);
          FillTreeNodeAttributes(pNode, spItem);

          SysFreeString(bstrName);
          
          EnumReadNode(pNode, spItem);
        }
      }
    }
  }
  return true;
}

CString CXmlAid::GetNodeValue(IXMLDOMNode* pXmlNode)
{
  CString sRet(_T(""));
  CComPtr<IXMLDOMNodeList> spList;
  BSTR bstrText = nullptr;
  long lLength = 0;
  if (pXmlNode->selectNodes(L"*", &spList) == S_OK && spList)
    spList->get_length(&lLength);
  if (lLength)
  {
    CComPtr<IXMLDOMNamedNodeMap> spMap;
    if (S_OK == pXmlNode->get_attributes(&spMap) && spMap)
    {
      CComPtr<IXMLDOMNode> spNode;
      BSTR bstrValue = SysAllocString(TYPE_VALUE);
      if (S_OK == spMap->getNamedItem(bstrValue, &spNode) && spNode)
      {
        spNode->get_text(&bstrText);
        sRet = bstrText;

        SysFreeString(bstrText);
      }
      SysFreeString(bstrValue);
    }
  }
  else
  {
    pXmlNode->get_text(&bstrText);
    sRet = bstrText;
    SysFreeString(bstrText);
  }
  return sRet;
}

// #define _AFXDIAG
// return nullptr if can't parse xml
// bFirstSymbolIsSpec==true means that 1-st symbol in pBuf is special XML Unicode simbol 0xFEFF
CParamsTreeNode * CXmlAid::CreateTreeFromXmlBuffer(LPCWSTR pBuf, bool bFirstSymbolIsSpec /*= true*/)
{
  CParamsTreeNode * pRootNode = nullptr;
  try
  {
    HRESULT h = CoInitialize(nullptr);
    if (h == RPC_E_CHANGED_MODE)
    {
//      h = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);
      h = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
    }
    if (h != S_OK && h != S_FALSE)
    {
#ifdef _AFXDIAG
      CString str;
      str.Format(_T("������ CoInitialize: %0x"), h);
      AfxMessageBox(str);
#endif
      throw(1);
    }

    CComPtr<IXMLDOMDocument2> spDoc;
    h = spDoc.CoCreateInstance(CLSID_DOMDocument, 0, CLSCTX_INPROC_SERVER);
    if (h != S_OK)
    {
#ifdef _AFXDIAG
      AfxMessageBox(_T("������ CoCreateInstance"));
#endif
      throw(1);
    }
    spDoc->put_resolveExternals(VARIANT_FALSE);
    spDoc->put_validateOnParse(VARIANT_FALSE);
    spDoc->put_async(VARIANT_FALSE);

    VARIANT_BOOL b;
    h = spDoc->loadXML(_bstr_t(bFirstSymbolIsSpec ? pBuf+1 : pBuf), &b);

    if (h != S_OK || b != VARIANT_TRUE)
    {
#ifdef _AFXDIAG
      AfxMessageBox(_T("������ loadXML"));
#endif
      throw(1);
    }

    CComPtr<IXMLDOMElement> spElem;
    h = spDoc->get_documentElement(&spElem);
    if (h != S_OK)
    {
#ifdef _AFXDIAG
      AfxMessageBox(_T("������ get_documentElement"));
#endif
      throw(1);
    }

    BSTR bstrName=nullptr;
    spElem->get_nodeName(&bstrName);

    CString sValue = GetNodeValue(spElem);

    pRootNode = new CParamsTreeNode(bstrName, sValue);

    SysFreeString(bstrName);
    
    FillTreeNodeAttributes(pRootNode, spElem);
    EnumReadNode(pRootNode, spElem);
  }
  catch(_com_error & err)
  {
#ifdef _AFXDIAG
    AfxMessageBox(err.Description());
#endif
    OutputDebugString(err.Description());
    throw;
  }
  catch(...)
  {
    if (pRootNode)
    {
#ifdef _AFXDIAG
      AfxMessageBox(_T("Exception CreateTreeFromXmlBuffer"));
#endif
      delete pRootNode;
      pRootNode = nullptr;
    }
  }
  CoUninitialize();
  return pRootNode;
}

// Return nullptr if can't parse xml or can't read file
// NOTE! File in UTF16
CParamsTreeNode * CXmlAid::CreateTreeFromXmlFile(LPCTSTR pFileName)
{
  CParamsTreeNode * pRootNode = nullptr;
  LPWSTR pBuf = nullptr;
  try
  {
    CFile file;
    CFileException ex;
    if (! file.Open(pFileName, CFile::modeRead, &ex) )
      return false;

    int nFileLength = (int)file.GetLength();
    int nWLength = (nFileLength+1)/2; // length in Unicode
    pBuf = new wchar_t[nWLength+1];
    file.Read(pBuf, nFileLength);
    pBuf[nWLength] = _T('\0');

    pRootNode = CreateTreeFromXmlBuffer(pBuf);
  }
  catch(...)
  {
    if (pRootNode)
    {
      delete pRootNode;
      pRootNode = nullptr;
    }
  }
  if (pBuf)
    delete[] pBuf;
  return pRootNode;
}


// Fill pTreeNode attributes from pXmlNode attributes
void CXmlAid::FillTreeNodeAttributes(CParamsTreeNode * pTreeNode, IXMLDOMNode* pXmlNode)
{
  CComPtr<IXMLDOMNamedNodeMap> spMap;
  if (S_OK == pXmlNode->get_attributes(&spMap) && spMap)
  {
    long l;
    HRESULT h = spMap->get_length(&l);
    if (h == S_OK)
    {
      for (long i = 0; i < l; ++i)
      {
        CComPtr<IXMLDOMNode> spItem;
        h = spMap->get_item(i,&spItem);
        if (h != S_OK)
          break;
        BSTR bstrName=nullptr;
        spItem->get_nodeName(&bstrName);

        BSTR bstrText=nullptr;
        spItem->get_text(&bstrText);

        if (CStringW(bstrName) != TYPE_VALUE)
          pTreeNode->AddAttribute(bstrName, bstrText);

        SysFreeString(bstrName);
        SysFreeString(bstrText);
      }
    }
  }
}

// Fill pXmlElem attributes from pTreeNode attributes
void CXmlAid::FillXmlElemAttributes(IXMLDOMElement* pXmlElem, CParamsTreeNode * pTreeNode)
{
  map<CString, CParamsTreeNode *>::iterator iter;
  for (iter = pTreeNode->GetAttributes().begin(); iter != pTreeNode->GetAttributes().end(); ++iter)
  {
    if (iter->first != TYPE_VALUE_T)
      pXmlElem->setAttribute(CComBSTR(iter->first), _variant_t(iter->second->GetValue()));
  }
}

void CXmlAid::SetXmlElemValue(IXMLDOMElement* pXmlElem, LPCTSTR pValue, bool bHasChild)
{
  if (_tcslen(pValue))
  {
    if (bHasChild)
      pXmlElem->setAttribute(CComBSTR(TYPE_VALUE), _variant_t(pValue));
    else
      pXmlElem->put_text(_bstr_t(pValue));
  }
}

// return false if error occurs
bool CXmlAid::EnumWriteXmlElem(IXMLDOMDocument * pDoc, IXMLDOMElement* pXmlElem, CParamsTreeNode * pTreeNode)
{
  bool bPresent = false;
  list<CParamsTreeNode *>::iterator iter;
  for (iter = pTreeNode->GetListChildNodes().begin(); iter != pTreeNode->GetListChildNodes().end(); ++iter)
  {
    CComPtr<IXMLDOMElement> spNewElem;
    if (pDoc->createElement(_bstr_t((*iter)->GetName()), &spNewElem) != S_OK)
      return false;
    AppendNewline(pDoc, pXmlElem);
    if (pXmlElem->appendChild(spNewElem, nullptr) != S_OK)
      return false;
    // AppendNewline(pDoc, pXmlElem);
    CParamsTreeNode * pChildTreeNode = *iter;
    bool bChildHasChild = ! pChildTreeNode->GetListChildNodes().empty();
    SetXmlElemValue(spNewElem, pChildTreeNode->GetValue(), bChildHasChild);
    FillXmlElemAttributes(spNewElem, pChildTreeNode);

    if (! EnumWriteXmlElem(pDoc, spNewElem, pChildTreeNode))
      return false;
    bPresent = true;
  }
  if (bPresent)
    AppendNewline(pDoc, pXmlElem);
  return true;
}

// return false if can't create xml
bool CXmlAid::CreateXmlFileFromTree(LPCTSTR pFileName, CParamsTreeNode * pTreeNode)
{
  bool bRet = false;
  try
  {
    HRESULT h = CoInitialize(nullptr);
    if ( h != S_OK && h != S_FALSE)
      throw(1);

    CComPtr<IXMLDOMDocument2> spDoc;
    if (! CreateIXmlDomDocFromTreeFile(spDoc, pTreeNode) )
      throw(1);

    if (spDoc->save(_variant_t(pFileName)) != S_OK)
      throw(1);

    bRet = true;
  }
  catch(_com_error & err)
  {
    OutputDebugString(err.Description());
    throw;
  }
  catch(...)
  {
    bRet = false;
  }

  CoUninitialize();
  return bRet;
}

// return false if can't create
bool CXmlAid::CreateIXmlDomDocFromTreeFile(CComPtr<IXMLDOMDocument2> & spDoc, CParamsTreeNode * pTreeNode)
{
  bool bRet = false;
  try
  {
    HRESULT h = spDoc.CoCreateInstance(CLSID_DOMDocument, 0, CLSCTX_INPROC_SERVER);
    if (h != S_OK)
      throw(1);
    spDoc->put_resolveExternals(VARIANT_FALSE);
    spDoc->put_validateOnParse(VARIANT_FALSE);
    spDoc->put_async(VARIANT_FALSE);
    spDoc->put_preserveWhiteSpace(VARIANT_TRUE);

    CComPtr<IXMLDOMProcessingInstruction> spProcInstr;
    h = spDoc->createProcessingInstruction(_bstr_t("xml"), _bstr_t("version=\"1.0\"  encoding=\"utf-16\""), &spProcInstr);
    if (h != S_OK)
      throw(1);

    if (spDoc->appendChild(spProcInstr, nullptr) != S_OK)
      throw(1);

    CComPtr<IXMLDOMElement> spRootElem;
    if (spDoc->createElement(_bstr_t(pTreeNode->GetName()), &spRootElem) != S_OK)
      throw(1);
    if (spDoc->appendChild(spRootElem, nullptr) != S_OK)
      throw(1);

    bool bHasChild = ! pTreeNode->GetListChildNodes().empty();
    SetXmlElemValue(spRootElem, pTreeNode->GetValue(), bHasChild);
    FillXmlElemAttributes(spRootElem, pTreeNode);

    if (! EnumWriteXmlElem(spDoc, spRootElem, pTreeNode))
      throw(1);

    bRet = true;
  }
  catch(...)
  {
    bRet = false;
  }
  return bRet;
}

// return nullptr if can't create xml
LPWSTR CXmlAid::CreateXmlBufferFromTree(CParamsTreeNode * pTreeNode, int & nLength)
{
  LPWSTR pBuf = nullptr;
  nLength = 0;
  try
  {
    HRESULT h = CoInitialize(nullptr);
    if ( h != S_OK && h != S_FALSE)
      throw(1);

    CComPtr<IXMLDOMDocument2> spDoc;
    if (! CreateIXmlDomDocFromTreeFile(spDoc, pTreeNode) )
      throw(1);

    CComPtr<IStream> spStream;
    if (CreateStreamOnHGlobal(nullptr, TRUE, &spStream) != S_OK)
      throw(1);

    CComVariant v(spStream);
    if (spDoc->save(v) != S_OK)
      throw(1);

    int nSaveBytes = 0;
    LARGE_INTEGER i = {0,}; ULARGE_INTEGER u;
    if (spStream->Seek(i,STREAM_SEEK_END,&u) != S_OK)
      throw(1);
    if (!u.HighPart && u.LowPart <= _I32_MAX)
      nSaveBytes = (int)u.LowPart;

    if (!nSaveBytes) // IStream too long
      throw(1);
      
    HGLOBAL hSaveBytes = 0;
    if (GetHGlobalFromStream(spStream, &hSaveBytes) != S_OK)
      throw(1);

    LPVOID pSaveBytes = GlobalLock(hSaveBytes);
    if (!pSaveBytes)
      throw(1);

    nLength = nSaveBytes;
    
    int nWLength = (nSaveBytes+1)/2; // unicode length
    pBuf = new wchar_t[nWLength + 1];
    memcpy(pBuf, pSaveBytes, nSaveBytes);
    pBuf[nWLength] = L'\0';

    GlobalUnlock(hSaveBytes);
  }
  catch(_com_error & err)
  {
    OutputDebugString(err.Description());
    throw;
  }
  catch(...)
  {
    if (pBuf)
    {
      delete[] pBuf;
      pBuf = nullptr;
    }
  }

  CoUninitialize();
  return pBuf;
}

void CXmlAid::AppendNewline(IXMLDOMDocument * pDoc, IXMLDOMNode * pXmlNode)
{
  CComPtr<IXMLDOMText> spText;
  CComBSTR t("\n");
  pDoc->createTextNode(t,&spText);
  pXmlNode->appendChild(spText, nullptr);
}
