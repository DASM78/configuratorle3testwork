#pragma once

#include "ExportDef.h"

struct PRIVATELIBS_API MESSAGE_SET
{
  MESSAGE_SET() {}
  MESSAGE_SET(void* data, int size)
  {
    if (size > 0)
      Add_pData(data, size);
    else
      pData = (BYTE*)data;
  }
  MESSAGE_SET(CString data)
    : sData(data)
  { }
  MESSAGE_SET(int data)
    : nData(data)
  {
  }

  int nData = -1;

  CString sData;

  int nDataSize = 0;
  BYTE* pData = nullptr;

  void Add_pData(void* data, int size)
  {
    ASSERT(data);
    if (!data)
      return;
    nDataSize = size;
    pData = new BYTE[nDataSize]{};
    CopyMemory(pData, data, nDataSize);
  }

  void Free_pData()
  {
    if (pData)
    {
      delete pData;
      pData = nullptr;
    }
    nDataSize = 0;
  }

  MESSAGE_SET& operator=(MESSAGE_SET m)
  {
    nData = m.nData;

    sData = m.sData;

    nDataSize = m.nDataSize;
    pData = new BYTE[nDataSize]{};
    CopyMemory(pData, m.pData, nDataSize);

    return *this;
  }
};