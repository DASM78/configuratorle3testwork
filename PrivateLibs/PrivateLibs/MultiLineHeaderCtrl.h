#pragma once

#include "ExportDef.h"
#include "afxheaderctrl.h"

// CMultiLineHeaderCtrl

//	Структурка для быстрого отображения заголовков
typedef struct HEADEREXSHOWDATA
{
  HEADEREXSHOWDATA*	next;
  int		width;
  TCHAR	szName[512];
} *LPHEADEREXSHOWDATA;


class PRIVATELIBS_API CMultiLineHeaderCtrl: public CMFCHeaderCtrl
{
  DECLARE_DYNAMIC(CMultiLineHeaderCtrl)

public:
  CMultiLineHeaderCtrl();
  virtual ~CMultiLineHeaderCtrl();

protected:
  DECLARE_MESSAGE_MAP()

  int HeaderExTitleHeight();
  void DrawFrameControlEx(CDC * pDC, LPRECT lprc, UINT uType, UINT uState);
  DWORD OffsetColor(DWORD color,BYTE offset,BOOL dir);
  void HeaderEx_SetTit(CDC * pDC, RECT **r, TCHAR ** out, LPHEADEREXSHOWDATA* l, int pos, int maxstr);
  void HeaderExVCenter(CDC *pDC, TCHAR* out, RECT* r);

  afx_msg LRESULT OnLayout(WPARAM wparam, LPARAM lparam);
public:
  afx_msg void OnPaint();
};

