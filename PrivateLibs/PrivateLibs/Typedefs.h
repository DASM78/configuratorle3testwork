#pragma once

typedef unsigned int formID_t;
typedef CString varMeaning_t;
typedef bool varOnce_t;

#define bSetVariable_d true
#define bUnsetVariable_d false

#define bSwitchOnProperty_d true
#define bSwitchOffProperty_d false
