// ����� ��� CMFCListCtrl � ������������ �������������� ������ �������� ������� � ������������ ����������� �������� �������.
// �������� ����:
// http://www.codeproject.com/KB/list/Emery-Emerald.aspx
// http://www.sources.ru/cpp/winapi/api_header_ex.html

// ���������: ���� ��������� ���������� � '\r', �� ��������� ������ ������� (������ '\r' ����������

#include "stdafx.h"
#include "afxglobals.h"
#include "MultiLineHeaderCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// CMultiLineHeaderCtrl

IMPLEMENT_DYNAMIC(CMultiLineHeaderCtrl, CMFCHeaderCtrl)

CMultiLineHeaderCtrl::CMultiLineHeaderCtrl()
{

}

CMultiLineHeaderCtrl::~CMultiLineHeaderCtrl()
{
}


BEGIN_MESSAGE_MAP(CMultiLineHeaderCtrl, CMFCHeaderCtrl)
  ON_MESSAGE(HDM_LAYOUT, OnLayout)
  ON_WM_PAINT()
END_MESSAGE_MAP()


LRESULT CMultiLineHeaderCtrl::OnLayout(WPARAM, LPARAM lParam)
{
  LPHDLAYOUT pHL = reinterpret_cast<LPHDLAYOUT>(lParam);
  int nRet = __super::DefWindowProc(HDM_LAYOUT, 0, lParam);
  if(pHL->pwpos && pHL->pwpos->cy)
  {
    pHL->prc->top = HeaderExTitleHeight()+5;
    pHL->pwpos->cy = HeaderExTitleHeight()+4;
  }
  return nRet;
}

/*
  ������� ������� ������ ���������
  ��� ���� ������ ������� ���������� ��������� ������ �
  �������� �� ������ ����� "W" - ���������� ��� MSDN
  �� � ��������� ������ �����, ������� ����� �������
  ������� ���������
*/
int CMultiLineHeaderCtrl::HeaderExTitleHeight()
{
  int titleheight = 0;
  TCHAR out[512];
  HDITEM hdi;
  hdi.mask = HDI_TEXT;
  hdi.pszText = out;
  hdi.cchTextMax = sizeof(out);

  CDC * pDC = GetDC();
  CFont* pOldFont = pDC->SelectObject(&afxGlobalData.fontRegular);
  CSize sz = pDC->GetTextExtent(_T("W"),1);
  pDC->SelectObject (pOldFont);
  ReleaseDC(pDC);

  int cnt = GetItemCount();
  for (int i = 0; i < cnt; ++i)
  {
    GetItem(i, &hdi);
    int tmp = 0;
    TCHAR *a = out;
    while(a)
    {
      ++tmp;
      a = _tcschr(a+1, _T('\n'));
    }
    if(titleheight < tmp)
      titleheight=tmp;
  }
  sz.cy += 2;//*GetSystemMetrics(SM_CXDLGFRAME)+2;
  titleheight *= sz.cy;
  return titleheight;
}


void CMultiLineHeaderCtrl::OnPaint()
{
  int titleheight = 0;
  RECT r, rw, rc, *r1=&r;
  TCHAR *out=_T("�");
  GetWindowRect(&r);
  GetWindowRect(&rw);
  GetClientRect(&rc);
  titleheight = rw.bottom-rw.top;

  CPaintDC dcc(this); // device context for painting
  CDC dc;
  dc.CreateCompatibleDC(&dcc);
  CBitmap bmp;
  bmp.CreateCompatibleBitmap(&dcc,r.right-r.left,titleheight);
  CBitmap * oldbmp = dc.SelectObject(&bmp);
  dc.SetBkMode(TRANSPARENT);
  CFont * oldfont = dc.SelectObject(&afxGlobalData.fontRegular);
  int maxstr=0;
  int cnt = GetItemCount();
  if(cnt)
  {
    int* ar = new int[cnt];
    GetOrderArray(ar, cnt);
    LPHEADEREXSHOWDATA ldata = new HEADEREXSHOWDATA[cnt+1];
    for(int i=0; i<cnt; ++i)
    {
      memset(&ldata[i],0,sizeof(ldata[i]));
      HDITEM hdi;
      hdi.mask = HDI_WIDTH|HDI_TEXT;
      hdi.cxy = 0;
      hdi.pszText = ldata[i].szName;
      hdi.cchTextMax = sizeof(ldata[i].szName);
      GetItem(ar[i], &hdi);
      ldata[i].width = hdi.cxy;
      ldata[i].next = &ldata[i+1];
    }
    memset(&ldata[cnt], 0, sizeof(ldata[cnt]));
    lstrcpy(ldata[cnt].szName, _T("          "));
    for(LPHEADEREXSHOWDATA l = ldata; l!=nullptr; l=l->next)
    {
      int tmp=0;
      TCHAR *a = l->szName;
      while(a)
      {
        ++tmp;
        a = _tcschr(a+1, _T('\n'));
      }
      if(maxstr < tmp)
        maxstr = tmp;
    }

    SetRect(&r, 0, 0, 0, titleheight);
    LPHEADEREXSHOWDATA l = ldata;
    HeaderEx_SetTit(&dc, &r1, &out, &l, 0, maxstr);

    delete [] ldata;
    delete [] ar;
  }
  else
  {
    SetRect(&r, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top);
    DrawFrameControlEx(&dc, &r, DFC_BUTTON, DFCS_BUTTONPUSH);
  }
  dc.SelectObject(oldfont);
  SetRect(&r,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top);
  dcc.BitBlt(r.left,r.top,r.right,r.bottom, &dc,0,0,SRCCOPY);
  dc.SelectObject(oldbmp);
  dc.DeleteDC();
}

/*
  ��� �-��� ������ DrawFrameControl, ������ ��������� �������������
  ���������, ���� ����� ����� 256 ������
 */
void CMultiLineHeaderCtrl::DrawFrameControlEx(CDC * pDC, LPRECT lprc, UINT uType, UINT uState)
{
  if(uType==DFC_BUTTON && pDC->GetDeviceCaps(BITSPIXEL)>=16)
  {
    int offset = 32;
    DWORD cl[2] = { OffsetColor(GetSysColor(COLOR_BTNFACE),offset/2,TRUE), OffsetColor(GetSysColor(COLOR_BTNFACE),offset/2,FALSE) };
    if((uState&DFCS_PUSHED) == DFCS_PUSHED)
    {
      DWORD cll = cl[0];
      cl[0] = cl[1];
      cl[1] = cll;
    }
    int r1 = GetRValue(cl[0]);
    int g1 = GetGValue(cl[0]);
    int b1 = GetBValue(cl[0]);
    int r2 = GetRValue(cl[1]);
    int g2 = GetGValue(cl[1]);
    int b2 = GetBValue(cl[1]);
    CPen pen;
    pen.CreatePen(PS_SOLID, 1, cl[0]);
    CPen * opn = pDC->SelectObject(&pen);
    for(int y=0; y <= lprc->bottom-lprc->top; ++y)
    {
      DWORD cll = RGB(
        r1+(r2-r1)*y/(lprc->bottom-lprc->top),
        g1+(g2-g1)*y/(lprc->bottom-lprc->top),
        b1+(b2-b1)*y/(lprc->bottom-lprc->top));
      CPen tmp;
      tmp.CreatePen(PS_SOLID, 1, cll);
      pDC->SelectObject(&tmp);
      pDC->MoveTo(lprc->left, lprc->top+y);
      pDC->LineTo(lprc->right, lprc->top+y);
    }
    pDC->SelectObject(opn);
    pDC->DrawEdge(lprc, ((uState&DFCS_PUSHED) == DFCS_PUSHED) ? BDR_SUNKENOUTER:EDGE_RAISED, BF_RECT);
  }
  else
  {
  // � ��� ������, ���� ���� �����, � �������� ����!
  // ���, �����, ������ �������� �� ������.
    pDC->DrawFrameControl(lprc, uType, uState);
  }
}

/*
    � ��� �-��� �������� ��� �������� ��������
  � ������ ���� ������� - ����� ������ ������ 256,
  ������ "��������" ���������
 */
DWORD CMultiLineHeaderCtrl::OffsetColor(DWORD color,BYTE offset,BOOL dir)
{
  DWORD ncolor = color;
  if(!dir)
  {
    if(GetRValue(ncolor)>offset) ncolor-=offset;				else ncolor&=0xffffff00;
    if(GetGValue(ncolor)>offset) ncolor-=(DWORD)(offset<<8);	else ncolor&=0xffff00ff;
    if(GetBValue(ncolor)>offset) ncolor-=(DWORD)(offset<<16);	else ncolor&=0xff00ffff;
  }
  else
  {
    if((GetRValue(ncolor)+offset)<0x100) ncolor+=offset;				else ncolor|=0x000000ff;
    if((GetGValue(ncolor)+offset)<0x100) ncolor+=(DWORD)(offset<<8);	else ncolor|=0x0000ff00;
    if((GetBValue(ncolor)+offset)<0x100) ncolor+=(DWORD)(offset<<16);	else ncolor|=0x00ff0000;
  }
  return ncolor;
}

void CMultiLineHeaderCtrl::HeaderEx_SetTit(CDC * pDC, RECT **r, TCHAR ** out, LPHEADEREXSHOWDATA* l, int pos, int maxstr)
{
  RECT r1,r2;
  TCHAR tmp[1000];
  while(*l)
  {
    SetRect(&r1, (*r)->right, (*r)->top, (*r)->right+(*l)->width, (*r)->bottom);
    TCHAR *a=(*out);
    for(int i=0; i<=pos; ++i)
    {
      a = _tcschr(a+1, _T('\n'));
      if(!a)
      {
        a = (*out)+lstrlen(*out);
        break;
      }
    }
    int alen = a-(*out);
    if(*a==_T('\n') && !_tcsncmp((*out), (*l)->szName, alen))
    {
      int rl=(*r)->left;
      TCHAR * b=a=(*out);
      for(int i=0; i<pos; ++i)
      {
        a=_tcschr(a+1, _T('\n'));
        if(!a)
        { a = (*out)+lstrlen(*out);
          break;
        }
      }
      int aa = _tcschr(a+1,_T('\n'))-a;
      while(!_tcsncmp(b,(*out),alen))
        HeaderEx_SetTit(pDC, r, out, l, pos+1, maxstr);
      SetRect(&r2, rl, (*r)->top+((*r)->bottom-(*r)->top)*pos/maxstr, (*r)->left, (*r)->top+((*r)->bottom-(*r)->top)*(pos+1)/maxstr);
      lstrcpyn(tmp, a+(*a==_T('\n') ? 1 : 0), aa+(*a==_T('\n') ? 0 : 1));
      HeaderExVCenter(pDC,tmp,&r2);
      if(pos)
        return;
      continue;
    }
    a=(*out);
    for(int i=0; i<pos; ++i)
    {
      a = _tcschr(a+1, _T('\n'));
      if(!a)
      {
        a = (*out)+lstrlen(*out);
        break;
      }
      ++a;
    }
    SetRect(&r2, (*r)->left, (*r)->top+((*r)->bottom-(*r)->top)*pos/maxstr, (*r)->right, (*r)->bottom);
    HeaderExVCenter(pDC,a,&r2);
    CopyRect(*r,&r1);
    if(_tcsncmp((*out), (*l)->szName, a-(*out)))
      break;
    if(*l)
    {
      (*out)=(*l)->szName;
      (*l)=(*l)->next;
    }
  }
  if(*l)
  { (*out)=(*l)->szName;
    (*l)=(*l)->next;
  }
  SetRect(&r1,(*r)->right,(*r)->top,2000,(*r)->bottom);
  HeaderExVCenter(pDC,_T(""),&r1);
}

/*
  ��� ������� ������������ ��������� ������ � �����
  ����� � ������, �� �, ������� ��, ������ ��� �
  ������.
 */
void CMultiLineHeaderCtrl::HeaderExVCenter(CDC *pDC, TCHAR* out, RECT* r)
{
  RECT rr,r1;
  CopyRect(&r1,r);
  CopyRect(&rr,r);
  if(((r1.right-1)>r1.left)&&((r1.bottom-7)>r1.top))
  {
    CFont * pOldFont = pDC->GetCurrentFont();
    bool bBold = false;
    if (*out == _T('\r'))
    {
      ++out;
      bBold = true;
      LOGFONT lf;
      pOldFont->GetLogFont(&lf);
      lf.lfWeight = FW_BOLD;

      CFont newFont;
      newFont.CreateFontIndirect(&lf);
      pDC->SelectObject(&newFont);
    }

    DrawFrameControlEx(pDC, &r1, DFC_BUTTON, DFCS_BUTTONPUSH);
//		pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER|DT_CALCRECT);
    pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER|DT_CALCRECT|DT_EXPANDTABS);
//		pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER|DT_CALCRECT|DT_MODIFYSTRING|DT_END_ELLIPSIS);
    int dist = ((rr.bottom-rr.top)-(r1.bottom-r1.top))/2;
    r1.top+=dist; r1.bottom+=dist; r1.right = rr.right;
    if(dist<0)
      CopyRect(&r1,&rr);
//		pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER);
    pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER|DT_EXPANDTABS);
//		pDC->DrawText(out, -1, &r1, DT_WORDBREAK|DT_CENTER|DT_END_ELLIPSIS|DT_MODIFYSTRING);

    if (bBold)
      pDC->SelectObject(pOldFont);
  }
}
