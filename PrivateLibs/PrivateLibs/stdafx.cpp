// stdafx.cpp : source file that includes just the standard includes
// PrivateLibs.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#pragma comment(lib,"msxml2")
#pragma comment(lib,"gdiplus")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


