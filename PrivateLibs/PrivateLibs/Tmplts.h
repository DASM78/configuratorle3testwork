#pragma once

template <typename T>
bool CompareValues(const T& v1, const T& v2, T& result)
{
  if (v1 != v2)
  {
    result = v1;
    return true;
  }
  return false;
}