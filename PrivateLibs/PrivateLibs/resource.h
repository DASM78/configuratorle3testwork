//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PrivateLibs.rc
//
#define IDS_YES                         1100
#define IDS_NO                          1101
#define IDS_ERROR_RUN_APP               1102
#define IDS_LIST_FULL                   1103
#define IDS_ALL_MEMBERS_ONE_VALUE       1104
#define IDS_ERROR_CREATING_FOLDER       1105
#define IDS_SELECTED_FOLDER             1106
#define IDS_SELECTED_FILE               1107
#define IDS_NO_RIGHTS_READ              1108
#define IDS_TO_SELECTED_FOLDER          1109
#define IDS_TO_SELECTED_FILE            1110
#define IDS_NO_RIGHTS_WRITE             1111
#define IDS_IN_SELECTED_FOLDER          1112
#define IDS_IN_SELECTED_FILE            1113
#define IDS_NO_RIGHTS_READWRITE         1114
#define IDS_REQUIRE_REBOOT              1115
#define MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL 1900
#define MSG_MEAN_WAS_NOT_SETED_AFTER_BIND_CTRL 1901
#define MSG_BEFORE_ADD_NEW_STRING_BY_ENTER_KEYDOWN 1902
#define MSG_AFTER_ADD_NEW_STRING_BY_ENTER_KEYDOWN 1903
#define MSG_AFTER_SELCHANGE_STRING      1904

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        1103
#define _APS_NEXT_COMMAND_VALUE         33000
#define _APS_NEXT_CONTROL_VALUE         1600
#define _APS_NEXT_SYMED_VALUE           1905
#endif
#endif
