#include "stdafx.h"
#include <tchar.h>
#include "VersionInterface.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CVersionInterface::CompareVersions(CString sVersion1, CString sVersion2)
{
  int n1 = _tstoi(sVersion1);
  int n2 = _tstoi(sVersion2);
  if (n1 < n2)
    return -1;
  if (n1 > n2)
    return 1;

  n1 = 0; n2 = 0;
  int n = sVersion1.Find(_T('.'));
  if (n != -1)
  {
    sVersion1 = sVersion1.Right(sVersion1.GetLength() - n - 1);
    n1 = _tstoi(sVersion1);
  }
  n = sVersion2.Find(_T('.'));
  if (n != -1)
  {
    sVersion2 = sVersion2.Right(sVersion2.GetLength() - n - 1);
    n2 = _tstoi(sVersion2);
  }
  if (n1 < n2)
    return -1;
  if (n1 > n2)
    return 1;

  n1 = 0; n2 = 0;
  n = sVersion1.Find(_T('.'));
  if (n != -1)
  {
    sVersion1 = sVersion1.Right(sVersion1.GetLength() - n - 1);
    n1 = _tstoi(sVersion1);
  }
  n = sVersion2.Find(_T('.'));
  if (n != -1)
  {
    sVersion2 = sVersion2.Right(sVersion2.GetLength() - n - 1);
    n2 = _tstoi(sVersion2);
  }
  if (n1 < n2)
    return -1;
  if (n1 > n2)
    return 1;
 
  n1 = 0; n2 = 0;
  n = sVersion1.Find(_T('.'));
  if (n != -1)
  {
    sVersion1 = sVersion1.Right(sVersion1.GetLength() - n - 1);
    n1 = _tstoi(sVersion1);
  }
  n = sVersion2.Find(_T('.'));
  if (n != -1)
  {
    sVersion2 = sVersion2.Right(sVersion2.GetLength() - n - 1);
    n2 = _tstoi(sVersion2);
  }
  if (n1 < n2)
    return -1;
  if (n1 > n2)
    return 1;
 
  return 0;
}

// ��������, ��� sVersion= 1.2.3.4 
// GetNumVersionByIndex(0, sVersion)==1
// GetNumVersionByIndex(3, sVersion)==4
int CVersionInterface::GetNumVersionByIndex(int nIndex, CString sVersion)
{
  for (int i = 0; i < nIndex; ++i)
  {
    int n = sVersion.Find(_T('.'));
    if (n != -1)
      sVersion = sVersion.Right(sVersion.GetLength() - n - 1);
  }
  return _tstoi(sVersion);
}