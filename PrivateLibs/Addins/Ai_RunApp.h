#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_RunApp
{
public:
  BOOL RunApplication(CString strPath, CString strParams = _T(""), 
                               BOOL bWaitApp = TRUE, WORD swShow = SW_SHOW, DWORD *pnExitCode = nullptr, 
                               CString* pstrEnv = nullptr, CString* pstrCurrDir = nullptr);
  bool RunApplication2(CString strCmdLine, bool bShow = false);
};
