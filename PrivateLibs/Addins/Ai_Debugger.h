#pragma once

#include "AddinsDef.h"
#include <vector>

class ADDINS_API CDebugger
{
public:
  CDebugger();

  static void TraceOfNew(CString strValueName, void* p);
  
  //std::vector<std::pair<int, int>> m_newList; // int, int -> new, was delele
  //void NewList(CString strValueName, void* p);
  //void DeleteList(CString strValueName, void* p);
};
