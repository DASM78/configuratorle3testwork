#pragma once

#include "AddinsDef.h"

#ifndef __BINARY_VAL_H__
#define __BINARY_VAL_H__

template<int i>
struct ADDINS_API _b8tpl_ {
	enum {
		// incorrectly defined numbers will try to instantiate _b8tpl_<-1>
		ok = (i >= 0) && (i <= 011111111) && ((i & 7) <= 1),
		val = 2 * _b8tpl_<ok ? (i >> 3) : -1>::val + (i & 7)
	};
};

template <>
struct _b8tpl_<0> {
	enum { val = 0 };
};

template <>
struct ADDINS_API _b8tpl_<-1> {
	// _b8tpl_<-1> doesn't define the enumerator "val"
	// so an erroneous number will produce a compile error
};

// several macros to support definition of integral values
// on a 32-bit machine by using the binary notation
// (we'll make sure that the integral literals are octal by prefixing them with a zero)
#define BIN8(b) ((unsigned char)_b8tpl_<0##b>::val)
#define BIN16(b1, b0) ((((unsigned short)BIN8(b1)) << 8) | BIN8(b0))
#define BIN32(b3, b2, b1, b0) ((((unsigned int)BIN16(b3, b2)) << 16) | BIN16( b1, b0))
#define BIN64(b7, b6, b5, b4, b3, b2, b1, b0) ((((unsigned __int64)BIN32(b7, b6, b5, b4)) << 32) | BIN32(b3, b2, b1, b0))
#define BIN64_1(b0) BIN64(0, 0, 0, 0, 0, 0, 0, b0)
#define BIN64_2(b1, b0) BIN64(0, 0, 0, 0, 0, 0, b1, b0)
#define BIN64_3(b2, b1, b0) BIN64(0, 0, 0, 0, 0, b2, b1, b0)
#define BIN64_4(b3, b2, b1, b0) BIN64(0, 0, 0, 0, b3, b2, b1, b0)

#endif //__BINARY_VAL_H__

