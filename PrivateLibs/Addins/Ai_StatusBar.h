#pragma once

#include "afxstatusbar.h"
#include "AddinsDef.h"

class ADDINS_API CAi_StatusBar : public CMFCStatusBar
{
public:
	CAi_StatusBar();
	~CAi_StatusBar();

	enum DRAW_MODE { dmNone, dmFirst, dmSecond };

	bool m_bUse;

	void SetPaneDraw(int nDrawInPane, UINT nIDFirstModeBitmap, UINT nIDSecondModeBitmap);
	void SetDrawMode(DRAW_MODE newDrawMode);

	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
		
private:
	DRAW_MODE m_drawMode;
	int m_nDrawInPane;
	CBitmap m_btmFirsMode;
	CBitmap m_btmSecondMode;

	void DrawIcons();
};