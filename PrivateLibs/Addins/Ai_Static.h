#pragma once

#include <vector>
#include "AddinsDef.h"

class ADDINS_API CAi_Static : public CStatic
{
public:
  CAi_Static();
  virtual ~CAi_Static();

  void SetStartRGB(int nR = 145, int nG = 145, int nB = 145) { m_nR = (double)nR; m_nG = (double)nG; m_nB = (double)nB; }
  void SetGradient(double nGradient = 1.65) { m_nGradient = nGradient; }

  void WorkWithColor(bool bR = true, bool bG = true, bool bB = true) { m_bR = bR; m_bG = bG; m_bB = bB; }
  
  enum EForwardOrBack { fbForward, fbBack }; // fbForward - gradient will be increased to color, i.e. from dark to white
  void ForwardOrBack(EForwardOrBack fb) { m_forwardOrBack = fb; }
  
  enum EHorizontalOrVertical { hvHorizontal, hvVertical };
  void HorizontalOrVertical(EHorizontalOrVertical hv) { m_horizontalOrVertical = hv; }

  void StopIfEqual(int nR = 255, int nG = 255, int nB = 255) { m_nStopR = nR; m_nStopG = nG; m_nStopB = nB; }
  void SetStep(int nStep = 1);

  void SetFrame(int nLineThickness = 1, COLORREF nColor = RGB(0, 0, 255)) { m_nFrameLineThickness = nLineThickness; m_nFrameColor = nColor; }

  void SetText(CString strText, COLORREF nClr = RGB(0, 0, 0)) { m_strText = strText; m_nTextColor = nClr; }
  void SetTextLeftMargin(int nOffset) { m_nLeftMargin = nOffset; }
  void SetTextTopMargin(int nOffset) { m_nTopMargin = nOffset; }
  void SetTextOtherColor(COLORREF nClr) { m_nTextColor = nClr; }
  void SetOtherFont(CFont* pFont);
  enum ASTextAligh // horizontal, vertical
  {
    taLeftTop,
    taLeftCenter,
    taCenterCenter
  }
  m_textAlign;
  void SetTextAlign(ASTextAligh ta = taLeftTop) { m_textAlign = ta; }
  void CalcForVerticalAlign(CPaintDC* pDC, CString sIn, int& nVertStrPoint, int& nNextStrOffset, std::vector<CString>* pResStrs);
  void CalcForHorizontalAlign(CPaintDC* pDC, CString sIn, int& nLeftStrPoint);

  void SetStatus(bool bEnable) { m_bEnable = bEnable; }
  bool GetStatus() { return m_bEnable; }

protected:
  DECLARE_MESSAGE_MAP()

  bool m_bEnable;
  double m_nR;
  double m_nG;
  double m_nB;
  bool m_bR;
  bool m_bG;
  bool m_bB;
  double m_nGradient;
  int m_nStopR;
  int m_nStopG;
  int m_nStopB;
  int m_nStep;
  EForwardOrBack m_forwardOrBack;
  EHorizontalOrVertical m_horizontalOrVertical ;
  CString m_strText;
  COLORREF m_nTextColor;
  int m_nLeftMargin;
  int m_nTopMargin;
  CFont* m_pFont;
  int m_nFrameLineThickness;
  COLORREF m_nFrameColor;

  afx_msg void OnPaint();
public:
  virtual void OnPaintAfter(CPaintDC* pdc) { pdc = pdc; }
};
