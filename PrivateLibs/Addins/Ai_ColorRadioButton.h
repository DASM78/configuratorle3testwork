#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_ColorRadioButton: public CButton
{
public:
  void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text.

  CAi_ColorRadioButton();
  ~CAi_ColorRadioButton();

  DECLARE_MESSAGE_MAP()

protected:

  CBrush m_brBkgnd; // Holds Brush Color for the Static Text
  COLORREF m_crBkColor; // Holds the Background Color for the Text

  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
};