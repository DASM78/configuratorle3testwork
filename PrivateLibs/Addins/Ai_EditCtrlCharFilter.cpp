#include "StdAfx.h"
#include "Ai_EditCtrlCharFilter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_EditCtrlCharFilter::CAi_EditCtrlCharFilter()
{
  m_sASCII_Chars = L"`-=~!@#$%^&*()_+,./<>?;'\\:\"|[]{} 1234567890AaBbCcDdEeFfGgHhJjKkIiLlMmNnOoPpQqRrSsTtVvUuWwXxYyZz";
}

CAi_EditCtrlCharFilter::~CAi_EditCtrlCharFilter()
{

}

BEGIN_MESSAGE_MAP(CAi_EditCtrlCharFilter, CEdit)
  //ON_WM_CHAR()
  ON_WM_KEYUP()
END_MESSAGE_MAP()

void CAi_EditCtrlCharFilter::IgnoreSimbols(CString sSimbols)
{
  m_sIgnoreSimbols = sSimbols;
}

/*void CAi_EditCtrlCharFilter::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  //if (m_bCheckChar)
    CEdit::OnChar(nChar, nRepCnt, nFlags);
}*/

void CAi_EditCtrlCharFilter::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  CEdit::OnKeyUp(nChar, nRepCnt, nFlags);

  int nStartPos = 0;
  int nEnd = 0;
  GetSel(nStartPos, nEnd);

  CString sNewText;
  GetWindowText(sNewText);
  CString sTextWas = sNewText;
  TRACE(sNewText + L"\n");

  auto lmbDelSimbol = [](CString& sNewText, int& nStartPos, int& i)->void
  {
    sNewText.Delete(i);
    --nStartPos;
    --i;
  };

  if (m_filterSet == fsASCII)
  {
    for (int i = 0; i < sNewText.GetLength(); ++i)
    {
      bool bIs = false;
      CString sCharInCtrl(sNewText.GetAt(i));

      for (int a = 0; a < m_sASCII_Chars.GetLength(); ++a)
      {
        CString sCharReal(m_sASCII_Chars.GetAt(a));
        if (sCharReal == sCharInCtrl)
        {
          bIs = true;
          break;
        }
      }

      if (bIs) // ������ ����������� ������ ������ �������� - fsASCII
      {
        // �������� - �� �������� �� ������ ���, ������� ���� ������������ �� ������ ������
        for (int k = 0; k < m_sIgnoreSimbols.GetLength(); ++k)
        {
          CString sCharIgnore(m_sIgnoreSimbols.GetAt(k));
          if (sCharIgnore == sCharInCtrl)
          {
            lmbDelSimbol(sNewText, nStartPos, i);
            break;
          }
        }
      }
      else // ������ �� ����������� ������ ������ �������� - fsASCII
      {
        lmbDelSimbol(sNewText, nStartPos, i);
      }
    }
  }

  if (sTextWas != sNewText)
  {
    SetWindowText(sNewText);
    if (nStartPos > 0)
      SetSel(nStartPos, nStartPos, 0);
  }

  CtrlTextAfterCheck(sNewText);
}
