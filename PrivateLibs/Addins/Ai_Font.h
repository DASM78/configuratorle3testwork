#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_Font
{
public:
  CAi_Font();
  ~CAi_Font();

  /*
  CWnd* pCtrl, - ��������� �� Ctrl �� �������� ����� Font, ��� ������� - this
  CFont& fntDst, - ��� ���� �������
  CString pszFaceName = L"", - ���� ����� �� ����� ��� � Ctrl
  int cEscapement = FW_NORMAL, - �������
  BYTE iQuality = DEFAULT_QUALITY,
  int nHeight = MAXINT - ���� MAXINT, �� ������ ������� ��� � Ctrl
  */
  static bool CreateFont_(
    CWnd* pCtrl,
    CFont& fntDst,
    CString pszFaceName = L"",
    int cEscapement = FW_NORMAL,
    BYTE iQuality = DEFAULT_QUALITY,
    int nHeight = MAXINT,
    bool bItalic = false
    );
};

