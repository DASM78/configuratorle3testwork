#pragma once

#include <map>
#include "afxtooltipctrl.h"
#include "AddinsDef.h"

struct ADDINS_API TOOL_TIP_CTRL_INFO
{
  CString strDescription;
  int nBmpResID;
};

class ADDINS_API CAi_ToolTip : public CMFCToolTipCtrl
{
public:
  CAi_ToolTip();
  virtual ~CAi_ToolTip();

  std::map <int, TOOL_TIP_CTRL_INFO> m_toolTCtrlsInfo;

private:
  int	m_nCurrID;
  std::map <int, TOOL_TIP_CTRL_INFO>::iterator m_iter;

  virtual CSize GetIconSize ()
  {
    return CSize (32, 32);
  }
  virtual BOOL OnDrawIcon (CDC* pDC, CRect rectImage);
  afx_msg void OnShow(NMHDR* pNMHDR, LRESULT* pResult);
  DECLARE_MESSAGE_MAP()
};
