#include "StdAfx.h"
#include "Ai_GetResFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_GetResFile::CAi_GetResFile()
  : m_bGetBuff(false)
  , m_pBuff(nullptr)
  , m_nBuffSize(0)
  , m_hModule(nullptr)
{
}

CAi_GetResFile::~CAi_GetResFile()
{
  FreeBuff();
}

CString CAi_GetResFile::GetFile(WORD nIdRes, CString strUseItFileName)
{
  m_bGetBuff = true;
  if (!GetFile(nIdRes, _T("HELPFILE"), nullptr, nullptr, fcfByDef))
  {
    m_bGetBuff = false;
    ASSERT(false);
    return _T("");
  }
  m_bGetBuff = false;

  if (strUseItFileName.IsEmpty())
    return _T("");

  CString strFile = CAi_File::ExamineTmpFile(strUseItFileName);
  if (strFile.IsEmpty())
    return _T("");
  if (CAi_File::WriteBufferToFile(strFile, m_pBuff, m_nBuffSize))
    return strFile;
  return _T("");
}

bool CAi_GetResFile::GetFile(const WORD nIdRes, const CString strUserResType, const LPTSTR lpServiceType, 
  CString* pstrOutData, FileCodingFormat resFileCoding)
{
  ASSERT(IS_INTRESOURCE (nIdRes));
  if (!m_bGetBuff)
    ASSERT(pstrOutData);

  HRSRC hRes = nullptr;
  if (strUserResType.IsEmpty())
    hRes = FindResource(m_hModule, (LPCTSTR)MAKEINTRESOURCE(nIdRes), lpServiceType);
  else
    hRes = FindResource(m_hModule, MAKEINTRESOURCE(nIdRes), strUserResType);

  if(hRes)
  {
    HGLOBAL hGl = LoadResource(m_hModule, hRes);
    if(hGl) 
    {
      DWORD nResSize = SizeofResource(m_hModule, hRes);
      
      if (resFileCoding == fcfByDef)
      {
        #ifdef _UNICODE
        resFileCoding = fcfUnicode;
        #else
        resFileCoding = fcfASCII;
        #endif
      }

      if (m_bGetBuff)
      {
        m_nBuffSize = nResSize;
        BYTE* pBuff = (BYTE*)LockResource(hGl);
        m_pBuff = new BYTE[m_nBuffSize + 2];
        if (m_pBuff == nullptr)
          return false;
        ZeroMemory(m_pBuff, m_nBuffSize + 2);
        CopyMemory(m_pBuff, pBuff, m_nBuffSize);
        return true;
      }

      if (resFileCoding == fcfUnicode)
      {
        wchar_t* wszBuff = new wchar_t[nResSize];
        if (wszBuff == nullptr)
          return false;
        ZeroMemory(wszBuff, sizeof(wchar_t) * nResSize);
        wchar_t* pRes = (wchar_t*)LockResource(hGl);
        CopyMemory(wszBuff, (pRes + 1), nResSize - 1); // - 1 - becouse the first two simbols in unicode file is mark of unicode file: FF FE
        *pstrOutData = CString(wszBuff);
        delete wszBuff;
      }
      else
      {
        char* szBuff = new char[nResSize + 1];
        if (szBuff == nullptr)
          return false;
        ZeroMemory(szBuff, sizeof(char) * (nResSize + 1));
        CopyMemory(szBuff, (char*)LockResource(hGl), nResSize);
        *pstrOutData = CString(szBuff);
        delete szBuff;
      }
      return true;
    }
  }
  return false;
}

void CAi_GetResFile::FreeBuff()
{
  if (m_pBuff != nullptr)
  {
    delete m_pBuff;
    m_pBuff = nullptr;
    m_nBuffSize = 0;
  }
}
