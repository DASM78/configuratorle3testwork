#include "stdafx.h"
#include "Resource.h"
#include "Ai_Str.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const char* LOCALE_LANGUAGE = "Russian_Russia.1251";

namespace ns_CAi_Str
{
  bool g_bStringLikeHex = false;
} // namespace ns_CAi_Str

using namespace ns_CAi_Str;

CAi_Str::CAi_Str()
{
}

TCHAR CAi_Str::CharToLower(TCHAR ch)
{
  if (_istalpha(ch) && _istascii(ch))
    return _totlower(ch);
  switch(ch)
  {
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  case _T('�'): return _T('�');
  }
  return ch;
}

CString CAi_Str::StringToLower(CString str)
{
  CString sRes;
  const TCHAR * pCh = str;
  while (*pCh)
  {
    sRes += CharToLower(*pCh);
    ++pCh;
  }
  return sRes;
}

TCHAR* CAi_Str::ToChar(CString str, TCHAR** pSz)
{
  if (*pSz != nullptr)
    delete *pSz;

  size_t nCntConverted;
  int nBuffSize = str.GetLength();
  *pSz = new TCHAR[nBuffSize + 1];
  memset(*pSz, 0, nBuffSize + 1);
  
  #ifdef _UNICODE
  nCntConverted = (wcscpy_s(*pSz, nBuffSize + 1, str.GetBuffer()) == 0) ? 1 : 0;
  #else
  nCntConverted = (strcpy_s(*pSz, nBuffSize + 1, str.GetBuffer()) == 0) ? 1 : 0;
  #endif

  // Ok
  if (nCntConverted > 0)
    return *pSz;

  // Failure
  delete *pSz;
  *pSz = nullptr;
  return *pSz;
}

void CAi_Str::ToBuffers(CString& strSrc, wchar_t** lpwsDst, int nDstBuffersSize) throw(...)
{
  _locale_t l = _create_locale(LC_ALL, LOCALE_LANGUAGE);
  CAi_Str::ToBuffers_l(strSrc, lpwsDst, nDstBuffersSize, l);
  _free_locale(l);
}

void CAi_Str::ToBuffers_l(CString& strSrc, wchar_t** lpwsDst, int nDstBuffersSize, _locale_t l) throw(...)
{
  if (nDstBuffersSize <= 0)
    nDstBuffersSize = strSrc.GetLength() + 1;
  if (*lpwsDst == nullptr)
    *lpwsDst = new wchar_t[nDstBuffersSize];
  CAi_Str::ToBuffers_l(strSrc, *lpwsDst, nullptr, nDstBuffersSize, l);
}

void CAi_Str::ToBuffers(CString& strSrc, char** lpsDst, int nDstBuffersSize) throw(...)
{
  _locale_t l = _create_locale(LC_ALL, LOCALE_LANGUAGE);
  CAi_Str::ToBuffers_l(strSrc, lpsDst, nDstBuffersSize, l);
  _free_locale(l);
}

void CAi_Str::ToBuffers_l(CString& strSrc, char** lpsDst, int nDstBuffersSize, _locale_t l) throw(...)
{
  if (nDstBuffersSize <= 0)
    nDstBuffersSize = strSrc.GetLength() + 1;
  if (*lpsDst == nullptr)
    *lpsDst = new char[nDstBuffersSize];
  CAi_Str::ToBuffers_l(strSrc, nullptr, *lpsDst, nDstBuffersSize, l);
}

void CAi_Str::ToBuffers(CString& strSrc, wchar_t* lpwsDst, char* lpsDst, int nDstBuffersSize) throw(...)
{
  _locale_t l = _create_locale(LC_ALL, LOCALE_LANGUAGE);
  CAi_Str::ToBuffers_l(strSrc, lpwsDst, lpsDst, nDstBuffersSize, l);
  _free_locale(l);
}

void CAi_Str::ToBuffers_l(CString& strSrc, wchar_t* lpwsDst, char* lpsDst, int nDstBuffersSize, _locale_t l) throw(...)
{
  if (nDstBuffersSize <= 0)
    nDstBuffersSize = strSrc.GetLength() + 1;

  if (lpwsDst)
    ZeroMemory(lpwsDst, nDstBuffersSize * sizeof(wchar_t));
  if (lpsDst)
    ZeroMemory(lpsDst, nDstBuffersSize * sizeof(char));

  int nSrcSize = strSrc.GetLength();
  int nCopySize = (nSrcSize >= nDstBuffersSize) ? (nDstBuffersSize - 1) : nSrcSize;
  size_t resSize = 0;

  #ifdef _UNICODE

  if (lpwsDst)
  {
    if (wcscpy_s(lpwsDst, nCopySize + 1, strSrc.GetBuffer()) != 0)
      throw false;
  }
  if (lpsDst)
  {
    if (_wcstombs_s_l(&resSize, lpsDst, nDstBuffersSize, strSrc.GetBuffer(), _TRUNCATE/*nCopySize + 1*/, l) != 0)
      throw false;
  }

  #else

  if (lpwsDst)
  {
    if (_mbstowcs_s_l(&resSize, lpwsDst, nDstBuffersSize, strSrc.GetBuffer(), nCopySize + 1, l) != 0)
      throw false;
  }

  if (lpsDst)
  {
    if (_mbscpy_s((unsigned char*)lpsDst, nCopySize + 1, (const unsigned char*)strSrc.GetBuffer()) != 0)
      throw false;
  }
 
  #endif
}

int CAi_Str::ToInt_sHex(CString sHex)
{
  g_bStringLikeHex = true;
  return ToInt(sHex);
}

int CAi_Str::ToInt(CString str)
{
  int nRes = 0;

  if (!str.IsEmpty())
  {
    try
    {
#ifdef _UNICODE

      // �������� �� ������ ����������� �����

      wchar_t* ch = str.GetBuffer();
      int nL = str.GetLength();
      wchar_t* ch2 = new wchar_t[nL + 1];
      ZeroMemory(ch2, sizeof(wchar_t) * (nL + 1));
      for (int i = 0, j = 0; i < nL; ++i)
      {
        if ((int)*(ch + i) == 160)
          continue;
        *(ch2 + j) = *(ch + i);
        ++j;
      }
      CString str1 = (ch2);
      if (str1.IsEmpty())
        return 0;

      if (g_bStringLikeHex)
        swscanf_s(str1.GetBuffer(), _T("%x"), &nRes);
      else
        swscanf_s(str1.GetBuffer(), _T("%d"), &nRes);

      delete ch2;

#else

      // �������� �� ������ ����������� �����

      char* ch = str.GetBuffer();
      int nL = str.GetLength();
      char* ch2 = new char[nL + 1];
      ZeroMemory(ch2, sizeof(char) * (nL + 1));
      for (int i = 0, j = 0; i < nL; ++i)
      {
        if ((int)*(ch + i) == -96)
          continue;
        *(ch2 + j) = *(ch + i);
        ++j;
      }
      CString str1(ch2);
      if (str1.IsEmpty())
        return 0;

      if (g_bStringLikeHex)
        sscanf_s(str1.GetBuffer(), "%d", &nRes);
      else
        sscanf_s(str1.GetBuffer(), "%x", &nRes);

      delete ch2;
#endif
    }
    catch(...)
    {
      nRes = 0;
    }
  }

  g_bStringLikeHex = false;

  return nRes;
}

double CAi_Str::ToFloat(CString str)
{
  double f = 0.0;
  if (!str.IsEmpty())
  {
    try
    {
      #ifdef _UNICODE
      f = _wtof(str);
      //swscanf_s(str, L"%f", &f);
      #else
      f = atof(str);
      //sscanf_s(str.GetBuffer(), "%f", &f);
      #endif
    }
    catch(...)
    {
      f = 0.0;
    }
  }

  return f;
}

int CAi_Str::GetPrecision(CString str)
{
  int i = str.Find(_T("."));

  if (i != -1)
    i = (str.GetLength() - 1) - i;
  
  return i;
}

LONG CAi_Str::To16x16(CString str)
{
  float f = 0;
  LONG i = 0;

  if (!str.IsEmpty())
  {
    try
    {
      #ifdef _UNICODE
      swscanf_s(str.GetBuffer(), _T("%f"), &f);
      #else
      sscanf_s(str.GetBuffer(), "%f", &f);
      #endif
    }
    catch(...)
    {
      f = 0;
    }
    i = To16x16(f);
  }
  return i;
}

LONG CAi_Str::To16x16(float f)
{
  return (LONG)(f * 65536);
}

CString CAi_Str::From16x16(LONG i, int nPrecision)
{
  float f = 0;

  if (i != 0)
    f = (float)(i / (float)65536);
  return ToString(f, nPrecision);
}

UINT CAi_Str::GetIntFromBinStr(CString strBinStr)
{
  UINT nMeaning = 0;
  strBinStr.Remove(' ');
  if (!strBinStr.IsEmpty())
  {
    for (int i = 0; i < strBinStr.GetLength(); ++i)
    {
      CString strM(strBinStr.GetAt(i));
      nMeaning = (nMeaning << 1) | ((strM == L"1") ? 1 : 0);
    }
  }
  return nMeaning;
}

CString CAi_Str::ToString(int i)
{
  CString strTmp;

  strTmp.Format(_T("%d"), i);
  return strTmp;
}


CString CAi_Str::ToString(unsigned int i)
{
  CString strTmp;

  strTmp.Format(_T("%u"), i);
  return strTmp;
}

CString CAi_Str::ToString(ULONG i)
{
  CString strTmp;

  strTmp.Format(_T("%u"), i);
  return strTmp;
}

CString CAi_Str::ToString(float f, int nPrecision)
{
  return ToString(static_cast<double>(f), nPrecision);
}

CString CAi_Str::ToString(double f, int nPrecision)
{
  return ToString(f, nPrecision, fsrJustZero);
  /*
  CString strDeterminative;
  CString strTmp;

  if (nPrecision < 0)
    return _T("");

  strDeterminative.Format(_T(".%df"), nPrecision);
  strDeterminative = _T("%") + strDeterminative;
  strTmp.Format(strDeterminative, f);
  if (strTmp.Find(_T(".")) != -1)
  {
    strTmp.TrimRight(_T("0"));
    if (strTmp.Right(1) == _T("."))
      strTmp += _T("0");
  }
  else
    strTmp += _T(".0");
  if ( CAi_Str::ToFloat(strTmp) == 0.0)
    strTmp = _T("0");
  return strTmp;*/
}

CString CAi_Str::ToString(double f, int nPrecision, EFloatStringResult0 fsr)
{
  CString strDeterminative;
  CString strTmp;

  if (nPrecision < 0)
    return _T("");

  strDeterminative.Format(_T(".%df"), nPrecision);
  strDeterminative = _T("%") + strDeterminative;
  strTmp.Format(strDeterminative, f);

  if (fsr == fsrFull)
    return strTmp;

  if (strTmp.Find(_T(".")) != -1)
  {
    strTmp.TrimRight(_T("0"));
    if (strTmp.Right(1) == _T("."))
      strTmp += _T("0");
  }
  else
    strTmp += _T(".0");

  if (fsr == fsrZeroPointZero)
    return strTmp;

  if (CAi_Str::ToFloat(strTmp) == 0.0)
    strTmp = _T("0");

  ASSERT(fsr == fsrJustZero);

  return strTmp;
}

BYTE CAi_Str::FromOctal(BYTE i)
{
  BYTE nTens = ((i >> 4) & 0x0F) * 10;
  return nTens + (i & 0x0F);
}

CString CAi_Str::AddFloatZeroEndIfNeed(CString str)
{
  try
  {
    int nPointPos = str.Find('.');
    if (nPointPos == -1)
    {
      str += _T(".0");
      throw true;
    }

    CString strDecPart = str.Right(str.GetLength() - (nPointPos + 1));
    if (strDecPart.IsEmpty())
    {
      str += _T("0");
      throw true;
    }
     
    strDecPart.TrimRight(_T("0"));
    if (strDecPart.IsEmpty())
    {
      str.TrimRight(_T("0"));
      str += _T("0");
    }
    throw true;
  }
  catch (bool b)
  {
    b = b;
    str.TrimLeft(_T("0"));
    if (str.Find('.') == 0)
      str = _T("0") + str;
  }
  return str;
}

bool CAi_Str::CutString(CString strSrc, CString strSeparator, vector <CString> *pCutStrList, CString strEmptyParamMark)
{
  if (pCutStrList == nullptr)
    return false;

  pCutStrList->clear();

  if (strSrc.IsEmpty())
    return false;
  if (strSeparator.IsEmpty())
    return false;

  if (strSrc.Left(strSeparator.GetLength()) == strSeparator)
    strSrc = L" " + strSrc;
  if (strSrc.Right(strSeparator.GetLength()) == strSeparator)
    strSrc += L" ";

  CString strResToken;
  int nCurPos = 0;

  strResToken = strSrc.Tokenize(strSeparator, nCurPos);
  while (strResToken != _T(""))
  {
    strResToken.TrimLeft(_T(" "));
    strResToken.TrimLeft(_T("\t"));
    strResToken.TrimRight(_T(" "));
    strResToken.TrimRight(_T("\t"));
    if (strResToken == strEmptyParamMark)
      strResToken = _T("");
    pCutStrList->push_back(strResToken);
    strResToken = strSrc.Tokenize(strSeparator, nCurPos);
  }

  return true;
}

bool CAi_Str::CutString(CString strSrc, CString strSeparator, vector <int> *pCutIntList, CString strEmptyParamMark)
{
  if (pCutIntList == nullptr)
    return false;
  pCutIntList->clear();

  vector<CString> str;
  int nCnt;
  int nRes;

  if (!CutString(strSrc, strSeparator, &str, strEmptyParamMark))
    return false;
  nCnt = (int)str.size();
  if (nCnt == 0)
    return false;
  for (int i = 0; i < nCnt; ++i)
  {
    nRes = ToInt(str[i]);
    pCutIntList->push_back(nRes);
  }
  return true;
}

// Take string in quote: aaa => "aaa"; "bbb" => "bbb"
CString CAi_Str::TakeStringInQuotes(CString *strText)
{
  CString strQuote = _T("\"");

  if (strText->Left(1) != strQuote)
    strText->Insert(0, strQuote);
  if (strText->Right(1) != strQuote)
    *strText += strQuote;

  return *strText;
}

int CAi_Str::AddIfNo(vector<CString>* pLst, CString strAdd)
{
  if (pLst == nullptr)
    return -1;

  bool bIs = false;

  for (int i = 0; i < (int)pLst->size(); ++i)
    if (pLst->at(i) == strAdd)
    {
      bIs = true;
      break;
    }

  if (!bIs)
  {
    pLst->push_back(strAdd);
    return 1;
  }

  return 0;
}

void CAi_Str::StringToIP(CString strIP, BYTE &nIP0, BYTE &nIP1, BYTE &nIP2, BYTE &nIP3)
{
  CString strResToken;
  int nCurPos = 0;
  int i = 0;
  int nIP;

  nIP0 = nIP1 = nIP2 = nIP3 = 0;
  if (strIP.IsEmpty())
    return;

  strResToken = strIP.Tokenize(_T("."), nCurPos);
  while (strResToken != "" || i < 4)
  {
    strResToken.TrimLeft(_T(" "));
    strResToken.TrimRight(_T(" "));
    if (strResToken.IsEmpty())
      strResToken = _T("0");
    nIP = CAi_Str::ToInt(strResToken);
    switch (i++)
    {
      case 0: nIP0 = (BYTE)nIP; break;
      case 1: nIP1 = (BYTE)nIP; break;
      case 2: nIP2 = (BYTE)nIP; break;
      case 3: nIP3 = (BYTE)nIP; break;
      default: break;
    };
    strResToken = strIP.Tokenize(_T("."), nCurPos);
  }
}

DWORD CAi_Str::StringToIP(CString strIP)
{
  DWORD nIP;
  BYTE nIP0 = 0, nIP1 = 0, nIP2 = 0, nIP3 = 0;
  StringToIP(strIP, nIP0, nIP1, nIP2, nIP3);
  nIP = 0;
  nIP = nIP0 << 24;
  nIP |= nIP1 << 16;
  nIP |= nIP2 << 8;
  nIP |= nIP3;
  return nIP;
}

CString CAi_Str::IPToString(BYTE nIP0, BYTE nIP1, BYTE nIP2, BYTE nIP3)
{
  CString strIP;
  strIP.Format(_T("%d.%d.%d.%d"), nIP0, nIP1, nIP2, nIP3);
  return strIP;
}

CString CAi_Str::IPToString(DWORD nIP)
{
  return IPToString((BYTE)(nIP >> 24 & 0xFF), (BYTE)(nIP >> 16 & 0xFF), (BYTE)(nIP >> 8 & 0xFF), (BYTE)(nIP & 0xFF));
}

CString CAi_Str::FlagToYesNo(int nF)
{
  CString sY;
  sY.LoadString(IDS_YES); // ��
  CString sN;
  sN.LoadString(IDS_NO); // ���
  return nF == 0 ? sN : sY;
}
