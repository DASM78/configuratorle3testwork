#include "StdAfx.h"
#include "Ai_Bmp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_Bmp::CAi_Bmp(void)
  : m_pBmp(nullptr)
  , m_hBmp(nullptr)
  , m_bmpBuffer(nullptr)
{
}

CAi_Bmp::~CAi_Bmp(void)
{
  if (m_bmpBuffer)
  {
    //delete m_bmpBuffer;
    m_bmpBuffer = nullptr;
  }
}

bool CAi_Bmp::GetBitmapSize(DWORD nResourceID, SIZE *pBMPSize)
{
  ASSERT(nResourceID > 0);
  ASSERT(pBMPSize);
  if (nResourceID < 0
    || pBMPSize == nullptr)
    return false;

  CBitmap bmp;
  if (bmp.LoadBitmap(nResourceID))
  {
    *pBMPSize = bmp.GetBitmapDimension();
    if (pBMPSize->cx == 0 && pBMPSize->cy == 0)
    {
      BITMAP bmpInfo;
      bmp.GetBitmap(&bmpInfo);
      pBMPSize->cx = bmpInfo.bmWidth;
      pBMPSize->cy = bmpInfo.bmHeight;
    }
    return true;
  }
  return false;
}

bool CAi_Bmp::GetBitmapSize(CString strFileName, SIZE *pBMPSize)
{
  ASSERT(pBMPSize);
  if (pBMPSize == nullptr)
    return false;

  pBMPSize->cx = 0;
  pBMPSize->cy = 0;

  if (strFileName.IsEmpty())
    return false;

  HBITMAP hbit;
  BITMAP bm;

  hbit = (HBITMAP)LoadImage(AfxGetInstanceHandle(),
                 strFileName,
                 IMAGE_BITMAP,
                 0,
                 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE | LR_DEFAULTCOLOR);

  if (!hbit)
    return false;

  GetObject(hbit, sizeof(BITMAP), (LPVOID)&bm);
  pBMPSize->cx = bm.bmWidth;
  pBMPSize->cy = bm.bmHeight;
  DeleteObject(hbit);
  return true;
}

void CAi_Bmp::SetBMP(CBitmap* pBmp)
{
   m_hBmp = (HBITMAP)pBmp;
   pBmp->GetBitmap(&m_bm);
   int nSize = m_bm.bmWidthBytes * m_bm.bmHeight;
   if (m_bmpBuffer)
   {
     delete m_bmpBuffer;
     m_bmpBuffer = nullptr;
   }
   m_bmpBuffer = (BYTE*)GlobalAlloc(GPTR, nSize); //allocate memory for image
   ZeroMemory(m_bmpBuffer, nSize);
   DWORD dwValue = pBmp->GetBitmapBits(m_bm.bmWidthBytes * m_bm.bmHeight, m_bmpBuffer);//Get the bitmap bits 
   dwValue = dwValue;
   m_bm.bmBits = m_bmpBuffer;
}

// pSpacePos - left top
bool CAi_Bmp::MoveOutBMPFileToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos, CString strFileName)
{
  ASSERT(hDC);
  ASSERT(pSpaceSize);
  ASSERT(pSpacePos);
  ASSERT(strFileName.IsEmpty());
  if (hDC == nullptr
    ||pSpaceSize == nullptr
    || pSpacePos == nullptr
    || strFileName.IsEmpty())
    return false;

  // This code shows how to print a resource bitmap or a .bmp file using StretchDIBits.
  // The bitmap must not be compressed (BI_RGB compression type, check MSDN).

  HBITMAP hbit;           // Handle to the bitmap to print
  // The following line loads the bitmap from resource bitmap
  hbit = (HBITMAP) LoadImage(AfxGetInstanceHandle(),
                 strFileName,
                 IMAGE_BITMAP,
                 0,
                 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE | LR_DEFAULTCOLOR);

  if (!hbit)
    return false;
  ASSERT(false);
  //!!bool bRes = MoveOutBMPToDC(hDC, pSpaceSize, pSpacePos, (CBitmap*)hbit);
  DeleteObject(hbit);
  //return bRes;
  return false;
}

// pSpaceSize - cx, cy
// pSpacePos - left top
// pSpacePos == nullptr -> pSpacePos->x = 0; pSpacePos->y = pSpaceSize->cy
bool CAi_Bmp::MoveOutBMPToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos)
{
  ASSERT(hDC);
  ASSERT(pSpaceSize);
  if (hDC == nullptr
    ||pSpaceSize == nullptr)
    return false;

  CPoint pos = pSpacePos == nullptr ? CPoint(0, pSpaceSize->cy) : *pSpacePos;

  ASSERT(m_bmpBuffer);
  if (m_bmpBuffer == nullptr)
    return false;
  return MoveOutBMPToDC(hDC, pSpaceSize, &pos, m_hBmp, &m_bm);
}

// pSpacePos - left top
bool CAi_Bmp::MoveOutBMPToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos, HBITMAP hBm, BITMAP* pBm)
{
  ASSERT(hDC);
  ASSERT(pSpaceSize);
  ASSERT(pSpacePos);
  ASSERT(pBm);
  ASSERT(hBm);
  if (hDC == nullptr
    ||pSpaceSize == nullptr
    || pSpacePos == nullptr
    || pBm == nullptr
    || hBm == nullptr)
    return false;

  pSpaceSize->cy *= -1;

  int nColors  = 0;   // Used to store the number of colors the DIB has
  nColors = (1 << pBm->bmBitsPixel);
  if(nColors > 256)
    nColors = 0;           // This is when DIB is 24 bit.
               // In this case there is not any color table information

  RGBQUAD rgb[256] = {0};       // Used to store the DIB color table
  LPBITMAPINFO info;           // Structure for storing the DIB information,
                 // it will be used by 'StretchDIBits()'
  int sizeinfo = 0;   // Will contain the size of info

  // Now we need to know how much size we have to give for storing "info" in memory.
  // This involves the proper BITMAPINFO size and the color table size.
  // Color table is only needed when the DIB has 256 colors or less.
  sizeinfo = sizeof(BITMAPINFO) + (nColors * sizeof(RGBQUAD));   // This is the size required

  info = (LPBITMAPINFO) malloc(sizeinfo);                        // Storing info in memory

  // Before 'StretchDIBits()' we have to fill some "info" fields.
  // This information was stored in 'bm'.
  info->bmiHeader.biSize          = sizeof(BITMAPINFOHEADER);
  info->bmiHeader.biWidth         = pBm->bmWidth;
  info->bmiHeader.biHeight        = pBm->bmHeight;
  info->bmiHeader.biPlanes        = 1;
  info->bmiHeader.biBitCount      = pBm->bmBitsPixel * pBm->bmPlanes;
  info->bmiHeader.biCompression   = BI_RGB;
  info->bmiHeader.biSizeImage     = pBm->bmWidthBytes * pBm->bmHeight;
  info->bmiHeader.biXPelsPerMeter = 0;
  info->bmiHeader.biYPelsPerMeter = 0;
  info->bmiHeader.biClrUsed       = 0;
  info->bmiHeader.biClrImportant  = 0;

  // If the bitmap is a compressed bitmap (BI_RLE for example), the 'biSizeImage' can not
  // be calculated that way. A call to 'GetDIBits()' will fill the 'biSizeImage' field with
  // the correct size.

  // Now for 256 or less color DIB we have to fill the "info" color table parameter
  if(nColors <= 256)
  {
    HBITMAP hOldBitmap;
    HDC hMemDC = CreateCompatibleDC(nullptr);      // Creating an auxiliary device context

    hOldBitmap = (HBITMAP) SelectObject(hMemDC, hBm);  // Select 'hbit' in 'it'
    GetDIBColorTable(hMemDC, 0, nColors, rgb);          // Obtaining the color table information

    // Now we pass this color information to "info"
    for(int iCnt = 0; iCnt < nColors; ++iCnt)
    {
      info->bmiColors[iCnt].rgbRed   = rgb[iCnt].rgbRed;
      info->bmiColors[iCnt].rgbGreen = rgb[iCnt].rgbGreen;
      info->bmiColors[iCnt].rgbBlue  = rgb[iCnt].rgbBlue;
    }
        
    SelectObject(hMemDC, hOldBitmap);
    DeleteDC(hMemDC);
  }

  // Let's get 'StretchDIBiting'! 'pDC' is the printer DC
  //HDC hdc = pPrintDC->GetSafeHdc();   // Getting a safe handle

  // Stretching all the bitmap on a destination rectangle of size (size_x, size_y)
  // and upper left corner at (initial_pos_x, initial_pos_y)
  StretchDIBits(
          hDC,
          pSpacePos->x, // XDst
          pSpacePos->y, // YDst
          pSpaceSize->cx, // W Dst
          pSpaceSize->cy, // H Dst
          0, // XSrc
          0, // YSrc
          pBm->bmWidth, // nSrcWidth
          pBm->bmHeight, // nSrcHeight
          pBm->bmBits,
          info,
          DIB_RGB_COLORS,
          SRCCOPY);

  // This mode, DIB_RGB_COLORS indicate the color table are pure RGB values
  // The mode DIB_PAL_COLORS indicate the color table items are index to the local palette items

  free(info);

  return true;
}

bool CAi_Bmp::SaveBitmapInFile(HWND hWnd, HBITMAP HBM, CString strFileName)
{
  ASSERT(hWnd);
  ASSERT(HBM);
  ASSERT(strFileName.IsEmpty());
  if (hWnd == nullptr
    ||HBM == nullptr
    || strFileName.IsEmpty())
    return false;

  BITMAP BM; 
  BITMAPFILEHEADER BFH; 
  LPBITMAPINFO BIP; 
  HDC DC; 
  LPBYTE Buf; 
  DWORD ColorSize = 0, DataSize; 
  WORD BitCount; 
  HANDLE FP; 
  DWORD dwTemp; 

  GetObject(HBM, sizeof(BITMAP), (LPSTR)&BM); 

  BitCount = (WORD)BM.bmPlanes * BM.bmBitsPixel; 
  switch (BitCount) 
  { 
    case 1: 
    case 4: 
    case 8: 
    case 32: 
    ColorSize = sizeof(RGBQUAD) * (1 << BitCount); 
    case 24: 
    ColorSize = 0; 
  } 

  DataSize = ((BM.bmWidth*BitCount+31) & ~31)/8*BM.bmHeight; 

  BIP = (LPBITMAPINFO)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,sizeof(BITMAPINFO) + ColorSize); 

  BIP->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
  BIP->bmiHeader.biWidth = BM.bmWidth; 
  BIP->bmiHeader.biHeight = BM.bmHeight; 
  BIP->bmiHeader.biPlanes = 1; 
  BIP->bmiHeader.biBitCount = BitCount; 
  BIP->bmiHeader.biCompression = 0; 
  BIP->bmiHeader.biSizeImage = DataSize; 
  BIP->bmiHeader.biXPelsPerMeter = 0; 
  BIP->bmiHeader.biYPelsPerMeter = 0; 
  if (BitCount < 24) 
  BIP->bmiHeader.biClrUsed = (1<<BitCount); 
  BIP->bmiHeader.biClrImportant = 0; 

  BFH.bfType = 0x4d42; 
  BFH.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+ BIP->bmiHeader.biClrUsed * sizeof(RGBQUAD); 
  BFH.bfReserved1 = 0; 
  BFH.bfReserved2 = 0; 
  BFH.bfSize = BFH.bfOffBits + DataSize; 

  Buf = (LPBYTE)GlobalAlloc(GMEM_FIXED, DataSize); 

  DC = GetDC(hWnd);//->m_hDC; 
  GetDIBits(DC, HBM, 0,(WORD)BM.bmHeight, Buf, BIP, DIB_RGB_COLORS); 
  ReleaseDC(hWnd, GetDC(hWnd)); 

  FP = CreateFile(strFileName,GENERIC_READ | GENERIC_WRITE, 0, nullptr, 
  CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,nullptr); 
  WriteFile(FP,&BFH,sizeof(BITMAPFILEHEADER),&dwTemp,nullptr); 
  WriteFile(FP,BIP,sizeof(BITMAPINFOHEADER) + BIP->bmiHeader.biClrUsed * sizeof(RGBQUAD),&dwTemp,nullptr); 
  WriteFile(FP,Buf,DataSize,&dwTemp,nullptr); 
  FlushFileBuffers(FP);  

  CloseHandle(FP); 
  GlobalFree((HGLOBAL)Buf); 
  HeapFree(GetProcessHeap(),0,(LPVOID)BIP); 

  return true;
}