#include "stdafx.h"
#include <sys/timeb.h>
#include <time.h>
#include "Ai_Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define USE_AI_LOG

#ifdef USE_AI_LOG
#define WRITE_LOG_FILE
#endif

CFile CAi_Log::m_hFile;
CString CAi_Log::m_strFile;

CAi_Log::CAi_Log()
{
  #ifdef WRITE_LOG_FILE
  if (!m_strFile.IsEmpty())
    SetPathToLoggingFile(L"");
  #endif
}

CAi_Log::~CAi_Log()
{
}

bool CAi_Log::OpenFile()
{
  #ifdef WRITE_LOG_FILE
  if (m_strFile.IsEmpty())
    SetPathToLoggingFile(L"");

  if (m_hFile.Open(m_strFile, CFile::modeWrite) == FALSE)
  {
    if (m_hFile.Open(m_strFile, CFile::modeCreate | CFile::modeWrite) == TRUE)
      return true;
  }
  else
  {
    m_hFile.SeekToEnd();
    return true;
  }
  #endif

  return false;
}

void CAi_Log::CloseFile()
{
  #ifdef WRITE_LOG_FILE
  m_hFile.Flush();
  m_hFile.Close();
  #endif
}

void CAi_Log::SetPathToLoggingFile(CString strPath)
{
  if (!strPath.IsEmpty())
  {
    strPath.TrimRight(L"\\");
    m_strFile = strPath + IDS_Ai_Log_FILE_NAME;
  }
  else
    m_strFile = IDS_Ai_Log_OPEN_FILE;
}

bool CAi_Log::ClearFile()
{
  #ifdef WRITE_LOG_FILE
  if (OpenFile())
  {
    m_hFile.SetLength(0);
    CloseFile();
    return true;
  }
  #endif

  return false;
}

void CAi_Log::AddStr(CString str)
{
  #ifdef WRITE_LOG_FILE
  if (OpenFile())
  {
    #ifdef _UNICODE
    wchar_t szEndl[] = L"\r\n";
    #else
    char szEndl[] = "\r\n";
    #endif

    #ifdef _UNICODE
    m_hFile.Write(str, wcslen(str) * 2);
    m_hFile.Write(szEndl, wcslen(szEndl) * 2);
    #else
    m_hFile.Write(str, strlen(str));
    m_hFile.Write(szEndl, strlen(szEndl));
    #endif

    CloseFile();
  }
  #endif
}

void CAi_Log::AddStr(int n)
{
  n = n;
  #ifdef WRITE_LOG_FILE
  CString strIn;

  #ifdef _UNICODE
  strIn.Format(L"%d", n);
  #else
  strIn.Format("%d", n);
  #endif
  AddStr(strIn);
  #endif
}

void CAi_Log::AddStr(CString str, int n)
{
  n = n;
  #ifdef WRITE_LOG_FILE
  CString strIn;

  #ifdef _UNICODE
  strIn.Format(L"%s %d", str, n);
  #else
  strIn.Format("%s %d", str, n);
  #endif
  AddStr(strIn);
  #endif
}

void CAi_Log::AddStr(int n, CString str)
{
  n = n;
  #ifdef WRITE_LOG_FILE
  CString strIn;

  #ifdef _UNICODE
  strIn.Format(L"%d s%", n, str);
  #else
  strIn.Format("%d s%", n, str);
  #endif
  AddStr(strIn);
  #endif
}

void CAi_Log::MyTRACE(CString str)
{
  #ifdef WRITE_LOG_FILE
  AddStr(str);
  #endif
  //TRACE(str);
}

void CAi_Log::MyTRACE_tm(CString str)
{
#ifdef WRITE_LOG_FILE
  __timeb32 tbForBlock;
  _ftime32_s(&tbForBlock);
  CTime currTime(tbForBlock.time);
  CString sTime;
  sTime.Format(L"%s.%hu; ", currTime.Format(_T("%H:%M:%S")), tbForBlock.millitm);

  AddStr(sTime + str);
#endif
  //TRACE(str);
}