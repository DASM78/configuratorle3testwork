#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_BCC
{
public:
  CAi_BCC();

  static unsigned char CalcBCC(unsigned char* pData, int nDataLen);
};