#include "StdAfx.h"
#include "Resource.h"
#include "Ai_RunApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef IGNORE_IMPORT_EXPORT_ADDINS_OBJS
#define IDS_2_ERROR_RUN_APP_ L"Error when the application starts: %s"
#endif

BOOL CAi_RunApp::RunApplication(CString strPath, CString strParams, BOOL bWaitApp, WORD swShow, DWORD* pnExitCode, CString* pstrEnv, CString* pstrCurrDir)
{
  STARTUPINFO si;
  PROCESS_INFORMATION pi;
  DWORD dwCreationFlags = NORMAL_PRIORITY_CLASS;

  memset(&si,0,sizeof(STARTUPINFO));
  si.cb = sizeof(si);
  si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
  si.wShowWindow = swShow;//SW_SHOW;

  TCHAR* pszPath = nullptr;
  TCHAR* pszArgs = nullptr;
  void* pvEnv = (void*)pstrEnv;
  TCHAR* pszCurrDir = nullptr;

  if (!strPath.IsEmpty())
    pszPath = strPath.GetBuffer();
  if (!strParams.IsEmpty())
    pszArgs = strParams.GetBuffer();
  if (pstrCurrDir != nullptr)
    pszCurrDir = pstrCurrDir->GetBuffer();

  if (CreateProcess(pszPath, pszArgs, nullptr, nullptr, TRUE, dwCreationFlags, pvEnv, pszCurrDir, &si, &pi))
  {
    CloseHandle(pi.hThread);
    if(bWaitApp)
      if (WaitForSingleObject(pi.hProcess, INFINITE) != WAIT_FAILED)
      {
        if (pnExitCode != nullptr)
          GetExitCodeProcess(pi.hProcess, pnExitCode);
      }
    CloseHandle(pi.hProcess);
    return TRUE;
  }
  
  CString strMsg;
  CString strF;
  #ifdef IGNORE_IMPORT_EXPORT_ADDINS_OBJS
    strF = IDS_2_ERROR_RUN_APP_;
  #else
  strF.LoadString(IDS_ERROR_RUN_APP); // ������ ������� ����������!\n[%s]
  #endif
  strMsg.Format(strF, strParams);
  AfxMessageBox(strMsg, MB_ICONERROR);

  return FALSE;
}

bool CAi_RunApp::RunApplication2(CString strCmdLine, bool bShow)
{
  STARTUPINFO si = {0};
  si.cb = sizeof(si);
  si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
  si.wShowWindow = bShow ? SW_SHOW : SW_HIDE;
  PROCESS_INFORMATION  pi = {0};
  DWORD dwCreationFlags = NORMAL_PRIORITY_CLASS;
  if (CreateProcess(nullptr, strCmdLine.GetBuffer(), 0, 0, FALSE, dwCreationFlags, 0, 0, &si, &pi))
  {
    CloseHandle(pi.hThread);
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hProcess);
    return true;
  }
  return false;
}