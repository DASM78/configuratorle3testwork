#pragma once

#include <locale.h> 
#include <vector>
#include "AddinsDef.h"

class ADDINS_API CAi_Str
{
public:
  CAi_Str();

  static TCHAR CharToLower(TCHAR ch);
  static CString StringToLower(CString str);

  static TCHAR* ToChar(CString str, TCHAR** pSz);

  /*
    if nDstBuffersSize == 0 => nDstBuffersSize = strSrc.GetLength() + 1;
    if lpwsDst == nullptr => memory for this buffer will be get from the heap
  */
  static void ToBuffers(CString& strSrc, wchar_t** lpwsDst, int nDstBuffersSize = 0) throw(...); // here use RUS locale
  static void ToBuffers_l(CString& strSrc, wchar_t** lpwsDst, int nDstBuffersSize, _locale_t l) throw(...);
  static void ToBuffers(CString& strSrc, char** lpsDst, int nDstBuffersSize = 0) throw(...);  // here use RUS locale
  static void ToBuffers_l(CString& strSrc, char** lpsDst, int nDstBuffersSize, _locale_t l) throw(...);
  /*
    if nDstBuffersSize == 0 => nDstBuffersSize = strSrc.GetLength() + 1;
    if nDstBuffersSize == strSrc.GetLength() => will copy strSrc.GetLength() - 1 simbols and last lpDst[last] simbol will has '\0'
    if lpwsDst || lpsDst == nullptr => data in this buffer will not been to copy
  */
  static void ToBuffers(CString& strSrc, wchar_t* lpwsDst, char* lpsDst, int nDstBuffersSize = 0) throw(...);  // here use RUS locale
  static void ToBuffers_l(CString& strSrc, wchar_t* lpwsDst, char* lpsDst, int nDstBuffersSize, _locale_t l) throw(...);

public:
  
  static int ToInt(CString str);
  static int ToInt_sHex(CString sHex);
  static double ToFloat(CString str);
  static int GetPrecision(CString str);
  static CString ToString(int i);
  static CString ToString(unsigned int i);
  static CString ToString(ULONG i);

  // ���� �����, �������� f = 0.0, � nPrecision = 2, �� �������� 0
  static CString ToString(float f, int nPrecision = 4);
  static CString ToString(double f, int nPrecision = 4);

  enum EFloatStringResult0
  {
    fsrJustZero, // 0
    fsrZeroPointZero, // 0.0
    fsrFull // 0.00 ���� nPrecision = 2
  };
  // ���� �����, �������� f = 0.0, � nPrecision = 2, �� �������� �������� fsr
  static CString ToString(double f, int nPrecision, EFloatStringResult0 fsr);

  static LONG To16x16(CString str);
  static LONG To16x16(float f);
  static CString From16x16(LONG i, int nPrecision = 4);
  static UINT GetIntFromBinStr(CString strBinStr);

  // ����������� � ����������
  static BYTE FromOctal(BYTE i);
  static CString AddFloatZeroEndIfNeed(CString str);

  // Cut src string by tokenize

  static bool CutString(CString strSrc, CString strSeparator, std::vector <CString> *pCutStrList, CString strEmptyParamMark = _T("~"));
  static bool CutString(CString strSrc, CString strSeparator, std::vector <int> *pCutIntList, CString strEmptyParamMark = _T("~"));

  // Take string in quote: aaa => "aaa"; "bbb" => "bbb"

  static CString TakeStringInQuotes(CString *strText);
  
  // Work with vector<CString>
  static int AddIfNo(std::vector<CString>* pLst, CString strAdd);

  // IP

  static void StringToIP(CString strIP, BYTE &nIP0, BYTE &nIP1, BYTE &nIP2, BYTE &nIP3);
  static DWORD StringToIP(CString strIP);

  static CString IPToString(BYTE nIP0, BYTE nIP1, BYTE nIP2, BYTE nIP3);
  static CString IPToString(DWORD nIP);

  // Flag

  static CString FlagToYesNo(int nF);
};
