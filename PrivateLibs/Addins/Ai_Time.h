#pragma once

#include <vector>
#include "AddinsDef.h"

#define TIMER_MARK_FILE "StorageOfTimerMarks.ini"

struct ADDINS_API TIMER_MARK
{
  int nID;
  __time64_t tTime;
};

/*
����� ��������������� ����������� ��������� ������� �� �������

���������� �������:
nID - ��������� ������� ������������� � ������ ID, �.�. ����� ������� ����� �������� ��������� �������� �� ������ ��������
bUpdateCurrentTimeMark - ����� ��������� ���������� ��������� �������� ��������� ������� ������� ��������
bRoundUp - ��������� �����������, �.�. ���� ����������� < 500, �� + 0 ������, ���� > 500, �� + 1 �������
tmrResult - � ����� ������� ����������� ���������
bFromFile - ������� � ��������� ��������� ������� �� ������ ID � ����� �� �����
strFileName - ��� �����, ��� ������� ��������� ������� �� ������ ID
*/

class ADDINS_API CAi_Time
{
public:
  CAi_Time(void);
  // ����� ���������� ������� ����� ��� ������ ��������������� (nID)
  CAi_Time(int nID);
  ~CAi_Time(void);

  enum TimerMarkResult
  {
    tmrNone,
    tmrGetByMilliseconds,
    tmrGetBySeconds,
    tmrGetByMinutes,
    tmrGetByHours
  };

  static void SaveCurrentTime_(int nID);
  void SaveCurrentTime(int nID, bool bSaveToFile = false, CString strFileName = _T(""));

  static __int64 GetTimePeriod_(int nID, TimerMarkResult tmrResult, bool bUpdateCurrentTimeMark = false, bool bRoundUp = false);
  __int64 GetTimePeriod(int nID, TimerMarkResult tmrResult, bool bUpdateCurrentTimeMark = false, bool bRoundUp = false, bool bFromFile = false, CString strFileName = _T(""));
  static __int64 GetSavedTime_(int nID, TimerMarkResult tmrResult = CAi_Time::tmrGetByMilliseconds);
  __int64 GetSavedTime(int nID, TimerMarkResult tmrResult = CAi_Time::tmrGetByMilliseconds, bool bFromFile = false, CString strFileName = _T(""));
  
  static void FreeID_(int nID);

  __int64 m_nMillisecondsTail = 0;
  __int64 m_nSecondsTail = 0;
  __int64 m_nMinitesTail = 0;

  __int64 GetCurrentTimeByMilliseconds();
  
  enum TimerStringResult
  {
    tsrD_M_Y_H_M_S_Ms,
    tsrH_M_S_Ms,
    tsrH_M_S,
    tsrH2_M2_S2_Ms // ����, ��� ���. ��; ���� ���� ����� ��� + ���; ���� ���� ����� ��� + ���; ���� ���� ����� ���� + ����
  };
  CString GetCurrentTimeByString(TimerStringResult tsr = tsrH_M_S_Ms, __int64 nFromItTime_Ms = 0);
  
  // return: "����� %H:%M:%S"
  static CString GetCur(CString IDS = L"�����");

private:
  __int64 GetTimeIn(__int64 nMilliseconds, TimerMarkResult tmrResult, bool bRoundUp = false);
  __int64 ConvertMilliseconds(__int64 nMilliseconds, TimerMarkResult tmrResult);
  __int64 LoadTimerMark(int nID, bool bFromFile = false, CString strFileName = _T(""));

  std::vector<TIMER_MARK> m_timerMarkList;
  static std::vector<TIMER_MARK> m_timerMarkList_stc;
  bool m_bToStatic = false;

public:
  bool IsEmpty(SYSTEMTIME* pSt);
};
