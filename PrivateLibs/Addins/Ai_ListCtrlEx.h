#pragma once

#include <afxcmn.h>
#include <vector>
#include "AddinsDef.h"
#include "Ai_ListCtrlExDef.h"

class �Ai_ListCtrlEx_Table;

 //Function parameters info:
 //     - ..._inList - a index of a row in the list of all items (i.e. nodes and their wrap and unwrap children)
 //     - ..._inView - a index of a row in the ListCtrl
 //...

class ADDINS_API �Ai_ListCtrlEx
{
private:
  �Ai_ListCtrlEx_Table *m_pTable;
  
public:
  �Ai_ListCtrlEx(void);
  ~�Ai_ListCtrlEx(void);

  // Create
  
  // Create (step 1)
  void SetList(CListCtrl *pList, HWND pParentWnd);
  bool CreateList(bool bChkBoxes = false, DWORD nExWinStyle = 0);
  void Destroy();
  void ClearView();
  bool IsTable();
  
  bool CreateImageList(int cx, int cy, UINT nFlags, int nImageListType, std::vector<IMAGE_LIST>* pImgList);
  bool SetImageListAgain();

   //Columns (step 2)
  int SetColumnEx(CString strName, int nFormat = LVCFMT_LEFT);

  void SetColumnWidthStyle(int nCol, LONGLONG nColStyle, int nWidth = 0);
  void MatchColumns(bool bVertScrollOffset = true);
  bool IsMatchColumn();
  void ChangeColumnName(int nCol, CString strText);
  void ChangeColumnName(CListCtrl* pLst, int nCol, CString strText);
  int GetCountColumns();

  // Rows (step 3)
  //      Add nodes (3.1)
  int SetNode(std::vector<CString>* pLabels, BIND_TO_CELL* pBindToCells, bool bWithoutFirstNode);
  void DeleteEmptyMark(int nNodeRow_inList);
  void SetNodeStyle(int nNodeRow_inList, LONGLONG nNodeStyle = 0, int nUnwrapCell = 0, int nComnSignificantCell = 0);
  void SetNodeDisable(int nNodeRow_inList);
  void SetNodeEnable(int nNodeRow_inList);
  //      Add children (3.2); All three functions return nRow_inList
  int SetChild(int nNodeRow_inList, std::vector<CString>* pText, LONGLONG nRowStyle = 0, BIND_TO_CELL* pBindToCells = nullptr, bool bInnerNode = false);
  int SetChild(int nNodeRow_inList, CString strText = _T(""), int nTextToCell = 0);
  
  /*
  ������� ������� ������ ������ � �� ����� ������.
  ����� ��� ����, �������� SetNodeStyle (��. ����), ����������� nComnSignificantCell - �������� �������. ���� � ������ ������� ������� ��������� (�������
  GetCellText, ��. ����) �����, �� ������ ������������. ���� ������ ������ - ������ �� ���������.
  ��� ������������� �������� � ����������� ���������� ������ ����� ���������� �������, ������ ��������, ����� ������������ ���������� ����� (���������
  ������������ ���������� ����� ��� � ��������, ������ 20000) � �����, ������ ������������ ����� ����������, ����������� �������� ��� �������� ����� � �������� ������.
  ��� ��������� �������� ����������� ������������ ������� � ������������� � item-��.
  ��� ������� � empty ������ ������ ���������� ������������ �������� InsertTextToChildCells, �.�. ��� �������� ����������� ������������� �� ������� SetChild.
  ��� ������������ ��������� ���������� � ������� ������� ���������� ������������ ������� ClearChild.
  */
  int SetEmptyChild(int nNodeRow_inList, std::vector<CString>* pText, LONGLONG nRowStyle = 0, BIND_TO_CELL* pBindToCells = nullptr);
  void SetEmptyChild(int nCountRows); // fast insert to 0 node with empty cell text

  int MatchHeight(bool bScrollBar = true);
  
  void ClearColumn(int nCell);
  void ClearRows();

  //������� SetChild �� SetEmptyChild
  //SetChild - ������� ��� ����� �������� - ���� ����� � �������� ������

  //  Styles
  void SetCommonStyle(LONGLONG nStyle);
  void SetRowStyle(int nRow_inList, LONGLONG nStyle);

  // Insert, edit, move child(ren)
  
  // Work with child
  int GetInsertedIdxInFuture(int nRow_inView, LONGLONG nFlag);
  int InsertTextToChild(int nRow_inView/*or nNodeRow_inView*/, int nCell, CString strText, std::vector<CString>* pServiceInfo = nullptr,
    LONGLONG nFlag =  CHIF_TO_EMPTY_TAIL | CHIF_FILL_IN_GAPS,
    int nRow_inListInFuture = -1); /* ��������! �������� nRow_inListInFuture ����������� ���������� �������� GetInsertedIdxInFuture */
  int GetLastInsertedChildIdx_inList();
  
  int InsertTextToChildCells(int nChildRow_inList, std::vector<CString>* pstrText, std::vector<CString>* pServiceInfo = nullptr, LONGLONG nSrvcFlag = CHIF_SRVC_INFO_UPDATE);
  
  void InsertServiceInfo(int nRow_inList, std::vector<CString>* pServiceInfo, LONGLONG nFlag = CHIF_SRVC_INFO_UPDATE);
  void InsertServiceInfoToCell(int nRow_inList, int nCell, std::vector<CString>* pServiceInfo, LONGLONG nFlag = CHIF_SRVC_INFO_UPDATE);
  
  std::vector<int> m_refinedList_inList; // ��� �������� �������� � ���� ������ �������� ������� (���� inList) ��������� ��������� (�.�. �� ��� �������� ����� ���� �������, ��� ������� �� ������������ ������ ��� ���� (���� � ����� �������� � CListCtrl) ��������� �� �����

  bool ClearChild(int nRow_inView = -1, int nFlag = CRF_ALL_CELLS, std::vector<int>* pClearCellsList = nullptr, int nEnumCells = 0, ...);
   // nRow_inView >= 0 - �������� � ���� ������
   // nRow_inView == -1 - �������� ���������� ������
   // nRow_inView == -1 & nFlag & CRF_GET_ALL_ROWS - �������� ��� ������
   // nFlag & CRF_BY_INIT_CHAIN cells �������� �� <<int nEnumCells, ...>> ����� �� << vector<int>* pClearCellsList >>
  
  void ClearNodeChildren(int& nNodeRow_inView);

  enum SHOW_AND_SELECT_CHILD
  {
    shaschSpecified, // in nRow function argument
    shaschLastChildBySelectedRow,
    shaschLastInserted
  };
  int SelectAndShowChild(SHOW_AND_SELECT_CHILD nFlag, int nRow_inView, bool bMoveMouse);

  void MoveSelectedChildren(bool bUp, int nCopyFlag, int nFlag, std::vector<int>* pMovingCellsList = nullptr, int nEnumCells = 0, ...);
  void* m_pOuterMoveFunc;
  struct OUTER_MOVE_FUNC
  {
    // init before call
    
    int nMovedRowActive_inList; // ������, ������� ���� �������� � ����������
    int nMovedRowPassive_inList; // ������, �� ����������, ���� �������� ������������� 
    bool bUp;
    int nMoveLoop;

    // init outer

    bool bAnalysis;
    bool bRepeatAgain;
  };

  // Draw table
  
  // Draw
  void UpdateView();
  bool IsUpdatingView();
  void WrapTree();
  void UnwrapTree();

  // Work with messages from mouse, cells BindToCtrl etc.

  // Messages
  void OnDblClick(int nRow_inView, int nCell, POINT* pptAction = nullptr);
  void OnLClick(int nRow_inView, int nCell);
  int OnCustomDrawEx(NMHDR *pNMHDR, LRESULT *pResult, int colorBk = -1, int colorTx = -1);
  void OnSelchangeCmbbox();
  void OnCloseUpCmbbox();
  
  // Row indexes

  int CnvIdxViewToList(int nIdx_inView);
  bool GetIdxOfNodesInList(std::vector<int>* pIdxOfNodes_inList);
  int GetNodeRowIdxInView(int nRow_inList);
  int GetRowIdxInView(int nRow_inList);

  // Results

  int GetCommonSignificantCell(int nRow_inList);
  bool SetCommonSignificantCell(int nRow_inList, int nComnSignificantCell = 0);

  // Get result
  bool GetListItems(GET_BY_FILTER* pFilter, std::vector<RESULT_GET_LIST>* pResultList, int nCntReadColumns, ...); // ... - list of columns for read; -1 - end of it list
  // Find row
  int FindRow(CString strSoughtText, int nInCell);
  int FindRow(CString strSoughtText, int nFromRow_inList, int nInCell);
  // Service info
  bool GetServiceInfo(int nRow_inList, std::vector<CString>* pRowServiceInfo);
  bool SetServiceInfo(int nRow_inList, std::vector<CString>* pRowServiceInfo);
  bool GetCellServiceInfo(int nRow_inList, int nCell, std::vector<CString>* pServiceInfo);
  // Row
  CString GetNodeName(int nRow_inList);
  int GetAvailabelCountOfChild(int nRow_inList);
  int GetSetedCountOfChild(int nRow_inList);
  bool IsNodeRow(int nRow_inList);
  bool IsNodeUnwrapped(int nRow_inList);
  int GetNodeRow(int nRow_inList);
  bool IsSignificantCellSeted(int nRow_inList);
  bool IsRowEnable(int nRow_inList);
  bool DeleteRow(int nRow_inList);

  bool IsSelectedRows();
  void UnselectRows();
  void SelectRow(int nRow_inView);

  bool GetSelectedItems(std::vector<int>* pItems_inView);
  bool GetFirstSelectedItem(int* nRow_inList, int* nRow_inView = nullptr);
  bool IsSelectedRowVisible();
  void DoSelectedRowVisible();

  bool IsRowVisible(int nRow_inList);

  // Cell
  void SetCellStyle(int nRow_inList, int nCell, LONGLONG nStyle);
  LONGLONG GetCellStyle(int nRow_inList, int nCell);

  bool IsCellEnable(int nRow_inList, int nCell);
  CString GetCellText(int nRow_inList, int nCell);
  CString GetCellTextInSelRow(int nCell);

  CString GetChainOfSignCellsAbove(int nRow_inView);
  struct BIND_INFO
  {
    BIND_INFO()
      : nRow_inView(0)
      , nRow_inList(0)
      , nCell(0)
      , nCellFlag(0)
      , bct(bctNone)
      , imgWas(iltNone)
      , imgNew(iltNone)
    {}

    int nRow_inView;
    int nRow_inList;
    int nCell;
    int nCellFlag;
    CString strOldText;
    CString strNewText;
    bool IfNewMeaningsWasSeted() { return strOldText != strNewText; }
    BindCtrlType bct;

    ImageListType imgWas;
    ImageListType imgNew;
  };  
  BIND_INFO m_bindInfo;
  BindCtrlType HasCellBindCtrl(int nRow_inList, int nCell);
  int GetCellBindCtrlID(int nRow_inList, int nCell);
  void EditBindCtrlKillFocus(bool bSet = true);
  bool IsCtrlMeanWasSetedMulty();
  bool IsCtrlMeanWasSetedSingle();
  void ClearCtrlMeanWasSeted();
 
  bool SetCellTextColor(int nRow_inList, int nCell, COLORREF nColor);
  bool SetCellTextColor(int nCellFlag, COLORREF nColor);
  COLORREF GetCellTextColor(int nRow_inList, int nCell);
  bool SetCellBgColor(int nRow_inList, int nCell, COLORREF nColor);
  COLORREF GetCellBgColor(int nRow_inList, int nCell);
  bool SetRowBgColor(int nRow_inList, COLORREF nColor);
  bool SetRowTextColor(int nRow_inList, COLORREF nColor);
  bool SetCellEnable(int nRow_inList, int nCell);
  bool SetCellEnable(int nRow_inList, int nCell, bool bEnable);
  bool SetCellDisable(int nRow_inList, int nCell);
  bool SetRowEnable(int nRow_inList, bool bEnable = true);
  bool SetCellText(int nRow_inList, int nCell, CString strText, bool bUpdBindCtrl = false);
  bool SetCellsText(int nIdxNode, int nCell, CString strText, bool bIgnoreDisableCells);
  bool SetCellsText(int nIdxNode, int nCell, int nMeaning, int nInc, int nIncMax, bool bIgnoreDisableCells);
  bool SetCellHideText(int nRow_inList, int nCell);
  bool SetCellShowText(int nRow_inList, int nCell);

  void SetCellFlag(int nRow_inList, int nCell, int nFlag);
  int GetCellFlag(int nRow_inList, int nCell = 0);

  void SetCellData(int nRow_inList, int nCell, std::vector<__int64>* pData);
  std::vector<__int64>& GetCellData(int nRow_inList, int nCell);

  void SetRowBoldText(int nRow_inList, CellTextStyle cts);
  void SetCellTextStyle(int nRow_inList, int nCell, CellTextStyle cts);
  CellTextStyle GetCellTextStyle(int nRow_inList, int nCell);
  
  void SetCellImg(int nRow_inList, int nCell, ImageListType ilt);
  void SetCellImg(int nRow_inList, int nCell, int nImg);
  ImageListType GetCellImgType(int nRow_inList, int nCell);

  bool IsPointInCell(/*In*/ POINT* pP, /*Out*/ int& nRow_inView, /*Out*/ int& nCell);
  bool GetCellRect(int nRow_inList, int nCell, CRect* pRectInList, CRect* pRectInScreen);
   
  // Table

  void SetTableEnable(bool bEnable);
  bool IsTableEnable();
};

class �Ai_ListCtrlEx_Table
{
  friend class �Ai_ListCtrlEx;

private:
  void Destroy();

protected:
  �Ai_ListCtrlEx_Table(void);
  ~�Ai_ListCtrlEx_Table(void);
  void SetVarsByDef();

  �Ai_ListCtrlEx* m_pListCtrlExParent;
  CListCtrl* m_pListCtrl;
  HWND m_pParentWnd;
  bool m_bTableEnable;
  int m_nLastInsertedNodeChild;
  bool m_bFirstColIsChkBoxes;
  bool m_bWithoutFirstNode; // ��� ������ ����� � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  bool m_bSettingInnerNode;
  LONGLONG m_nCommonStyle;
  CFont* m_pFontOld;
  bool m_bUpdating;

  struct COLUMNS
  {
    COLUMNS ()
      : nWidth(0)
      , nStyle(0)
      , bMatchWidth(false)
    {}

    CString strName;
    int nWidth;
    bool bMatchWidth;
    LONGLONG nStyle;
  };
  std::vector<COLUMNS> m_columns;

  struct NODE_ROW
  {
    NODE_ROW()
      : bNodeDisable(false)
      , nUnwrapCell(-1)
      , nCntChildren(0)
      , nCntSetedChildren(0)
      , nStyle(0)
      , nComnSignificantCell(-1)
      , bUnwrapped(false)
      , strMarkCurrent(_T("[  ] "))
      , strMarkEmpty(_T("[  ] "))
      , strMarkClose(_T("[+] "))
      , strMarkOpen(_T("[ - ] "))
    {}

    bool bNodeDisable;
    int nUnwrapCell;
    int nComnSignificantCell; // if nStyle = NS_UNWRAP_ONLY_SETED or NS_UNWRAP_ONLY_SETED_WITH_GAPS then this id use
    int nCntChildren;
    int nCntSetedChildren; // * ���������� ����������� ������� �������� ��������� ��� ��������������������, �.�. �������� ������ ������, ����
    // ��� ����� � �� ������������ ���� - � �������� ������ �� ����� ������ (���� �� ������ �������) � ���� ����� ����� NS_USE_COMN_SIGNIFICANT_CELL
    // * ������� ���������������� ���������� ��� ������� �� ������� ��������� SetChild � �� ����. ��� ������� ������� ��������� SetEmptyChild
    // * ��� ��������� �����: ���������� �������� ����� �������. �������� ��������� + ����� ��������� ����� (�� ����������� 
    // �������� �������� ��������� �����)
    bool bUnwrapped;
    LONGLONG nStyle;
    CString strMarkCurrent;
    CString strMarkEmpty;
    CString strMarkClose;
    CString strMarkOpen;
  };

  struct CELL_BINDING
  {
    CELL_BINDING()
      : bDisableBind(false)
      , pstrExchange(nullptr)
      , pCmbBox(nullptr)
      , pDialog(nullptr)
      , nSelRowByDefault(-1)
      , pEdit(nullptr)
      , pIP(nullptr)
      , nID(-1)
    {}

    bool bDisableBind;
    CString* pstrExchange;

    CComboBox* pCmbBox;
    int nSelRowByDefault;

    CDialog* pDialog;

    CString strByDefault;

    bool bSetDefaultForce; // ������ ����� � ������ (��� ������� child) �� strByDefault ���� ���� ������ ��� �������� �����-�� �����
    
    CEdit* pEdit;

    CIPAddressCtrl* pIP;
    
    int nID;

    CELL_BINDING& operator=(const CELL_BINDING& ob);
    bool operator==(const CELL_BINDING& ob);
  };

  struct CELL
  {
    CELL()
      : bCellDisable(false)
      , bHideText(false)
      , nColorBg(COLOR_LCE_WHITE)
      , nColorTx(COLOR_LCE_BLACK)
      , nColorBgNew(-1)
      , nColorTxNew(-1)
      , nColorTxNew_beforeDisable(-1)
      , nStyle(0)
      , nFlag(-1)
      , textStyle(ctsNone)
      , imgType(iltNone)
      , nImgIdx(-1)
      , bUpdateImg(false)
    {}

    bool bCellDisable;
    bool bHideText;
    int nFlag;
    std::vector<__int64> data64;
    CString strText;
    CellTextStyle textStyle;
    // have <nColor...> and <nColor...New> can return unconfigured state
    const COLORREF nColorBg;
    int nColorBgNew;
    const COLORREF nColorTx;
    int nColorTxNew;
    int nColorTxNew_beforeDisable;
    LONGLONG nStyle;
    std::vector<CString> serviceInfo; // hide service info
    
    bool bUpdateImg;
    ImageListType imgType;
    int nImgIdx;
    
    CELL_BINDING bindTo;
    CELL_BINDING bindToTmp;
    
    CELL& operator=(const CELL& c);
  };
  struct ROW
  {
    ROW()
      : pNode(nullptr)
      , bCopyCells(true)
      , nStyle(0)
      , nIdx_inView(-1)
      , nIdxParentNode_inList(-1)
      , nIdxOneself_inList(-1)
      , bRowDisable(false)
      , bShowing(false)
      , bSeted(false)
    {}

    std::vector<CELL> cells;
    bool bCopyCells;

    LONGLONG nStyle;
    bool bRowDisable;
    bool bShowing;
    bool bSeted;
    int nIdx_inView; // view index (i.e. ListCtrl index);  = -1 if invisible in ListCtrl
    int nIdxParentNode_inList; // node index in inner list (i.e. ..._inList index, not ListCtrl (..._inView) index)
    int nIdxOneself_inList; //  inner list index (i.e. ..._inList index, not ListCtrl (..._inView) index)
    std::vector<CString> serviceInfo; // hide service info

    NODE_ROW* pNode;

    ROW& operator=(const ROW& r);
  };
  std::vector<ROW> m_rows;
  std::vector<ROW*> m_showingRows;

  // *** Create (step 1)
  bool m_bCtrlIs;
  bool m_bCreatedOK;
  // *** Columns (step 2)
  bool m_bColumnIs;
  // *** Rows (step 3)
  //       Add nodes (3.1)
  bool m_bNodeIs;
  //       Add children (3.2)
  bool m_bChildIs;
  
  // *** Can do somethings next
  bool CanWorkWithNode(int nNodeRow_inList);
  bool CanWorkWithCell_list(int nRow_inList, int nCell, bool bAssertByOutOfRange = true);
  bool CanWorkWithCell_view(int nRow_InView, int nCell, bool bAssertByOutOfRange = true);
  bool CanWorkWithRow_view(int nRow_InView, bool bAssertByOutOfRange = true);
  bool CanWorkWithRow_list(int nRow_inList, bool bAssertByOutOfRange = true);
  
  bool CanSetList(CListCtrl* pList, HWND pParentWnd);
  bool CanCreate();
  CFont m_fItalic;
  CFont m_fItalicBold;
  CFont m_fStrikeOut;
  void PrepareFonts();
  
  bool CanSetColumn();
  bool CanSetColumnWidthStyle(int nCol, LONGLONG &nColStyle, int nWidth);
  bool CanMatchColumns();
  bool CanSetNode(BIND_TO_CELL* pBindToCells);
  bool CanSetNodeStyle(int nNodeRow_inList, LONGLONG nStyle, int nUnwrapCell, int nComnSignificantCell);
  bool CanSetChild(int nNodeRow_inList, int nCount, LONGLONG nStyle, BIND_TO_CELL* pBindToCells);
  
  NODE_ROW* m_pInsNode;
  bool CanInsertTextToChild(int nRow_inView, int nCell, std::vector<CString>* pServiceInfo, LONGLONG* pnFlag);
  bool CanInsertServiceInfo(int nRow_inList, std::vector<CString>* pServiceInfo, LONGLONG nFlag);
  bool CanInsertTextToChildCells(int nChildRow_inList, LONGLONG nFlag);

  bool CanShowAndSelectChild(�Ai_ListCtrlEx::SHOW_AND_SELECT_CHILD nFlag, int nRow_inView);
  bool CanClearChild(int nRow_inView, int* pnFlag);
  bool CanGetListItems(GET_BY_FILTER* pFilter, std::vector<RESULT_GET_LIST>* pResultList, int nCntReadColumns);
  bool CanMoveUpDownSelectedChildren(bool bUp, int nFlag, std::vector<int>* pMovingCellsList, int nEnumCells, std::vector<int>* pSelItems);//, int& nIdxFrom, int& nIdxTo);
  bool CanSelchangeCmbbox();
  bool CanFindRow(int nCell);
  bool CanGetIndexesOfNodes_list(void* pIdxOfNodes_inList);
  bool CanWorkWithRowIdx_view(int nRow_InView);
  bool CanWorkWithRowIdx_list(int nRow_inList);
  bool CanGetServiceInfo(int nRow_inList, void* pRowServiceInfo);
  bool CanGetServiceInfoFromCell(int nRow_inList, int nCell, void* pServiceInfo);
  bool CanSetCellTextColor(int nRow_inList, int nCell, COLORREF nColor);
  bool CanOnDblClick(int nRow_inView, int nCell);
  
  int GetParentNodeIdx_inList(int nRowIdx_inList);
  int GetParentNodeIdx_inView(int nRowIdx_inView);
  
  bool CanUpdateView();
  void ShowRow(ROW* pRow);
  void ClearShowingRows();

  bool IsCellEnable(int nRow_inList, int nCell);
  bool IsRowEnable(int nRow_inList);
  
  struct IMG_LIST_INFO
  {
    IMG_LIST_INFO()
      : pBmp(nullptr)
      , nName(-1)
      , nIdxInImageList(-1)
      , imgType(iltNone)
    {}

    CBitmap* pBmp;
    int nName;
    int nIdxInImageList;
    ImageListType imgType;
  };
  std::vector<IMG_LIST_INFO> m_bmps;
  CImageList* m_pImageList;
  int m_nImageListType;
  int m_nImgesWidth;
  void CheckCellImg(int nRow_inView, int nCell, bool bShow = true);
  bool IsClkOnImg(int nRow_inView, int nCell, POINT* p);

  void SetNodeEnblDsbl(int nNodeRow_inList, bool bDisable);
  
  void ParentsAboveUpdate(int nRow_inList, bool bIncrSetedCount);
  bool IsAllParentsAboveUnwrapped(int nRow_inList);
  void AddInnerNodeTextOffset(int nRow_inList, int nCell, CString& strText);
  int GetIdxImgListType(ImageListType ilt);
  void SetRowEnblDsbl(int nRow_inList, bool bDisable);
  bool m_bSetOnlyCell;
  int m_nEnblDsblCell;
  void SetCellEnblDsbl(int nRow_inList, int nCell, bool bDisable);

  bool IsNode(int nRow_inList);
  bool IsNode_inner(int nRow_inList);
  int GetCountColumns();
  
  bool m_bCheckUnwrap;
  void UnwrapTree(bool bUnwrap);
  bool ChangeUnwrapNodeMark(int nIdxRow, int nCell);
  bool CheckUnwrapNodeMark(int nIdxNodeRow, int nCell);
  void SetUnwrapNodeMark(int nIdxNodeRow, CString strNewMark);
  CString DeleteUnwrapCellMark(int nIdxNodeRow, CString strSrc);
  
  void SelectAndShowThisRow(int nRow_inView, bool bMoveMouse);
  void SelectThisRows(std::vector<int>* pRows_inView, bool bSelFirst = true);
  void UnselectRows();
  void UnselectRow(int nRow_inView);
  bool GetSelectedItems(std::vector<int>* pItems_inView);
  bool IsSetedChildInFront(int nRow_inView);
  bool MoveUpDownChildren(bool bUp, int nCopyFlag, int nFlag, std::vector<int>* pMovingCellsList, std::vector<int>* pSelItems);//, int nFrom, int nTo);
  void ClearCell(CELL* pCell);
  void ClearCellBind(CELL_BINDING* pCellBind);
  
  enum SEARCH_IN_NODE_CHILDREN
  {
    searchFirstGapIndex,
    searchFirstGapIndex_couple,
    searchFreeTailIndex,
    searchLastSetedChild
  };
  int SearchInNodeChildren(SEARCH_IN_NODE_CHILDREN nSearchFlag, int nNodeRow_InList);
  void SetMeansInCtrlsForChild(int nChildRow_InList);
  void UpdateCellCtrlsAfterSetNewMeansInCells(int nChildRow_InList);
  
  // Bind ctrl

  CELL_BINDING* m_pCurrCellBind;
  struct EVENT_IN_BIND_CELL
  {
    EVENT_IN_BIND_CELL()
      : pcmbOpened(nullptr)
      , pedtOpened(nullptr)
      , pipOpened(nullptr)
      , cellRect(0, 0, 0, 0)
    {}

    int nRow_inView;
    int nCell;
    CRect cellRect;
    CString strSelectedInCmbbox;
    CString strBefore;
    CComboBox* pcmbOpened;
    CEdit* pedtOpened;
    CIPAddressCtrl* pipOpened;
  };
  EVENT_IN_BIND_CELL m_eventInDisableBindCell;
  bool IsDisableBindCorrect(BIND_TO_CELL* pBindToCells);
  void BindToCell(BIND_TO_CELL* pBindToCells, int nCell, CELL_BINDING *pCellBinding);
  bool RecalcParamsAfterClickOnCell(int nRow_inView, int nCell);
  void GetSubItemRect(int nRow_inView, int nCell, CRect& subItemRect, CRect& subItemWinRect);
  int ChangeTextInCellAfterCtrl(CString strNewText);
  void UpdateNodeCellIfNeed(int nRow_inList, int nCell, CString strText);
  int SelchangeCmbbox();
  void InitAllCellsOfNodeChildren(int nIdxNode, int nCell, CString strText);
  
  void UpdateAllCellsOfNodeChildren(int nIdxNode, int nCell, CString strText, int nMeaning, int nInc, int nIncMax, bool bIgnoreDisableCells);
  bool m_bUseInc;

  bool IfCellMarksOfSetedNodeChildrenDifferent(int nIdxNode, int nCell);
  void SelectThisStr(CComboBox *pCmbBox, CString strText);
  void CloseOpenedCtrl(bool bKillFocus = false, bool bSet = true);
  enum MEAN_WAS_SETET_CTRL // setting after sel in combo box
  {
    mwsNone,
    mwsSingle, // changed in one row
    mwsMulty // changed in node and were changes in children too
  };
  MEAN_WAS_SETET_CTRL m_meanWasSetedAfterBindCtrl;
  
  // ...
  void CommonSignificantCellWasCleared(int nRow_inView);
  void ClearNodeCtrlCells(int nNodeRow_inView);

  void SendMsg(DWORD nMsg, WPARAM wp, LPARAM lp);
};
