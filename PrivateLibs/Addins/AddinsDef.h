#pragma once

#ifndef __AFXWIN_H__
  #error "include 'stdafx.h' before including this file for PCH"
#endif

#ifdef ADDINS_EXPORTS
  #define ADDINS_API __declspec(dllexport)
  #else
  #define ADDINS_API __declspec(dllimport)
#endif
