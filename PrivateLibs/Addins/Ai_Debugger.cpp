#include "stdafx.h"
#include "Ai_Debugger.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CDebugger::CDebugger()
{
}

void CDebugger::TraceOfNew(CString strValueName, void* p)
{
  p = p;
  #ifdef _DEBUG
  CString strMsg;
  strMsg.Format(_T("### new; Value name: \"%s\", Address: 0x%.8X\r\n"), strValueName, p);
  TRACE(strMsg);
  #endif
}