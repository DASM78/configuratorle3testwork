#include "StdAfx.h"
#include "Resource.h"
#include "afxglobals.h"
#include <algorithm>
#include "Ai_Str.h"
#include "Ai_ListCtrlEx.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void(*OuterMoveFunc)(void*);

�Ai_ListCtrlEx::�Ai_ListCtrlEx(void)
  : m_pTable(new �Ai_ListCtrlEx_Table)
{
   m_pTable->m_pImageList = nullptr;
   m_pTable->m_pListCtrlExParent = this;
   m_pOuterMoveFunc = nullptr;

   m_refinedList_inList.clear();
}

�Ai_ListCtrlEx::~�Ai_ListCtrlEx(void)
{
  Destroy();
}

�Ai_ListCtrlEx_Table::�Ai_ListCtrlEx_Table(void)
  // : init variables do in function SetVarsByDef()
{
  SetVarsByDef();
}

void �Ai_ListCtrlEx_Table::SetVarsByDef()
{
  m_pListCtrl = nullptr;
  m_pParentWnd = nullptr;
  m_bCtrlIs = false;
  m_bCreatedOK = false;
  m_bColumnIs = false;
  m_bNodeIs = false;
  m_bChildIs = false;
  m_nLastInsertedNodeChild = -1;
  m_bCheckUnwrap = false;
  m_bSetOnlyCell = false;
  m_meanWasSetedAfterBindCtrl = mwsNone;
  m_bFirstColIsChkBoxes = false;
  m_nCommonStyle = 0;
  m_bWithoutFirstNode = false;
  m_pCurrCellBind = nullptr;
  m_nEnblDsblCell = -1;
  m_bSettingInnerNode = false;
  m_bUpdating = false;
  m_pFontOld = nullptr;
  m_bTableEnable = true;

  for (size_t i = 0; i < m_rows.size(); ++i)
  {
    if (m_rows[i].pNode != nullptr)
    {
      delete m_rows[i].pNode;
      m_rows[i].pNode = nullptr;
      m_rows[i].serviceInfo.clear();
      m_rows[i].cells.clear();
    }
  }
  m_showingRows.clear();
  m_rows.clear();
  m_columns.clear();

  m_bUseInc = false;
}

�Ai_ListCtrlEx_Table::~�Ai_ListCtrlEx_Table(void)
{
}

void �Ai_ListCtrlEx_Table::Destroy()
{
  SetVarsByDef();

  if (m_pImageList)
  {
    for (size_t i = 0; i < m_bmps.size(); ++i)
      delete m_bmps[i].pBmp;
    m_bmps.clear();
    delete m_pImageList;
    m_pImageList = nullptr;
  }
}

void �Ai_ListCtrlEx::Destroy()
{
  if (m_pTable != nullptr)
  {
    m_pTable->Destroy();
    delete m_pTable; // ��������� ����� ��-������� - ����� ������������ ��� ����� (bool) �������� (� 
    // ��������� �� � �������� ��������� �� ������ new)
    m_pTable = nullptr;
  }
}

void �Ai_ListCtrlEx::ClearView()
{
  m_pTable->SetVarsByDef();
}

bool �Ai_ListCtrlEx::IsTable()
{
  if (m_pTable == nullptr)
    return false;
  return m_pTable->m_bCreatedOK;
}

///*******************************************************************
//Create
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanSetList(CListCtrl* pList, HWND pParentWnd)
{
  RETURN_AST_FALSE_BY_TRUE(m_bCtrlIs);
  RETURN_AST_FALSE_BY_TRUE(pList == nullptr);
  RETURN_AST_FALSE_BY_TRUE(pParentWnd == nullptr);
  return true;
}

void �Ai_ListCtrlEx::SetList(CListCtrl* pList, HWND pParentWnd)
{
  if (m_pTable == nullptr)
  {
    m_pTable = (�Ai_ListCtrlEx_Table*) new �Ai_ListCtrlEx_Table();
    m_pTable->m_pImageList = nullptr;
    m_pTable->m_pListCtrlExParent = this;
    RETURN_AST_BY_TRUE(m_pTable == nullptr);
  }

  RETURN_BY_TRUE(!m_pTable->CanSetList(pList, pParentWnd));
  m_pTable->m_pListCtrl = pList;
  //m_pTable->m_pListCtrl->SendMessage(CCM_SETVERSION, 5, 0);
  m_pTable->m_pParentWnd = pParentWnd;
  m_pTable->m_bCtrlIs = true;
}

bool �Ai_ListCtrlEx_Table::CanCreate()
{
  RETURN_AST_FALSE_BY_TRUE(m_bCreatedOK);
  return m_bCtrlIs;
}

void �Ai_ListCtrlEx_Table::PrepareFonts()
{
  CFont* f = m_pListCtrl->GetFont();
  LOGFONT lf;
  f->GetLogFont(&lf);

  BYTE IfItalic = lf.lfItalic;
  lf.lfItalic = TRUE;
  m_fItalic.DeleteObject();
  m_fItalic.CreateFontIndirect(&lf);

  LONG IfWeight = lf.lfWeight;
  lf.lfWeight = FW_BOLD;
  m_fItalicBold.DeleteObject();
  m_fItalicBold.CreateFontIndirect(&lf);

  lf.lfItalic = IfItalic;
  lf.lfWeight = IfWeight;
  lf.lfStrikeOut = TRUE;
  m_fStrikeOut.DeleteObject();
  m_fStrikeOut.CreateFontIndirect(&lf);
}

bool �Ai_ListCtrlEx::CreateList(bool bChkBoxes, DWORD nExWinStyle)
{
  RETURN_AST_FALSE_BY_TRUE(!m_pTable->CanCreate());

  DWORD nStyle = LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

  if (bChkBoxes)
  {
    nStyle |= LVS_EX_CHECKBOXES;
    m_pTable->m_bFirstColIsChkBoxes = true;
  }
  m_pTable->m_pListCtrl->SetExtendedStyle(nStyle);
  m_pTable->m_pListCtrl->SetExtendedStyle(m_pTable->m_pListCtrl->GetExtendedStyle() | nExWinStyle);
  m_pTable->m_bCreatedOK = true;
  m_pTable->PrepareFonts();

  return true;
}

bool �Ai_ListCtrlEx::CreateImageList(int cx, int cy, UINT nFlags, int nImageListType, vector<IMAGE_LIST>* pImgList)
{
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_pListCtrl == nullptr);
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_pImageList != nullptr);
  RETURN_AST_FALSE_BY_TRUE(cx <= 0);
  RETURN_AST_FALSE_BY_TRUE(cy <= 0);
//  RETURN_AST_FALSE_BY_TRUE((nFlags == ILC_COLOR4
//    || nFlags == ILC_COLOR8
//    || nFlags == ILC_COLOR16
//    || nFlags == ILC_COLOR24
//    || nFlags == ILC_COLOR32
//    || nFlags == ILC_MASK) == false);
  RETURN_AST_FALSE_BY_TRUE(nImageListType != LVSIL_SMALL);
  RETURN_AST_FALSE_BY_TRUE(pImgList == nullptr);
  RETURN_AST_FALSE_BY_TRUE(pImgList->size() == 0);

  m_pTable->m_pImageList = new CImageList();
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_pImageList == nullptr);

  m_pTable->m_pListCtrl->SetExtendedStyle(m_pTable->m_pListCtrl->GetExtendedStyle() | LVS_EX_SUBITEMIMAGES);

  m_pTable->m_pImageList->Create(cx, cy, nFlags, (int)pImgList->size(), 0);
  for (size_t i = 0; i < pImgList->size(); ++i)
  {
    if (pImgList->at(i).nResourceBmpID != -1)
    {
      �Ai_ListCtrlEx_Table::IMG_LIST_INFO im;
      im.pBmp = new CBitmap();
      im.pBmp->LoadBitmap(pImgList->at(i).nResourceBmpID);
      im.nName = pImgList->at(i).nName;
      im.imgType = pImgList->at(i).imgType;
      im.nIdxInImageList = m_pTable->m_pImageList->Add(im.pBmp, pImgList->at(i).nMask);

      bool bAdd = true;
      for (size_t img = 0; img < m_pTable->m_bmps.size(); ++img)
      {
        if (pImgList->at(i).imgType != iltNone
          && pImgList->at(i).imgType == m_pTable->m_bmps[img].imgType)
        {
          ASSERT(false);
          bAdd = false;
          break;
        }
        if (pImgList->at(i).nName != -1
          && pImgList->at(i).nName == m_pTable->m_bmps[img].nName)
        {
          ASSERT(false);
          bAdd = false;
          break;
        }
      }
      if (bAdd)
        m_pTable->m_bmps.push_back(im);
    }

    if (pImgList->at(i).nResourceIconID != -1)
    {
      HICON hIcon = AfxGetApp()->LoadIcon(pImgList->at(i).nResourceIconID);
      m_pTable->m_pImageList->Add(hIcon); 
    }
  }
  m_pTable->m_nImageListType = nImageListType;
  m_pTable->m_pListCtrl->SetImageList(m_pTable->m_pImageList, nImageListType);

  m_pTable->m_nImgesWidth = cx;

  return true;
}

bool �Ai_ListCtrlEx::SetImageListAgain()
{
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_pListCtrl == nullptr);
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_pImageList == nullptr);
  RETURN_AST_FALSE_BY_TRUE(m_pTable->m_bmps.size() == 0);
  m_pTable->m_pListCtrl->SetImageList(m_pTable->m_pImageList, m_pTable->m_nImageListType);
  return true;
}

///*******************************************************************
//Set columns
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanSetColumn()
{
  RETURN_AST_FALSE_BY_TRUE(m_bNodeIs);
  return m_bCreatedOK;
}

int �Ai_ListCtrlEx::SetColumnEx(CString strName, int nFormat)
{
  RETURN_AST_1_BY_TRUE(!m_pTable->CanSetColumn());

  �Ai_ListCtrlEx_Table::COLUMNS column;
  
  if (strName.Right(CString(C_MATCH_WIDTH).GetLength()) == C_MATCH_WIDTH)
  {
    strName.TrimRight(C_MATCH_WIDTH);
    column.bMatchWidth = true;
  }
  column.strName = strName;
  column.nWidth = 0;
  column.nStyle = CS_WIDTH_SET_BY_TEXT;
  m_pTable->m_columns.push_back(column);

  int nIdx = (int)m_pTable->m_columns.size() - 1;

  m_pTable->m_bColumnIs = true;
  SetColumnWidthStyle(nIdx, column.nStyle);

  if (nFormat == -2)
  {
    LVCOLUMN lvColumn = {0};
    lvColumn.mask = LVCF_FMT | LVCF_WIDTH; 
    lvColumn.fmt = LVCFMT_CENTER | LVCFMT_IMAGE;
    lvColumn.cx = m_pTable->m_columns[nIdx].nWidth;
    m_pTable->m_pListCtrl->InsertColumn(nIdx, &lvColumn);
  }
  else
    m_pTable->m_pListCtrl->InsertColumn(nIdx, m_pTable->m_columns[nIdx].strName, nFormat, m_pTable->m_columns[nIdx].nWidth);

  return nIdx;
}

bool �Ai_ListCtrlEx_Table::CanSetColumnWidthStyle(int nCol, LONGLONG& nColStyle, int nWidth)
{
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCol, 0, (int)m_columns.size() - 1);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nWidth, 0, CS_WIDTH_LIMIT);

  if (!(nColStyle & CS_WIDTH_SET_BY_TEXT) &&
      !(nColStyle & CS_WIDTH_FIX))
      nColStyle |= CS_WIDTH_FREE;

  return m_bColumnIs;
}

void �Ai_ListCtrlEx::SetColumnWidthStyle(int nCol, LONGLONG nColStyle, int nWidth)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanSetColumnWidthStyle(nCol, nColStyle, nWidth));

  if ((nColStyle & CS_WIDTH_FREE) == 0)
  {
    if (nColStyle & CS_WIDTH_FIX)
      m_pTable->m_columns[nCol].nWidth = nWidth;
    else // CS_WIDTH_SET_BY_TEXT
    {
      int nStrWidth = 0;
      CHeaderCtrl* pHdr = m_pTable->m_pListCtrl->GetHeaderCtrl();
      if (pHdr)
      {
        CDC* pDCHListCtrl = pHdr->GetDC();
        nStrWidth = pDCHListCtrl->GetTextExtent(m_pTable->m_columns[nCol].strName).cx;
      }

      int nL = m_pTable->m_columns[nCol].strName.GetLength();
      if (nL < 14)
        nStrWidth += ((int)(nL * 1.7));
      m_pTable->m_columns[nCol].nWidth = nStrWidth;
    }
  }
  m_pTable->m_columns[nCol].nStyle = nColStyle;
  if (nWidth != 0 &&
    (m_pTable->m_pListCtrl->GetColumnWidth(nCol) != nWidth))
  {
    m_pTable->m_pListCtrl->SetColumnWidth(nCol, nWidth);
  }
}

bool �Ai_ListCtrlEx_Table::CanMatchColumns()
{
  return m_bColumnIs;
}

void �Ai_ListCtrlEx::MatchColumns(bool bVertScrollOffset)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanMatchColumns());

  CRect rect;
  int nColsWidth = 0;
  int nCol = 0;
  int nWidthVertScrollWnd = 35;

  m_pTable->m_pListCtrl->GetClientRect(&rect);
  for (nCol = 0; nCol < m_pTable->GetCountColumns(); ++nCol)
    nColsWidth += m_pTable->m_pListCtrl->GetColumnWidth(nCol);
  
  int nCntCols = (int)m_pTable->m_columns.size();

  for (int i = 0; i < nCntCols; ++i)
    if (m_pTable->m_columns[i].bMatchWidth && 
      (i + 1) <= nCol)
    {
      int nVertScrollOffset = bVertScrollOffset ? nWidthVertScrollWnd : 0;
      int nW = m_pTable->m_columns[i].nWidth + ((rect.Width() - nVertScrollOffset) - nColsWidth);
      m_pTable->m_pListCtrl->SetColumnWidth(i, nW);
    }
}

bool �Ai_ListCtrlEx::IsMatchColumn()
{
  int nCntCols = (int)m_pTable->m_columns.size();
  for (int i = 0; i < nCntCols; ++i)
    if (m_pTable->m_columns[i].bMatchWidth)
      return true;
  return false;
}

void �Ai_ListCtrlEx::ChangeColumnName(int nCol, CString strNewName)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanMatchColumns());
  ChangeColumnName(m_pTable->m_pListCtrl, nCol, strNewName);
}

void �Ai_ListCtrlEx::ChangeColumnName(CListCtrl* pLst, int nCol, CString strNewName)
{
  RETURN_AST_BY_TRUE(pLst == nullptr);
  RETURN_AST_BY_TRUE(nCol < 0 || nCol > pLst->GetHeaderCtrl()->GetItemCount());

  LVCOLUMN col;
  TCHAR szText[MAX_PATH] = {0};
  
  col.mask = LVCF_TEXT;
  col.pszText = szText;
  col.cchTextMax = MAX_PATH;

  if(pLst->GetColumn(nCol, &col))
  {
    col.pszText = strNewName.GetBuffer();
    col.cchTextMax = strNewName.GetLength();
    pLst->SetColumn(nCol, &col);
  }
}

int �Ai_ListCtrlEx::GetCountColumns()
{
  return m_pTable->GetCountColumns();
}

///*******************************************************************
//Set nodes
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanSetNode(BIND_TO_CELL* pBindToCells)
{
  RETURN_AST_FALSE_BY_TRUE(!IsDisableBindCorrect(pBindToCells));
  return m_bColumnIs;
}

int �Ai_ListCtrlEx::SetNode(vector<CString>* pLabels, BIND_TO_CELL* pBindToCells, bool bWithoutFirstNode)
{
  RETURN_AST_1_BY_TRUE(!m_pTable->CanSetNode(pBindToCells));

  int nCntStr = 0;
  int nCntCols = (int)m_pTable->m_columns.size();
  CString strText;
  �Ai_ListCtrlEx_Table::ROW row;
  �Ai_ListCtrlEx_Table::CELL cell;

  if (m_pTable->m_rows.size() == 0)
    m_pTable->m_bWithoutFirstNode = bWithoutFirstNode;

  if (pLabels != nullptr)
    nCntStr = (int)pLabels->size();
  
  for (int i = 0; i < nCntCols; ++i)
  {
    strText = _T("");
    
    if (i < nCntStr)
      strText += pLabels->at(i);

    m_pTable->ClearCell(&cell);
    
    cell.strText = strText;
    
    COLORREF* clr = const_cast<COLORREF*>(&cell.nColorBg);
    if (cell.bCellDisable)
    {
      if (m_pTable->m_bSettingInnerNode)
        *clr = COLOR_LCE_RED_YELLOW2;
      else
        *clr = COLOR_LCE_RED_YELLOW;
    }
    else
    {
      if (m_pTable->m_bSettingInnerNode)
        *clr = COLOR_LCE_LIGHT_YELLOW;
      else
        *clr = COLOR_LCE_YELLOW;
    }

    m_pTable->BindToCell(pBindToCells, i, &cell.bindTo);
    if (cell.bindTo.bDisableBind)
    {
      *clr = COLOR_LCE_RED_YELLOW;
      cell.bCellDisable = true;
    }

    row.cells.push_back(cell);
  }

  // Node`s initialisation

  row.pNode = new �Ai_ListCtrlEx_Table::NODE_ROW();
  if (row.pNode != nullptr)
  {
    if (bWithoutFirstNode)
      row.pNode->bUnwrapped = true;

    row.nIdxOneself_inList = (int)m_pTable->m_rows.size();
    m_pTable->m_rows.push_back(row);

    m_pTable->m_bNodeIs = true;

    return row.nIdxOneself_inList;
  }

  return -1;
}

void �Ai_ListCtrlEx::DeleteEmptyMark(int nNodeRow_inList)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nNodeRow_inList));
  RETURN_AST_BY_TRUE(!m_pTable->IsNode(nNodeRow_inList));
  CString strText = m_pTable->m_rows[nNodeRow_inList].cells[m_pTable->m_rows[nNodeRow_inList].pNode->nUnwrapCell].strText;
  strText = m_pTable->DeleteUnwrapCellMark(nNodeRow_inList, strText);
  m_pTable->m_rows[nNodeRow_inList].cells[m_pTable->m_rows[nNodeRow_inList].pNode->nUnwrapCell].strText = strText;
  m_pTable->m_rows[nNodeRow_inList].pNode->strMarkCurrent = "";
  m_pTable->m_rows[nNodeRow_inList].pNode->strMarkEmpty = "";
}

bool �Ai_ListCtrlEx_Table::CanSetNodeStyle(int nNodeRow_inList, LONGLONG nStyle, int nUnwrapCell, int nComnSignificantCell)
{
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nNodeRow_inList));
  RETURN_AST_FALSE_BY_TRUE(!IsNode(nNodeRow_inList));
  if (nNodeRow_inList == 0 && m_bWithoutFirstNode)
  {
  }
  else
  {
    RETURN_AST_FALSE_BY_TRUE(!(nStyle & NS_UNWRAP));
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nUnwrapCell, 0, (int)m_columns.size() - 1)
  }
  if ((nStyle & NS_USE_COMN_SIGNIFICANT_CELL))
  {
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nComnSignificantCell, 0, (int)m_columns.size() - 1);
  }
  else
    nComnSignificantCell = -1;

  return true;
}

void �Ai_ListCtrlEx::SetNodeStyle(int nNodeRow_inList, LONGLONG nStyle, int nUnwrapCell, int nComnSignificantCell)
{
  RETURN_BY_TRUE(!m_pTable->CanSetNodeStyle(nNodeRow_inList, nStyle, nUnwrapCell, nComnSignificantCell));

  m_pTable->m_rows[nNodeRow_inList].pNode->nUnwrapCell = nUnwrapCell;
  if (nStyle & NS_USE_COMN_SIGNIFICANT_CELL)
    m_pTable->m_rows[nNodeRow_inList].pNode->nComnSignificantCell = nComnSignificantCell;
  m_pTable->m_rows[nNodeRow_inList].pNode->nStyle = nStyle;
  if (nUnwrapCell >= 0)
  {
    m_pTable->m_rows[nNodeRow_inList].pNode->bUnwrapped = false;
    m_pTable->m_rows[nNodeRow_inList].cells[nUnwrapCell].strText = m_pTable->m_rows[nNodeRow_inList].pNode->strMarkEmpty + m_pTable->m_rows[nNodeRow_inList].cells[nUnwrapCell].strText;
  }
  m_pTable->m_rows[nNodeRow_inList].pNode->strMarkCurrent = m_pTable->m_rows[nNodeRow_inList].pNode->strMarkEmpty;
}

void �Ai_ListCtrlEx::SetNodeDisable(int nNodeRow_inList)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithNode(nNodeRow_inList));
  m_pTable->SetNodeEnblDsbl(nNodeRow_inList, true);
}

void �Ai_ListCtrlEx::SetNodeEnable(int nNodeRow_inList)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithNode(nNodeRow_inList));
  m_pTable->SetNodeEnblDsbl(nNodeRow_inList, false);
}

void �Ai_ListCtrlEx_Table::SetNodeEnblDsbl(int nNodeRow_inList, bool bDisable)
{
  int nCntChild = m_rows[nNodeRow_inList].pNode->nCntChildren;

  m_rows[nNodeRow_inList].pNode->bNodeDisable = bDisable;
  SetRowEnblDsbl(nNodeRow_inList, bDisable);
  for (int i = nNodeRow_inList; (i - nNodeRow_inList) < nCntChild; ++i)
    SetRowEnblDsbl(i, bDisable);
}

void �Ai_ListCtrlEx_Table::SetRowEnblDsbl(int nRow_inList, bool bDisable)
{
  int nCntCols = (int)m_columns.size();
  COLORREF nColor;
 
  if (!m_bSetOnlyCell)
    m_rows[nRow_inList].bRowDisable = bDisable;

  for (int i = 0; i < nCntCols; ++i)
  {
    if (m_bSetOnlyCell && m_nEnblDsblCell != i) // set only cell not row
      continue;

    if (m_bSetOnlyCell)
      m_rows[nRow_inList].cells[i].bCellDisable = bDisable;

    if (!IsCellEnable(nRow_inList, i))
    {
      if (m_rows[nRow_inList].cells[i].nColorTxNew != COLOR_LCE_GRAY)
      {
        m_rows[nRow_inList].cells[i].nColorTxNew_beforeDisable = m_rows[nRow_inList].cells[i].nColorTxNew;
        m_rows[nRow_inList].cells[i].bindToTmp = m_rows[nRow_inList].cells[i].bindTo;
        CELL_BINDING b;
        m_rows[nRow_inList].cells[i].bindTo = b; // clear
      }
      m_rows[nRow_inList].cells[i].nColorTxNew = COLOR_LCE_GRAY;
    }
    else
    {
      if (m_rows[nRow_inList].cells[i].nColorTxNew != m_rows[nRow_inList].cells[i].nColorTxNew_beforeDisable)
      {
        m_rows[nRow_inList].cells[i].nColorTxNew = m_rows[nRow_inList].cells[i].nColorTxNew_beforeDisable;
        m_rows[nRow_inList].cells[i].nColorTxNew_beforeDisable = -1;
      }
      CELL_BINDING b;
      if (!(m_rows[nRow_inList].cells[i].bindToTmp == b))
      {
        m_rows[nRow_inList].cells[i].bindTo = m_rows[nRow_inList].cells[i].bindToTmp;
        m_rows[nRow_inList].cells[i].bindToTmp = b; // clear
      }
    }

    if (i == 0 && m_bFirstColIsChkBoxes) // if first cell and it is checkbox
      continue;

    if (!IsCellEnable(nRow_inList, i))
    {
      nColor = (m_rows[nRow_inList].pNode != nullptr) ? 
        (IsNode_inner(nRow_inList) ? COLOR_LCE_RED_YELLOW2 : COLOR_LCE_RED_YELLOW) : 
        COLOR_LCE_LIGHT_GRAY;
    }
    else
    {
      nColor = (m_rows[nRow_inList].pNode != nullptr) ? 
        (IsNode_inner(nRow_inList) ? COLOR_LCE_LIGHT_YELLOW : COLOR_LCE_YELLOW) : 
        COLOR_LCE_WHITE;
    }
    COLORREF* clr = const_cast<COLORREF*>(&m_rows[nRow_inList].cells[i].nColorBg);
    *clr = nColor;
  }
}

void �Ai_ListCtrlEx_Table::SetCellEnblDsbl(int nRow_inList, int nCell, bool bDisable)
{
  m_bSetOnlyCell = true;
  m_nEnblDsblCell = nCell;
  SetRowEnblDsbl(nRow_inList, bDisable);
  m_nEnblDsblCell = -1;
  m_bSetOnlyCell = false;
}

///*******************************************************************
//Set children
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanSetChild(int nNodeRow_inList, int nCount, LONGLONG nStyle, BIND_TO_CELL* pBindToCells)
{
  nCount = nCount;
  nStyle = nStyle;
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nNodeRow_inList));
  RETURN_AST_FALSE_BY_TRUE(m_rows[nNodeRow_inList].pNode == nullptr);
  RETURN_AST_FALSE_BY_TRUE(!IsNode(nNodeRow_inList));
  RETURN_AST_FALSE_BY_TRUE(!IsDisableBindCorrect(pBindToCells));
  return true;
}

// ���������� nCntSetedChildren ���������� ����� ��� ���������� � ���� ��������
// nCntSetedChildren ���� ������ � ������ bSeted

int �Ai_ListCtrlEx::SetChild(int nNodeRow_inList, vector<CString>* pText, LONGLONG nRowStyle, BIND_TO_CELL* pBindToCells, bool bInnerNode)
{
  if (bInnerNode)
  {
    m_pTable->m_bSettingInnerNode = true;
    int nIdxNode = SetNode(pText, pBindToCells, false);
    m_pTable->m_bSettingInnerNode = false;

    m_pTable->m_rows[nIdxNode].nIdxParentNode_inList = nNodeRow_inList;
    m_pTable->ParentsAboveUpdate(nIdxNode, true);
    m_pTable->m_rows[nIdxNode].bSeted = true;
    m_pTable->m_rows[nIdxNode].bRowDisable = false;
    return nIdxNode;
  }
  int nRow_inList = SetEmptyChild(nNodeRow_inList, pText, nRowStyle, pBindToCells);
  ++m_pTable->m_rows[nNodeRow_inList].pNode->nCntSetedChildren;
  m_pTable->m_rows[nRow_inList].bSeted = true;
  m_pTable->m_rows[nRow_inList].bRowDisable = false; 
  return nRow_inList;
}

void �Ai_ListCtrlEx_Table::ParentsAboveUpdate(int nCurrParentRow_inList, bool bIncrSetedCount)
{
  int nIdxUpNode = m_rows[nCurrParentRow_inList].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(
  do
  {
    if (nIdxUpNode == -1)
      break;
    ++m_rows[nIdxUpNode].pNode->nCntChildren;
    if (bIncrSetedCount)
      ++m_rows[nIdxUpNode].pNode->nCntSetedChildren;
    nIdxUpNode = m_rows[nIdxUpNode].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(
  }
  while(true);
}

bool �Ai_ListCtrlEx_Table::IsAllParentsAboveUnwrapped(int nRow_inList)
{
  int nItParentNodeIdx = GetParentNodeIdx_inList(nRow_inList);
  if (nItParentNodeIdx == -1) // top node
    return m_rows[nItParentNodeIdx].pNode->bUnwrapped;
  int nAboveParentNodeIdx = m_rows[nItParentNodeIdx].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(

  do
  {
    if (nAboveParentNodeIdx == -1)
      break;
    if (!m_rows[nAboveParentNodeIdx].pNode->bUnwrapped)
      return false;
    nAboveParentNodeIdx = m_rows[nAboveParentNodeIdx].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(
  }
  while(true);
  return true;
}

void �Ai_ListCtrlEx_Table::AddInnerNodeTextOffset(int nRow_inList, int nCell, CString& strText)
{
  int nItParentNodeIdx = GetParentNodeIdx_inList(nRow_inList);
  if (nItParentNodeIdx == -1) // top node
    return;
  if (nCell != m_rows[nItParentNodeIdx].pNode->nComnSignificantCell)
    return;

  CString strOffset = _T("     "); // 5 spaces
  CString strTotalOffset;

  while (IsNode_inner(nItParentNodeIdx))
  {
    strTotalOffset += strOffset;
    nItParentNodeIdx = m_rows[nItParentNodeIdx].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(
  };
  strText = strTotalOffset + strText;
}

int �Ai_ListCtrlEx::SetChild(int nNodeRow_inList, CString strText, int nTextToCell)
{
  RETURN_AST_1_BY_OUT_OF_RANGE(nTextToCell, 0, (int)m_pTable->m_columns.size() - 1);

  vector<CString> cellText;
  
  if (nTextToCell == 0)
    cellText.push_back(strText);
  else
    for (int i = 0; i < nTextToCell + 1; ++i)
    {
      if (i == nTextToCell)
        cellText.push_back(strText);
      else
        cellText.push_back(_T(""));
    }

  return SetChild(nNodeRow_inList, &cellText);
}

int �Ai_ListCtrlEx::SetEmptyChild(int nNodeRow_inList, vector<CString>* pText, LONGLONG nRowStyle, BIND_TO_CELL* pBindToCells)
{
  RETURN_AST_1_BY_TRUE(pText == nullptr);
  RETURN_1_BY_TRUE(!m_pTable->CanSetChild(nNodeRow_inList, (int)pText->size(), nRowStyle, pBindToCells));

  �Ai_ListCtrlEx_Table::ROW row;
  �Ai_ListCtrlEx_Table::CELL cell;
  int nCntCol = (int)m_pTable->m_columns.size();
  int nOffs;
  
  for (int i = 0; i < nCntCol; ++i)
  {
    m_pTable->ClearCell(&cell);

    if (i < (int)pText->size())
      cell.strText = pText->at(i);

    if (pBindToCells)
      m_pTable->BindToCell(pBindToCells, i, &cell.bindTo);
    if (cell.bindTo.bDisableBind)
    {
      COLORREF* clr = const_cast<COLORREF*>(&cell.nColorBg);
      *clr = COLOR_LCE_LIGHT_GRAY;
      cell.bCellDisable = true;
    }

    row.cells.push_back(cell);
  }

  row.nStyle = nRowStyle;

  row.nIdxParentNode_inList = nNodeRow_inList;
  ++m_pTable->m_rows[nNodeRow_inList].pNode->nCntChildren;
  m_pTable->ParentsAboveUpdate(nNodeRow_inList, false);

  nOffs = nNodeRow_inList + m_pTable->m_rows[nNodeRow_inList].pNode->nCntChildren;
  row.nIdxOneself_inList = nOffs;
  
  m_pTable->m_showingRows.clear(); // �.�. ����� �������� ��������� �� ������������ ������, �� ��������� insert �������� ����� ��� ���������
  m_pTable->m_rows.insert(m_pTable->m_rows.begin() + nOffs, row);

  m_pTable->m_bChildIs = true;

  return (int)m_pTable->m_rows.size() - 1;
}

void �Ai_ListCtrlEx::SetEmptyChild(int nCountRows)
{
  m_pTable->m_bChildIs = true;
  m_pTable->m_showingRows.clear(); // �.�. ����� �������� ��������� �� ������������ ������, �� ��������� insert �������� ����� ��� ���������

  int nCntCol = (int)m_pTable->m_columns.size();
  �Ai_ListCtrlEx_Table::ROW row;
  //�Ai_ListCtrlEx_Table::CELL cell;
  for (int i = 0; i < nCntCol; ++i)
  {
    �Ai_ListCtrlEx_Table::CELL cell;
    //m_pTable->ClearCell(&cell);
    row.cells.push_back(cell);
  }
  row.nIdxParentNode_inList = 0;

  m_pTable->m_rows.reserve(nCountRows * 2);

  ++nCountRows;
  while (--nCountRows)
  {
    row.nIdxOneself_inList = ++m_pTable->m_rows[0].pNode->nCntChildren;
    m_pTable->m_rows.push_back(row);
  }
}

int �Ai_ListCtrlEx::MatchHeight(bool bScrollBar)
{
  ASSERT(m_pTable->m_bChildIs);
  if (!m_pTable->m_bChildIs)
    return 0;

  CRect r;
  m_pTable->m_pListCtrl->GetHeaderCtrl()->GetItemRect(0, r) ; // header
  int nHeaderHeight = r.Height();
  int nHeight = nHeaderHeight;
  int nItems = m_pTable->m_pListCtrl->GetItemCount();
  m_pTable->m_pListCtrl->GetSubItemRect(0, 0, LVIR_LABEL, r);
  int nRowHeight = r.Height();
  nHeight += nItems * nRowHeight;
  if (bScrollBar)
  {
    //SCROLLBARINFO scr = {0};
    //scr.cbSize = sizeof(SCROLLBARINFO);
    //CScrollBar* pScr = m_pTable->m_pListCtrl->GetScrollBarCtrl(SB_HORZ);
    //pScr->GetScrollBarInfo(&scr);
    //nHeight += (scr.rcScrollBar.bottom - scr.rcScrollBar.top); // ����� ��� ������ ���������, ���� ������� ������ �� �����������
    nHeight += 5;
    nHeight += nRowHeight;
  }
  
  m_pTable->m_pListCtrl->GetClientRect(&r);
  r.bottom = nHeight;
  m_pTable->m_pListCtrl->MoveWindow(&r);
  m_pTable->m_pListCtrl->Invalidate();

  return nHeight;
}

void �Ai_ListCtrlEx::ClearColumn(int nCell)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanWorkWithCell_list(0, nCell));
  for (size_t i = 0; i < m_pTable->m_rows.size(); ++i)
  {
    if (m_pTable->m_rows[i].pNode == NULL)
    {
      m_pTable->m_rows[i].cells[nCell].strText = L"";
    }
  }
}

void �Ai_ListCtrlEx::ClearRows()
{
  for (size_t i = 0; i < m_pTable->m_rows.size(); ++i)
  {
    if (m_pTable->m_rows[i].pNode != nullptr)
    {
      delete m_pTable->m_rows[i].pNode;
      m_pTable->m_rows[i].pNode = nullptr;
      m_pTable->m_rows[i].serviceInfo.clear();
      m_pTable->m_rows[i].cells.clear();
    }
  }
  m_pTable->m_showingRows.clear();
  m_pTable->m_rows.clear();
}

void �Ai_ListCtrlEx::SetCommonStyle(LONGLONG nStyle)
{
  m_pTable->m_nCommonStyle = nStyle;
}

void �Ai_ListCtrlEx::SetRowStyle(int nRow_inList, LONGLONG nStyle)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nRow_inList));
  m_pTable->m_rows[nRow_inList].nStyle |= nStyle;
}

///*******************************************************************
//Work with child
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanInsertTextToChild(int nRow_inView, int nCell, vector<CString>* pServiceInfo, LONGLONG *pnFlag)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_inView, 0, (int)m_showingRows.size() - 1);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  if (!(*pnFlag & CHIF_TO_EMPTY_TAIL) && !(*pnFlag & CHIF_FILL_IN_GAPS))
    *pnFlag |= CHIF_TO_EMPTY_TAIL | CHIF_FILL_IN_GAPS;
  if (nCell != -1
    && (*pnFlag & CHIF_SRVS_INFO_ADD) || (*pnFlag & CHIF_SRVC_INFO_UPDATE))
  {
    RETURN_AST_FALSE_BY_TRUE(pServiceInfo == nullptr);
  }
  RETURN_FALSE_BY_TRUE(!IsRowEnable(m_showingRows[nRow_inView]->nIdxOneself_inList)); // exit without assert
  return true;
}

int �Ai_ListCtrlEx::GetInsertedIdxInFuture(int nRow_inView, LONGLONG nFlag)
{
  RETURN_AST_1_BY_TRUE(!m_pTable->CanWorkWithRow_view(nRow_inView, false));

  int nNodeRow_InList = m_pTable->IsNode(m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList) ?
    m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList :
    m_pTable->GetParentNodeIdx_inView(nRow_inView); // from view to rows list

  RETURN_AST_1_BY_TRUE(!(nNodeRow_InList >= 0));

  �Ai_ListCtrlEx_Table::ROW* pNodeRow = &m_pTable->m_rows[nNodeRow_InList];
  m_pTable->m_pInsNode = pNodeRow->pNode;

  if (nFlag & CHIF_TO_EMPTY_TAIL &&
    m_pTable->m_pInsNode->nCntChildren == m_pTable->m_pInsNode->nCntSetedChildren)
  {
    if (nFlag & CHIF_MSG_IF_NODE_FULL)
    {
      CString strMsg;
      int nNodeRow_inList = m_pTable->m_rows[nNodeRow_InList].nIdxOneself_inList;
      CString strUnwrapCellText = m_pTable->DeleteUnwrapCellMark(nNodeRow_inList, pNodeRow->cells[m_pTable->m_pInsNode->nUnwrapCell].strText);

      strMsg.Format(IDS_LIST_FULL, strUnwrapCellText); // ������ ��������!\n���������� �������� ����� ������� � ������:\n\n\"%s\"!
      AfxMessageBox(strMsg, MB_ICONERROR);
    }
    return RCHI_LIST_FULL;
  }

  int nIdxNewChild = -1;
  if ((nFlag & CHIF_TO_CURR_IF_EMPTY)
      && m_pTable->m_showingRows[nRow_inView]->pNode == nullptr // child
      && m_pTable->m_showingRows[nRow_inView]->cells[m_pTable->m_pInsNode->nComnSignificantCell].strText.IsEmpty())
  {
    bool bGet = true;
    if (nFlag & CHIF_COUPLE_ROWS)
    {
      ASSERT(false); //...outstanding 
    }

    if (bGet)
      nIdxNewChild = m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList;
  }
  else
  if ((nFlag & CHIF_FILL_IN_GAPS))
  {
    �Ai_ListCtrlEx_Table::SEARCH_IN_NODE_CHILDREN flagSearch = �Ai_ListCtrlEx_Table::searchFirstGapIndex;
    if (nFlag & CHIF_COUPLE_ROWS)
      flagSearch = �Ai_ListCtrlEx_Table::searchFirstGapIndex_couple;
    nIdxNewChild = m_pTable->SearchInNodeChildren(flagSearch, nNodeRow_InList); // first of empties
  }
  else // search in tail
  {
    if (nFlag & CHIF_COUPLE_ROWS)
    {
      ASSERT(false); //...outstanding 
    }

    nIdxNewChild = m_pTable->SearchInNodeChildren(�Ai_ListCtrlEx_Table::searchFreeTailIndex, nNodeRow_InList); // last empty in tail
  }

  return nIdxNewChild;
}

/*
  /// �������
  ������� ������ � �������� ������ ����.
  /// ���������
  ��������� ����� strText � ��������� ������ nCell.
  ������ ��� ������� �������� nRow_inView ��� ���� nRow_inListInFuture ������ ���� -1. ����� �������� ������������ ������
  ��� ������� nRow_inView ����������� �������� GetInsertedIdxInFuture(...). ��� ���� ���� ����� ��������� ������������ ��������
  �� "Child inserted flags".
  ���� nRow_inListInFuture != -1 �������� nRow_inView ������ ���� ������� ���� ��� �������.
  ���� ���� ����� NS_USE_COMN_SIGNIFICANT_CELL � �������� nCell ��������� �� ����� �������� ������ (�������), ��
  ������ ������� ��������� �������������� ������ ��������� ��������� ����.
  � ����������� ������� ����� ��������� ��������� ����������, ������������������ �������� pServiceInfo � ����������� nFlag � 
  ��������� CHIF_SRVC_INFO_UPDATE. ���� nFlag ����������� � ��������� CHIF_SRVS_INFO_ADD, �� ��������� ����������
  ��������� � ���������.
  /// ������������ ��������
  -1 - ������ � ����������
  RCHI_LIST_FULL - ���� ��������
  RCHI_SUCCESS - ������� ��������� �������

*/
int �Ai_ListCtrlEx::InsertTextToChild(int nRow_inView, int nCell, CString strText, vector<CString>* pServiceInfo, LONGLONG nFlag, int nRow_inListInFuture)
{
  RETURN_1_BY_TRUE(!m_pTable->CanInsertTextToChild(nRow_inView, nCell, pServiceInfo, &nFlag));

  m_pTable->m_pInsNode = nullptr;
  int nIdxNewChild = nRow_inListInFuture;

  if (nIdxNewChild == -1)
    nIdxNewChild = GetInsertedIdxInFuture(nRow_inView, nFlag);
  if (nIdxNewChild == -1
    || nIdxNewChild == RCHI_LIST_FULL)
    return nIdxNewChild;

  if (m_pTable->m_pInsNode == nullptr)
  {
    int nNodeRow = m_pTable->GetParentNodeIdx_inView(nRow_inView); // from view to rows list
    RETURN_AST_1_BY_TRUE(!(nNodeRow >= 0));
    �Ai_ListCtrlEx_Table::ROW* pNodeRow = &m_pTable->m_rows[nNodeRow];
    m_pTable->m_pInsNode = pNodeRow->pNode;
  }

  m_pTable->m_rows[nIdxNewChild].cells[nCell].strText = strText;
  m_pTable->m_nLastInsertedNodeChild = nIdxNewChild;
  if (m_pTable->m_pInsNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL)
  {
    if (nCell == m_pTable->m_pInsNode->nComnSignificantCell)
    {
      ++m_pTable->m_pInsNode->nCntSetedChildren;
      m_pTable->m_rows[nIdxNewChild].bSeted = true;
      m_pTable->m_rows[nIdxNewChild].bRowDisable = false; 
    }
  }
  m_pTable->SetMeansInCtrlsForChild(nIdxNewChild);
  m_pTable->CheckUnwrapNodeMark(m_pTable->GetParentNodeIdx_inList(nIdxNewChild), m_pTable->m_pInsNode->nUnwrapCell);

  if (nIdxNewChild >= 0 && pServiceInfo != nullptr)
    InsertServiceInfo(nIdxNewChild, pServiceInfo, nFlag);

  return RCHI_SUCCESS;
}

int �Ai_ListCtrlEx::GetLastInsertedChildIdx_inList()
{
  RETURN_AST_1_BY_TRUE(!m_pTable->IsNode(0));
  return m_pTable->m_nLastInsertedNodeChild;
}

bool �Ai_ListCtrlEx_Table::CanInsertServiceInfo(int nRow_inList, vector<CString>* pServiceInfo, LONGLONG nFlag)
{
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nRow_inList));
  RETURN_AST_FALSE_BY_TRUE(pServiceInfo == nullptr);
  if (!(nFlag & CHIF_SRVS_INFO_ADD) && !(nFlag & CHIF_SRVC_INFO_UPDATE))
  {
    RETURN_AST_FALSE_BY_TRUE(true);
  }
  return true;
}

void �Ai_ListCtrlEx::InsertServiceInfo(int nRow_inList, vector<CString>* pServiceInfo, LONGLONG nFlag)
{
  RETURN_BY_TRUE(!m_pTable->CanInsertServiceInfo(nRow_inList, pServiceInfo, nFlag));

  if (nFlag & CHIF_SRVC_INFO_UPDATE)
  {
    m_pTable->m_rows[nRow_inList].serviceInfo.clear();
    nFlag = CHIF_SRVS_INFO_ADD;
  }
  if (nFlag & CHIF_SRVS_INFO_ADD)
  {
    int nCntServ = (int)pServiceInfo->size();

    for (int i = 0; i < nCntServ; ++i)
      m_pTable->m_rows[nRow_inList].serviceInfo.push_back((*pServiceInfo)[i]);
  }
}

void �Ai_ListCtrlEx::InsertServiceInfoToCell(int nRow_inList, int nCell, vector<CString>* pServiceInfo, LONGLONG nFlag)
{
  RETURN_BY_TRUE(!m_pTable->CanInsertServiceInfo(nRow_inList, pServiceInfo, nFlag));
  RETURN_AST_BY_OUT_OF_RANGE(nCell, 0, (int)m_pTable->m_columns.size() - 1);

  if (nFlag & CHIF_SRVC_INFO_UPDATE)
  {
    m_pTable->m_rows[nRow_inList].cells[nCell].serviceInfo.clear();
    nFlag = CHIF_SRVS_INFO_ADD;
  }
  if (nFlag & CHIF_SRVS_INFO_ADD)
  {
    int nCntServ = (int)pServiceInfo->size();

    for (int i = 0; i < nCntServ; ++i)
      m_pTable->m_rows[nRow_inList].cells[nCell].serviceInfo.push_back((*pServiceInfo)[i]);
  }
}

bool �Ai_ListCtrlEx_Table::CanGetServiceInfoFromCell(int nRow_inList, int nCell, void* pServiceInfo)
{
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nRow_inList));
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  RETURN_AST_FALSE_BY_TRUE(pServiceInfo == nullptr);
  return true;
}

bool �Ai_ListCtrlEx::GetCellServiceInfo(int nRow_inList, int nCell, vector<CString>* pServiceInfo)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanGetServiceInfoFromCell(nRow_inList, nCell, pServiceInfo));

  int nCntServ = (int)m_pTable->m_rows[nRow_inList].cells[nCell].serviceInfo.size();
  
  if (nCntServ == 0)
    return false;

  pServiceInfo->clear();
  for (int i = 0; i < nCntServ; ++i)
    pServiceInfo->push_back(m_pTable->m_rows[nRow_inList].cells[nCell].serviceInfo[i]);
  return true;
}

bool �Ai_ListCtrlEx_Table::CanInsertTextToChildCells(int nChildRow_inList, LONGLONG nFlag)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nChildRow_inList, 0, (int)m_rows.size() - 1);
  if (!(nFlag & CHIF_SRVS_INFO_ADD) && !(nFlag & CHIF_SRVC_INFO_UPDATE))
  {
    RETURN_AST_FALSE_BY_TRUE(true);
  }
  RETURN_FALSE_BY_TRUE(!IsRowEnable(nChildRow_inList)); // exit without assert
  return true;
}

int �Ai_ListCtrlEx::InsertTextToChildCells(int nChildRow_inList, vector<CString>* pstrText, vector<CString>* pServiceInfo, LONGLONG nSrvcFlag)
{
  RETURN_1_BY_TRUE(!m_pTable->CanInsertTextToChildCells(nChildRow_inList, nSrvcFlag));
  
  int nNodeRow = m_pTable->GetParentNodeIdx_inList(nChildRow_inList);
  �Ai_ListCtrlEx_Table::ROW* pNodeRow = &m_pTable->m_rows[nNodeRow];
  if (pNodeRow->pNode->nCntSetedChildren == pNodeRow->pNode->nCntChildren)
    return RCHI_LIST_FULL;
  
  if (pServiceInfo != nullptr)
    InsertServiceInfo(nChildRow_inList, pServiceInfo, nSrvcFlag);
  if (pstrText != nullptr)
  {
    int nCntText = (int)pstrText->size();
    bool bUpdRow = false;
    bool bUpdCtrl = false;

    for (int i = 0; i < nCntText; ++i)
    {
      if (pstrText->at(i) != CHIFS_NO_WRITE_TO_CELL)
      {
        //if (pstrText->at(i).IsEmpty() && // text for set is not
        //  pNodeRow->pNode != nullptr &&
        //  (pNodeRow->pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL) && // common significant cells are
        //  i == pNodeRow->pNode->nComnSignificantCell) // cell for set is a common significant cell
        //  continue; // �.�. ���� �� ������ �������� ������ � ������ �� ��� �������� ������ ����� �� �������, �� �������
        // ��� ���� ������ ������������. ������ ��� �������������� ���� ������� ��������� ����� �������� �����. � ������� 
        // �������� ��� common significant cell ������������� ��������� ��� ������ ��� ����������������������� � ������ ��.
        // ��� ������� ������ ���������� ��������������� �������� � ������ ������ �����, ���������������� �������� SetCellText;
        // ���� ��������� ������� �������� ClearChild

        m_pTable->m_rows[nChildRow_inList].cells[i].strText = pstrText->at(i);
        if (!bUpdCtrl)
          bUpdCtrl = true;
        if (!m_pTable->m_rows[nChildRow_inList].bSeted)
        {
          m_pTable->m_rows[nChildRow_inList].bSeted = true;
          m_pTable->m_rows[nChildRow_inList].bRowDisable = false;
        }

        if (!bUpdRow && pNodeRow->pNode != nullptr)
        {
          bUpdRow = true;
          ++pNodeRow->pNode->nCntSetedChildren;
          m_pTable->CheckUnwrapNodeMark(pNodeRow->nIdxOneself_inList, pNodeRow->pNode->nUnwrapCell); // InsertTextToChildCells (change [] on [+])
        }
      }
    }

    if (bUpdCtrl && pNodeRow)
      m_pTable->UpdateCellCtrlsAfterSetNewMeansInCells(nChildRow_inList);
  }

  return RCHI_SUCCESS;
}

bool �Ai_ListCtrlEx_Table::CanClearChild(int nRow_inView, int* pnFlag)
{
  pnFlag = pnFlag;
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  RETURN_AST_FALSE_BY_TRUE(nRow_inView < -1);
  return true;
}

// if nRow_inView == -1 - clear all selected rows
bool �Ai_ListCtrlEx::ClearChild(int nRow_inView, int nFlag, vector<int>* pClearCellsList, int nEnumCells, ...)
{
  pClearCellsList = pClearCellsList;
  m_refinedList_inList.clear();

  RETURN_AST_FALSE_BY_TRUE(!m_pTable->CanClearChild(nRow_inView, &nFlag));

  bool bWasClear = false;
  �Ai_ListCtrlEx_Table::NODE_ROW* pNode = nullptr;
  vector<int> params;
  vector<int>* pClrCells = nullptr;

  if ((nFlag & CRF_BY_INIT_CHAIN))
  {
    va_list arg;
    va_start(arg, nEnumCells);
    int nColumn = 0;

    while ((nColumn = va_arg(arg, int)) != -1
      || (int)params.size() < nEnumCells)
    {
      RETURN_AST_FALSE_BY_OUT_OF_RANGE(nColumn, 0, (int)m_pTable->m_columns.size() - 1);
      params.push_back(nColumn);
    }
    if (params.empty())
      return false;
    pClrCells = &params;
  }
  else
  {
    if (pClearCellsList == nullptr)
      return false;
    params = *pClearCellsList;
    if (params.empty())
      return false;
    pClrCells = &params;
  }

  ASSERT(pClrCells);

  vector<int> items;

  if (nRow_inView >= 0) // clear one determined row
  {
    int nNode_inView = m_pTable->GetParentNodeIdx_inView(nRow_inView);
    /*if (GetSetedCountOfChild(nNode_inView) > 0)
    {
      bool bMouseMove = false;
      SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, 0, bMouseMove);
    }*/

    pNode = m_pTable->m_rows[nNode_inView].pNode;
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_inView, 0, (int)m_pTable->m_showingRows.size() - 1);
    pNode = m_pTable->m_rows[nNode_inView].pNode;
    RETURN_AST_FALSE_BY_TRUE(pNode == nullptr);
    if (m_pTable->m_showingRows[nRow_inView]->pNode) // it`s node
      return false;
    if (m_pTable->m_showingRows[nRow_inView]->nStyle & CHS_CONST_CLEAR_CHILD)
      return false;
    if (nFlag & CRF_IGNORE_DISABLE && m_pTable->m_showingRows[nRow_inView]->bRowDisable)
      return false;
    if (pNode->nComnSignificantCell > -1 // � ���� ������ �������� ������ (������������� - ��� ������� child ������ ����������� ��������� ��� ������)
        && find(pClrCells->begin(), pClrCells->end(), pNode->nComnSignificantCell) == pClrCells->end()) // �� � ������� ����� �� ������� �������� �� ������
    {
      ASSERT(true);
      RETURN_AST_FALSE_BY_TRUE(true); // ������ �� ������, �.�. ����������� ����� �� ����� �� �������, ����� �� ����������� � ������ �������
    }
    int nCntCells = (int)pClrCells->size();
    for (int j = 0; j < nCntCells; ++j)
    {
      m_pTable->m_showingRows[nRow_inView]->cells[(*pClrCells)[j]].strText = _T("");
      m_pTable->m_showingRows[nRow_inView]->cells[(*pClrCells)[j]].nColorTxNew = -1;
    }
    m_pTable->CommonSignificantCellWasCleared(nRow_inView);
    m_refinedList_inList.push_back(m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList);

  }
  else
  if (nRow_inView == -1) // clear all selected rows
  {
    if (nFlag & CRF_GET_ALL_ROWS)
    {
      int nCnt = m_pTable->m_pListCtrl->GetItemCount();
      for (int itm = 0; itm < nCnt; ++itm)
        items.push_back(itm);
    }
    else
    if (!m_pTable->GetSelectedItems(&items))
      return false;
    else
    if ((int)items.size() == 0)
      return false;

    // check what selected items can be cleared
    int nCntItems = (int)items.size();
    for (int i = 0; i < nCntItems; ++i)
      if ((m_pTable->m_showingRows[items[i]]->nStyle & CHS_CONST_CLEAR_CHILD)
        || (nFlag & CRF_IGNORE_DISABLE && m_pTable->m_showingRows[items[i]]->bRowDisable))
      {
        items.erase(items.begin() + i);
        nCntItems--;
        i--;
      }

    if ((int)items.size() == 0)
      return false;
    
    nCntItems = (int)items.size();
    int nCntCells;
    int nIdxNodeRow;
  int nIdxShowingNodeRow;

    for (int i = 0; i < nCntItems; ++i)
    {
      if (m_pTable->m_showingRows[items[i]]->pNode != nullptr) // it`s node
        continue;
      if (!m_pTable->m_showingRows[items[i]]->bSeted)
        continue;
      if (nFlag & CRF_BY_INIT_CHAIN)
      {
        nCntCells = (int)pClrCells->size();
        nIdxNodeRow = m_pTable->GetParentNodeIdx_inView(items[i]);

        bool bWasComnSignificantCellCleared = false;
        for (int j = 0; j < nCntCells; ++j)
        {
          if (nFlag & CRF_IGNORE_DISABLE
            //&& m_pTable->m_showingRows[items[i]]->cells[(*pClrCells)[j]].bCellDisable)
            && !m_pTable->IsCellEnable(m_pTable->m_showingRows[items[i]]->nIdxOneself_inList, (*pClrCells)[j]))
            continue;

          m_pTable->m_showingRows[items[i]]->cells[(*pClrCells)[j]].strText = _T("");
          m_pTable->m_showingRows[items[i]]->cells[(*pClrCells)[j]].nColorTxNew = -1;

          if ((*pClrCells)[j] == m_pTable->m_rows[nIdxNodeRow].pNode->nComnSignificantCell)
            bWasComnSignificantCellCleared = true;
        }

        m_refinedList_inList.push_back(m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList);

        if (!bWasComnSignificantCellCleared)
          continue;
        //m_pTable->CommonSignificantCellWasCleared(items[i]);
      m_pTable->m_showingRows[items[i]]->serviceInfo.clear();
      m_pTable->m_showingRows[items[i]]->bSeted = false;
      m_refinedList_inList.push_back(m_pTable->m_showingRows[items[i]]->nIdxOneself_inList);

      nIdxShowingNodeRow = m_pTable->m_rows[nIdxNodeRow].nIdx_inView;
      pNode = m_pTable->m_rows[nIdxNodeRow].pNode;
      pNode->nCntSetedChildren--;

      if (pNode->nCntSetedChildren == 0)
      {
        m_pTable->CheckUnwrapNodeMark(nIdxNodeRow, pNode->nUnwrapCell); // clear child
        m_pTable->ClearNodeCtrlCells(nIdxShowingNodeRow);
      }		
        if (bWasClear)
          m_pTable->UnselectRow(items[i]);        
        bWasClear = true;
      }
    }
  }

  if (bWasClear)
    m_pTable->SelectAndShowThisRow(items[0], false);
  return bWasClear;
}

bool �Ai_ListCtrlEx_Table::CanShowAndSelectChild(�Ai_ListCtrlEx::SHOW_AND_SELECT_CHILD nFlag, int nRow_inView)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  if ((nFlag != �Ai_ListCtrlEx::shaschSpecified) 
    && (nFlag != �Ai_ListCtrlEx::shaschLastChildBySelectedRow)
    && (nFlag != �Ai_ListCtrlEx::shaschLastInserted))
  {
    RETURN_AST_FALSE_BY_TRUE(true);
  }

  if (nFlag == �Ai_ListCtrlEx::shaschSpecified)
  {
    RETURN_FALSE_BY_TRUE(!CanWorkWithRow_view(nRow_inView)); // exit without assert (assert in call function)
  }
  else
  if (nFlag == �Ai_ListCtrlEx::shaschLastChildBySelectedRow)
  {
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_inView, 0, (int)m_showingRows.size() - 1);
  }

  return true;
}

// if nRow_inView == -1 => will select LastInsertedNodeChild
int �Ai_ListCtrlEx::SelectAndShowChild(SHOW_AND_SELECT_CHILD nFlag, int nRow_inView, bool bMoveMouse)
{
  RETURN_1_BY_TRUE(!m_pTable->CanShowAndSelectChild(nFlag, nRow_inView));
  if (nFlag == shaschLastInserted)
  {
    RETURN_1_BY_TRUE(m_pTable->m_nLastInsertedNodeChild < 0);
  }

  int nRow_inList;
  if (nRow_inView == -1)
    nRow_inList = m_pTable->m_rows[m_pTable->m_nLastInsertedNodeChild].nIdxOneself_inList;
  else
    nRow_inList = m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList;
  m_pTable->m_nLastInsertedNodeChild = -1;

  �Ai_ListCtrlEx_Table::ROW* pNodeRow = nullptr;
  �Ai_ListCtrlEx_Table::NODE_ROW* pNode = nullptr;

  if (nFlag == shaschSpecified)
    pNodeRow = &m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)];
  else
  if (nFlag == shaschLastChildBySelectedRow)
    pNodeRow = &m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)];
  else
  if (nFlag == shaschLastInserted)
    pNodeRow = &m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)];
  RETURN_AST_1_BY_TRUE(pNodeRow == nullptr);
  pNode = pNodeRow->pNode;
  RETURN_AST_1_BY_TRUE(pNode == nullptr);
  if (pNode->nCntChildren == 0)
    return -1;
  
  if (!pNode->bUnwrapped)
    pNode->bUnwrapped = true;
  m_pTable->CheckUnwrapNodeMark(pNodeRow->nIdxOneself_inList, pNode->nUnwrapCell); // select and show
  UpdateView();

  if (nRow_inView == -1)
    nRow_inView = m_pTable->m_rows[nRow_inList].nIdx_inView;
  RETURN_AST_1_BY_TRUE(nRow_inView == -1);

  if (pNode->nCntSetedChildren > 0)
  {
    if (nFlag == shaschLastChildBySelectedRow)
    {
      nRow_inView = pNodeRow->nIdx_inView + 
        m_pTable->SearchInNodeChildren(�Ai_ListCtrlEx_Table::searchLastSetedChild, m_pTable->GetParentNodeIdx_inView(nRow_inView));
      m_pTable->SelectAndShowThisRow(nRow_inView, bMoveMouse);
    }
    else
    if (nFlag == shaschLastInserted)
      m_pTable->SelectAndShowThisRow(nRow_inView, bMoveMouse);
    else
    if (nFlag == shaschSpecified)
      m_pTable->SelectAndShowThisRow(nRow_inView, bMoveMouse);
  }
  return nRow_inView;
}

bool �Ai_ListCtrlEx_Table::CanMoveUpDownSelectedChildren(bool bUp, int nFlag, vector<int>* pMovingCellsList, int nEnumCells, vector<int>* pSelItems)//, int& nIdxFrom, int& nIdxTo)
{
  bUp = bUp;
  switch (nFlag)
  {
    case MCHF_USE_ENUM_LIST:
    case MCHF_USE_INVERS_ENUM_LIST: break;
    default:
      {
        RETURN_AST_FALSE_BY_TRUE(false);
      }
      break;
  };

  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  if (pMovingCellsList == nullptr)
  {
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nEnumCells, 0, (int)m_columns.size() - 1);
  }
  else
  {
    int nMoveList = (int)pMovingCellsList->size();

    RETURN_AST_FALSE_BY_TRUE(nMoveList == 0);
    for (int i = 0; i < nMoveList; ++i)
    {
      RETURN_AST_FALSE_BY_OUT_OF_RANGE((*pMovingCellsList)[i], 0, (int)m_columns.size() - 1);
    }
  }

  // * Check selected rows

  int nCntItems = 0;

  if (!GetSelectedItems(pSelItems))
    return false;

  // check what selected items can be moving
  nCntItems = (int)pSelItems->size();
  for (int i = 0; i < nCntItems; ++i)
    if ((m_showingRows[pSelItems->at(i)]->nStyle & CHS_CONST_MOVE_CHILD) // CHS_CONST_MOVE_CHILD - delete node least
      || (nFlag & MCHF_COPY_IGNORE_DISABLE
          && !IsRowEnable(m_showingRows[pSelItems->at(i)]->nIdxOneself_inList)))
    {
      pSelItems->erase(pSelItems->begin() + i);
      nCntItems--;
      i--;
    }

  if ((int)pSelItems->size() == 0)
      return false;

  // check what selected items are children and have common node
  nCntItems = (int)pSelItems->size();
  for (int i = 0, nIdxParentNode_inList = 0; i < nCntItems; ++i)
  {
    if (i > 0 && (nIdxParentNode_inList != GetParentNodeIdx_inView(pSelItems->at(i))))
      return false;
    nIdxParentNode_inList = GetParentNodeIdx_inView(pSelItems->at(i));

    if (m_showingRows[pSelItems->at(i)]->pNode != nullptr)
      return false;
  }

  return true;
}

// nEnumCells - ����� ������� �����, ������� ����� ������������ � ��������� "..."
// "..." - ������������ �����, ������� ��� ����������� �� ����� ������������
void �Ai_ListCtrlEx::MoveSelectedChildren(bool bUp, int nCopyFlag, int nFlag, vector<int>* pMovingCellsList, int nEnumCells, ...)
{
  vector<int> selItems;
  RETURN_BY_TRUE(!m_pTable->CanMoveUpDownSelectedChildren(bUp, nFlag, pMovingCellsList, nEnumCells, &selItems))

  vector<int> params;
  vector<int>* pMovCells;

  if (nFlag == MCHF_USE_ENUM_LIST
    || nFlag == MCHF_USE_INVERS_ENUM_LIST)
  {
    va_list arg;
    va_start(arg, nEnumCells);
    int nColumn = 0;
    int nCntParams = 0;

    while ((nColumn = va_arg(arg, int)) != -1
      || (int)params.size() < nEnumCells)
    {
      RETURN_AST_BY_OUT_OF_RANGE(nColumn, 0, (int)m_pTable->m_columns.size() - 1);
      params.push_back(nColumn);
    }
    nCntParams = (int)params.size();
    if (nCntParams == 0)
      return;
    pMovCells = &params;
  }
  else
    pMovCells = pMovingCellsList;

  if (m_pTable->MoveUpDownChildren(bUp, nCopyFlag, nFlag, pMovCells, &selItems))
  {
    UpdateView();
    m_pTable->SelectThisRows(&selItems);
  }
}

int �Ai_ListCtrlEx_Table::GetParentNodeIdx_inList(int nRow_inList)
{
  if (!IsNode(nRow_inList))
    return m_rows[nRow_inList].nIdxParentNode_inList;
  return nRow_inList;
}

// return nNodeRow_InList
int �Ai_ListCtrlEx_Table::GetParentNodeIdx_inView(int nRow_inView)
{
  if (!IsNode(m_showingRows[nRow_inView]->nIdxOneself_inList))
    return m_showingRows[nRow_inView]->nIdxParentNode_inList;
  return m_showingRows[nRow_inView]->nIdxOneself_inList;
}

bool �Ai_ListCtrlEx_Table::CanUpdateView()
{
  //RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  return true;
}

void �Ai_ListCtrlEx_Table::ShowRow(ROW* pRow)
{
   m_showingRows.push_back(pRow);
}

void �Ai_ListCtrlEx_Table::ClearShowingRows()
{
  for (size_t i = 0; i < m_showingRows.size(); ++i)
    for (size_t c  = 0; c < m_showingRows[i]->cells.size(); ++c)
      if (m_showingRows[i]->cells[c].imgType != iltNone
        || m_showingRows[i]->cells[c].nImgIdx != -1)
      {
        m_showingRows[i]->cells[c].bUpdateImg = true;
        CheckCellImg(i, c, false);
        m_showingRows[i]->cells[c].bUpdateImg = true;
      }
  m_showingRows.clear();
}

bool �Ai_ListCtrlEx_Table::IsCellEnable(int nRow_inList, int nCell)
{
  bool bNodeDisable = false;
  if (IsNode(nRow_inList))
    bNodeDisable = m_rows[nRow_inList].pNode->bNodeDisable;
  else
    bNodeDisable = m_rows[m_rows[nRow_inList].nIdxParentNode_inList].pNode->bNodeDisable;

  if (m_bTableEnable
    && !bNodeDisable
    && !m_rows[nRow_inList].bRowDisable
    && !m_rows[nRow_inList].cells[nCell].bCellDisable)
    return true;
  return false;
}

bool �Ai_ListCtrlEx_Table::IsRowEnable(int nRow_inList)
{
  bool bNodeDisable = false;
  if (IsNode(nRow_inList))
    bNodeDisable = m_rows[nRow_inList].pNode->bNodeDisable;
  else
    bNodeDisable = m_rows[m_rows[nRow_inList].nIdxParentNode_inList].pNode->bNodeDisable;

  if (m_bTableEnable
    && !bNodeDisable
    && !m_rows[nRow_inList].bRowDisable)
    return true;
  return false;
}

void �Ai_ListCtrlEx_Table::CheckCellImg(int nRow_inView, int nCell, bool bShow)
{
  if (nRow_inView < 0)
    return;

  if (m_showingRows[nRow_inView]->cells[nCell].bUpdateImg)
  {
    m_showingRows[nRow_inView]->cells[nCell].bUpdateImg = false;
   
    LVITEM lvitem = {0};
    lvitem.mask = LVIF_IMAGE;
    lvitem.iItem = nRow_inView;
    lvitem.iSubItem = nCell;
    /*if (nCell == 0)
      lvitem.iImage = I_IMAGENONE;
    else*/
    {
      int i = -1;
      ImageListType ilt = m_showingRows[nRow_inView]->cells[nCell].imgType;
      if (ilt != iltNone)
      {
        bool bCellEnable = IsCellEnable(m_showingRows[nRow_inView]->nIdxOneself_inList, nCell);
        if (!bCellEnable && ilt == iltOnEnable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOnDisable;
        else
        if (!bCellEnable && ilt == iltOnDisable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOnEnable;
        else
        if (!bCellEnable && ilt == iltOnAdvEnable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOnAdvDisable;
        else
        if (!bCellEnable && ilt == iltOnAdvDisable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOnAdvEnable;
        else
        if ( !bCellEnable && ilt == iltOffEnable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOffDisable;
        else
        if (!bCellEnable && ilt == iltOffDisable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOffEnable;
        else
        if (!bCellEnable && ilt == iltOffAdvEnable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOffAdvDisable;
        else
        if (!bCellEnable && ilt == iltOffAdvDisable)
          ilt = m_showingRows[nRow_inView]->cells[nCell].imgType = iltOffAdvEnable;

        i = GetIdxImgListType(ilt);
      }
      else
        i = m_showingRows[nRow_inView]->cells[nCell].nImgIdx;
      lvitem.iImage = bShow ? i : -1;
    }
    m_pListCtrl->SetItem(&lvitem);
  }
}

bool �Ai_ListCtrlEx_Table::IsClkOnImg(int nRow_inView, int nCell, POINT* p) 
{
  LVITEM lvitem = {0};
  lvitem.iImage = -1;
  lvitem.mask = LVIF_IMAGE;
  lvitem.iItem = nRow_inView;
  lvitem.iSubItem = nCell;
  m_pListCtrl->GetItem(&lvitem);
  if (lvitem.iImage == -1)
    return false;
  if ((ImageListType)lvitem.iImage == iltNone)
    return false;

  CRect r;
  m_pListCtrl->GetSubItemRect(nRow_inView, nCell, LVIR_LABEL, r);
  if (p->x <= (r.left + m_nImgesWidth))
  {
    if (lvitem.iImage == -1)
      return true;

    m_pListCtrlExParent->m_bindInfo.imgWas = (ImageListType)lvitem.iImage;

    ImageListType newImg = iltNone;
    if ((ImageListType)lvitem.iImage == iltOnEnable)
      newImg = iltOffEnable;
    if ((ImageListType)lvitem.iImage == iltOnAdvEnable)
      newImg = iltOffAdvEnable;
    if ((ImageListType)lvitem.iImage == iltOffEnable)
      newImg = iltOnEnable;
    if ((ImageListType)lvitem.iImage == iltOffAdvEnable)
      newImg = iltOnAdvEnable;
    
    if (newImg != iltNone)
    {
      lvitem.iImage = newImg;
      m_showingRows[nRow_inView]->cells[nCell].imgType = newImg;
      m_pListCtrl->SetItem(&lvitem);
    }

    m_pListCtrlExParent->m_bindInfo.imgNew = (ImageListType)lvitem.iImage;
    return true;
  }
  return false;
}

int �Ai_ListCtrlEx_Table::GetIdxImgListType(ImageListType ilt)
{
  for (size_t i = 0; i < m_bmps.size(); ++i)
    if (m_bmps[i].imgType == ilt)
      return m_bmps[i].nIdxInImageList;
  return -1;
}

void �Ai_ListCtrlEx::UpdateView()
{
  RETURN_BY_TRUE(!m_pTable->CanUpdateView());
  
  m_pTable->m_bUpdating = true;

  int nCntRows = (int)m_pTable->m_rows.size();
  LONGLONG nNodeStyle = 0;
  bool bUnwrapped = true;
  int nComnSignificantCell = 0;

  // ���������� m_showingRows

  m_pTable->ClearShowingRows();

  for (int i = 0; i < nCntRows; ++i)
  {
    // ��� ���������

    m_pTable->m_rows[i].nIdx_inView = -1;
    m_pTable->m_rows[i].bShowing = false;

    // ��������� ����

    if (m_pTable->m_rows[i].pNode != nullptr)
    {

      nComnSignificantCell = m_pTable->m_rows[i].pNode->nComnSignificantCell; // �������� ������, �������� ������� (������� � ������� � ��������� 
      // �������� ������, ���� �� �������) ����� ���������� ���� �������
      nNodeStyle = m_pTable->m_rows[i].pNode->nStyle;

      if (i == 0 && m_pTable->m_bWithoutFirstNode)
      {
        bUnwrapped = true;
        continue;
      }

      if (m_pTable->IsNode_inner(i)
        && !m_pTable->IsAllParentsAboveUnwrapped(i)) // ������������ ���� �������
        continue;

      //nNodeStyle = m_pTable->m_rows[i].pNode->nStyle;
      bUnwrapped = m_pTable->m_rows[i].pNode->bUnwrapped;
      if (nNodeStyle == 0) // ���� ��� ������
      {
        bUnwrapped = true; // �� ������ ������
        continue;
      }
      //nComnSignificantCell = m_pTable->m_rows[i].pNode->nComnSignificantCell; // �������� ������, �������� ������� (������� � ������� � ��������� 
      // �������� ������, ���� �� �������) ����� ���������� ���� �������
      m_pTable->ShowRow(&m_pTable->m_rows[i]); // ����������� ����

      if (m_pTable->m_rows[i].pNode->nCntSetedChildren > 0 && // ���� �������� �� ������ �������� ��������
          (m_pTable->m_rows[i].pNode->strMarkCurrent == m_pTable->m_rows[i].pNode->strMarkEmpty)) // � ������ ���� [ ]
      {
        if (bUnwrapped)
          m_pTable->SetUnwrapNodeMark(i, m_pTable->m_rows[i].pNode->strMarkOpen); // ���������� [-]
        else
          m_pTable->SetUnwrapNodeMark(i, m_pTable->m_rows[i].pNode->strMarkClose); // ���������� [+]
      }      
      continue;
    }
    if (!bUnwrapped) // ���� ���� ������� - �������� �������� �� ����������
      continue;

    // ��������� �������� ��������� ����

    if ((nNodeStyle & NS_UNWRAP_ONLY_SETED) // ����� - ���������� ������ ��������������������� ������ (cell[nComnSignificantCell] != "")
      || (nNodeStyle & NS_UNWRAP_ONLY_SETED_WITH_GAPS)) // ����� - ���� ����� (�����) �������� �� ������ ��������� ����� ������ - ������
      // ����� ��������. ��� ������ �������� �������� ����� ���������� �� ������� ��������� ����� ������
    {
      if (!m_pTable->m_rows[i].cells[nComnSignificantCell].strText.IsEmpty())
        m_pTable->ShowRow(&m_pTable->m_rows[i]); // ����������� ��������� ��������
      else
      {
        if ((nNodeStyle & NS_UNWRAP_ONLY_SETED_WITH_GAPS))
        {
          if (m_pTable->IsSetedChildInFront(i))
            m_pTable->ShowRow(&m_pTable->m_rows[i]); // ����������� ��������� ��������
        }
      }
    }
    else
      m_pTable->ShowRow(&m_pTable->m_rows[i]); // ����� ����� �������� ��������� �� �����
  }

  // ������� ������� ����� �������� ��� ������� � �������

  int nOldItemsCount = m_pTable->m_pListCtrl->GetItemCount();
  int nNewItemsCount = (int)m_pTable->m_showingRows.size();
  int nTailItemsCount = nOldItemsCount - nNewItemsCount;
  int nDeleteItems = 0;
  int nAddItems = 0;

  if (nTailItemsCount > 0) // old was more then now new
    nDeleteItems = nTailItemsCount;
  else
  if (nTailItemsCount < 0) // new more then old
    nAddItems = -nTailItemsCount;

  if (nOldItemsCount == nDeleteItems)
    m_pTable->m_pListCtrl->DeleteAllItems();
  else
  {
    for (int i = 0, nDel = (nOldItemsCount - 1); i < nDeleteItems; ++i)
      m_pTable->m_pListCtrl->DeleteItem(nDel--); // ex.: old was 10 and will del 3, then need delete rows 9, 8, 7
  }
  for (int i = 0; i < nAddItems; ++i)
    m_pTable->m_pListCtrl->InsertItem(nOldItemsCount + i, _T(""));

  // ����� �� m_showingRows � ������� (����������� - �� �����)
  // ������������� ��� ������������� ������ ������� - ���� � ��� ��������� CheckBoxes, ��:
  // - ���� CheckBox ��������, ������ ����� disable
  // - ����� enable

  CString strText;
  int nCntCol = (int)m_pTable->m_columns.size();
  int nCntCells = 0;
  
  if ((int)m_pTable->m_rows.size() > 0)
    nCntCells = (int)m_pTable->m_rows[0].cells.size();
  for (int i = 0; i < nNewItemsCount; ++i)
  {
    for (int j = 0; j < nCntCol; ++j)
    {
      strText = _T("");
      if (j < nCntCells)
        strText = m_pTable->m_showingRows[i]->cells[j].strText;
      if (m_pTable->m_showingRows[i]->cells[j].bHideText)
        strText = _T("");

      if (j == 0 && m_pTable->m_bFirstColIsChkBoxes)
      {
        if (m_pTable->m_nCommonStyle & CMNS_DISABLE_ROW_IF_CHKBX_OFF)
        {
          bool bSet = m_pTable->m_pListCtrl->GetCheck(i) ? true : false;
          m_pTable->SetRowEnblDsbl(CnvIdxViewToList(i), !bSet);
        }
      }
      else
      {
        m_pTable->AddInnerNodeTextOffset(m_pTable->m_showingRows[i]->nIdxOneself_inList, j, strText);

        if (m_pTable->m_pListCtrl->GetItemText(i, j) != strText)
          m_pTable->m_pListCtrl->SetItemText(i, j, strText);

        m_pTable->CheckCellImg(i, j);
      }
     }
    m_pTable->m_showingRows[i]->nIdx_inView = i;
    m_pTable->m_showingRows[i]->bShowing = true;
  }

  //m_pTable->m_pListCtrl->Invalidate();
  int j = 0;
  for (auto& i : m_pTable->m_showingRows)
  {
    if (i->bShowing)
      m_pTable->m_pListCtrl->Update(j);
    ++j;
  }
  m_pTable->m_bUpdating = false;
}

bool �Ai_ListCtrlEx::IsUpdatingView()
{
  return m_pTable->m_bUpdating;
}

void �Ai_ListCtrlEx::WrapTree()
{
  if (m_pTable->m_bNodeIs)
  {
    m_pTable->UnwrapTree(false);
    UpdateView();
  }
}

void �Ai_ListCtrlEx::UnwrapTree()
{
  if (m_pTable->m_bNodeIs)
  {
    m_pTable->UnwrapTree(true);
    UpdateView();
  }
}

///*******************************************************************
//Messages
//*******************************************************************

bool �Ai_ListCtrlEx_Table::CanOnDblClick(int nRow_inView, int nCell)
{
  //RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);  
  RETURN_AST_FALSE_BY_TRUE((int)m_showingRows.size() == 0)
  RETURN_FALSE_BY_OUT_OF_RANGE(nRow_inView, 0, (int)m_showingRows.size() - 1); // exit without assert
  RETURN_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1); // exit without assert
  
  �Ai_ListCtrlEx_Table::NODE_ROW* pNode = IsNode(m_showingRows[nRow_inView]->nIdxOneself_inList) ? 
    m_rows[m_showingRows[nRow_inView]->nIdxOneself_inList].pNode :
    m_rows[GetParentNodeIdx_inView(nRow_inView)].pNode;
  
  if (pNode != nullptr && 
     (pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL) &&
     ((pNode->nStyle & NS_DBLCLK_DO_ALWAYS) == false) &&
     m_showingRows[nRow_inView]->cells[pNode->nComnSignificantCell].strText.IsEmpty()) // exit without assert
    return false;

  return true;
}

void �Ai_ListCtrlEx::OnDblClick(int nRow_inView, int nCell, POINT* pptAction)
{
  if (!m_pTable->CanOnDblClick(nRow_inView, nCell))
  {
    m_pTable->CloseOpenedCtrl();
    return;
  }

  RETURN_BY_TRUE(!m_pTable->IsRowEnable(m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList));

  CString strCellText = m_pTable->m_showingRows[nRow_inView]->cells[nCell].strText;
  m_bindInfo.nRow_inView = nRow_inView;
  m_bindInfo.nRow_inList = CnvIdxViewToList(nRow_inView);
  m_bindInfo.nCell = nCell;
  m_bindInfo.nCellFlag = GetCellFlag(m_bindInfo.nRow_inList, nCell);
  m_bindInfo.strOldText = strCellText;
  m_bindInfo.strNewText = "";
  m_bindInfo.bct = HasCellBindCtrl(m_bindInfo.nRow_inList, nCell);
  m_pTable->m_pCurrCellBind = &m_pTable->m_showingRows[nRow_inView]->cells[nCell].bindTo;

  if (pptAction != nullptr)
    if (m_pTable->IsClkOnImg(nRow_inView, nCell, pptAction))
    {
      m_pTable->SendMsg(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, (WPARAM)m_pTable->m_pParentWnd, (LPARAM)m_pTable->m_pCurrCellBind->nID);
      return;
    }

  if (m_pTable->ChangeUnwrapNodeMark(m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList, nCell)) // dbl click
  { // dbl click on node - wrap/unwrap

    m_pTable->UnselectRows();
    UpdateView();
    m_pTable->SelectAndShowThisRow(nRow_inView, false);
    return;
  }
  else
  if (!m_pTable->m_pCurrCellBind->bDisableBind)
  { // dbl click on binding ctrl

    int nRow_inList = CnvIdxViewToList(nRow_inView);
    
    �Ai_ListCtrlEx_Table::NODE_ROW* pNode = m_pTable->IsNode(nRow_inList) ?
      m_pTable->m_rows[nRow_inList].pNode :
      m_pTable->m_rows[m_pTable->GetParentNodeIdx_inView(nRow_inView)].pNode;

    if (m_pTable->IsNode(nRow_inList)
      && pNode->nCntSetedChildren == 0)
      return;

    bool bClosePrevCloseOpenedCtrl = true;

    m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsNone;

    if (m_pTable->m_pCurrCellBind->pDialog != nullptr)
    { // dialog ctrl

      if (!strCellText.IsEmpty() && m_pTable->m_showingRows[nRow_inView]->pNode != nullptr)
      {
        if (strCellText == DIFF_NODE_CHLDR_CELL_MARKS)
          strCellText = _T("");
        else
        {
          strCellText.TrimLeft(_T("< "));
          strCellText.TrimRight(_T(" >"));
        }
      }
      
      *m_pTable->m_pCurrCellBind->pstrExchange = strCellText;
      if (m_pTable->RecalcParamsAfterClickOnCell(nRow_inView, nCell)
        && m_pTable->m_pCurrCellBind->pDialog->DoModal() == IDOK)
      {
        m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsNone;
        if (m_pTable->ChangeTextInCellAfterCtrl(*m_pTable->m_pCurrCellBind->pstrExchange) == 2)
        {
          UpdateView();
          m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsMulty;
        }
        else
        if (m_pTable->ChangeTextInCellAfterCtrl(*m_pTable->m_pCurrCellBind->pstrExchange) == 1)
          m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsSingle;
        
        m_bindInfo.strNewText = m_pTable->m_showingRows[m_bindInfo.nRow_inView]->cells[m_bindInfo.nCell].strText;
        m_pTable->SendMsg(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, (WPARAM)m_pTable->m_pParentWnd, (LPARAM)m_pTable->m_pCurrCellBind->nID);
      }
    }
    else
    if ((m_pTable->m_pCurrCellBind->pCmbBox != nullptr) && 
        m_pTable->RecalcParamsAfterClickOnCell(nRow_inView, nCell))
    { // combo box ctrl

      if (m_pTable->m_pCurrCellBind->pCmbBox != m_pTable->m_eventInDisableBindCell.pcmbOpened)
      {
        m_pTable->CloseOpenedCtrl();
        bClosePrevCloseOpenedCtrl = false;
     
        m_pTable->m_eventInDisableBindCell.pcmbOpened = m_pTable->m_pCurrCellBind->pCmbBox;

        m_pTable->m_eventInDisableBindCell.strBefore = strCellText;
        CString strLBText;
        int nCount = m_pTable->m_pCurrCellBind->pCmbBox->GetCount();
        for (int i = 0; i < nCount; ++i)
        {
          m_pTable->m_pCurrCellBind->pCmbBox->GetLBText(i, strLBText);
          if (strLBText == strCellText)
          {
            m_pTable->m_pCurrCellBind->pCmbBox->SetCurSel(i);
            break;
          }
        }
        m_pTable->m_pCurrCellBind->pCmbBox->MoveWindow(m_pTable->m_eventInDisableBindCell.cellRect, TRUE);
        m_pTable->m_pCurrCellBind->pCmbBox->ShowWindow(SW_SHOW);
        m_pTable->m_pCurrCellBind->pCmbBox->ShowDropDown(TRUE);
        m_pTable->m_pCurrCellBind->pCmbBox->SetFocus();
      }
    }
    else
    if ((m_pTable->m_pCurrCellBind->pEdit != nullptr) && 
        m_pTable->RecalcParamsAfterClickOnCell(nRow_inView, nCell))
    { // edit ctrl

      if (m_pTable->m_pCurrCellBind->pEdit != m_pTable->m_eventInDisableBindCell.pedtOpened)
      {
        m_pTable->CloseOpenedCtrl();
        bClosePrevCloseOpenedCtrl = false;

        m_pTable->m_eventInDisableBindCell.pedtOpened = m_pTable->m_pCurrCellBind->pEdit;
        m_pTable->m_eventInDisableBindCell.strBefore = strCellText;
        m_pTable->m_pCurrCellBind->pEdit->MoveWindow(m_pTable->m_eventInDisableBindCell.cellRect, TRUE);
        m_pTable->m_pCurrCellBind->pEdit->SetWindowText(strCellText);
        m_pTable->m_pCurrCellBind->pEdit->SetFocus();
        int nPos = strCellText.GetLength();
        m_pTable->m_pCurrCellBind->pEdit->SetSel(nPos, nPos, 0);
        m_pTable->m_pCurrCellBind->pEdit->ShowWindow(SW_SHOW);
        m_pTable->m_pCurrCellBind->pEdit->Invalidate();
      }
    }
    else
    if ((m_pTable->m_pCurrCellBind->pIP != nullptr) && 
        m_pTable->RecalcParamsAfterClickOnCell(nRow_inView, nCell))
    { // ip edit ctrl

      if (m_pTable->m_pCurrCellBind->pIP != m_pTable->m_eventInDisableBindCell.pipOpened)
      {
        m_pTable->CloseOpenedCtrl();
        bClosePrevCloseOpenedCtrl = false;

        m_pTable->m_eventInDisableBindCell.pipOpened = m_pTable->m_pCurrCellBind->pIP;
        m_pTable->m_eventInDisableBindCell.strBefore = strCellText;
        m_pTable->m_pCurrCellBind->pIP->MoveWindow(m_pTable->m_eventInDisableBindCell.cellRect, TRUE);
        BYTE nIP0, nIP1, nIP2, nIP3;
        CAi_Str::StringToIP(strCellText, nIP0, nIP1, nIP2, nIP3);
        m_pTable->m_pCurrCellBind->pIP->ShowWindow(SW_SHOW);
        m_pTable->m_pCurrCellBind->pIP->ClearAddress();
        m_pTable->m_pCurrCellBind->pIP->SetAddress(nIP0, nIP1, nIP2, nIP3);
        m_pTable->m_pCurrCellBind->pIP->SetFieldFocus(0);
        ////m_pTable->m_pCurrCellBind->pIP->Invalidate();
      //��������!!! 
      //����������������� ����� �����������, ��� ���� ���  ������ ������� CIPAddressCtrl::Create �� ���������� ����������
      //���������� ������� ����: CRect(0, 0, 115, 17), �.�. ��������� ������� ������ ��� ������ CRect(0, 0, 0, 0)
      //��!!!! ������ ����� ����:
      //- ������������ �����������
      //- ���� �������� �������� ������ ����� ������ �� ���� ������ �� 0 �� 99 : 99, 99, 99, 99 
      }
    }

    if (bClosePrevCloseOpenedCtrl)
      m_pTable->CloseOpenedCtrl();
  }
}

void �Ai_ListCtrlEx::OnLClick(int nRow_inView, int nCell)
{
  m_pTable->CloseOpenedCtrl();
  if (!m_pTable->CanOnDblClick(nRow_inView, nCell))
  {
  }
}

int �Ai_ListCtrlEx::OnCustomDrawEx(NMHDR *pNMHDR, LRESULT *pResult, int colorBk, int colorTx)
{
  if (m_pTable == nullptr)
    return 0;
  if (m_pTable->m_bUpdating)
    return 0;


  LPNMLVCUSTOMDRAW pNMCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
  int nRes = 0;
  // ����� ��������� ����������� ��������� ��� ���� ��������� �� ���������
  *pResult = CDRF_DODEFAULT;

  //������� ���� ���������� ������� ������
  if ( CDDS_PREPAINT == pNMCD->nmcd.dwDrawStage )
  {
    //���� �������� ���� ������� ������� - ����������� ��������� ��������� ��� ������� �������� ������.
    *pResult = CDRF_NOTIFYITEMDRAW;
  }
  else
  if ( CDDS_ITEMPREPAINT == pNMCD->nmcd.dwDrawStage )
  {
    *pResult = CDRF_NOTIFYSUBITEMDRAW;
  }
  else 
  if( (CDDS_ITEMPREPAINT | CDDS_SUBITEM) == pNMCD->nmcd.dwDrawStage)
  {
    if (m_pTable->m_showingRows.empty())
      return nRes;

    //������, ������� ��������� ����� ���������� ������� ������� �������� ������
    // �� �� ��������� ��������� ������. ������ ����� ������ �������� ���� �
    //���� �������� Windows, ��� ��������� ������ ��������� �������.

    // ���������� �������, ����� ��� �������������� ���������� �������.
    *pResult = CDRF_DODEFAULT;

    int nRow = pNMCD->nmcd.dwItemSpec;
    int nCell = pNMCD->iSubItem;

    if (m_pTable->m_pFontOld)
      SelectObject(pNMCD->nmcd.hdc, m_pTable->m_pFontOld);

    switch (m_pTable->m_showingRows[nRow]->cells[nCell].textStyle)
    {
      case ctsBold:
        m_pTable->m_pFontOld = 
          (CFont*)SelectObject(pNMCD->nmcd.hdc, afxGlobalData.fontBold);
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
        break;
      case ctsItalic:
        m_pTable->m_pFontOld = 
          (CFont*)SelectObject(pNMCD->nmcd.hdc, m_pTable->m_fItalic);
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
        break;
      case ctsItalicBold:
        m_pTable->m_pFontOld = 
          (CFont*)SelectObject(pNMCD->nmcd.hdc, m_pTable->m_fItalicBold);
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
        break;
      case ctsStrikeOut:
        m_pTable->m_pFontOld =
          (CFont*)SelectObject(pNMCD->nmcd.hdc, m_pTable->m_fStrikeOut);
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
        break;
      case ctsPrevious:
         SelectObject(pNMCD->nmcd.hdc, m_pTable->m_pFontOld);
         m_pTable->m_showingRows[nRow]->cells[nCell].textStyle = ctsNone;
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
      default:
        m_pTable->m_pFontOld = nullptr;
        break;
    };

    if (m_pTable->m_pListCtrl != nullptr &&
      nRow < (int)m_pTable->m_showingRows.size() && nCell < (int)m_pTable->m_showingRows[nRow]->cells.size())
    {
      // Bg color

      if (!m_pTable->m_bTableEnable)
        pNMCD->clrTextBk = COLOR_LCE_LIGHT_GRAY;
      else
      {
        if (m_pTable->m_showingRows[nRow]->cells[nCell].nColorBgNew != -1)
          pNMCD->clrTextBk = (COLORREF)m_pTable->m_showingRows[nRow]->cells[nCell].nColorBgNew;
        else
          pNMCD->clrTextBk = m_pTable->m_showingRows[nRow]->cells[nCell].nColorBg;
      }

      // Text color

      if (!m_pTable->m_bTableEnable)
        pNMCD->clrText = COLOR_LCE_GRAY;
      else
      {
        if (!m_pTable->IsCellEnable(m_pTable->m_showingRows[nRow]->nIdxOneself_inList, nCell)
          && m_pTable->m_showingRows[nRow]->cells[nCell].nColorTxNew == COLOR_LCE_BLACK)
          pNMCD->clrText = COLOR_LCE_GRAY;
        else
        {
          if (m_pTable->m_showingRows[nRow]->cells[nCell].nColorTxNew > -1)
            pNMCD->clrText = (COLORREF)m_pTable->m_showingRows[nRow]->cells[nCell].nColorTxNew;
          else
            pNMCD->clrText = m_pTable->m_showingRows[nRow]->cells[nCell].nColorTx;
        }
      }
    }
    else
    if (nRow % 2)
      pNMCD->clrTextBk = RGB(242, 255, 233);
    else
      pNMCD->clrTextBk = COLOR_LCE_WHITE;

    if (colorBk != -1)
      pNMCD->clrTextBk = colorBk;
    if (colorTx != -1)
      pNMCD->clrText = colorTx;

    nRes = 1;
  }
  else
  if( (CDDS_ITEMPOSTPAINT | CDDS_SUBITEM) == pNMCD->nmcd.dwDrawStage)
  {
    *pResult = CDRF_DODEFAULT;

    int nRow = pNMCD->nmcd.dwItemSpec;
    int nCell = pNMCD->iSubItem;

    switch (m_pTable->m_showingRows[nRow]->cells[nCell].textStyle)
    {
      case ctsBold:
         SelectObject(pNMCD->nmcd.hdc, m_pTable->m_pFontOld);
        *pResult = CDRF_NEWFONT | CDRF_NOTIFYPOSTPAINT;
        break;
    };
  }

  return nRes;
}

bool �Ai_ListCtrlEx_Table::CanSelchangeCmbbox()
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  return true;
}

void �Ai_ListCtrlEx::OnSelchangeCmbbox()
{
  RETURN_BY_TRUE(!m_pTable->CanSelchangeCmbbox());
  int nRes = m_pTable->SelchangeCmbbox();
  
  m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsNone;
  if (nRes == 2)
  {
    UpdateView();
    m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsMulty;
  }
  else
  if (nRes == 1)
    m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsSingle;
  m_pTable->CloseOpenedCtrl();

  m_bindInfo.strNewText = m_pTable->m_showingRows[m_bindInfo.nRow_inView]->cells[m_bindInfo.nCell].strText;

  m_pTable->SendMsg(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, (WPARAM)m_pTable->m_pParentWnd, (LPARAM)m_pTable->m_pCurrCellBind->nID);
}

void �Ai_ListCtrlEx::OnCloseUpCmbbox()
{
  RETURN_BY_TRUE(!m_pTable->CanSelchangeCmbbox());
  m_pTable->CloseOpenedCtrl();
}

int �Ai_ListCtrlEx::CnvIdxViewToList(int nRow_inView)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithRowIdx_view(nRow_inView));
  return m_pTable->m_showingRows[nRow_inView]->nIdxOneself_inList;
}

bool �Ai_ListCtrlEx_Table::CanGetIndexesOfNodes_list(void* pIdxOfNodes_inList)
{
  RETURN_AST_FALSE_BY_TRUE(pIdxOfNodes_inList == nullptr);
  return true;
}

bool �Ai_ListCtrlEx::GetIdxOfNodesInList(vector<int>* pIdxOfNodes_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanGetIndexesOfNodes_list((void*)pIdxOfNodes_inList));
 
  int nCntRows = m_pTable->m_rows.size();
  
  pIdxOfNodes_inList->clear();
  for (int i = 0; i < nCntRows; ++i)
    if (m_pTable->m_rows[i].pNode != nullptr)
      pIdxOfNodes_inList->push_back(i);
  return true;
}

int �Ai_ListCtrlEx::GetNodeRowIdxInView(int nRow_inList)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nRow_inList));
  if (m_pTable->IsNode(nRow_inList))
    return m_pTable->m_rows[nRow_inList].nIdx_inView;
  return m_pTable->m_rows[m_pTable->m_rows[nRow_inList].nIdxParentNode_inList].nIdx_inView;
}

int �Ai_ListCtrlEx::GetRowIdxInView(int nRow_inList)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nRow_inList));
  return m_pTable->m_rows[nRow_inList].nIdx_inView;
}

///*******************************************************************
//Get result
//*******************************************************************
bool �Ai_ListCtrlEx_Table::CanGetListItems(GET_BY_FILTER* pFilter, vector<RESULT_GET_LIST>* pResultList, int nCntReadColumns)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  RETURN_AST_FALSE_BY_TRUE(pFilter == nullptr);
  RETURN_AST_FALSE_BY_TRUE(pResultList == nullptr);
  if (pFilter->nFlag1 == GRFF_GET_NODE_INDEXES)
  {
  }
  else
  {
    RETURN_AST_FALSE_BY_TRUE(nCntReadColumns <= 0);

    if (pFilter->nFlag1 != GRFF_GET_CHILDREN
      && pFilter->nFlag1 != GRFF_GET_ALL
      && pFilter->nFlag1 != GRFF_GET_NODE_INDEXES
      && pFilter->nFlag1 != GRFF_GET_NODES_CHILDREN_INDEXES)
      ASSERT(false);

    if (pFilter->nFlag1 == GRFF_GET_CHILDREN)
    {
      if (pFilter->nFlag3 > 0)
      {
        RETURN_AST_FALSE_BY_OUT_OF_RANGE(pFilter->nFlag3 - 1, 0, (int)m_columns.size() - 1);
      }
    }

    if (pFilter->nFlag1 == GRFF_GET_NODES_CHILDREN_INDEXES)
    {
      RETURN_AST_FALSE_BY_TRUE(!CanWorkWithRow_list(pFilter->nFlag2));
      RETURN_AST_FALSE_BY_TRUE(!IsNode(pFilter->nFlag2));
      if (pFilter->nFlag3 > 0)
      {
        RETURN_AST_FALSE_BY_OUT_OF_RANGE(pFilter->nFlag3 - 1, 0, (int)m_columns.size() - 1);
      }
    }
  }
  return true;
}

int �Ai_ListCtrlEx::GetCommonSignificantCell(int nRow_inList)
{
  RETURN_AST_1_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  int nNodeRow_inList = m_pTable->GetParentNodeIdx_inList(nRow_inList);
  //if (m_pTable->m_rows[nNodeRow_inList].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL)
    return m_pTable->m_rows[nNodeRow_inList].pNode->nComnSignificantCell;
}

bool �Ai_ListCtrlEx::SetCommonSignificantCell(int nRow_inList, int nComnSignificantCell)
{
  RETURN_AST_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  int nNodeRow_inList = m_pTable->GetParentNodeIdx_inList(nRow_inList);
  RETURN_AST_FALSE_BY_TRUE((m_pTable->m_rows[nNodeRow_inList].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL) == false);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nComnSignificantCell, 0, (int)m_pTable->m_columns.size() - 1);
  m_pTable->m_rows[nNodeRow_inList].pNode->nComnSignificantCell = nComnSignificantCell;
  return true;
}

bool �Ai_ListCtrlEx::GetListItems(GET_BY_FILTER* pFilter, vector<RESULT_GET_LIST>* pResultList, int nCntReadColumns, ...)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanGetListItems(pFilter, pResultList, nCntReadColumns));
  pResultList->clear();

  RESULT_GET_LIST result;
  int nIdxNodeRow;
  �Ai_ListCtrlEx_Table::NODE_ROW *pNode;
  vector<int> params;
  va_list arg;
  va_start(arg, nCntReadColumns);
  int nColumn = 0;
  int nCntParams = 0;

  if (pFilter->nFlag1 == GRFF_GET_NODE_INDEXES)
  {
    for (size_t i = 0; i < m_pTable->m_rows.size(); ++i)
    {
      if (m_pTable->m_rows[i].pNode)
      {
        RESULT_GET_LIST res;
        res.bNode = true;
        res.nNodeIdx_inList = i;
        res.nIdx_inList = m_pTable->m_rows[i].nIdxOneself_inList;
        res.nIdx_inView = res.nNodeIdx_inView = GetRowIdxInView(i);
        pResultList->push_back(res);
      }
    }
    return true;
  }

  while ((nColumn = va_arg(arg, int)) != -1)
    params.push_back(nColumn);
  nCntParams = (int)params.size();
  if (nCntParams == 0)
    return false;

  int nCntRows = (int)m_pTable->m_rows.size();
  int nStart = 0;
  CString strTmp;

  if (pFilter->nFlag1 == GRFF_GET_NODES_CHILDREN_INDEXES)
  {
    nStart = pFilter->nFlag2 + 1;
    nCntRows = m_pTable->m_rows[pFilter->nFlag2].pNode->nCntChildren;
  }

  // filter
  for (int i = nStart, j = 0; j < nCntRows; ++i, ++j)
  {
    result.bNode = false;
    nIdxNodeRow = m_pTable->GetParentNodeIdx_inList(i);
    pNode = m_pTable->m_rows[nIdxNodeRow].pNode;
    if (pFilter->nFlag2 == GRFF_SETED_IN_COMM_SIGN_CELLS && 
      !(pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL))
      return false;

    if (pFilter->nFlag1 == GRFF_GET_ALL)
    {
      if (m_pTable->m_rows[i].pNode != nullptr)
        result.bNode = true;
      else // child
      if ((pFilter->nFlag2 == GRFF_SETED_IN_COMM_SIGN_CELLS) &&
        m_pTable->m_rows[i].cells[pNode->nComnSignificantCell].strText.IsEmpty())
        continue;
    }
    else
    if (pFilter->nFlag1 == GRFF_GET_CHILDREN &&
        m_pTable->m_rows[i].pNode == nullptr)
    { // need analis flag2

      if ((pFilter->nFlag2 == GRFF_SETED_IN_COMM_SIGN_CELLS) &&
        m_pTable->m_rows[i].cells[pNode->nComnSignificantCell].strText.IsEmpty())
        continue;

      if (pFilter->nFlag3 > 0
        && m_pTable->m_rows[i].cells[pFilter->nFlag3 - 1].strText.IsEmpty())
        continue;
    }
    else
    if (pFilter->nFlag1 == GRFF_GET_NODES_CHILDREN_INDEXES)
    {
      if (pFilter->nFlag3 > 0
        && m_pTable->m_rows[i].cells[pFilter->nFlag3 - 1].strText.IsEmpty())
        continue;
    }
    else
    if (i == 0 && m_pTable->m_bWithoutFirstNode)
      continue;

    if (pFilter->bIgnoreNodes
      && m_pTable->m_rows[i].pNode != nullptr)
      continue;

    result.textOfCellsOfRow.clear();
    result.textOfServiceInfoOfRow.clear();

    // copy cell`s text
    for (int j = 0; j < nCntParams; ++j)
    {
      nColumn = params[j];
      if (nColumn > -1 && nColumn < (int)m_pTable->m_rows[i].cells.size())
      {
        strTmp = m_pTable->m_rows[i].cells[nColumn].strText;
        if (nColumn == pNode->nUnwrapCell)
          strTmp = m_pTable->DeleteUnwrapCellMark(nIdxNodeRow, strTmp);
        result.textOfCellsOfRow.push_back(strTmp);
      }
    }
    // copy row`s hide service info
    int nCntServ = (int)m_pTable->m_rows[i].serviceInfo.size();

    for (int j = 0; j < nCntServ; ++j)
      result.textOfServiceInfoOfRow.push_back(m_pTable->m_rows[i].serviceInfo[j]);

    result.nIdx_inView = m_pTable->m_rows[i].nIdx_inView;
    result.nNodeIdx_inList = m_pTable->GetParentNodeIdx_inList(i);
    result.nNodeIdx_inView = m_pTable->m_rows[result.nNodeIdx_inList].nIdx_inView;
    pResultList->push_back(result);
  }

  return true;
}

bool �Ai_ListCtrlEx_Table::CanFindRow(int nCell)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bChildIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  return true;
}

int �Ai_ListCtrlEx::FindRow(CString strSoughtText, int nInCell)
{
  return FindRow(strSoughtText, 0, nInCell);
}

int �Ai_ListCtrlEx::FindRow(CString strSoughtText, int nFromRow_inList, int nInCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanFindRow(nInCell));
  
  int nCntRows = m_pTable->m_rows.size();
  if (nFromRow_inList >= nCntRows)
    return -1;
  
  for (int i = nFromRow_inList; i < nCntRows; ++i)
  {
    CString strText = m_pTable->m_rows[i].cells[nInCell].strText;
    if (m_pTable->m_rows[i].pNode
      && m_pTable->m_rows[i].pNode->nUnwrapCell == nInCell)
    {
      strText.TrimLeft(_T(" "));
      strText.TrimLeft(_T("["));
      strText.TrimLeft(_T(" "));
      strText.TrimLeft(_T("+"));
      strText.TrimLeft(_T("-"));
      strText.TrimLeft(_T(" "));
      strText.TrimLeft(_T("]"));
      strText.TrimLeft(_T(" "));
    }
    if (strText == strSoughtText)
      return i;
  }
  return -1;
}

 //*************************** Indexes *******************************
 
bool �Ai_ListCtrlEx_Table::CanWorkWithRowIdx_view(int nRow_InView)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bNodeIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_InView, 0, (int)m_showingRows.size() - 1);
  return true;
}

bool �Ai_ListCtrlEx_Table::CanWorkWithRowIdx_list(int nRow_inList)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bNodeIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_inList, 0, (int)m_rows.size() - 1);
  return true;
}

CString �Ai_ListCtrlEx::GetNodeName(int nRow_inList)
{
  if (!m_pTable->CanWorkWithRowIdx_list(nRow_inList))
    return _T("");
  
  int nIdxNode = m_pTable->GetParentNodeIdx_inList(nRow_inList);
  int nUnwrCell = m_pTable->m_rows[nIdxNode].pNode->nUnwrapCell;

  CString strNodeName = m_pTable->m_rows[nIdxNode].cells[nUnwrCell].strText;
  strNodeName = m_pTable->DeleteUnwrapCellMark(nIdxNode, strNodeName);

  return strNodeName;
}

int �Ai_ListCtrlEx::GetAvailabelCountOfChild(int nRow_inList)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nRow_inList));
  if (m_pTable->IsNode(nRow_inList))
    return m_pTable->m_rows[nRow_inList].pNode->nCntChildren;
  return m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)].pNode->nCntChildren;
}

int �Ai_ListCtrlEx::GetSetedCountOfChild(int nRow_inList)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithRowIdx_list(nRow_inList));
  return m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)].pNode->nCntSetedChildren;
}

bool �Ai_ListCtrlEx_Table::CanGetServiceInfo(int nRow_inList, void* pRowServiceInfo)
{
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nRow_inList));
  RETURN_AST_FALSE_BY_TRUE(pRowServiceInfo == nullptr);
  return true;
}

bool �Ai_ListCtrlEx::GetServiceInfo(int nRow_inList, vector<CString>* pRowServiceInfo)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanGetServiceInfo(nRow_inList, (void*)pRowServiceInfo));
  *pRowServiceInfo = m_pTable->m_rows[nRow_inList].serviceInfo;
  return true;
}

bool �Ai_ListCtrlEx::SetServiceInfo(int nRow_inList, vector<CString>* pRowServiceInfo)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanGetServiceInfo(nRow_inList, (void*)pRowServiceInfo));
  m_pTable->m_rows[nRow_inList].serviceInfo = *pRowServiceInfo;
  return true;
}

bool �Ai_ListCtrlEx::IsNodeRow(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  return m_pTable->m_rows[nRow_inList].pNode != nullptr ? true : false;
}

bool �Ai_ListCtrlEx::IsNodeUnwrapped(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  if (m_pTable->m_rows[nRow_inList].pNode)
    return m_pTable->m_rows[nRow_inList].pNode->bUnwrapped;
  return false;
}

int �Ai_ListCtrlEx::GetNodeRow(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  return m_pTable->GetParentNodeIdx_inList(nRow_inList);
}

bool �Ai_ListCtrlEx::IsSignificantCellSeted(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  
  int nSignificantCell = -1;

  if (m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL)
    nSignificantCell = m_pTable->m_rows[m_pTable->GetParentNodeIdx_inList(nRow_inList)].pNode->nComnSignificantCell;
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nSignificantCell));

  return m_pTable->m_rows[nRow_inList].cells[nSignificantCell].strText.IsEmpty() ? false : true;
}

bool �Ai_ListCtrlEx::IsRowEnable(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  return m_pTable->IsRowEnable(nRow_inList);//m_rows[nRow_inList].bRowDisable ? false : true;
}

bool �Ai_ListCtrlEx::DeleteRow(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  int nFinish, nStart;
  nFinish = nStart = nRow_inList;
  if (m_pTable->m_rows[nRow_inList].pNode)
    nFinish += m_pTable->m_rows[nRow_inList].pNode->nCntChildren;
  for (int i = nFinish; i >= nStart; i--)
  {
    ClearChild(m_pTable->m_rows[i].nIdx_inView);
    m_pTable->m_rows.erase(m_pTable->m_rows.begin() + i);
  }
  return true;
}

bool �Ai_ListCtrlEx::IsSelectedRows()
{
  vector<int> items;
  if (GetSelectedItems(&items))
    if (items.size() > 0)
      return true;
  return false;
}

void �Ai_ListCtrlEx::UnselectRows()
{
  RETURN_BY_TRUE(!m_pTable->m_bNodeIs);
  m_pTable->UnselectRows();
}

void �Ai_ListCtrlEx::SelectRow(int nRow_inView)
{
  if (m_pTable->CanWorkWithRow_view(nRow_inView))
    m_pTable->m_pListCtrl->SetItemState(nRow_inView, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
}

bool �Ai_ListCtrlEx::GetSelectedItems(vector<int>* pItems_inView)
{
  RETURN_FALSE_BY_TRUE(pItems_inView == nullptr);
  return m_pTable->GetSelectedItems(pItems_inView);
}

bool �Ai_ListCtrlEx::GetFirstSelectedItem(int* nRow_inList, int* nRow_inView)
{
  vector<int> items_inView;
  if (GetSelectedItems(&items_inView))
  {
    if (items_inView.size() > 0)
    {
      if (nRow_inView)
        *nRow_inView = items_inView[0];
      *nRow_inList = CnvIdxViewToList(items_inView[0]);
    }
    return true;
  }
  return false;
}

bool �Ai_ListCtrlEx::IsSelectedRowVisible()
{
  vector<int> items;
  if (GetSelectedItems(&items)
    && items.size() == 1)
  {
    if (!m_pTable->m_pListCtrl->SendMessage(LVM_ISITEMVISIBLE, items[0], 0))
      return false;
  }
  return true;
}

void �Ai_ListCtrlEx::DoSelectedRowVisible()
{
  vector<int> items;
  if (GetSelectedItems(&items)
    && items.size() == 1)
  {
    m_pTable->m_pListCtrl->SetSelectionMark(items[0]);
    m_pTable->m_pListCtrl->SetItemState(items[0], LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
    m_pTable->m_pListCtrl->EnsureVisible(items[0], FALSE);
  }
}

bool �Ai_ListCtrlEx::IsRowVisible(int nRow_inList)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList, false));
  return m_pTable->m_rows[nRow_inList].bShowing;
}

void �Ai_ListCtrlEx::SetCellStyle(int nRow_inList, int nCell, LONGLONG nStyle)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
    return;
  m_pTable->m_rows[nRow_inList].cells[nCell].nStyle = nStyle;
}

LONGLONG �Ai_ListCtrlEx::GetCellStyle(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
    return -1;
  return m_pTable->m_rows[nRow_inList].cells[nCell].nStyle;
}

bool �Ai_ListCtrlEx::IsCellEnable(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  return m_pTable->IsCellEnable(nRow_inList, nCell);
}

CString �Ai_ListCtrlEx::GetCellText(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
    return _T("");
  return m_pTable->m_rows[nRow_inList].cells[nCell].strText;
}

CString �Ai_ListCtrlEx::GetCellTextInSelRow(int nCell)
{
  RETURN_AST_EMPTS_BY_TRUE(!m_pTable->m_bNodeIs);
  RETURN_AST_EMPTS_BY_OUT_OF_RANGE(nCell, 0, (int)m_pTable->m_columns.size() - 1);
  
  vector<int> sel;

  if (m_pTable->GetSelectedItems(&sel)
    && (int)sel.size() == 1)
    return m_pTable->m_showingRows[sel[0]]->cells[nCell].strText;
  else
    return _T("");
}

CString �Ai_ListCtrlEx::GetChainOfSignCellsAbove(int nRow_inView)
{
  RETURN_AST_EMPTS_BY_TRUE(!m_pTable->m_bNodeIs);
  RETURN_AST_EMPTS_BY_OUT_OF_RANGE(nRow_inView, 0, (int)m_pTable->m_showingRows.size() - 1);
  int nRow_inList = CnvIdxViewToList(nRow_inView);

  int nItParentNodeIdx = nRow_inList;
  if (!m_pTable->IsNode(nRow_inList))
    nItParentNodeIdx = m_pTable->GetParentNodeIdx_inList(nRow_inList);
  int nSignCell = m_pTable->m_rows[nItParentNodeIdx].pNode->nComnSignificantCell;
  
  CString strCellText = m_pTable->m_rows[nRow_inList].cells[nSignCell].strText;
  strCellText = m_pTable->DeleteUnwrapCellMark(nRow_inList, strCellText);
  CString strTextChain = strCellText;
  
  if (!m_pTable->IsNode(nRow_inList))
  {
    strCellText = m_pTable->m_rows[nItParentNodeIdx].cells[nSignCell].strText;
    strCellText = m_pTable->DeleteUnwrapCellMark(nItParentNodeIdx, strCellText);
    strTextChain = strCellText + _T("->") + strTextChain;
  }

  if (nItParentNodeIdx == -1) // top node
    return strTextChain;
  int nAboveParentNodeIdx = m_pTable->m_rows[nItParentNodeIdx].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(

  do
  {
    if (nAboveParentNodeIdx == -1)
      return strTextChain;
    nSignCell = m_pTable->m_rows[nAboveParentNodeIdx].pNode->nComnSignificantCell;
    strCellText = m_pTable->m_rows[nAboveParentNodeIdx].cells[nSignCell].strText;
    strCellText = m_pTable->DeleteUnwrapCellMark(nAboveParentNodeIdx, strCellText);
    strTextChain = strCellText + _T("->") + strTextChain;
    nAboveParentNodeIdx = m_pTable->m_rows[nAboveParentNodeIdx].nIdxParentNode_inList; // withour GetParentNodeIdx_inList(
  }
  while(true);
  return strTextChain;
}

bool �Ai_ListCtrlEx::SetCellText(int nRow_inList, int nCell, CString strText, bool bUpdBindCtrl)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  if (bUpdBindCtrl
    && (int)m_pTable->m_showingRows.size() > 0 
    && !m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.bDisableBind)
    m_pTable->ChangeTextInCellAfterCtrl(strText);
  else
    m_pTable->m_rows[nRow_inList].cells[nCell].strText = strText;
  return true;
}

bool �Ai_ListCtrlEx::SetCellsText(int nIdxNode, int nCell, CString strText, bool bIgnoreDisableCells)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithNode(nIdxNode));
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nIdxNode, nCell));
  m_pTable->UpdateAllCellsOfNodeChildren(nIdxNode, nCell, strText, 0, 0, 0, bIgnoreDisableCells);
  return true;
}

bool �Ai_ListCtrlEx::SetCellsText(int nIdxNode, int nCell, int nMeaning, int nInc, int nIncMax, bool bIgnoreDisableCells)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithNode(nIdxNode));
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nIdxNode, nCell));
  m_pTable->m_bUseInc = true;
  m_pTable->UpdateAllCellsOfNodeChildren(nIdxNode, nCell, _T(""), nMeaning, nInc, nIncMax, bIgnoreDisableCells);
  m_pTable->m_bUseInc = false;
  return true;
}

 //*************************** Bind ctrl *******************************

BindCtrlType �Ai_ListCtrlEx::HasCellBindCtrl(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
    return bctNone;

  if (m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.pCmbBox != nullptr)
    return bctComboBox;
  else
  if (m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.pDialog != nullptr)
    return bctDialog;
  else
  if (m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.pEdit != nullptr)
    return bctEdit;
  else
  if (m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.pIP != nullptr)
    return bctIP;

  return bctNone;
}

int �Ai_ListCtrlEx::GetCellBindCtrlID(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  return m_pTable->m_rows[nRow_inList].cells[nCell].bindTo.nID;
}

void �Ai_ListCtrlEx::EditBindCtrlKillFocus(bool bSet)
{
  m_pTable->CloseOpenedCtrl(true, bSet);
}

bool �Ai_ListCtrlEx::IsCtrlMeanWasSetedMulty()
{
  if (!m_pTable->m_bWithoutFirstNode)
    RETURN_AST_FALSE_BY_TRUE(!m_pTable->m_bNodeIs);
  return (m_pTable->m_meanWasSetedAfterBindCtrl == �Ai_ListCtrlEx_Table::mwsMulty) ? true : false;
}

bool �Ai_ListCtrlEx::IsCtrlMeanWasSetedSingle()
{
  if (!m_pTable->m_bWithoutFirstNode)
    RETURN_AST_FALSE_BY_TRUE(!m_pTable->m_bNodeIs);
  return (m_pTable->m_meanWasSetedAfterBindCtrl == �Ai_ListCtrlEx_Table::mwsSingle) ? true : false;
}

void �Ai_ListCtrlEx::ClearCtrlMeanWasSeted()
{
  if (m_pTable->m_bNodeIs)
    m_pTable->m_meanWasSetedAfterBindCtrl = �Ai_ListCtrlEx_Table::mwsNone;
}

//*************************** Set *******************************

bool �Ai_ListCtrlEx_Table::CanSetCellTextColor(int nRow_inList, int nCell, COLORREF nColor)
{
  RETURN_FALSE_BY_TRUE(!CanWorkWithRowIdx_list(nRow_inList));
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  RETURN_AST_FALSE_BY_TRUE(nColor < 0);
  return true;
}

bool �Ai_ListCtrlEx::SetCellTextColor(int nRow_inList, int nCell, COLORREF nColor)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanSetCellTextColor(nRow_inList, nCell, nColor));
  m_pTable->m_rows[nRow_inList].cells[nCell].nColorTxNew = (int)nColor;
  m_pTable->m_rows[nRow_inList].cells[nCell].nColorTxNew_beforeDisable = (int)nColor;
  return true;
}

bool �Ai_ListCtrlEx::SetCellTextColor(int nCellFlag, COLORREF nColor)
{
  RETURN_AST_FALSE_BY_TRUE(!m_pTable->m_bNodeIs);
  for (size_t r = 0; r < m_pTable->m_rows.size(); ++r)
    for (size_t c = 0; c < m_pTable->m_rows[r].cells.size(); ++c)
      if (m_pTable->m_rows[r].cells[c].nFlag == nCellFlag)
      {
        m_pTable->m_rows[r].cells[c].nColorTxNew = (int)nColor;
        m_pTable->m_rows[r].cells[c].nColorTxNew_beforeDisable = (int)nColor;
      }

  return true;
}

COLORREF �Ai_ListCtrlEx::GetCellTextColor(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell, false))
    return (COLORREF)-1;
  int nColor = m_pTable->m_rows[nRow_inList].cells[nCell].nColorTxNew;
  if (nColor > -1)
    return (COLORREF)nColor;
  if (nColor == -2)
    m_pTable->m_rows[nRow_inList].cells[nCell].nColorTxNew_beforeDisable;
  return m_pTable->m_rows[nRow_inList].cells[nCell].nColorTx;
}

bool �Ai_ListCtrlEx::SetCellBgColor(int nRow_inList, int nCell, COLORREF nColor)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanSetCellTextColor(nRow_inList, nCell, nColor)); // the same test
  m_pTable->m_rows[nRow_inList].cells[nCell].nColorBgNew = (int)nColor;
  return true;
}

COLORREF �Ai_ListCtrlEx::GetCellBgColor(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell, false))
    return (COLORREF)-1;
  return m_pTable->m_rows[nRow_inList].cells[nCell].nColorBgNew;
}

bool �Ai_ListCtrlEx::SetRowBgColor(int nRow_inList, COLORREF nColor)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  for (size_t i = 0; i < m_pTable->m_columns.size(); ++i)
    SetCellBgColor(nRow_inList, i, nColor);
  return true;
}

bool �Ai_ListCtrlEx::SetRowTextColor(int nRow_inList, COLORREF nColor)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  for (size_t i = 0; i < m_pTable->m_columns.size(); ++i)
    SetCellTextColor(nRow_inList, i, nColor);
  return true;
}

bool �Ai_ListCtrlEx::SetCellEnable(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->SetCellEnblDsbl(nRow_inList, nCell, false);
  return true;
}

bool �Ai_ListCtrlEx::SetCellEnable(int nRow_inList, int nCell, bool bEnable)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->SetCellEnblDsbl(nRow_inList, nCell, !bEnable);
  return true;
}

bool �Ai_ListCtrlEx::SetCellDisable(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->SetCellEnblDsbl(nRow_inList, nCell, true);
  return true;
}

bool �Ai_ListCtrlEx::SetRowEnable(int nRow_inList, bool bEnable)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  for (size_t i = 0; i < m_pTable->m_columns.size(); ++i)
    SetCellEnable(nRow_inList, i, bEnable);
  m_pTable->m_rows[nRow_inList].bRowDisable = bEnable ? false : true;
  return true;
}

bool �Ai_ListCtrlEx::SetCellHideText(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].bHideText = true;
  return true;
}

bool �Ai_ListCtrlEx::SetCellShowText(int nRow_inList, int nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].bHideText = false;
  return true;
}

void �Ai_ListCtrlEx::SetCellFlag(int nRow_inList, int nCell, int nFlag)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].nFlag = nFlag;
}

int �Ai_ListCtrlEx::GetCellFlag(int nRow_inList, int nCell)
{
  RETURN_1_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  return m_pTable->m_rows[nRow_inList].cells[nCell].nFlag;
}

void �Ai_ListCtrlEx::SetCellData(int nRow_inList, int nCell, vector<__int64>* pData)
{
  RETURN_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].data64 = *pData;
}

vector<__int64>& �Ai_ListCtrlEx::GetCellData(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
    m_pTable->m_rows[nRow_inList].cells[nCell].data64.end();
  return m_pTable->m_rows[nRow_inList].cells[nCell].data64;
}

void �Ai_ListCtrlEx::SetRowBoldText(int nRow_inList, CellTextStyle cts)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanWorkWithRow_list(nRow_inList));
  for (size_t i = 0; i < m_pTable->m_columns.size(); ++i)
    SetCellTextStyle(nRow_inList, i, cts);
}

void �Ai_ListCtrlEx::SetCellTextStyle(int nRow_inList, int nCell, CellTextStyle cts)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].textStyle = cts;
}

CellTextStyle �Ai_ListCtrlEx::GetCellTextStyle(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
  {
    ASSERT(false);
    return ctsNone;
  }
  return m_pTable->m_rows[nRow_inList].cells[nCell].textStyle;
}

// � imgType ����������� ����� �������� ����������� �� ��������� ����� UpdateView
void �Ai_ListCtrlEx::SetCellImg(int nRow_inList, int nCell, ImageListType ilt)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].imgType = ilt;
  m_pTable->m_rows[nRow_inList].cells[nCell].bUpdateImg = true;
}

void �Ai_ListCtrlEx::SetCellImg(int nRow_inList, int nCell, int nImg)
{
  RETURN_AST_BY_TRUE(!m_pTable->CanWorkWithCell_list(nRow_inList, nCell));
  m_pTable->m_rows[nRow_inList].cells[nCell].nImgIdx = nImg;
  m_pTable->m_rows[nRow_inList].cells[nCell].bUpdateImg = true;
}

ImageListType �Ai_ListCtrlEx::GetCellImgType(int nRow_inList, int nCell)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
  {
    ASSERT(false);
    return iltNone;
  }
  return m_pTable->m_rows[nRow_inList].cells[nCell].imgType;
}

bool �Ai_ListCtrlEx::IsPointInCell(/*In*/ POINT* pP, /*Out*/ int& nRow_inView, /*Out*/ int& nCell)
{
  RETURN_FALSE_BY_TRUE(!m_pTable->m_bNodeIs);
  for (size_t i = 0; i < m_pTable->m_showingRows.size(); ++i)
  {
    for (size_t c = 0; c < m_pTable->m_showingRows[i]->cells.size(); ++c)
    {
      CRect rectInList = 0;
      CRect rectInScreen = 0;
      if (GetCellRect(m_pTable->m_showingRows[i]->nIdxOneself_inList, c, &rectInList, &rectInScreen))
      {
        if (pP->x >= rectInScreen.left && pP->x <= rectInScreen.right
          && pP->y >= rectInScreen.top && pP->y <= rectInScreen.bottom)
        {
          nRow_inView = m_pTable->m_showingRows[i]->nIdx_inView;
          nCell = (int)c;
          return true;
        }
      }
    }
  }
  return false;
}

bool �Ai_ListCtrlEx::GetCellRect(int nRow_inList, int nCell, CRect* pRectInList, CRect* pRectInScreen)
{
  if (!m_pTable->CanWorkWithCell_list(nRow_inList, nCell))
  {
    ASSERT(false);
    return false;
  }
  RETURN_FALSE_BY_TRUE(pRectInList == nullptr);
  RETURN_FALSE_BY_TRUE(pRectInScreen == nullptr);

  CRect rectInList;
  CRect rectInScreen;
  m_pTable->GetSubItemRect(GetRowIdxInView(nRow_inList), nCell, rectInList, rectInScreen);
  *pRectInList = rectInList;
  *pRectInScreen = rectInScreen;
  return true;
}

void �Ai_ListCtrlEx::ClearNodeChildren(int& nNodeRow_inView)
{
  int nNodeRow_inList = CnvIdxViewToList(nNodeRow_inView);
  int nRow_inList = nNodeRow_inList;
  if (!m_pTable->CanWorkWithNode(nNodeRow_inList))
  {
    ASSERT(false);
    return;
  }

  for (int i = 0; i < m_pTable->m_rows[nNodeRow_inList].pNode->nCntChildren; ++i)
    m_pTable->m_rows[++nRow_inList].cells[m_pTable->m_rows[nNodeRow_inList].pNode->nComnSignificantCell].strText = _T("");
}

void �Ai_ListCtrlEx::SetTableEnable(bool bEnable)
{
  ASSERT(m_pTable->m_pListCtrl);
  if (m_pTable->m_pListCtrl)
  {
    m_pTable->m_pListCtrl->EnableWindow(bEnable);
    m_pTable->m_bTableEnable = bEnable;
  }
}

bool �Ai_ListCtrlEx::IsTableEnable()
{
  ASSERT(m_pTable->m_pListCtrl);
  if (m_pTable->m_pListCtrl)
    return m_pTable->m_bTableEnable;
  return true;
}

//*************************** Can work with ... *******************************

bool �Ai_ListCtrlEx_Table::CanWorkWithNode(int nNodeRow_inList)
{
  RETURN_AST_FALSE_BY_TRUE(!m_bNodeIs);
  RETURN_AST_FALSE_BY_OUT_OF_RANGE(nNodeRow_inList, 0, (int)m_rows.size() - 1);
  RETURN_AST_FALSE_BY_TRUE(!IsNode(nNodeRow_inList));
  return true;
}

bool �Ai_ListCtrlEx_Table::CanWorkWithCell_list(int nRow_inList, int nCell, bool bAssertByOutOfRange)
{
  if (bAssertByOutOfRange)
  {
    RETURN_AST_FALSE_BY_TRUE(!m_bNodeIs);
    if (nRow_inList < 0 || nRow_inList > (int)m_rows.size() - 1)
    {
      int i = 0;
      i = i;
    }
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nRow_inList, 0, (int)m_rows.size() - 1);
    RETURN_AST_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  }
  else
  {
    RETURN_FALSE_BY_TRUE(!m_bNodeIs);
    RETURN_FALSE_BY_OUT_OF_RANGE(nRow_inList, 0, (int)m_rows.size() - 1);
    RETURN_FALSE_BY_OUT_OF_RANGE(nCell, 0, (int)m_columns.size() - 1);
  }
  return true;
}

bool �Ai_ListCtrlEx_Table::CanWorkWithCell_view(int nRow_InView, int nCell, bool bAssertByOutOfRange)
{
  return CanWorkWithCell_list(m_pListCtrlExParent->CnvIdxViewToList(nRow_InView), nCell, bAssertByOutOfRange);
}

bool �Ai_ListCtrlEx_Table::CanWorkWithRow_view(int nRow_InView, bool bAssertByOutOfRange)
{
  return CanWorkWithCell_list(m_pListCtrlExParent->CnvIdxViewToList(nRow_InView), 0, bAssertByOutOfRange);
}

bool �Ai_ListCtrlEx_Table::CanWorkWithRow_list(int nRow_inList, bool bAssertByOutOfRange)
{
  return CanWorkWithCell_list(nRow_inList, 0, bAssertByOutOfRange);
}

bool �Ai_ListCtrlEx_Table::IsNode(int nRow_inList)
{
  if (m_rows[nRow_inList].pNode != nullptr)
    return true;
  return false;
}

bool �Ai_ListCtrlEx_Table::IsNode_inner(int nRow_inList)
{
  if (IsNode(nRow_inList)
    && m_rows[nRow_inList].nIdxParentNode_inList != -1)
    return true;
  return false;
}

int �Ai_ListCtrlEx_Table::GetCountColumns()
{
  return m_pListCtrl->GetHeaderCtrl()->GetItemCount();
}

void �Ai_ListCtrlEx_Table::UnwrapTree(bool bUnwrap)
{
  for (size_t i = 0; i < m_rows.size(); ++i)
    if (m_rows[i].pNode)
      m_rows[i].pNode->bUnwrapped = bUnwrap;
}

bool �Ai_ListCtrlEx_Table::ChangeUnwrapNodeMark(int nIdxRow, int nCell)
{
  if (m_rows[nIdxRow].pNode != nullptr)
    if (m_rows[nIdxRow].pNode->nUnwrapCell == nCell)
    {
      if (!m_bCheckUnwrap)
        m_rows[nIdxRow].pNode->bUnwrapped = !m_rows[nIdxRow].pNode->bUnwrapped;
      if (m_rows[nIdxRow].pNode->nCntSetedChildren == 0)
      {
        if ((m_rows[nIdxRow].pNode->nStyle & NS_UNWRAP))
          m_rows[nIdxRow].pNode->bUnwrapped = false;
        SetUnwrapNodeMark(nIdxRow, m_rows[nIdxRow].pNode->strMarkEmpty);
      }
      else
      if (m_rows[nIdxRow].pNode->bUnwrapped)
        SetUnwrapNodeMark(nIdxRow, m_rows[nIdxRow].pNode->strMarkOpen);
      else
        SetUnwrapNodeMark(nIdxRow, m_rows[nIdxRow].pNode->strMarkClose);
      return true;
    }
    return false;
}

bool �Ai_ListCtrlEx_Table::CheckUnwrapNodeMark(int nIdxNodeRow, int nCell)
{
  if (nCell == -1)
    return false;
  m_bCheckUnwrap = true;
  bool bRes = ChangeUnwrapNodeMark(nIdxNodeRow, nCell);
  m_bCheckUnwrap = false;
  return bRes;
}

void �Ai_ListCtrlEx_Table::SetUnwrapNodeMark(int nIdxNodeRow, CString strNewMark)
{
  CString* pstrUnwrapCell = &m_rows[nIdxNodeRow].cells[m_rows[nIdxNodeRow].pNode->nUnwrapCell].strText;

  *pstrUnwrapCell = DeleteUnwrapCellMark(nIdxNodeRow, *pstrUnwrapCell);
  *pstrUnwrapCell = strNewMark + *pstrUnwrapCell;
  m_rows[nIdxNodeRow].pNode->strMarkCurrent = strNewMark;
}

CString �Ai_ListCtrlEx_Table::DeleteUnwrapCellMark(int nIdxNodeRow, CString strSrc)
{
  if (!IsNode(nIdxNodeRow))
    return strSrc;
  CString strMarkEmpty = m_rows[nIdxNodeRow].pNode->strMarkEmpty;
  CString strMarkClose = m_rows[nIdxNodeRow].pNode->strMarkClose;
  CString strMarkOpen = m_rows[nIdxNodeRow].pNode->strMarkOpen;

  if (strSrc.Left(strMarkClose.GetLength()) == strMarkClose)
    strSrc.TrimLeft(strMarkClose);
  else
  if (strSrc.Left(strMarkOpen.GetLength()) == strMarkOpen)
    strSrc.TrimLeft(strMarkOpen);
  else
  if (strSrc.Left(strMarkEmpty.GetLength()) == strMarkEmpty)
    strSrc.TrimLeft(strMarkEmpty);

  return strSrc;
}

void �Ai_ListCtrlEx_Table::UnselectRows()
{
  vector<int> items;
  
  if (!GetSelectedItems(&items))
    return;

  int nCntItems = (int)items.size();

  for (int i = 0; i < nCntItems; ++i)
  {
    m_pListCtrl->SetItemState(items[i], 0, LVIS_SELECTED | LVIS_FOCUSED);
  }
}

void �Ai_ListCtrlEx_Table::UnselectRow(int nRow_inView)
{
  m_pListCtrl->SetItemState(nRow_inView, 0, LVIS_SELECTED | LVIS_FOCUSED);
}

bool �Ai_ListCtrlEx_Table::GetSelectedItems(vector<int>* pItems_inView)
{
  int uSelectedCount = m_pListCtrl->GetSelectedCount();
  int  nItem = -1;

  pItems_inView->clear();
  for (int i = 0; i < uSelectedCount; ++i)
  {
    nItem = m_pListCtrl->GetNextItem(nItem, LVNI_SELECTED);
    pItems_inView->push_back(nItem);
  }

  if ((int)pItems_inView->size() > 0)
    return true;
  return false;
}

void �Ai_ListCtrlEx_Table::SelectAndShowThisRow(int nRow_inView, bool bMoveMouse)
{
  CRect subItemRect(0, 0, 0, 0);
  CRect listRect(0, 0, 0, 0);
  POINT point;

  if (bMoveMouse)
  {
    m_pListCtrl->GetWindowRect(listRect);
    m_pListCtrl->GetSubItemRect(nRow_inView, m_rows[GetParentNodeIdx_inView(nRow_inView)].pNode->nUnwrapCell, LVIR_LABEL, subItemRect);
    GetCursorPos(&point);
    point.x = listRect.left + subItemRect.left + ((subItemRect.right - subItemRect.left) / 2);
  }

  UnselectRows();
  m_pListCtrl->SetSelectionMark(nRow_inView);
  m_pListCtrl->SetItemState(nRow_inView, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
  m_pListCtrl->EnsureVisible(nRow_inView, FALSE);
  
  if (bMoveMouse)
  {
    point.y = listRect.top + (subItemRect.top + (subItemRect.Height() / 2));
    if (point.y > listRect.bottom)
      point.y = listRect.bottom - 10;
    SetCursorPos(point.x, point.y);
  }
}

void �Ai_ListCtrlEx_Table::SelectThisRows(vector<int>* pRows_inView, bool bSelFirst)
{
  UnselectRows();

  int nCnt = (int)pRows_inView->size();

  for (int i = 0; i < nCnt; ++i)
    m_pListCtrl->SetItemState((*pRows_inView)[i], LVIS_SELECTED, LVIS_SELECTED);
  if (nCnt > 0)
  {
    int nSel = (*pRows_inView)[0];
    
    if (!bSelFirst)
      nSel = (*pRows_inView)[nCnt - 1];
    m_pListCtrl->SetItemState(nSel, LVIS_FOCUSED, LVIS_FOCUSED);
  }
}

bool �Ai_ListCtrlEx_Table::IsSetedChildInFront(int nRow_inView)
{
  int nCntRows = (int)m_rows.size();
  int nSignificantCell = 1;

  for (int i = nRow_inView; i < nCntRows; ++i)
  {
    if (m_rows[i].pNode != nullptr)
      break;

    if (m_rows[GetParentNodeIdx_inList(i)].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL)
      nSignificantCell = m_rows[GetParentNodeIdx_inList(i)].pNode->nComnSignificantCell;

    if (!m_rows[i].cells[nSignificantCell].strText.IsEmpty())
      return true;
  }

  return false;
}

// nFrom / nTo - borders in group
bool �Ai_ListCtrlEx_Table::MoveUpDownChildren(bool bUp, int nCopyFlag, int nFlag, vector<int>* pMovingCellsList, vector<int>* pSelItems)
{
  vector<int> movingCellsListNew;
  int n_CntCols = (int)m_columns.size();
  int nCntMoveList = (int)pMovingCellsList->size();

  // Prepare cells for move
  if (nFlag == MCHF_USE_INVERS_ENUM_LIST)
  {
    bool bAdd = true;

    for (int i = 0; i < n_CntCols; ++i)
    {
      bAdd = true;
      for (int j = 0; j < nCntMoveList; ++j)
      {
        if (i == pMovingCellsList->at(j))
        {
          bAdd = false;
          break;
        }
      }
      if (bAdd)
        movingCellsListNew.push_back(i);
    }
    pMovingCellsList = &movingCellsListNew;
  }
  nCntMoveList = (int)pMovingCellsList->size();

  bool bWasMove = false;
  int nCntMovingRows = pSelItems->size();
  bool bMove = false;
  int nFrom;
  int nTo;
  CString strTmp;
  int nIdx;

  for (int i = 0; i < nCntMovingRows; ++i)
  {
    nIdx = bUp ? i : (nCntMovingRows - 1 - i);

    // ������ ����� ���� ������ ��� ��������� �������, �� ���� ��������� - ����� �� ����� �� ����������
    // ������� � ������ ������ ������

    if (bUp && nIdx == 0) // first of selected item
    {
      int nRow_inList = m_showingRows[pSelItems->at(nIdx)]->nIdxOneself_inList;
      if (m_rows[nRow_inList].pNode == nullptr)
      {
        if (m_rows[nRow_inList - 1].pNode == nullptr)
          bMove = true;
      }
    }
    else
    if (!bUp && nIdx == (nCntMovingRows - 1)) // last of selected item
    {
      int nRow_inList = m_showingRows[pSelItems->at(nIdx)]->nIdxOneself_inList;
      if (m_rows[nRow_inList].pNode == nullptr)
      {
        if (nRow_inList + 1 != m_rows.size()
          && m_rows[nRow_inList + 1].pNode == nullptr)
          bMove = true;
      }
    }
    else
    {
      if (bUp)
      {
        if (pSelItems->at(nIdx) - 1 != pSelItems->at(nIdx - 1)) // not first item is not behind previous at once
          bMove = true;
      }
      else
      {
        if (pSelItems->at(nIdx) + 1 != pSelItems->at(nIdx + 1))
          bMove = true;
      }
    }

    if (bMove)
    {
      bMove = false;

      bool bRepeatAgain = false;
      int nMoveLoop = 0;
      int nToIdx = bUp ? -1 : 1;
      nFrom = m_showingRows[pSelItems->at(nIdx)]->nIdxOneself_inList;
      nTo = nFrom + nToIdx;

      while (1)
      {
        if (bRepeatAgain)
        {
        }
        bRepeatAgain = false;

        bool bWasItItemMoved = false;

        for (int m = 0; m < nCntMoveList; ++m) // move row cells
        {
          bool bFromCellEnable = IsCellEnable(m_rows[nFrom].nIdxOneself_inList, pMovingCellsList->at(m));
          bool bToCellEnable = IsCellEnable(m_rows[nTo].nIdxOneself_inList, pMovingCellsList->at(m));
          if (nCopyFlag & MCHF_COPY_IGNORE_DISABLE
              && (!bFromCellEnable || !bToCellEnable))
              continue;

          if (nCopyFlag & MCHF_COPY_CELL_INFO)
          {
            CELL cellTmp = m_rows[nTo].cells[pMovingCellsList->at(m)];
            m_rows[nTo].cells[pMovingCellsList->at(m)] = m_rows[nFrom].cells[pMovingCellsList->at(m)];
            m_rows[nFrom].cells[pMovingCellsList->at(m)] = cellTmp;
            m_rows[nFrom].cells[pMovingCellsList->at(m)].bUpdateImg = true;
            m_rows[nTo].cells[pMovingCellsList->at(m)].bUpdateImg = true;
          }
          else
          {
            strTmp = m_rows[nTo].cells[pMovingCellsList->at(m)].strText;
            m_rows[nTo].cells[pMovingCellsList->at(m)].strText = m_rows[nFrom].cells[pMovingCellsList->at(m)].strText;
            m_rows[nFrom].cells[pMovingCellsList->at(m)].strText = strTmp;
          }

          bWasMove = true;
          bWasItItemMoved = true;
        }

        if (bWasItItemMoved
            && nCopyFlag & MCHF_COPY_ROW_INFO)
        {
          m_rows[nTo].bCopyCells = m_rows[nFrom].bCopyCells = false;
          ROW rowTmp;
          rowTmp = m_rows[nTo];
          m_rows[nTo] = m_rows[nFrom];
          m_rows[nFrom] = rowTmp;
          m_rows[nTo].bCopyCells = m_rows[nFrom].bCopyCells = true;
          if (rowTmp.pNode)
            delete rowTmp.pNode;
        }

        if (bWasItItemMoved
            && nCopyFlag & MCHF_COPY_CALL_OUTER_MOVE_FUNC
            && m_pListCtrlExParent->m_pOuterMoveFunc)
        {
          �Ai_ListCtrlEx::OUTER_MOVE_FUNC mf{};
          mf.nMovedRowActive_inList = nTo; // ����� ������ ��� ������ �� ������� nTo
          mf.nMovedRowPassive_inList = nFrom; // ����� ������ ��� ������ �� ������� nFrom
          mf.bUp = bUp;
          mf.nMoveLoop = nMoveLoop;

          OuterMoveFunc = reinterpret_cast<void(*)(void*)>(m_pListCtrlExParent->m_pOuterMoveFunc);
          OuterMoveFunc(&mf);

          if (mf.bAnalysis)
          {
            ++nMoveLoop;
            bRepeatAgain = mf.bRepeatAgain;
            nFrom = nTo;
            nTo = bUp ? nFrom - 1 : nFrom + 1;
            continue;
          }
        }

        if (bWasItItemMoved)
        {
          int nMoveLoopCount = nMoveLoop + 1;
          pSelItems->at(nIdx) += bUp ? (-1 * nMoveLoopCount) : (1 * nMoveLoopCount);
        }

        break;
      } // while
      
    } // if (bMove)

  }

  return bWasMove;
}

void �Ai_ListCtrlEx_Table::ClearCell(CELL* pCell)
{
  pCell->bCellDisable = false; // new
  pCell->bHideText = false; // new
  pCell->nFlag = -1; // new
  pCell->data64.clear();
  pCell->strText = _T("");
  pCell->nStyle = 0;

  COLORREF* clr = const_cast<COLORREF*>(&pCell->nColorBg);
  *clr = COLOR_LCE_WHITE;

  pCell->nColorBgNew = -1; // new

  clr = const_cast<COLORREF*>(&pCell->nColorTx);
  *clr = COLOR_LCE_BLACK; // new

  pCell->nColorTxNew = -1; // new
  pCell->nColorTxNew_beforeDisable = -1;
  pCell->bindTo.bDisableBind = false;
  pCell->bindTo.pCmbBox = nullptr;
  pCell->bindTo.pDialog = nullptr;
  pCell->bindTo.pEdit = nullptr;
  pCell->bindTo.pIP = nullptr;
  pCell->bindTo.pstrExchange = nullptr;
  pCell->bindTo.nSelRowByDefault = -1;
  pCell->bindTo.strByDefault = _T("");
  pCell->bindTo.nID = -1;
  pCell->serviceInfo.clear(); // new
  
  ClearCellBind(&pCell->bindTo); // new
}

void  �Ai_ListCtrlEx_Table::ClearCellBind(CELL_BINDING* pCellBind)
{
  pCellBind->bDisableBind = false;
  pCellBind->pstrExchange = nullptr;
  pCellBind->pCmbBox = nullptr;
  pCellBind->pEdit = nullptr;
  pCellBind->pIP = nullptr;
  pCellBind->nSelRowByDefault = -1;
  pCellBind->pDialog = nullptr;
  pCellBind->strByDefault = _T("");
  pCellBind->nID = -1;
}

int �Ai_ListCtrlEx_Table::SearchInNodeChildren(SEARCH_IN_NODE_CHILDREN nSearchFlag, int nNodeRow_InList)
{
  LONGLONG nStyle = m_rows[nNodeRow_InList].pNode->nStyle;
  int nSignCell = m_rows[nNodeRow_InList].pNode->nComnSignificantCell;
  int nCntChildren = m_rows[nNodeRow_InList].pNode->nCntChildren;
  int nLastChildInGroup = RCHI_LIST_FULL;
  int nFreeTailIndex = RCHI_LIST_FULL;
  int nFirstNodeChild = nNodeRow_InList + 1;
  
  if ((nStyle & NS_USE_COMN_SIGNIFICANT_CELL))
  {
    for (int i = 0, j = 0; i < nCntChildren; ++i)
    {
      j = nFirstNodeChild + i;

      CString& strText = m_rows[j].cells[nSignCell].strText;
      bool bFreeCell = strText.IsEmpty();
      /*if (!bFreeCell
          && pCellTextAsEmpty
          && !pCellTextAsEmpty->empty())
      {
        for (size_t t = 0; t < pCellTextAsEmpty->size(); ++t)
        {
          if (strText == pCellTextAsEmpty->at(t))
          {
            bFreeCell = true;
            break;
          }
        }
      }*/

      if (bFreeCell)
      {
        nFreeTailIndex = j; 
        if (nSearchFlag == searchFirstGapIndex)
          return nFreeTailIndex;
        else
        if (nSearchFlag == searchFirstGapIndex_couple)
        {
          if (i + 1 < nCntChildren) // it is not last row from children of node
          {
            CString& strTextNextRow = m_rows[j + 1].cells[nSignCell].strText;
            bFreeCell = strText.IsEmpty();
            /*if (!bFreeCell
                && pCellTextAsEmpty
                && !pCellTextAsEmpty->empty())
            {
              for (size_t t = 0; t < pCellTextAsEmpty->size(); ++t)
              {
                if (strText == pCellTextAsEmpty->at(t))
                {
                  bFreeCell = true;
                  break;
                }
              }
            }*/

            if (bFreeCell)
              return nFreeTailIndex;
          }
        }
      }
      else
        nLastChildInGroup = (i + 1);
    }

    if (nSearchFlag == searchFreeTailIndex)
      return nFreeTailIndex;
    else
    if (nSearchFlag == searchLastSetedChild)
      return nLastChildInGroup;
  }

  return -1;
}

// ���������� �������� ��� �����, ������������ ��������� ��������, ������� ����� ctrls:
//   - �� ��������� (���� ctrl ���� == <~~~>)
//   - �����, ������� ����� ������ ����
void �Ai_ListCtrlEx_Table::SetMeansInCtrlsForChild(int nChildRow_InList)
{
  ROW* pNodeRow = &m_rows[GetParentNodeIdx_inList(nChildRow_InList)];
  
  if (pNodeRow->nIdx_inView == -1
    && pNodeRow->nIdxOneself_inList == 0
    && m_bWithoutFirstNode)
    return;

  LONGLONG nChildStyle = m_rows[nChildRow_InList].nStyle;
  CELL_BINDING *pBind;
  int nCntCols = (int)m_columns.size();
  bool bNodeHasDefValue;
  int nSelDef;
  CString strDef;
  bool bSetDefaultForce = true;
  CString strCurr;
  CString strNeedSet;

  for (int i = 0; i < nCntCols; ++i)
  {
    strDef = strNeedSet = _T("");
    bNodeHasDefValue = false;
    nSelDef = -1;

    pBind = &m_rows[nChildRow_InList].cells[i].bindTo;
    if (pBind->bDisableBind)
      continue;
    else
    if (pBind->pCmbBox != nullptr)
    {
     nSelDef = pNodeRow->cells[i].bindTo.nSelRowByDefault;
     bNodeHasDefValue = ((nSelDef >= 0) && (nSelDef < pBind->pCmbBox->GetCount())) ? true : false;
     if (nSelDef == -1)
     {
        strDef = pNodeRow->cells[i].bindTo.strByDefault;
        bSetDefaultForce = pNodeRow->cells[i].bindTo.bSetDefaultForce;
        bNodeHasDefValue = (strDef.IsEmpty()) ? false : true;
     }
    }
    else
    if (pBind->pDialog != nullptr || pBind->pEdit != nullptr || pBind->pIP != nullptr)
    {
     strDef = pNodeRow->cells[i].bindTo.strByDefault;
     bSetDefaultForce = pNodeRow->cells[i].bindTo.bSetDefaultForce;
     bNodeHasDefValue = (strDef.IsEmpty()) ? false : true;
    }
    else
     continue;

    strCurr = m_showingRows[pNodeRow->nIdx_inView]->cells[i].strText;

    if (strCurr.IsEmpty() || (strCurr == DIFF_NODE_CHLDR_CELL_MARKS))
    {
      if ((nChildStyle & CHS_SET_CTRL_BY_NODE_DEF_OR_CURR)) 
      {
        if (bNodeHasDefValue) // get def value
        {
          if (pBind->pCmbBox != nullptr)
          {
            if (nSelDef != -1)
            {
              pBind->pCmbBox->SetCurSel(nSelDef);
              pBind->pCmbBox->GetLBText(nSelDef, strNeedSet);
            }
            else
              strNeedSet = strDef;
          }
          else
          if (pBind->pDialog != nullptr)
            strNeedSet = strDef;          
        }
        else // no def value
        {
          if (pBind->pCmbBox != nullptr)
          {
            // ... exception
            //pBind->pCmbBox->SetCurSel(0);
            //pBind->pCmbBox->GetLBText(0, strNeedSet);
          }
        }
      }
    }
    else // strCurr has mean
    {
      strCurr.TrimLeft(_T("< "));
      strCurr.TrimRight(_T(" >"));
      
      if ((pNodeRow->pNode->nStyle & CHS_SET_CTRL_BY_NODE_DEF_OR_CURR)) //  need set curr node value
      {
        if (pBind->pCmbBox != nullptr)
          SelectThisStr(pBind->pCmbBox, strCurr);
        strNeedSet = strCurr;
      }
    }

    if (!strNeedSet.IsEmpty())
    {
      bool bSetDefault = bSetDefaultForce;
      if (!bSetDefault)
        bSetDefault = m_rows[nChildRow_InList].cells[i].strText.IsEmpty();

      if (bSetDefault)
      m_rows[nChildRow_InList].cells[i].strText = strNeedSet;
      UpdateNodeCellIfNeed(nChildRow_InList, i, strNeedSet);
    }
  } // for
}

void �Ai_ListCtrlEx_Table::UpdateCellCtrlsAfterSetNewMeansInCells(int nChildRow_InList)
{
  int nCntCols = (int)m_columns.size();
  CString strNewText;
  
  for (int i = 0; i < nCntCols; ++i)
  {
    if (m_rows[nChildRow_InList].cells[i].bindTo.bDisableBind)
      m_rows[nChildRow_InList].cells[i].strText = _T("");
    else
    {
      strNewText = m_rows[nChildRow_InList].cells[i].strText;
      if (m_rows[nChildRow_InList].cells[i].bindTo.pCmbBox != nullptr)
      {
        SelectThisStr(m_rows[nChildRow_InList].cells[i].bindTo.pCmbBox, strNewText); // if cell have ownew ctrl
        UpdateNodeCellIfNeed(nChildRow_InList, i, strNewText);
      }
      else
      if (m_rows[nChildRow_InList].cells[i].bindTo.pDialog != nullptr
       || m_rows[nChildRow_InList].cells[i].bindTo.pEdit != nullptr
       || m_rows[nChildRow_InList].cells[i].bindTo.pIP != nullptr)
        UpdateNodeCellIfNeed(nChildRow_InList, i, strNewText);
    }
  }
}

bool �Ai_ListCtrlEx_Table::IsDisableBindCorrect(BIND_TO_CELL* pBindToCells)
{
  if (pBindToCells != nullptr)
  {
    int nCnt = (int)pBindToCells->ctrlToCell.size();
    for (int i = 0; i < nCnt; ++i)
    {
      RETURN_FALSE_BY_OUT_OF_RANGE(pBindToCells->ctrlToCell[i].nCell, 0, (int)m_columns.size() - 1);
    }
  }
  
  return true;
}

void �Ai_ListCtrlEx_Table::BindToCell(BIND_TO_CELL* pBindToCells, int nCell, CELL_BINDING *pCellBinding)
{
  if (pBindToCells != nullptr)
  {
    int nCntBinds = pBindToCells->ctrlToCell.size();

    for (int i = 0; i < nCntBinds; ++i)
      if (nCell == pBindToCells->ctrlToCell[i].nCell)
      {
        pCellBinding->bDisableBind = pBindToCells->ctrlToCell[i].bDisableBind;
        pCellBinding->pstrExchange = pBindToCells->ctrlToCell[i].pstrExchange;
        pCellBinding->pCmbBox = pBindToCells->ctrlToCell[i].pCmbBox;
        pCellBinding->nSelRowByDefault = pBindToCells->ctrlToCell[i].nSelRowByDefault;
        pCellBinding->pDialog = pBindToCells->ctrlToCell[i].pDialog;
        pCellBinding->pEdit = pBindToCells->ctrlToCell[i].pEdit;
        pCellBinding->pIP = pBindToCells->ctrlToCell[i].pIP;
        pCellBinding->strByDefault = pBindToCells->ctrlToCell[i].strByDefault;
        pCellBinding->nID = pBindToCells->ctrlToCell[i].nID;
      }
  }
}

bool �Ai_ListCtrlEx_Table::RecalcParamsAfterClickOnCell(int nRow_inView, int nCell)
{
  CRect subItemRect(0, 0, 0, 0);
  m_eventInDisableBindCell.nRow_inView = -1;
  m_eventInDisableBindCell.nCell = -1;
  m_eventInDisableBindCell.cellRect = subItemRect;

  CRect subItemWinRect(0, 0, 0, 0);
  GetSubItemRect(nRow_inView, nCell, subItemRect, subItemWinRect);

  POINT mouse_point = {0};
  GetCursorPos(&mouse_point);

  if((mouse_point.x > subItemWinRect.left && mouse_point.x < subItemWinRect.right) &&
    (mouse_point.y > subItemWinRect.top && mouse_point.y < subItemWinRect.bottom))
  {
    m_eventInDisableBindCell.nRow_inView = nRow_inView;
    m_eventInDisableBindCell.nCell = nCell;
    m_eventInDisableBindCell.cellRect = subItemRect;
    return true;
  }

  return false;
}

void �Ai_ListCtrlEx_Table::GetSubItemRect(int nRow_inView, int nCell, CRect& subItemRect, CRect& subItemWinRect)
{
  RECT rectList = {0};

  m_pListCtrl->GetSubItemRect(nRow_inView, nCell, LVIR_LABEL, subItemRect);
  subItemWinRect = subItemRect; 
  m_pListCtrl->GetWindowRect(&rectList);
  subItemWinRect.left += rectList.left;
  subItemWinRect.right = rectList.left + subItemWinRect.right;
  subItemWinRect.top += rectList.top;
  subItemWinRect.bottom = rectList.top + subItemWinRect.bottom;
}

�Ai_ListCtrlEx_Table::CELL_BINDING& �Ai_ListCtrlEx_Table::CELL_BINDING::operator=(const �Ai_ListCtrlEx_Table::CELL_BINDING& ob)
{
  if (this == &ob)
    return *this;

  bDisableBind = ob.bDisableBind;
  pstrExchange = ob.pstrExchange;
  pCmbBox = ob.pCmbBox;
  nSelRowByDefault = ob.nSelRowByDefault;
  pDialog = ob.pDialog;
  pEdit = ob.pEdit;
  pIP = ob.pIP;
  strByDefault = ob.strByDefault;
  nID = ob.nID;
  return *this;
}

bool �Ai_ListCtrlEx_Table::CELL_BINDING::operator==(const �Ai_ListCtrlEx_Table::CELL_BINDING& ob)
{
  if (this == &ob)
    return true;

  if (bDisableBind != ob.bDisableBind
    || pstrExchange != ob.pstrExchange
    || pCmbBox != ob.pCmbBox
    || nSelRowByDefault != ob.nSelRowByDefault
    || pDialog != ob.pDialog
    || pEdit != ob.pEdit
    || pIP != ob.pIP
    || strByDefault != ob.strByDefault
    || nID != ob.nID)
    return false;
  return true;
}

�Ai_ListCtrlEx_Table::CELL& �Ai_ListCtrlEx_Table::CELL::operator=(const �Ai_ListCtrlEx_Table::CELL& c)
{
  if (this == &c)
    return *this;

  bCellDisable = c.bCellDisable;
  bHideText = c.bHideText;
  nFlag = c.nFlag;
  data64 = c.data64;
  strText = c.strText;
  textStyle = c.textStyle;

  COLORREF* clr = const_cast<COLORREF*>(&nColorBg);
  *clr = c.nColorBg;

  nColorBgNew = c.nColorBgNew;
  
  clr = const_cast<COLORREF*>(&nColorTx);
  *clr = c.nColorTx;
  
  nColorTxNew = c.nColorTxNew;
  nColorTxNew_beforeDisable = c.nColorTxNew_beforeDisable;

  nStyle = c.nStyle;
  serviceInfo = c.serviceInfo;
  bUpdateImg = c.bUpdateImg;
  imgType = c.imgType;
  nImgIdx = c.nImgIdx;
  bindTo = c.bindTo;
  bindToTmp = c.bindToTmp;

  return *this;
}

�Ai_ListCtrlEx_Table::ROW& �Ai_ListCtrlEx_Table::ROW::operator=(const �Ai_ListCtrlEx_Table::ROW& r)
{
  if (this == &r)
    return *this;

  bCopyCells = r.bCopyCells;
  if (r.bCopyCells)
    cells = r.cells;

  nStyle = r.nStyle;
  bRowDisable = r.bRowDisable;
  bShowing = r.bShowing;
  bSeted = r.bSeted;
  nIdx_inView = r.nIdx_inView;
  nIdxParentNode_inList = r.nIdxParentNode_inList;
  //nIdxOneself_inList = r.nIdxOneself_inList; // ������������ ��� ���������� ������
  serviceInfo = r.serviceInfo;

   if (r.pNode)
   {
     if (!pNode)
       pNode = new NODE_ROW();
     ZeroMemory(pNode, sizeof (NODE_ROW));
     CopyMemory(pNode, r.pNode, sizeof (NODE_ROW));
   }
   else
   {
      if (pNode)
      {
        delete pNode;
        pNode = nullptr;
      }
   }

  return *this;
}

int �Ai_ListCtrlEx_Table::SelchangeCmbbox()
{
  CString strSelectedInCmbbox;

  if (m_eventInDisableBindCell.pcmbOpened != nullptr)
  {
    m_eventInDisableBindCell.pcmbOpened->GetWindowText(strSelectedInCmbbox);
    SelectThisStr(m_eventInDisableBindCell.pcmbOpened, strSelectedInCmbbox);
  }
  else
  if (!m_eventInDisableBindCell.strSelectedInCmbbox.IsEmpty())
  {
    strSelectedInCmbbox = m_eventInDisableBindCell.strSelectedInCmbbox;
    m_eventInDisableBindCell.strSelectedInCmbbox = _T("");
  }
  else
    return 0;

  return ChangeTextInCellAfterCtrl(strSelectedInCmbbox);
}

int �Ai_ListCtrlEx_Table::ChangeTextInCellAfterCtrl(CString strNewText)
{
  int nRes = 1;

  if (m_showingRows[m_eventInDisableBindCell.nRow_inView]->pNode != nullptr)
  {
    if (m_showingRows[m_eventInDisableBindCell.nRow_inView]->pNode->nCntSetedChildren < 1)
      return 0;

    if (m_showingRows[m_eventInDisableBindCell.nRow_inView]->pNode->nStyle & NS_MSG_BEFORE_COMMON_INIT
      && strNewText != _T("..."))
    {
      CString strMsg;

      strMsg.Format(IDS_ALL_MEMBERS_ONE_VALUE, // ������ ���� ������ ������ ������ ���� ��������:\n\n\"%s\"->%s?
        m_columns[m_eventInDisableBindCell.nCell].strName, strNewText);
      if (AfxMessageBox(strMsg, MB_ICONQUESTION | MB_YESNO) == IDNO)
      {
        CString strTmp = m_pListCtrl->GetItemText(m_eventInDisableBindCell.nRow_inView, m_eventInDisableBindCell.nCell);
        strTmp.TrimLeft(_T("< "));
        strTmp.TrimRight(_T(" >"));
        if (strTmp == _T("..."))
        {
          m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].strText = m_eventInDisableBindCell.strBefore;
          m_pListCtrl->SetItemText(m_eventInDisableBindCell.nRow_inView, m_eventInDisableBindCell.nCell, m_eventInDisableBindCell.strBefore);
        }
        return 0;
      }
    }
    if (strNewText != _T("..."))
    {
      InitAllCellsOfNodeChildren(m_showingRows[m_eventInDisableBindCell.nRow_inView]->nIdxOneself_inList, m_eventInDisableBindCell.nCell, strNewText);
      nRes = 2; // - update all view
    }
    else
    {
      m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].strText = strNewText;
      m_pListCtrl->SetItemText(m_eventInDisableBindCell.nRow_inView, m_eventInDisableBindCell.nCell, strNewText);
      return 0;
    }
  }

  if (nRes == 2) // it`s node
    strNewText = _T("< ") + strNewText + _T(" >");

  if (!m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].bHideText)
    m_pListCtrl->SetItemText(m_eventInDisableBindCell.nRow_inView, m_eventInDisableBindCell.nCell, strNewText);
  m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].strText = strNewText;

  if (nRes != 2) // was seted new mean in child - maybe request update node cell
    UpdateNodeCellIfNeed(m_showingRows[m_eventInDisableBindCell.nRow_inView]->nIdxOneself_inList, m_eventInDisableBindCell.nCell, strNewText);

  return nRes;
}

void �Ai_ListCtrlEx_Table::UpdateNodeCellIfNeed(int nRow_inList, int nCell, CString strText)
{
  if (nCell == m_rows[GetParentNodeIdx_inList(nRow_inList)].pNode->nUnwrapCell
    && m_rows[GetParentNodeIdx_inList(nRow_inList)].pNode->nStyle & NS_NO_CHANGE_UNWRAP_NAME_BY_BIND_SEL)
    return;

  if (m_rows[nRow_inList].cells[nCell].nStyle & SS_NO_CHG_NODE_CELL_BY_CHG_CHILD_BIND_CELL)
    return;

  int nIdxNode_inList = GetParentNodeIdx_inList(nRow_inList);
  int nIdxNode_inView = m_rows[nIdxNode_inList].nIdx_inView;
  CString strMarkAboutDifferent = _T("< ") + strText + _T(" >");

  if (IfCellMarksOfSetedNodeChildrenDifferent(nIdxNode_inList, nCell))
    strMarkAboutDifferent = DIFF_NODE_CHLDR_CELL_MARKS;
          
  if (nIdxNode_inView != -1 
    && strMarkAboutDifferent != m_rows[nIdxNode_inList].cells[nCell].strText
    && !m_rows[nIdxNode_inList].cells[nCell].bHideText)
     m_pListCtrl->SetItemText(nIdxNode_inView, nCell, strMarkAboutDifferent);
  m_rows[nIdxNode_inList].cells[nCell].strText = strMarkAboutDifferent;
}

void �Ai_ListCtrlEx_Table::InitAllCellsOfNodeChildren(int nIdxNode, int nCell, CString strText)
{
  int nCntChildren = m_rows[nIdxNode].pNode->nCntChildren;
  int nIdxChild;
  int nIdxChildFirst = nIdxNode + 1;

  for (int i = 0; i < nCntChildren; ++i)
  {
    nIdxChild = nIdxChildFirst + i;

    if (m_rows[nIdxNode].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL
      && m_rows[nIdxChild].cells[m_rows[nIdxNode].pNode->nComnSignificantCell].strText.IsEmpty())
      continue;

    m_rows[nIdxChild].cells[nCell].strText = strText;
    if (m_rows[nIdxChild].cells[nCell].bindTo.pCmbBox != nullptr)
      SelectThisStr(m_rows[nIdxChild].cells[nCell].bindTo.pCmbBox, strText);
  }
}

void �Ai_ListCtrlEx_Table::UpdateAllCellsOfNodeChildren(int nIdxNode, int nCell, CString strText, int nMeaning, int nInc, int nIncMax, bool bIgnoreDisableCells)
{
  int nCntChildren = m_rows[nIdxNode].pNode->nCntChildren;
  int nIdxChild;
  int nIdxChildFirst = nIdxNode + 1;

  for (int i = 0; i < nCntChildren; ++i)
  {
    nIdxChild = nIdxChildFirst + i;

    if (bIgnoreDisableCells && !IsCellEnable(nIdxChild, nCell))
      continue;

    if (m_bUseInc)
    {
      if (i > 0)
      {
        if ((nMeaning + nInc) <= nIncMax)
          nMeaning += nInc;
        else
          break;
      }
      strText = CAi_Str::ToString(nMeaning);
    }

    m_rows[nIdxChild].cells[nCell].strText = strText;
    if (m_rows[nIdxChild].cells[nCell].bindTo.pCmbBox != nullptr)
      SelectThisStr(m_rows[nIdxChild].cells[nCell].bindTo.pCmbBox, strText);
  }
}

bool �Ai_ListCtrlEx_Table::IfCellMarksOfSetedNodeChildrenDifferent(int nIdxNode, int nCell)
{
  int nCntChildren = m_rows[nIdxNode].pNode->nCntChildren;
  int nIdxChild;
  int nIdxChildFirst = nIdxNode + 1;
  CString strCmp;

  for (int i = 0; i < nCntChildren; ++i)
  {
    nIdxChild = nIdxChildFirst + i;

    if (m_rows[nIdxNode].pNode->nStyle & NS_USE_COMN_SIGNIFICANT_CELL
      && m_rows[nIdxChild].cells[m_rows[nIdxNode].pNode->nComnSignificantCell].strText.IsEmpty())
      continue;

    if (!m_rows[nIdxChild].cells[nCell].strText.IsEmpty())
    {
      if (strCmp.IsEmpty())
        strCmp = m_rows[nIdxChild].cells[nCell].strText;
      else
      if (strCmp != m_rows[nIdxChild].cells[nCell].strText)
        return true;
    }
  }

  return false;
}

void �Ai_ListCtrlEx_Table::SelectThisStr(CComboBox *pCmbBox, CString strText)
{
  if (!pCmbBox->m_hWnd)
    return;

  int nCnt = pCmbBox->GetCount();
  CString strTextIn;

  for (int i = 0; i < nCnt; ++i)
  {
    pCmbBox->GetLBText(i, strTextIn);
    if (strTextIn == strText)
    {
      pCmbBox->SetCurSel(i);
      break;
    }
  }
}

void �Ai_ListCtrlEx_Table::CloseOpenedCtrl(bool bKillFocus, bool bSet)
{
  if (m_eventInDisableBindCell.pcmbOpened != nullptr)
  {
    m_eventInDisableBindCell.pcmbOpened->GetWindowText(m_eventInDisableBindCell.strSelectedInCmbbox);
    SelectThisStr(m_eventInDisableBindCell.pcmbOpened, m_eventInDisableBindCell.strSelectedInCmbbox);
    m_eventInDisableBindCell.pcmbOpened->ShowWindow(SW_HIDE);
    m_eventInDisableBindCell.pcmbOpened = nullptr;
  }
  else
  if (m_eventInDisableBindCell.pedtOpened != nullptr && bKillFocus)
  {
    if (bSet)
    {
      CString strText;
      m_eventInDisableBindCell.pedtOpened->GetWindowText(strText);
      m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].strText = strText;
      m_pListCtrlExParent->UpdateView();
      m_pListCtrlExParent->m_bindInfo.strNewText = m_showingRows[m_pListCtrlExParent->m_bindInfo.nRow_inView]->cells[m_pListCtrlExParent->m_bindInfo.nCell].strText;
      SendMsg(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, (WPARAM)m_pParentWnd, (LPARAM)m_pCurrCellBind->nID);
    }
    else
      SendMsg(MSG_MEAN_WAS_NOT_SETED_AFTER_BIND_CTRL, (WPARAM)m_pParentWnd, (LPARAM)m_pCurrCellBind->nID);

    m_eventInDisableBindCell.pedtOpened->ShowWindow(SW_HIDE);
    m_eventInDisableBindCell.pedtOpened = nullptr;
  }
  else
  if (m_eventInDisableBindCell.pipOpened != nullptr && bKillFocus)
  {
    if (bSet)
    {
      BYTE n1 = 0, n2 = 0, n3 = 0, n4 = 0;
      m_eventInDisableBindCell.pipOpened->GetAddress(n1, n2, n3, n4);
      CString strText;
      strText.Format(_T("%d.%d.%d.%d"), n1, n2, n3, n4);
      m_showingRows[m_eventInDisableBindCell.nRow_inView]->cells[m_eventInDisableBindCell.nCell].strText = strText;
      m_pListCtrlExParent->UpdateView();
      m_pListCtrlExParent->m_bindInfo.strNewText = m_showingRows[m_pListCtrlExParent->m_bindInfo.nRow_inView]->cells[m_pListCtrlExParent->m_bindInfo.nCell].strText;
      SendMsg(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, (WPARAM)m_pParentWnd, (LPARAM)m_pCurrCellBind->nID);
    }
    else
      SendMsg(MSG_MEAN_WAS_NOT_SETED_AFTER_BIND_CTRL, (WPARAM)m_pParentWnd, (LPARAM)m_pCurrCellBind->nID);

    m_eventInDisableBindCell.pipOpened->ShowWindow(SW_HIDE);
    m_eventInDisableBindCell.pipOpened = nullptr;
  }
}

void �Ai_ListCtrlEx_Table::CommonSignificantCellWasCleared(int nRow_inView)
{
  m_showingRows[nRow_inView]->serviceInfo.clear();
  m_showingRows[nRow_inView]->bSeted = false;

  int nIdxNodeRow = GetParentNodeIdx_inView(nRow_inView);
  int nIdxShowingNodeRow = m_rows[nIdxNodeRow].nIdx_inView;
  m_rows[nIdxNodeRow].pNode->nCntSetedChildren--;

  if (m_rows[nIdxNodeRow].pNode->nCntSetedChildren == 0)
  {
    CheckUnwrapNodeMark(nIdxNodeRow, m_rows[nIdxNodeRow].pNode->nUnwrapCell); // clear child
    ClearNodeCtrlCells(nIdxShowingNodeRow);
  }
}

void �Ai_ListCtrlEx_Table::ClearNodeCtrlCells(int nNodeRow_inView)
{
  if (nNodeRow_inView == -1) // when without first node
    return;

  int nCntCols = (int)m_columns.size();

  for (int i = 0; i < nCntCols; ++i)
  {
    if (m_showingRows[nNodeRow_inView]->cells[i].bindTo.pCmbBox != nullptr
      || m_showingRows[nNodeRow_inView]->cells[i].bindTo.pDialog != nullptr
      || m_showingRows[nNodeRow_inView]->cells[i].bindTo.pEdit != nullptr
      || m_showingRows[nNodeRow_inView]->cells[i].bindTo.pIP != nullptr)
    {
      m_showingRows[nNodeRow_inView]->cells[i].strText = _T("");
      m_pListCtrl->SetItemText(nNodeRow_inView, i, _T(""));
    }
  }
}

void �Ai_ListCtrlEx_Table::SendMsg(DWORD nMsg, WPARAM wp, LPARAM lp)
{
   if (!::SendMessage(::AfxGetApp()->m_pMainWnd->m_hWnd, nMsg, wp, lp))
    ::SendMessage(m_pParentWnd, nMsg, wp, lp);
}
