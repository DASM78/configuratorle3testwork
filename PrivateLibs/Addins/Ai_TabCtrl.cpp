#include "stdafx.h"
#include <cmath>
#include "Ai_TabCtrl.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CAi_TabCtrl, CTabCtrl)

CAi_TabCtrl::CAi_TabCtrl()
{
  m_crSelColour = RGB(0, 0, 255);
  m_crUnselColour = RGB(50, 50, 50);
}

CAi_TabCtrl::~CAi_TabCtrl()
{
  m_SelFont.DeleteObject();
  m_UnselFont.DeleteObject();
}

BEGIN_MESSAGE_MAP(CAi_TabCtrl, CTabCtrl)
  ON_NOTIFY_REFLECT(TCN_SELCHANGING, OnTcnSelchanging)
  ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnTcnSelchange)
END_MESSAGE_MAP()

// pDC : pointer to your device-context
// str : the text
// rect: the rectangle
// nOptions: can be a combination of ETO_CLIPPED and ETO_OPAQUE
// (see documentation of ExtTextOut for more details)
void DrawRotatedText(CDC* pDC, const CString str, CRect rect, double angle, UINT nOptions = 0)
{
  // convert angle to radian
  double pi = 3.141592654;
  double radian = pi * 2 / 360 * angle;

  // get the center of a not-rotated text
  CSize TextSize = pDC->GetTextExtent(str);
  CPoint center;
  center.x = TextSize.cx / 2;
  center.y = TextSize.cy / 2;

  // now calculate the center of the rotated text
  CPoint rcenter;
  rcenter.x = long(cos(radian) * center.x - sin(radian) * center.y);
  rcenter.y = long(sin(radian) * center.x + cos(radian) * center.y);

  // finally draw the text and move it to the center of the rectangle
  pDC->SetTextAlign(TA_BASELINE);
  pDC->SetBkMode(TRANSPARENT);
  pDC->ExtTextOut(rect.left + rect.Width() / 2 - rcenter.x,
                  rect.top + rect.Height() / 2 + rcenter.y,
                  nOptions, rect, str, nullptr);
}

void CAi_TabCtrl::DrawItem_(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
  CRect rect = lpDrawItemStruct->rcItem;
  int nTabIndex = lpDrawItemStruct->itemID;
  if (nTabIndex < 0) return;
  BOOL bSelected = (nTabIndex == GetCurSel());

  wchar_t label[64] = {0};
  TC_ITEM tci;
  tci.mask = TCIF_TEXT | TCIF_IMAGE;
  tci.pszText = label;
  tci.cchTextMax = 63;
  if (!GetItem(nTabIndex, &tci))
    return;

  CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
  if (!pDC)
    return;
  int nSavedDC = pDC->SaveDC();

  // For some bizarre reason the rcItem you get extends above the actual
  // drawing area. We have to workaround this "feature".
  rect.top += ::GetSystemMetrics(SM_CYEDGE);

  pDC->SetBkMode(TRANSPARENT);
  pDC->FillSolidRect(rect, ::GetSysColor(COLOR_BTNFACE));

  // Draw image
  CImageList* pImageList = GetImageList();
  if (pImageList && tci.iImage >= 0)
  {

    rect.left += pDC->GetTextExtent(_T("W")).cx;		// Margin
    // Get height of image so we 
    IMAGEINFO info;
    pImageList->GetImageInfo(tci.iImage, &info);
    CRect ImageRect(info.rcImage);
    int nYpos = rect.top;

    pImageList->Draw(pDC, tci.iImage, CPoint(rect.left, nYpos), ILD_TRANSPARENT);
    rect.left += ImageRect.Width();
  }

  CRect rectClient = lpDrawItemStruct->rcItem;
  CRect rectText = rectClient;
  int x = rectText.left;// +6;
  int textHeight = pDC->GetTextExtent(_T("W")).cy;
  int y = rectText.top + (rectText.Height() - textHeight) / 2 + 10;
  rectText.bottom = rectText.top + 20;// (rectText.Height() - textHeight) / 2;

  if (bSelected)
  {
    COLOR16 nR_vrtx1 = 0xEC00;
    COLOR16 nG_vrtx1 = 0xE200;
    COLOR16 nB_vrtx1 = 0x5900;

    COLOR16 nR_vrtx2 = 0xFF00;
    COLOR16 nG_vrtx2 = 0xFF00;
    COLOR16 nB_vrtx2 = 0xFC00;

    TRIVERTEX vertex[2] = {
      {rect.left, rect.top, nR_vrtx1, nG_vrtx1, nB_vrtx1, 0x0000},
      {rect.right, rect.bottom, nR_vrtx2, nG_vrtx2, nB_vrtx2, 0x0000}
    };
    GRADIENT_RECT grRect = {0, 1};
    pDC->GradientFill(vertex, 2, &grRect, 1, GRADIENT_FILL_RECT_H);

    pDC->SetTextColor(m_crSelColour);
    pDC->SelectObject(&m_SelFont);
    pDC->DrawText(label, rect, DT_SINGLELINE | DT_CENTER);
  }
  else
  {
    pDC->SetTextColor(m_crUnselColour);
    pDC->SelectObject(&m_UnselFont);
    pDC->DrawText(label, rect, DT_SINGLELINE | DT_CENTER);
  }

  pDC->RestoreDC(nSavedDC);
}

void CAi_TabCtrl::SetColours(COLORREF bSelColour, COLORREF bUnselColour)
{
  m_crSelColour = bSelColour;
  m_crUnselColour = bUnselColour;
  Invalidate();
}

void CAi_TabCtrl::SetFonts(CFont* pSelFont, CFont* pUnselFont)
{
  ASSERT(pSelFont && pUnselFont);

  LOGFONT lFont;
  int nSelHeight, nUnselHeight;

  m_SelFont.DeleteObject();
  m_UnselFont.DeleteObject();

  pSelFont->GetLogFont(&lFont);
  m_SelFont.CreateFontIndirect(&lFont);
  nSelHeight = lFont.lfHeight;

  pUnselFont->GetLogFont(&lFont);
  m_UnselFont.CreateFontIndirect(&lFont);
  nUnselHeight = lFont.lfHeight;

  SetFont((nSelHeight > nUnselHeight) ? &m_SelFont : &m_UnselFont);
}

void CAi_TabCtrl::SetFonts(int nSelWeight, BOOL bSelItalic, BOOL bSelUnderline, int nUnselWeight, BOOL bUnselItalic, BOOL bUnselUnderline)
{
  // Free any memory currently used by the fonts.
  m_SelFont.DeleteObject();
  m_UnselFont.DeleteObject();

  // Get the current font
  LOGFONT lFont;
  CFont *pFont = GetFont();
  if (pFont)
    pFont->GetLogFont(&lFont);
  else
  {
    NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(NONCLIENTMETRICS);
    VERIFY(SystemParametersInfo(SPI_GETNONCLIENTMETRICS,
      sizeof(NONCLIENTMETRICS), &ncm, 0));
    lFont = ncm.lfMessageFont;
  }

  // Create the "Selected" font
  lFont.lfWeight = nSelWeight;
  lFont.lfItalic = bSelItalic;
  lFont.lfUnderline = bSelUnderline;
  lFont.lfEscapement = 0; // Rotate it 90 degrees
  lFont.lfOrientation = 0;// -See more at : http ://msgroups.net/vc.mfc/rotating-text-in-a-cdc/554388#sthash.nojPei0W.dpuf
  m_SelFont.CreateFontIndirect(&lFont);

  // Create the "Unselected" font
  lFont.lfWeight = nUnselWeight;
  lFont.lfItalic = bUnselItalic;
  lFont.lfUnderline = bUnselUnderline;
  m_UnselFont.CreateFontIndirect(&lFont);

  SetFont((nSelWeight > nUnselWeight) ? &m_SelFont : &m_UnselFont);
}

void CAi_TabCtrl::OnTcnSelchanging(NMHDR *pNMHDR, LRESULT *pResult)
{
  /*int iTab = GetCurSel();
  TC_ITEM tci{};
  tci.mask = TCIF_PARAM;
  GetItem(iTab, &tci);
  CWnd* pWnd = (CWnd *)tci.lParam;
  pWnd->ShowWindow(SW_HIDE);*/

  int nCurSel = GetCurSel();
  if (nCurSel != -1)
  {
    CString sPageName;

    if (nCurSel < (int)m_Pages.size())
    {
      map<CDialog*, CString>::iterator iter = m_Pages.begin();
      for (size_t i = 0; i < m_Pages.size(); ++i, ++iter)
      {
        if (i == nCurSel)
        {
          iter->first->ShowWindow(SW_HIDE);
          sPageName = iter->second;
          break;
        }
      }
    }
    OnSelchanging(nCurSel, sPageName);
  }
}

void CAi_TabCtrl::OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult)
{
  /*int iTab = GetCurSel();
  TC_ITEM tci{};
  tci.mask = TCIF_PARAM;
  GetItem(iTab, &tci);
  CWnd* pWnd = (CWnd *)tci.lParam;
  pWnd->ShowWindow(SW_SHOW);
  pWnd->SetFocus();
  pWnd->EnableWindow(TRUE);
  //OnSelchanged(iTab);*/

  int nCurSel = GetCurSel();
  if (nCurSel != -1)
  {
    CString sPageName;

    if (nCurSel < (int)m_Pages.size())
    {
      map<CDialog*, CString>::iterator iter = m_Pages.begin();
      for (size_t i = 0; i < m_Pages.size(); ++i, ++iter)
      {
        if (i == nCurSel)
        {
          iter->first->ShowWindow(SW_SHOW);
          iter->first->EnableWindow(TRUE);
          iter->first->SetFocus();
          sPageName = iter->second;
          break;
        }
      }
    }
    OnSelchanged(nCurSel, sPageName);
  }
  *pResult = 0;
}

void CAi_TabCtrl::AddPage(CDialog* pPage, CString sPageName)
{
  m_Pages.insert(pair<CDialog*, CString>(pPage, sPageName));
}

void CAi_TabCtrl::SelectItem(CString sPageName)
{
  int nItemCount = GetItemCount();
  for (int i = 0; i < nItemCount; ++i)
  {
    /*TCITEM itm{};
    itm.mask = TCIF_TEXT;
    GetItem(i, &itm);
    CString sPageName_i(itm.pszText);*/
    map<CDialog*, CString>::iterator iter = m_Pages.begin();
    for (size_t i = 0; i < m_Pages.size(); ++i, ++iter)
    {
      if (iter->second == sPageName)
      {
        SetCurSel(i);

        iter->first->ShowWindow(SW_SHOW);
        iter->first->EnableWindow(TRUE);
        iter->first->SetFocus();
        break;
      }
    }
  }
}

