#include "stdafx.h"
#include "Ai_Font.h"

CAi_Font::CAi_Font()
{
}

CAi_Font::~CAi_Font()
{
}

bool CAi_Font::CreateFont_(
  CWnd* pCtrl, // ��������� �� Ctrl �� �������� ����� Font, ��� ������� - this
  CFont& fntDst, // ��� ���� �������
  CString sFaceName, // ���� ����� �� ����� ��� � Ctrl
  int cEscapement, // �������
  BYTE iQuality,
  int nHeight, // ���� MAXINT, �� ������� ������� ��� � Ctrl
  bool bItalic
  )
{
  ASSERT(pCtrl);
  if (!pCtrl)
    return false;

  CFont* pFont = pCtrl->GetFont();
  ASSERT(pFont);
  if (!pFont)
    return false;

  LOGFONT lf = {0};
  pFont->GetLogFont(&lf);
  
  int nFontH = (nHeight != MAXINT) ? nHeight : lf.lfHeight;

  CString sFace = sFaceName.IsEmpty() ? CString(lf.lfFaceName) : sFaceName;
  
  fntDst.CreateFont(nFontH, 0,
                          0, 0,
                          cEscapement,
                          bItalic ? TRUE : FALSE, // Italic
                          FALSE, // Undeline
                          FALSE,
                          lf.lfCharSet,
                          OUT_CHARACTER_PRECIS,
                          CLIP_CHARACTER_PRECIS,
                          iQuality,
                          DEFAULT_PITCH | FF_DONTCARE,
                          sFace);

  return true;
}
