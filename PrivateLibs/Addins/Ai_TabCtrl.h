#pragma once

#include "AddinsDef.h"
#include <map>

class ADDINS_API CAi_TabCtrl: public CTabCtrl
{
  DECLARE_DYNAMIC(CAi_TabCtrl)

public:
  CAi_TabCtrl();
  virtual ~CAi_TabCtrl();

protected:
  DECLARE_MESSAGE_MAP()

private:
  UINT m_nIDD = 0;
  COLORREF m_crSelColour = RGB(0, 0, 0);
  COLORREF m_crUnselColour = RGB(0, 0, 0);
  CFont  m_SelFont;
  CFont  m_UnselFont;

public:
  void SetFonts(CFont* pSelFont, CFont* pUnselFont);
  void SetFonts(int nSelWeight = FW_SEMIBOLD, BOOL bSelItalic = FALSE, BOOL bSelUnderline = FALSE,
                int nUnselWeight = FW_MEDIUM, BOOL bUnselItalic = FALSE, BOOL bUnselUnderline = FALSE);

  void SetColours(COLORREF bSelColour, COLORREF bUnselColour);

  // Select pages

private:
  void OnTcnSelchanging(NMHDR *pNMHDR, LRESULT *pResult);
  void OnTcnSelchange(NMHDR *pNMHDR, LRESULT *pResult);

  std::map<CDialog*, CString> m_Pages;
  //int m_nPrevSel = 0;

protected:
  void DrawItem_(LPDRAWITEMSTRUCT lpDrawItemStruct); // draw vertical ctrl for CAi_VertTabCtrl

public:
  virtual void OnSelchanging(int nSelectedItem, CString sPageName) { }
  virtual void OnSelchanged(int nSelectedItem, CString sPageName) { }
  void AddPage(CDialog* pPage, CString sPageName);
  void SelectItem(CString sPageName);
};

class ADDINS_API CAi_VertTabCtrl: public CAi_TabCtrl
{
public:
  CAi_VertTabCtrl() { }
  ~CAi_VertTabCtrl() { }

private:
  virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
    DrawItem_(lpDrawItemStruct);
  }
};


