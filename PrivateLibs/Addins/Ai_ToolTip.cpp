#include "stdafx.h"
#include "afxtoolbarimages.h"
#include "Ai_ToolTip.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_ToolTip::CAi_ToolTip()
  : m_nCurrID(0)
{
}

CAi_ToolTip::~CAi_ToolTip()
{
}

BEGIN_MESSAGE_MAP(CAi_ToolTip, CMFCToolTipCtrl)
  ON_NOTIFY_REFLECT(TTN_SHOW, OnShow)
END_MESSAGE_MAP()

void CAi_ToolTip::OnShow(NMHDR* pNMHDR, LRESULT* pResult)
{
  if (m_toolTCtrlsInfo.size() == 0)
    return;
  m_nCurrID = CWnd::FromHandle ((HWND)pNMHDR->idFrom)->GetDlgCtrlID ();
  m_iter = m_toolTCtrlsInfo.find(m_nCurrID);
  if (m_iter == m_toolTCtrlsInfo.end())
    return;
  SetDescription (m_iter->second.strDescription);
  CMFCToolTipCtrl::OnShow (pNMHDR, pResult);
}

BOOL CAi_ToolTip::OnDrawIcon (CDC* pDC, CRect rectImage)
{
  if (m_toolTCtrlsInfo.size() == 0)
    return FALSE;

  UINT uiBmpResID = m_iter->second.nBmpResID;
  if (uiBmpResID == 0)
    return FALSE;

  CMFCToolBarImages image;
  image.Load (uiBmpResID);
  image.SetSingleImage ();
  image.DrawEx (pDC, rectImage, 0);

  return TRUE;
}
