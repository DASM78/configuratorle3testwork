#include "StdAfx.h"
#include "Aclapi.h"
#include <lmaccess.h>
#include <stdlib.h>

#ifdef SWITCH_ON_FILE_VERSION_CODE
#include "Winver.h"
// add in project properties to 'Propertis'->'Linker'->'Input'->'Additional Dependencies' this string: Version.lib
#endif

#include "Ai_File.h"
#include "Resource.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString CAi_File::m_strPermissionErrorInfo;

CAi_File::CAi_File()
  : m_pstrSectionData(nullptr)
  , m_pInner(new CFileCtrlInnerUsing())
{
}

CAi_File::~CAi_File()
{
  delete m_pInner;
  m_pInner = nullptr;
}

bool CAi_File::IfPathHasNotBadSimbols(CString strPath)
{
  int nPos = strPath.Find(L':');
  if (nPos != -1)
    if (nPos != 1)
      return false;

  if (strPath.Find(L'*') != -1
      || strPath.Find(L'?') != -1
      || strPath.Find(L'<') != -1
      || strPath.Find(L'>') != -1
      || strPath.Find(L'|') != -1)
      return false;
  return true;
}

bool CAi_File::IfFolderNameHasNotBadSimbols(CString strFolderName)
{
  return IfFileNameHasNotBadSimbols(strFolderName);
}

bool CAi_File::IfFileNameHasNotBadSimbols(CString strFileName)
{
  if (strFileName.Find(L'\\') != -1
      || strFileName.Find(L'/') != -1
      || strFileName.Find(L':') != -1
      || strFileName.Find(L'*') != -1
      || strFileName.Find(L'?') != -1
      || strFileName.Find(L'\"') != -1
      || strFileName.Find(L'<') != -1
      || strFileName.Find(L'>') != -1
      || strFileName.Find(L'|') != -1)
      return false;
  return true;
}

//***************************************** Win Folders

CString CAi_File::GetWinTmpPath()
{
  TCHAR szTempFolder[_MAX_PATH + 1] = {0};
  if (GetTempPath(_MAX_PATH, szTempFolder) > 0)
  {
    TCHAR szTempFolderL[_MAX_PATH + 1] = {0};
    if (GetLongPathName(szTempFolder, szTempFolderL, _MAX_PATH) > 0)
      return CString(szTempFolderL);
  }
  _ASSERTE(!_T("CAi_File::GetWinTmpPath"));
  return _T("");
}

CString CAi_File::GetWinLocalSettingsApplicationDataPath()
{
  CString strRes;
  IMalloc * pShellMalloc = nullptr; // A pointer to the shell's IMalloc interface

  HRESULT hRes = SHGetMalloc(&pShellMalloc);
  if (FAILED(hRes))
      return strRes;

  LPITEMIDLIST pidlItem = nullptr; // The item's PIDL.

  hRes = SHGetSpecialFolderLocation(nullptr, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, &pidlItem);
  if (hRes == NOERROR)
  {
    TCHAR szPath[MAX_PATH + 1] = {0};
    if (SHGetPathFromIDList(pidlItem, szPath))
      strRes = CString(szPath);
  }
  else
  _ASSERTE(!_T("CAi_File::GetWinLocalSettingsApplicationDataPath"));

  // Clean up allocated memory
  if (pidlItem)
    pShellMalloc->Free(pidlItem);
  pShellMalloc->Release();

  return strRes;
}

//***************************************** Folders

CString CAi_File::GetCurrentPath()
{
  TCHAR szPath[_MAX_PATH + 1] = {0};
  if (GetCurrentDirectory(_MAX_PATH, szPath))
    return CString(szPath);
  _ASSERTE(!_T("CAi_File::GetCurrentPath"));
  return _T("");
}

CString CAi_File::GetAppPath()
{
  CString strPath = CAi_File::GetAppPathName();
  return strPath.Mid(0, CAi_File::GetAppPathName().ReverseFind(_T('\\')));
}

CString CAi_File::GetAppPathName(HMODULE hModule)
{
  TCHAR szPath[_MAX_PATH + 1] = {0};
  if (GetModuleFileName(hModule, szPath, _MAX_PATH))
    return CString(szPath);
  _ASSERTE(!_T("CAi_File::GetAppPathName"));
  return _T("");
}

//***************************************** File parh parsers

CString CAi_File::GetFileName_(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetFileName_"));
    return _T("");
  }
  CAi_File f;
  return f.m_pInner->SplitPath(strFilePathName, CFileCtrlInnerUsing::spFileName);
  /*int nIdx = strFilePathName.ReverseFind(_T('\\'));
  if (nIdx == -1)
    return strFilePathName;
  return strFilePathName.Mid(nIdx + 1, strFilePathName.GetLength() - nIdx);*/
}

CString CAi_File::GetFileTitle_(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetFileTitle_"));
    return _T("");
  }
  int nIdx = strFilePathName.ReverseFind(_T('\\'));
  if (nIdx == -1)
    return strFilePathName;
  strFilePathName = strFilePathName.Mid(nIdx + 1, strFilePathName.GetLength() - nIdx);
  nIdx = strFilePathName.ReverseFind(_T('.'));
  if (nIdx == -1)
    return strFilePathName;
  return strFilePathName.Mid(0, nIdx);
}

CString CAi_File::GetFileExt(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetFileExt"));
    return _T("");
  }
  int nIdx = strFilePathName.ReverseFind(_T('.'));
  if (nIdx == -1)
    return strFilePathName;
  return strFilePathName.Mid(nIdx + 1, strFilePathName.GetLength() - nIdx);
}

CString CAi_File::GetFilePath_(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetFilePath_"));
    return _T("");
  }
  if (strFilePathName.Find(_T("\\")) == -1)
    return strFilePathName;
  return strFilePathName.Mid(0, strFilePathName.ReverseFind(_T('\\')));
}

CString CAi_File::GetFileFolder(CString strFilePathName)
{
  CString strPath = CAi_File::GetFilePath_(strFilePathName);
  if (!strPath.IsEmpty())
  {
    int nIdx = strPath.ReverseFind(_T('\\'));
    if (nIdx == -1)
      return strFilePathName;
    return strPath.Mid(nIdx + 1, strPath.GetLength() - 1);
  }
  _ASSERTE(!_T("CAi_File::GetFileFolder"));
  return _T("");
}

//***************************************** Create or open a file

bool CAi_File::CreateFile_(CString strFilePathName, CreateFileMode howCreate)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CreateFile_"));
    return false;
  }

  bool bFileIsExists = CAi_File::IsFileExist(strFilePathName);
  if (bFileIsExists && howCreate == cfmClearIfExists)
    return CAi_File::ClearFile(strFilePathName);
  if (bFileIsExists && howCreate == cfmJustOpenIfExists)
    return true;
  if (bFileIsExists && howCreate == cfmErrorIfExists)
  {
    AfxMessageBox(_T("File already exists!"), MB_ICONERROR);
    return false;
  }

  CFile file;
  if (!file.Open(strFilePathName, CFile::modeCreate))
  {
    _ASSERTE(!_T("CAi_File::CreateFile_(3)"));
    return false;
  }
  file.Close();
  return true;
}

CString CAi_File::CreateTempFile(CString strFileName, CString* pstrWriteToFile)
{
  if (strFileName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CreateTempFile"));
    return _T("");
  }

  strFileName = GetWinTmpPath() + strFileName;
  if (!CAi_File::CreateFile_(strFileName)) // create temp file
  {
    _ASSERTE(!_T("CAi_File::CreateTempFile(2)"));
    return _T("");
  }

  if (pstrWriteToFile && !pstrWriteToFile->IsEmpty())
    CAi_File::WriteStringToFile(strFileName,  pstrWriteToFile);
  return strFileName;
}

//***************************************** Create a new file if it is absented

bool CAi_File::ExamineFile(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::ExamineFile"));
    return false;
  }
  if (!CAi_File::IsFileExist(strFilePathName))
    if (!CreateFile_(strFilePathName, cfmJustOpenIfExists)) // examine create file
    {
      _ASSERTE(!_T("CAi_File::ExamineFile(2)"));
      return false;
    }
  return true;
}

CString CAi_File::ExamineTmpFile(CString strFileName)
{
  strFileName = GetWinTmpPath() + _T("\\") + strFileName;
  if (CAi_File::ExamineFile(strFileName))
    return strFileName;
  _ASSERTE(!_T("CAi_File::ExamineTmpFile"));
  return _T("");
}

//***************************************** A file extension

// Example:
//    - strFileName = "....\aaa" or "....\aaa." and strExt = ".txt" or "txt" => "...\aaa.txt" 
//    - strFileName = "....\aaa.txt" and strExt = ".txt" or "txt" => "...\aaa.txt" 
//    - strFileName = "....\aaa..." and strExt = ".txt" or "txt" => "...\aaa.txt" 
//    - for result as  "....\aaa...txt" put strExt as "...txt" => "...\aaa...txt" 
void CAi_File::AddExtensionIfNeed(CString* pstrFileName, CString strExt)
{
  ASSERT(pstrFileName);
  if (!pstrFileName || pstrFileName->IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::AddExtensionIfNeed"));
    return;
  }

  pstrFileName->TrimRight(_T("."));
  if(strExt.Left(1) != _T("."))
    strExt = _T(".") + strExt;

  if (pstrFileName->GetLength() <= strExt.GetLength())
    *pstrFileName += strExt;
  else
  if (pstrFileName->Right(strExt.GetLength()) != strExt)
    *pstrFileName += strExt;
}

//***************************************** Delete or clear a file

bool CAi_File::DeleteFile_(CString strFilePathName)
{
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    //_ASSERTE(!_T("CAi_File::DeleteFile_"));
    return false;
  }
  CAi_File::RemoveAttribute_ReadOnly(strFilePathName);
  if (DeleteFile(strFilePathName))
    return true;
  else
  {
    _ASSERTE(!_T("CAi_File::DeleteFile_(2)"));
    return false;
  }
}

bool CAi_File::DeleteFiles(CString strPathName)
{
  CAi_File f;
  if (f.GetListOfFilesInFolder(strPathName))
  {
    if (f.m_listOfFolderAndFiles.size() == 1)
      for (int i = 0; i < (int)f.m_listOfFolderAndFiles[0].files.size(); ++i)
        DeleteFile(f.m_listOfFolderAndFiles[0].files[i].strFilePathName);
    return true;
  }
  _ASSERTE(!_T("CAi_File::DeleteFiles"));
  return false;
}

bool CAi_File::ClearFile(CString strFilePathName)
{
  CFile file;
  if (strFilePathName.IsEmpty()
    || !file.Open(strFilePathName, CFile::modeWrite))
    {
      _ASSERTE(!_T("CAi_File::ClearFile"));
      return false;
    }
  file.SetLength(0);
  file.Flush();
  file.Close();
  return true;
}

//***************************************** Create a folder

bool CAi_File::CreateFolder(CString strFolderPath, bool bShowErrorMsg)
{
  if (strFolderPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CreateFolder"));
    return false;
  }
  if (CAi_File::IsFolderExist(strFolderPath))
  {
    _ASSERTE(!_T("CAi_File::CreateFolder(2)"));
    return false;
  }

  CString strOrigFolderPath = strFolderPath;
  CString strDisk;
  strFolderPath.Replace(_T("/"), _T("\\"));
  if (strFolderPath.Find(_T(":")) == 1)
  {
    strDisk = strFolderPath;
    strDisk.GetBufferSetLength(2);
    strFolderPath.Delete(0, strFolderPath.Find(_T("\\")) + 1);
  }

  vector<CString> folders;
  int nCurPos = 0;
  CString strResToken = strFolderPath.Tokenize(_T("\\"), nCurPos);
  while (strResToken != _T(""))
  {
    strResToken.TrimLeft(_T(" "));
    strResToken.TrimRight(_T(" "));
    folders.push_back(strResToken);
    strResToken = strFolderPath.Tokenize(_T("\\"), nCurPos);
  }

  CString strTmpFolder = _T("tmpfolterfortestcai_file");
  if (folders.size() == 0 // ���� ����� ���
    && !strDisk.IsEmpty()) // � ���� ����
  { // ��������, ��� ���� ���������
    CString strTmpPath = strDisk + _T("\\") + strTmpFolder;
    if (!CreateFolder(strTmpPath))
      return false;
    DeleteFolder(strTmpPath, false);
  }

  CString strDir = strDisk.IsEmpty() ? _T(".") : strDisk;
  for (int i = 0; i < (int)folders.size(); ++i)
  {
    strDir += _T("\\");
    strDir += folders[i];
    if (CAi_File::IsFolderExist(strDir))
      continue;
    if (!CreateDirectory(strDir, nullptr))
    {
      if (folders[i] == strTmpFolder)
        return false;
      if (bShowErrorMsg)
      {
        CString strErr;
        strErr.Format(IDS_ERROR_CREATING_FOLDER, strOrigFolderPath); // ������ ��� �������� �����:\n%s!
        AfxMessageBox(strErr, MB_ICONERROR);
      }
      _ASSERTE(!_T("CAi_File::CreateFolder(3)"));
      return false;
    }
  }
  return true;
}

bool CAi_File::ExamineFolder(CString strFolderPath)
{
  if (!CAi_File::IsFolderExist(strFolderPath))
    if (!CAi_File::CreateFolder(strFolderPath))
    {
      _ASSERTE(!_T("CAi_File::ExamineFolder"));
      return false;
    }
  return true;
}

bool CAi_File::ChoiseFolderDlg(CString* pstrFolderPath, CString strCaption, CShellManager* pShM, CWnd* pWndParent)
{
  _ASSERT(pstrFolderPath);
  _ASSERT(pWndParent);
  return pShM->BrowseForFolder(*pstrFolderPath, pWndParent, *pstrFolderPath, strCaption, BIF_USENEWUI) ? true : false;
}

//***************************************** Delete a folder and subfolders (and files inside)

bool CAi_File::DeleteFolder(CString strFolderPath, bool bNoIfFilesAreInside)
{
  if (strFolderPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::DeleteFolder"));
    return false;
  }
  if (!CAi_File::IsFolderExist(strFolderPath))
  {
    _ASSERTE(!_T("CAi_File::DeleteFolder(2)"));
    return false;
  }

  CAi_File f;
  if (!f.GetListOfFilesInFolder(strFolderPath, true))
  {
    _ASSERTE(!_T("CAi_File::DeleteFolder(3)"));
    return false;
  }

  bool bRes = true;
  if (f.m_listOfFolderAndFiles.size() > 0)
    for (int i = (int)f.m_listOfFolderAndFiles.size() - 1; i >= 0; i--)
    {
      int nCntFiles = f.m_listOfFolderAndFiles[i].files.size();
      if (bNoIfFilesAreInside && nCntFiles > 0)
        continue;
      for (int j = 0; j < nCntFiles; ++j)
        if (!CAi_File::DeleteFile_(f.m_listOfFolderAndFiles[i].files[j].strFilePathName))
        {
          _ASSERTE(!_T("CAi_File::DeleteFolder(4)"));
          bRes = false;
        }
      if (!RemoveDirectory(f.m_listOfFolderAndFiles[i].strPath))
      {
        //_ASSERTE(!_T("CAi_File::DeleteFolder(5)"));
        bRes = false;
      }
    }
  else
  if (!RemoveDirectory(strFolderPath))
  {
    _ASSERTE(!_T("CAi_File::DeleteFolder(6)"));
    bRes = false;
  }
  return bRes;
}

//***************************************** Detect if the folder is

bool CAi_File::IsFolderExist(CString strFolderPath)
{
  if (strFolderPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::IsFolderExist"));
    return false;
  }
  DWORD attr = GetFileAttributes(strFolderPath);
  if(attr == INVALID_FILE_ATTRIBUTES)
    return false;
  if (attr & FILE_ATTRIBUTE_DIRECTORY)
    return true;
  return false;
}

//***************************************** Detect if the file is

bool CAi_File::IsFileExist(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("The file name is empty!"));
    return false;
  }
  DWORD attr = GetFileAttributes(strFilePathName);
  if (attr == INVALID_FILE_ATTRIBUTES)
    return false;
  if (attr & FILE_ATTRIBUTE_DIRECTORY)
    return false;
  return true;
}

//***************************************** File length

bool CAi_File::IsFileEmpty(CString strFilePathName, long* nFileLength)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::IsFileEmpty"));
    return false;
  }
  int nFileLen = CAi_File::GetFileLength(strFilePathName);
  if (nFileLen == -1)
   return true; // !
  if (nFileLength != nullptr)
    *nFileLength = nFileLen;
  if (nFileLen == 0)
    return true;
  return false;
}

long CAi_File::GetFileLength(CString strFilePathName)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetFileLength"));
    return -1;
  }

  CFile file;
  if (!file.Open(strFilePathName, CFile::modeRead))
  {
    #ifdef _DEBUG
    _ASSERTE(!_T("CAi_File::GetFileLength(2)"));
    #endif
    return -1;
  }
  long nFileLen = (long)file.GetLength();
  file.Close();
  if (nFileLen == 2
    && CAi_File::IsUnicodeFile(strFilePathName)) // i.e.(MSDN): the text contains ONLY the Unicode byte-order mark (BOM) 0xFEFF as its first character
    return 0;
  return nFileLen;
}

//***************************************** Has file unicode coding

bool CAi_File::IsUnicodeFile(CString strFilePathName)
{
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::IsUnicodeFile"));
    return false;
  }
  
  CFile file;
  if (file.Open(strFilePathName, CFile::modeRead)) // dont use here function: CAi_File::IsFileEmpty(strFilePathName)
  {
    int nFileLen = (long)file.GetLength();
    file.Close();
    if (nFileLen == 0)
      return false;
  }
  else
  {
    _ASSERTE(!_T("CAi_File::IsUnicodeFile(2)"));
    return false;
  }

  BYTE* pBuff = nullptr;
  int nReadCount = CAi_File::ReadFileToBuffer(strFilePathName, &pBuff);
  if (nReadCount > 0)
  {
    BOOL bU = IsTextUnicode(pBuff, nReadCount, nullptr);
    delete pBuff;
    return bU ? true : false;
  }
  _ASSERTE(!_T("CAi_File::IsUnicodeFile(3)"));
  return false;
}

FileCodingFormat CAi_File::GetFileCodingFormat(CString strFilePathName)
{
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::GetFileCodingFormat"));
    return fcfUnicode;
  }
  return CAi_File::IsUnicodeFile(strFilePathName) ? fcfUnicode : fcfASCII;
}

//***************************************** Remove a "Read only" file attribute

bool CAi_File::RemoveAttribute_ReadOnly(CString strFilePathName, CString strWarningMessage)
{
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::RemoveAttribute_ReadOnly"));
    return false;
  }

  FILE_ATTR attr;
  if (!GetFileAttributes_(strFilePathName, &attr))
  {
    _ASSERTE(!_T("CAi_File::RemoveAttribute_ReadOnly(2)"));
    return false;
  }
  if ((attr.nAttributes & FILE_ATTRIBUTE_READONLY) == FILE_ATTRIBUTE_READONLY)
  {
    if (!strWarningMessage.IsEmpty())
      if (AfxMessageBox(strWarningMessage, MB_ICONWARNING | MB_YESNO) == IDNO)
        return false;
    if (SetFileAttributes(strFilePathName, 0) == 0)
    {
      _ASSERTE(!_T("CAi_File::RemoveAttribute_ReadOnly(3)"));
      return false;
    }
    return true;
  }
  else
    return true;
  _ASSERTE(!_T("CAi_File::RemoveAttribute_ReadOnly(4)"));
  return false;
}

//***************************************** Get the file attributes

bool CAi_File::GetFileAttributes_(CString strFilePathName, FILE_ATTR* pFileAttr)
{
  if (!IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::GetFileAttributes_"));
    return false;
  }
  ASSERT(pFileAttr);
  if (pFileAttr == nullptr)
  {
    _ASSERTE(!_T("CAi_File::GetFileAttributes_(2)"));
    return false;
  }

  pFileAttr->nAttributes = GetFileAttributes(strFilePathName);
  if (pFileAttr->nAttributes == INVALID_FILE_ATTRIBUTES)
  {
    _ASSERTE(!_T("CAi_File::GetFileAttributes_(3)"));
    return false;
  }

  BOOL bRes = FALSE;
  CFile file;
  if (file.Open(strFilePathName, CFile::modeRead))
  {
    bRes = GetFileInformationByHandle(file.m_hFile, &pFileAttr->fileInfo);
    if (!bRes)
    {
      _ASSERTE(!_T("CAi_File::GetFileAttributes_(4)"));
      return false;
    }
    file.Close();
  }
  return bRes ? true : false;
}

// bFolder - just path or path with file name in strFilePath
bool CAi_File::ExaminePermissions(CString strFilePath, bool bFolder, Permission per, bool bShowMsgByDenied)
{
  if (bFolder)
  { // just path
    if (!CAi_File::IsFolderExist(strFilePath))
    {
      _ASSERTE(!_T("CAi_File::ExaminePermissions"));
      return false;
    }
  }
  else
  { // file path name
    if (!CAi_File::IsFileExist(strFilePath))
    {
      _ASSERTE(!_T("CAi_File::ExaminePermissions(2)"));
      return false;
    }
  }

  PSID ppsidOwner;
  PSID ppsidGroup;
  PACL ppSacl = nullptr;
  PACL pOldDACL = nullptr;
  PSECURITY_DESCRIPTOR pSD = nullptr;
  BOOL bRet;
  SECURITY_INFORMATION si = OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION;

  DWORD dwRes = GetNamedSecurityInfo(strFilePath.GetBuffer(), SE_FILE_OBJECT, si, &ppsidOwner, &ppsidGroup, &pOldDACL, &ppSacl, &pSD);
  if (ERROR_SUCCESS != dwRes)
  {
    _ASSERTE(!_T("CAi_File::ExaminePermissions(3)"));
    return false;
  }
  bRet = ::IsValidSecurityDescriptor(pSD);
  if (FALSE == bRet)
  {
    _ASSERTE(!_T("CAi_File::ExaminePermissions(4)"));
    return false;
  }

  HANDLE hPrevToken;
  ImpersonateSelf(SecurityImpersonation);
  bRet = ::OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE, &hPrevToken);
  if (FALSE == bRet)
  {
    _ASSERTE(!_T("CAi_File::ExaminePermissions(5)"));
    return false;
  }

  BOOL bStatus;
  GENERIC_MAPPING genMapping;
  PRIVILEGE_SET   pset;
  DWORD  dwStructSize = sizeof(PRIVILEGE_SET);
  DWORD dwGrantedAccess;

  genMapping.GenericRead    = ACCESS_READ;
  genMapping.GenericWrite   = ACCESS_WRITE;
  genMapping.GenericExecute = 0;
  genMapping.GenericAll = ACCESS_READ | ACCESS_WRITE;

  DWORD nLookPer = FILE_READ_EA;
  int nR = 1;
  if (per == pRead)
    nLookPer = FILE_READ_EA;
  else
  if (per == pWrite)
    nLookPer = FILE_WRITE_EA;
  else
    nR = 2;

  bool bR = true;
  bool bW = true;

  for (int i = 0; i < nR; ++i)
  {
    if (nR == 2 && i == 1)
      nLookPer = FILE_WRITE_EA;

    bRet = ::AccessCheck(pSD,
               hPrevToken,
               nLookPer,
               &genMapping,
               &pset,
               &dwStructSize,
               &dwGrantedAccess,
               &bStatus);
    if (FALSE == bRet)
    {
      _ASSERTE(!_T("CAi_File::ExaminePermissions(6)"));
      CloseHandle(hPrevToken);
      return false;
    }

    if (per == pRead)
      bR = bStatus ? true : false;
    else
    if (per == pWrite)
      bW = bStatus ? true : false;
    else
    {
      if (i == 0)
        bR = bStatus ? true : false;
      else
        bW = bStatus ? true : false;
    }
  }

  bool bRes = (per == pRead) ? bR : 
                   (
                    (per == pWrite) ? bW : 
                      (
                        (bR && bW) ? true : false
                       )
                    );
  
  if (!bRes)
  {
    CString strMsg;
    CString str2;
    if (!bR && bW)
    {
      str2.LoadString(bFolder ? IDS_SELECTED_FOLDER : IDS_SELECTED_FILE); // ��������� �����! : ���������� �����!
      strMsg.Format(IDS_NO_RIGHTS_READ, str2, strFilePath); // �� �� ������ ���� �� ������ %s:\r\n\r\n%s
    }
    else
    if (bR && !bW)
    {
      str2.LoadString(bFolder ? IDS_TO_SELECTED_FOLDER : IDS_TO_SELECTED_FILE); // � ��������� �����! : � ��������� ����!
      strMsg.Format(IDS_NO_RIGHTS_WRITE, str2, strFilePath); // �� �� ������ ���� �� ������ %s:\r\n\r\n%s
    }
    else
    {
      str2.LoadString(bFolder ? IDS_IN_SELECTED_FOLDER : IDS_IN_SELECTED_FILE); // � ��������� �����! : � ��������� ����!
      strMsg.Format(IDS_NO_RIGHTS_READWRITE, str2, strFilePath); // �� �� ������ ���� �� ������ � ������ %s:\r\n\r\n%s
    }

    if (bShowMsgByDenied)
      AfxMessageBox(strMsg, MB_ICONERROR);
    m_strPermissionErrorInfo = strMsg.Mid(0, strMsg.Find(':')) + _T("!");
    CloseHandle(hPrevToken);
    return false;
  }
  CloseHandle(hPrevToken);
  return true;
}

//***************************************** Add or (delete from) path to environment
  
bool CAi_File::AddPathToSystemEnvironment(CString strPath)
{
  CFileCtrlInnerUsing _m_innerCtrl;
  return _m_innerCtrl.PathInSystemEnvironment(strPath, true);
}

bool CAi_File::DeletePathFromSystemEnvironment(CString strPath)
{
  CFileCtrlInnerUsing _m_innerCtrl;
  return _m_innerCtrl.PathInSystemEnvironment(strPath, false);
}

//***************************************** Copy a file to a file

bool CAi_File::CopyFileToFile(CString strSrcFilePathName, CString strDstFilePathName)
{
  return CAi_File::CopyFileToFile(strSrcFilePathName, strDstFilePathName, _T(""));
}

bool CAi_File::CopyFileToFile(CString strSrcFilePathName, CString strDstFilePathName, CString strStopIfChars)
{
  if (strSrcFilePathName == strDstFilePathName)
    return true;
  if (strSrcFilePathName.IsEmpty() || strDstFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CopyFileToFile"));
    return false;
  }
  if (!IsFileExist(strSrcFilePathName) || !IsFileExist(strDstFilePathName))
  {
    _ASSERTE(!_T("CAi_File::CopyFileToFile(2)"));
    return false;
  }

  bool bRes = false;
  CFile fileDestinationFile;
  if (fileDestinationFile.Open(strDstFilePathName, CFile::modeWrite))
  {
    int nSeekIfStop = -1;

    fileDestinationFile.SetLength(0);

    if (!strStopIfChars.IsEmpty())
    {
#ifdef _UNICODE
      ASSERT(false);
#endif
      nSeekIfStop = GetSeekBefore(strSrcFilePathName, strStopIfChars);
    }
  
    CFile fileSourceFile;
    if (fileSourceFile.Open(strSrcFilePathName, CFile::modeRead))
    {
      BYTE *pbufWrite = nullptr;
      int nSourceFileLength = (int)fileSourceFile.GetLength();
      
      if (nSourceFileLength > 0)
      {
        pbufWrite = new BYTE[nSourceFileLength];
        ZeroMemory(pbufWrite, nSourceFileLength);
        fileSourceFile.Read(pbufWrite, nSourceFileLength);

        CFileCtrlInnerUsing _m_innerCtrl;
        if (nSeekIfStop == -1)
          _m_innerCtrl.WriteBufToF(&fileDestinationFile, pbufWrite, nSourceFileLength);
        else
          _m_innerCtrl.WriteBufToF(&fileDestinationFile, pbufWrite, nSeekIfStop);

        fileDestinationFile.SeekToBegin();

        delete pbufWrite;
        pbufWrite = nullptr;
        fileSourceFile.Close();
      }

      bRes = true;
    }
    else
      _ASSERTE(!_T("CAi_File::CopyFileToFile(3)"));
    fileDestinationFile.Flush();
    fileDestinationFile.Close();
  }
  else
  {
    _ASSERTE(!_T("CAi_File::CopyFileToFile(4)"));
    return false;
  }
  return bRes;
}

//***************************************** Copy a folder to a folder; previous dst folder will be deleted

bool CAi_File::CopyFolderToFolder(CString strSrcPath, CString strDstPath, HWND hwndForMsg, CString strTitleProgressDlg)
{
  if (strSrcPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CopyFolderToFolder"));
    return false;
  }
  if (strDstPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::CopyFolderToFolder(2)"));
    return false;
  }
  strSrcPath += _T("\\*");
  strDstPath += _T("\\");

  TCHAR szSrc[MAX_PATH + 1] = {0};
  TCHAR szDst[MAX_PATH + 1] = {0};
  #ifdef _UNICODE
  wcscpy_s(szSrc, strSrcPath.GetLength() + 1, strSrcPath.GetBuffer()); 
  wcscpy_s(szDst, strDstPath.GetLength() + 1, strDstPath.GetBuffer()); 
  #else
  strcpy_s(szSrc, strSrcPath.GetLength() + 1, strSrcPath.GetBuffer()); 
  strcpy_s(szDst, strDstPath.GetLength() + 1, strDstPath.GetBuffer()); 
   #endif
  
  SHFILEOPSTRUCT f;
  f.hwnd = hwndForMsg;
  f.wFunc = FO_COPY;
  f.pFrom = szSrc;
  f.pTo = szDst;
  f.fFlags = FOF_NOCONFIRMATION | FOF_NOCONFIRMMKDIR | FOF_SILENT;
  if (hwndForMsg != nullptr)
    f.fFlags |= FOF_SIMPLEPROGRESS;
  f.lpszProgressTitle = strTitleProgressDlg;
  if (SHFileOperation(&f) == 0)
    return true;
  _ASSERTE(!_T("CAi_File::CopyFolderToFolder(3)"));
  return false;
}

//***************************************** Get all fles in folder

// In m_listOfFolderAndFiles will be full pathes to files (Ex.: C:\\Folder\Folder\\File.txt)
bool CAi_File::GetListOfFilesInFolder(CString strFolderPath, bool bLookSubFolders)
{
  if (strFolderPath.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::GetListOfFilesInFolder"));
    return false;
  }
  m_listOfFolderAndFiles.clear();
  CFileCtrlInnerUsing _m_innerCtrl;
  int nOrderIdx = 0;
  _m_innerCtrl.GetListOfFilesAndFolders(strFolderPath, &m_listOfFolderAndFiles, bLookSubFolders, nOrderIdx, -1, -1);
  return true;
}

//***************************************** Seeks in a file

int CAi_File::GetSeekBefore(CString strFilePathName, CString strChars)
{
  if (strFilePathName.IsEmpty() || strChars.IsEmpty())
 {
    _ASSERTE(!_T("CAi_File::GetSeekBefore"));
    return -1;
  }

  bool bUnicode = IsUnicodeFile(strFilePathName);;
  int nAddZero = bUnicode ? 3 : 1;

  CFile fileSourceFile;
  if (fileSourceFile.Open(strFilePathName, CFile::modeRead))
  {
    TCHAR *pbufWrite = nullptr;
    int nSourceFileLength = (int)fileSourceFile.GetLength();
    pbufWrite = new TCHAR[nSourceFileLength + nAddZero];
    ZeroMemory(pbufWrite, nSourceFileLength + nAddZero);

    fileSourceFile.Read(pbufWrite, nSourceFileLength);
    CString pBuf = CString(pbufWrite);
    
    delete pbufWrite;
    fileSourceFile.Close();
    
    int nSeek = pBuf.Find(strChars);
    if (nSeek > -1
      && bUnicode)
      ASSERT(false);
      //nSeek = nSeek * 2;
    return nSeek;
  }
  return -1;
}

int CAi_File::GetSeekAfter(CString strFilePathName, CString strChars)
{
  int nSeek = CAi_File::GetSeekBefore(strFilePathName, strChars);
  if (nSeek >= 0)
    return nSeek + strChars.GetLength();
  return -1;
}

//***************************************** Read file to

int CAi_File::ReadFileToBuffer(CString strFilePathName, BYTE** pBuffer, bool bZeroEnd)
{
  CFileCtrlInnerUsing _m_innerCtrl;
  return _m_innerCtrl.ReadFileToBuffer(strFilePathName, pBuffer, nullptr, bZeroEnd);
}

int CAi_File::ReadFileToString(CString strFilePathName, CString* pstrBuff)
{
  CFileCtrlInnerUsing _m_innerCtrl;
  return _m_innerCtrl.ReadFileToBuffer(strFilePathName, nullptr, pstrBuff);
}

//***************************************** Write to file

bool CAi_File::WriteBufferToFile(CString strFilePathName, BYTE* pBufferToFile, int nBuffSize, bool bClearFileBefore)
{
  if (pBufferToFile == nullptr)
  {
    _ASSERTE(!_T("CAi_File::WriteBufferToFile"));
    return false;
  }
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::WriteBufferToFile(2)"));
    return false;
  }
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::WriteBufferToFile(3)"));
    return false;
  }
  if (bClearFileBefore)
    CAi_File::ClearFile(strFilePathName);

  CFile file;
  if (!file.Open(strFilePathName, CFile::modeWrite))
  {
    _ASSERTE(!_T("CAi_File::WriteBufferToFile(4)"));
    return false;
  }
  file.SeekToEnd();
  CFileCtrlInnerUsing _m_innerCtrl;
  _m_innerCtrl.WriteBufToF(&file, pBufferToFile, nBuffSize);
  file.Flush();
  file.Close();
  return true;
}

bool CAi_File::WriteStringToFile(CString strFilePathName, CString* pstrToFile, FileCodingFormat dstCoding)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::WriteStringToFile"));
    return false;
  }
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::WriteStringToFile(2)"));
    return false;
  }
  if (pstrToFile == nullptr)
  {
    _ASSERTE(!_T("CAi_File::WriteStringToFile(3)"));
    return false;
  }
  if (pstrToFile->IsEmpty())
    return true;

  bool bAppU = false;
  #ifdef _UNICODE
  bAppU = true;
  #endif
  FileCodingFormat coding = (dstCoding == fcfByDef) ? (bAppU ? fcfUnicode : fcfASCII) : dstCoding;

  CFileCtrlInnerUsing _m_innerCtrl;

  _m_innerCtrl.m_bSetUnicodeByteOrderMark = false;
  if (CAi_File::GetFileLength(strFilePathName) > 0)
  {
    bool bDstFileU = CAi_File::IsUnicodeFile(strFilePathName);
    CString strMsg;

    switch (coding)
    {
      case fcfUnicode:
        if (!bDstFileU)
        {
          strMsg.Format(_T("Can not write characters of Unicode coding to the file which has ASCII coding\r\n(The file: %s)!"), strFilePathName);
          _ASSERTE(!_T("CAi_File::WriteStringToFile(4)"));
          AfxMessageBox(strMsg);
          return false;
        }
        break;
      case fcfASCII:
        if (bDstFileU)
        {
          strMsg.Format(_T("Can not write characters of ASCII coding to the file which has Unicode coding\r\n(The file: %s)!"), strFilePathName);
          _ASSERTE(!_T("CAi_File::WriteStringToFile(5)"));
          AfxMessageBox(strMsg);
          return false;
        }
        break;
    };
  }
  else
  {
    if (coding == fcfUnicode)
      _m_innerCtrl.m_bSetUnicodeByteOrderMark = true;
  }

  CFile file;
  if (!file.Open(strFilePathName, CFile::modeWrite))
  {
    _ASSERTE(!_T("CAi_File::WriteStringToFile(6)"));
    return false;
  }
  _m_innerCtrl.WriteBufToF(&file, pstrToFile, coding);
  file.Flush();
  file.Close();
  return true;
}

//***************************************** Add a new line in a file end/begin and a line feed ("\r\n") if need

bool CAi_File::AddLineToEnd(CString strFilePathName, CString strAdd, InsertNewLine newLine)
{
  if (strFilePathName.IsEmpty() || strAdd.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::AddLineToEnd"));
    return false;
  }
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::AddLineToEnd(2)"));
    return false;
  }

  FileCodingFormat dstCoding = CAi_File::GetFileCodingFormat(strFilePathName);

  CFile file;
  if (file.Open(strFilePathName, CFile::modeWrite))
  {
    file.SeekToEnd();
    CString strNL = CString(FC_NEW_LINE);

    CFileCtrlInnerUsing _m_innerCtrl;
    if (newLine == inlToBegin || newLine == inlToBeginAndEnd)
      _m_innerCtrl.WriteBufToF(&file, &strNL, dstCoding);
    _m_innerCtrl.WriteBufToF(&file, &strAdd, dstCoding);
    if (newLine == inlToEnd || newLine == inlToBeginAndEnd)
      _m_innerCtrl.WriteBufToF(&file, &strNL, dstCoding);
    
    file.Flush();
    file.Close();
    return true;
  }
  _ASSERTE(!_T("CAi_File::AddLineToEnd(3)"));
  return false;
}

bool CAi_File::AddLineToBegin(CString strFilePathName, CString strAdd, InsertNewLine newLine)
{
  CString strTmpF = CAi_File::CreateTempFile(_T("tmpfileforaddlinetobegin.txt"));
  if (strTmpF.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::AddLineToBegin"));
    return false;
  }

  if (CAi_File::AddLineToEnd(strTmpF, strAdd, inlNoNewLine))
    if (CAi_File::ChangeBlockInFile(strTmpF, strFilePathName, 0, 0, newLine))
      if (CAi_File::DeleteFile_(strTmpF))
        return true;
  _ASSERTE(!_T("CAi_File::AddLineToBegin(2)"));
  return false;
}

//***************************************** Change the block in a file by seeks

// ������� �������� ���� ���� (Dst) ������ ���������� ������� � ������ ����� (Src).
// ������� ��� ������� � Dst �������� SeekStart � SeekStop.
// ����� ����, ����������� �������� ����� ���� �������� ��������� �������� ������ �������� ����� - newLine
bool CAi_File::ChangeBlockInFile(CString strSrcFilePathName, CString strDstFilePathName, int nSeekStartInDstFile, int nSeekStopInDstFile, InsertNewLine newLine)
{
/* �������� ��������� ���������:

      - �� Dst ������� ����� [A] �� nSeekStopInDstFile �� ����� �����:
|--------nSeekStartInDstFile----------nSeekStopInDstFile----------[A]----------|
      - ����� Dst ������������ �� nSeekStartInDstFile:
|--------nSeekStartInDstFile|
      - ��������� ����� Dst ������������ � ����� �����
      - � Dst ����������� Src ���� (� ����� ��������������)
|--------nSeekStartInDstFile+Src|
      - ��������� ����� Dst ������������ � ����� �����
      - � Dst ����������� ����� [A] (� ����� ��������������)
|--------nSeekStartInDstFile+Src+[A]|
  */

  if (strSrcFilePathName.IsEmpty() || strDstFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::ChangeBlockInFile"));
    return false;
  }
  if (nSeekStartInDstFile < 0 || nSeekStopInDstFile < 0 || nSeekStartInDstFile > nSeekStopInDstFile)
  {
    _ASSERTE(!_T("CAi_File::ChangeBlockInFile(2)"));
    return false;
  }
  int nDstFileLength = GetFileLength(strDstFilePathName);
  if (nSeekStartInDstFile > nDstFileLength || nSeekStopInDstFile > nDstFileLength)
  {
    _ASSERTE(!_T("CAi_File::ChangeBlockInFile(3)"));
    return false;
  }
  if (CAi_File::GetFileLength(strSrcFilePathName) == 0)
    return true;
  
  BYTE *pbufAfterStop = nullptr;
  int nDstFileLengthAfterStop = nDstFileLength - nSeekStopInDstFile; 
  CFile fileDst;
  if (fileDst.Open(strDstFilePathName, CFile::modeReadWrite))
  {
    // �� Dst ������� ����� [A] �� nSeekStopInDstFile �� ����� �����

    pbufAfterStop = new BYTE[nDstFileLengthAfterStop];
    ZeroMemory(pbufAfterStop, nDstFileLengthAfterStop);
    fileDst.Seek(nSeekStopInDstFile, CFile::begin);
    fileDst.Read(pbufAfterStop, nDstFileLengthAfterStop);

    // ����� Dst ������������ �� nSeekStartInDstFile

    fileDst.SetLength(nSeekStartInDstFile);
    fileDst.Flush();
    fileDst.Close();
  }
  else
  {
    _ASSERTE(!_T("CAi_File::ChangeBlockInFile(4)"));
    return false;
  }

  bool bRes = false;

  if (fileDst.Open(strDstFilePathName, CFile::modeReadWrite))
  {  
    // ��������� ����� Dst ������������ � ����� �����
    fileDst.Seek(0, CFile::end);

    // � Dst ����������� Src ���� (� ����� ��������������)

    CFile fileSrc;
    if (fileSrc.Open(strSrcFilePathName, CFile::modeRead))
    { 
      CString strNL = CString(FC_NEW_LINE);
      int nSrcFileLength = (int)fileSrc.GetLength();
      BYTE *pbufSrc = new BYTE[nSrcFileLength];
      ZeroMemory(pbufSrc, nSrcFileLength);

      fileSrc.Read(pbufSrc, nSrcFileLength);

      CFileCtrlInnerUsing _m_innerCtrl;
      if (newLine == inlToBegin || newLine == inlToBeginAndEnd)
        _m_innerCtrl.WriteBufToF(&fileDst, &strNL, fcfByDef);
      _m_innerCtrl.WriteBufToF(&fileDst, pbufSrc, nSrcFileLength);
      if (newLine == inlToEnd || newLine == inlToBeginAndEnd)
        _m_innerCtrl.WriteBufToF(&fileDst, &strNL, fcfByDef);

      // � Dst ����������� ����� [A] (� ����� ��������������)

      _m_innerCtrl.WriteBufToF(&fileDst, pbufAfterStop, nDstFileLengthAfterStop);
      
      delete pbufSrc;
      fileSrc.Close();
      bRes = true;
    }
    else
      _ASSERTE(!_T("CAi_File::ChangeBlockInFile(5)"));
    
    delete pbufAfterStop;
    fileDst.Flush();
    fileDst.Close();
  }
  else
  {
    _ASSERTE(!_T("CAi_File::ChangeBlockInFile(6)"));
    return false;
  }
  return bRes;
}

//***************************************** Change the string in a file

bool CAi_File::ChangeStringInFile(CString strFilePathName, CString strOldStringInFile, CString strNewMeanOfOldString)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::ChangeStringInFile"));
    return false;
  }

  CFile file;
  if (file.Open(strFilePathName, CFile::modeReadWrite))
  {
    int nFileLength = (int)file.GetLength();
    BYTE *pbufFromFile = new BYTE[nFileLength + 1];
    ZeroMemory(pbufFromFile, nFileLength + 1);

    file.Read(pbufFromFile, nFileLength);
    CString strFromFile = CString(pbufFromFile);
    strFromFile.Replace(strOldStringInFile.GetBuffer(), strNewMeanOfOldString.GetBuffer());
    file.SetLength(0);
    CFileCtrlInnerUsing _m_innerCtrl;
    _m_innerCtrl.WriteBufToF(&file, &strFromFile, fcfByDef);

    delete pbufFromFile;
    file.Flush();
    file.Close();
    return true;
  }
  _ASSERTE(!_T("CAi_File::ChangeStringInFile(2)"));
  return false;
}

//***************************************** Cut a file fragment

bool CAi_File::CutFragment(CString strFilePathName, int nSeekStart, int nSeekStop)
{
  if (strFilePathName.IsEmpty() || nSeekStart < 0 || nSeekStop < 0 || nSeekStart > nSeekStop)
  {
    _ASSERTE(!_T("CAi_File::CutFragment"));
    return false;
  }

  CFile file;
  if (file.Open(strFilePathName, CFile::modeReadWrite))
  {
    int nSourceFileLength = (int)file.GetLength();
    int nTailLength = nSourceFileLength - nSeekStop;
    BYTE *pbufWrite = new BYTE[nTailLength];
    file.Seek(nSeekStop, CFile::begin);
    file.Read(pbufWrite, nTailLength);
    file.SetLength(nSeekStart);
    file.Seek(0, CFile::end);

    CFileCtrlInnerUsing _m_innerCtrl;
    _m_innerCtrl.WriteBufToF(&file, pbufWrite, nTailLength * sizeof(TCHAR));

    file.Flush();
    file.Close();
    delete pbufWrite;
    return true;
  }
  _ASSERTE(!_T("CAi_File::CutFragment(2)"));
  return false;
}

bool CAi_File::TrimRight(CString strFilePathName, CString strTrim)
{
  if (strFilePathName.IsEmpty() || strTrim.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::TrimRight"));
    return false;
  }

  CFile file;
  if (file.Open(strFilePathName, CFile::modeReadWrite))
  {
    int nSourceFileLength = (int)file.GetLength();
    BYTE *pbufWrite = new BYTE[nSourceFileLength + 1];
    ZeroMemory(pbufWrite, nSourceFileLength + 1);

    file.Read(pbufWrite, nSourceFileLength);
    CString strTmp = CString(pbufWrite);
    strTmp.TrimRight(strTrim);
    file.SetLength(0);
    
    ZeroMemory(pbufWrite, nSourceFileLength + 1);
    memcpy(pbufWrite, strTmp.GetBuffer(), strTmp.GetLength());
    CFileCtrlInnerUsing _m_innerCtrl;
    _m_innerCtrl.WriteBufToF(&file, pbufWrite, strTmp.GetLength() * sizeof(TCHAR));

    file.Flush();
    file.Close();
    delete pbufWrite;
    return true;
  }
  _ASSERTE(!_T("CAi_File::TrimRight"));
  return false;
}

//***************************************** Work with sections in a file

bool CAi_File::SetParameterAndMeanToSection(
                CString strPathNameToFile, 
                CString strSectStart, // ������ ������ (����) � �����, ��� ������
                CString strSectStop, // ����� ������ (����)
                CString strParameter,
                CString strMean,
                CString strSepar) // ����������� � ������ (�������� - ":=")
{
  if (strParameter.IsEmpty() || strMean.IsEmpty())
    return false;

  if (strPathNameToFile.IsEmpty() || strSectStart.IsEmpty() || strSectStop.IsEmpty() || strSepar.IsEmpty())
    return false;

  int nSeekAfter = GetSeekAfter(strPathNameToFile, strSectStart);
  int nSeekBefore = GetSeekBefore(strPathNameToFile, strSectStop);
#ifdef _UNICODE
  ASSERT(false);
#endif
  if (nSeekAfter == -1 && nSeekBefore == -1)
    return AddLineToSection(strPathNameToFile, strParameter + strSepar + strMean, strSectStart, strSectStop);
  // [x]nSeekAfter...nSeekBefore[/x]...nSourceFileLength
  if (nSeekAfter > nSeekBefore)
    return false;
  
  GetParametersFromSection(strPathNameToFile, strSectStart, strSectStop, strSepar);

  CString strAdd;
  bool bIsParameter = false;

  for (int i = 0; i < (int)m_paramsFromFile.size(); ++i)
    if (m_paramsFromFile[i].strParameter == strParameter)
    {
      m_paramsFromFile[i].strMean = strMean;
      bIsParameter = true;
      break;
    }
  if (!bIsParameter)
  {
    PARAMS_FROM_FILE newPar;

    newPar.strParameter = strParameter;
    newPar.strMean = strMean;
    m_paramsFromFile.push_back(newPar);
  }

  CutFragment(strPathNameToFile, nSeekAfter + 2, nSeekBefore); // +2 - "\r\n"

  for (int i = 0; i < (int)m_paramsFromFile.size(); ++i)
  {
    strAdd = m_paramsFromFile[i].strParameter;
    strAdd += strSepar;
    strAdd += m_paramsFromFile[i].strMean;
    AddLineToSection(strPathNameToFile, strAdd, strSectStart, strSectStop);
  }

  FreeParameters();

  return true;
}

// Work with sections in a file
//  Example:
//  ...
//  ##1                 => "##1"      => strSectStart
//  Parameter:=Mean  => ":="        => strSepar
//  ##1end              => "##1end" => strSectStop
//  ...
bool CAi_File::GetParametersFromSection(CString strPathNameToFile,
                    CString strSectStart, // ������ ������ (����) � �����, ��� ������
                    CString strSectStop, // ����� ������ (����)
                    CString strSepar) // ����������� � ������ (�������� - ":=")
{
  FreeParameters();

  if (strPathNameToFile.IsEmpty() || strSectStart.IsEmpty())
    return false;

  CFileCtrlInnerUsing _m_innerCtrl;
  if (strSectStop.IsEmpty())
    strSectStop = _m_innerCtrl.CreateSectStop(strSectStart);

  //int nSeekAfter;
  //int nSeekBefore;

  //nSeekBefore = GetSeekBefore(strPathNameToFile, strSectStart);
  //if (nSeekBefore == -1)
  //  return false;
  //nSeekAfter = GetSeekAfter(strPathNameToFile, strSectStop);
  //if (nSeekAfter == -1)
  //  return false;

  int nAddZero = IsUnicodeFile(strPathNameToFile) ? 3 : 1;
  CFile fileSourceFile;
  CFileException feSourceFile;

  if (fileSourceFile.Open(strPathNameToFile, CFile::modeRead, &feSourceFile))
  {
    TCHAR *pbufWrite = nullptr;
    int nSourceFileLength = (int)fileSourceFile.GetLength();
    
    // nSeekBefore[x]...[/x]nSeekAfter...nSourceFileLength
    //if (nSeekBefore < nSeekAfter && nSeekBefore < nSourceFileLength && nSeekAfter <= nSourceFileLength)
    {
      //nSourceFileLength = nSeekAfter - nSeekBefore;
      pbufWrite = new TCHAR[nSourceFileLength + nAddZero];
      memset(pbufWrite, 0, nSourceFileLength + nAddZero);
      //fileSourceFile.Seek(nSeekBefore, CFile::begin);
      fileSourceFile.Read(pbufWrite, nSourceFileLength);
      GetParametersFromSection(pbufWrite, strSectStart, strSectStop, strSepar);
    }

    delete pbufWrite;
    fileSourceFile.Close();
  }

  if (m_paramsFromFile.size() > 0)
    return true;

  return false;
}

bool CAi_File::GetParametersFromSection(TCHAR *szData, CString strSectStart, CString strSectStop, CString strSepar)
{
  FreeParameters();

  if (szData == nullptr || strSectStart.IsEmpty())
    return false;

  CFileCtrlInnerUsing _m_innerCtrl;
  if (strSectStop.IsEmpty())
    strSectStop = _m_innerCtrl.CreateSectStop(strSectStart);

  CString strData = szData;
  if (m_pstrSectionData)
    *m_pstrSectionData = strData;

  int nSectStart = strData.Find(strSectStart);
  strData.Delete(0, nSectStart + strSectStart.GetLength());
  int nSectStop = strData.Find(strSectStop);    
  strData.Delete(nSectStop, strData.GetLength() - nSectStop);

  CString resToken;
  int nCurPos = 0;
  int nCurPos2 = 0;
  PARAMS_FROM_FILE parFromFile;

  strData.TrimLeft(_T("\r\n"));
  strData.TrimRight(_T("\r\n"));
  resToken = strData.Tokenize(_T("\r\n"), nCurPos);
  while (resToken != "")
  {
    if (strSepar.IsEmpty())
    {
      parFromFile.strParameter = resToken;
      parFromFile.strMean = "";
    }
    else
    {
      nCurPos2 = 0;
      parFromFile.strParameter = resToken.Tokenize(strSepar, nCurPos2); // Parameter
      parFromFile.strMean = resToken.Tokenize(strSepar, nCurPos2); // Mean
      parFromFile.strMean.TrimLeft(_T(" "));
      parFromFile.strMean.TrimRight(_T(" "));
    }
    parFromFile.strParameter.TrimLeft(_T(" "));
    parFromFile.strParameter.TrimRight(_T(" "));
    
    if (!parFromFile.strParameter.IsEmpty())
      m_paramsFromFile.push_back(parFromFile);
    
    resToken = strData.Tokenize(_T("\r\n"), nCurPos);
  }

  return true;
}

bool CAi_File::AddLineToSection(CString strPathNameToFile, CString strAdd, 
                   CString strSectStart, CString strSectStop)
{
  if (strPathNameToFile.IsEmpty() || strAdd.IsEmpty() || strSectStart.IsEmpty() || strSectStop.IsEmpty())
    return false;

  int nSeekAfter;
  int nSeekBefore;
  
  // [x]nSeekAfter...nSeekBefore[/x]
  nSeekAfter = GetSeekAfter(strPathNameToFile, strSectStart);
  if (nSeekAfter == -1)
  {
    TrimRight(strPathNameToFile, FC_NEW_LINE);
    CString strA = 
      CString(FC_NEW_LINE) + CString(FC_NEW_LINE) +
      CString(strSectStart) + CString(FC_NEW_LINE) + CString(strSectStop);
    AddLineToEnd(strPathNameToFile, strA);
  }
  nSeekAfter = GetSeekAfter(strPathNameToFile, strSectStart);
  nSeekBefore = GetSeekBefore(strPathNameToFile, strSectStop);
#ifdef _UNICODE
  ASSERT(false);
#endif
  if (nSeekAfter == -1 || nSeekBefore == -1 || nSeekAfter > nSeekBefore)
  {
    CString strMsg;
    strMsg.Format(_T("Error in CAi_File::AddLineToSection (Af-%d,Bf-%d,Sta-%s,Sto-%s)!"), nSeekAfter,nSeekBefore,strSectStart,strSectStop);
    AfxMessageBox(strMsg);
    return false;
  }

  CFile fileSourceFile;
  CFileException feSourceFile;
  char *pbufWrite = nullptr;
  int nSourceFileLength;
  int nTailLength;

  if (fileSourceFile.Open(strPathNameToFile, CFile::modeReadWrite, &feSourceFile))
  {
    nSourceFileLength = (int)fileSourceFile.GetLength();
    nTailLength = nSourceFileLength - nSeekBefore;
    pbufWrite = new char[nTailLength + 1];
    memset(pbufWrite, 0, nTailLength + 1);
    fileSourceFile.Seek(nSeekBefore, CFile::begin);
    fileSourceFile.Read(pbufWrite, nTailLength);
    fileSourceFile.SetLength(nSeekBefore);
    fileSourceFile.Flush();
    fileSourceFile.Close();
  }
  else
  {
    AfxMessageBox(_T("Error in CAi_File::AddLineToSection 2!"));
    return false;
  }

  CString strA = strAdd + CString(FC_NEW_LINE) + CString(pbufWrite);
  AddLineToEnd(strPathNameToFile, strA);

  delete pbufWrite;
  return true;
}

void CAi_File::FreeParameters()
{
  if (m_paramsFromFile.size() > 0)
    m_paramsFromFile.clear();
}

#ifdef SWITCH_ON_FILE_VERSION_CODE

BOOL CAi_File::DetectFileVersionInfo(HMODULE hModule /*= nullptr*/)
{
  CString strPath;

  GetModuleFileName(hModule, strPath.GetBuffer(_MAX_PATH), _MAX_PATH);
  strPath.ReleaseBuffer();
  return DetectFileVersionInfo(strPath);
}

BOOL CAi_File::DetectFileVersionInfo(LPCTSTR lpszFileName)
{
  m_pInner->Reset();

  DWORD dwHandle;
  DWORD dwFileVersionInfoSize = GetFileVersionInfoSize((LPTSTR)lpszFileName, &dwHandle);
  if (!dwFileVersionInfoSize)
    return FALSE;

  LPVOID lpData = (LPVOID)new BYTE[dwFileVersionInfoSize];
  if (!lpData)
    return FALSE;

  try
  {
    if (!GetFileVersionInfo((LPTSTR)lpszFileName, dwHandle, dwFileVersionInfoSize, lpData))
      throw FALSE;

    // catch default information
    LPVOID lpInfo;
    UINT  unInfoLen;
    if (VerQueryValue(lpData, _T("\\"), &lpInfo, &unInfoLen))
    {
      ASSERT(unInfoLen == sizeof(m_pInner->m_FileInfo));
      if (unInfoLen == sizeof(m_pInner->m_FileInfo))
        memcpy(&m_pInner->m_FileInfo, lpInfo, unInfoLen);
    }

    // find best matching language and codepage
    VerQueryValue(lpData, _T("\\VarFileInfo\\Translation"), &lpInfo, &unInfoLen);
    
    DWORD dwLangCode = 0;
    if (!m_pInner->GetTranslationId(lpInfo, unInfoLen, GetUserDefaultLangID(), dwLangCode, FALSE))
    {
      if (!m_pInner->GetTranslationId(lpInfo, unInfoLen, GetUserDefaultLangID(), dwLangCode, TRUE))
      {
        if (!m_pInner->GetTranslationId(lpInfo, unInfoLen, MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL), dwLangCode, TRUE))
        {
          if (!m_pInner->GetTranslationId(lpInfo, unInfoLen, MAKELANGID(LANG_ENGLISH, SUBLANG_NEUTRAL), dwLangCode, TRUE))
            // use the first one we can get
            dwLangCode = *((DWORD*)lpInfo);
        }
      }
    }
    
    CString strSubBlock;
    strSubBlock.Format(_T("\\StringFileInfo\\%04X%04X\\"), dwLangCode&0x0000FFFF, (dwLangCode&0xFFFF0000)>>16);
    
    // catch string table
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("CompanyName")), &lpInfo, &unInfoLen))
      m_pInner->m_strCompanyName = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("FileDescription")), &lpInfo, &unInfoLen))
      m_pInner->m_strFileDescription = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("FileVersion")), &lpInfo, &unInfoLen))
      m_pInner->m_strFileVersion = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("InternalName")), &lpInfo, &unInfoLen))
      m_pInner->m_strInternalName = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("LegalCopyright")), &lpInfo, &unInfoLen))
      m_pInner->m_strLegalCopyright = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("OriginalFileName")), &lpInfo, &unInfoLen))
      m_pInner->m_strOriginalFileName = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("ProductName")), &lpInfo, &unInfoLen))
      m_pInner->m_strProductName = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("ProductVersion")), &lpInfo, &unInfoLen))
      m_pInner->m_strProductVersion = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("Comments")), &lpInfo, &unInfoLen))
      m_pInner->m_strComments = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("LegalTrademarks")), &lpInfo, &unInfoLen))
      m_pInner->m_strLegalTrademarks = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("PrivateBuild")), &lpInfo, &unInfoLen))
      m_pInner->m_strPrivateBuild = CString((LPCTSTR)lpInfo);
    if (VerQueryValue(lpData, (LPTSTR)(LPCTSTR)(strSubBlock+_T("SpecialBuild")), &lpInfo, &unInfoLen))
      m_pInner->m_strSpecialBuild = CString((LPCTSTR)lpInfo);

    delete[] lpData;
  }
  catch (BOOL)
  {
    delete[] lpData;
    return FALSE;
  }

  return TRUE;
}

WORD CAi_File::GetFileVersion(int nIndex)
{
  if (nIndex == 0)
    return (WORD)(m_pInner->m_FileInfo.dwFileVersionLS & 0x0000FFFF);
  else if (nIndex == 1)
    return (WORD)((m_pInner->m_FileInfo.dwFileVersionLS & 0xFFFF0000) >> 16);
  else if (nIndex == 2)
    return (WORD)(m_pInner->m_FileInfo.dwFileVersionMS & 0x0000FFFF);
  else if (nIndex == 3)
    return (WORD)((m_pInner->m_FileInfo.dwFileVersionMS & 0xFFFF0000) >> 16);
  else
    return 0;
}

WORD CAi_File::GetProductVersion(int nIndex)
{
  if (nIndex == 0)
    return (WORD)(m_pInner->m_FileInfo.dwProductVersionLS & 0x0000FFFF);
  else if (nIndex == 1)
    return (WORD)((m_pInner->m_FileInfo.dwProductVersionLS & 0xFFFF0000) >> 16);
  else if (nIndex == 2)
    return (WORD)(m_pInner->m_FileInfo.dwProductVersionMS & 0x0000FFFF);
  else if (nIndex == 3)
    return (WORD)((m_pInner->m_FileInfo.dwProductVersionMS & 0xFFFF0000) >> 16);
  else
    return 0;
}

DWORD CAi_File::GetFileFlagsMask()
{
  return m_pInner->m_FileInfo.dwFileFlagsMask;
}

DWORD CAi_File::GetFileFlags()
{
  return m_pInner->m_FileInfo.dwFileFlags;
}

DWORD CAi_File::GetFileOs()
{
  return m_pInner->m_FileInfo.dwFileOS;
}

DWORD CAi_File::GetFileType()
{
  return m_pInner->m_FileInfo.dwFileType;
}

DWORD CAi_File::GetFileSubtype()
{
  return m_pInner->m_FileInfo.dwFileSubtype;
}

CTime CAi_File::GetFileDate()
{
  FILETIME ft;
  ft.dwLowDateTime = m_pInner->m_FileInfo.dwFileDateLS;
  ft.dwHighDateTime = m_pInner->m_FileInfo.dwFileDateMS;
  return CTime(ft);
}

CString CAi_File::GetCompanyName()
{
  return m_pInner->m_strCompanyName;
}

CString CAi_File::GetFileDescription()
{
  return m_pInner->m_strFileDescription;
}

CString CAi_File::GetFileVersion()
{
  return m_pInner->m_strFileVersion;
}

CString CAi_File::GetInternalName()
{
  return m_pInner->m_strInternalName;
}

CString CAi_File::GetLegalCopyright()
{
  return m_pInner->m_strLegalCopyright;
}

CString CAi_File::GetOriginalFileName()
{
  return m_pInner->m_strOriginalFileName;
}

CString CAi_File::GetProductName()
{
  return m_pInner->m_strProductName;
}

CString CAi_File::GetProductVersion()
{
  return m_pInner->m_strProductVersion;
}

CString CAi_File::GetComments()
{
  return m_pInner->m_strComments;
}

CString CAi_File::GetLegalTrademarks()
{
  return m_pInner->m_strLegalTrademarks;
}

CString CAi_File::GetPrivateBuild()
{
  return m_pInner->m_strPrivateBuild;
}

CString CAi_File::GetSpecialBuild()
{
  return m_pInner->m_strSpecialBuild;
}

#endif // #ifdef SWITCH_ON_FILE_VERSION_CODE

////////////////////////////////////////////////

bool CFileCtrlInnerUsing::PathInSystemEnvironment(CString strPath, bool bAdd)
{
  // ���������� ���� � ���������
  HKEY hKEY;
  if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"), 0, KEY_ALL_ACCESS, &hKEY) != ERROR_SUCCESS)
    return false;
  DWORD nValueType = REG_EXPAND_SZ;
  DWORD nSz = 5000;
  BYTE pBuff[5000] = {0};
  if (RegQueryValueEx(hKEY, _T("Path"), nullptr, &nValueType, pBuff, &nSz) != ERROR_SUCCESS)
    return false;
  CString strPathes((TCHAR*)pBuff);
  if (bAdd)
  {
    int nFindIt = strPathes.Find(strPath);
    if (nFindIt != -1) // Has
      return true;
    if (!strPathes.IsEmpty()
      && strPathes.Right(1) != ';')
      strPathes += _T(";");
    strPathes += strPath;
  }
  else // delete
  {
    /*
    �������� ������ � ����� � ������� � ��������� ����� �������� ������ (� ������� - ���� ����� ��������� nFindIt):
    1) strPath                 =>                  (����� � strPathes)
    2) strPath;<...>         =>  ;<...>         (nFindIt->;)
    3) <...>;strPath;<...> =>  <...>;;<...> (;nFindIt->;)
    4) <...>;strPath         =>  <...>;         (;nFindIt->)
    */
    int nFindIt = strPathes.Find(strPath);
    if (nFindIt == -1) // Has not
      return true;
    strPathes.Delete(nFindIt, strPath.GetLength());
    if (!strPathes.IsEmpty()) // 1)
    {
      strPathes.TrimLeft(_T(";")); // 2)
      strPathes.TrimRight(_T(";")); // 4
      int nFindIt = strPathes.Find(_T(";;"));
      if (nFindIt != -1) // Is
        strPathes.Delete(nFindIt, 1); // 3
    }
  }

  if (RegSetValueEx(hKEY,  _T("Path"), 0, REG_EXPAND_SZ, (BYTE*)strPathes.GetBuffer(), (strPathes.GetLength() + 1) * sizeof(TCHAR)) != ERROR_SUCCESS)
    return false;

  //Notify all windows that User Environment variables are changed
  CWaitCursor wait;
  TCHAR* szwEnv = _T("Environment");
  PDWORD_PTR result = 0;
  if (SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0, (LPARAM)szwEnv, SMTO_NORMAL, 5000, result) == 0)
    AfxMessageBox(IDS_REQUIRE_REBOOT); // ��� �������� ��������� ��������� ����������� ������������� ���������!

  return true;
}

CString CFileCtrlInnerUsing::SplitPath(CString strFilePathName, CFileCtrlInnerUsing::ESplitPath sp)
{
  TCHAR szDisk[_MAX_DRIVE] = {0};
  TCHAR szPath[_MAX_DIR] = {0};
  TCHAR szFileName[_MAX_FNAME] = {0};
  TCHAR szFileExtention[_MAX_EXT] = {0};
  
  #ifdef _UNICODE
  _wsplitpath_s(strFilePathName, szDisk, _MAX_DRIVE, szPath, _MAX_DIR, szFileName, _MAX_FNAME, szFileExtention, _MAX_EXT);
  #else
  _splitpath_s(strFilePathName, szDisk, _MAX_DRIVE, szPath, _MAX_DIR, szFileName, _MAX_FNAME, szFileExtention, _MAX_EXT);
  #endif
  
  TCHAR* pszStr = nullptr;
  switch (sp)
  {
    case spDisk: pszStr = szDisk; break;
    case spPath: pszStr = szPath; break;
    case spFileName: return CString(CString(szFileName) + CString(szFileExtention)); break;
    case spFileExtention: pszStr = szFileExtention; break;
    default: ASSERT(false);
  };

  return CString(pszStr);
}

void CFileCtrlInnerUsing::GetListOfFilesAndFolders(CString strFolderPath, vector<LIST_OF_FOLDER_AND_FILES>* lstF, bool bLookSubFolders, int& nOrderIdx, int nParentIdx, int nDeep)
{
  CFileFind finder;
  CString strFileOrNewFolder;
  LIST_OF_FOLDER_AND_FILES folder;
  LIST_OF_FILES_IN_FOLDER file;
  BOOL bWorking = finder.FindFile(strFolderPath + _T("\\*.*"));

  ++nDeep;
  int nCurrentIdx = nOrderIdx;
  int nParentIdxForDeeper = nOrderIdx;
  ++nOrderIdx;

  if (!bWorking)
    return;
  
  folder.strPath = strFolderPath;

  while (bWorking)
  {
    bWorking = finder.FindNextFile();
    strFileOrNewFolder = finder.GetFileName();
    if (strFileOrNewFolder != "." && strFileOrNewFolder != _T(".."))
    {
      if (finder.IsDirectory())
      {
        if (bLookSubFolders)
          GetListOfFilesAndFolders(folder.strPath + _T("\\") + strFileOrNewFolder, lstF, true, nOrderIdx, nParentIdxForDeeper, nDeep);
      }
      else
      {
        file.strFileName = strFileOrNewFolder;
        file.strFilePathName = finder.GetFilePath();
        folder.files.push_back(file);
      }
    }
  }

  folder.nDeep = nDeep;
  folder.nParentIdx = nParentIdx;
  folder.nCurrentIdx = nCurrentIdx;
  lstF->insert(lstF->begin(), folder);
}

CString CFileCtrlInnerUsing::CreateSectStop(CString strSectStart)
{
  strSectStart.Insert(1, _T("/"));
  return strSectStart;
}

int CFileCtrlInnerUsing::ReadFileToBuffer(CString strFilePathName, BYTE** pBufferB, CString* pstrBufferS, bool bZeroEnd)
{
  if (strFilePathName.IsEmpty())
  {
    _ASSERTE(!_T("CAi_File::ReadFileToBuffer"));
    return -1;
  }
  if (!CAi_File::IsFileExist(strFilePathName))
  {
    _ASSERTE(!_T("CAi_File::ReadFileToBuffer(2)"));
    return -1;
  }

  enum Read_to_Type { rttB, rttS };
  Read_to_Type rtt = rttS;
  BYTE* pTmpBuff = nullptr;

  if (pBufferB != nullptr)
  {
    delete *pBufferB;
    *pBufferB = nullptr;
    rtt = rttB;
  }
  else
  {
    ASSERT(pstrBufferS);
    *pstrBufferS = "";
  }

  CFile file;
  if (file.Open(strFilePathName, CFile::modeRead))
  {
    int nCntRead = 0;
    int nFileLength = (int)file.GetLength();
    if (nFileLength > 0)
    {
      int nBufSize = (bZeroEnd) ? nFileLength + 1 : nFileLength;
      pTmpBuff = new BYTE[nBufSize];
      ZeroMemory(pTmpBuff, nBufSize);
      nCntRead = file.Read(pTmpBuff, nFileLength);

      if (rtt == rttB)
        *pBufferB = pTmpBuff;
      else
      if (rtt == rttS)
      {
        CString strTmp2;
        if (IsTextUnicode(pTmpBuff, nCntRead, nullptr))
        {
          BYTE nByteOrderMark = *pTmpBuff;
          BYTE nByteOrderMark2 = (nCntRead > 1) ? *(pTmpBuff + 1) : 0;
          int nInc = (nByteOrderMark == 0xFF && nByteOrderMark2 == 0xFE) ? 1 : 0;
          CString strTmp = CString((wchar_t*)pTmpBuff + nInc);
          strTmp2.SetString(strTmp, (nCntRead - nInc * 2) / 2);
        }
        else
        {
          CString strTmp = CString(pTmpBuff);
          strTmp2.SetString(strTmp, nCntRead);
        }
        *pstrBufferS = strTmp2;
        delete pTmpBuff;
      }
    }
        
    file.Close();
    return nCntRead;
  }
  else
  {
    _ASSERTE(!_T("CAi_File::ReadFileToBuffer(3)"));
    return -1;
  }
  _ASSERTE(!_T("CAi_File::ReadFileToBuffer(4)"));
  return 0;
}

void CFileCtrlInnerUsing::WriteBufToF(CFile* pF, CString* pstrBuf, FileCodingFormat coding)
{
  if (pstrBuf->GetLength() == 0)
    return;

  bool bAppU = false;
  #ifdef _UNICODE
  bAppU = true;
  #endif
  BYTE* pBuff = nullptr;
  int nBuffSize = 0;

  if (bAppU && coding == fcfASCII) // need unicode CString convert to multi-byte
  {
    if (!ConverWToA(pstrBuf, &pBuff, nBuffSize))
      return;
  }
  else
  if (!bAppU && coding == fcfUnicode)
  {
    if (!ConverAToW(pstrBuf, &pBuff, nBuffSize))
      return;
  }
  else
  {
    if (!ConvertToBuffByDef(pstrBuf, &pBuff, nBuffSize))
      return;
  }

  WriteBufToF(pF, pBuff, nBuffSize);
  delete pBuff;
}

void CFileCtrlInnerUsing::WriteBufToF(CFile* pF, BYTE* pstrBuf, int nBufSize)
{
  if (nBufSize <= 0)
  {
    _ASSERTE(!_T("CAi_File::WriteBufToF"));
    return;
  }
  pF->Write(pstrBuf, nBufSize);
  pF->Flush();
}

bool CFileCtrlInnerUsing::ConverWToA(CString* pstrBuf, BYTE** pBuf, int& nBufSize)
{
  nBufSize = pstrBuf->GetLength() + 1; // 1 for zero end
  *pBuf = new BYTE[nBufSize];
  if (*pBuf == nullptr)
  {
    _ASSERTE(!_T("CAi_File::WriteBufToF(2)"));
    return false;
  }
  ZeroMemory(*pBuf, nBufSize);
  size_t i = 0;
  const wchar_t* pSrcStr = (const wchar_t*)pstrBuf->GetBuffer();
  mbstate_t nMbSt = 0;
  if (wcsrtombs_s(&i, (CHAR*)*pBuf, nBufSize, &pSrcStr, _TRUNCATE, &nMbSt) > 0
    || i <= 0)
  {
    delete *pBuf;
    _ASSERTE(!_T("CAi_File::WriteBufToF(2)"));
    return false;
  }
  return true;
}

bool CFileCtrlInnerUsing::ConverAToW(CString* pstrBuf, BYTE** pBuf, int& nBufSize)
{
  int nStrWLen = pstrBuf->GetLength() * 2;
  nBufSize = nStrWLen + 2; // 2 for zero end
  nBufSize += m_bSetUnicodeByteOrderMark ? 2 : 0;
  *pBuf = new BYTE[nBufSize];
  if (*pBuf == nullptr)
  {
    _ASSERTE(!_T("CAi_File::WriteBufToF(2)"));
    return false;
  }
  ZeroMemory(*pBuf, nBufSize);
  if (m_bSetUnicodeByteOrderMark)
  {
    USHORT nUM = 0xFEFF;
    memcpy(*pBuf, &nUM, 2);
  }
  size_t i = 0;
  wchar_t* pBufTmp = (wchar_t*)(m_bSetUnicodeByteOrderMark ? *pBuf + 2 : *pBuf);
  int nCopyBuffSize = m_bSetUnicodeByteOrderMark ? nBufSize - 2 : nBufSize;
  const char* pSrcStr = (const char*)pstrBuf->GetBuffer();
  if (mbstowcs_s(&i, pBufTmp, nCopyBuffSize / 2, pSrcStr, nStrWLen) > 0
    || i <= 0)
  {
    delete *pBuf;
    _ASSERTE(!_T("CAi_File::WriteBufToF(2)"));
    return false;
  }
  return true;
}

bool CFileCtrlInnerUsing::ConvertToBuffByDef(CString* pstrBuf, BYTE** pBuf, int& nBufSize)
{
  #ifdef _UNICODE

  nBufSize = pstrBuf->GetLength() * 2;
  nBufSize += m_bSetUnicodeByteOrderMark ? 2 : 0;
  *pBuf = new BYTE[nBufSize];
  if (*pBuf == nullptr)
  {
    _ASSERTE(!_T("CAi_File::ConvertToBuffByDef"));
    return false;
  }
  ZeroMemory(*pBuf, nBufSize);
  if (m_bSetUnicodeByteOrderMark)
  {
    USHORT nUM = 0xFEFF;
    memcpy(*pBuf, &nUM, 2);
  }
  BYTE* pBufTmp = m_bSetUnicodeByteOrderMark ? *pBuf + 2 : *pBuf;
  int nCopyCount = m_bSetUnicodeByteOrderMark ? nBufSize - 2 : nBufSize;
  memcpy(pBufTmp, pstrBuf->GetBuffer(), nCopyCount);

  #else

  nBufSize = pstrBuf->GetLength();
  *pBuf = new BYTE[nBufSize];
  if (*pBuf == nullptr)
  {
    _ASSERTE(!_T("CAi_File::ConvertToBuffByDef(2)"));
    return false;
  }
  ZeroMemory(*pBuf, nBufSize);
  memcpy(*pBuf, pstrBuf->GetBuffer(), nBufSize);

  #endif

  return true;
}

#ifdef SWITCH_ON_FILE_VERSION_CODE

BOOL CFileCtrlInnerUsing::GetTranslationId(LPVOID lpData, UINT unBlockSize, WORD wLangId, DWORD &dwId, BOOL bPrimaryEnough/*= FALSE*/)
{
  LPWORD lpwData = nullptr;
  for (lpwData = (LPWORD)lpData; (LPBYTE)lpwData < ((LPBYTE)lpData)+unBlockSize; lpwData+=2)
  {
    if (*lpwData == wLangId)
    {
      dwId = *((DWORD*)lpwData);
      return TRUE;
    }
  }

  if (!bPrimaryEnough)
    return FALSE;

  for (lpwData = (LPWORD)lpData; (LPBYTE)lpwData < ((LPBYTE)lpData)+unBlockSize; lpwData+=2)
  {
    if (((*lpwData)&0x00FF) == (wLangId&0x00FF))
    {
      dwId = *((DWORD*)lpwData);
      return TRUE;
    }
  }

  return FALSE;
}

void CFileCtrlInnerUsing::Reset()
{
  ZeroMemory(&m_FileInfo, sizeof(m_FileInfo));
  m_strCompanyName.Empty();
  m_strFileDescription.Empty();
  m_strFileVersion.Empty();
  m_strInternalName.Empty();
  m_strLegalCopyright.Empty();
  m_strOriginalFileName.Empty();
  m_strProductName.Empty();
  m_strProductVersion.Empty();
  m_strComments.Empty();
  m_strLegalTrademarks.Empty();
  m_strPrivateBuild.Empty();
  m_strSpecialBuild.Empty();
}

#endif // #ifdef SWITCH_ON_FILE_VERSION_CODE