#include "stdafx.h"
#include "Ai_BCC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_BCC::CAi_BCC()
{
}

unsigned char CAi_BCC::CalcBCC(unsigned char* pData, int nDataLen)
{
  ASSERT(pData);
  ASSERT(nDataLen > 0);
  
  /*
  Algorithm from:
  https://en.wikipedia.org/wiki/Longitudinal_redundancy_check

    Set LRC = 0
    For each byte b in the buffer
    do
        Set LRC = (LRC + b) AND 0xFF
    end do
    Set LRC = (((LRC XOR 0xFF) + 1) AND 0xFF)

    BCC is XOR of all the bytes in a given byte stream
    excluding the first SOH[chr(1)] or STX[chr(2)] till first ETX[chr(3)]
    or EOT[chr(4)]. ETX is included in the BCC.

    Another says this:

    The BCC consists of 2 bytes of characters.
    The BCC is the result of exclusive OR operation (XOR) on the
    BCC calculation range from the first character to the character
    immediately before the BCC. The character string of the XOR result is
    inserted as a BCC before the terminator.
  */

  unsigned char LRC = 0;

  for (int i = 0; i < nDataLen; ++i)
  {
    unsigned char ch = *(pData + i);
    LRC = (LRC + ch);
  }

  LRC ^= 0xFF;
  LRC += 1;

  return LRC;
}
