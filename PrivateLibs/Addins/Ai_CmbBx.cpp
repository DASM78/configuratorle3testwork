#include "StdAfx.h"
#include "Ai_File.h"
#include "Ai_Str.h"
#include "Ai_CmbBx.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int CAi_CmbBx::alignLeft = 0;
const int CAi_CmbBx::alignRight = 1;
const int CAi_CmbBx::alignMiddle = 2;

CWnd CAi_CmbBx::m_tipWnd;
int CAi_CmbBx::m_OriginalSel = LB_ERR;
UINT CAi_CmbBx::m_nDelayTime = 9000; // millisecond
BOOL CAi_CmbBx::m_isEnter = FALSE;
CFont CAi_CmbBx::m_font;
CMap<HWND, HWND, WNDPROC, WNDPROC&> CAi_CmbBx::m_mapWndProc;
CMap<HWND, HWND, CAi_CmbBx*, CAi_CmbBx*&> CAi_CmbBx::m_mapCombo;

CAi_CmbBx::CAi_CmbBx(bool bShowTrackToolTip)
  : m_bResave(false)
  , m_nTextesCount(15)
  , m_bShowTrackToolTip(bShowTrackToolTip)
{
  if (m_bShowTrackToolTip)
  {
    // Using system tool-tip font for the tool-tip window
    NONCLIENTMETRICS ncm = {0};
    ncm.cbSize = sizeof(NONCLIENTMETRICS);
    SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
    if (m_font.m_hObject == nullptr)
      m_font.CreateFontIndirect(&ncm.lfStatusFont);

    m_rcButton = CRect(0,0,0,0);
    m_isEnableTool = TRUE; // The tool-tip is allowed

    m_alignStyle = CAi_CmbBx::alignLeft;

    // Create the tool-tip window
    CreateTooltipWnd();
  }
}

CAi_CmbBx::~CAi_CmbBx()
{
  if (m_bShowTrackToolTip)
  {
    // Clean up the map
    if (m_cbi.hwndList)
    {
      m_mapWndProc.RemoveKey(m_cbi.hwndList);
      m_mapCombo.RemoveKey(m_cbi.hwndList);
    }
    if (m_cbi.hwndItem)
    {
      m_mapWndProc.RemoveKey(m_cbi.hwndItem);
      m_mapCombo.RemoveKey(m_cbi.hwndItem);
    }
    // Destroy the tool-tip window
    DestroyTooltipWnd();

    // Remove all disabled items
    m_arrDisabledItems.RemoveAll();
  }
}

BEGIN_MESSAGE_MAP(CAi_CmbBx, CComboBox)
  //{{AFX_MSG_MAP(CAi_CmbBx)
  ON_WM_CREATE()
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONDOWN()
  ON_CONTROL_REFLECT(CBN_DROPDOWN, OnDropdown)
  ON_WM_MOUSEWHEEL()
  //}}AFX_MSG_MAP
  ON_WM_DESTROY()
  ON_MESSAGE(WM_MOUSEHOVER, OnMouseOver)
  ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
END_MESSAGE_MAP()

void CAi_CmbBx::SelectItem(CComboBox* pCmbBx, CString str)
{
  ASSERT(pCmbBx);

  CString strLBText;
  int nCount = pCmbBx->GetCount();
  for (int i = 0; i < nCount; ++i)
  {
    pCmbBx->GetLBText(i, strLBText);
    if (strLBText == str)
    {
      pCmbBx->SetCurSel(i);
      break;
    }
  }
}

void CAi_CmbBx::SelectItem(CString str)
{
  SelectItem(this, str);
}

void CAi_CmbBx::DeleteItem(CComboBox* pCmbBx, CString str)
{
  ASSERT(pCmbBx);

  CString strLBText;
  int nIDDel = -1;
  int nCount = pCmbBx->GetCount();
  for (int i = 0; i < nCount; ++i)
  {
    pCmbBx->GetLBText(i, strLBText);
    if (strLBText == str)
    {
      nIDDel = i;
      break;
    }
  }
  if (nIDDel >= 0)
    pCmbBx->DeleteString(nIDDel);
}

void CAi_CmbBx::DeleteItem(CString str)
{
  DeleteItem(this, str);
}

void CAi_CmbBx::FillInOfTextes(CString strFileName, int nTextesCount)
{
  m_nTextesCount = nTextesCount;
  m_textes.clear();

  CString strBuff;
  CAi_File::ReadFileToString(strFileName, &strBuff);
  if (!strBuff.IsEmpty())
  {
    vector<CString> items;
    CAi_Str::CutString(strBuff, _T("\r\n"), &items);
    int nCount = (int)items.size();
    if (nCount > m_nTextesCount)
    {
      nCount = m_nTextesCount;
      m_bResave = true;
    }
    for (int i = 0; i < nCount; ++i)
    {
      vector<CString> pairOfTextes;
      CAi_Str::CutString(items[i], _T("|"), &pairOfTextes);

      __time64_t nTime = 0;
      #ifdef UNICODE
      swscanf_s(pairOfTextes[1].GetBuffer(), _T("%d"), &nTime);
      #else
      sscanf_s(pairOfTextes[1].GetBuffer(), _T("%d"), &nTime);
      #endif
      
      m_textes.insert(pair<__time64_t, CString>(nTime, pairOfTextes[0]));
    }
    Refill();
  }
}

void CAi_CmbBx::Refill()
{
  ResetContent();
  if (m_textes.size() > 0)
  {
    map<__time64_t, CString>::iterator iter = m_textes.end();
    iter--;
    for (size_t i = m_textes.size() - 1; ; i--)
    {
      AddString(iter->second);
      iter--;
      if (i == 0)
        break;
    }
  }
}

int CAi_CmbBx::IsTextes()
{
  return (int)m_textes.size();
}

// Get last (FindTextMode::fpmNewest) selected text or oldest selected text (FindTextMode::fpmOldest)
CString CAi_CmbBx::FindText_(FindTextMode fpm, CString strText, __time64_t* pnTime)
{
  if (fpm == fpmOldest && m_textes.size() > 0) // oldest in the begin
  {
    map<__time64_t, CString>::iterator iter = m_textes.begin();
    if (pnTime)
      *pnTime = iter->first;
    return iter->second;
  }
  else
  if (fpm == fpmNewest && m_textes.size() > 0) // newest in the end
  {
    map<__time64_t, CString>::iterator iter = m_textes.end();
    iter--;
    if (pnTime)
      *pnTime = iter->first;
    return iter->second;
  }
  else
  if (fpm == fpmText && !strText.IsEmpty() && m_textes.size() > 0)
  {
    map<__time64_t, CString>::iterator iter = m_textes.begin();
    for (int i = 0; i < (int)m_textes.size(); ++i)
    {
      if (strText == iter->second)
      {
        if (pnTime)
          *pnTime = iter->first;
        return iter->second;
      }
      ++iter;
    }
  }
  return _T("");
}

// Call this function from OnCbnSelchange() if a new item was selected for update selected item time
void CAi_CmbBx::UpdateChoiseTextTime()
{
  CString strSelText;
  GetLBText(GetCurSel(), strSelText);
  map<__time64_t, CString>::iterator iter = m_textes.begin();
  int nCount = (int)m_textes.size();
  
  for (int i = 0; i < nCount; ++i)
  {
    if (iter->second == strSelText)
    {
      if (i == nCount - 1) // it item already selected
        return;

      m_textes.erase(iter);
      m_textes.insert(pair<__time64_t, CString>(CTime::GetCurrentTime().GetTime(), strSelText));
      
      Refill();
      SelectItem(strSelText);
      
      m_bResave = true;
      break;
    }
    ++iter;
  }
}

void CAi_CmbBx::AddOrJustSelectTextIn(CString strText)
{
  if (strText.IsEmpty())
    return;
  CString strCurrentText;
  GetWindowText(strCurrentText);
  if (strText == strCurrentText)
    return;

  m_bResave = true;

  // detect if text exists

  CString strLBText;
  bool bAdd = true;
  int nCount = GetCount();
  int nTextPos = 0;
  for (; nTextPos < nCount; ++nTextPos)
  {
    GetLBText(nTextPos, strLBText);
    if (strLBText == strText)
    {
      bAdd = false; // text is
      break;
    }
  }
  if (bAdd)
  {
    if ((int)m_textes.size() == m_nTextesCount)
    { // delete oldest text because stock is full
      map<__time64_t, CString>::iterator iter = m_textes.begin();
      DeleteItem(iter->second);
      m_textes.erase(iter);
    }
    
    // add new text

    m_textes.insert(pair<__time64_t, CString>(CTime::GetCurrentTime().GetTime(), strText));
    Refill();
    SelectItem(strText);
  }
  else
  {
    SelectItem(strText);
    UpdateChoiseTextTime();
  }
}

void CAi_CmbBx::SaveTextes(CString strFileName)
{
  if (m_bResave)
  {
    m_bResave = false;

    CString strTextes;
    CString strTmp;
    map<__time64_t, CString>::iterator iter = m_textes.begin();
    for (int i = 0; i < (int)m_textes.size(); ++i)
    {
      if (i > 0)
        strTextes += _T("\r\n");
      strTmp.Format(_T("%s|%d"), iter->second, iter->first);
      strTextes += strTmp;
      ++iter;
    }
    if (CAi_File::IsFileExist(strFileName))
      CAi_File::ClearFile(strFileName);
    CAi_File::WriteStringToFile(strFileName, &strTextes);
  }
}

CEdit* CAi_CmbBx::GetEditBox()
{
  if (::IsWindow(m_cbi.hwndItem))
    return (CEdit*)CWnd::FromHandle(m_cbi.hwndItem);

  return nullptr;
}

CListBox* CAi_CmbBx::GetListBox()
{
  if (::IsWindow(m_cbi.hwndList))
    return (CListBox*)CWnd::FromHandle(m_cbi.hwndList);

  return nullptr;
}

// Default, Align the left-edge of list-box with the combo,
// also we can set other alignment style here.
void CAi_CmbBx::SetAlignStyle(int style)
{
  m_alignStyle = style;
}

void CAi_CmbBx::SetTooltipDelay(UINT nDelay)
{
  m_nDelayTime = nDelay;
}

UINT CAi_CmbBx::GetTooltipDelay()
{
  return m_nDelayTime;
}

void CAi_CmbBx::SetToolTip(BOOL bEnable/*=TRUE*/)
{
  m_isEnableTool = bEnable;
}

void CAi_CmbBx::SetTipText(int nIndex, LPCTSTR pszTipText)
{
  CListBox* pListbox = this->GetListBox();
  if (!pListbox) return;

  if (pszTipText)
    m_mapTipTexts.SetAt(nIndex, pszTipText);
  else
    m_mapTipTexts.RemoveKey(nIndex);
}

BOOL CAi_CmbBx::SetDisabledItem(int nIndex, BOOL bDisabled/*=TRUE*/)
{
  if (nIndex < 0 || nIndex > (this->GetCount() - 1))
    return FALSE;
  
  for (int i = 0; i < m_arrDisabledItems.GetSize(); ++i)
  {
    if ((UINT)nIndex == m_arrDisabledItems.GetAt(i))
    {
      if (bDisabled == FALSE)
      {
        m_arrDisabledItems.RemoveAt(i);
      }

      return TRUE;
    }
  }

  m_arrDisabledItems.Add(nIndex);

  return TRUE;
}

int CAi_CmbBx::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  if (CComboBox::OnCreate(lpCreateStruct) == -1)
    return -1;

  // If the combo-box isn't dynamically subclassed that 
  // should get rid of the /* and */ commenting characters below
  /*
  if (m_isEnableTool == TRUE)
  {
    InstallHookForListboxAndEditbox();
  }
  */
  return 0;
}

void CAi_CmbBx::CreateTooltipWnd()
{
  // However, the tool-tip window only once created and all combobox 
  // have share in the tool-tip window.
  if (!::IsWindow(m_tipWnd.m_hWnd))
  {
    m_tipWnd.CreateEx(WS_EX_TOOLWINDOW,
      AfxRegisterWndClass(0, AfxGetApp()->LoadStandardCursor(IDC_ARROW)),
    nullptr, WS_POPUP, 0, 0, 0, 0, nullptr, nullptr);

    // Set up hooking with the tip window
    // Note: Refer to MSDN, In Windows NT/2000, You cannot change this attribute(GWL_WNDPROC) 
    // if the window(i.e, too-tip) does not belong to the same process as the calling thread.
    WNDPROC oldTipWndProc = (WNDPROC)::SetWindowLongPtr(m_tipWnd.m_hWnd, 
      GWLP_WNDPROC, (DWORD_PTR)HookTipWndProc);
    // Add the old hook(i.e, hook associated with window) to the map
    m_mapWndProc.SetAt(m_tipWnd.m_hWnd, oldTipWndProc);
  }
}

void CAi_CmbBx::DestroyTooltipWnd()
{
  INT_PTR nSize = m_mapWndProc.GetCount();
  WNDPROC oldTipWndProc;
  HWND hWnd = m_tipWnd.m_hWnd;
  BOOL bRet = m_mapWndProc.Lookup(hWnd, oldTipWndProc);
  if (nSize == 1 && bRet == TRUE)
  {// Reset only one window procedure(i.e, the tool-tip window procedure)
   // that means any combobox doesn't use the tool-tip window. 
    m_tipWnd.DestroyWindow();
    m_mapWndProc.RemoveKey(hWnd);
    m_mapWndProc.RemoveAll();
    m_mapCombo.RemoveAll();
  }
}

void CAi_CmbBx::InstallHookForListboxAndEditbox()
{
  // Get the list-box/edit-box within the combobox and set up hooking with it.
  // Note: this only for Win98 or WinNT5.0 or later, 
  // However you must defined WINVER >= 0x0500 within the project
  ZeroMemory(&m_cbi, sizeof(COMBOBOXINFO));
  m_cbi.cbSize = sizeof(COMBOBOXINFO);
  ::GetComboBoxInfo(m_hWnd, &m_cbi);
  m_rcButton = m_cbi.rcButton;

  if (m_cbi.hwndList)
  {
    // Set up hooking for the list-box within the combobox
    // Note: Refer to MSDN, In Windows NT/2000, You cannot change this attribute(GWL_WNDPROC) 
    // if the window(i.e, list-box) does not belong to the same process as the calling thread.
    WNDPROC oldListWndProc = (WNDPROC)::SetWindowLongPtr(m_cbi.hwndList, GWLP_WNDPROC, (DWORD_PTR)HookListboxWndProc);
    // Add the old hook(i.e, hook associated with window) to the map
    m_mapWndProc.SetAt(m_cbi.hwndList, oldListWndProc);
    m_mapCombo.SetAt(m_cbi.hwndList, (CAi_CmbBx*)this);
  }
  
  if (m_cbi.hwndItem)
  {
    // Set up hooking for the edit-box within the combobox
    // Note: Refer to MSDN, In Windows NT/2000, You cannot change this attribute(GWL_WNDPROC) 
    // if the window(i.e, edit-box) does not belong to the same process as the calling thread.
    WNDPROC oldEditWndProc = (WNDPROC)::SetWindowLongPtr(m_cbi.hwndItem, GWLP_WNDPROC, (DWORD_PTR)HookEditboxWndProc);
    // Add the old hook(i.e, hook associated with window) to the map
    m_mapWndProc.SetAt(m_cbi.hwndItem, oldEditWndProc);
    m_mapCombo.SetAt(m_cbi.hwndItem, (CAi_CmbBx*)this);
  }
}

// The callback function that hooks up and processes messages
// send to the list-box within the combobox
LRESULT CALLBACK 
CAi_CmbBx::HookListboxWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  CListBox* pList = (CListBox*)CWnd::FromHandle(hWnd);
  CAi_CmbBx* pThisCombo = nullptr;
  m_mapCombo.Lookup(hWnd, pThisCombo);

  if (message == WM_MOUSEMOVE)
  {
    WORD xPos, yPos;
    xPos = LOWORD(lParam);
    yPos = HIWORD(lParam);
    CPoint point(xPos, yPos); 
    CRect rcClient;
    ::GetClientRect(hWnd, &rcClient);
    if (rcClient.PtInRect(point))
    {// Handle mouse move event that may show a tool-tip window...
      CAi_CmbBx::HandleListboxMouseMove(pList, wParam, point);
    }
    else
    {// However, the list-box may be captured thus we must know when the mouse cursor
     // out of the area of the list-box, and send 'WM_MOUSELEAVE' notification.
      ::SendMessage(hWnd, WM_MOUSELEAVE, wParam, lParam);
    }

    if (!m_isEnter)
    {// Tracking the mouse event which are hovering and leaving.
      OnTrackMouseEvent(hWnd, TME_HOVER|TME_LEAVE);
      m_isEnter = TRUE;
    }
  }
  else if (message == WM_MOUSELEAVE)
  {
    // When the mouse cursor has been left current window, the original select of the list-box 
    // to reset LB_ERR.
    m_OriginalSel = LB_ERR;
    m_isEnter = FALSE;	
    // The tool-tip window is hidden
    m_tipWnd.ShowWindow(SW_HIDE);
  }
  else if (message == WM_CAPTURECHANGED)
  {	// Ignore the mouse capture changed...
    return 1;
  }
  else if (message == WM_LBUTTONDOWN || 
       message == WM_LBUTTONDBLCLK)
  {
    WORD xPos, yPos;
    xPos = LOWORD(lParam);
    yPos = HIWORD(lParam);
    CPoint point(xPos, yPos); 
    CRect rcClient;
    ::GetClientRect(hWnd, &rcClient);
    if (rcClient.PtInRect(point))
    {
      BOOL bOutside;
      int curSel = pList->ItemFromPoint(point, bOutside);
      if (!bOutside && curSel != LB_ERR)
      {
        for (int i = 0; i < pThisCombo->m_arrDisabledItems.GetSize(); ++i)
        {
          if ((UINT)curSel == pThisCombo->m_arrDisabledItems.GetAt(i))
          {
            return TRUE; // Don't process the message
          }
        }
      }
    }
  }
  else if (message == WM_SHOWWINDOW)
  {
    if (wParam == TRUE) // Show up!
    {
      pThisCombo->AlignListBoxWithCombo();
    }
  }

  // Get previous window procedure
  WNDPROC oldListWndProc = nullptr;
  m_mapWndProc.Lookup(hWnd, oldListWndProc);
  // Call previous window procedure
  return ::CallWindowProc(oldListWndProc, hWnd, message, wParam, lParam);
}

LRESULT CALLBACK 
CAi_CmbBx::HookEditboxWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{	
  CWnd* pWnd = CWnd::FromHandle(hWnd);
  
  if (message == WM_MOUSEMOVE)
  {
    // However, the edit-box may be captured thus we must know when the mouse cursor
    // out of the area of the edit-box, and send 'WM_MOUSELEAVE' notification.
    CPoint point;
    point.x = LOWORD(lParam);
    point.y = HIWORD(lParam);
    CRect rcBounds;
    ::GetClientRect(hWnd, &rcBounds);
    if (rcBounds.PtInRect(point) == FALSE)
    {
      ::SendMessage(hWnd, WM_MOUSELEAVE, wParam, lParam);
    }

    if (!m_isEnter)
    {// Tracking the mouse event which are hovering and leaving.
      OnTrackMouseEvent(hWnd, TME_HOVER|TME_LEAVE);
      m_isEnter = TRUE;
    }
  }
  else if (message == WM_MOUSEHOVER)
  {
    HandleEditboxMouseOver(pWnd);
  }
  else if (message == WM_MOUSELEAVE)
  {
    // When the mouse cursor has been left current window, the original select of the list-box 
    // to reset LB_ERR.
    m_OriginalSel = LB_ERR;
    m_isEnter = FALSE;
    // The tool-tip window is hidden
    m_tipWnd.ShowWindow(SW_HIDE);
  }

  // Get previous window procedure
  WNDPROC oldEditWndProc = nullptr;
  m_mapWndProc.Lookup(hWnd, oldEditWndProc);
  // Call previous window procedure
  return ::CallWindowProc(oldEditWndProc, hWnd, message, wParam, lParam);
}

// The callback function that hooks up and processes messages
// send to the tool-tip window.
LRESULT CALLBACK CAi_CmbBx::HookTipWndProc(HWND hWnd, UINT message, WPARAM wp, LPARAM lp)
{
  if (message == WM_PAINT)
  {
    CAi_CmbBx::HandleOnPaint();
  }
  else if (message == WM_TIMER)
  {
    CAi_CmbBx::HandleOnTimer(wp);
  }
  else if (message == WM_SHOWWINDOW &&
    wp == FALSE)
  {
     // Release the mouse capture from a window 
     // mean the mouse will reenter the(or other) window at next time
    ReleaseCapture();
  }

  // Get previous window procedure
  WNDPROC oldTipWndProc = nullptr;
  m_mapWndProc.Lookup(hWnd, oldTipWndProc);

  // Call previous window procedure
  return ::CallWindowProc(oldTipWndProc, hWnd, message, wp, lp);
}
#include <shlobj.h>
#include <shellapi.h>
//#include <wfext.h>
void CAi_CmbBx::HandleListboxMouseMove(CListBox* pList, UINT uFlag, CPoint point)
{	
  BOOL bOutside;
  int curSel = pList->ItemFromPoint(point, bOutside);
  if (bOutside || curSel == LB_ERR ||
    curSel < 0 || curSel >= pList->GetCount())
    return; // the point is outside the client area of the item in the list-box

  if (m_OriginalSel == curSel)
  {// If The current option is equal to original that get outta here
    return; 
  }

  m_OriginalSel = curSel;

  CDC* pDC = m_tipWnd.GetDC();
  CFont* pOldFont = pDC->SelectObject(&m_font);
  CString strText;

  BOOL bForceAppear = FALSE;
  CAi_CmbBx* pThisCombo = nullptr;
  m_mapCombo.Lookup(pList->m_hWnd, pThisCombo);
  if (pThisCombo)
  {
    if (pThisCombo->m_mapTipTexts.Lookup(curSel, strText))
      bForceAppear = TRUE;
  }
  if (strText.IsEmpty())
    pList->GetText(curSel, strText);

  CRect rcBounds;
  pList->GetItemRect(curSel, &rcBounds);
  CRect rcDraw = rcBounds;
  pDC->DrawText(strText, &rcDraw, DT_CALCRECT|DT_SINGLELINE|DT_CENTER|DT_VCENTER|DT_NOPREFIX);
  pDC->SelectObject(pOldFont);
  ::ReleaseDC(m_tipWnd.GetSafeHwnd(), pDC->m_hDC);

  if (!bForceAppear && 
    (rcDraw.right <= rcBounds.right))
  {// However don't show the tool-tip window and get outta here, 
   // unless the tool-tip window's width is greater than 
   // the option's width in the list-box.
    m_tipWnd.ShowWindow(SW_HIDE);
    return;
  }

  rcDraw.InflateRect(2,2);
  pList->ClientToScreen(&rcDraw);
  m_tipWnd.SetWindowText(strText);

  m_tipWnd.ShowWindow(SW_HIDE); // Last showing window is hidden

  // Capture current window that prevent mouse cursor leave current window,
  // The cause is the mouse cursor will be over the tool-tip window(see below).
  if (GetCapture() != pList)
    ::SetCapture(pList->m_hWnd);

  // Delayed to show the tool-tip window by given position
  m_tipWnd.SetWindowPos(&CWnd::wndTopMost, 
    rcDraw.left, rcDraw.top, rcDraw.Width(), rcDraw.Height(), SWP_NOACTIVATE | SWP_SHOWWINDOW);
  m_tipWnd.SetTimer(1, m_nDelayTime, nullptr);
}

void CAi_CmbBx::HandleEditboxMouseOver(CWnd* pWnd)
{
  CRect rcBounds;
  pWnd->GetClientRect(&rcBounds);

  CString strText;
  pWnd->GetWindowText(strText);

  CDC* pDC = m_tipWnd.GetDC();
  CFont* pOldFont = pDC->SelectObject(&m_font);
  CRect rcDraw = rcBounds;
  pDC->DrawText(strText, &rcDraw, DT_CALCRECT|DT_SINGLELINE|DT_CENTER|DT_VCENTER|DT_NOPREFIX);
  pDC->SelectObject(pOldFont);
  ::ReleaseDC(m_tipWnd.GetSafeHwnd(), pDC->m_hDC);

  if (rcDraw.right <= rcBounds.right)
  {// However don't show the tool-tip window and get outta here, 
   // unless the tool-tip window's width is greater than 
   // the option's width in the list-box.
    m_tipWnd.ShowWindow(SW_HIDE);
  }
  else
  {
    rcDraw.bottom = rcBounds.bottom;
    rcDraw.InflateRect(2,2);
    pWnd->ClientToScreen(&rcDraw);
    m_tipWnd.SetWindowText(strText);
    m_tipWnd.ShowWindow(SW_HIDE); // Last showing window is hidden
    
    // Capture current window that prevent mouse cursor leave current window,
    // The cause is the mouse cursor will be over the tool-tip window(see below).
    ::SetCapture(pWnd->m_hWnd);

    // Delayed to show the tool-tip window by given position
    m_tipWnd.SetWindowPos(&CWnd::wndTopMost, 
      rcDraw.left, rcDraw.top, rcDraw.Width(), rcDraw.Height(), SWP_NOACTIVATE | SWP_SHOWWINDOW);
    m_tipWnd.SetTimer(1, m_nDelayTime, nullptr);
  }
}

// Handler for repaint tool-tip window
void CAi_CmbBx::HandleOnPaint()
{
  CPaintDC dc(&m_tipWnd);
  CRect rc;
  m_tipWnd.GetClientRect(&rc);
  CBrush brush(GetSysColor(COLOR_INFOBK));
  dc.FillRect(&rc, &brush);

  CBrush border(RGB(0,0,0));
  dc.FrameRect(&rc, &border);

  CString strText;
  m_tipWnd.GetWindowText(strText);
  dc.SetBkMode(TRANSPARENT);
  CFont* pOldFont = dc.SelectObject(&m_font);
  dc.DrawText(strText, &rc, DT_SINGLELINE|DT_CENTER|DT_VCENTER|DT_NOPREFIX);

  dc.SelectObject(pOldFont);
}

// Handler for each interval specified when we used SetTimer to install a timer
// for tool-tip window.
void CAi_CmbBx::HandleOnTimer(UINT nIDEvent)
{
  m_tipWnd.KillTimer(nIDEvent);
  m_tipWnd.ShowWindow(SW_HIDE);
}

BOOL CAi_CmbBx::OnTrackMouseEvent(HWND hWnd, DWORD dwFlags)
{
  TRACKMOUSEEVENT tme;
  tme.cbSize = sizeof(TRACKMOUSEEVENT);
  tme.dwFlags = dwFlags;
  tme.dwHoverTime = HOVER_DEFAULT;
  tme.hwndTrack = hWnd;
  return ::_TrackMouseEvent(&tme);
}

void CAi_CmbBx::AlignListBoxWithCombo()
{
  if (m_cbi.hwndList == nullptr ||
    !::IsWindow(m_cbi.hwndList))
    return;

  CListBox* pList = (CListBox*)CWnd::FromHandle(m_cbi.hwndList);
  CRect l_rcCombo;
  GetClientRect(&l_rcCombo);
  CRect l_rcList;
  pList->GetClientRect(&l_rcList);
  int dropWidth = GetDroppedWidth();
  //int listWidth = l_rcList.Width();
  int xOffset = 0;
  switch (m_alignStyle)
  {
  case 0:
    xOffset = 0;
    break;
  case 1:
    xOffset = dropWidth - l_rcCombo.Width();
    break;
  case 2:
    xOffset = (dropWidth - l_rcCombo.Width()) / 2;
    break;
  default:
    xOffset = 0;
    break;
  }
  pList->ClientToScreen(&l_rcList);
  pList->SetWindowPos(nullptr, l_rcList.left - xOffset, l_rcList.top, 0, 0, SWP_NOSIZE);
}

void CAi_CmbBx::PreSubclassWindow() 
{
  CComboBox::PreSubclassWindow();

  // If the combo-box isn't dynamically subclassed that should get rid of 
  // the codes as follows and the codes should be placed in the OnCreate function.
  if (m_isEnableTool == TRUE)
  {
    InstallHookForListboxAndEditbox();
  }
}

void CAi_CmbBx::OnMouseMove(UINT nFlags, CPoint point) 
{
  // However, the combobox may be captured thus we must know when the mouse cursor
  // out of the area of the combobox, and send 'WM_MOUSELEAVE' notification.
  CRect rcBounds;
  GetClientRect(&rcBounds);
  if (rcBounds.PtInRect(point) == FALSE)
  {
    SendMessage(WM_MOUSELEAVE);
  }
  
  // Here is tracking mouse event such as 'HOVER', 'LEAVE'.
  if (m_isEnter == FALSE)
  {
    OnTrackMouseEvent(m_hWnd, TME_HOVER|TME_LEAVE);
    m_isEnter = TRUE;
  }

  CComboBox::OnMouseMove(nFlags, point);
}

LRESULT CAi_CmbBx::OnMouseOver(WPARAM wp, LPARAM lp) 
{
  if (m_isEnableTool == FALSE)
    return 0;

  // The below code is obsolete, 
  // Ideally, the below code is to block mouse over Editbox for DropDown style.
  // Somehow, The m_cbi.hwndItem is also non-zero for a DropList under Vista common controls,
  // or the project is compiled under Unicode version?.
  
  // Actually, if The combo is DropDown style, it has owns Editbox, 
  // and the event function won't be fired when mouse hover on that Editbox.
  // Thus, the below code should be useless, I decided to remove the below code.
  /* OBSOLETE..
  if (::IsWindow(m_cbi.hwndItem))
    return 0; // If the edit-box has exist that gotta here
  */

  CRect rcBounds;
  GetClientRect(&rcBounds);
  rcBounds.DeflateRect(2, 2);
  rcBounds.right -= m_rcButton.Width();

  CString strText;
  BOOL bForceAppear = FALSE;
  if (m_mapTipTexts.Lookup(GetCurSel(), strText))
    bForceAppear = TRUE;
  if (strText.IsEmpty())
    GetWindowText(strText);

  CDC* pDC = m_tipWnd.GetDC();
  CFont* pOldFont = pDC->SelectObject(&m_font);
  CRect rcDraw = rcBounds;
  pDC->DrawText(strText, &rcDraw, DT_CALCRECT|DT_SINGLELINE|DT_CENTER|DT_VCENTER|DT_NOPREFIX);
  pDC->SelectObject(pOldFont);
  ::ReleaseDC(m_tipWnd.GetSafeHwnd(), pDC->m_hDC);

  if (!bForceAppear &&
    rcDraw.right <= rcBounds.right)
  {// However don't show the tool-tip window and get outta here, 
   // unless the tool-tip window's width is greater than 
   // the option's width in the list-box.
    m_tipWnd.ShowWindow(SW_HIDE);
    return 0;
  }

  rcDraw.bottom = rcBounds.bottom;
  rcDraw.InflateRect(2,2);
  ClientToScreen(&rcDraw);
  m_tipWnd.SetWindowText(strText);
  m_tipWnd.ShowWindow(SW_HIDE); // Last showing window is hidden

  // Capture current window that prevent mouse cursor leave current window,
  // The cause is the mouse cursor will be over the tool-tip window(see below).
  SetCapture();

  // Delayed to show the tool-tip window by given position
  m_tipWnd.SetWindowPos(&CWnd::wndTopMost, 
    rcDraw.left, rcDraw.top, rcDraw.Width(), rcDraw.Height(), SWP_NOACTIVATE | SWP_SHOWWINDOW);
  m_tipWnd.SetTimer(1, m_nDelayTime, nullptr);

  return 0;
}

LRESULT CAi_CmbBx::OnMouseLeave(WPARAM wp, LPARAM lp)
{
  // The mouse cursor has leave current window
  m_isEnter = FALSE;
  // The tool-tip window is hidden
  m_tipWnd.ShowWindow(SW_HIDE);

  return 0;
}

void CAi_CmbBx::OnLButtonDown(UINT nFlags, CPoint point) 
{
  if (m_tipWnd.IsWindowVisible())
    ReleaseCapture();
  
  CComboBox::OnLButtonDown(nFlags, point);
}

void CAi_CmbBx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
  CRect rc;
  GetWindowRect(&rc);
  rc.bottom -= 15;
  ASSERT(lpDrawItemStruct->CtlType == ODT_COMBOBOX);
  CString strText;
  // If there are no list box items, skip this message. 
  if ((int)lpDrawItemStruct->itemID < 0 || (int)lpDrawItemStruct->itemID > (GetCount()-1) )
    return;
  this->GetLBText(lpDrawItemStruct->itemID, strText);
  CDC dc;

  dc.Attach(lpDrawItemStruct->hDC);

  // Save these value to restore them when done drawing.
  COLORREF crOldTextColor = dc.GetTextColor();
  COLORREF crOldBkColor = dc.GetBkColor();

  // The item maybe disabled...
  BOOL bDisabledItem = FALSE;
  for (int i = 0; i < m_arrDisabledItems.GetSize(); ++i)
  {
    if (lpDrawItemStruct->itemID == m_arrDisabledItems.GetAt(i))
    {
      bDisabledItem = TRUE;
      break;
    }
  }   

  // If this item is selected, set the background color 
  // and the text color to appropriate values. Erase
  // the rect by filling it with the background color.
  if ((lpDrawItemStruct->itemAction | ODA_SELECT) &&
    (lpDrawItemStruct->itemState  & ODS_SELECTED) && bDisabledItem == FALSE)
  {
    dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
    dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
    dc.FillSolidRect(&lpDrawItemStruct->rcItem, ::GetSysColor(COLOR_HIGHLIGHT));
  }
  else
  {
    dc.FillSolidRect(&lpDrawItemStruct->rcItem, crOldBkColor);
  }

  if (lpDrawItemStruct->itemState & ODS_DISABLED)
  {
    dc.SetTextColor(::GetSysColor(COLOR_GRAYTEXT));
  }

  // Draw the text.
  dc.DrawText(
    strText,
    &lpDrawItemStruct->rcItem,
    DT_LEFT|DT_SINGLELINE|DT_VCENTER|DT_NOPREFIX);

  // Reset the background color and the text color back to their
  // original values.
  dc.SetTextColor(crOldTextColor);
  dc.SetBkColor(crOldBkColor);

  dc.Detach();
}

void CAi_CmbBx::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
//	lpMeasureItemStruct->itemHeight = 63;
}

BOOL CAi_CmbBx::PreCreateWindow(CREATESTRUCT& cs) 
{
  // TODO: Add your specialized code here and/or call the base class
  
  return CComboBox::PreCreateWindow(cs);
}

LRESULT CAi_CmbBx::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
  return CComboBox::WindowProc(message, wParam, lParam);
}

void CAi_CmbBx::OnDestroy()
{
  CComboBox::OnDestroy();
}

void CAi_CmbBx::OnDropdown() 
{
}

BOOL CAi_CmbBx::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
  BOOL ret = CComboBox::OnMouseWheel(nFlags, zDelta, pt);

  BOOL bSelBack = zDelta >= 0 ? TRUE : FALSE;
  int nCurSel = GetCurSel();
  if (nCurSel != CB_ERR)
  {
    for (int i = 0; i < m_arrDisabledItems.GetSize(); ++i)
    {
      int nItem = m_arrDisabledItems.GetAt(i);
      if (nItem == nCurSel)
      {
        if (bSelBack == TRUE)
          --nCurSel;
        else
          ++nCurSel;
        
        SetCurSel(nCurSel);
      }
    }
  }

  return ret;
}
