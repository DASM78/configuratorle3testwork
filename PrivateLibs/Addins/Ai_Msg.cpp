#include "stdafx.h"
#include "stdarg.h"
#include "Ai_Str.h"
#include "Ai_Msg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CString CAi_Msg::m_strMadeMessage;

CAi_Msg::CAi_Msg()
{
}

int CAi_Msg::AfxMsgBox(UINT nType, CString strMsg, int nCntParams, ...)
{
  va_list arg;
  MSG_PARAM msgPar;
  CString strTmp;

  va_start(arg, nCntParams);
  while (nCntParams)
  {
    msgPar = va_arg(arg, MSG_PARAM);
    nCntParams--;

    switch (msgPar.nT)
    {
      case MSG_PARAM::msgtNumber: 
        InsertParam(strMsg, _T("%d"), CAi_Str::ToString(msgPar.nP)); 
        break;
      case MSG_PARAM::msgtString: 
        InsertParam(strMsg, _T("%s"), msgPar.strP); 
        break;
      default: AfxMessageBox(_T("Error [AfxMsgBox]!"), MB_ICONERROR); break;
    };
  }

  m_strMadeMessage = strMsg;
  return AfxMessageBox(strMsg, nType);
}

void CAi_Msg::InsertParam(CString& strMsg, CString strChange, CString strIns)
{
  int nIdx;

  if ((nIdx = strMsg.Find(strChange)) == -1)
    return;
  strMsg.Delete(nIdx, 2);
  strMsg.Insert(nIdx, strIns);
}