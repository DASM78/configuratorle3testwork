#pragma once

#include "AddinsDef.h"
#include "Ai_Defines.h"
#include <vector>

// * Colomn
//          Width style
#define CS_WIDTH_FREE                      BIN64_1(00000001) // without width (= 0)
#define CS_WIDTH_SET_BY_TEXT          BIN64_1(00000010) // by length column title
#define CS_WIDTH_FIX                        BIN64_1(00001000) // width by integer, and will be constant
#define CS_WIDTH_LIMIT                     1000
//          Match width
#define C_MATCH_WIDTH                      _T("-cmw_ThisColumn")

// * Node style
#define NS_UNWRAP                                     BIN64_1(00000001) // ������ ����� ��������������/������������� [+]/[-]
#define NS_UNWRAP_ONLY_SETED                  BIN64_1(00000010) // ���� ����� ���������� ������ ���� ��������, �� ��� �� ����� ������������
#define NS_UNWRAP_ONLY_SETED_WITH_GAPS BIN64_1(00000100) // ���� ����� ���������� ������ ���� ��������, �� ��� ����� ������������
#define NS_MSG_BEFORE_COMMON_INIT          BIN64_1(00010000) // ����� ���������� ���������-������ ����� �������� ���� �������� ��������� ������ ��������, ������� �������� ���� ����������� bind ctrl
#define NS_USE_COMN_SIGNIFICANT_CELL       BIN64_1(00100000) // ������ ����� �������� ������ (������, ���������� ������� ����� �� ����� ������, ����� ������ �����������������, ��� ������ �, ��������, �� ���������� �� �����)
#define NS_NO_CHANGE_UNWRAP_NAME_BY_BIND_SEL BIN64_1(10000000) // �� ������ ����� �����. unwrap ������ ���� ������ ��������� �������� ����� Ctrl � � ��� �������� ��������
#define NS_DBLCLK_DO_ALWAYS                      BIN64_2(000000010, 000000000)

// Cell style
#define SS_NO_CHG_NODE_CELL_BY_CHG_CHILD_BIND_CELL BIN64_2(000000001, 000000000) // �� ������ ����� �����. ������ ���� ������ ��������� �������� ����� Ctrl � � ��� �������� ��������

// * Common style
#define CMNS_DISABLE_ROW_IF_CHKBX_OFF      BIN64_1(00000001) // ��� ���������� ��������� enable/disable ������ ����� ���������� ��������
// ������� UpdateView() �� �������� �� ��������� LVN_ITEMCHANGED

// * Child style
#define CHS_SET_CTRL_BY_NODE_DEF               BIN64_1(00000010) // ���������� �������� ������ � Ctrl-��� �� ��������� �������� ������ ������������� Ctrl-�
#define CHS_SET_CTRL_BY_NODE_DEF_OR_CURR BIN64_1(00000100) // ���������� �������� ������ � Ctrl-��� �� ����������� �������� � ������ ������������� Ctrl-�
#define CHS_CONST_CLEAR_CHILD                    BIN64_1(00010000) // ����� ��� ���. ��. ������ �������� ������ �������
#define CHS_CONST_MOVE_CHILD                     BIN64_1(00100000) // ����� ��� ���. ��. ������� ������ ����������

// * Child inserted flags
#define CHIF_TO_EMPTY_TAIL                BIN64_1(00000001)
#define CHIF_FILL_IN_GAPS                   BIN64_1(00000010)
#define CHIF_COUPLE_ROWS                  BIN64_1(00000100)
#define CHIF_TO_CURR_IF_EMPTY          BIN64_1(00001000)
#define CHIF_SRVS_INFO_ADD                BIN64_1(00010000)
#define CHIF_SRVC_INFO_UPDATE          BIN64_1(00100000)
#define CHIF_MSG_IF_NODE_FULL          BIN64_1(01000000)
// Return value after insert child
#define RCHI_SUCCESS                         0
#define RCHI_LIST_FULL                        -2
// 
#define CHIFS_NO_WRITE_TO_CELL _T("chifs_NoWriteToCell")

// * Clear rows flag
#define CRF_ALL_CELLS                     BIN64_1(00000000) // doesnt work
#define CRF_BY_INIT_CHAIN              BIN64_1(00000001)
#define CRF_IGNORE_DISABLE          BIN64_1(00000100)
#define CRF_IGNORE_BOOL_OF_SETED BIN64_1(00001000) // ignore < bool bSeted > member
#define CRF_GET_ALL_ROWS   BIN64_1(00010000) // << int nRow_inView >> must be '-1'

// * Move child flag
      // nAdvFlag
#define MCHF_USE_ENUM_LIST            4 // what move
#define MCHF_USE_INVERS_ENUM_LIST 5 // what stay
      // nCopyFlag
#define MCHF_COPY_CELL_TEXT    BIN64_1(00000000)
#define MCHF_COPY_ROW_INFO    BIN64_1(00000001) // copy all without vector<CELL> cells
#define MCHF_COPY_CELL_INFO    BIN64_1(00000010)
#define MCHF_COPY_IGNORE_DISABLE    BIN64_1(00000100)
#define MCHF_COPY_CALL_OUTER_MOVE_FUNC    BIN64_1(00001000) // �������� ��� ��� ������� m_pOuterMoveFunc

// * Get result filter flag (GET_BY_FILTER::nFlag1)
#define GRFF_GET_ALL                               1
#define GRFF_GET_CHILDREN                       2 
#define GRFF_GET_NODE_INDEXES               3 // get nRow_inList index
#define GRFF_GET_NODES_CHILDREN_INDEXES 4 // get nRow_inList index; spec Node index is seted in nFlag2
//          get result filter flag (GET_BY_FILTER::nFlag2) (no effect on node)
#define GRFF_SETED_IN_COMM_SIGN_CELLS 6 // nRow_inList index of Node
//          get result filter flag (GET_BY_FILTER::nFlag3) (no effect on node)
/* ���� 
      nFlag1 == GRFF_GET_NODES_CHILDREN_INDEXES
      ���
      nFlag1 == GRFF_GET_CHILDREN
      �� � Flag3 ����� ������� ��� � ����� ������, ������� ����� ���������������
�� ������� � ��� ������ (���� ������ �����, �� ������� ����� ������ ������������� �� ����� 
����� ������ � Flag3 ������ ���� ���: nCell +1*/

// * Colors of ListCtrlEx (LCE)
#define COLOR_LCE_WHITE RGB(254, 254, 254)
#define COLOR_LCE_BLACK RGB(0, 0, 0)
#define COLOR_LCE_BLACK2 RGB(80, 80, 80)
#define COLOR_LCE_DARK_GREEN RGB(1, 142, 14)
#define COLOR_LCE_LIGHT_GREEN RGB(212, 255, 223)
#define COLOR_LCE_YELLOW RGB(255, 255, 153)
#define COLOR_LCE_LIGHT_YELLOW RGB(255, 255, 190)
#define COLOR_LCE_LIGHT_LIGHT_YELLOW RGB(255, 255, 220)
#define COLOR_LCE_RED RGB(255, 20, 20)
#define COLOR_LCE_LIGHT_RED RGB(255, 180, 180)
#define COLOR_LCE_RED_YELLOW RGB(255, 233, 153)
#define COLOR_LCE_RED_YELLOW2 RGB(255, 243, 173)
#define COLOR_LCE_GRAY_1 RGB(109, 109, 109)
#define COLOR_LCE_GRAY RGB(150, 150, 150)
#define COLOR_LCE_LIGHT_GRAY RGB(230, 230, 230)
#define COLOR_LCE_LIGHT_BLUE RGB(214, 255, 255) // lighter then COLOR_LCE_LIGHT_...2
#define COLOR_LCE_LIGHT_BLUE2 RGB(160, 255, 255)

// * Bind info mark
#define DIFF_NODE_CHLDR_CELL_MARKS _T("< ~~~ >")

enum ImageListType
{
  iltNone = -10,
  iltOnEnable = 0,
  iltOnDisable,
  iltOnAdvEnable,
  iltOnAdvDisable,
  iltOffEnable,
  iltOffDisable,
  iltOffAdvEnable,
  iltOffAdvDisable
};

struct ADDINS_API IMAGE_LIST
{
  // For bitmap
  IMAGE_LIST(int resBmpID, int name, COLORREF clrMask, ImageListType ilt = iltNone)
    : nResourceBmpID(resBmpID)
    , nResourceIconID(-1)
    , nName(name)
    , nMask(clrMask)
    , imgType(ilt)
  {};
  // For icons
  IMAGE_LIST(int resIconID, int nIdx = -1)
    : nResourceIconID(resIconID)
    , nResourceBmpID(-1)
    , nName(nIdx)
    , nMask(RGB(255, 255, 255))
    , imgType(iltNone)
  {};
  int nResourceBmpID;
  int nResourceIconID;
  int nName;
  COLORREF nMask;
  ImageListType imgType;
};

// ��������� �������� ���������� ��� �������� ������ �� �������

struct ADDINS_API GET_BY_FILTER
{
  GET_BY_FILTER()
    : nFlag1(GRFF_GET_ALL)
    , nFlag2(GRFF_SETED_IN_COMM_SIGN_CELLS)
    , nFlag3(0)
    , bIgnoreNodes(false)
  {}

  int nFlag1;
  int nFlag2;
  int nFlag3;
  bool bIgnoreNodes;
};

// ��������� ��� ������ ��� �������� ������ �� �������, �.�. ����������� ���� ����������� ������� (������) �������� �������

struct ADDINS_API RESULT_GET_LIST
{
  RESULT_GET_LIST()
    : bNode(false)
    , nIdx_inList(-1)
    , nIdx_inView(-1)
    , nNodeIdx_inList(-1)
    , nNodeIdx_inView(-1)
  {}

  RESULT_GET_LIST& operator=(const RESULT_GET_LIST& ob)
  {
    if (this == &ob)
      return *this;

    bNode = ob.bNode;
    nIdx_inList = ob.nIdx_inList;
    nIdx_inView = ob.nIdx_inView;
    nNodeIdx_inView = ob.nNodeIdx_inView;
    nNodeIdx_inList = ob.nNodeIdx_inList;
  
    int nCnt;
  
    nCnt = (int)ob.textOfCellsOfRow.size();
    textOfCellsOfRow.clear();
    for (int i = 0; i < nCnt; ++i)
      textOfCellsOfRow.push_back(ob.textOfCellsOfRow[i]);

    nCnt = (int)ob.textOfServiceInfoOfRow.size();
    textOfServiceInfoOfRow.clear();
    for (int i = 0; i < nCnt; ++i)
      textOfServiceInfoOfRow.push_back(ob.textOfServiceInfoOfRow[i]);
 
    return *this;
  }

  bool bNode;
  int nIdx_inList;
  int nIdx_inView;
  int nNodeIdx_inView;
  int nNodeIdx_inList;
  std::vector<CString> textOfCellsOfRow;
  std::vector<CString> textOfServiceInfoOfRow;
};

// ��������� �������� �������� ��� �������������� Ctrls

struct ADDINS_API BIND_CTRL_TO_CELL
{
  BIND_CTRL_TO_CELL()
    : bDisableBind(false)
    , nCell(-1)
    , pstrExchange(nullptr)
    , pDialog(nullptr)
    , pEdit(nullptr)
    , pIP(nullptr)
    , nSelRowByDefault(-1)
    , pCmbBox(nullptr)
    , nID(-1)
  {}

  void Clear()
  {
    bDisableBind = false;
    nCell = -1;
    strByDefault = _T("");
    bSetDefaultForce = true;
  
    pstrExchange = nullptr;
    pDialog = nullptr;

    pEdit = nullptr;
  
    pIP = nullptr;

    nSelRowByDefault = -1;
    pCmbBox = nullptr;
    nID = -1;
  }

  BIND_CTRL_TO_CELL& operator=(const BIND_CTRL_TO_CELL& ob)
  {
    bDisableBind = ob.bDisableBind;
    nCell = ob.nCell;
    pstrExchange = ob.pstrExchange;
    strByDefault = ob.strByDefault;
    bSetDefaultForce = ob.bSetDefaultForce;
    nSelRowByDefault = ob.nSelRowByDefault;
    pCmbBox = ob.pCmbBox;
    pDialog = ob.pDialog;
    pEdit = ob.pEdit;
    pIP = ob.pIP;
    nID = ob.nID;
 
    return *this;
  }

  bool bDisableBind; // if cell will be desable
  int nCell; // ctrl will be binded to this cell
  CString strByDefault; // default string in cell for new addition child (item)
  bool bSetDefaultForce;
  
  CString* pstrExchange; // link string for CDialog binding ctrl
  CDialog* pDialog; // pointer to CDialog binding ctrl
  
  CEdit* pEdit; // pointer to CEdit binding ctrl
  
  CIPAddressCtrl* pIP; // pointer to CIPAddressCtrl binding ctrl
  
  int nSelRowByDefault; // default selected index in CComboBox binding ctrl
  CComboBox* pCmbBox; // pointer to CComboBox binding ctrl
  int nID; // id for message which send like SendMessage to outer before new mean will be set in the cell after a work in the ctrl
};

struct ADDINS_API BIND_TO_CELL
{
  BIND_TO_CELL& operator=(const BIND_TO_CELL& ob)
  {
    if (this == &ob)
      return *this;

    int nCnt = (int)ob.ctrlToCell.size();

    ctrlToCell.clear();
    for (int i = 0; i < nCnt; ++i)
      ctrlToCell.push_back(ob.ctrlToCell[i]);//bind);

    return *this;
  }

  std::vector<BIND_CTRL_TO_CELL> ctrlToCell;
};

// Bind ctrl
enum BindCtrlType
{
  bctError = -1,
  bctNone = 0,
  bctComboBox,
  bctDialog,
  bctEdit,
  bctIP,
  bctCheck,
};

enum CellTextStyle
{
  ctsNone,
  ctsPrevious,
  ctsBold,
  ctsItalic,
  ctsItalicBold,
  ctsStrikeOut
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Compare and exit from function
// ..._AST_... - ASSERT message before exit

// For exit from function look as: void F()
#define RETURN_BY_TRUE(b) { if ((b) == true) return; }
#define RETURN_AST_BY_TRUE(b) { if ((b) == true) { ASSERT(!(b)); return; } }

// For exit from function look as: int F()
#define RETURN_1_BY_TRUE(b) { if ((b) == true) return -1; }
#define RETURN_AST_1_BY_TRUE(b) { if ((b) == true) { ASSERT(!(b)); return -1; } }

// For exit from function look as: bool F()
#define RETURN_FALSE_BY_TRUE(b) { if ((b) == true) return false; }
#define RETURN_AST_FALSE_BY_TRUE(b) { if ((b) == true) { ASSERT(!(b)); return false; } }

// For exit from function look as: CString F()
#define RETURN_EMPTS_BY_TRUE(b) { if ((b) == true) return _T(""); }
#define RETURN_AST_EMPTS_BY_TRUE(b) { if ((b) == true) { ASSERT(!(b)); return _T(""); } }

#define RETURN_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_BY_TRUE(i < minLimit || i > maxLimit)
#define RETURN_AST_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_AST_BY_TRUE(i < minLimit || i > maxLimit)

#define RETURN_1_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_1_BY_TRUE(i < minLimit || i > maxLimit)
#define RETURN_AST_1_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_AST_1_BY_TRUE(i < minLimit || i > maxLimit)

#define RETURN_FALSE_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_FALSE_BY_TRUE(i < minLimit || i > maxLimit)
#define RETURN_AST_FALSE_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_AST_FALSE_BY_TRUE(i < minLimit || i > maxLimit)

#define RETURN_EMPTS_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_EMPTS_BY_TRUE(i < minLimit || i > maxLimit)
#define RETURN_AST_EMPTS_BY_OUT_OF_RANGE(i, minLimit, maxLimit) RETURN_AST_EMPTS_BY_TRUE(i < minLimit || i > maxLimit)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////