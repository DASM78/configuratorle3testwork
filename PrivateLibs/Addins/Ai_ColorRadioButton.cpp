#include "stdafx.h"
#include "Ai_ColorRadioButton.h"

CAi_ColorRadioButton::CAi_ColorRadioButton()
{
  m_crBkColor = ::GetSysColor(COLOR_3DFACE); // Initializing the Background Color to the system face color.
  m_brBkgnd.CreateSolidBrush(m_crBkColor); // Create the Brush Color for the Background.
}

CAi_ColorRadioButton::~CAi_ColorRadioButton()
{
}

BEGIN_MESSAGE_MAP(CAi_ColorRadioButton, CButton)
  //{{AFX_MSG_MAP(CAi_ColorRadioButton)
  ON_WM_CTLCOLOR_REFLECT()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

HBRUSH CAi_ColorRadioButton::CtlColor(CDC* pDC, UINT nCtlColor)
{
  HBRUSH hbr;
  hbr = (HBRUSH)m_brBkgnd; // Passing a Handle to the Brush
  pDC->SetBkColor(m_crBkColor); // Setting the Color of the Text Background to the one passed by the Dialog

  if (nCtlColor)       // To get rid of compiler warning
    nCtlColor += 0;

  return hbr;
}

void CAi_ColorRadioButton::SetBkColor(COLORREF crColor)
{
  m_crBkColor = crColor; // Passing the value passed by the dialog to the member varaible for Backgound Color
  m_brBkgnd.DeleteObject(); // Deleting any Previous Brush Colors if any existed.
  m_brBkgnd.CreateSolidBrush(crColor); // Creating the Brush Color For the Static Text Background
  RedrawWindow();
}