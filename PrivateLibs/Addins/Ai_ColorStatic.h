#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_ColorStatic : public CStatic
{
public:
  void SetTxColor(COLORREF crColor); // This Function is to set the Color for the Text.
  void SetBkColor(COLORREF crColor); // This Function is to set the BackGround Color for the Text.

  CAi_ColorStatic();
  ~CAi_ColorStatic();

  DECLARE_MESSAGE_MAP()

protected:

  CBrush m_brBkgnd; // Holds Brush Color for the Static Text
  COLORREF m_crBkColor; // Holds the Background Color for the Text
  COLORREF m_crTextColor; // Holds the Color for the Text

  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
};
