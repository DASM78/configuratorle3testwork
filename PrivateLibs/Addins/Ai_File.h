#pragma once

#define SWITCH_ON_FILE_VERSION_CODE

#include "afxshellmanager.h"
#include "afxwinappex.h"
#include <vector>
#include "AddinsDef.h"

#define FC_NEW_LINE _T("\r\n")

enum FileCodingFormat
{
  fcfByDef, // As in application
  fcfUnicode,
  fcfASCII
};

struct ADDINS_API PARAMS_FROM_FILE
{
  CString strParameter;
  CString strMean;
};

struct ADDINS_API LIST_OF_FILES_IN_FOLDER
{
  CString strFilePathName; // path and name
  CString strFileName;
};
struct ADDINS_API LIST_OF_FOLDER_AND_FILES // I.e. root folder and subfolders this folder
{
  CString strPath;
  std::vector<LIST_OF_FILES_IN_FOLDER> files;
  int nCurrentIdx;
  int nParentIdx;
  int nDeep;
};

class CFileCtrlInnerUsing;

class ADDINS_API CAi_File
{
public:
  CAi_File();
  ~CAi_File();

private:
  CFileCtrlInnerUsing* m_pInner;

public:
  static bool IfPathHasNotBadSimbols(CString strPath);
  static bool IfFolderNameHasNotBadSimbols(CString strFolderName);
  static bool IfFileNameHasNotBadSimbols(CString strFileName);

// Windows folders

  static CString GetWinTmpPath();
  static CString GetWinLocalSettingsApplicationDataPath();

// Folders

  static CString GetCurrentPath();
  static CString GetAppPath();
  static CString GetAppPathName(HMODULE hModule = nullptr);

// File parh parsers

  // C:\FolderName\File.ext => File.ext
  static CString GetFileName_(CString strFilePathName);
  // C:\FolderName\File.ext => File
  static CString GetFileTitle_(CString strFilePathName);
  // C:\FolderName\File.ext => ext
  static CString GetFileExt(CString strFilePathName);
  // C:\FolderName\File.ext => C:\FolderName
  static CString GetFilePath_(CString strFilePathName);
  // C:\FolderName\File.ext => FolderName
  static CString GetFileFolder(CString strFilePathName);

// Create or open a file

  enum CreateFileMode
  {
    cfmClearIfExists,
    cfmJustOpenIfExists,
    cfmErrorIfExists
  };
  static bool CreateFile_(CString strFilePathName, CreateFileMode howCreate = cfmClearIfExists);
  static CString CreateTempFile(CString strFileName, CString* pstrWriteToFile = nullptr);

// Create a new file if it is absented

  static bool ExamineFile(CString strFilePathName);
  static CString ExamineTmpFile(CString strFileName);

// A file extension

  static void AddExtensionIfNeed(CString* pstrFileName, CString strExt);

// Delete or clear a file

  static bool DeleteFile_(CString strFilePathName);
  static bool DeleteFiles(CString strPathName);
  static bool ClearFile(CString strFilePathName);

// Create a folder

  static bool CreateFolder(CString strFolderPath, bool bShowErrorMsg = true);
  static bool ExamineFolder(CString strFolderPath);
  static bool ChoiseFolderDlg(CString* pstrFolderPath, CString strCaption, CShellManager* pShM, CWnd* pWndParent);

// Delete a folder and subfolders (and files inside)

  static bool DeleteFolder(CString strFolderPath, bool bNoIfFilesAreInside = true);

// Detect if the folder is

  static bool IfFolderExists(CString strFolderPath) { return CAi_File::IsFolderExist(strFolderPath); };
  static bool IsFolderExist(CString strFolderPath);

// Detect if the file is

  static bool IfFileExists(CString strFilePathName) { return CAi_File::IsFileExist(strFilePathName); };
  static bool IsFileExist(CString strFilePathName);

// File length

  static bool IsFileEmpty(CString strFilePathName, long* nFileLength = nullptr);
  static long GetFileLength(CString strFilePathName);

// Has file unicode coding

  static bool IsUnicodeFile(CString strFilePathName);
  static FileCodingFormat GetFileCodingFormat(CString strFilePathName);

// Remove the "Read only" file attribute

  static bool RemoveAttribute_ReadOnly(CString strFilePathName, CString strWarningMessage = _T(""));

// Get the file attributes

  struct FILE_ATTR
  {
    BY_HANDLE_FILE_INFORMATION fileInfo;
    DWORD nAttributes;
  };
  static bool GetFileAttributes_(CString strFilePathName, FILE_ATTR* pFileAttr);

// Security permissions

  enum Permission
  {
    pRead,
    pWrite,
    pReadWrite
  };
  static bool ExaminePermissions(CString strFilePath, bool bFolder, Permission per = pReadWrite, bool bShowMsgByDenied = true);
  static CString m_strPermissionErrorInfo;

// Add (or delete from) path to environment
  
  static bool AddPathToSystemEnvironment(CString strPath);
  static bool DeletePathFromSystemEnvironment(CString strPath);
  
// Copy a file to a file

  static bool CopyFileToFile(CString strSrcFilePathName, CString strDstFilePathName, CString strStopIfChars);
  static bool CopyFileToFile(CString strSrcFilePathName, CString strDstFilePathName);

// Copy a folder to a folder

  static bool CopyFolderToFolder(CString strSrcPath, CString strDstPath, HWND hwndForMsg = nullptr, CString strTitleProgressDlg = _T(""));

// Get all files in folder

  std::vector<LIST_OF_FOLDER_AND_FILES> m_listOfFolderAndFiles;
  bool GetListOfFilesInFolder(CString strFolderPath, bool bLookSubFolders = false);

// Seeks in a file

  static int GetSeekBefore(CString strFilePathName, CString strChars);
  static int GetSeekAfter(CString strFilePathName, CString strChars);

// Read file to

  static int ReadFileToBuffer(CString strFilePathName, BYTE** pBuffer, bool bZeroEnd = true);
  static int ReadFileToString(CString strFilePathName, CString* pstrBuff);
 
// Write to file

  static bool WriteBufferToFile(CString strFilePathName, BYTE *pBufferToFile, int nBuffSize, bool bClearFileBefore = true);
  static bool WriteStringToFile(CString strFilePathName, CString* pstrToFile, FileCodingFormat dstCoding = fcfByDef);

// Add a new line in a file end/begin and a line feed ("\r\n") if need

  enum InsertNewLine
  {
    inlNoNewLine,
    inlToBegin,
    inlToEnd,
    inlToBeginAndEnd
  };
  static bool AddLineToEnd(CString strFilePathName, CString strAdd, InsertNewLine newLine = inlToEnd);
  static bool AddLineToBegin(CString strFilePathName, CString strAdd, InsertNewLine newLine = inlToEnd);

// Change the block in a file by seeks

  static bool ChangeBlockInFile(CString strSrcFilePathName, CString strDstFilePathName, int nSeekStartInDstFile, int nSeekStopInDstFile, InsertNewLine newLine);

// Change the string in a file

  static bool ChangeStringInFile(CString strFilePathName, CString strOldStringInFile, CString strNewMeanOfOldString);

// Cut a file fragment

  static bool CutFragment(CString strFilePathName, int nSeekStart, int nSeekStop);
  static bool TrimRight(CString strFilePathName, CString strTrim);

// Work with sections in a file

  CString* m_pstrSectionData;
  bool SetParameterAndMeanToSection(CString strFilePathName, CString strSectStart, CString strSectStop, CString strParameter, CString strMean, CString strSepar);
  bool AddLineToSection(CString strFilePathName, CString strAdd, CString strSectStart, CString strSectStop);
  bool GetParametersFromSection(CString strFilePathName, CString strSectStart, CString strSectStop = _T(""), CString strSepar = _T("="));
  bool GetParametersFromSection(TCHAR *szData, CString strSectStart, CString strSectStop, CString strSepar);
  std::vector<PARAMS_FROM_FILE> m_paramsFromFile;
  void FreeParameters();

  // File version info

#ifdef SWITCH_ON_FILE_VERSION_CODE

  BOOL DetectFileVersionInfo(HMODULE hModule = nullptr);
  BOOL DetectFileVersionInfo(LPCTSTR lpszFileName);

  WORD GetFileVersion(int nIndex);
  WORD GetProductVersion(int nIndex);
  DWORD GetFileFlagsMask();
  DWORD GetFileFlags();
  DWORD GetFileOs();
  DWORD GetFileType();
  DWORD GetFileSubtype();
  CTime GetFileDate();

  CString GetCompanyName();
  CString GetFileDescription();
  CString GetFileVersion();
  CString GetInternalName();
  CString GetLegalCopyright();
  CString GetOriginalFileName();
  CString GetProductName();
  CString GetProductVersion();
  CString GetComments();
  CString GetLegalTrademarks();
  CString GetPrivateBuild();
  CString GetSpecialBuild();

#endif

};

class ADDINS_API CFileCtrlInnerUsing
{
  friend class CAi_File;

protected:
  CFileCtrlInnerUsing()
    : m_bSetUnicodeByteOrderMark(false)
  {}
  CFileCtrlInnerUsing(CFileCtrlInnerUsing& fc)
  { *this = fc; }

  bool PathInSystemEnvironment(CString strPath, bool bAdd);

  enum ESplitPath
  {
    spDisk,
    spPath,
    spFileName,
    spFileExtention
  };
  CString SplitPath(CString strFilePathName, ESplitPath sp);

  void GetListOfFilesAndFolders(CString strFolderPath, std::vector<LIST_OF_FOLDER_AND_FILES>* lstF,
                                bool bLookSubFolders, int& nOrderIdx, int nParentIdx, int nDeep);

  CString CreateSectStop(CString strSectStart);

  int ReadFileToBuffer(CString strFilePathName, BYTE** pBufferB, CString* pstrBufferS, bool bZeroEnd = false);

  bool m_bSetUnicodeByteOrderMark;
  void WriteBufToF(CFile* pF, CString* pstrBuf, FileCodingFormat coding);
  void WriteBufToF(CFile* pF, BYTE* pstrBuf, int nBufSize);
  bool ConverWToA(CString* pstrBuf, BYTE** pBuf, int& nBufSize);
  bool ConverAToW(CString* pstrBuf, BYTE** pBuf, int& nBufSize);
  bool ConvertToBuffByDef(CString* pstrBuf, BYTE** pBuf, int& nBufSize);

  // File version

#ifdef SWITCH_ON_FILE_VERSION_CODE

  virtual void Reset();
  BOOL GetTranslationId(LPVOID lpData, UINT unBlockSize, WORD wLangId, DWORD &dwId, BOOL bPrimaryEnough = FALSE);

  VS_FIXEDFILEINFO m_FileInfo;

  CString m_strCompanyName;
  CString m_strFileDescription;
  CString m_strFileVersion;
  CString m_strInternalName;
  CString m_strLegalCopyright;
  CString m_strOriginalFileName;
  CString m_strProductName;
  CString m_strProductVersion;
  CString m_strComments;
  CString m_strLegalTrademarks;
  CString m_strPrivateBuild;
  CString m_strSpecialBuild;

#endif
};