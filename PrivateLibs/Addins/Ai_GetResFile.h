#pragma once

#include "AddinsDef.h"
#include "Ai_File.h"

class ADDINS_API CAi_GetResFile
{
public:
  CAi_GetResFile();
  ~CAi_GetResFile();

  /*
  @@@ ���� ������ ��������� � ����� AppName.rc, �� ������� ��� �� ���� ����� ���������� ���������:

  - GetFile(ID, fileName): � ������ ������ ���� ����� �������� �� ���������� ID � ������� "HELPFILE" � ����� ��������:
        - ���� �� ����� fileName (�.�. == ""), �� ������ ����� ��������� ������ � m_pBuff[m_nBuffSize];
        - ���� ����� fileName, �� ������ ����� ���������:
            - � � ������ ���� (������ ��� �� ��������� ����������� Windows Temp);
            - � � m_pBuff[m_nBuffSize];

   - GetFile(ID, userResType, type, outbuff, fileCoding).
   ���� ����� ���� m_bGetBuff, �� ������ ����� ��������� � m_pBuff[m_nBuffSize], � �������� ����� pstrOutData (����� �� ��� nullptr) ����� ��������������.
   ���� ���� m_bGetBuff �� �����, �� ������ ����� ��������� � pstrOutData (������� ������ ���� �� nullptr, ��������������), � m_pBuff ����� nullptr � m_nBuffSize == 0.
   ����� ���� ��������� ��������� ���� �� ����������� Visual Studio ����� �������� (BITMAP, ICON,...), �� ��������� ������ ID ����� � ��� ������ � lpServiceType,
   ��� ��� ������ BITMAP ��������� ��� ����� RT_BITMAP. �������� ��������� ���� ����� ����������� � ����� WinUser.h (C:\Program Files\Microsoft SDKs\Windows\v6.0A\Include)
   � ������� "Predefined Resource Types".
   ���� ��������� ��������� ���� �� ����� ������ � ��������, �� ����������� ID ���� � ��� ������ � strUserResType.

   @@@ ���� ����� ������ ��������� � ����� ������� ���� AppName.rc2 � ����:
   IDR_FILE_X  GROUP_NAME  "FileName.txt"

   IDR_FILE_X ����� ���� ���������� � ����� resource.h.
   ����� ���� ������������� ������������� � exe ����� ���������� � ���� ��������� � ��������� ��������� ������� ��� rc2.
   ������� ������ ���� ����� ���:
   GetFile(IDR_FILE_X, L"GROUP_NAME", &strOutBuff);
   ���� ����� ���� m_bGetBuff, �� ������ ����� ��������� � m_pBuff[m_nBuffSize], � �������� ����� pstrOutData (����� �� ��� nullptr) ����� ��������������.
   ���� ���� m_bGetBuff �� �����, �� ������ ����� ��������� � pstrOutData (� ������ ������ ��� ���������� strOutBuff), � m_pBuff ����� nullptr � m_nBuffSize == 0.

   @@@ ��� �������� ������� ������� GetFile ���������� �������
HRSRC WINAPI FindResource(
  __in_opt  HMODULE hModule,
  __in      LPCTSTR lpName,
  __in      LPCTSTR lpType
);
    �� ��������� ������ �������� ���������������� ��� nullptr, � ������ ������ ����� ����� ����������� � ������� �������� �������� ����������.
    ������ ���� GetFile ������������ � DLL, �� ��������� nullptr �������� � ������ � ������� �������� EXE ����������. ��� ���� ����� �����
    ���������� � ������� �������� DLL, ��������� ������� ������� SetHModule, ������� �� ��������������� HANDLE. ��� DLL �����
    ����� �������� �� HINSTANCE ������������� � ������� ������� DllMain(HINSTANCE hInstance, ....
  */

  void SetHModule(HMODULE hModule) { m_hModule = hModule; }
  CString GetFile(WORD nIdRes, CString strUseItFileName); // func extracts file and copy it to tmp dir; func returns it tmp file also
  bool GetFile(const WORD nIdRes,
    const CString strUserResType /* ex.:"BIN" */, const LPTSTR lpServiceType /* ex.: RT_BITMAP or RT_HTML or nullptr if set strUserResType*/, 
    /*out*/ CString* pstrOutData = nullptr, /*in*/ FileCodingFormat resFileCoding = fcfByDef);

  bool m_bGetBuff;
  BYTE* m_pBuff;
  int m_nBuffSize;

private:
  HMODULE m_hModule;
  void FreeBuff();
};
