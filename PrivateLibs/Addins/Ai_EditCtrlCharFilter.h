#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_EditCtrlCharFilter : public CEdit
{
public:
  CAi_EditCtrlCharFilter();
  ~CAi_EditCtrlCharFilter();

private:
  DECLARE_MESSAGE_MAP()


public:
  enum EFilterSet
  {
    fsASCII // ISO 646
  };

  //void SetFilterSet(EFilterSet fs);

  void IgnoreSimbols(CString sSimbols);
  void(*CtrlTextAfterCheck)(CString sText);

private:

  CString m_sASCII_Chars;
  CString m_sIgnoreSimbols;

  EFilterSet m_filterSet = fsASCII;

  afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
};
