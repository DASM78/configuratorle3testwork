#pragma once

#include "AddinsDef.h"

#ifdef _UNICODE
#define IDS_Ai_Log_FILE_NAME L"\\Logging.txt"
#define IDS_Ai_Log_OPEN_FILE L"C:\\Logging.txt"
#else
#define IDS_Ai_Log_FILE_NAME "\\Logging.txt"
#define IDS_Ai_Log_OPEN_FILE "C:\\Logging.txt"
#endif

class ADDINS_API CAi_Log
{
private:
  static CFile m_hFile;
  static CString m_strFile;
  static bool OpenFile();
  static void CloseFile();
  static void Write(bool bEndLine = true);
public:
  // ����������� ���� ��� �����������
  // ���� ���� ��� ��� ������ �� ����� ������ ������
  CAi_Log();
  ~CAi_Log();

  int ifg = 0;
  static void SetPathToLoggingFile(CString strPath);
  static bool ClearFile();

  // --------------------------
  // ��� ���� ������� "\r\n" ��������� �� ����, ������� ������ ����� �������� �������������

  static void AddStr(CString str);           // Ex.: "Error!"
  static void AddStr(int n);                   // Ex.: 1270
  static void AddStr(CString str, int n);   // Ex.: "Error! �" 1270
  static void AddStr(int n, CString str);   // Ex.: 1 ") Error!"

  // --------------------------
  static void MyTRACE(CString str);
  static void MyTRACE_tm(CString str); // ��������� ����: time mark; <������>
};
