#include "StdAfx.h"
#include "resource.h"
#include "Ai_CmbBxDO.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_CmbBxDO::CAi_CmbBxDO()
: m_bAutoComplete(TRUE)
, m_bAutoCompleteSwitch(TRUE)
, m_bDropDown(FALSE)
, m_bAddNewStringByEnterKeyDown(false)
, m_hWndForMsgAfterInnerEvent(nullptr)
, m_bMsgIfAddedItemExistsAlready(false)
, m_bMsgBeforeAddNewStringByEnterKeyDown(false)
, m_nIDWithMsg(-1)
{
}

CAi_CmbBxDO::~CAi_CmbBxDO()
{
}


BEGIN_MESSAGE_MAP(CAi_CmbBxDO, CComboBox)
//{{AFX_MSG_MAP(CAi_CmbBxDO)
ON_CONTROL_REFLECT(CBN_EDITUPDATE, OnEditupdate)
ON_CONTROL_REFLECT(CBN_DROPDOWN, OnDropdown)
ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelchange)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAi_CmbBxDO message handlers

BOOL CAi_CmbBxDO::PreTranslateMessage(MSG* pMsg)
{
  if (pMsg->message == WM_KEYDOWN)
  {
    m_bAutoComplete = TRUE;

    int nVirtKey = (int) pMsg->wParam;
    
    if (nVirtKey == VK_DELETE || nVirtKey == VK_BACK)
    {
      m_bAutoComplete = FALSE;
      TestShowString();
    }
    else 
    if (nVirtKey == VK_RETURN)
    {
      int iIndex = GetCurSel();
    
      if (iIndex < 0)
      {
        GetWindowText(m_strNewAddingStr);

        if (!m_strNewAddingStr.IsEmpty()) // ������ �� ������
        {
          if (FindStringExact(-1, m_strNewAddingStr) != CB_ERR) // ������ ��� ����
          {
            if (m_bMsgIfAddedItemExistsAlready)
              AfxMessageBox(m_strMsgIfAddedItemExistsAlready, MB_ICONERROR | MB_OK);
            SelectString(-1, m_strNewAddingStr);
          }
          else
          { // ������ ��� ���

            if (m_bMsgBeforeAddNewStringByEnterKeyDown)
            {
               if (m_hWndForMsgAfterInnerEvent != nullptr)
                ::PostMessage(m_hWndForMsgAfterInnerEvent, MSG_BEFORE_ADD_NEW_STRING_BY_ENTER_KEYDOWN, m_nIDWithMsg, (LPARAM)&m_strNewAddingStr);
            }

            if (m_bAddNewStringByEnterKeyDown)
            {
              AddString(m_strNewAddingStr);
              if (m_hWndForMsgAfterInnerEvent != nullptr)
                ::PostMessage(m_hWndForMsgAfterInnerEvent, MSG_AFTER_ADD_NEW_STRING_BY_ENTER_KEYDOWN, m_nIDWithMsg, (LPARAM)&m_strNewAddingStr);
            }

            return TRUE;
          }
        }
      }
    }
    else
    if (nVirtKey == VK_DOWN)
    {
      ShowDropDown();
    }
  }

  return CComboBox::PreTranslateMessage(pMsg);
}

// current typed
bool CAi_CmbBxDO::IsTheTextInList()
{
  CString strText;

  GetWindowText(strText);
  return IsTheTextInList(strText);
}

bool CAi_CmbBxDO::IsTheTextInList(CString strFoundText)
{
   if (FindStringExact(-1, strFoundText) != CB_ERR)
     return true;

   return false;
}

void CAi_CmbBxDO::TestShowString()
{
  CString str;

  GetWindowText(str);
  if (str.GetLength() == 1)
    OnDropdown();
}

void CAi_CmbBxDO::OnEditupdate()
{
  if (!m_bAutoComplete || !m_bAutoCompleteSwitch)
    return;

  // Get the text in the edit box
  CString str;
  GetWindowText(str);

  int nLength = str.GetLength();

  // Currently selected range
  DWORD dwCurSel = GetEditSel();
  WORD dStart = LOWORD(dwCurSel);
  WORD dEnd = HIWORD(dwCurSel);

  // Search for, and select in, and string in the combo box that is prefixed
  // by the text in the edit box
  int iIndex, iItemHeight, iEditHeight, iBoxHeight, iNumInList, iNumToShow;
  CRect r, r2;
  WINDOWPLACEMENT wp;
  
  if ((iIndex = SelectString(-1, str)) == CB_ERR)
  {
    ShowDropDown(FALSE);
    SetWindowText(str); // No text selected, so restore what was there before
    if (dwCurSel != CB_ERR)
      SetEditSel(dStart, dEnd); //restore cursor postion
  }
  else
  {
    iItemHeight = GetItemHeight(0);
    iEditHeight = GetItemHeight(-1);
    iNumInList = GetCount();
    GetDroppedControlRect(&r);
    GetParent()->ScreenToClient(&r);

    iNumToShow = iNumInList - iIndex;
    if (iNumToShow > 7)
      iNumToShow = 7;

    iBoxHeight = (iNumToShow * iItemHeight);

    GetWindowPlacement(&wp);
    r2 = wp.rcNormalPosition;
    r2.bottom = r2.top + (iBoxHeight + iEditHeight) + iItemHeight;

    MoveWindow(r2);
    SetTopIndex(iIndex);
    SelectString(-1, str);

    m_bDropDown = TRUE;
    ShowDropDown(TRUE);
    m_bDropDown = FALSE;

    if (dEnd < nLength && dwCurSel != CB_ERR)
      SetEditSel(dStart, dEnd);
    else
    if (dEnd == nLength && dwCurSel != CB_ERR)
    {
      iIndex = SelectString(-1, str);
      if (FindString(iIndex, str) == CB_ERR)
        ShowDropDown(FALSE);
      SetEditSel(nLength, -1);
    }
    else
      SetEditSel(nLength, -1);
  }
}

void CAi_CmbBxDO::OnDropdown()
{
if (m_bDropDown)
  return;

  int iItemHeight, iEditHeight, iBoxHeight, iNumInList, iNumToShow;
  CRect r, r2;
  WINDOWPLACEMENT wp;

  iItemHeight = GetItemHeight(0);
  iEditHeight = GetItemHeight(-1);
  iNumInList = GetCount();
  GetDroppedControlRect(&r);
  GetParent()->ScreenToClient(&r);

  iNumToShow = iNumInList;
  if (iNumToShow > 7)
    iNumToShow = 7;

  iBoxHeight = (iNumToShow * iItemHeight);

  GetWindowPlacement(&wp);
  r2 = wp.rcNormalPosition;
  r2.bottom = r2.top + (iBoxHeight + iEditHeight) + iItemHeight;

  MoveWindow(r2);
}

BOOL CAi_CmbBxDO::AddNewStrSelect(CString str)
{
  bool bRet = false;
  int i, iNum;

  iNum = str.GetLength();
  for (i = 0; i < iNum; ++i)
  {
    if (!isspace(str.GetAt(i)))
    {
      bRet = true;
      break;
    }
  }
  if (bRet == false)
    return FALSE;

  if (FindStringExact(-1, static_cast<LPCTSTR> (str)) == CB_ERR)
    AddString(static_cast<LPCTSTR> (str));

  SelectString(-1, static_cast<LPCTSTR> (str));
  return TRUE;
}

BOOL CAi_CmbBxDO::AddNewStrNoSelect(CString str)
{
  bool bRet = false;
  int i, iNum;

  iNum = str.GetLength();
  for (i = 0; i < iNum; ++i)
  {
    if (!isspace(str.GetAt(i)))
    {
      bRet = true;
      break;
    }
  }
  if (bRet == false)
    return FALSE;

  if (FindStringExact(-1, static_cast<LPCTSTR> (str)) == CB_ERR)
    AddString(static_cast<LPCTSTR> (str));

  return TRUE;
}

CString CAi_CmbBxDO::GetCurSelected()
{
  int nSelIndex = GetCurSel();

  if(nSelIndex < 0)
    return L"";
  GetLBText(nSelIndex, m_strSelectedStr);
  return m_strSelectedStr;
}

void CAi_CmbBxDO::OnSelchange()
{
  int nSelIndex = GetCurSel();
  
  if(nSelIndex < 0)
    return;
  GetLBText(nSelIndex, m_strSelectedStr);
  if (m_hWndForMsgAfterInnerEvent != nullptr)
    ::PostMessage(m_hWndForMsgAfterInnerEvent, MSG_AFTER_SELCHANGE_STRING, m_nIDWithMsg, (LPARAM)&m_strSelectedStr);
}

void CAi_CmbBxDO::FillInNewStrings(vector<CString> *strList)
{
  ResetContent();
  for (int i = 0; i < (int)strList->size(); ++i)
    AddString(strList->at(i));
}

void CAi_CmbBxDO::GetAllList(vector<CString> *strList)
{
  CString strMean;

  strList->clear();
  for (int i = 0; i < GetCount(); ++i)
  {
    GetLBText(i, strMean);
    strList->push_back(strMean);
  }
}