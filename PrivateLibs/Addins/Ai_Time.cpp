#include "stdafx.h"
#include "Ai_Time.h"
#include "Ai_File.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

vector<TIMER_MARK> CAi_Time::m_timerMarkList_stc;

CAi_Time::CAi_Time(void)
  : m_bToStatic(false)
{
}

CAi_Time::CAi_Time(int nID)
  : m_bToStatic(false)
{
  SaveCurrentTime(nID);
}

CAi_Time::~CAi_Time(void)
{
}

void CAi_Time::SaveCurrentTime_(int nID)
{
  CAi_Time t;
  t.m_bToStatic = true;
  t.SaveCurrentTime(nID);
}

void CAi_Time::SaveCurrentTime(int nID, bool bSaveToFile, CString strFileName)
{
  __int64 nMillisecondsNow = GetCurrentTimeByMilliseconds();

  if (bSaveToFile)
  {
    CString strTimerMarkFile = strFileName.IsEmpty() ? CAi_File::ExamineTmpFile(CString(TIMER_MARK_FILE)) : strFileName;
    CString strID;
    CString strTimerMark;
    
    strID.Format(_T("%d"), nID);
    strTimerMark.Format(_T("%{I64}d"), nMillisecondsNow);
    WritePrivateProfileString(strID, _T("TimerMark"), strTimerMark, strTimerMarkFile);
  }

  vector<TIMER_MARK>* pStorage = &m_timerMarkList;
  if (m_bToStatic)
    pStorage = &m_timerMarkList_stc;

  bool bSaved = false;

  if (pStorage->size() > 0)
    for (size_t i = 0; i < pStorage->size(); ++i)
      if (pStorage->at(i).nID == nID)
      {
        pStorage->at(i).tTime = nMillisecondsNow;
        bSaved = true;
        break;
      }

  if (!bSaved)
  {
    TIMER_MARK tm;
    tm.nID = nID;
    tm.tTime = nMillisecondsNow;
    pStorage->push_back(tm);
  }
}

__int64 CAi_Time::GetTimePeriod_(int nID, TimerMarkResult tmrResult, bool bUpdateCurrentTimeMark, bool bRoundUp)
{
  CAi_Time t;
  t.m_bToStatic = true;
  return t.GetTimePeriod(nID, tmrResult, bUpdateCurrentTimeMark, bRoundUp, false, _T(""));
}

__int64 CAi_Time::GetTimePeriod(int nID, TimerMarkResult tmrResult, bool bUpdateCurrentTimeMark, bool bRoundUp, bool bFromFile, CString strFileName)
{
  __int64 nMillisecondsWas = 0;
  if (m_bToStatic)
    nMillisecondsWas = GetSavedTime_(nID, tmrGetByMilliseconds);
  else
    nMillisecondsWas = GetSavedTime(nID, tmrGetByMilliseconds, bFromFile, strFileName);

  if (nMillisecondsWas == -1)
    return -1;

  if (bUpdateCurrentTimeMark)
    SaveCurrentTime(nID);

  __int64 nMilliSecInterval = GetCurrentTimeByMilliseconds() - nMillisecondsWas;
  return GetTimeIn(nMilliSecInterval, tmrResult, bRoundUp);
}

__int64 CAi_Time::GetTimeIn(__int64 nMs, TimerMarkResult tmrResult, bool bRoundUp)
{
  if (tmrResult == tmrGetByMilliseconds)
    return nMs;

  if (tmrResult == tmrGetBySeconds)
  {
    __int64 nSecInterval = ConvertMilliseconds(nMs, tmrResult);
    if (bRoundUp)
      nSecInterval += (m_nMillisecondsTail > 500) ? 1 : 0;
    return nSecInterval;
  }

  if (tmrResult == tmrGetByMinutes)
  {
    __int64 nMinInterval = ConvertMilliseconds(nMs, tmrResult);
    if (bRoundUp)
      nMinInterval += (m_nSecondsTail > 30) ? 1 : 0;
    return nMinInterval;
  }

  if (tmrResult == tmrGetByHours)
  {
    __int64 nHoursInterval = ConvertMilliseconds(nMs, tmrResult);
    if (bRoundUp)
      nHoursInterval += (m_nMinitesTail > 30) ? 1 : 0;
    return nHoursInterval;
  }

  return 0;
}

__int64 CAi_Time::GetSavedTime_(int nID, TimerMarkResult tmrResult)
{
  CAi_Time t;
  t.m_bToStatic = true;
  return t.GetSavedTime(nID, tmrResult);
}

__int64 CAi_Time::GetSavedTime(int nID, TimerMarkResult tmrResult, bool bFromFile, CString strFileName)
{
  return ConvertMilliseconds(LoadTimerMark(nID, bFromFile, strFileName), tmrResult);
}

void CAi_Time::FreeID_(int nID)
{
  for (size_t i = 0; i < m_timerMarkList_stc.size(); ++i)
    if (m_timerMarkList_stc[i].nID == nID)
    {
      m_timerMarkList_stc.erase(m_timerMarkList_stc.begin() + i);
      i--;
    }
}

__int64 CAi_Time::GetCurrentTimeByMilliseconds()
{
  SYSTEMTIME st = {0};
  GetLocalTime(&st); // 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601 (UTC)

  FILETIME ft = {0};
  SystemTimeToFileTime(&st, &ft);

  __int64 nCurrTime = 0;
  memcpy(&nCurrTime, &ft, sizeof(FILETIME));
  return (nCurrTime / 10000); // ��� ����� ������������
}

__int64 CAi_Time::ConvertMilliseconds(__int64 nMilliseconds, TimerMarkResult tmrResult)
{
  if (nMilliseconds == -1)
    return -1;
  if (tmrResult == tmrGetByMilliseconds)
    return nMilliseconds;

  __int64 nSeconds = nMilliseconds / 1000;
  m_nMillisecondsTail = (int)(nMilliseconds % 1000);
  if (tmrResult == tmrGetBySeconds)
    return nSeconds;

  __int64 nMinutes = nSeconds / 60;
  m_nSecondsTail = (int)(nSeconds % 60);
  if (tmrResult == tmrGetBySeconds)
    return nMinutes;

  __int64 nHours = nMinutes / 60;
  m_nMinitesTail = (int)(nMinutes % 60);
  if (tmrResult == tmrGetByHours)
    return nHours;

  return 0;
}

CString CAi_Time::GetCurrentTimeByString(TimerStringResult tsr, __int64 nFromItTime_Ms)
{
  SYSTEMTIME st = {0};
  GetLocalTime(&st);
  CString strTime;

  switch (tsr)
  {
    case tsrD_M_Y_H_M_S_Ms:
      strTime.Format(_T("%d.%d.%d %d:%d:%d.%d"), st.wDay, st.wMonth, st.wYear, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
      break;
    case tsrH_M_S_Ms:
      strTime.Format(_T("%d:%d:%d.%d"), st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
      break;
    case tsrH_M_S:
      strTime.Format(_T("%d:%d:%d"), st.wHour, st.wMinute, st.wSecond);
      break;
    case tsrH2_M2_S2_Ms:
      {
        __int64 nInTm = nFromItTime_Ms;
        int nH = (int)GetTimeIn(nInTm, tmrGetByHours);
        int nHByMs = nH * 60 * 60 * 1000; // count of Ms
        nInTm = nFromItTime_Ms - nHByMs;
        int nM = (int)ConvertMilliseconds(nInTm, tmrGetByMinutes);
        int nMByMs = nM * 60 * 1000; // count of Ms
        nInTm = nFromItTime_Ms - nHByMs - nMByMs;
        int nS = (int)ConvertMilliseconds(nInTm, tmrGetBySeconds);
        int nSByMs = nS * 1000; // count of Ms
        int nMs = (int)(nFromItTime_Ms - nHByMs - nMByMs - nSByMs);
        if (nH > 0)
          strTime.Format(_T("%d:%d:%d.%d �"), nH, nM, nS, nMs);
        else
        if (nM > 0)
          strTime.Format(_T("%d:%d.%d ���"), nM, nS, nMs);
        else
        if (nS > 0)
          strTime.Format(_T("%d.%d �"), nS, nMs);
        else
          strTime.Format(_T("%d ��"), nMs);
      }
      break;
  };
  return strTime;
}

CString CAi_Time::GetCur(CString IDS)
{
  __time64_t t;
  t = time(nullptr);
  CTime time(t);
  CString sFormat = IDS;
  if (!sFormat.IsEmpty())
    sFormat += L" ";
  sFormat += _T("  %H:%M:%S");
  CString strTime = time.Format(sFormat);
  return strTime;
}

__time64_t CAi_Time::LoadTimerMark(int nID, bool bFromFile, CString strFileName)
{
  if (bFromFile)
  {
    CString strTimerMarkFile = strFileName.IsEmpty() ? CAi_File::ExamineTmpFile(CString(TIMER_MARK_FILE)) : strFileName;
    CString strID;
    CString strTimerMark;
    TCHAR szTimerMark[_MAX_PATH] = {0};
    __int64 nTimePrev = 0;

    strID.Format(_T("%d"), nID);
    GetPrivateProfileString(strID, _T("TimerMark"), _T("0"), szTimerMark, 20, strTimerMarkFile);
    #ifdef _UNICODE
    swscanf_s(szTimerMark, _T("%d"), &nTimePrev);
    #else
    sscanf_s(szTimerMark, _T("%d"), &nTimePrev);
    #endif
    return nTimePrev;
  }
  else
  {
    vector<TIMER_MARK>* pStorage = &m_timerMarkList;
    if (m_bToStatic)
      pStorage = &m_timerMarkList_stc;

    if (pStorage->size() > 0)
      for (size_t i = 0; i < pStorage->size(); ++i)
        if (pStorage->at(i).nID == nID)
          return pStorage->at(i).tTime;
  }

  return -1;
}

bool CAi_Time::IsEmpty(SYSTEMTIME* pSt)
{
  SYSTEMTIME st = {0};
  return (memcmp(&st, pSt, sizeof(SYSTEMTIME)) == 0) ? true : false;
}