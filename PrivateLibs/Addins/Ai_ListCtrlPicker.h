#pragma once

#include <vector>
#include "afxlistctrl.h"
#include "AddinsDef.h"

class ADDINS_API CAi_ListCtrlPicker : public CMFCListCtrl
{
  DECLARE_DYNAMIC(CAi_ListCtrlPicker)

public:
  CAi_ListCtrlPicker();
  ~CAi_ListCtrlPicker();

protected:
  struct ColumnState
  {
    ColumnState()
      : m_Visible(false)
      , m_OrgWidth(0)
      , m_OrgPosition(-1)
    {}
    bool m_Visible;
    int  m_OrgWidth; // Width it had before being hidden
    int  m_OrgPosition;	// Position it had before being hidden
  };
  std::vector<ColumnState> m_ColumnStates;
  int GetColumnStateCount();
  void InsertColumnState(int nCol, bool bVisible, int nOrgWidth = 0);
  void DeleteColumnState(int nCol);
  ColumnState& GetColumnState(int nCol);

  DECLARE_MESSAGE_MAP()

  virtual afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  virtual afx_msg LRESULT OnDeleteColumn(WPARAM wParam, LPARAM lParam);
  virtual afx_msg LRESULT OnInsertColumn(WPARAM wParam, LPARAM lParam);
  virtual afx_msg LRESULT OnSetColumnWidth(WPARAM wParam, LPARAM lParam);
  virtual afx_msg BOOL OnHeaderBeginResize(UINT id, NMHDR* pNmhdr, LRESULT* pResult);
  virtual afx_msg BOOL OnHeaderBeginDrag(UINT id, NMHDR* pNmhdr, LRESULT* pResult);
  virtual afx_msg BOOL OnHeaderEndDrag(UINT id, NMHDR* pNmhdr, LRESULT* pResult);
  virtual afx_msg BOOL OnHeaderDividerDblClick(UINT, NMHDR* pNMHDR, LRESULT* pResult);
  virtual afx_msg void OnContextMenu(CWnd*, CPoint point);
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

public:
  bool m_bShowHeaderContextMenu;
  bool m_bCanHeaderDrag;
  void MyPreSubclassWindow();

  bool IsColumnVisible(int nCol);
  int GetFirstVisibleColumn();
  BOOL ShowColumn(int nCol, bool bShow);
  BOOL SetColumnWidthAuto(int nCol = -1, bool includeHeader = false);

private:
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT pdis);
public:
  virtual void OnMyDrawItem(int nIDCtl, LPDRAWITEMSTRUCT pdis);

  virtual HFONT OnGetCellFont(int nRow, int nColum, DWORD dwData = 0);
  void SetBoldColumn(int nColumn, bool bExclude = false);
private:
  std::vector<int> m_boldColumns;

  // tool tips for header

public:
  BOOL AddHeaderToolTip(int nCol, LPCTSTR sTip);
  void RecalcHeaderTips();

  virtual BOOL PreTranslateMessage(MSG* pMsg);

  CToolTipCtrl m_tooltipHeader;
  
private:
  std::vector<int> m_columnWidths;
  void FormColumnWidthsList();
  bool WasColumnWidthsChanged();
public:
  virtual void ColumnWidthsWasChanged() {}

public:
  virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

private:
  afx_msg void OnNMCustomdrawLst(NMHDR *pNMHDR, LRESULT *pResult) {}
};