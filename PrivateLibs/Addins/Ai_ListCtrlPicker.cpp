#include "stdafx.h"
#include "afxglobals.h"
#include "Ai_ListCtrlPicker.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CAi_ListCtrlPicker, CMFCListCtrl)

BEGIN_MESSAGE_MAP(CAi_ListCtrlPicker, CMFCListCtrl)
  ON_MESSAGE(LVM_DELETECOLUMN, OnDeleteColumn)
  ON_MESSAGE(LVM_INSERTCOLUMN, OnInsertColumn)
  ON_MESSAGE(LVM_SETCOLUMNWIDTH, OnSetColumnWidth)
  ON_NOTIFY_EX(HDN_BEGINTRACKA, 0, OnHeaderBeginResize)
  ON_NOTIFY_EX(HDN_BEGINTRACKW, 0, OnHeaderBeginResize)
  ON_NOTIFY_EX(HDN_BEGINDRAG, 0, OnHeaderBeginDrag)
  ON_NOTIFY_EX(HDN_ENDDRAG, 0, OnHeaderEndDrag)
  ON_NOTIFY_EX(HDN_DIVIDERDBLCLICKA, 0, OnHeaderDividerDblClick)
  ON_NOTIFY_EX(HDN_DIVIDERDBLCLICKW, 0, OnHeaderDividerDblClick)
  //ON_NOTIFY_EX(NM_CUSTOMDRAW, OnNMCustomdrawLst)
  ON_WM_CONTEXTMENU() // OnContextMenu
  ON_WM_KEYDOWN() // OnKeyDown
  ON_WM_DRAWITEM()
END_MESSAGE_MAP()

CAi_ListCtrlPicker::CAi_ListCtrlPicker()
  : m_bShowHeaderContextMenu(true)
  , m_bCanHeaderDrag(true)
{
  m_tooltipHeader.Create(this);
}

CAi_ListCtrlPicker::~CAi_ListCtrlPicker()
{
}

int CAi_ListCtrlPicker::GetFirstVisibleColumn()
{
  int nColCount = CListCtrl::GetHeaderCtrl()->GetItemCount();
  for(int i = 0; i < nColCount; ++i)
  {
    int nCol = CListCtrl::GetHeaderCtrl()->OrderToIndex(i);
    if (IsColumnVisible(nCol))
      return nCol;
  }
  return -1;
}

BOOL CAi_ListCtrlPicker::ShowColumn(int nCol, bool bShow)
{
  SetRedraw(FALSE);

  ColumnState& columnState = GetColumnState(nCol);

  int nColCount = CListCtrl::GetHeaderCtrl()->GetItemCount();
  int* pOrderArray = new int[nColCount];
  VERIFY( GetColumnOrderArray(pOrderArray, nColCount) );
  if (bShow)
  {
    // Restore the position of the column
    int nCurIndex = -1;
    for(int i = 0; i < nColCount ; ++i)
    {
      if (pOrderArray[i]==nCol)
        nCurIndex = i;
      else
      if (nCurIndex!=-1)
      {
        // We want to move it to the original position,
        // and after the last hidden column
        if ( (i <= columnState.m_OrgPosition)
          || !IsColumnVisible(pOrderArray[i])
           )
        {
          pOrderArray[nCurIndex] = pOrderArray[i];
          pOrderArray[i] = nCol;
          nCurIndex = i;
        }
      }
    }
  }
  else
  {
    // Move the column to the front of the display order list
    int nCurIndex(-1);
    for(int i = nColCount-1; i >=0 ; --i)
    {
      if (pOrderArray[i]==nCol)
      {
        // Backup the current position of the column
        columnState.m_OrgPosition = i;
        nCurIndex = i;
      }
      else
      if (nCurIndex!=-1)
      {
        pOrderArray[nCurIndex] = pOrderArray[i];
        pOrderArray[i] = nCol;
        nCurIndex = i;
      }
    }
  }

  VERIFY( SetColumnOrderArray(nColCount, pOrderArray) );
  delete [] pOrderArray;

  if (bShow)
  {
    // Restore the column width
    columnState.m_Visible = true;
    VERIFY( SetColumnWidth(nCol, columnState.m_OrgWidth) );
  }
  else
  {
    // Backup the column width
    int orgWidth = GetColumnWidth(nCol);
    if (orgWidth > 0)
    {
      VERIFY( SetColumnWidth(nCol, 0) );
    }
    columnState.m_Visible = false;
    columnState.m_OrgWidth = orgWidth;
  }
  SetRedraw(TRUE);
  Invalidate(FALSE);
  return TRUE;
}

BOOL CAi_ListCtrlPicker::SetColumnWidthAuto(int nCol, bool includeHeader)
{
  if (nCol == -1)
  {
    for(int i = 0; i < CListCtrl::GetHeaderCtrl()->GetItemCount() ; ++i)
      SetColumnWidthAuto(i, includeHeader);
    return TRUE;
  }
  else
  {
    if (includeHeader)
      return SetColumnWidth(nCol, LVSCW_AUTOSIZE_USEHEADER);
    else
      return SetColumnWidth(nCol, LVSCW_AUTOSIZE);
  }
}

CAi_ListCtrlPicker::ColumnState& CAi_ListCtrlPicker::GetColumnState(int nCol)
{
  VERIFY( nCol >=0 && nCol < (int)m_ColumnStates.size() );
  return m_ColumnStates[nCol];
}

bool CAi_ListCtrlPicker::IsColumnVisible(int nCol)
{
  return GetColumnState(nCol).m_Visible;
}

int CAi_ListCtrlPicker::GetColumnStateCount()
{
  return m_ColumnStates.size();
}

void CAi_ListCtrlPicker::InsertColumnState(int nCol, bool bVisible, int nOrgWidth)
{
  VERIFY( nCol >=0 && nCol <= (int)m_ColumnStates.size() );

  ColumnState columnState;
  columnState.m_OrgWidth = nOrgWidth;
  columnState.m_Visible = bVisible;

  if (nCol == (int)m_ColumnStates.size())
    // Append column picker to the end of the array
    m_ColumnStates.push_back(columnState);
  else
  {
    // Insert column in the middle of the array
    vector<ColumnState> newArray;
    for(int i=0 ; i < (int)m_ColumnStates.size(); ++i)
    {
      if (i == nCol)
        newArray.push_back(columnState);
      newArray.push_back(m_ColumnStates[i]);
    }
    m_ColumnStates = newArray;
  }
}

void CAi_ListCtrlPicker::DeleteColumnState(int nCol)
{
  VERIFY( nCol >=0 && nCol < (int)m_ColumnStates.size() );
  m_ColumnStates.erase(m_ColumnStates.begin() + nCol);
}

void CAi_ListCtrlPicker::OnContextMenu(CWnd* pWnd, CPoint point)
{
  if (!m_bShowHeaderContextMenu)
    return;
  if (point.x==-1 && point.y==-1)
  {
    // OBS! point is initialized to (-1,-1) if using SHIFT+F10 or VK_APPS
  }
  else
  {
    CPoint pt = point;
    ScreenToClient(&pt);

    CRect headerRect;
    CListCtrl::GetHeaderCtrl()->GetClientRect(&headerRect);
    if (headerRect.PtInRect(pt))
    {
      // Show context-menu with the option to show hide columns
      CMenu menu;
      if (menu.CreatePopupMenu())
      {
        for( int i = GetColumnStateCount()-1 ; i >= 0; --i)
        {
          UINT uFlags = MF_BYPOSITION | MF_STRING;

          // Put check-box on context-menu
          if (IsColumnVisible(i))
            uFlags |= MF_CHECKED;
          else
            uFlags |= MF_UNCHECKED;

          // Retrieve column-title
          LVCOLUMN lvc = {0};
          lvc.mask = LVCF_TEXT;
          TCHAR sColText[256];
          lvc.pszText = sColText;
          lvc.cchTextMax = sizeof(sColText)-1;
          VERIFY( GetColumn(i, &lvc) );

          menu.InsertMenu(0, uFlags, i, lvc.pszText);
        }

        menu.TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, this, 0);
      }
    }
  }
}

// Handle context-menu event for showing / hiding columns
BOOL CAi_ListCtrlPicker::OnCommand(WPARAM wParam, LPARAM lParam)
{
  if (HIWORD(wParam) == 0
    && (int)wParam < GetColumnStateCount())
  {
    /*int nCol = LOWORD(wParam);
    ShowColumn(nCol, !IsColumnVisible(nCol));*/
  }
  else
    return CMFCListCtrl::OnCommand(wParam, lParam);

  return TRUE;
}

void CAi_ListCtrlPicker::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  switch(nChar)
  {
    case VK_ADD:	// CTRL + NumPlus (Auto size all columns)
    {
      if (GetKeyState(VK_CONTROL) < 0)
      {
        // Special handling to avoid showing "hidden" columns
        SetColumnWidthAuto(-1);
        return;
      }
    } break;
  }
  CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CAi_ListCtrlPicker::OnHeaderBeginResize(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
  // Check that column is allowed to be resized
  NMHEADER* pNMH = (NMHEADER*)pNMHDR;
  int nCol = (int)pNMH->iItem;
  if (!IsColumnVisible(nCol))
  {
    *pResult = TRUE;	// Block resize
    return TRUE;		// Block event
  }
  return FALSE;
}

LRESULT CAi_ListCtrlPicker::OnSetColumnWidth(WPARAM wParam, LPARAM lParam)
{
  // Check that column is allowed to be resized
  int nCol = (int)wParam;
  if (!IsColumnVisible(nCol))
  {
    return FALSE;
  }
  // Let the CListCtrl handle the event
  return DefWindowProc(LVM_SETCOLUMNWIDTH, wParam, lParam);
}

BOOL CAi_ListCtrlPicker::OnHeaderBeginDrag(UINT id, NMHDR* pNMHDR, LRESULT* pResult)
{
  if (!m_bCanHeaderDrag)
  *pResult = TRUE;
  return TRUE;
}

BOOL CAi_ListCtrlPicker::OnHeaderEndDrag(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
  NMHEADER* pNMH = (NMHEADER*)pNMHDR;
  if (pNMH->pitem->mask & HDI_ORDER)
  {
    // Correct iOrder so it is just after the last hidden column
    int nColCount = CListCtrl::GetHeaderCtrl()->GetItemCount();
    int* pOrderArray = new int[nColCount];
    VERIFY( GetColumnOrderArray(pOrderArray, nColCount) );

    for(int i = 0; i < nColCount ; ++i)
    {
      if (IsColumnVisible(pOrderArray[i]))
      {
        pNMH->pitem->iOrder = max(pNMH->pitem->iOrder,i);
        break;
      }
    }
    delete [] pOrderArray;
  }
  return FALSE;
}

BOOL CAi_ListCtrlPicker::OnHeaderDividerDblClick(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
  NMHEADER* pNMH = (NMHEADER*)pNMHDR;
  SetColumnWidthAuto(pNMH->iItem);
  return TRUE;	// Don't let parent handle the event
}

LRESULT CAi_ListCtrlPicker::OnDeleteColumn(WPARAM wParam, LPARAM lParam)
{
  // Let the CListCtrl handle the event
  LRESULT lRet = DefWindowProc(LVM_DELETECOLUMN, wParam, lParam);
  if (lRet == FALSE)
    return FALSE;

  // Book keeping of columns
  DeleteColumnState((int)wParam);
  return lRet;
}

LRESULT CAi_ListCtrlPicker::OnInsertColumn(WPARAM wParam, LPARAM lParam)
{
  // Let the CListCtrl handle the event
  LRESULT lRet = DefWindowProc(LVM_INSERTCOLUMN, wParam, lParam);
  if (lRet == -1)
    return -1;

  int nCol = (int)lRet;

  // Book keeping of columns
  if (GetColumnStateCount() < CListCtrl::GetHeaderCtrl()->GetItemCount())
    InsertColumnState((int)nCol, true);	// Insert as visible

  return lRet;
}

namespace {
  LRESULT EnableWindowTheme(HWND hwnd, LPCWSTR classList, LPCWSTR subApp, LPCWSTR idlist)
  {
    HMODULE hinstDll;
    HRESULT (__stdcall *pSetWindowTheme)(HWND hwnd, LPCWSTR pszSubAppName, LPCWSTR pszSubIdList);
    HANDLE (__stdcall *pOpenThemeData)(HWND hwnd, LPCWSTR pszClassList);
    HRESULT (__stdcall *pCloseThemeData)(HANDLE hTheme);

    hinstDll = ::LoadLibrary(_T("UxTheme.dll"));
    if (hinstDll)
    {
      CString strTmp = _T("OpenThemeData");
      size_t i = 0;
      int nBufSize = strTmp.GetLength() + 1; // 1 for zero end
      char* szBuf = new char[nBufSize];
      ZeroMemory(szBuf, nBufSize);
      const wchar_t* pSrcStr = (const wchar_t*)strTmp.GetBuffer();
      mbstate_t nMbSt = 0;
      wcsrtombs_s(&i, szBuf, nBufSize, &pSrcStr, _TRUNCATE, &nMbSt);

     (FARPROC&)pOpenThemeData = ::GetProcAddress(hinstDll, szBuf);
     delete szBuf;
    
     strTmp = _T("CloseThemeData");
      nBufSize = strTmp.GetLength() + 1; // 1 for zero end
      szBuf = new char[nBufSize];
      ZeroMemory(szBuf, nBufSize);
      pSrcStr = (const wchar_t*)strTmp.GetBuffer();
      nMbSt = 0;
      wcsrtombs_s(&i, szBuf, nBufSize, &pSrcStr, _TRUNCATE, &nMbSt);
      
     (FARPROC&)pCloseThemeData = ::GetProcAddress(hinstDll, szBuf);
     delete szBuf;

     strTmp = _T("SetWindowTheme");
      nBufSize = strTmp.GetLength() + 1; // 1 for zero end
      szBuf = new char[nBufSize];
      ZeroMemory(szBuf, nBufSize);
      pSrcStr = (const wchar_t*)strTmp.GetBuffer();
      nMbSt = 0;
      wcsrtombs_s(&i, szBuf, nBufSize, &pSrcStr, _TRUNCATE, &nMbSt);
      
      (FARPROC&)pSetWindowTheme = ::GetProcAddress(hinstDll, szBuf);
     delete szBuf;

      ::FreeLibrary(hinstDll);
      if (pSetWindowTheme && pOpenThemeData && pCloseThemeData)
      {
        HANDLE theme = pOpenThemeData(hwnd,classList);
        if (theme!=nullptr)
        {
          VERIFY(pCloseThemeData(theme)==S_OK);
          return pSetWindowTheme(hwnd, subApp, idlist);
        }
      }
    }
    return S_FALSE;
  }
}

void CAi_ListCtrlPicker::MyPreSubclassWindow()
{
  //CListCtrl::PreSubclassWindow();

  // Focus retangle is not painted properly without double-buffering
#if (_WIN32_WINNT >= 0x501)
  SetExtendedStyle(LVS_EX_DOUBLEBUFFER | GetExtendedStyle());
#endif
  SetExtendedStyle(GetExtendedStyle() | LVS_EX_FULLROWSELECT);
  SetExtendedStyle(GetExtendedStyle() | LVS_EX_HEADERDRAGDROP);
  SetExtendedStyle(GetExtendedStyle() | LVS_EX_GRIDLINES);

#ifdef NO_LIST_CTRL_THEME
#else
  // Enable Vista-look if possible
  EnableWindowTheme(GetSafeHwnd(), L"ListView", L"Explorer", nullptr);
#endif

  /*m_tooltipHeader.Create(this);*/
}

void CAi_ListCtrlPicker::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT pdis)
{
  OnMyDrawItem(nIDCtl, pdis);
}

void CAi_ListCtrlPicker::OnMyDrawItem(int nIDCtl, LPDRAWITEMSTRUCT pdis)
{
}

HFONT CAi_ListCtrlPicker::OnGetCellFont(int nRow, int nColum, DWORD dwData)
{
  if (m_boldColumns.size() == 0)
    return nullptr;
  for (size_t i = 0; i < m_boldColumns.size(); ++i)
    if (nColum == m_boldColumns[i])
      return afxGlobalData.fontDefaultGUIBold;
  return nullptr;
}

void CAi_ListCtrlPicker::SetBoldColumn(int nColumn, bool bExclude)
{
  bool bAdd = false;
  for (size_t i = 0; i < m_boldColumns.size(); ++i)
  {
    if (bExclude)
    {
      if (m_boldColumns[i] == nColumn)
      {
        m_boldColumns.erase(m_boldColumns.begin() + i);
        i--;
      }
    }
    else
    {
      bAdd = true;
      if (m_boldColumns[i] == nColumn)
      {
        bAdd = false;
        break;
      }
    }
    if (bAdd)
      m_boldColumns.push_back(nColumn);
  }
}

BOOL CAi_ListCtrlPicker::AddHeaderToolTip(int nCol, LPCTSTR sTip)
{
  const int TOOLTIP_LENGTH = 80;
  TCHAR buf[TOOLTIP_LENGTH+1];

  CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
  int nColumnCount = pHeader->GetItemCount();
  if( nCol >= nColumnCount)
    return FALSE;

  if( (GetStyle() & LVS_TYPEMASK) != LVS_REPORT )
    return FALSE;

  // Get the header height
  RECT rect;
  pHeader->GetClientRect( &rect );

  RECT rctooltip;
  rctooltip.top = 0;
  rctooltip.bottom = rect.bottom;

  // Now get the left and right border of the column
  rctooltip.left = 0 - GetScrollPos( SB_HORZ );
  for( int i = 0; i < nCol; i++ )
    rctooltip.left += GetColumnWidth( i );
  rctooltip.right = rctooltip.left + GetColumnWidth( nCol );

  if( sTip == nullptr )
  {
    // Get column heading
    LV_COLUMN lvcolumn;
    lvcolumn.mask = LVCF_TEXT;
    lvcolumn.pszText = buf;
    lvcolumn.cchTextMax = TOOLTIP_LENGTH;
    if( !GetColumn( nCol, &lvcolumn ) )
      return FALSE;
  }

  m_tooltipHeader.AddTool(GetDlgItem(0), sTip ? sTip : buf, &rctooltip, nCol+1 );
  return TRUE;
}

void CAi_ListCtrlPicker::RecalcHeaderTips()
{
   // Update the tooltip info
   CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
   RECT rect;
   pHeader->GetClientRect(&rect);
   RECT rctooltip;
   rctooltip.top = 0;
   rctooltip.bottom = rect.bottom;
   rctooltip.left = 0 - GetScrollPos(SB_HORZ);
   CToolInfo toolinfo;
   toolinfo.cbSize = sizeof(toolinfo);
   // Cycle through the tooltipinfo for each column
   int numcol = pHeader->GetItemCount();
   for (int col = 0; col < numcol; col++ )
   {
      m_tooltipHeader.GetToolInfo(toolinfo,pHeader,col+2);
      rctooltip.right = rctooltip.left + GetColumnWidth(col+1);
      toolinfo.rect = rctooltip;
      m_tooltipHeader.SetToolInfo (&toolinfo);
      rctooltip.left += GetColumnWidth(col+1);
   }
}

BOOL CAi_ListCtrlPicker::PreTranslateMessage(MSG* pMsg)
{
  if (m_tooltipHeader.m_hWnd)
    m_tooltipHeader.RelayEvent(pMsg);

  if (m_columnWidths.size() == 0)
  {
    FormColumnWidthsList();
    RecalcHeaderTips();
    ColumnWidthsWasChanged();
  }
  if (pMsg->message == WM_MOUSEMOVE)
    if (WasColumnWidthsChanged())
    {
      RecalcHeaderTips();
      ColumnWidthsWasChanged();
    }
  
  return CMFCListCtrl::PreTranslateMessage(pMsg);
}

void CAi_ListCtrlPicker::FormColumnWidthsList()
{
  m_columnWidths.clear();
  CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
  if (pHeader)
  {
    int numcol = pHeader->GetItemCount();
    for (int col = 0; col <= numcol; col++ )
      m_columnWidths.push_back(GetColumnWidth(col));
  }
}

bool CAi_ListCtrlPicker::WasColumnWidthsChanged()
{
  if (m_columnWidths.size() == 0)
  {
    FormColumnWidthsList();
    return true;
  }
  CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
  if (pHeader)
  {
    int numcol = pHeader->GetItemCount();
    if ((int)m_columnWidths.size() == numcol)
    {
      FormColumnWidthsList();
      return true;
    }
    for (int col = 0; col <= numcol; col++ )
      if (m_columnWidths[col] != GetColumnWidth(col))
      {
        FormColumnWidthsList();
        return true;
      }
  }
  return false;
}

BOOL CAi_ListCtrlPicker::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
  HD_NOTIFY	*pHDN = (HD_NOTIFY*)lParam;

  if((pHDN->hdr.code == HDN_ENDTRACKA || pHDN->hdr.code == HDN_ENDTRACKW))
  {
    // Update the tooltip info
    CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();

    CToolInfo toolinfo;
    toolinfo.cbSize = sizeof( toolinfo );

    // Cycle through the tooltipinfo for each effected column
    for( int i = pHDN->iItem; i <= nColumnCount; i++ )
    {
      m_tooltipHeader.GetToolInfo( toolinfo, pHeader, i + 1 );

      int dx = 0;				// store change in width
      if( i == pHDN->iItem )
        dx = pHDN->pitem->cxy - (toolinfo.rect.right - toolinfo.rect.left);
      else 
        toolinfo.rect.left += dx;
      toolinfo.rect.right += dx;
      m_tooltipHeader.SetToolInfo( &toolinfo );
    }
  }
  
  return CListCtrl::OnNotify(wParam, lParam, pResult);
}