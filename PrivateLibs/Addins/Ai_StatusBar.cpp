#include "stdafx.h"
#include <vector>
#include "Ai_StatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;

CAi_StatusBar::CAi_StatusBar() :
m_bUse(false),
m_drawMode(dmNone),
m_nDrawInPane(-1)
{
}

CAi_StatusBar::~CAi_StatusBar()
{
}

void CAi_StatusBar::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (m_bUse &&
		m_drawMode != dmNone &&
		lpDrawItemStruct->itemID == (UINT)m_nDrawInPane)
	{
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CDC srcDC; // select current bitmap into a compatible CDC
    CBitmap *pBitmap;
    CRect rect(&lpDrawItemStruct->rcItem); // Get the pane rectangle and calculate text coordinates

    srcDC.CreateCompatibleDC(nullptr);
    pBitmap = (m_drawMode == dmFirst) ? &m_btmFirsMode : &m_btmSecondMode;
    CBitmap* pOldBitmap = srcDC.SelectObject(pBitmap);

    dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &srcDC, 0, 0, SRCCOPY); // BitBlt to pane rect
    srcDC.SelectObject(pOldBitmap);

    dc.Detach(); // Detach from the CDC object, otherwise the hDC will be destroyed when the CDC object goes out of scope
	}
}

void CAi_StatusBar::SetPaneDraw(int nDrawInPane, UINT nIDFirstModeBitmap, UINT nIDSecondModeBitmap)
{
	if (nDrawInPane < 0 || nIDFirstModeBitmap < 0 &&  nIDSecondModeBitmap < 0)
		return;

	m_bUse = true;
	m_nDrawInPane = nDrawInPane;
	m_btmFirsMode.LoadBitmap(nIDFirstModeBitmap);
	m_btmSecondMode.LoadBitmap(nIDSecondModeBitmap);
}

void CAi_StatusBar::SetDrawMode(DRAW_MODE newDrawMode)
{
	m_drawMode = newDrawMode;
	Invalidate();
}

void CAi_StatusBar::DrawIcons()
{
	COLORREF colorBall;

	if (m_drawMode == dmFirst)
		colorBall = RGB(255, 0, 0);
	else
	if (m_drawMode == dmSecond)
		colorBall = RGB(255, 0, 0);
	else
		return;
}