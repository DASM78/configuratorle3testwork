#pragma once

#include <vector>
#include "AddinsDef.h"

class ADDINS_API CAi_COMPortList
{
public:
  CAi_COMPortList(void);
  ~CAi_COMPortList(void);

  std::vector<CString> m_outList;
  bool GetCOMPortList(std::vector<CString>* pOutList = nullptr); // out list put in m_outList also

private:
  std::vector<CString>* m_pOutList;
  bool QueryKey(HKEY hKey);
};
