#pragma once

#include "AddinsDef.h"

struct ADDINS_API GET_REG_PARAM
{
  HKEY hKey = 0;
  CString strSubKey;
  CString strParamName;
  DWORD nType = 0; // REG_DWORD, REG_MULTI_SZ...

  CString strMean;
  DWORD nMeaning = 0;
};

class ADDINS_API CAi_Registry
{
public:
  CAi_Registry(void);
  ~CAi_Registry(void);

public:
  enum GetRegResult
  {
    grrSuccess,
    grrAbsent,
    grrErrorParameter,
    grrErrorAPIFunc
  };
  GetRegResult GetRegParam(GET_REG_PARAM* pGRP);
private:
  GET_REG_PARAM* m_pGRP;
  GetRegResult QueryKey(HKEY hKey);

public:
  bool SetValue(GET_REG_PARAM* pGRP);
  bool CreateAndSetValue(GET_REG_PARAM* pGRP);
private:
  bool m_bCreateParamName;
};
