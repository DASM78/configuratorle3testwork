#pragma once

// CAi_CmbBxDO - Dynamically Offering Combo Box ctrl

#include <vector>
#include "AddinsDef.h"

class ADDINS_API CAi_CmbBxDO : public CComboBox
{
public:
  CAi_CmbBxDO();
  virtual ~CAi_CmbBxDO();

  bool m_bAddNewStringByEnterKeyDown;
  bool m_bMsgBeforeAddNewStringByEnterKeyDown;
  bool m_bMsgIfAddedItemExistsAlready;
  int m_nIDWithMsg;
  CString m_strMsgIfAddedItemExistsAlready;
  HWND m_hWndForMsgAfterInnerEvent;
  CString m_strNewAddingStr;
  CString m_strSelectedStr;

  BOOL m_bAutoComplete;
  BOOL m_bAutoCompleteSwitch;
  BOOL m_bDropDown;

  BOOL AddNewStrSelect(CString str);
  BOOL AddNewStrNoSelect(CString str);
  void FillInNewStrings(std::vector<CString> *strList);
  void GetAllList(std::vector<CString> *strList);
  CString GetCurSelected();
  bool IsTheTextInList(CString strFoundText);
  bool IsTheTextInList();

private:
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnEditupdate();
  afx_msg void OnDropdown();
  void TestShowString();
  void OnSelchange();

  DECLARE_MESSAGE_MAP()
};