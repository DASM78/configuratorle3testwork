#include "StdAfx.h"
#include "shlwapi.h"
#include "Ai_COMPortList.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

CAi_COMPortList::CAi_COMPortList()
{
}

CAi_COMPortList::~CAi_COMPortList()
{
}

bool CAi_COMPortList::GetCOMPortList(vector<CString>* pOutList)
{
  m_outList.clear();
  m_pOutList = pOutList;
  if (m_pOutList != nullptr)
    m_pOutList->clear();

   HKEY hTestKey;
   bool bResult = true;

   if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,
        _T("HARDWARE\\DEVICEMAP\\SERIALCOMM"),
        0,
        KEY_READ,
        &hTestKey) == ERROR_SUCCESS)
      bResult = QueryKey(hTestKey);
   RegCloseKey(hTestKey);
   return bResult;
}

bool CAi_COMPortList::QueryKey(HKEY hKey) 
{ 
    TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string 
    TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
    DWORD    cchClassName = MAX_PATH;  // size of class string 
    DWORD    cSubKeys=0;               // number of subkeys 
    DWORD    cbMaxSubKey;              // longest subkey size 
    DWORD    cchMaxClass;              // longest class string 
    DWORD    cValues;              // number of values for key 
    DWORD    cchMaxValue;          // longest value name 
    DWORD    cbMaxValueData;       // longest value data 
    DWORD    cbSecurityDescriptor; // size of security descriptor 
    FILETIME ftLastWriteTime;      // last write time 
 
    DWORD i, retCode; 
 
    TCHAR  achValue[MAX_VALUE_NAME]; 
    DWORD cchValue = MAX_VALUE_NAME; 
 
    // Get the class name and the value count. 
    retCode = RegQueryInfoKey(
        hKey,                    // key handle 
        achClass,                // buffer for class name 
        &cchClassName,           // size of class string 
        nullptr,                    // reserved 
        &cSubKeys,               // number of subkeys 
        &cbMaxSubKey,            // longest subkey size 
        &cchMaxClass,            // longest class string 
        &cValues,                // number of values for this key 
        &cchMaxValue,            // longest value name 
        &cbMaxValueData,         // longest value data 
        &cbSecurityDescriptor,   // security descriptor 
        &ftLastWriteTime);       // last write time 
 
    // Enumerate the subkeys, until RegEnumKeyEx fails.
    if (cSubKeys)
        for (i=0; i<cSubKeys; ++i) 
        { 
            cbName = MAX_KEY_LENGTH;
            retCode = RegEnumKeyEx(hKey, i,
                     achKey, 
                     &cbName, 
                     nullptr, 
                     nullptr, 
                     nullptr, 
                     &ftLastWriteTime); 
            if (retCode == ERROR_SUCCESS)
              return false;
        }
 
    // Enumerate the key values. 

    if (cValues) 
        for (i=0, retCode=ERROR_SUCCESS; i<cValues; ++i) 
        { 
            cchValue = MAX_VALUE_NAME; 
            achValue[0] = '\0'; 
            retCode = RegEnumValue(hKey, i, 
                achValue, 
                &cchValue, 
                nullptr, 
                nullptr,
                nullptr,
                nullptr);
 
            if (retCode == ERROR_SUCCESS )
            {
              DWORD nType = REG_MULTI_SZ;
              TCHAR szValue[128] = {0};
              DWORD dwLen = 128;

              LSTATUS st = SHGetValue(
                HKEY_LOCAL_MACHINE,
                _T("HARDWARE\\DEVICEMAP\\SERIALCOMM"),
                achValue,
                &nType,
                szValue,
                &dwLen);

              if(ERROR_SUCCESS == st)
              {
                m_outList.push_back(CString(szValue));
                if (m_pOutList != nullptr)
                  m_pOutList->push_back(CString(szValue));
              }
            }
        }

  return true;
}