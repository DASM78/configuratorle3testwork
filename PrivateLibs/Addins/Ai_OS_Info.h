#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_OS_Info
{
public:
  CAi_OS_Info(void);
  ~CAi_OS_Info(void);

  BOOL GetOSDisplayString(LPTSTR pszOS, BOOL* pb64);
};

