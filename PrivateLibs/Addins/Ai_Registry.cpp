#include "StdAfx.h"
#include "shlwapi.h"
#include "Ai_Registry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#pragma comment(lib, "shlwapi.lib")

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

CAi_Registry::CAi_Registry(void)
  : m_bCreateParamName(false)
{
}

CAi_Registry::~CAi_Registry(void)
{
}

CAi_Registry::GetRegResult CAi_Registry::GetRegParam(GET_REG_PARAM* pGRP)
{
  ASSERT(pGRP);
  if (pGRP == nullptr)
    return grrErrorParameter;

  HKEY hTestKey;
  GetRegResult bResult = grrErrorAPIFunc;
  if (RegOpenKeyEx(pGRP->hKey, pGRP->strSubKey, 0, KEY_READ, &hTestKey) == ERROR_SUCCESS)
  {
    m_pGRP = pGRP;
    bResult = QueryKey(hTestKey);
  }
  RegCloseKey(hTestKey);
  return bResult;
}

CAi_Registry::GetRegResult CAi_Registry::QueryKey(HKEY hKey) 
{ 
  TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
  DWORD    cbName;                   // size of name string 
  TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
  DWORD    cchClassName = MAX_PATH;  // size of class string 
  DWORD    cSubKeys=0;               // number of subkeys 
  DWORD    cbMaxSubKey;              // longest subkey size 
  DWORD    cchMaxClass;              // longest class string 
  DWORD    cValues;              // number of values for key 
  DWORD    cchMaxValue;          // longest value name 
  DWORD    cbMaxValueData;       // longest value data 
  DWORD    cbSecurityDescriptor; // size of security descriptor 
  FILETIME ftLastWriteTime;      // last write time 

  DWORD i, retCode; 

  TCHAR  achValue[MAX_VALUE_NAME]; 
  DWORD cchValue = MAX_VALUE_NAME; 

  // Get the class name and the value count. 
  retCode = RegQueryInfoKey(
      hKey,                    // key handle 
      achClass,                // buffer for class name 
      &cchClassName,           // size of class string 
      nullptr,                    // reserved 
      &cSubKeys,               // number of subkeys 
      &cbMaxSubKey,            // longest subkey size 
      &cchMaxClass,            // longest class string 
      &cValues,                // number of values for this key 
      &cchMaxValue,            // longest value name 
      &cbMaxValueData,         // longest value data 
      &cbSecurityDescriptor,   // security descriptor 
      &ftLastWriteTime);       // last write time 

  // Enumerate the subkeys, until RegEnumKeyEx fails.
  if (cSubKeys)
    for (i=0; i<cSubKeys; ++i) 
    { 
        cbName = MAX_KEY_LENGTH;
        retCode = RegEnumKeyEx(hKey, i,
                 achKey, 
                 &cbName, 
                 nullptr, 
                 nullptr, 
                 nullptr, 
                 &ftLastWriteTime); 
        if (retCode == ERROR_SUCCESS)
          return grrErrorAPIFunc;
    }

  // Enumerate the key values. 

  if (cValues)
  {
    for (i=0, retCode=ERROR_SUCCESS; i<cValues; ++i) 
    { 
      cchValue = MAX_VALUE_NAME; 
      achValue[0] = '\0';
       retCode = RegEnumValue(hKey, i, 
          achValue, 
          &cchValue, 
          nullptr, 
          nullptr,
          nullptr,
          nullptr);

      if (retCode == ERROR_SUCCESS && CString(achValue) == m_pGRP->strParamName)
      {
        DWORD nType = m_pGRP->nType;
        TCHAR szValue[128] = {0};
        DWORD dwLen = 128;
        LSTATUS st = SHGetValue(m_pGRP->hKey, m_pGRP->strSubKey, achValue, &nType, szValue, &dwLen);

        if(ERROR_SUCCESS == st)
        {
          switch (m_pGRP->nType)
          {
            case REG_DWORD:
              memcpy(&m_pGRP->nMeaning, (DWORD*)szValue, sizeof(DWORD));
            case REG_SZ:
              m_pGRP->strMean = CString(szValue);
              break;
          };
          return grrSuccess;
        }
      }
    }
    return grrAbsent;
  }
  return grrErrorAPIFunc;
}

bool CAi_Registry::SetValue(GET_REG_PARAM* pGRP)
{
	HKEY hKey = 0;
	DWORD dwDisposition = 0;
  CString strParamName = pGRP->strSubKey + _T("\\") + pGRP->strParamName;
  bool bSet = false;

  if (m_bCreateParamName)
  {
    if (RegCreateKeyEx(pGRP->hKey, pGRP->strSubKey, 0, 0, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0, &hKey, &dwDisposition) != ERROR_SUCCESS)
      return false;
    else
      bSet = true;
  }

  switch (pGRP->nType)
  {
    case REG_DWORD:
      if (!m_bCreateParamName
        && RegOpenKeyEx(pGRP->hKey, pGRP->strSubKey, 0, KEY_WRITE, &hKey) == ERROR_SUCCESS)
        bSet = true;
      if (bSet
        && RegSetValueEx(hKey, pGRP->strParamName,	0, REG_DWORD, (BYTE*)&pGRP->nMeaning,  sizeof(DWORD)) == ERROR_SUCCESS)
        return true;
      break;

    case REG_SZ:
      if (!m_bCreateParamName
          && RegOpenKeyEx(pGRP->hKey, pGRP->strSubKey, 0, KEY_WRITE, &hKey) == ERROR_SUCCESS)
          bSet = true;
      if (bSet
          && RegSetValueEx(hKey, pGRP->strParamName, 0, REG_SZ, (BYTE*)pGRP->strMean.GetBuffer(), (pGRP->strMean.GetLength() + 1) * 2) == ERROR_SUCCESS) // * 2 - unicode
          return true;
      break;

  };
  return false;
}

bool CAi_Registry::CreateAndSetValue(GET_REG_PARAM* pGRP)
{
  m_bCreateParamName = true;
  bool bResult = SetValue(pGRP);
  m_bCreateParamName = false;
  return bResult;
}
