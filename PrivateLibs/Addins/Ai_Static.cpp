#include "stdafx.h"
#include "Ai_Str.h"
#include "Ai_Static.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAi_Static::CAi_Static()
  : m_pFont(nullptr)
  , m_nLeftMargin(2)
  , m_nTopMargin(0)
  , m_bEnable(true)
{
  SetStartRGB();
  SetGradient();
  StopIfEqual();
  ForwardOrBack(fbForward);
  HorizontalOrVertical(hvHorizontal);
  SetStep();
  WorkWithColor();
  SetText(_T(""));
  SetTextAlign();
  m_nFrameLineThickness = 0;
}

CAi_Static::~CAi_Static()
{
}

BEGIN_MESSAGE_MAP(CAi_Static, CStatic)
  ON_WM_PAINT()
END_MESSAGE_MAP()

void CAi_Static::OnPaint()
{
  CPaintDC dc(this);
  CRect tRect;
  GetClientRect(tRect);
  double R = m_nR, G = m_nG, B = m_nB;
  CRect nRect(tRect);
  int nStep = m_nStep;

  CPen pen(PS_SOLID, 1, RGB((int)R, (int)G, (int)B));
  CPen* pPenOld = dc.SelectObject(&pen);

  int nFrom = tRect.left; // for hvHorizontal
  int nTo = tRect.right; // for hvHorizontal
  if (m_horizontalOrVertical == hvVertical)
  {
    nFrom = tRect.top;
    nTo = tRect.bottom;
  }
  
  for(int j = nFrom; j < nTo; j += nStep) 
  {
    if (m_horizontalOrVertical == hvHorizontal)
    {
      nRect.left = j; 
      nRect.right = j + nStep;
      if (nRect.right > nTo)
        nRect.right = nTo;
    }
    else
    {
      nRect.top = j; 
      nRect.bottom = j + nStep;
      if (nRect.bottom > nTo)
        nRect.bottom = nTo;
    }

    CBrush _brush; 
    _brush.CreateSolidBrush(RGB((int)R, (int)G, (int)B));
    dc.FillRect(&nRect, &_brush);
    _brush.DeleteObject();

    if (m_bR)
      if (m_nStopR != -1)
        if (m_forwardOrBack == fbForward)
        {
          if (R < m_nStopR)
            R += m_nGradient;
        }
        else
        {
          if (R > m_nStopR)
            R -= m_nGradient;
        }
    if (R < 0)
      R = 0;
    if (R > 255)
      R = 255;

    if (m_bG)
      if (m_nStopG != -1)
        if (m_forwardOrBack == fbForward)
        {
          if (G < m_nStopG)
            G += m_nGradient;
        }
        else
        {
          if (G > m_nStopG)
            G -= m_nGradient;
        }
    if (G < 0)
      G = 0;
    if (G > 255)
      G = 255;

    if (m_bB)
      if (m_nStopB != -1)
        if (m_forwardOrBack == fbForward)
        {
          if (B < m_nStopB)
            B += m_nGradient;
        }
        else
        {
          if (B > m_nStopB)
            B -= m_nGradient;
        }
    if (B < 0)
      B = 0;
    if (B > 255)
      B = 255;
  }

  if (!m_strText.IsEmpty())
  {
    dc.SetTextColor(m_nTextColor);
    dc.SetBkMode(TRANSPARENT);
    
    vector<CString> strs;
    int nVertStrPoint = 0;
    int nNextStrOffset = 0;
    int nLeftStrPoint = 0;

    switch (m_textAlign)
    {
      case taLeftCenter:
        CalcForVerticalAlign(&dc, m_strText, nVertStrPoint, nNextStrOffset, &strs);
        break;

      case taCenterCenter:
        CalcForVerticalAlign(&dc, m_strText, nVertStrPoint, nNextStrOffset, &strs);
        CalcForHorizontalAlign(&dc, m_strText, nLeftStrPoint);
        break;

      default:
        CAi_Str::CutString(m_strText, L"\r\n", &strs);
        break;
    }
    
    CFont* pFontOld = nullptr;
    if (m_pFont != nullptr)
      pFontOld = dc.SelectObject(m_pFont);

    for (size_t str_s = 0; str_s < strs.size(); str_s++, nVertStrPoint += nNextStrOffset)
      dc.TextOut(nLeftStrPoint + m_nLeftMargin, nVertStrPoint + m_nTopMargin, strs[str_s]);

    if (pFontOld != nullptr)
      dc.SelectObject(pFontOld);
  }

  if (m_nFrameLineThickness > 0)
  {
    CBrush _brush; 
    _brush.CreateSolidBrush(m_nFrameColor);

    CRect frameRect(tRect.left, tRect.top, tRect.right, tRect.top + m_nFrameLineThickness);
    dc.FillRect(&frameRect, &_brush);
    frameRect.top = tRect.bottom - m_nFrameLineThickness;
    frameRect.bottom = tRect.bottom;
    dc.FillRect(&frameRect, &_brush);
    frameRect.top = tRect.top + m_nFrameLineThickness;
    frameRect.right = frameRect.left + m_nFrameLineThickness;
    frameRect.bottom = tRect.bottom - m_nFrameLineThickness;
    dc.FillRect(&frameRect, &_brush);
    frameRect.left = tRect.right - m_nFrameLineThickness;
    frameRect.right = tRect.right;
    dc.FillRect(&frameRect, &_brush);

    _brush.DeleteObject();
  }

  dc.SelectObject(pPenOld);

  OnPaintAfter(&dc);
}

void CAi_Static::CalcForVerticalAlign(CPaintDC* pDC, CString sIn, int& nVertStrPoint, int& nNextStrOffset, vector<CString>* pResStrs)
{
  CAi_Str::CutString(sIn, L"\r\n", pResStrs);

  int cy = pDC->GetTextExtent(L"W").cy;
  CRect r;
  GetClientRect(&r);
  nVertStrPoint = (r.Height() / 2) - (cy / 2); // �������� ��� ������ � ���� ������

  int nRows = (int)pResStrs->size(); // ������� ����� �����
  int nRowsHeight = (cy + 4) * nRows; // ������ ���� ����� ������; 4 - ���������� ����� �������
  nVertStrPoint = (r.Height() / 2) - (nRowsHeight / 2); // ������ ������ ������
  nNextStrOffset = cy + 4; // ��� ��� ���������
}

void CAi_Static::CalcForHorizontalAlign(CPaintDC* pDC, CString sIn, int& nLeftStrPoint)
{
  int nMaxStrLength = 0;
  vector<CString> resStrs;
  CAi_Str::CutString(sIn, L"\r\n", &resStrs);
  for (auto i : resStrs)
  {
    int cx = pDC->GetTextExtent(i).cx;
    nMaxStrLength = max(nMaxStrLength, cx);
  }

  CRect r;
  GetClientRect(&r);
  nLeftStrPoint = (r.Width() / 2); // �������� ��� ������ � ���� ������
  nLeftStrPoint -= (nMaxStrLength / 2);
}

void CAi_Static::SetStep(int nStep)
{
  if (nStep <= 0)
    nStep = 1;
  m_nStep = nStep;
}

void CAi_Static::SetOtherFont(CFont* pFont)
{
  if (pFont == nullptr)
    return;
  m_pFont = pFont;
}
