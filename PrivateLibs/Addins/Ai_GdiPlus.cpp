#include "stdafx.h"
#include "Ai_GdiPlus.h"

using namespace std;
using namespace Gdiplus;

/*#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif*/

#define GDI_ZOOM_COUNT 20

namespace ns_AiG
{
  Graphics* g_pG = nullptr;
  BYTE g_nZooms[GDI_ZOOM_COUNT]{};

  CRect g_rClient(0, 0, 0, 0);
  CRect g_rGraph(0, 0, 0, 0);
  CRect g_rXSteps(0, 0, 0, 0);
  CRect g_rYSteps(0, 0, 0, 0);

  CRect g_rZoomPlus(0, 0, 0, 0);
  CRect g_rZoomMinus(0, 0, 0, 0);

  CString g_sFontName = L"Arial";
  REAL g_nFontSize = 10;

  Color g_brhColor(255, 255, 255);

  Color g_penColor(255, 255, 255);
  REAL g_nLineThickness = 1;

  /*enum EOverZoom
  {
    ozNone,
    ozPlus,
    ozMinus,
  };
  EOverZoom g_overZoom = ozNone;
  int g_nIdxOfRuler = -1;
  bool g_bZoomClick = false;*/

  INT g_nX_margin = 0;
  INT g_nY_margin = 0;

} // ns_AiG

bool IsInRect(CRect& r, CPoint& p)
{
  if (p.x > r.left && p.x < r.right
      && p.y > r.top && p.y < r.bottom)
      return true;
  return false;
}

CAi_GdiPlus::CAi_GdiPlus()
{
}

CAi_GdiPlus::~CAi_GdiPlus()
{
}

void CAi_GdiPlus::Set(CRect& rClient, Graphics* pG)
{
  ASSERT(pG);
  ns_AiG::g_rClient = rClient;
  ns_AiG::g_pG = pG;

  ns_AiG::g_rGraph = CRect(0, 0, 0, 0);
}

void CAi_GdiPlus::Clear(Color c)
{
  ASSERT(ns_AiG::g_pG);
  const SolidBrush brhBg(c);
  ns_AiG::g_pG->FillRectangle(&brhBg, 0, 0, ns_AiG::g_rClient.Width(), ns_AiG::g_rClient.Height());
}

void CAi_GdiPlus::UseFont(CString sName, int nSize)
{
  ns_AiG::g_sFontName = sName;
  ns_AiG::g_nFontSize = static_cast<REAL>(nSize);
}

void CAi_GdiPlus::UseBrushColor(Color c)
{
  ns_AiG::g_brhColor = c;
}

void CAi_GdiPlus::UsePen(Color c, int nThickness)
{
  ns_AiG::g_penColor = c;
  ns_AiG::g_nLineThickness = static_cast<REAL>(nThickness);
}

void CAi_GdiPlus::DrawCoordinateAxises(CString sX, CString sY, int nYmargin, int nXmargin)
{
  ns_AiG::g_nX_margin = nXmargin;
  ns_AiG::g_nY_margin = nYmargin;
  Graphics* g = ns_AiG::g_pG;
  Gdiplus::Font fnt(ns_AiG::g_sFontName, ns_AiG::g_nFontSize);
  SolidBrush brh(ns_AiG::g_brhColor);
  Pen pen(ns_AiG::g_penColor, ns_AiG::g_nLineThickness);

  // y text

  RectF layoutR(static_cast<REAL>(nYmargin) / 2, 10, 100, 60);
  PointF py(layoutR.GetLeft(), layoutR.GetTop());
  RectF boundRy;
  g->MeasureString(sX, sX.GetLength(), &fnt, layoutR, &boundRy);
  g->DrawString(sX, sX.GetLength(), &fnt, py, &brh);

  // x text

  layoutR.X = static_cast<REAL>(ns_AiG::g_rClient.Width() - 50);
  layoutR.Y = static_cast<REAL>(ns_AiG::g_rClient.Height() - 80);
  RectF rx(layoutR.GetLeft() + 30, layoutR.GetTop(), 14, REAL(sY.GetLength() * 15));
  RectF boundRx;
  g->MeasureString(sY, sY.GetLength(), &fnt, layoutR, &boundRx);
  /*if ((layoutR.GetLeft() + boundRx.Width) > ns_AiG::g_rClient.right) // ����� ��������� �� �����
    rx.X = ns_AiG::g_rClient.right - boundRx.Width - 3;*/
  StringFormat sf;
  sf.SetFormatFlags(StringFormatFlagsDirectionVertical);
  g->DrawString(sY, sY.GetLength(), &fnt, rx, &sf, &brh);

  // ��������� ��������������

  INT nHalfTriangle = 6;

                /* ����� ��� 'y' �������������� ���:
                       1

                0,3      2
                */

  Point pntsY_Arrow[4]{};
  pntsY_Arrow[0].X = nYmargin - nHalfTriangle;
  pntsY_Arrow[0].Y = 55;
  pntsY_Arrow[1].X = pntsY_Arrow[0].X + nHalfTriangle;
  pntsY_Arrow[1].Y = pntsY_Arrow[0].Y - (nHalfTriangle * 2);
  pntsY_Arrow[2].X = pntsY_Arrow[1].X + nHalfTriangle;
  pntsY_Arrow[2].Y = pntsY_Arrow[0].Y;
  pntsY_Arrow[3].X = pntsY_Arrow[0].X;
  pntsY_Arrow[3].Y = pntsY_Arrow[0].Y;
  g->FillPolygon(&brh, pntsY_Arrow, 4);

                /* ����� ��� 'x' �������������� ���:
                1
                        2
                0,3
                */

  Point pntsX_Arrow[4]{};
  pntsX_Arrow[0].X = ns_AiG::g_rClient.right - (nHalfTriangle * 3);
  pntsX_Arrow[0].Y = static_cast<INT>(boundRx.GetTop()) - 5;
  pntsX_Arrow[1].X = pntsX_Arrow[0].X;
  pntsX_Arrow[1].Y = pntsX_Arrow[0].Y - (nHalfTriangle * 2);
  pntsX_Arrow[2].X = pntsX_Arrow[1].X + (nHalfTriangle * 2);
  pntsX_Arrow[2].Y = pntsX_Arrow[1].Y + nHalfTriangle;
  pntsX_Arrow[3].X = pntsX_Arrow[0].X;
  pntsX_Arrow[3].Y = pntsX_Arrow[0].Y;
  g->FillPolygon(&brh, pntsX_Arrow, 4);

  // ��� x, y

  //pen.SetEndCap(LineCapTriangle);
  //g->DrawLine(&pen, nXmargin, 55, nXmargin, static_cast<INT>(boundRx.GetTop()) - 5); // y
  g->DrawLine(&pen, pntsY_Arrow[1].X, pntsY_Arrow[0].Y, pntsY_Arrow[1].X, pntsX_Arrow[2].Y); // y
  g->DrawLine(&pen, pntsY_Arrow[1].X, pntsX_Arrow[2].Y, pntsX_Arrow[1].X, pntsX_Arrow[2].Y); // x

  // ������ ������� �������� - ��� ���������� ���� �������, �����, ����� ��� � � �

  INT nTh = static_cast<INT>(ns_AiG::g_nLineThickness);
  ns_AiG::g_rGraph =
    //CRect(nXmargin + nTh, nYmargin + 10, static_cast<int>(boundRx.Width) - nXmargin - 10, static_cast<int>(boundRx.Height) - nYmargin - nTh);
    CRect(pntsY_Arrow[1].X + nTh, pntsY_Arrow[0].Y + 10, pntsX_Arrow[0].X - 10, pntsX_Arrow[2].Y - nTh);
}

void CAi_GdiPlus::DrawYGuidlines(vector<CString>& textOfMarks)
{
  Graphics* g = ns_AiG::g_pG;

  SolidBrush brush(Color(0x70, 255, 255, 255));
  SolidBrush brushT(Color::White);
  Pen pen(Color(0xA0, 255, 255, 255), 1);
  Pen pen2(Color(0xA0, 255, 255, 255), 1);
  pen.SetDashStyle(DashStyleDashDotDot);

  INT w = 50;
  INT h = 14;
  INT rm = 10;
  INT x = ns_AiG::g_rGraph.left - w - (INT)ns_AiG::g_nLineThickness - rm;
  INT y = ns_AiG::g_rGraph.bottom + (INT)ns_AiG::g_nLineThickness;
  INT x_l1 = x + w;
  INT x_l2 = ns_AiG::g_rGraph.right;
  int nGrH = ns_AiG::g_rGraph.Height();
  int nPieces = (int)textOfMarks.size() - 1;
  int nStep = (int)((nGrH / (double)nPieces) + 0.5);
  Gdiplus::Font fnt(ns_AiG::g_sFontName, ns_AiG::g_nFontSize);

  for (size_t i = 0; i < textOfMarks.size(); ++i)
  {
    if (i == textOfMarks.size() - 1) // last
      y = ns_AiG::g_rGraph.top;
    g->FillRectangle(&brush, x, y, w, h);
    g->DrawLine(&pen, x_l1, y, x_l2, y);
    g->DrawLine(&pen2, x_l1, y, x_l1 + rm, y);
    PointF py(REAL(x + 1), REAL(y - 1));
    g->DrawString(textOfMarks[i], textOfMarks[i].GetLength(), &fnt, py, &brushT);
    y -= nStep;
  }
}

/*void CAi_GdiPlus::DrawZoomCtrl(BYTE zooms[])
{
  ZeroMemory(&ns_AiG::g_nZooms, GDI_ZOOM_COUNT);
  int nCnt = 0;
  while (zooms[nCnt] > 0)
  {
    ns_AiG::g_nZooms[nCnt] = zooms[nCnt];
    if (++nCnt > GDI_ZOOM_COUNT)
     break;
  }

  ns_AiG::g_rZoomPlus = CRect(ns_AiG::g_rClient.right - 25, 5, ns_AiG::g_rClient.right - 5, 25);
  ns_AiG::g_rZoomMinus = CRect(ns_AiG::g_rClient.right - 50, 30, ns_AiG::g_rClient.right - 30, 50);

  Graphics* g = ns_AiG::g_pG;

  SolidBrush brushPlus(Color::White);
  SolidBrush brushPlus2(Color::SteelBlue);
  Pen penPlus(Color::White, 2);
  Pen penMinus(Color::White, 2);

  if (ns_AiG::g_overZoom == ns_AiG::ozPlus)
  {
    if (ns_AiG::g_nIdxOfRuler == -1)
    {
      brushPlus.SetColor(Color::SteelBlue);
      brushPlus2.SetColor(Color::White);
      penPlus.SetColor(Color::DarkBlue);
    }
  }
  SolidBrush brushMinus(Color::White);
  SolidBrush brushMinus2(Color::SteelBlue);

  g->FillRectangle(&brushPlus, ns_AiG::g_rZoomPlus.left, ns_AiG::g_rZoomPlus.top, ns_AiG::g_rZoomPlus.Width(), ns_AiG::g_rZoomPlus.Height());
  g->FillRectangle(&brushMinus, ns_AiG::g_rZoomMinus.left, ns_AiG::g_rZoomMinus.top, ns_AiG::g_rZoomMinus.Width(), ns_AiG::g_rZoomMinus.Height());
  
  g->FillRectangle(&brushPlus2, ns_AiG::g_rZoomPlus.left + 3, ns_AiG::g_rZoomPlus.top + 3, ns_AiG::g_rZoomPlus.Width() - 6, ns_AiG::g_rZoomPlus.Height() - 6);
  g->FillRectangle(&brushMinus2, ns_AiG::g_rZoomMinus.left + 3, ns_AiG::g_rZoomMinus.top + 3, ns_AiG::g_rZoomMinus.Width() - 6, ns_AiG::g_rZoomMinus.Height() - 6);


  Point points[6]{};
  points[0] = Point(ns_AiG::g_rZoomPlus.left + 6, ns_AiG::g_rZoomPlus.top + 10);
  points[1] = Point(ns_AiG::g_rZoomPlus.right - 6, ns_AiG::g_rZoomPlus.top + 10);
  points[2] = Point(ns_AiG::g_rZoomPlus.left + 6 + 4, ns_AiG::g_rZoomPlus.top + 10); // back to center of "+"
  points[3] = Point(ns_AiG::g_rZoomPlus.right - 6 - 4, ns_AiG::g_rZoomPlus.top + 10); // back to center of "+"
  points[4] = Point(ns_AiG::g_rZoomPlus.left + 10, ns_AiG::g_rZoomPlus.top + 6);
  points[5] = Point(ns_AiG::g_rZoomPlus.left + 10, ns_AiG::g_rZoomPlus.bottom - 6);
  g->DrawLines(&penPlus, points, 6);

  Point points2[2]{};
  points2[0] = Point(ns_AiG::g_rZoomMinus.left + 6, ns_AiG::g_rZoomMinus.top + 10);
  points2[1] = Point(ns_AiG::g_rZoomMinus.right - 6, ns_AiG::g_rZoomMinus.top + 10);
  g->DrawLines(&penMinus, points2, 2);

  for (int i = 0; i < GDI_ZOOM_COUNT; ++i)
  {

  }
}*/

CRect CAi_GdiPlus::GetGraphRect()
{
  return ns_AiG::g_rGraph;
}

bool CAi_GdiPlus::CheckMouseMove(CPoint& p)
{
  bool bRedraw = false;

  /*ns_AiG::EOverZoom overZoom = ns_AiG::ozNone;
  if (IsInRect(ns_AiG::g_rZoomPlus, p))
    overZoom = ns_AiG::ozPlus;
  if (ns_AiG::g_overZoom != overZoom)
  {
    ns_AiG::g_overZoom = overZoom;
    bRedraw = true;
  }*/

  return bRedraw;
}