#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_CRC
{
public:
  CAi_CRC();

  static short SetCRCTo(unsigned char* pnH, unsigned char* pnL, unsigned char* pData , int nDataLen);
  static short CalcCRC(unsigned char* pData, int nDataLen);
  static short CalcCRC_FromStreem(char Data, short nDataLen);
};