#pragma once

#include "AddinsDef.h"

class ADDINS_API CAi_Bmp
{
public:
  CAi_Bmp(void);
  ~CAi_Bmp(void);

  bool GetBitmapSize(DWORD nResourceID, SIZE *pBMPSize);
  bool GetBitmapSize(CString strFileName, SIZE *pBMPSize);

  void SetBMP(CBitmap* pBmp);
private:
  CBitmap* m_pBmp;
  HBITMAP m_hBmp;
  BITMAP m_bm;
  BYTE* m_bmpBuffer;
public:
  bool MoveOutBMPFileToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos/*left top*/, CString strFileName);
  bool MoveOutBMPToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos = nullptr);
  bool MoveOutBMPToDC(HDC hDC, SIZE* pSpaceSize, POINT* pSpacePos/*left top*/, HBITMAP hBm, BITMAP* pBm);

  bool SaveBitmapInFile(HWND hWnd, HBITMAP HBM, CString strFileName);
};
