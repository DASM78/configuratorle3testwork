#pragma once

#include "AddinsDef.h"
#include <vector>
#include <gdiplus.h>
#include "RoundRect.h"

class ADDINS_API CAi_GdiPlus: public CRoundRect
{
public:
  CAi_GdiPlus();
  ~CAi_GdiPlus();

  void Set(CRect& rClient, Gdiplus::Graphics* pG);

  void Clear(Gdiplus::Color c); // ������� ����� View

  /*
  ���� � ������ �� ������ ���� ������������� ���� �����, ����, ������, �� ��� �����������
  ��� ��-��������� ��� ���������������� ���������� ��������
  */

  void UseFont(CString sName = L"Tahoma", int nSize = 10);
  void UseBrushColor(Gdiplus::Color c = Gdiplus::Color(255, 255, 255));
  void UsePen(Gdiplus::Color c = Gdiplus::Color(255, 255, 255), int nThickness = 1);

  // ---

  void DrawCoordinateAxises(CString sX, CString sY, int nYmargin = 100, int nXmargin = 100);
  void DrawYGuidlines(std::vector<CString>& textOfMarks);

  CRect GetGraphRect();

  bool CheckMouseMove(CPoint& p);
};

