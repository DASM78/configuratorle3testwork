#pragma once

#include "AddinsDef.h"

class CAi_Msg;

struct ADDINS_API MSG_PARAM
{
  friend class CAi_Msg;

  MSG_PARAM()
    : nT(msgtNumber)
    , nP(0)
  {}

  MSG_PARAM(int nParam)
    : nT(msgtNumber)
    , nP(nParam)
  {}

  MSG_PARAM(CString strParam)
    : nT(msgtString)
    , strP(strParam)
    , nP(-1)
  {}

private:
  
  enum MSG_PARAM_TYPE { msgtNumber = 0, msgtString };

  MSG_PARAM_TYPE nT;
  int nP;
  CString strP;
};

class ADDINS_API CAi_Msg
{
  CAi_Msg();

public:
  static int AfxMsgBox(UINT nType, CString strMsg, int nCntParams, ...);
private:
  static CString m_strMadeMessage;
public:
  static CString GetMadeMessage() { return m_strMadeMessage; }

private:
  static void InsertParam(CString& strMsg, CString strChange, CString strIns);
};