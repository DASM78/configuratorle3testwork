#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include <memory>
#include "Ai_ListCtrlEx.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "TabCtrlOvrd.h"
#include "afxwin.h"
#include "Model_.h"

class CView_Energy: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_Energy)

public:
  CView_Energy();
  virtual ~CView_Energy();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_DATA_ENERGY
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  // Tab

  CStatic m_stcTabSpace;
  CTabCtrlOvrd m_tabs;
  LRESULT OnChangeActiveTab(WPARAM wparam, LPARAM lparam);
  int m_nCurrEnergyTabIdx = 0;
  int m_nCurrEnergyTabIdx_prev = -1;
  
  CCommonListCtrl* m_plstCurrentData = nullptr;
  �Ai_ListCtrlEx* m_plstCurrentDataEx = nullptr;
  void CreateCurrentDataTab();
  void StopCurrentDataMonitoring();
  void StartCurrentDataMonitoring();
 
  CCommonListCtrl* m_plstDailyData = nullptr;
  �Ai_ListCtrlEx* m_plstDailyDataEx = nullptr;
  CCommonListCtrl* m_plstMonthlyData = nullptr;
  �Ai_ListCtrlEx* m_plstMonthlyDataEx = nullptr;
  CCommonListCtrl* m_plstDateForYears = nullptr;
  �Ai_ListCtrlEx* m_plstDateForYearsEx = nullptr;

public:
  enum DMY_List
  {
    dmyNone,
    dmyDaily,
    dmyMonthly,
    dmyForYears
  };
private:

  void GetDMYs(CCommonListCtrl*** pLst, �Ai_ListCtrlEx*** pLstEx, DMY_List& dmy);
  void CreateDMY_DataTab(DMY_List dmy);
  void FillInDMY_DataTab(DMY_List dmy);
  void ClearAllDMY_Rows(DMY_List dmy);

  virtual void OnConnectionStateWasChanged() override final;

  void CheckMeaningsViewByConnectionStatus();
  void CheckMeaningsViewByConnectionStatus_InCurrentTbl();
  void CheckMeaningsViewByConnectionStatus_InDMY_Tbl(DMY_List dmy);

  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  void UpdMeaningsByRecvdDeviceData_InCurrentTbl(mdl::teFrameTypes& ft);
  void UpdMeaningsByRecvdDeviceData_InDMY_Tbl(mdl::teFrameTypes& ft, DMY_List dmy);

  void FormAllReqsOfItemsOfDMY_Tbl(DMY_List dmy);
  bool IsReqItems_ForDMY_Tbl();
  int GetReqItemsFrom_ForDMY_Tbl(DMY_List dmy);
  
  bool IsRecreateDMY_Tbl(DMY_List dmy);

  // Load

  CMFCButton m_btnLoadEnergyFromDevice;
  afx_msg void OnBnClickedBtnLoadEnergyFromDevice();
  virtual void DoAfterClearLoadData() override final;
  void ShowLoadingSuccessResultMsg(DMY_List dmy);
  void ClearAllReqDataInDB_ForDMY_Tbl();
  void ReceiveData_ForDMY_Tbl(DMY_List dmy);
};
