#pragma once

#ifndef __AFXWIN_H__
  #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

class CElectricMeterApp : public CWinAppEx
{
public:
  CElectricMeterApp();

  DECLARE_MESSAGE_MAP()

private:
  CString m_sPathApp;
  ULONG_PTR m_GdiplusToken;

#ifdef _DEBUG
  bool m_bAdminRegim;
#endif
  bool m_bFlag1;

  virtual BOOL InitInstance();
  void ParseCommArgs();
  virtual int ExitInstance();
  afx_msg void OnAppAbout();
};

extern CElectricMeterApp theApp;

