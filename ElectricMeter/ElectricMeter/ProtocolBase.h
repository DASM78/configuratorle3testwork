#pragma once

#include "stdafx.h"

struct CONNECT_INIT
{
  CONNECT_INIT()
  {
    Free();
  }
  ~CONNECT_INIT()
  {
    Free();
  }
  void Free()
  {
    delete pchPassword;
    pchPassword = nullptr;
    delete pchDevAddress;
    pchDevAddress = nullptr;
    delete pchDevName;
    pchDevName = nullptr;
  }
  unsigned char* pchPassword = nullptr;
  unsigned char* pchDevAddress = nullptr;
  unsigned char* pchDevName = nullptr;
};

class CProtocolBase
{
public:
  CProtocolBase() { }
  virtual ~CProtocolBase() { }

  virtual void Connect(CONNECT_INIT* pInit) = 0;
  virtual void Disconnect() = 0;

  virtual LRESULT TimerTick() = 0;
  
  virtual LRESULT OnResultNotify(WPARAM wParam, LPARAM lParam) = 0;
};

