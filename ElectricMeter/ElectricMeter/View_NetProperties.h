#pragma once

#include "CommonViewForm.h"
#include "Ai_ListCtrlEx.h"
#include "Ai_Static.h"
#include "afxwin.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Model_.h"

class CNetPropDiagrams: public CAi_Static
{
public:
  CNetPropDiagrams();
  ~CNetPropDiagrams();

  virtual void OnPaintAfter(CPaintDC* pdc);
};

class CView_NetProperties: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_NetProperties)

public:
  CView_NetProperties();
  virtual ~CView_NetProperties();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_DATA_NET_PROPERTIES
  };

  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);  // overridden to draw this view
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  CCommonListCtrl m_lstNetProp;
  �Ai_ListCtrlEx* m_plstNetPropEx = nullptr;
  CNetPropDiagrams m_stcDiagrams;

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
public:
	
};
