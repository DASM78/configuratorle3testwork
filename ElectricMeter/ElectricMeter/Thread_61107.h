#pragma once

#include "Defines.h"
#include "Typedefs.h"
#include "RSCOMMng.h"
#include "Ai_Time.h"
#include "Ai_Str.h"
#include "Ai_Log.h"
#include "ExchangeData.h"
#include "DataStruct.h"
#include <vector>

enum ETreadPhase
{
  tphNone,
  tphReset,

  tphConnect, // ������������ � ��������

  tphBeginInitialisation, // �������� ������� �� ����������/�����
  tphReceiveIdentificationOfInitialisation, // ��������� �������������
  tphSendConfirmationAboutInitialisation, // �������� ������������� ���������� ������ ��������
  tphReceivePasswordReqiure, // ��������� ���������� ������������
  tphSendPassword, // �������� ������ �� ������������
  tphReceivePasswordResult, // ��������� ���������� �� ����������� �� ����������� ������

  tphMonitoringOfData,
  tphReceiveMonitoringData,

  tphRecordingOfData,
  tphRecordingOfDataConfirmation,

  tphPrepareDisconnection,
  tphSendBreakBeforeDisconnection,
  tphDisconnect, // ����������� �� ��������

  tphStop //���������� �����, ��� ��� ��������
};

extern CWnd* gpParentForThread;

extern void TypeTRACEMsg(CString sMsg);

extern void CreateExchThread(CWnd* pParentForThread);
extern void DestroyExchThread();

extern void SetPassword(unsigned char* pPass);
extern void SetDevAddress(unsigned char* pAddr);
extern void SetDevName(unsigned char* pName);
extern void SetThreadPhase(ETreadPhase tph);
extern void SetNextThreadPhase(ETreadPhase tph);
extern ETreadPhase GetNextThreadPhase();

extern void SetEvent_();

extern bool CheckTimePeriodBetweenReq();

extern void SetToMonitoringQueue(int nDataID, std::vector<std::pair<const unsigned char*, varOnce_t>>* pList);

extern void AddToSendingQueue(SEND_UNIT* pSendUnit);

//////////////////////////////////////////////////////////////////////////////

enum EUsingPort
{
  eRS_COM,
  eUSB,
  eEthernet
};

class CLinkThread
{
public:
  CLinkThread(); // create thread
  ~CLinkThread(); // delete thread

  CAi_Log m_logDlr;
  void TypeTRACEMsg(CString& sMsg);

public:
  void CreateExchangerThread();
  void DestroyExchangerThread();

  // --- Port

public:
  EUsingPort m_usingPort = eRS_COM;
  CRSCOMMng m_COMPort;

  // --- Connect / disconnect phase

public:
  void Connection();
  void Disconnection();

public:
  void ExchageData();

public:
  bool CheckExchTimePeriod(bool bBegin = true);
private:
  int m_nExchTimePeriod = 100; // 100 ms
  CAi_Time m_tmrDlr;
  CAi_Str m_strDlr;

  void FormResultAfterExch(
    bool bRes,
    bool bConfirmation,
    CString sCmd,
    CString sMsg,
    CString sDbgMsg,
    int nInnerErr = -1);

  const int m_nRepeatRead = 5;
public:
  void SetPassword(unsigned char* pPass);
  void SetDevAddress(unsigned char* pAddr);
  void SetDevName(unsigned char* pName);
private:
  unsigned char m_sPassword[MAX_PATH + 1];
  unsigned char m_sAddress[MAX_PATH + 1];
  CString m_sDevName;
  char m_nConfirmSpeed = '0';

public:
  void SendBeginReq();
  void ReceiveIdentification();
  void SendConfirmation();
  void ReceivePasswordReqiure();
  void SendPassword();
  void ReceivePasswordResult();
  void SendParamsAddresses();
  void ReceiveData(ETreadPhase tph);

  void SendData();

  void SendBreakBeforeDisconnection();

private:
  int Reader(unsigned char* pInBuff, int nSizeInBuff,
             unsigned char* pchEnd_1 = nullptr, // last
             unsigned char* pchEnd_2 = nullptr); // before last
  int Writer(unsigned char* pOutBuff, int nSizeOutBuff);
};
