#pragma once

#include "CommonViewForm.h"
#include "afxwin.h"

class CView_UnderConstruction: public CCommonViewForm
{
  DECLARE_DYNCREATE(CView_UnderConstruction)

public:
  CView_UnderConstruction();
  virtual ~CView_UnderConstruction();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_UNDER_CONSTRUCTION
  };

  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  void OnDrawBackground(CDC* pDC, CRect rect);

  CFont m_fBold;
  
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);
public:
  //virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = nullptr);
  CButton m_btnTest;
};
