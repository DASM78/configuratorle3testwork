#include "StdAfx.h"
#include "MainFrm.h"
#include "CommonViewDealer.h"
#include "Ai_File.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool DataStoreFinder(const unsigned int nInAddress, CString& sOutData)
{
  CMainFrame* pMf = (CMainFrame*)::AfxGetMainWnd();
  if (pMf)
  {
    dataTypesAndVarMeanings_t* pDb = pMf->GetDataStore()->GetReceivedDataStore();
    if (pDb)
    {
      auto iter = pDb->begin();
      for (size_t i = 0; i < pDb->size(); ++i, ++iter)
      {
        if (iter->first == nInAddress)
        {
          sOutData = iter->second;
          return true;
        }
      }
    }
  }
  return false;
}

LRESULT CMainFrame::OnClearReceivedDataStoreItem(WPARAM wParam, LPARAM lParam)
{
  const vector<teFrameTypes>* pClearedList = (vector<teFrameTypes>*)lParam;
  if (pClearedList && !pClearedList->empty())
  {
    for (size_t i = 0; i < pClearedList->size(); ++i)
    {
      teFrameTypes ft = pClearedList->at(i);
      unsigned int nAddress = 0;
      if (GetDataFrameAddress(ft, nAddress))
      {
        int nExtendedAddresses = GetExtendedAddresses(ft) + 1; // 1 - itself
        for (int e = 0; e < nExtendedAddresses; ++e)
          GetDataStore()->ClearReceivedDataStoreItem(nAddress + e);
      }
    }
  }

  bool bComeback = wParam ? true : false;
  CView* pActiveView = GetActiveView_();
  if (pActiveView)
  {
    CCommonViewDealer* pViewDealer = dynamic_cast<CCommonViewDealer*>(pActiveView);
    if (pViewDealer)
      pViewDealer->AfterClearReceivedData();
  }

  return TRUE;
}

void CMainFrame::OnUpdateFile(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CMainFrame::OnUpdateFileSave(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CMainFrame::OnSaveDocument()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(FALSE, _T("xml"), L"��-������������",
                  OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  CString sTitle = L"C��������� ���� �������� ����������";
  dlg.GetOFN().lpstrTitle = sTitle.GetBuffer(_MAX_PATH);
  CString sFile = L"��-������������. ��� ���������.xml";
  dlg.GetOFN().lpstrFile = sFile.GetBuffer(_MAX_PATH);

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    bool bResult = false;
    
    if (CAi_File::ExamineFile(sFileName)
        && CAi_File::IsFileExist(sFileName)
        && CAi_File::ExaminePermissions(sFileName, false))
    {
      CXmlAid_* pXml = GetAppEnviron()->GetXmlSett();
      pXml->SetUserFile(sFileName);
      if (pXml->CreateXmlFileFromTree_(CXmlAid_::xmlUserFile))
      {
        AfxMessageBox(L"���� � ����������� ������� ��������!\n\n" + sFileName, MB_ICONINFORMATION);
        bResult = true;
      }
    }
    
    if (!bResult)
    {
      AfxMessageBox(L"���� � ����������� �� ��������!\n\n" + sFileName, MB_ICONERROR);
      ASSERT(false);
    }
  }
}

void CMainFrame::OnOpenDocument()
{
  OpenXmlDoc();
}

LRESULT CMainFrame::OnOpenSpecXmlFile(WPARAM wParam, LPARAM lParam)
{
  return TRUE;
}

bool CMainFrame::OpenXmlDoc()
{
  // todo ��� ���� �������� ����� � ����������� ���� ��� ���� � ������ ���� ������� �� ���������

  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(TRUE, _T("xml"), nullptr, 
                  OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  dlg.GetOFN().lpstrTitle = L"�������� ���� ����� ��������";

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    bool bResult = false;

    if (CAi_File::IsFileExist(sFileName))
    {
      CXmlAid_* pXml = GetAppEnviron()->GetXmlSett();

      if (!pXml->CheckXmlFile(sFileName))
      {
        AfxMessageBox(L"����� � ����������� �������� �� ���������� ����������!\n\n" + sFileName, MB_ICONERROR);
        return false;
      }

      if (pXml->LoadTreeFromFile(sFileName))
      {
        OnReloadCurrentView(0, 0);
        pXml->CreateXmlFileFromTree_(CXmlAid_::xmlBackupFile);
        AfxMessageBox(L"���� � ����������� ������� ��������!", MB_ICONINFORMATION);
        bResult = true;
      }
    }

    if (!bResult)
    {
      AfxMessageBox(L"������ �������� ����� � �����������!\n\n" + sFileName, MB_ICONERROR);
      ASSERT(false);
      return false;
    }
  }
  
  return true;
}