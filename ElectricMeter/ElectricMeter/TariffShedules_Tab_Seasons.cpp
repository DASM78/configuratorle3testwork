#include "stdafx.h"
#include "Resource.h"
#include <algorithm>
#include "Ai_Str.h"
#include "Defines.h"
#include "View_TariffShedules.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Tab_Seasons
{
  #define TBL_SEASONS_MAX 12
  #define TBL_SEASONS_MIN 1
  #define TBL_SEASONS_DEFAULT 2

  #define TBL_DATA_AND_WEEK_DAYS 8 // ���� + 7 ���� ������

#define TBL_DEFUALT_DATE_TEXT L"01/01"
#define TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT L"1"

  struct COLUMNS
  {
    int nSeasonNumber = -1;
    int nBegin = -1;
    int nMonday = -1;
    int nTuesday = -1;
    int nWednesday = -1;
    int nThursday = -1;
    int nFriday = -1;
    int nSaturday = -1;
    int nSunday = -1;
  }
  g_columns;
  int g_nComnSignificantCell = -1;

  struct ROWS
  {
    int nCount_using = TBL_SEASONS_DEFAULT; // load/save via XML file
    int nCount_using_tmp = -1;
  }
  g_rows;

  struct CELL_PROPS
  {
    bool bShow = true; // �������������� ����
    CString sText;
    bool bEditable = false;
    bool bEnable = true;
    COLORREF colorBg = RGB(0, 0, 0);
    COLORREF colorText = RGB(0, 0, 0);
    CellTextStyle textStyle = ctsNone;
  };

  //                                                  i                                 j
  CELL_PROPS g_cellProps[TBL_DATA_AND_WEEK_DAYS][TBL_SEASONS_MAX] {};
  vector<CString> g_cellProps_tmp; // ��������� �������� �������� �� ��������

  CELL_PROPS* GetCellProp(int j /*row*/, int i /*cell*/)
  {
    ASSERT(j < TBL_SEASONS_MAX);
    ASSERT(i < TBL_DATA_AND_WEEK_DAYS);
    return &g_cellProps[i][j];
  }

  struct X_TAGS
  {
    CString sSeasonsNode = L"TariffShedules_Seasons";
    CParamsTreeNode* pSeasonsNode = nullptr;

    CString sTableNode = L"TableData";
    CString sTableAttr_usingRows = L"UsingRows";
    CParamsTreeNode* pTableNode = nullptr;
    CString sSeason = L"Season_";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

} // namespace ns_Tab_Seasons

using namespace ns_Tab_Seasons;

bool CView_TariffShedules::IsSheduleUsing_InSeasonsTbl(int nShedule)
{
  for (int j = 0; j < TBL_SEASONS_MAX; ++j)
    for (int i = 1; i < TBL_DATA_AND_WEEK_DAYS; ++i)
    {
      if (CAi_Str::ToInt(GetCellProp(j, i)->sText) == nShedule)
        return true;
    }
  return false;
}

void CView_TariffShedules::TakeValueFromXmlTree_Seasons()
{
  for (int j = 0; j < TBL_SEASONS_MAX; ++j)
    for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
      GetCellProp(j, i)->sText = (i == 0) ? TBL_DEFUALT_DATE_TEXT : TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT;

  CString sTag = g_xTags.sSeasonsNode;
  g_xTags.pSeasonsNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!g_xTags.pSeasonsNode)
  {
    g_xTags.pSeasonsNode = m_pTariffShedulesNode->AddNode(sTag);
    return; // ��������� �������� �� ���������
  }

  sTag = g_xTags.sTableNode;
  g_xTags.pTableNode = g_xTags.pSeasonsNode->FindFirstNode(sTag);
  if (!g_xTags.pTableNode)
  {
    g_xTags.pTableNode = g_xTags.pSeasonsNode;
    TakeValueFromXmlTree_Seasons_newXmlVer(); // �������� ��� ����� ������, �.�. g_xTags.sTableNode � ����� ������ ���� �� ������
    // old ver: g_xTags.pTableNode = g_xTags.pSeasonsNode->AddNode(sTag);
    return; // ��������� �������� �� ���������
  }

  // ������ ������ �� �������, ��������� ��� ����, � ������� ���������

  CString sTableAttr_usingRows = g_xTags.pTableNode->GetAttributeValue(g_xTags.sTableAttr_usingRows);
  g_rows.nCount_using = min(CAi_Str::ToInt(sTableAttr_usingRows), TBL_SEASONS_MAX); // ����� �� ������� �� ��������
  g_rows.nCount_using = max(g_rows.nCount_using, TBL_SEASONS_MIN); // ����� �� ������� �� �������

  for (int j = 0; j < g_rows.nCount_using; ++j)
    for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
      GetCellProp(j, i)->sText = (i == 0) ? TBL_DEFUALT_DATE_TEXT : TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT;

  // ������� �� ���� pTableNode - unit-� � �������

  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    sTag = g_xTags.sSeason + CAi_Str::ToString(j + 1);
    CParamsTreeNode* pTfShdSeasonsTblRowNode = g_xTags.pTableNode->FindFirstNode(sTag);
    if (pTfShdSeasonsTblRowNode) // ���� ����� ������ � ����������� �� ����������, �� ��������� ��������� �� ���������
    {
      CString sColumnSettings = pTfShdSeasonsTblRowNode->GetAttributeValue(g_xTags.sDataSet);
      ParseRowDataString_Seasons(sColumnSettings, j);
    }
  }
}

void CView_TariffShedules::TakeValueFromXmlTree_Seasons_newXmlVer()
{
  // ������� �� ���� pTableNode - unit-� � �������

  g_rows.nCount_using = TBL_SEASONS_DEFAULT;

  int j = 0;
  for (; j < TBL_SEASONS_MAX; ++j)
  {
    CString sTag;
    sTag.Format(L"%s%.2d", g_xTags.sSeason, j + 1);
    CParamsTreeNode* pTfShdSeasonsTblRowNode = g_xTags.pTableNode->FindFirstNode(sTag);
    if (!pTfShdSeasonsTblRowNode)
      break;

    CString sColumnSettings = pTfShdSeasonsTblRowNode->GetAttributeValue(g_xTags.sDataSet);
    ParseRowDataString_Seasons(sColumnSettings, j);
  }

  if (j == 0)
    j = TBL_SEASONS_DEFAULT;
  g_rows.nCount_using = max(j, TBL_SEASONS_MIN); // ����� �� ������� �� �������
}

void CView_TariffShedules::ParseRowDataString_Seasons(CString& sRowData, int j)
{
  vector <CString> cellSettings;
  if (CAi_Str::CutString(sRowData, L";", &cellSettings)
      && cellSettings.size() == TBL_DATA_AND_WEEK_DAYS)
  {
    cellSettings[0].Replace(L"-", L"/"); // date

    for (size_t i = 0; i < cellSettings.size(); ++i)
    {
      //����� ����� ��������:
      //- ��� � cellSettings[0] ���������� ����;
      //- � � cellSettings[> 0] ���������� ������.
      CString sText = cellSettings[i];
      if (i > 0) // ��� �������
        sText = sText.TrimLeft(L"0");
      GetCellProp(j, i)->sText = sText;
    }
  }
}

void CView_TariffShedules::PutValueToXmlTree_Seasons()
{
  CString sTag = g_xTags.sSeasonsNode;
  g_xTags.pSeasonsNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!g_xTags.pSeasonsNode)
    g_xTags.pSeasonsNode = m_pTariffShedulesNode->AddNode(sTag);

  //old ver: sTag = g_xTags.sTableNode;
  //old ver: g_xTags.pSeasonsNode->DeleteNodes(sTag);
  g_xTags.pSeasonsNode->DeleteAllNodes();
  //old ver: g_xTags.pTableNode = g_xTags.pSeasonsNode->AddNode(sTag);
  g_xTags.pTableNode = g_xTags.pSeasonsNode;

  //��������� ������� sTableAttr_usingRows, ��-����, ��������� (��������� ������������� ����������),
  //�.�. ��������� � �������� ���������� ����� �������:
  //- ������������ (�������) ������ - �� ���������� ����� g_xTags.sSeason (��. ����), ��� ����� ����������� ������������ �����;
  //������ ��������� ������� �������� ��� ����������� � ��������.
  
  //old ver: g_xTags.pTableNode->AddAttribute(g_xTags.sTableAttr_usingRows, CAi_Str::ToString(g_rows.nCount_using));

  // ���������� � ���� pTableNode - unit-�� � �������

  vector<CString> res;
  FormStringFrom_Seasons(res);

  for (int j = 0; j < (int)res.size(); ++j)
  {
    CString sTagSeasonN;
    sTagSeasonN .Format(L"%s%.2d", g_xTags.sSeason, j + 1);
    CParamsTreeNode* pNode_SeasonN = g_xTags.pTableNode->AddNode(sTagSeasonN);
    CString sValue = res[j];
    pNode_SeasonN->AddAttribute(g_xTags.sDataSet, sValue);
  }
}

CString CView_TariffShedules::GetXmlTag_Seasons()
{
  return g_xTags.sSeasonsNode;
}

void CView_TariffShedules::FormStringFrom_Seasons(vector<CString>& res)
{
  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    CString sValue;
    for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
    {
      if (i > 0)
        sValue += L";";
      CString sText = GetCellProp(j, i)->sText;
      if (i == 0)
        sText.Replace(L"/", L"-"); // date
      if (i > 0 && sText.GetLength() == 1)
        sText = L"0" + sText;
      sValue += sText;
    }
    res.push_back(sValue);
  }
}

void CView_TariffShedules::CreateSeasonsTab()
{
  m_plstSeasons->ShowHorizScroll(true);

  m_plstSeasonsEx = new �Ai_ListCtrlEx();
  m_plstSeasonsEx->SetList(m_plstSeasons, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    //LVS_EX_FULLROWSELECT |
    LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstSeasonsEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  m_plstSeasons->MyPreSubclassWindow();

  bool m_bGridLinesStyle = false;
  if (!m_bGridLinesStyle)
    m_plstSeasons->SetExtendedStyle(m_plstSeasons->GetExtendedStyle() & ~LVS_EX_GRIDLINES);
  m_plstSeasons->EnableInterlacedColorScheme(false);

  m_plstSeasons->m_bShowHeaderContextMenu = false;
  m_plstSeasons->m_bCanHeaderDrag = false;

  m_plstSeasonsEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  g_columns.nSeasonNumber = m_plstSeasonsEx->SetColumnEx(L"�����", LVCFMT_LEFT);
  m_plstSeasonsEx->SetColumnWidthStyle(g_columns.nSeasonNumber, CS_WIDTH_FIX, 100);
  g_columns.nBegin = m_plstSeasonsEx->SetColumnEx(L"������ (��/��)", LVCFMT_CENTER);
  m_plstSeasonsEx->SetColumnWidthStyle(g_columns.nBegin, CS_WIDTH_FIX, 100);
  int nColWidthForWeekDay = 80;
  map<int*, CString> weekDays;
  weekDays[&g_columns.nMonday] = L"�����������";
  weekDays[&g_columns.nTuesday] = L"�������";
  weekDays[&g_columns.nWednesday] = L"�����";
  weekDays[&g_columns.nThursday] = L"�������";
  weekDays[&g_columns.nFriday] = L"�������";
  weekDays[&g_columns.nSaturday] = L"�������";
  weekDays[&g_columns.nSunday] = L"�����������";
  for (auto i : weekDays)
  {
    *i.first = m_plstSeasonsEx->SetColumnEx(i.second, LVCFMT_CENTER);
    m_plstSeasonsEx->SetColumnWidthStyle(*i.first, CS_WIDTH_FIX, nColWidthForWeekDay);
  }

  m_plstSeasonsEx->MatchColumns();

  int nNodeIdx = m_plstSeasonsEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  g_nComnSignificantCell = g_columns.nSeasonNumber + 1;
  m_plstSeasonsEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, g_nComnSignificantCell);

  bool bLightRowColor = true;
  COLORREF colorLight = RGB(230, 240, 251);
  COLORREF colorDark = RGB(185, 205, 251);
  COLORREF color = colorLight;
  COLORREF colorDisableEditableText = RGB(160, 160, 160);
  COLORREF colorEditableText = RGB(60, 180, 60);

  m_pedtDate_InSeasonsTbl = new CDateTimeEdit_binding(nullptr);
  m_pedtDate_InSeasonsTbl->CreateEx(WS_EX_CLIENTEDGE, L"EDIT", nullptr, WS_CHILD | ES_AUTOHSCROLL | ES_CENTER, CRect(0, 0, 0, 0), m_plstSeasons, ID_EDT_DATE_IN_SEASONS_TBL_CTRL);
  m_pedtDate_InSeasonsTbl->SetFont(GetFont());
  m_pedtDate_InSeasonsTbl->SetMask(L"DD/MM");
  m_pedtDate_InSeasonsTbl->SetLinkingListCtrEx(m_plstSeasonsEx);

  m_pcbxShedules_InSeasonsTbl = new CComboBox_binding();
  m_pcbxShedules_InSeasonsTbl->Create_(m_plstSeasons, ID_CBX_SHEDULES_IN_SEASONS_TBL_CTRL, false_d(L"sort"), m_plstSeasonsEx);

  BIND_TO_CELL bindColumnList{};

  for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
  {
    int nCell = g_columns.nSeasonNumber + 1 + i;

    if (i == 0)
    {
      BIND_CTRL_TO_CELL bindCtrl_date{};
      bindCtrl_date.bDisableBind = false;
      bindCtrl_date.nCell = nCell;
      bindCtrl_date.strByDefault = TBL_DEFUALT_DATE_TEXT;
      bindCtrl_date.pEdit = m_pedtDate_InSeasonsTbl;
      bindColumnList.ctrlToCell.push_back(bindCtrl_date);
    }
    else
    {
      BIND_CTRL_TO_CELL bindCtrl_tariff{};
      bindCtrl_tariff.bDisableBind = false;
      bindCtrl_tariff.nCell = nCell;
      bindCtrl_tariff.strByDefault = TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT;
      bindCtrl_tariff.pCmbBox = m_pcbxShedules_InSeasonsTbl;
      bindColumnList.ctrlToCell.push_back(bindCtrl_tariff);
    }
  }

  vector<CString> textOfCells;
  textOfCells.push_back(L"����� ");
  for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
    textOfCells.push_back((i == 0) ? TBL_DEFUALT_DATE_TEXT : TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT);

  for (int j = 0; j < TBL_SEASONS_MAX; ++j)
  { // ����������� ����� ��� ������, �� �� default ��� ����� ��������� SetChild, � ���������, ��� ������� SetEmptyChild

    bool bStrongRow = true;
    textOfCells[0] = L"����� " + CAi_Str::ToString(j + 1);
    if (j >= g_rows.nCount_using)
    {
      bStrongRow = false;
      textOfCells[g_nComnSignificantCell] = L""; // � significant cell ������ ����� ��� SetEmptyChild
    }

    bLightRowColor = !bLightRowColor;
    color = bLightRowColor ? colorLight : colorDark;

    // ���������� ������. ����� ������������ ���������� ������� ����������� �� XML �����

    for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
    {
      CELL_PROPS* pCellProp = GetCellProp(j, i);
      pCellProp->colorBg = color;
      pCellProp->bEditable = true;
      pCellProp->colorText = colorEditableText;
      if (i == 0 && j == 0)
      {
        pCellProp->bEditable = false; // ���� � ������ ������ ������ ������ ������ 01/01 � �� �������������
        pCellProp->colorText = colorDisableEditableText;
      }
      pCellProp->textStyle = ctsBold;

      if (bStrongRow) // ������ ����� �����
      {
        CString sText = pCellProp->sText;
        if (i > 0)
          sText = L"������ " + sText;
        textOfCells[g_nComnSignificantCell + i] = sText;
      }
    }

    int nRow_inList = -1;
    if (bStrongRow) // ������ ����� �����
      nRow_inList = m_plstSeasonsEx->SetChild(nNodeIdx, &textOfCells, 0, &bindColumnList);
    else
      nRow_inList = m_plstSeasonsEx->SetEmptyChild(nNodeIdx, &textOfCells, 0, &bindColumnList);
  }

  UpdateView_InSeasonsTbl();
  m_plstSeasons->CtrlWasCreated();

#ifdef _DEBUG
  MeaningsWasChanged_InSeasonsTbl();
#endif
}

void CView_TariffShedules::OnDblClick_InSeasonsTable(int nRow_inView, int nCell)
{
  int i = nCell - 1 - g_columns.nSeasonNumber;
  int j = nRow_inView;
  if (GetCellProp(j, i)->bEditable)
  {
    if (nCell > g_columns.nBegin)
      m_pcbxShedules_InSeasonsTbl->FillIn(GetSheduleCount_From24HoursTbl());
    m_plstSeasonsEx->OnDblClick(nRow_inView, nCell);
  }
}

void CView_TariffShedules::CheckSettBtnEnable_ForSeasonsTbl()
{
  if (g_rows.nCount_using >= TBL_SEASONS_MIN
      && g_rows.nCount_using < TBL_SEASONS_MAX)
      m_btnAddRow.EnableWindow(TRUE);
  else
    m_btnAddRow.EnableWindow(FALSE);

  if (g_rows.nCount_using > TBL_SEASONS_MIN
      && g_rows.nCount_using <= TBL_SEASONS_MAX)
      m_btnDelRow.EnableWindow(TRUE);
  else
    m_btnDelRow.EnableWindow(FALSE);
}

void CView_TariffShedules::OnBnClickedBtnAddRow_InSeasonsTbl()
{
  if (g_rows.nCount_using < TBL_SEASONS_MAX)
  {
    ++g_rows.nCount_using;

    int nResult = -1;
    int nNodeRow_inView = 0;
    CString sCellText = TBL_DEFUALT_DATE_TEXT;

    nResult = m_plstSeasonsEx->InsertTextToChild(nNodeRow_inView, g_nComnSignificantCell, sCellText);
    if (nResult != RCHI_SUCCESS)
    {
      ASSERT(false);
      g_rows.nCount_using--;
    }
    else
    {
      int nRow_inView = m_plstSeasonsEx->GetLastInsertedChildIdx_inList() - 1;
      for (int i = 1; i < TBL_DATA_AND_WEEK_DAYS; ++i) // �������� ���������, �� g_columns.nSeasonNumber + 1, �����
      {
        int j = nRow_inView;
        CELL_PROPS* pCellProp = GetCellProp(j, i);
        pCellProp->sText = TBL_DEFUALT_WEEK_DAY_SHEDULE_TEXT;
        int nCell = g_columns.nSeasonNumber + 1 + i;
        m_plstSeasonsEx->SetCellText(nRow_inView + 1, nCell, CString(L"������ ") + pCellProp->sText);
      }
    }

    if (nResult == RCHI_SUCCESS)
    {
      UpdateView_InSeasonsTbl();
      m_plstSeasonsEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, g_rows.nCount_using - 1, false_d(L"mouse move"));

      MeaningsWasChanged_InSeasonsTbl();
    }
    CheckSettBtnEnable_ForSeasonsTbl();
  }
}

void CView_TariffShedules::OnBnClickedBtnDelRow_InSeasonsTbl()
{
  if (g_rows.nCount_using > TBL_SEASONS_MIN)
  {
    int nRow_inView = g_rows.nCount_using - 1;
    m_plstSeasonsEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, g_nComnSignificantCell, -1);
    for (int cell = 0; cell < TBL_DATA_AND_WEEK_DAYS; ++cell) // �������� ���������, �� g_columns.nSeasonsNumber + 1, ����� 
    {
      int i = cell;
      int j = nRow_inView;
      CELL_PROPS* pCellProp = GetCellProp(j, i);
      pCellProp->sText = L"";
      int nCell = cell + g_columns.nSeasonNumber + 1;
      m_plstSeasonsEx->SetCellText(nRow_inView + 1, nCell, L"");
    }
    g_rows.nCount_using--;
    UpdateView_InSeasonsTbl(); // ����� ������� ���� ��� ������������� bShow
    m_plstSeasonsEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, g_rows.nCount_using - 1, false_d(L"mouse move"));

    MeaningsWasChanged_InSeasonsTbl();
    CheckSettBtnEnable_ForSeasonsTbl();
  }
}

void CView_TariffShedules::Check_InSeasonsTbl(CString& sErrorMsg)
{ // �� ��������: ��. �������� �������� � ����� TariffShedules.txt

  CString sErrorMsgTmp = L"� ���������� ������� \"������\" ���������� ������������ ������.\n\n";
  CDateTimeEdit tmeUtil;
  tmeUtil.SetMask(L"DD/MM", false_d("��������� ctrl"));
  COleDateTime tmeUnitPrev;

  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    CELL_PROPS* pUnit = GetCellProp(j, 0);
    COleDateTime tmeUnitCurr = tmeUtil.GetDateTime(pUnit->sText);
    if (j > 0 && tmeUnitPrev >= tmeUnitCurr) // wrong
    {
      CString sMsg;
      sMsg.Format(L"����� - %d.\n������� - ���� ������ ���� �� �� �����������.", j + 1);
      sErrorMsg = sErrorMsgTmp + sMsg;
      return;
    }
    tmeUnitPrev = tmeUnitCurr;
  }
}

void CView_TariffShedules::UpdateView_InSeasonsTbl(bool bUpdateCellText)
{
  m_plstSeasonsEx->UpdateView();

  for (int row = 0; row < TBL_SEASONS_MAX; ++row)
  {
    int nRow_inList = row + 1;
    int j = row;

    m_plstSeasonsEx->SetRowBgColor(nRow_inList, GetCellProp(j, 0)->colorBg); // ������������ - ����� ��������� �����

    for (int cell = 0; cell < TBL_DATA_AND_WEEK_DAYS; ++cell) // �������� ���������, �� g_columns.nSeasonNumber, ����� 
    {
      int i = cell;
      int nCell = i + g_columns.nSeasonNumber + 1;
      CELL_PROPS* pCellProp = GetCellProp(j, i);

      if (!m_plstSeasonsEx->IsRowVisible(nRow_inList))
        pCellProp->bShow = false;
      else
      {
        pCellProp->bShow = true;
        if (pCellProp->bEditable)
        {
          m_plstSeasonsEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          m_plstSeasonsEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
          if (!pCellProp->bEnable)
            m_plstSeasonsEx->SetCellDisable(nRow_inList, nCell);
        }
        else
        {
          if (pCellProp->textStyle != ctsNone)
            m_plstSeasonsEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          if (pCellProp->colorText > 0)
            m_plstSeasonsEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
        }

        if (bUpdateCellText)
        {
          CString sCellText = m_plstSeasonsEx->GetCellText(nRow_inList, nCell);
          if (nCell == g_columns.nBegin
              && sCellText != pCellProp->sText)
              m_plstSeasonsEx->SetCellText(nRow_inList, nCell, pCellProp->sText);
          if (nCell > g_columns.nBegin
              && sCellText != L"������ " + pCellProp->sText)
            m_plstSeasonsEx->SetCellText(nRow_inList, nCell, L"������ " + pCellProp->sText);
        }
      }
    }
  }

  m_plstSeasonsEx->UpdateView();
}

void CView_TariffShedules::MeanWasSeted_InSeasonsTbl()
{
  MeaningsWasChanged_InSeasonsTbl();
}

void CView_TariffShedules::MeaningsWasChanged_InSeasonsTbl()
{
  GetCutOfMeanings_InSeasonsTbl();
  PutValueToXmlTree_Seasons();
  UpdateXmlBackupFile();
}

void CView_TariffShedules::GetCutOfMeanings_InSeasonsTbl()
{
  for (int j = 0; j < TBL_SEASONS_MAX; ++j)
  {
    int nRow_inList = j + 1;
    if (!m_plstSeasonsEx->IsRowVisible(nRow_inList))
      break;

    for (int i = 0; i < TBL_DATA_AND_WEEK_DAYS; ++i)
    {
      int nCell = g_columns.nSeasonNumber + 1 + i;
      CString sText = m_plstSeasonsEx->GetCellText(nRow_inList, nCell);
      sText.TrimLeft(L"������ ");
      GetCellProp(j, i)->sText = sText;
    }
  }
}

void CView_TariffShedules::ClearAllReqDataInDB_ForSeasonsTbl()
{
  FormListForClearReceivedData(FRAME_TYPE_SEAS_CNT);
  for (int i = (int)FRAME_TYPE_SEAS_01; i < (int)FRAME_TYPE_SEAS_12; ++i)
    FormListForClearReceivedData((teFrameTypes)i);
}

void CView_TariffShedules::ReceiveData_ForSeasonsTbl()
{
  g_cellProps_tmp.clear();
  g_rows.nCount_using_tmp = -1;

  SetParamTypeForRead(FRAME_TYPE_SEAS_CNT, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
}

void CView_TariffShedules::UpdateParamMeaningsByReceivedDeviceData_Seasons(teFrameTypes ft)
{
  if (ft == FRAME_TYPE_SEAS_CNT)
  {
    varMeaning_t m = FindReceivedData(FRAME_TYPE_SEAS_CNT);
    if (m != L"?")
    {
      int nNewRowCount = CAi_Str::ToInt(m);
      g_rows.nCount_using_tmp = min(nNewRowCount, TBL_SEASONS_MAX); // ����� �� ������� �� ��������
      g_rows.nCount_using_tmp = max(g_rows.nCount_using_tmp, TBL_SEASONS_MIN); // ����� �� ������� �� �������
    }
    return;
  }

  if (ft >= FRAME_TYPE_SEAS_01
      && ft <= FRAME_TYPE_SEAS_12)
  {
    varMeaning_t m = FindReceivedData(ft);
    g_cellProps_tmp.push_back(m);
  }

  if (g_rows.nCount_using_tmp == g_cellProps_tmp.size()) // ��� ��������, �������� �������
  {
    if (g_rows.nCount_using_tmp > g_rows.nCount_using)
    {
      int nCount = g_rows.nCount_using_tmp - g_rows.nCount_using;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnAddRow_InSeasonsTbl();
    }
    if (g_rows.nCount_using_tmp < g_rows.nCount_using)
    {
      int nCount = g_rows.nCount_using - g_rows.nCount_using_tmp;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnDelRow_InSeasonsTbl();
    }

    vector <CString> cellSettings;
    if (CAi_Str::CutString(g_cellProps_tmp[0], L";", &cellSettings)
        && cellSettings.size() <= 1)
    {
      ASSERT(false);
      AfxMessageBox(L"������ � ���������� ������!", MB_ICONERROR);
      return;
    }

    for (size_t j = 0; j < g_cellProps_tmp.size(); ++j)
      ParseRowDataString_Seasons(g_cellProps_tmp[j], j);

    UpdateView_InSeasonsTbl(true_d("�������� ����� � ������"));
    MeaningsWasChanged_InSeasonsTbl();

    if (!IsReqItems_SpecDays())
      m_bShowLoadingSuccessResultMsg = true;
  }
}

bool CView_TariffShedules::IsReqItems_Seasons()
{
  return (g_rows.nCount_using_tmp > 0);
}

void CView_TariffShedules::FormAllReadRowReq_Seasons()
{
  teFrameTypes reqFirstItem = FRAME_TYPE_SEAS_01;
  int nNext = static_cast<int>(reqFirstItem);
  for (int i = 0; i < g_rows.nCount_using_tmp; ++i)
  {
    reqFirstItem = static_cast<teFrameTypes>(nNext);
    SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    nNext += 1;
  }
}