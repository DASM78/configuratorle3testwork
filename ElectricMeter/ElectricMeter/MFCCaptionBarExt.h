#pragma once

#include "afxcaptionbar.h"

enum ECaptionButton
{
  cbNone,
  cbConnect,
  cbConnectClick,
  cbBreakConnection,
  cbConnection,
  cbConnectionClick,
};

class CMFCCaptionBarExt : public CMFCCaptionBar
{
public:
  CMFCCaptionBarExt();
  ~CMFCCaptionBarExt();

  DECLARE_MESSAGE_MAP()
  
public:
  void SetBtnType(ECaptionButton cb) { m_btnType = cb; }

  // L - Light
  enum EBgColor
  {
    clrGray_LGray, // default
    clrFree_Free, // default2
    clrWhite_LGray,
    clrYelow_LYellow,
    clrBlue_LBlue,
    clrLGreen_Green // indicator mode
  };

  void SetBgColor(EBgColor bg);

  void EnableIndicatorMode(bool bEnable);
  void SetIndicatorPos(BYTE nPos);

  void CheckButtonView(POINT* pPt);

private:
  EBgColor m_bgColor = clrFree_Free; // clrGray_LGray;
  EBgColor m_bgPreviousColor = clrFree_Free; // clrGray_LGray;
  
  CBitmap m_bmpConnect;
  CBitmap m_bmpConnectMouseOver;
  CBitmap m_bmpConnectClick;
  CBitmap m_bmpConnection; // on-line
  CBitmap m_bmpConnectionMouseOver; // on-line
  CBitmap m_bmpConnectionClick; // on-line
  CRect m_rectConnect;
  bool m_bMouseOver = false;
  int m_nNeedRedrawButton = 0;
  int m_nRedrawButton = 0;
  
  bool m_bIndicatorMode = false;
  BYTE m_nIndicatorPos = 0;

  virtual void OnDrawBackground(CDC* pDC, CRect rect);
  virtual void OnDrawText(CDC* pDC, CRect rect, const CString& strText);
  HFONT CreateBoldWindowFont(HWND window);

  ECaptionButton m_btnType = cbNone;
  ECaptionButton m_btnTypePrev = cbNone;
  virtual void OnDrawButton(CDC* pDC, CRect rect, const CString& strButton, BOOL bEnabled);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnMouseLeave();
};

