#pragma once

#include <vector>
using namespace std;

struct APP_ERROR
{
  APP_ERROR() { }
  APP_ERROR(int id, CString descr, CString descr2 = L"")
    : nID(id)
    , sDescription(descr)
    , sDescription2(descr2)
  {
  }

  int nID = -1;
  CString sDescription;
  CString sDescription2;
};

class CErrorStore
{
public:
  CErrorStore();
  ~CErrorStore();

  virtual void FormErrorList() { }

  void SetLastError(int nID, CString str = L"");
  APP_ERROR* GetLastError();
  void ShowLastError();
  void Throw();

private:
  vector<APP_ERROR> m_errors;
  APP_ERROR* m_pLastError = NULL;
  APP_ERROR m_unknown;
  APP_ERROR m_prevLastError;
};
