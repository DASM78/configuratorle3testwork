#pragma once

#include "afxwin.h"
#include "Ai_TabCtrl.h"
#include "Ai_ColorStatic.h" 
#include "CommonViewForm.h"
#include "CommonViewDealer.h"
#include "Settings_Task_InterfaceChoice_RS.h"
#include "Ai_EditCtrlCharFilter.h"
#include "XmlAid.h"
#include "Model_.h"

class CAi_VertTabCtrl_My2 : public CAi_VertTabCtrl
{
public:
	CAi_VertTabCtrl_My2 ()
	{
	}
	~CAi_VertTabCtrl_My2 ()
	{
	}

private:
	virtual void OnSelchanged (int nSelectedItem, CString sPageName)
	{
	}
};

class CMFCButton_ : public CMFCButton
{
public:
	void SetDelayFullTextTooltipSet (bool DelayFullTextTooltipSet)
	{
		m_bDelayFullTextTooltipSet = DelayFullTextTooltipSet;
	}
};

class CView_Interfaces : public CCommonViewForm, public CCommonViewDealer
{
	DECLARE_DYNCREATE (CView_Interfaces)

public:
	CView_Interfaces ();
	virtual ~CView_Interfaces ();

	DECLARE_MESSAGE_MAP ()

	CDocument* GetDocument ();

private:
	enum
	{
		IDD = IDD_SETTINGS_INTERFACES
	};

	CParamsTreeNode* m_pInterfacesNode = nullptr;

	virtual void DoDataExchange (CDataExchange* pDX);    // DDX/DDV support

	virtual void OnInitialUpdate ();
	virtual void BeforeDestroyClass () override final;
	virtual void OnSize_ ();

	virtual void OnDraw (CDC* pDC);  // overridden to draw this view

	bool m_bCanSave = false;

	// Interface settings

	CAi_ColorStatic m_stcInterfacesCap;

	void FormTask_InterfaceSets ();
	CAi_VertTabCtrl_My2 m_tabInterfaces;
	CSettings_Task_InterfaceChoice_RS m_SettTask_Prot_RS;

	// Passwords

	CAi_ColorStatic m_stcPasswordCap;

	CButton m_chkShowPassword;
	CAi_EditCtrlCharFilter m_edtOldPasswordUser;
	CAi_EditCtrlCharFilter m_edtNewPasswordUser;
	CAi_EditCtrlCharFilter m_edtOldPasswordAdmin;
	CAi_EditCtrlCharFilter m_edtNewPasswordAdmin;
	CAi_EditCtrlCharFilter m_edtNewPasswordUser2;

	afx_msg void OnBnClickedChkShowPassword ();
	void CheckShowPasswordState ();
	void CheckUserEnable (bool bEnable);
	void CheckAdminEnable (bool bEnable);

public:
	void OldUserPasswordWasChanged (CString sText);
	void OldAdminPasswordWasChanged (CString sText);

private:
	sCfg m_cfg;	
	CMFCButton_ m_btnUserPasswordError;
	CMFCButton_ m_btnAdminPasswordError;

	CButton m_rdoNewAdminPassword;
	CButton m_rdoNewUserPassword;
	afx_msg void OnBnClickedRdoNewAdminPassword ();
	afx_msg void OnBnClickedRdoNewUserPassword ();

	// Exchange

	CMFCButton m_btnRecordInterfacesToDevice;
	CMFCButton m_btnRecordPasswordsToDevice;

	virtual void OnConnectionStateWasChanged () override final;
	void CheckMeaningsViewByConnectionStatus ();

	afx_msg void OnBnClickedBtnRecordToDevice ();
	afx_msg void OnBnClickedBtnRecordPasswToDevice ();
	virtual void UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult) override final;
	virtual void UpdateParamMeaningsByReceivedDeviceData (mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
public:
	afx_msg void OnBnClickedBtnRead ();
};
