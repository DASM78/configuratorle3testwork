#include "StdAfx.h"
#include "Resource.h"
#include "AboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAboutDlg::CAboutDlg()
  : CDialog(CAboutDlg::IDD)
{
  //{{AFX_DATA_INIT(CAboutDlg)
  //}}AFX_DATA_INIT
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
  //{{AFX_MSG_MAP(CAboutDlg)
  // No message handlers
  //}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_BTN_LOGO, &CAboutDlg::OnBnClickedBtnLogo)
END_MESSAGE_MAP()

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  //}}AFX_DATA_MAP
  DDX_Control(pDX, IDC_STATIC_VER, m_stcVersion);
}

BOOL CAboutDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  CString sVer = L"������ " + m_sProductVersion;
  m_stcVersion.SetWindowText(sVer);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CAboutDlg::SetProductVersion(CString sVer)
{
  m_sProductVersion = sVer;
}


void CAboutDlg::OnBnClickedBtnLogo()
{
  OnCancel();
}
