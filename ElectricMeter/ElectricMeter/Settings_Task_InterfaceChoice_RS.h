#pragma once

#include "afxwin.h"
#include "Resource.h"
#include <vector>
#include "XmlAid.h"
#include "Model_.h"
#include "Ai_EditCtrlCharFilter.h"
#include "CounterInfo.h"

class CSettings_Task_InterfaceChoice_RS : public CDialogEx
{
	DECLARE_DYNAMIC (CSettings_Task_InterfaceChoice_RS)

public:
	CSettings_Task_InterfaceChoice_RS (CWnd* pParent = NULL);   // standard constructor
	virtual ~CSettings_Task_InterfaceChoice_RS ();

	// Dialog Data
	enum { IDD = IDD_SETTINGS_TASK_INTERFACE_CHOICE_RS };

protected:
	virtual void DoDataExchange (CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP ()

public:
	void SetGlobalParent (HWND m_hWnd) { m_hWndGlobParent = m_hWnd; }
private:
	HWND m_hWndGlobParent = nullptr;

protected:

	//{{AFX_MSG(CMyDialog)
	virtual BOOL OnInitDialog ();
	virtual void OnOK () { }
	virtual void OnCancel () { }
	afx_msg void OnClose () { }
	//}}AFX_MSG

	CWnd* m_pParent = nullptr;

public:
	void CSettings_Task_InterfaceChoice_RS::SetComData(sCfg m_cfg);
	
	void GetComData(bool& is1ComEn, bool& is2ComEn, bool& isCom3En, bool& isCom4En, sCommInfo* info, sgsmSettings *gsmSett);
	void OnPostInit ();

	CString FormData (mdl::teFrameTypes n);

private:
	sCommInfo m_info[4];
	

	bool m_bCanSave = false;
	

	
	void SetEnableCtrls (std::vector<CWnd*>& rs, BOOL bEnable);
	bool m_bCtrlsDataIsFilling = false;
	void CtrlsDataWasChanged ();
	
	std::vector<CWnd*> m_rs1;
	std::vector<CWnd*> m_rs2;
	std::vector<CWnd*> m_rs3;
	std::vector<CWnd*> m_rs4;
	
	CButton m_chkUseCOM1;
	CComboBox m_cbxProtocol;
	CAi_EditCtrlCharFilter m_edtDeviceAddress;
public:
	void DeviceAddressWasChanged ();
private:
	CComboBox m_cbxRS_Speed;
	CComboBox m_cbxRS_Parity;
	CComboBox m_cbxRS_BitCount;
	CComboBox m_cbxRS_BytesInterval;

	CButton m_chkUseCOM2;
	CComboBox m_cbxProtocol2;
	CAi_EditCtrlCharFilter m_edtDeviceAddress2;
public:
	void DeviceAddress2WasChanged ();
private:
	CComboBox m_cbxRS_Speed2;
	CComboBox m_cbxRS_Parity2;
	CComboBox m_cbxRS_BitCount2;
	CComboBox m_cbxRS_IntervalTimeout2;

	CButton m_chkUseCOM3;
	CComboBox m_cbxProtocol3;
	CAi_EditCtrlCharFilter m_edtDeviceAddress3;

	CButton m_chkUseGsm;
	CComboBox m_cbxProtocol4;
	CAi_EditCtrlCharFilter m_edtDeviceAddress4;

public:
	void DeviceAddress3WasChanged ();
	void DeviceAddress4WasChanged ();
private:
	CComboBox m_cbxRS_Speed3;
	CComboBox m_cbxRS_Parity3;
	CComboBox m_cbxRS_BitCount3;
	CComboBox m_cbxRS_IntervalTimeout3;


	CComboBox m_cbxRS_Speed4;
	CComboBox m_cbxRS_Parity4;
	CComboBox m_cbxRS_BitCount4;
	CComboBox m_cbxRS_IntervalTimeout4;

	afx_msg void OnBnClickedChkCom1 ();
	afx_msg void OnBnClickedChkCom2 ();
	afx_msg void OnBnClickedChkCom3 ();
	afx_msg void OnBnClickedChkCom4 ();

	afx_msg void OnCbnSelchangeCbxRsProtocol1 ();
	afx_msg void OnCbnSelchangeCbxRsProtocol2 ();
	afx_msg void OnCbnSelchangeCbxRsProtocol3 ();
	afx_msg void OnCbnSelchangeCbxRsProtocol4 ();
	afx_msg void OnEnChangeEdtRsDeviceAddress1 ();
	afx_msg void OnEnChangeEdtRsDeviceAddress2 ();
	afx_msg void OnEnChangeEdtRsDeviceAddress3 ();
	afx_msg void OnEnChangeEdtRsDeviceAddress4 ();
	afx_msg void OnCbnSelchangeCbxRsSpeed1 ();
	afx_msg void OnCbnSelchangeCbxRsSpeed2 ();
	afx_msg void OnCbnSelchangeCbxRsSpeed3 ();
	afx_msg void OnCbnSelchangeCbxRsSpeed4 ();
	afx_msg void OnCbnSelchangeCbxRsParity1 ();
	afx_msg void OnCbnSelchangeCbxRsParity2 ();
	afx_msg void OnCbnSelchangeCbxRsParity3 ();
	afx_msg void OnCbnSelchangeCbxRsParity4 ();
	afx_msg void OnCbnSelchangeCbxRsBitCount1 ();
	afx_msg void OnCbnSelchangeCbxRsBitCount2 ();
	afx_msg void OnCbnSelchangeCbxRsBitCount3 ();
	afx_msg void OnCbnSelchangeCbxRsBitCount4 ();
	afx_msg void OnCbnSelchangeCbxRsIntervalTimeout1 ();
	afx_msg void OnCbnSelchangeCbxRsIntervalTimeout2 ();
	afx_msg void OnCbnSelchangeCbxRsIntervalTimeout3 ();
	afx_msg void OnCbnSelchangeCbxRsIntervalTimeout4 ();
public:

	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnChangeEditDns();
};
