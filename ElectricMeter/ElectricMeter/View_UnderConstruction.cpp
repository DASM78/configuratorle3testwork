#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "View_UnderConstruction.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const CString strInfo = _T("               � ����������...             ");

IMPLEMENT_DYNCREATE(CView_UnderConstruction, CCommonViewForm)

CView_UnderConstruction::CView_UnderConstruction()
  : CCommonViewForm(CView_UnderConstruction::IDD, L"CView_UnderConstruction")
{
}

CView_UnderConstruction::~CView_UnderConstruction()
{
}

BEGIN_MESSAGE_MAP(CView_UnderConstruction, CCommonViewForm)
END_MESSAGE_MAP()

void CView_UnderConstruction::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_BTN_TEST, m_btnTest);
}

void CView_UnderConstruction::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CFont* f = GetFont();
  LOGFONT lf;
  f->GetLogFont(&lf);
  //lf.lfWeight = FW_BOLD;
  lf.lfWeight = FW_HEAVY;
  lf.lfQuality = PROOF_QUALITY;
  lf.lfHeight = -20;
  m_fBold.CreateFontIndirect(&lf);

  // Test Ctrl's size and position

  if (m_btnTest.IsWindowVisible())
  {
    static int nTest = 0;

    switch (nTest)
    {
      default: nTest = 0; AfxMessageBox(L"����!");
      case 0: AddCtrlForPositioning(&m_btnTest, movbehConstTopMarginAndVertSizes, movbehConstLeftMarginAndHorizSizes);
        TRACE(L"const all\n");
        break;
      case 1: AddCtrlForPositioning(&m_btnTest, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);
        TRACE(L"������������ �� ������\n");
        break;
      case 2: AddCtrlForPositioning(&m_btnTest, movbehConstBottomMarginAndVertSizes, movbehConstLeftMarginAndHorizSizes);
        TRACE(L"���������� � ����\n");
        break;
      case 3: AddCtrlForPositioning(&m_btnTest, movbehConstTopMarginAndVertSizes, movbehConstLeftAndRigthMargins);
        TRACE(L"������������ �� ������\n");
        break;
      case 4: AddCtrlForPositioning(&m_btnTest, movbehConstTopMarginAndVertSizes, movbehConstRightMarginAndHorizSizes);
        TRACE(L"���������� � ������ �������\n");
        break;
      case 5: AddCtrlForPositioning(&m_btnTest, movbehConstTopAndBottomMargins, movbehConstLeftAndRigthMargins);
        TRACE(L"������������ �� ������ � ������\n");
        break;
      case 6: AddCtrlForPositioning(&m_btnTest, movbehConstTopAndBottomMargins, movbehConstRightMarginAndHorizSizes);
        TRACE(L"������������ �� ������ � ���������� � ������ �������\n");
        break;
      case 7: AddCtrlForPositioning(&m_btnTest, movbehConstBottomMarginAndVertSizes, movbehConstLeftAndRigthMargins);
        TRACE(L"���������� � ���� � ����������� �� ������\n");
        break;
      case 8: AddCtrlForPositioning(&m_btnTest, movbehConstBottomMarginAndVertSizes, movbehConstRightMarginAndHorizSizes);
        TRACE(L"���������� � ���� � ���������� � ������ �������\n");
        break;
      case 9: AddCtrlForPositioning_Stretchable(&m_btnTest);
        TRACE(L"������������ �� ������ � ������\n");
        break;
      case 10: AddCtrlForPositioning_FollowUpBottom(&m_btnTest);
        TRACE(L"���������� � ����\n");
        break;
      case 11: AddCtrlForPositioning_FollowUpRight(&m_btnTest);
        TRACE(L"���������� � ������ �������\n");
        break;
      case 12: AddCtrlForPositioning_FollowUpRightAndBottom(&m_btnTest);
        TRACE(L"���������� � ���� � ���������� � ������ �������\n");
        break;
    }
    ++nTest;
  }
  //

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_UnderConstruction::OnSize_()
{
}

void CView_UnderConstruction::OnDraw(CDC* pDC)
{
  //TRACE(L"CView_UnderConstruction::OnDraw begin\n");

  const int iOffset = 20;

  //CFont* pFontOld = (CFont*)pDC->SelectStockObject(DEFAULT_GUI_FONT);
  //ASSERT(pFontOld != nullptr);

  /*const HFONT font = (HFONT)SendMessage(WM_GETFONT, 0, 0);
  LOGFONT fontAttributes = {0};
  ::GetObject(font, sizeof(fontAttributes), &fontAttributes);
  wcscpy_s(fontAttributes.lfFaceName, 32, L"Calibri");
  fontAttributes.lfWeight = FW_SEMIBOLD;
  fontAttributes.lfQuality = PROOF_QUALITY;
  fontAttributes.lfHeight = -13;*/

  CFont* pFontOld = (CFont*)pDC->SelectObject(&m_fBold);

  CRect rctFactClient{};
  CRect rctFactWindow{};
  int nHideOverLeft = 0;
  int nHideOverRight = 0;
  int nHideOverTop = 0;
  int nHideOverBottom = 0;
  GetFactViewSizes(rctFactClient, rctFactWindow, nHideOverLeft, nHideOverRight, nHideOverTop, nHideOverBottom);
  CRect rectClient = rctFactWindow;
  ScreenToClient(&rectClient);
  //GetClientRect(&rectClient);

  CRect rectText = rectClient;
  rectText.DeflateRect(iOffset, iOffset);
  pDC->DrawText(strInfo, rectText, DT_CALCRECT | DT_WORDBREAK);

  rectText.OffsetRect((rectClient.Width() - rectText.Width() - 2 * iOffset) / 2,
                      (rectClient.Height() - rectText.Height() - 2 * iOffset) / 2);

  CRect rectFrame = rectText;
  rectFrame.InflateRect(iOffset, iOffset);

  CDrawingManager dm(*pDC);

  OnDrawBackground(pDC, rectClient);
  //dm.FillGradient2(rectFrame, RGB(200, 200, 200), RGB(255, 255, 255), 0);

  //pDC->Draw3dRect(rectFrame, RGB(5, 5, 5), RGB(250, 250, 250));

  pDC->SetBkMode(TRANSPARENT);
  pDC->SetTextColor(RGB(220, 220, 1220));

  pDC->DrawText(strInfo, rectText, DT_WORDBREAK);
  pDC->SelectObject(pFontOld);

  //fBold.DeleteObject();
  
  //TRACE(L"CView_UnderConstruction::OnDraw end\n");
}

void CView_UnderConstruction::OnDrawBackground(CDC* pDC, CRect rect)
{
  COLOR16 nR_vrtx1 = 0xFF00;
  COLOR16 nG_vrtx1 = 0xFF00;
  COLOR16 nB_vrtx1 = 0xFF00;

  COLOR16 nR_vrtx2 = 0xB000; // 239
  COLOR16 nG_vrtx2 = 0xB000;
  COLOR16 nB_vrtx2 = 0xB000;

  /*COLOR16 nR_vrtxIndic1 = 0xFF00;
  COLOR16 nG_vrtxIndic1 = 0xFF00;
  COLOR16 nB_vrtxIndic1 = 0xFF00;

  COLOR16 nR_vrtxIndic2 = 0xEF00; // 239
  COLOR16 nG_vrtxIndic2 = 0xEF00;
  COLOR16 nB_vrtxIndic2 = 0xEF00;*/

  /*switch (m_bgColor)
  {
    case clrGray_LGray:*/
      nR_vrtx1 = 0xA800;
      nG_vrtx1 = 0xA800;
      nB_vrtx1 = 0xA800;
      /*break;

    case clrWhite_LGray:
      nR_vrtx1 = 0xFF00; // 255
      nG_vrtx1 = 0xFF00;
      nB_vrtx1 = 0xFF00;
      break;

    case clrYelow_LYellow:
      nR_vrtx1 = 0xEC00; // 236
      nG_vrtx1 = 0xE200; // 226
      nB_vrtx1 = 0x5900; // 89

      nR_vrtx2 = 0xFF00; // 255
      nG_vrtx2 = 0xFF00;
      nB_vrtx2 = 0xDC00; // 220
      break;

    case clrBlue_LBlue:
      nR_vrtx1 = 0xA000; // 160
      nG_vrtx1 = 0xC000; // 192
      nB_vrtx1 = 0xEC00; // 236

      nR_vrtx2 = 0xA600; // 6
      nG_vrtx2 = 0xFF00; // 174
      nB_vrtx2 = 0xFF00;
      break;

    case clrLGreen_Green:
      nR_vrtx1 = 0xC800;
      nG_vrtx1 = 0xC800;
      nB_vrtx1 = 0xC800;

      nR_vrtx2 = 0xC800; // 239
      nG_vrtx2 = 0xC800;
      nB_vrtx2 = 0xC800;

      nR_vrtxIndic1 = 0xDA00; // 218
      nG_vrtxIndic1 = 0xFD00; // 253
      nB_vrtxIndic1 = 0xE200; // 226

      nR_vrtxIndic2 = 0xB100; // 177
      nG_vrtxIndic2 = 0xC100; // 193
      nB_vrtxIndic2 = 0x0300; // 3
      break;
  }*/

  CRect rectIndic = rect;
  /*if (m_bIndicatorMode)
  {
    if (m_nIndicatorPos < 100)
    {
      float nWidth = (float)rectIndic.Width(); // 100%
      float _1Prc = (nWidth / 100);
      rectIndic.right = rectIndic.left + (int)(_1Prc * m_nIndicatorPos);
    }
  }*/

  TRIVERTEX vertex[2] = {
    { rect.left, rect.top, nR_vrtx1, nG_vrtx1, nB_vrtx1, 0x00AA },
    { rect.right, rect.bottom, nR_vrtx2, nG_vrtx2, nB_vrtx2, 0x00AA }
  };
  /*TRIVERTEX vertexIndic[2] = {
    { rectIndic.left, rectIndic.top, nR_vrtxIndic1, nG_vrtxIndic1, nB_vrtxIndic1, 0x0000 },
    { rectIndic.right, rectIndic.bottom, nR_vrtxIndic2, nG_vrtxIndic2, nB_vrtxIndic2, 0x0000 }
  };*/
  GRADIENT_RECT grRect = {0, 1};

  pDC->GradientFill(vertex, 2, &grRect, 1, GRADIENT_FILL_RECT_H);

  //if (m_bIndicatorMode)
  //  pDC->GradientFill(vertexIndic, 2, &grRect, 1, GRADIENT_FILL_RECT_H);
}

#ifdef _DEBUG
void CView_UnderConstruction::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_UnderConstruction::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_UnderConstruction::OnContextMenu(CWnd*, CPoint point)
{
}

/*void CView_UnderConstruction::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
  CRect clientRect;
  GetClientRect(clientRect);
  pDC->SetMapMode(MM_ANISOTROPIC); // +y = down
  pDC->SetWindowExt(400, 450);
  pDC->SetViewportExt(clientRect.right, clientRect.bottom);
  pDC->SetViewportOrg(0, 0);
  //CCommonViewForm::OnPrepareDC(pDC, pInfo);
}*/
