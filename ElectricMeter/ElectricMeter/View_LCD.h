#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Ai_ColorStatic.h"
#include "Ai_ToolTip.h"
#include "afxbutton.h"
#include "Model_.h"

class CLCD_data;

class CView_LCD: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_LCD)

public:
  CView_LCD();
  virtual ~CView_LCD();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_SETTINGS_LCD
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();

  // Tables

  CCommonListCtrl m_lstPg;
  �Ai_ListCtrlEx* m_plstPgEx = nullptr;
  void CreatePageTable();

  CCommonListCtrl m_lstSt;
  �Ai_ListCtrlEx* m_plstStEx = nullptr;
  void CreateSetTable();

  CCommonListCtrl m_lstFr;
  �Ai_ListCtrlEx* m_plstFrEx = nullptr;
  void CreateFrameTable();

  virtual void OnLvnItemchangedLst(CListCtrl* pCtrl, int nRow_inView, bool bSelected) override final;
  void ReselectCurrentSelection();
  bool m_bIgnoreOnLvnItemchangedLst = false;
  afx_msg void OnNMSetfocusLstLcdFrames(NMHDR *pNMHDR, LRESULT *pResult);

  void RefillSetByPageChanged(CString& sPageName);
  void RefillFrameByPageChanged(CString& sPageName);
  void CheckBoldPageType();

  void CheckOnSetsByFrameList(CString& sPageName);
  void SelectSetBySelectFrame(CString& sFrameName);

  void ClearFrames();
  bool CheckOnLimit();
  CString GetCurrPage();
  void GetMaxAndOnCount(CString& sPageName, int&nMax, int& nOnCount);
  void InsertFrame();
  void SaveFrames();

  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  virtual bool UseAlternativeBgColor(CListCtrl* pCtrl, COLORREF& cNewCololr) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  CAi_ToolTip m_toolTip;

  CLCD_data* m_pData = nullptr;

  // Save to file / Load from file

  CMFCButton m_btnOpenLcdFromSpecXmlFile;
  CMFCButton m_btnSaveLcdToSpecXmlFile;
  afx_msg void OnBnClickedBtnOpenLcdFromFile();
  afx_msg void OnBnClickedBtnSaveLcdToFile();

private:

  CAi_ColorStatic m_stcPages;
  CAi_ColorStatic m_stcSets;
  CAi_ColorStatic m_stcFrames;

  // Tables frames

  CStatic m_stcSet;
  CStatic m_stcFrame;

  // Tables buttons

  CMFCButton m_btnPgSelAll;
  CMFCButton m_btnPgUnselAll;
  afx_msg void OnBnClickedBtnPgUnselAll();
  void OnPgUnselAll();
  afx_msg void OnBnClickedBtnPgSelAll();

  CMFCButton m_btnStSelAll;
  CMFCButton m_btnStUnselAll;
  afx_msg void OnBnClickedBtnStUnselAll();
  void OnStUnselAll();
  afx_msg void OnBnClickedBtnStSelAll();
  void OnStSelAll();

  // Exchange

  CMFCButton m_btnLoadLcdFromDevice;
  CMFCButton m_btnRecordLcdToDevice;

  afx_msg void OnBnClickedBtnLoadLcdFromDevice();
  virtual void DoAfterClearLoadData() override final;

  afx_msg void OnBnClickedBtnRecordLcdToDevice();

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) override final;

};
