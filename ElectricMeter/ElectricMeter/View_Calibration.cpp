#include "stdafx.h"
#include "Defines.h"
#include "Resource.h"
#include "Ai_Font.h"
#include "View_Calibration.h"
#include "DoubleEdit.h"

using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE (CView_Calibration, CCommonViewForm)

namespace ns_Clbr
{
	CString gsI1;
	CString gsI2;
	CString gsI3;
	CString gsU1;
	CString gsU2;
	CString gsU3;
	CString gsTime;
};


const CView_Calibration::frame_cntrl_pairT CView_Calibration::m_onlineData[] = {
	{ 0, FRAME_TYPE_CURRENT_R, FRAME_TYPE_CLBR_COEF_I_PH_1, IDC_EDIT_CURRENT_I1, IDC_EDIT_SETUP_I1, IDC_EDIT_ERRI1, IDC_EDT_I1, IDC_EDIT_AVER_I1 },
	{ 1, FRAME_TYPE_CURRENT_S, FRAME_TYPE_CLBR_COEF_I_PH_2, IDC_EDIT_CURRENT_I2, IDC_EDIT_SETUP_I2, IDC_EDIT_ERRI2, IDC_EDT_I2, IDC_EDIT_AVER_I2 },
	{ 2, FRAME_TYPE_CURRENT_T, FRAME_TYPE_CLBR_COEF_I_PH_3, IDC_EDIT_CURRENT_I3, IDC_EDIT_SETUP_I3, IDC_EDIT_ERRI3, IDC_EDT_I3, IDC_EDIT_AVER_I3 },
	{ 3, FRAME_TYPE_VOLTAGE_R, FRAME_TYPE_CLBR_COEF_U_PH_1, IDC_EDIT_VOLT_U1, IDC_EDIT_SETUP_U1, IDC_EDIT_ERRU1, IDC_EDT_U1, IDC_EDIT_AVER_U1 },
	{ 4, FRAME_TYPE_VOLTAGE_S, FRAME_TYPE_CLBR_COEF_U_PH_2, IDC_EDIT_VOLT_U2, IDC_EDIT_SETUP_U2, IDC_EDIT_ERRU2, IDC_EDT_U2, IDC_EDIT_AVER_U2 },
	{ 5, FRAME_TYPE_VOLTAGE_T, FRAME_TYPE_CLBR_COEF_U_PH_3, IDC_EDIT_VOLT_U3, IDC_EDIT_SETUP_U3, IDC_EDIT_ERRU3, IDC_EDT_U3, IDC_EDIT_AVER_U3 },	
};
const CView_Calibration::frame_cntrl_pairT CView_Calibration::m_manualCoeff[] = {
	{ 0, FRAME_TYPE_CURRENT_R, FRAME_TYPE_CLBR_COEF_I_PH_1, IDC_EDT_I1 },
	{ 1, FRAME_TYPE_CURRENT_S, FRAME_TYPE_CLBR_COEF_I_PH_2, IDC_EDT_I2 },
	{ 2, FRAME_TYPE_CURRENT_T, FRAME_TYPE_CLBR_COEF_I_PH_3, IDC_EDT_I3 },
	{ 3, FRAME_TYPE_VOLTAGE_R, FRAME_TYPE_CLBR_COEF_U_PH_1, IDC_EDT_U1 },
	{ 4, FRAME_TYPE_VOLTAGE_S, FRAME_TYPE_CLBR_COEF_U_PH_2, IDC_EDT_U2 },
	{ 5, FRAME_TYPE_VOLTAGE_T, FRAME_TYPE_CLBR_COEF_U_PH_3, IDC_EDT_U3 },
	{ 6, FRAME_TYPE_CLBR_COEF_CLOCK, FRAME_TYPE_CLBR_COEF_CLOCK, IDC_EDT_TIME },

};

CView_Calibration::CView_Calibration ()
: CCommonViewForm (CView_Calibration::IDD, L"CView_Calibration"), m_points (cMinCnt)
{


}

CView_Calibration::~CView_Calibration ()
{
}

BEGIN_MESSAGE_MAP (CView_Calibration, CCommonViewForm)

	ON_EN_KILLFOCUS (IDC_EDT_I1, &CView_Calibration::OnEnKillfocusEdtI1)
	ON_EN_KILLFOCUS (IDC_EDT_I2, &CView_Calibration::OnEnKillfocusEdtI2)
	ON_EN_KILLFOCUS (IDC_EDT_I3, &CView_Calibration::OnEnKillfocusEdtI3)
	ON_EN_KILLFOCUS (IDC_EDT_U1, &CView_Calibration::OnEnKillfocusEdtU1)
	ON_EN_KILLFOCUS (IDC_EDT_U2, &CView_Calibration::OnEnKillfocusEdtU2)
	ON_EN_KILLFOCUS (IDC_EDT_U3, &CView_Calibration::OnEnKillfocusEdtU3)
	ON_EN_KILLFOCUS (IDC_EDT_TIME, &CView_Calibration::OnEnKillfocusEdtTime)

	ON_BN_CLICKED (IDC_BTN_RECORD_I1, &CView_Calibration::OnBnClickedBtnRecordI1)
	ON_BN_CLICKED (IDC_BTN_RECORD_I2, &CView_Calibration::OnBnClickedBtnRecordI2)
	ON_BN_CLICKED (IDC_BTN_RECORD_I3, &CView_Calibration::OnBnClickedBtnRecordI3)
	ON_BN_CLICKED (IDC_BTN_RECORD_U1, &CView_Calibration::OnBnClickedBtnRecordU1)
	ON_BN_CLICKED (IDC_BTN_RECORD_U2, &CView_Calibration::OnBnClickedBtnRecordU2)
	ON_BN_CLICKED (IDC_BTN_RECORD_U3, &CView_Calibration::OnBnClickedBtnRecordU3)
	ON_BN_CLICKED (IDC_BTN_RECORD_TIME, &CView_Calibration::OnBnClickedBtnRecordTime)
	ON_BN_CLICKED (IDC_BTN_CHANGE_MODE, &CView_Calibration::OnBnClickedBtnChangeMode)
	ON_BN_CLICKED (IDC_BTN_READALL, &CView_Calibration::OnBnClickedBtnReadall)
	ON_BN_CLICKED (IDC_BTN_CALIBRATE, &CView_Calibration::OnBnClickedBtnCalibrate)
	ON_EN_CHANGE (IDC_EDIT_POINTS_COUNT, &CView_Calibration::OnEnChangeEditPointsCount)
	ON_EN_KILLFOCUS (IDC_EDIT_POINTS_COUNT, &CView_Calibration::OnEnKillfocusEditPointsCount)
	ON_BN_CLICKED (IDC_BTN_WRITEALL, &CView_Calibration::OnBnClickedBtnWriteall)
	ON_BN_CLICKED (IDC_BTN_PPS_ON, &CView_Calibration::OnBnClickedBtnPpsOn)
	ON_BN_CLICKED (IDC_BTN_PPS_OFF, &CView_Calibration::OnBnClickedBtnPpsOff)
	ON_BN_CLICKED (IDC_BTN_READ_TIME, &CView_Calibration::OnBnClickedBtnReadTime)
	ON_BN_CLICKED(IDC_BTN_PULSES, &CView_Calibration::OnBnClickedBtnPulses)
	ON_BN_CLICKED(IDC_BTN_CALIBRATE_CLK, &CView_Calibration::OnBnClickedBtnCalibrateClk)
END_MESSAGE_MAP ()

void CView_Calibration::DoDataExchange (CDataExchange* pDX)
{

	DDX_Control (pDX, IDC_EDT_TIME, m_edtTime);

	DDX_Control (pDX, IDC_EDIT_POINTS_COUNT, m_editPointCnt);

	DDX_Control (pDX, IDC_EDIT_SETUP_I1, m_editSetup_I_1);
	DDX_Control (pDX, IDC_EDIT_SETUP_I2, m_editSetup_I_2);
	DDX_Control (pDX, IDC_EDIT_SETUP_I3, m_editSetup_I_3);
	DDX_Control (pDX, IDC_EDIT_SETUP_U1, m_editSetup_U_1);
	DDX_Control (pDX, IDC_EDIT_SETUP_U2, m_editSetup_U_2);
	DDX_Control (pDX, IDC_EDIT_SETUP_U3, m_editSetup_U_3);

	DDX_Control (pDX, IDC_EDIT_CURRENT_I1, m_currentI1);
	DDX_Control (pDX, IDC_EDIT_CURRENT_I2, m_currentI2);
	DDX_Control (pDX, IDC_EDIT_CURRENT_I3, m_currentI3);
	DDX_Control (pDX, IDC_EDIT_VOLT_U1, m_currentU1);
	DDX_Control (pDX, IDC_EDIT_VOLT_U2, m_currentU2);
	DDX_Control (pDX, IDC_EDIT_VOLT_U3, m_currentU3);

	DDX_Control (pDX, IDC_EDIT_ERRI1, m_err1);
	DDX_Control (pDX, IDC_EDIT_ERRI2, m_err2);
	DDX_Control (pDX, IDC_EDIT_ERRI3, m_err3);
	DDX_Control (pDX, IDC_EDIT_ERRU1, m_err4);
	DDX_Control (pDX, IDC_EDIT_ERRU2, m_err5);
	DDX_Control (pDX, IDC_EDIT_ERRU3, m_err6);

	DDX_Control (pDX, IDC_EDIT_AVER_I1, m_averI1);
	DDX_Control (pDX, IDC_EDIT_AVER_I2, m_averI2);
	DDX_Control (pDX, IDC_EDIT_AVER_I3, m_averI3);
	DDX_Control (pDX, IDC_EDIT_AVER_U1, m_averU1);
	DDX_Control (pDX, IDC_EDIT_AVER_U2, m_averU2);
	DDX_Control (pDX, IDC_EDIT_AVER_U3, m_averU3);

	DDX_Control (pDX, IDC_EDT_I1, m_editCoeffI1);
	DDX_Control (pDX, IDC_EDT_I2, m_editCoeffI2);
	DDX_Control (pDX, IDC_EDT_I3, m_editCoeffI3);
	DDX_Control (pDX, IDC_EDT_U1, m_editCoeffU1);
	DDX_Control (pDX, IDC_EDT_U2, m_editCoeffU2);
	DDX_Control (pDX, IDC_EDT_U3, m_editCoeffU3);
	//

	DDX_Control (pDX, IDC_BTN_RECORD_I1, m_btnRecordI1);
	DDX_Control (pDX, IDC_BTN_RECORD_I2, m_btnRecordI2);
	DDX_Control (pDX, IDC_BTN_RECORD_I3, m_btnRecordI3);
	DDX_Control (pDX, IDC_BTN_RECORD_U1, m_btnRecordU1);
	DDX_Control (pDX, IDC_BTN_RECORD_U2, m_btnRecordU2);
	DDX_Control (pDX, IDC_BTN_RECORD_U3, m_btnRecordU3);
	DDX_Control (pDX, IDC_BTN_RECORD_TIME, m_btnRecordTime);


	DDX_Control (pDX, IDC_BTN_CHANGE_MODE, m_btnChangeMode);
	DDX_Control (pDX, IDC_BTN_READALL, m_btnReadAll);
}

void CView_Calibration::OnInitialUpdate ()
{
	CCommonViewForm::OnInitialUpdate ();

	SetInitialUpdateFlag (false_d (L"��� ������� �� ����������"));


	m_edtTime.SetLimitText (10);
	m_edtTime.SetWindowText (ns_Clbr::gsTime);

	m_currentI1.SetWindowText (CString (_T ("0")));
	m_currentI2.SetWindowText (CString (_T ("0")));
	m_currentI3.SetWindowText (CString (_T ("0")));
	m_currentU1.SetWindowText (CString (_T ("0")));
	m_currentU2.SetWindowText (CString (_T ("0")));
	m_currentU3.SetWindowText (CString (_T ("0")));

	m_editSetup_I_1.SetVal (5);
	m_editSetup_I_2.SetVal (5);
	m_editSetup_I_3.SetVal (5);
	m_editSetup_U_1.SetVal (220);
	m_editSetup_U_2.SetVal (220);
	m_editSetup_U_3.SetVal (220);

	m_editPointCnt.SetVal (m_points);

	WatchForCtrState (&m_btnRecordI1, rightAdmin);
	WatchForCtrState (&m_btnRecordI2, rightAdmin);
	WatchForCtrState (&m_btnRecordI3, rightAdmin);
	WatchForCtrState (&m_btnRecordU1, rightAdmin);
	WatchForCtrState (&m_btnRecordU2, rightAdmin);
	WatchForCtrState (&m_btnRecordU3, rightAdmin);
	WatchForCtrState (&m_btnRecordTime, rightAdmin);
	WatchForCtrState (&m_btnChangeMode, rightAdmin);

	SetInitialUpdateFlag (true_d (L"��� ������� ����������"));
}



void CView_Calibration::BeforeDestroyClass ()
{
}

void CView_Calibration::OnSize_ ()
{
}

void CView_Calibration::OnDraw (CDC* pDC)
{
}

bool CView_Calibration::OnNMCustomdrawLst (CListCtrl* pCtrl, int nRow_inView, int nCell)
{
	return true;
}

#ifdef _DEBUG
void CView_Calibration::AssertValid () const
{
	CCommonViewForm::AssertValid ();
}

void CView_Calibration::Dump (CDumpContext& dc) const
{
	CCommonViewForm::Dump (dc);
}
#endif //_DEBUG

void CView_Calibration::OnContextMenu (CWnd*, CPoint point)
{
}

void CView_Calibration::OnConnectionStateWasChanged ()
{
	CheckMeaningsViewByConnectionStatus ();
}

void CView_Calibration::OnEnKillfocusEdtI1 ()
{
	m_editCoeffI1.GetWindowText (ns_Clbr::gsI1);
}

void CView_Calibration::OnEnKillfocusEdtI2 ()
{
	m_editCoeffI2.GetWindowText (ns_Clbr::gsI2);
}

void CView_Calibration::OnEnKillfocusEdtI3 ()
{
	m_editCoeffI3.GetWindowText (ns_Clbr::gsI3);
}

void CView_Calibration::OnEnKillfocusEdtU1 ()
{
	m_editCoeffU1.GetWindowText (ns_Clbr::gsU1);
}

void CView_Calibration::OnEnKillfocusEdtU2 ()
{
	m_editCoeffU2.GetWindowText (ns_Clbr::gsU2);
}

void CView_Calibration::OnEnKillfocusEdtU3 ()
{
	m_editCoeffU3.GetWindowText (ns_Clbr::gsU3);
}

void CView_Calibration::OnEnKillfocusEdtTime ()
{
	m_edtTime.GetWindowText (ns_Clbr::gsTime);
}

void CView_Calibration::OnBnClickedBtnRecordI1 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_I_PH_1, &m_editCoeffI1);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordI2 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_I_PH_2, &m_editCoeffI2);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordI3 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_I_PH_3, &m_editCoeffI3);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordU1 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_U_PH_1, &m_editCoeffU1);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordU2 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_U_PH_2, &m_editCoeffU2);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordU3 ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_U_PH_3, &m_editCoeffU3);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnRecordTime ()
{
	SendDataTo (FRAME_TYPE_CLBR_COEF_CLOCK, &m_edtTime);
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnBnClickedBtnChangeMode ()
{
	SendDataTo (FRAME_TYPE_COMMAND_CLBR_CHG_MODE);
}

void CView_Calibration::UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult)
{
	if (!m_isSendCoeffAll) {
		if (ft >= FRAME_TYPE_CLBR_COEF_I_PH_1 && ft <= FRAME_TYPE_CLBR_COEF_CLOCK)	{
			bResult ? AfxMessageBox (L"�������� �������", MB_ICONINFORMATION) : AfxMessageBox (L"������ ������, ��������� �������", MB_ICONERROR);
			return;
		}
	}
	else //all
	{
		if (ft >= FRAME_TYPE_CLBR_COEF_I_PH_1 && ft <= FRAME_TYPE_CLBR_COEF_CLOCK && bResult != true)
		{
			AfxMessageBox (L"������ ������, ��������� �������", MB_ICONERROR);
			m_isSendCoeffAll = false;
		}
			// send all, comfirm only on last write
		if (ft == FRAME_TYPE_CLBR_COEF_U_PH_3) {
			bResult ? AfxMessageBox (L"��� ������������ ��������", MB_ICONINFORMATION) : AfxMessageBox (L"������ ������, ��������� �������", MB_ICONERROR);
			m_isSendCoeffAll = false;
		}
	}
}

void CView_Calibration::SendDataTo (teFrameTypes ft, CEdit* pedt)
{
	CString sData;
	if (pedt)
		pedt->GetWindowText (sData);

	//SetWaitCursor_answer(true); ������ �� ������ ���, � �� ����������� �� ������� ��������� �� ����� ����
	// ������� wait ������ ����� �������� ���� �� ����������� ����� ����� �������� ����� �����������

	BeginFormingSendPack (true_d (L"start"));

	if (sData.IsEmpty ())
		SendDataToDevice (ft);
	else
		SendDataToDevice (ft, sData);

	BeginFormingSendPack (false_d (L"start"));
}

void CView_Calibration::CheckMeaningsViewByConnectionStatus ()
{
	if (!IsConnection ())
	{
		ClearAllReadingReq ();
	}
	else
	{
		RequestAllCalibrations ();
		for (auto i : m_onlineData){
			SetParamTypeForRead (i.teOnline, false); // ��������� ������ ������ ���������� � �����
		}
	}
}

void CView_Calibration::UpdateParamMeaningsByReceivedDeviceData (teFrameTypes ft)
{
	varMeaning_t m = FindReceivedData (ft);
	wchar_t* aEndPtr = nullptr;
	double curVal = _tcstod (m, &aEndPtr);
	// ����� - ������ ������
	if (ft == FRAME_TYPE_CLBR_COEF_CLOCK)
	{
		static_cast<CEdit*>(GetDlgItem (IDC_EDT_TIME))->SetWindowText (m);
		return;
	}
	for (auto i : m_onlineData) 
	{
		if (ft == i.teCoeff)  // �������� ���� ������ �������������
		{
			static_cast<CEdit*>(GetDlgItem (i.idCoeff))->SetWindowText (m); return;
		}

		else if (ft == i.teOnline)	{ // �������� �����, �������� ������ � ������������
			int id = i.id;
			if (m_vMeasurements[id].size () >= m_points)	{

				m_vMeasurements[id].insert (m_vMeasurements[id].begin (), curVal);
				m_vMeasurements[id].pop_back ();
			}
			else
				m_vMeasurements[id].push_back (curVal);
			UpdateAverage ();
			CDoubleEdit* pEditOnline = dynamic_cast<CDoubleEdit*>(GetDlgItem (i.idOnline));
			pEditOnline->SetVal (curVal);
			return;
		}
	}
}

double CView_Calibration::GetAverage (unsigned int id)
{
	double acc = 0;
	int sz = m_vMeasurements[id].size ();
	if (sz == 0) return 0;
	for (int ix = 0; ix < sz; ix++)
		acc += m_vMeasurements[id][ix];
	double average = acc / sz;
	return average;
}

void CView_Calibration::UpdateAverage ()
{
	for (auto i : m_onlineData)	{
		CDoubleEdit* pAverage = dynamic_cast<CDoubleEdit*> (GetDlgItem (i.idAverage));
		CDoubleEdit* pEditErr = dynamic_cast<CDoubleEdit*>(GetDlgItem (i.iderr));
		CDoubleEdit* pEditSetup = dynamic_cast<CDoubleEdit*>(GetDlgItem (i.idSetup));
		ASSERT (pAverage);
		ASSERT (pEditErr);
		ASSERT (pEditSetup);
		double setupVar = pEditSetup->GetVal ();
		double err = 0;
		double average = GetAverage (i.id);
		if (setupVar != 0)
			err = 100.0 * (average - setupVar) / setupVar;		
		pEditErr->SetVal (err);		
		pAverage->SetVal (average);		
	}
}



void CView_Calibration::RequestAllCalibrations () {
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_I_PH_1, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_I_PH_2, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_I_PH_3, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_U_PH_1, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_U_PH_2, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_U_PH_3, true);
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_CLOCK, true);
}

void CView_Calibration::OnBnClickedBtnReadall ()
{
	RequestAllCalibrations ();
}

int CView_Calibration::CalcCoeff (int prevC, double val, double setup)
{
	double kx = (8192 - 1024 + prevC) / 8192.;
	double k1 = setup * kx / val;
	double k1int = k1 * 8192 - 8192 + 1024;
	return k1int;
}

void CView_Calibration::OnBnClickedBtnCalibrate ()
{
	for (auto i : m_onlineData)	{
		CDoubleEdit* pEditCoeff = dynamic_cast<CDoubleEdit*>(GetDlgItem (i.idCoeff));
		CDoubleEdit* pEditSetup = dynamic_cast<CDoubleEdit*>(GetDlgItem (i.idSetup));
		ASSERT (pEditSetup);
		ASSERT (pEditCoeff);
		int coeff = static_cast <int> (pEditCoeff->GetVal ());
		double setup = pEditSetup->GetVal ();
		int newCoeff = CalcCoeff (coeff, GetAverage (i.id), setup);
		if (newCoeff >= 0 && newCoeff <=1023)
			pEditCoeff->SetVal (newCoeff);
	}
	WriteAllCalibration ();
}

void CView_Calibration::WriteAllCalibration ()
{

	m_isSendCoeffAll = true;
	for (int i = 0; i < sizeof (m_onlineData) / sizeof (m_onlineData[0]); i++)
	{
		CEdit * pedit = dynamic_cast <CEdit *> (GetDlgItem (m_onlineData[i].idCoeff));
		SendDataTo (m_onlineData[i].teCoeff, pedit);
	}
	SendDataToDevice (FRAME_TYPE_COMMAND_WRITE_COEFF);
}

void CView_Calibration::OnEnChangeEditPointsCount ()
{	
}


void CView_Calibration::OnEnKillfocusEditPointsCount ()
{
	int count = 0;
	m_editPointCnt.GetVal (count);
	if (count >= cMinCnt && count <= cMaxCnt)
	{
		m_points = count;
		for (auto i : m_onlineData)
			m_vMeasurements[i.id].clear ();
	}
	m_editPointCnt.SetVal (m_points);
}


void CView_Calibration::OnBnClickedBtnWriteall ()
{
	WriteAllCalibration ();
}


void CView_Calibration::OnBnClickedBtnPpsOn ()
{

	SendDataTo (FRAME_TYPE_COMMAND_CLOCK_MODE_ON);
}


void CView_Calibration::OnBnClickedBtnPpsOff ()
{	
	SendDataTo (FRAME_TYPE_COMMAND_CLOCK_MODE_OFF);
}


void CView_Calibration::OnBnClickedBtnReadTime ()
{
	SetParamTypeForRead (FRAME_TYPE_CLBR_COEF_CLOCK, true);	
}


void CView_Calibration::OnBnClickedBtnPulses()
{
	CButton *pbtnReac = (CButton *)GetDlgItem(IDC_CHECK_REACT);
	if (pbtnReac->GetCheck() == BST_CHECKED) {
		//react
		SendDataTo(FRAME_TYPE_PULSES_CALCR);
	}
	else{
		SendDataTo(FRAME_TYPE_PULSES_CALC);
	}
}


void CView_Calibration::OnBnClickedBtnCalibrateClk()
{
	SendDataToDevice(FRAME_TYPE_CALIBRATE_CLK);
	// TODO: Add your control notification handler code here
}
