#include "StdAfx.h"
#include <algorithm>
#include <memory>
#include "Ai_Str.h"
#include "Ai_File.h"
#include "Model_.h"
#include "LCD_data.h"

using namespace mdl;
using namespace std;

namespace ns_LCD_dt
{
  const CString IDS_ENERGY_SETS = L"EnergySets";
  const CString IDS_ARCH_ENERGY_SETS = L"ArchiveOfEnergySets";
  const CString IDS_NET_SETS = L"NetSets";
  const CString IDS_MAX_POWER = L"MaxPower";
  const CString IDS_DATA_AND_TIME = L"DataAndTime";
  const CString IDS_SERVICE_FRAMES = L"ServiceFrames";
  const CString IDS_CYCLIC_INDICATION = L"CyclicIndication";
  const CString IDS_USER_FRAMES = L"UserFrames";
  const CString IDS_USER_FRAMES_NAME = L"����� ������������";
  #define MAX_USER_FRAMES 30

  struct DATA
  {
    DATA(int g, CString nm, CString sgn, teFrameTypes t)
      : nGroup(g)
      , sName(nm)
      , sSign(sgn)
      , type(t)
    {
    }

    int nGroup;
    CString sName;
    CString sSign;
    teFrameTypes type;
  };

  vector<DATA> g_data;
  vector<pair<CString, vector<CString>>> g_frames; // first like DATA::sName; second - selected frames from sets
  size_t g_nUserFrameIndex = 0;

  struct X_TAGS
  {
    CString sLCD = L"LCD";
    CString sSet = L"Set_";
    CString sPageName = L"PageName";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

} // ns_LCD_dt

using namespace ns_LCD_dt;

CLCD_data::CLCD_data(CParamsTreeNode* pInlaysNode)
  : m_pInlaysNode(pInlaysNode)
{
  FormData();
  TakeValueFromXmlTree(nullptr);
}

CLCD_data::~CLCD_data()
{

}

void CLCD_data::FormData()
{
  if (!ns_LCD_dt::g_data.empty())
    return;

  int i = 0;
  auto lbmSet = [&](int g, CString nm, CString sgn, teFrameTypes t)->void
  {
    ns_LCD_dt::g_data.push_back(ns_LCD_dt::DATA(g, nm, sgn, t));
  };

  vector <CString> empty_list;
  int nGr = 0;
  CString sGrXmlName;

  nGr = 0;
  //  ����� �� 30 ������(����� �������� ��� ���������� �� ������� + ����� + 2 ����� ��������).
  sGrXmlName = ns_LCD_dt::IDS_CYCLIC_INDICATION;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"����������� ���������", sGrXmlName, FRAME_TYPE_EMPTY);
  lbmSet(nGr, L"�������� ������� A", L"ActEnrA", FRAME_TYPE_ENERGY_ACT_SUM);
  lbmSet(nGr, L"�������� ������� A �� ������ 1", L"ActEnrATrf1", FRAME_TYPE_ENERGY_ACT_SUM_RATE1);
  lbmSet(nGr, L"�������� ������� A �� ������ 2", L"ActEnrATrf2", FRAME_TYPE_ENERGY_ACT_SUM_RATE2);
  lbmSet(nGr, L"�������� ������� A �� ������ 3", L"ActEnrATrf3", FRAME_TYPE_ENERGY_ACT_SUM_RATE3);
  lbmSet(nGr, L"�������� ������� A �� ������ 4", L"ActEnrATrf4", FRAME_TYPE_ENERGY_ACT_SUM_RATE4);
  lbmSet(nGr, L"�������� ������� A �� ������ 5", L"ActEnrATrf5", FRAME_TYPE_ENERGY_ACT_SUM_RATE5);
  lbmSet(nGr, L"�������� ������� A �� ������ 6", L"ActEnrATrf6", FRAME_TYPE_ENERGY_ACT_SUM_RATE6);
  lbmSet(nGr, L"�������� ������� A �� ������ 7", L"ActEnrATrf7", FRAME_TYPE_ENERGY_ACT_SUM_RATE7);
  lbmSet(nGr, L"�������� ������� A �� ������ 8", L"ActEnrATrf8", FRAME_TYPE_ENERGY_ACT_SUM_RATE8);
  lbmSet(nGr, L"���������� ������� R+", L"RctEngRPls", FRAME_TYPE_ENERGY_REACT_SUM);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 1", L"RctEngRPlsTrf1", FRAME_TYPE_ENERGY_REACT_SUM_RATE1);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 2", L"RctEngRPlsTrf2", FRAME_TYPE_ENERGY_REACT_SUM_RATE2);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 3", L"RctEngRPlsTrf3", FRAME_TYPE_ENERGY_REACT_SUM_RATE3);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 4", L"RctEngRPlsTrf4", FRAME_TYPE_ENERGY_REACT_SUM_RATE4);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 5", L"RctEngRPlsTrf5", FRAME_TYPE_ENERGY_REACT_SUM_RATE5);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 6", L"RctEngRPlsTrf6", FRAME_TYPE_ENERGY_REACT_SUM_RATE6);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 7", L"RctEngRPlsTrf7", FRAME_TYPE_ENERGY_REACT_SUM_RATE7);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 8", L"RctEngRPlsTrf8", FRAME_TYPE_ENERGY_REACT_SUM_RATE8);
  lbmSet(nGr, L"����������  ������� R-", L"RctEngRMns", FRAME_TYPE_ENERGY_REACT_NEG_SUM);
  lbmSet(nGr, L"���������� ������� R- �� ������ 1", L"RctEngRMnsTrf1", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE1);
  lbmSet(nGr, L"���������� ������� R- �� ������ 2", L"RctEngRMnsTrf2", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE2);
  lbmSet(nGr, L"���������� ������� R- �� ������ 3", L"RctEngRMnsTrf3", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE3);
  lbmSet(nGr, L"���������� ������� R- �� ������ 4", L"RctEngRMnsTrf4", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE4);
  lbmSet(nGr, L"���������� ������� R- �� ������ 5", L"RctEngRMnsTrf5", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE5);
  lbmSet(nGr, L"���������� ������� R- �� ������ 6", L"RctEngRMnsTrf6", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE6);
  lbmSet(nGr, L"���������� ������� R- �� ������ 7", L"RctEngRMnsTrf7", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE7);
  lbmSet(nGr, L"���������� ������� R- �� ������ 8", L"RctEngRMnsTrf8", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE8);
  lbmSet(nGr, L"���� � �����", L"DatAndTim", FRAME_TYPE_DATE_TIME);

  nGr = 1;
  sGrXmlName = ns_LCD_dt::IDS_USER_FRAMES;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  ns_LCD_dt::g_nUserFrameIndex = ns_LCD_dt::g_frames.size() - 1;
  lbmSet(nGr, ns_LCD_dt::IDS_USER_FRAMES_NAME, sGrXmlName, FRAME_TYPE_FGROUP_007);
  for (int i = 0; i < MAX_USER_FRAMES; ++i) // ����� �� 30 ������
    lbmSet(nGr, L"", L"", FRAME_TYPE_EMPTY); // ����� ����� ���� �����

  nGr = 2;
  sGrXmlName = ns_LCD_dt::IDS_ENERGY_SETS;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"�������������� ���������", sGrXmlName, FRAME_TYPE_FGROUP_001);
  lbmSet(nGr, L"�������� ������� A", L"ActEnrA", FRAME_TYPE_ENERGY_ACT_SUM);
  lbmSet(nGr, L"�������� ������� A �� ������ 1", L"ActEnrATrf1", FRAME_TYPE_ENERGY_ACT_SUM_RATE1);
  lbmSet(nGr, L"�������� ������� A �� ������ 2", L"ActEnrATrf2", FRAME_TYPE_ENERGY_ACT_SUM_RATE2);
  lbmSet(nGr, L"�������� ������� A �� ������ 3", L"ActEnrATrf3", FRAME_TYPE_ENERGY_ACT_SUM_RATE3);
  lbmSet(nGr, L"�������� ������� A �� ������ 4", L"ActEnrATrf4", FRAME_TYPE_ENERGY_ACT_SUM_RATE4);
  lbmSet(nGr, L"�������� ������� A �� ������ 5", L"ActEnrATrf5", FRAME_TYPE_ENERGY_ACT_SUM_RATE5);
  lbmSet(nGr, L"�������� ������� A �� ������ 6", L"ActEnrATrf6", FRAME_TYPE_ENERGY_ACT_SUM_RATE6);
  lbmSet(nGr, L"�������� ������� A �� ������ 7", L"ActEnrATrf7", FRAME_TYPE_ENERGY_ACT_SUM_RATE7);
  lbmSet(nGr, L"�������� ������� A �� ������ 8", L"ActEnrATrf8", FRAME_TYPE_ENERGY_ACT_SUM_RATE8);
  lbmSet(nGr, L"���������� ������� R+", L"RctEngRPls", FRAME_TYPE_ENERGY_REACT_SUM);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 1", L"RctEngRPlsTrf1", FRAME_TYPE_ENERGY_REACT_SUM_RATE1);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 2", L"RctEngRPlsTrf2", FRAME_TYPE_ENERGY_REACT_SUM_RATE2);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 3", L"RctEngRPlsTrf3", FRAME_TYPE_ENERGY_REACT_SUM_RATE3);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 4", L"RctEngRPlsTrf4", FRAME_TYPE_ENERGY_REACT_SUM_RATE4);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 5", L"RctEngRPlsTrf5", FRAME_TYPE_ENERGY_REACT_SUM_RATE5);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 6", L"RctEngRPlsTrf6", FRAME_TYPE_ENERGY_REACT_SUM_RATE6);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 7", L"RctEngRPlsTrf7", FRAME_TYPE_ENERGY_REACT_SUM_RATE7);
  lbmSet(nGr, L"���������� ������� R+ �� ������ 8", L"RctEngRPlsTrf8", FRAME_TYPE_ENERGY_REACT_SUM_RATE8);
  lbmSet(nGr, L"����������  ������� R-", L"RctEngRMns", FRAME_TYPE_ENERGY_REACT_NEG_SUM);
  lbmSet(nGr, L"���������� ������� R- �� ������ 1", L"RctEngRMnsTrf1", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE1);
  lbmSet(nGr, L"���������� ������� R- �� ������ 2", L"RctEngRMnsTrf2", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE2);
  lbmSet(nGr, L"���������� ������� R- �� ������ 3", L"RctEngRMnsTrf3", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE3);
  lbmSet(nGr, L"���������� ������� R- �� ������ 4", L"RctEngRMnsTrf4", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE4);
  lbmSet(nGr, L"���������� ������� R- �� ������ 5", L"RctEngRMnsTrf5", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE5);
  lbmSet(nGr, L"���������� ������� R- �� ������ 6", L"RctEngRMnsTrf6", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE6);
  lbmSet(nGr, L"���������� ������� R- �� ������ 7", L"RctEngRMnsTrf7", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE7);
  lbmSet(nGr, L"���������� ������� R- �� ������ 8", L"RctEngRMnsTrf8", FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE8);

  nGr = 3;
  sGrXmlName = ns_LCD_dt::IDS_ARCH_ENERGY_SETS;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"����� �������������� ����������", sGrXmlName, FRAME_TYPE_FGROUP_002);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������", L"ActEngAEndLstMth", FRAME_TYPE_ENERGY_ACT_MONTH);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 1", L"ActEngAEndLstMthTrf1", FRAME_TYPE_ENERGY_ACT_MONTH_RATE1);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 2", L"ActEngAEndLstMthTrf2", FRAME_TYPE_ENERGY_ACT_MONTH_RATE2);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 3", L"ActEngAEndLstMthTrf3", FRAME_TYPE_ENERGY_ACT_MONTH_RATE3);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 4", L"ActEngAEndLstMthTrf4", FRAME_TYPE_ENERGY_ACT_MONTH_RATE4);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 5", L"ActEngAEndLstMthTrf5", FRAME_TYPE_ENERGY_ACT_MONTH_RATE5);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 6", L"ActEngAEndLstMthTrf6", FRAME_TYPE_ENERGY_ACT_MONTH_RATE6);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 7", L"ActEngAEndLstMthTrf7", FRAME_TYPE_ENERGY_ACT_MONTH_RATE7);
  lbmSet(nGr, L"�������� ������� A �� ����� ����������� ������ �� ������ 8", L"ActEngAEndLstMthTrf8", FRAME_TYPE_ENERGY_ACT_MONTH_RATE8);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������", L"ActEngRPlsEndLstMth", FRAME_TYPE_ENERGY_REACT_MONTH);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 1", L"ActEngRPlsEndLstMthTrf1", FRAME_TYPE_ENERGY_REACT_MONTH_RATE1);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 2", L"ActEngRPlsEndLstMthTrf2", FRAME_TYPE_ENERGY_REACT_MONTH_RATE2);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 3", L"ActEngRPlsEndLstMthTrf3", FRAME_TYPE_ENERGY_REACT_MONTH_RATE3);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 4", L"ActEngRPlsEndLstMthTrf4", FRAME_TYPE_ENERGY_REACT_MONTH_RATE4);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 5", L"ActEngRPlsEndLstMthTrf5", FRAME_TYPE_ENERGY_REACT_MONTH_RATE5);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 6", L"ActEngRPlsEndLstMthTrf6", FRAME_TYPE_ENERGY_REACT_MONTH_RATE6);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 7", L"ActEngRPlsEndLstMthTrf7", FRAME_TYPE_ENERGY_REACT_MONTH_RATE7);
  lbmSet(nGr, L"���������� ������� R+ �� ����� ����������� ������ �� ������ 8", L"ActEngRPlsEndLstMthTrf8", FRAME_TYPE_ENERGY_REACT_MONTH_RATE8);
  lbmSet(nGr, L"���������� ������� R- �� ����� ����������� ������", L"ActEngRMnsEndLstMth", FRAME_TYPE_ENERGY_REACT_NEG_MONTH);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 1", L"ActEngRMnsEndLstMthTrf1", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE1);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 2", L"ActEngRMnsEndLstMthTrf2", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE2);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 3", L"ActEngRMnsEndLstMthTrf3", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE3);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 4", L"ActEngRMnsEndLstMthTrf4", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE4);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 5", L"ActEngRMnsEndLstMthTrf5", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE5);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 6", L"ActEngRMnsEndLstMthTrf6", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE6);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 7", L"ActEngRMnsEndLstMthTrf7", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE7);
  lbmSet(nGr, L"����������  ������� R- �� ����� ����������� ������ �� ������ 8", L"ActEngRMnsEndLstMthTrf8", FRAME_TYPE_ENERGY_REACT_NEG_MONTH_RATE8);

  nGr = 4;
  sGrXmlName = ns_LCD_dt::IDS_MAX_POWER;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"������������ ��������", sGrXmlName, FRAME_TYPE_FGROUP_004);
  lbmSet(nGr, L"�������� �������� �������� ��������", L"MrnMaxActPwr", FRAME_TYPE_POWER_P_MAX_MORNING);
  lbmSet(nGr, L"�������� �������� �������� ��������", L"EvnMaxActPwr", FRAME_TYPE_POWER_P_MAX_EVENING);
  lbmSet(nGr, L"�������� �������� ������������� ���������� ��������", L"MrnMaxPlsRctPwr", FRAME_TYPE_POWER_QP_MAX_MORNING);
  lbmSet(nGr, L"�������� �������� ������������� ���������� ��������", L"EvnMaxPlsRctPwr", FRAME_TYPE_POWER_QP_MAX_EVENING);
  lbmSet(nGr, L"�������� �������� �������������� ���������� ��������", L"MrnMaxMnsRctPwr", FRAME_TYPE_POWER_QM_MAX_MORNING);
  lbmSet(nGr, L"�������� �������� �������������� ���������� ��������", L"EvnMaxMnsRctPwr", FRAME_TYPE_POWER_QM_MAX_EVENING);

  nGr = 5;
  sGrXmlName = ns_LCD_dt::IDS_NET_SETS;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"��������� ����", sGrXmlName, FRAME_TYPE_FGROUP_003);
  lbmSet(nGr, L"������ ��������", L"TtlPwr", FRAME_TYPE_POWER_S_SUM);
  lbmSet(nGr, L"������ �������� ���� 1", L"TtlPwrPhs1", FRAME_TYPE_POWER_S_R);
  lbmSet(nGr, L"������ �������� ���� 2", L"TtlPwrPhs2", FRAME_TYPE_POWER_S_S);
  lbmSet(nGr, L"������ �������� ���� 3", L"TtlPwrPhs3", FRAME_TYPE_POWER_S_T);
  lbmSet(nGr, L"��������� �������� ��������", L"SumActPwr", FRAME_TYPE_POWER_P_SUM);
  lbmSet(nGr, L"�������� �������� ���� 1", L"ActPwrPhs1", FRAME_TYPE_POWER_P_R);
  lbmSet(nGr, L"�������� �������� ���� 2", L"ActPwrPhs2", FRAME_TYPE_POWER_P_S);
  lbmSet(nGr, L"�������� �������� ���� 3", L"ActPwrPhs3", FRAME_TYPE_POWER_P_T);
  lbmSet(nGr, L"��������� ���������� ��������", L"SumRctPwr", FRAME_TYPE_POWER_Q_SUM);
  lbmSet(nGr, L"���������� �������� ���� 1", L"RctPwrPhs1", FRAME_TYPE_POWER_Q_R);
  lbmSet(nGr, L"���������� �������� ���� 2", L"RctPwrPhs2", FRAME_TYPE_POWER_Q_S);
  lbmSet(nGr, L"���������� �������� ���� 3", L"RctPwrPhs3", FRAME_TYPE_POWER_Q_T);
  lbmSet(nGr, L"���������� ���� 1", L"VlgPhs1", FRAME_TYPE_VOLTAGE_R);
  lbmSet(nGr, L"���������� ���� 2", L"VlgPhs2", FRAME_TYPE_VOLTAGE_S);
  lbmSet(nGr, L"���������� ���� 3", L"VlgPhs3", FRAME_TYPE_VOLTAGE_T);
  lbmSet(nGr, L"��� ���� 1", L"CrtPhs1", FRAME_TYPE_CURRENT_R);
  lbmSet(nGr, L"��� ���� 2", L"CrtPhs2", FRAME_TYPE_CURRENT_S);
  lbmSet(nGr, L"��� ���� 3", L"CrtPhs3", FRAME_TYPE_CURRENT_T);
  lbmSet(nGr, L"����������� ��������", L"CftPwr", FRAME_TYPE_COSFI_SUM);
  lbmSet(nGr, L"����������� �������� ���� 1", L"CftPwrPhs1", FRAME_TYPE_COSFI_R);
  lbmSet(nGr, L"����������� �������� ���� 2", L"CftPwrPhs2", FRAME_TYPE_COSFI_S);
  lbmSet(nGr, L"����������� �������� ���� 3", L"CftPwrPhs3", FRAME_TYPE_COSFI_T);
  lbmSet(nGr, L"������� ����", L"FrqNet", FRAME_TYPE_FREQUENCY);

  nGr = 6;
  sGrXmlName = ns_LCD_dt::IDS_DATA_AND_TIME;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"���� � �����", sGrXmlName, FRAME_TYPE_FGROUP_005);
  lbmSet(nGr, L"���� � �����", L"DatAndTim", FRAME_TYPE_DATE_TIME);
  lbmSet(nGr, L"��������� �����", L"CorWch", FRAME_TYPE_DATE_TIME_EDIT);
  lbmSet(nGr, L"������� ������ �������� ��� 1", L"TimBgnTrfZon1", FRAME_TYPE_TIME_RATE1);
  lbmSet(nGr, L"������� ������ �������� ��� 2", L"TimBgnTrfZon2", FRAME_TYPE_TIME_RATE2);
  lbmSet(nGr, L"������� ������ �������� ��� 3", L"TimBgnTrfZon3", FRAME_TYPE_TIME_RATE3);
  lbmSet(nGr, L"������� ������ �������� ��� 4", L"TimBgnTrfZon4", FRAME_TYPE_TIME_RATE4);
  lbmSet(nGr, L"������� ������ �������� ��� 5", L"TimBgnTrfZon5", FRAME_TYPE_TIME_RATE5);
  lbmSet(nGr, L"������� ������ �������� ��� 6", L"TimBgnTrfZon6", FRAME_TYPE_TIME_RATE6);
  lbmSet(nGr, L"������� ������ �������� ��� 7", L"TimBgnTrfZon7", FRAME_TYPE_TIME_RATE7);
  lbmSet(nGr, L"������� ������ �������� ��� 8", L"TimBgnTrfZon8", FRAME_TYPE_TIME_RATE8);
  lbmSet(nGr, L"������� ������ �������� ��� 9", L"TimBgnTrfZon9", FRAME_TYPE_TIME_RATE9);
  lbmSet(nGr, L"������� ������ �������� ��� 10", L"TimBgnTrfZon10", FRAME_TYPE_TIME_RATE10);
  lbmSet(nGr, L"������� ������ �������� ��� 11", L"TimBgnTrfZon11", FRAME_TYPE_TIME_RATE11);
  lbmSet(nGr, L"������� ������ �������� ��� 12", L"TimBgnTrfZon12", FRAME_TYPE_TIME_RATE12);

  nGr = 7;
  sGrXmlName = ns_LCD_dt::IDS_SERVICE_FRAMES;
  ns_LCD_dt::g_frames.push_back(pair<CString, vector<CString>>(sGrXmlName, empty_list));
  lbmSet(nGr, L"��������� �����", sGrXmlName, FRAME_TYPE_FGROUP_006);
  lbmSet(nGr, L"����������������� �����", L"IdxNmb", FRAME_TYPE_DEV_ID);
  lbmSet(nGr, L"��������� 1", L"Interface1", FRAME_TYPE_INTERFACE1_SETT);
  lbmSet(nGr, L"��������� 2", L"Interface2", FRAME_TYPE_INTERFACE2_SETT);
  lbmSet(nGr, L"��������� 3", L"Interface3", FRAME_TYPE_INTERFACE3_SETT);
  lbmSet(nGr, L"��������� 4", L"Interface4", FRAME_TYPE_INTERFACE4_SETT);
  lbmSet(nGr, L"������ 1", L"Err1", FRAME_TYPE_ERROR1);
  lbmSet(nGr, L"������ 2", L"Err2", FRAME_TYPE_ERROR2);
  lbmSet(nGr, L"������ 3", L"Err3", FRAME_TYPE_ERROR3);
  lbmSet(nGr, L"������ 4", L"Err4", FRAME_TYPE_ERROR4);
  lbmSet(nGr, L"�����������", L"ErrAll", FRAME_TYPE_ALL_ERRORS);
}

void CLCD_data::TakeValueFromXmlTree(CParamsTreeNode* pMainNode)
{
  if (!pMainNode)
  {
    CString sTag = ns_LCD_dt::g_xTags.sLCD;
    m_pLcdNode = m_pInlaysNode->FindFirstNode(sTag);
    if (!m_pLcdNode)
    {
      m_pLcdNode = m_pInlaysNode->AddNode(sTag);
      return;
    }
    pMainNode = m_pLcdNode;
  }

  for (size_t j = 0; j < 30; ++j)
  {
    ASSERT(j < 29);

    CString sTag;
    sTag.Format(L"%s%.2d", ns_LCD_dt::g_xTags.sSet, j + 1);
    CParamsTreeNode* pSetNode = pMainNode->FindFirstNode(sTag);
    if (!pSetNode)
      break;
    CString sGroupXmlSign = pSetNode->GetAttributeValue(ns_LCD_dt::g_xTags.sPageName);
    for (auto& p : ns_LCD_dt::g_frames)
    {
      if (p.first == sGroupXmlSign) // ����� �������� �������
      {
        p.second.clear();
        CString sData = pSetNode->GetAttributeValue(ns_LCD_dt::g_xTags.sDataSet);
        if (!sData.IsEmpty())
        {
          vector<CString> signs;
          CAi_Str::CutString(sData, L";", &signs);
          if (!signs.empty())
          {
            vector<CString> frames = CheckFrames(sGroupXmlSign, &signs);
            p.second = frames;
          }
        }
      }
    }
  }
}

void CLCD_data::PutValueToXmlTree()
{
  m_pLcdNode->DeleteAllNodes();
  for (size_t j = 0; j < ns_LCD_dt::g_frames.size(); ++j)
  {
    CString sTag;
    sTag.Format(L"%s%.2d", ns_LCD_dt::g_xTags.sSet, j + 1);
    CParamsTreeNode* pSetNode = m_pLcdNode->AddNode(sTag);
    if (pSetNode)
    {
      pSetNode->AddAttribute(ns_LCD_dt::g_xTags.sPageName, ns_LCD_dt::g_frames[j].first);
      CString sData;
      for (auto& d : ns_LCD_dt::g_frames[j].second)
      {
        CString sXmlSign = GetChildXmlSign(d);
        if (!sXmlSign.IsEmpty())
        {
          if (!sData.IsEmpty())
            sData += L";";
          sData += sXmlSign;
        }
      }
      pSetNode->AddAttribute(ns_LCD_dt::g_xTags.sDataSet, sData);
    }
  }
}

bool CLCD_data::OpenLcdToFile()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(TRUE, _T("xml"), nullptr, OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  dlg.GetOFN().lpstrTitle = L"�������� �������� ���";

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    if (CAi_File::IsFileExist(sFileName))
    {
      unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
      CParamsTreeNode* pLcdNode = pXmlAid.get()->CreateTreeFromXmlFile(sFileName);
      if (!pLcdNode
          || pLcdNode->GetName() != ns_LCD_dt::g_xTags.sLCD)
      {
        CString s;
        s.Format(L"��������� ����:\n%s\n�� �������� �������� ���!", sFileName);
        AfxMessageBox(s, MB_ICONWARNING);
        return false;
      }

      // ����� �� ������ ������ � ��������� ��

      TakeValueFromXmlTree(pLcdNode);

      // ��������� ����� ������ � xml �� ��������� ��

      PutValueToXmlTree();

      // �������� �������

      AfxMessageBox(L"����� ��������� ��� ������� ��������� �� �����!", MB_ICONINFORMATION);
      return true;
    }
  }
  return false;
}

void CLCD_data::SaveLcdToFile()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(FALSE, _T("xml"), nullptr, OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  CString sTitle = L"C��������� �������� ���";
  dlg.GetOFN().lpstrTitle = sTitle.GetBuffer(_MAX_PATH);
  CString sFile = L"��-������������. ��������� ���.xml";
  dlg.GetOFN().lpstrFile = sFile.GetBuffer(_MAX_PATH);

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
    bool bResult = pXmlAid.get()->CreateXmlFileFromTree(sFileName, m_pLcdNode);

    CString s;
    if (bResult)
      s.Format(L"����:\n%s\n� ����������� ��� ������� ��������!", sFileName);
    else
      s.Format(L"������ ���������� ����� � ����������� ���:\n%s!", sFileName);
    AfxMessageBox(s, bResult ? MB_ICONINFORMATION : MB_ICONWARNING);
  }
}

void CLCD_data::ParseReceivedData(size_t nForPage, CString& sData)
{
  sData.TrimRight(L";");
  if (sData == L"0" || sData == L"0;")
    return;
  if (nForPage >= ns_LCD_dt::g_frames.size())
    return;
  if (nForPage >= ns_LCD_dt::g_frames.size())
    return;

  vector<CString> strs;
  if (!CAi_Str::CutString(sData, L";", &strs))
    return;
  if (strs.size() != 2)
    return;

  int nFrameCount = CAi_Str::ToInt(strs[0]);
  CString sFrames = strs[1];
  vector<CString> fs;
  if (!CAi_Str::CutString(sFrames, L",", &fs))
    return;
  if (nFrameCount != static_cast<int>(fs.size()))
    return;

  CString sPageName = GetPages().at(nForPage);
  int nMaxCount = min(nFrameCount, GetMaxCount(sPageName));

  CString sGroupXmlSign = GetPageXmlSign(nForPage);
  if (sGroupXmlSign.IsEmpty())
    return;

  vector<CString> xmls;
  for (auto& f : fs)
  {
    CString sFrame = GetChildFrame2(f);
    if (!sFrame.IsEmpty())
    {
      CString sXml = GetChildXmlSign(sFrame);
      xmls.push_back(sXml);
    }
  }
  if (xmls.empty())
    return;

  vector<CString> checkedFrames = CheckFrames(sGroupXmlSign, &xmls);
  if (checkedFrames.empty())
    return;

  ns_LCD_dt::g_frames[nForPage].second = checkedFrames;
}

int CLCD_data::GetSignificantPageCount()
{
  int nSignCount = 0;
  for (auto& f : ns_LCD_dt::g_frames)
  {
    if (!f.second.empty())
      ++nSignCount;
  }
  return nSignCount;
}

void CLCD_data::FormData(size_t nForPage, CString& sData)
{
  sData = L"0";
  if (nForPage >= ns_LCD_dt::g_frames.size())
    return;
  if (ns_LCD_dt::g_frames[nForPage].second.empty())
    return;

  size_t nSize = ns_LCD_dt::g_frames[nForPage].second.size();
  sData.Format(L"%d;", nSize);
  bool bFirst = true;
  for (auto& sFrame : ns_LCD_dt::g_frames[nForPage].second)
  {
    if (!bFirst)
      sData += L",";
    sData += GetChildEnumId(sFrame);
    bFirst = false;
  }
}

// ��������, ��� ����� ����������� � ������� ��������
vector<CString> CLCD_data::CheckFrames(CString& sGroupXmlSign, vector<CString>* pFrames)
{
  int nGr = -1;
  vector<CString> frames;

  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      if (i.sSign == sGroupXmlSign)
      {
        bool bTakeSign = true;
        vector<CString> sets = GetSets(i.sName, bTakeSign);
        for (size_t f = 0; f < pFrames->size(); ++f)
        {
          auto where_ = find_if(sets.begin(), sets.end(), [&](CString set)
          {
            return (pFrames->at(f) == set);
          });
          if (where_ != sets.end())
          {
            CString sName = GetChildFrame(*where_);
            if (!sName.IsEmpty())
              frames.push_back(sName);
          }
        }
      }
    }
  }

  return frames;
}

vector<CString> CLCD_data::GetPages()
{
  vector<CString> pgs;
  int nGr = -1;
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      pgs.push_back(i.sName);
    }
  }
  return pgs;
}

vector<CString> CLCD_data::GetSets(CString& sForPage, bool bTakeSign)
{
  vector<CString> sets;
  int nGr = -1;
  bool bIgnoreThisGr = false;

  if (sForPage == ns_LCD_dt::IDS_USER_FRAMES_NAME) // take all
  {
    for (auto& i : ns_LCD_dt::g_data)
    {
      if (nGr != i.nGroup) // group name
      {
        nGr = i.nGroup;
        
        bIgnoreThisGr = false;
        if (i.sName == ns_LCD_dt::IDS_USER_FRAMES_NAME)
          bIgnoreThisGr = true;
        if (i.sName == ns_LCD_dt::IDS_CYCLIC_INDICATION)
          bIgnoreThisGr = true;

        continue;
      }

      if (bIgnoreThisGr)
        continue;

      if (find(sets.begin(), sets.end(), bTakeSign ? i.sSign : i.sName) == sets.end()) // absent?
        sets.push_back(bTakeSign ? i.sSign : i.sName);
    }
  }
  else // take from spec group before user frame group
  {
    bool bTake = false;

    for (auto& i : ns_LCD_dt::g_data)
    {
      if (nGr != i.nGroup) // group name
      {
        nGr = i.nGroup;
        bTake = false;
        if (i.sName == sForPage)
          bTake = true;
        continue;
      }

      if (bTake)
        sets.push_back(bTakeSign ? i.sSign : i.sName);
    }
  }

  return sets;
}

vector<CString> CLCD_data::GetFrames(CString& sForPage)
{
  int nGr = -1;
  CString sGroupXmlSign;

  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      if (i.sName == sForPage)
      {
        sGroupXmlSign = i.sSign;
        break;
      }
    }
  }

  if (!sGroupXmlSign.IsEmpty())
    return ns_LCD_dt::g_frames[nGr].second;

  ASSERT(false);
  vector<CString> zero;
  return zero;
}

int CLCD_data::GetMaxCount(CString& sForPage)
{
  if (sForPage == ns_LCD_dt::IDS_USER_FRAMES_NAME)
    return MAX_USER_FRAMES;
  vector<CString> sets = GetSets(sForPage);
  return static_cast<int>(sets.size());
}

CString CLCD_data::GetPageXmlSign(size_t orderIdx)
{
  int nGr = -1;
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      ++nGr;
      if (orderIdx == static_cast<size_t>(nGr))
        return i.sSign;
    }
  }
  ASSERT(false);
  return L"";
}

CString CLCD_data::GetChildXmlSign(CString& sFrame)
{
  int nGr = -1;
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      continue;
    }
    if (sFrame == i.sName) // group name
      return i.sSign;
  }
  ASSERT(false);
  return L"";
}

CString CLCD_data::GetChildEnumId(CString& sFrame)
{
  int nGr = -1;
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      continue;
    }
    if (sFrame == i.sName)
    {
      int n = static_cast<int>(i.type);
      return CAi_Str::ToString(n);
    }
  }
  ASSERT(false);
  return L"";
}

CString CLCD_data::GetChildFrame(CString& sSign)
{
  int nGr = -1;
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      continue;
    }
    if (sSign == i.sSign)
      return i.sName;
  }
  ASSERT(false);
  return L"";
}

CString CLCD_data::GetChildFrame2(CString& sEnumId)
{
  int nGr = -1;
  int nType = CAi_Str::ToInt(sEnumId);
  teFrameTypes type = static_cast<teFrameTypes>(nType);
  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      continue;
    }
    if (type == i.type)
      return i.sName;
  }
  ASSERT(false);
  return L"";
}

void CLCD_data::UpdateFrames(CString& sForPage, vector<CString>* pFrames)
{
  // Collect xml signs by pFrames names

  CString sGroupXmlSign;
  int nGr = -1;
  bool bIgnoreThisGr = false;

  if (sForPage == ns_LCD_dt::IDS_USER_FRAMES_NAME) // take all
  {
    sGroupXmlSign = ns_LCD_dt::IDS_USER_FRAMES;
    ns_LCD_dt::g_frames[ns_LCD_dt::g_nUserFrameIndex].second.clear();

    for (auto& i : ns_LCD_dt::g_data)
    {
      if (nGr != i.nGroup) // group name
      {
        nGr = i.nGroup;

        bIgnoreThisGr = false;
        if (i.sName == ns_LCD_dt::IDS_USER_FRAMES_NAME)
          bIgnoreThisGr = true;
        if (i.sName == ns_LCD_dt::IDS_CYCLIC_INDICATION)
          bIgnoreThisGr = true;

        continue;
      }

      if (bIgnoreThisGr)
        continue;

      for (auto& j : *pFrames)
      {
        if (j == i.sName) // name was included by user
        {
          auto lst = ns_LCD_dt::g_frames[ns_LCD_dt::g_nUserFrameIndex].second;
          if (find(lst.begin(), lst.end(), i.sName) == lst.end()) // absent?
            ns_LCD_dt::g_frames[ns_LCD_dt::g_nUserFrameIndex].second.push_back(i.sName);
          break;
        }
      }
    }
  }
  else
  {
    for (auto& i : ns_LCD_dt::g_data)
    {
      if (nGr != i.nGroup) // group name
      {
        if (!sGroupXmlSign.IsEmpty()) // next group
          break;
        nGr = i.nGroup;
        if (i.sName == sForPage)
        {
          sGroupXmlSign = i.sSign;
          ns_LCD_dt::g_frames[nGr].second.clear();
          continue;
        }
      }

      if (!sGroupXmlSign.IsEmpty())
      {
        for (auto& j : *pFrames)
        {
          if (j == i.sName) // name was included by user
          {
            ns_LCD_dt::g_frames[nGr].second.push_back(i.sName);
            break;
          }
        }
      }
    }
  }

  // Save in xlm file

  PutValueToXmlTree();
}

void CLCD_data::IncludeAllForPage(CString& sForPage)
{
  int nGr = -1;
  CString sGroupXmlSign;

  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      if (i.sName == sForPage)
      {
        sGroupXmlSign = i.sSign;
        break;
      }
    }
  }

  if (!sGroupXmlSign.IsEmpty())
  {
    vector<CString> sets = GetSets(sForPage);
    if (sForPage == ns_LCD_dt::IDS_USER_FRAMES_NAME)
    {
      if (sets.size() > MAX_USER_FRAMES)
        sets.erase(sets.begin() + MAX_USER_FRAMES, sets.end()); // get first 30 items of sets
    }
    ns_LCD_dt::g_frames[nGr].second = sets;
  }
}

void CLCD_data::FreeAllForPage(CString& sForPage)
{
  int nGr = -1;
  CString sGroupXmlSign;

  for (auto& i : ns_LCD_dt::g_data)
  {
    if (nGr != i.nGroup) // group name
    {
      nGr = i.nGroup;
      if (i.sName == sForPage)
      {
        sGroupXmlSign = i.sSign;
        break;
      }
    }
  }

  if (!sGroupXmlSign.IsEmpty())
    ns_LCD_dt::g_frames[nGr].second.clear();
}