#pragma once

#include "XmlAid.h"
#include "XmlTagCollection.h"

/*
���� xmlMainFile � xmlBackupFile ����. ��� �������� ��������� ��������� �������� �� xmlMainFile � �� ���������
�� ����� �������� xmlBackupFile ����.
����� ��� ������ ������������ � xmlBackupFile ������. xmlMainFile ���� ����������� ������ ���� ����
������������ �������� "��������� ���������", ���� xmlMainFile ���� ����� ����������� �� ������� ���� �����
���������� �������� ����������� �������� ����� ��������������.
*/

class CXmlAid_: protected CXmlAid
{
public:
  CXmlAid_();
  ~CXmlAid_();

public:
  void SetBackupFile(CString sPathFile);
  bool CheckXmlFile(CString sPathFile);
  bool LoadTreeFromFile(CString sPathFile);
private:
  void InitNodes();
  CString m_sXmlBackupFile;
  CString m_sXmlUserFile;

  CParamsTreeNode* m_pMain = nullptr;
  CParamsTreeNode* m_pInlays = nullptr;

public:
  void SetProductVersion(CString sVer);
private:
  CString m_sCurrentProductVersion = L"0.0.0.1 (����� ���)"; // set programm by SetProductVersion (data from resource it app)
  CString m_sProductVersionFromXml;
  CString m_sXmlVersionFromXml;
  const CString m_sCurrentXmlVersion = L"0.0.0";
  CParamsTreeNode* m_pVersions = nullptr;

public:
  CParamsTreeNode* GetMainNode();
  CParamsTreeNode* GetInlaysNode();

public:
  void SetUserFile(CString&);

  enum EXmlFile
  {
    xmlBackupFile,
    xmlUserFile
  };

  bool CreateXmlFileFromTree_(EXmlFile xmlFileType);
};
