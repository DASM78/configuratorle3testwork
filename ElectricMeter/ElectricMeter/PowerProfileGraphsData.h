#pragma once

#include "Ai_Str.h"
#include "PowerProfileDataRow.h"
#include <map>
#include <vector>
#include <GdiPlus.h>

enum EPowerProfileType
{
  pptA,
  pptR_plus,
  pptR_minus,
  pptP,
  pptQ_plus,
  pptQ_minus
};

#define CUT_MARK_DAYS_SPACE -1
#define CUT_MARK_HALF_HOUR_SPACE -2

struct CUT_POINT
{
  CUT_POINT()
    : fY(-1) // ������������ ��� �������, ����� ����� ��������� ��������� ����������� ������� �������� (���� ��������)
    , nY(-1)
  {
  }

  CUT_POINT(CString& s)
    : fY(0.0)
    , nY(0)
  {
    SetValue(s);
  }

  CString sDate;
  CString sTime;
  void SetData(CString& d, CString& t, bool bPoints)
  {
    sDate = d;
    sTime = t;
    bManyPoints = bPoints;
  }

  CString sY;
  int nY = 0;
  double fY = 0.0f;
  bool bManyPoints = false;
  
  INT nY_ = 0;
  Gdiplus::REAL fY_ = 0.0f;

  double SetValue(CString& v)
  {
    fY = CAi_Str::ToFloat(v);
    nY = CAi_Str::ToInt(v);
    sY = v;
    return fY;
  }

  size_t nIdxInSouce = 0;
  std::vector<size_t> nIdxesInSouce;
};

class CPowerProfileGraphsData
{
public:
  CPowerProfileGraphsData();
  ~CPowerProfileGraphsData();

  std::map<EPowerProfileType, std::vector<CUT_POINT>>* GetDataPtr();

  void AnalisysData(std::vector<POWPROF_DATA_ROW>* p); // if nullptr - clear all data
  void FormPointsByHeight(int nHeight);
  void GetDataValue(double& fMax, double& fUnitsInPixel);
};

