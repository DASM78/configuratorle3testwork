#include "StdAfx.h"

#include "Thread_61107.h"
#include "Resource.h"
#include "Model_.h"
#include "auxthreads.h"
#include "MessageSet.h"
#include "Ai_BCC.h"
#include "Ai_Time.h"
#include "MessagesIds.h"

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <iterator>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _BCC

using namespace std;
using namespace aux;
using namespace mdl;

CWnd* gpParentForThread = nullptr;

namespace ns_Thread_61107
{
  EXCH_DATA gExchData; // Exch result struct
  mutex gMutex; // Data mutex

  // Thread items

  CWinThread* m_pExchangeThread = nullptr;
  CLinkThread* gpLinkThread = nullptr;
  HANDLE ghExchangerEvent = nullptr;

  // Phases

  ETreadPhase gTreadPhase = tphNone;
  ETreadPhase gNextTreadPhase = tphNone;

  // Reading data

  vector <pair<const unsigned char*, varOnce_t>> gpMonitoringQueue; // varOnce_t - ��� �������� �����������
  CString gsSOH_ID = 0; // ����������� ������� ����������� ����������; ���������� �����. ��� ������.
  // ��������� ��������� �� ���������� ������ �� ����������� - �.�. ��� ����� ������������� �������
  int gnData_ID = 0; // nDataID == nCurrViewID == inlay id
  int gpCurrSOH_Index = 0;

  // Sending data

  SEND_UNIT gpSendUnit;

} // namespace ns_Thread_61107

using namespace ns_Thread_61107;

void TypeTRACEMsg(CString sMsg)
{
  gpLinkThread->TypeTRACEMsg(sMsg);
}

void CreateExchThread(CWnd* pParentForThread)
{
  gpParentForThread = pParentForThread;
  gpLinkThread = new CLinkThread();
  gpLinkThread->CreateExchangerThread();
}

void DestroyExchThread()
{
  gpLinkThread->DestroyExchangerThread();
  delete gpLinkThread;
}

void SetPassword(unsigned char* pPass)
{
  gpLinkThread->SetPassword(pPass);
}

void SetDevAddress(unsigned char* pAddr)
{
  gpLinkThread->SetDevAddress(pAddr);
}

void SetDevName(unsigned char* pName)
{
  gpLinkThread->SetDevName(pName);
}

void SetThreadPhase(ETreadPhase tph)
{
  critical_section cs(gMutex);
  gTreadPhase = tph;
}

ETreadPhase GetThreadPhase()
{
  critical_section cs(gMutex);
  return gTreadPhase;
}

void SetNextThreadPhase(ETreadPhase tph)
{
  critical_section cs(gMutex);
  
  if (tph == tphReset)
    gNextTreadPhase = tphNone;
  else
    gNextTreadPhase = tph;
}

ETreadPhase GetNextThreadPhase()
{
  critical_section cs(gMutex);
  return gNextTreadPhase;
}

void SetEvent_()
{
  critical_section cs(gMutex);
  SetEvent(ghExchangerEvent);
}

// return false - ����� ����� ��������� ��� ����
bool CheckTimePeriodBetweenReq()
{
  critical_section cs(gMutex);
  return gpLinkThread->CheckExchTimePeriod(false_d(L"�� ������ ��������� �������, ������ �������� ������� ����� � ���������� �����������"));
}

void SetWithFirstItemInMonitoringQueue()
{
  critical_section cs(gMutex);
  gpCurrSOH_Index = 0;
}

// nDataID == nCurrViewID == inlay id
void SetToMonitoringQueue(int nDataID, vector<pair<const unsigned char*, varOnce_t>>* pList)
{
  critical_section cs(gMutex);
  gnData_ID = nDataID;
  gpMonitoringQueue.clear();
  if (pList)
    gpMonitoringQueue = *pList;
  gpCurrSOH_Index = 0;
}

unsigned char* GetMonitoringItemFromQueue()
{
  critical_section cs(gMutex);

  if (gpMonitoringQueue.empty())
    return nullptr;

  const unsigned char* p = gpMonitoringQueue[gpCurrSOH_Index].first;
  int nSz = strlen((const char*)p);
  if (nSz == 0)
  {
    ASSERT(false);
    return nullptr;
  }

  unsigned char* p_ = new unsigned char[nSz + 1]{};
  _mbsncat_s(p_, nSz + 1, p, nSz);

  if (gpMonitoringQueue[gpCurrSOH_Index].second)
    gpMonitoringQueue.erase(gpMonitoringQueue.begin() + gpCurrSOH_Index); // ������� ������� �����������
  else
    ++gpCurrSOH_Index;
  if (gpCurrSOH_Index >= (int)gpMonitoringQueue.size())
    gpCurrSOH_Index = 0;

  return p_;
}

void AddToSendingQueue(SEND_UNIT* pSendUnit)
{
  critical_section cs(gMutex);
  gpSendUnit = *pSendUnit;
}

bool GetSendingDataFromQueue(char** pData, int& nSize, teCodingViews& cv)
{
  critical_section cs(gMutex);
  if (gpSendUnit.sendType == FRAME_TYPE_EMPTY)
    return nullptr;

  nSize = gpSendUnit.nDataSize;
  *pData = new char[nSize + 1] {};
  CopyMemory(*pData, gpSendUnit.pszData, nSize);
  cv = gpSendUnit.codingViews;

  delete gpSendUnit.pszData;
  gpSendUnit.pszData = nullptr;
  gpSendUnit.sendType = FRAME_TYPE_EMPTY;

  return true;
}

/////////////////////////////////////////////////////// Thread

UINT ExchangerThread_(LPVOID pParam);

CLinkThread::CLinkThread()
{
  m_logDlr.ClearFile();
  ZeroMemory(m_sPassword, MAX_PATH);
}

CLinkThread::~CLinkThread()
{
}

void CLinkThread::TypeTRACEMsg(CString& sMsg)
{
  m_logDlr.MyTRACE_tm(sMsg);
}

void CLinkThread::CreateExchangerThread()
{
  ghExchangerEvent = CreateEvent(nullptr, true, false, nullptr);
  m_pExchangeThread = AfxBeginThread(ExchangerThread_, nullptr);
  m_pExchangeThread->m_bAutoDelete = false;
}

void CLinkThread::DestroyExchangerThread()
{
  SetThreadPhase(tphStop);
  SetEvent_();
  Sleep(100);
  if (m_pExchangeThread)
  {
    DWORD nSignal = WaitForSingleObject(m_pExchangeThread->m_hThread, 2000);
    if (nSignal == WAIT_OBJECT_0) // ����� �� ������� ������� � �� �� ��������
    {
      m_pExchangeThread->ExitInstance();
      delete m_pExchangeThread;
      m_pExchangeThread = nullptr;
    }
  }
  SetThreadPhase(tphNone);

  if (m_pExchangeThread)
  {
    WaitForSingleObject(m_pExchangeThread->m_hThread, 5000); // ����� ������� ������������� (5000 - ���� ������ ����. �������� �� ����� - �.�. ����� �� GSM ������ ����� 4000)
    m_pExchangeThread->ExitInstance();
    delete m_pExchangeThread;
    m_pExchangeThread = nullptr;
  }
}

UINT ExchangerThread_(LPVOID pParam)
{
  while (true)
  {
    DWORD nSignal = WaitForSingleObject(ghExchangerEvent, 30);

    if (nSignal == WAIT_OBJECT_0) // ����� �� ������� ������� � �� �� ��������
    {
      ResetEvent(ghExchangerEvent);

      bool bResult = false;
      HWND& hWnd = gpParentForThread->m_hWnd;
      ETreadPhase currThreadPhase = GetThreadPhase();
      LPARAM nCurrThdPh = (LPARAM)currThreadPhase;

      gExchData.Clear();
      SetThreadPhase(tphNone);

      function<void(int)> lmbPost = [=](int nAdvMsg) ->void
      {
        EXCH_DATA* pExchData = new EXCH_DATA();
        ASSERT_AND_RETURN(pExchData);
        *pExchData = gExchData;

        MESSAGE_SET* pMsgSet = new MESSAGE_SET(pExchData, 0);
        ASSERT_AND_RETURN(pMsgSet);
        pMsgSet->nData = (int)currThreadPhase;
        ::PostMessage(hWnd, UM_EXCHAGE_RESULT_NOTIFY, nAdvMsg, (LPARAM)pMsgSet);

      };

      switch (currThreadPhase)
      {
        case tphConnect:
          gpLinkThread->Connection();
          lmbPost(UM_CONNECTION_NOTIFY);
          break;

        case tphSendBreakBeforeDisconnection:
          SetToMonitoringQueue(0, nullptr);
          {
            SEND_UNIT s;
            AddToSendingQueue(&s);
          }
          gpLinkThread->SendBreakBeforeDisconnection();
          lmbPost(UM_DISCONNECTION_NOTIFY);
          break;
        case tphDisconnect:
          gpLinkThread->Disconnection();
          lmbPost(UM_DISCONNECTION_NOTIFY);
          break;

        case tphBeginInitialisation:
          gpLinkThread->SendBeginReq();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;
        case tphReceiveIdentificationOfInitialisation:
          gpLinkThread->ReceiveIdentification();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;
        case tphSendConfirmationAboutInitialisation:
          gpLinkThread->SendConfirmation();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;
        case tphReceivePasswordReqiure:
          gpLinkThread->ReceivePasswordReqiure();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;
        case tphSendPassword:
          gpLinkThread->SendPassword();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;
        case tphReceivePasswordResult:
          gpLinkThread->ReceivePasswordResult();
          lmbPost(UM_INITIALISATION_NOTIFY);
          break;

        case tphMonitoringOfData:
          gpLinkThread->SendParamsAddresses();
          lmbPost(UM_MONITORING_NOTIFY);
          break;
        case tphReceiveMonitoringData:
          gpLinkThread->ReceiveData(tphReceiveMonitoringData);
          lmbPost(UM_MONITORING_NOTIFY);
          break;

        case tphRecordingOfData:
          gpLinkThread->SendData();
          lmbPost(UM_RECORDING_NOTIFY);
          break;
        case tphRecordingOfDataConfirmation:
          gpLinkThread->ReceiveData(tphRecordingOfDataConfirmation);
          lmbPost(UM_RECORDING_NOTIFY);
          break;

        case tphStop:
          return 0;
      }
    }
  }

  return 0;
}

void CLinkThread::Connection()
{
  bool bConnection = true;

  switch (m_usingPort)
  {
    case eRS_COM:
      bConnection = m_COMPort.Connect();
      break;
  }

  FormResultAfterExch(bConnection, false_d(L"confirmation"), L"",
                      bConnection ? L"OK" : L"������ ����������� � ��������!", L" (CLinkThread::Connection())");
}

void CLinkThread::Disconnection()
{
  bool bDisconnection = true;

  switch (m_usingPort)
  {
    case eRS_COM:
      bDisconnection = m_COMPort.Disconnect();
      break;
  }

  FormResultAfterExch(bDisconnection, false_d(L"confirmation"), L"",
                      bDisconnection ? L"OK" : L"������ ��������� �� ��������!", L" (CLinkThread::Disconnection())");
}

bool CLinkThread::CheckExchTimePeriod(bool bBegin)
{
  /*
  ������ ���, ����� ������������ ��� ����������� ������ (������������ ������ � ������) ���������� �����.
  ����� ��������� ���������� � ����� ��� ������ ��� ������ ������������ ������� ������ ������� � ������� ����������
  ��������� � �����. ���� ��� ������ ��� m_nExchTimePeriod / 2, �� ��������� ��������� ������������ ������� ������.
  ��� ������������� ��� ����, ����� ������ � ��������� ��� � ������. �.�. ��������, ��� ������ �������� ������� � ���������� ����� ����
  ���������� �����������, ��� ���� �������.
  */
  if (bBegin)
  {
    m_tmrDlr.SaveCurrentTime(ID_EXCH_TIME_MARK);
  }
  else
  {
    __int64 nPeriodOfExch_ms = m_tmrDlr.GetTimePeriod(ID_EXCH_TIME_MARK, CAi_Time::tmrGetByMilliseconds);
    if (nPeriodOfExch_ms == -1)
      return true;
    int nHalfPeriod = m_nExchTimePeriod / 2;
    if (nPeriodOfExch_ms < nHalfPeriod)
    {
      //TRACE(L"..........����� �� ������!\n");
      return false;
    }
  }
  return true;
}

void CLinkThread::FormResultAfterExch(
  bool bRes,
  bool bConfirmation,
  CString sCmd,
  CString sMsg,
  CString sDbgMsg,
  int nInnerErr)
{
  critical_section cs(gMutex);

  gExchData.Clear();
  gExchData.bResult = bRes;
  gExchData.sSOHID = gsSOH_ID;
  gExchData.sData = sCmd;
  gExchData.nDataID = gnData_ID; // nDataID == nCurrViewID == inlay id
  gExchData.bConfirmation = bConfirmation;
  gExchData.sMsg = sMsg;
  gExchData.nInnerErrCode = nInnerErr;
  gExchData.nLastError = GetLastError();
}

void CLinkThread::SetPassword(unsigned char* pPass)
{
  ASSERT(pPass);
  ZeroMemory(m_sPassword, MAX_PATH);
  _mbscpy_s(m_sPassword, MAX_PATH, pPass);
}

void CLinkThread::SetDevAddress(unsigned char* pAddr)
{
  ASSERT(pAddr);
  ZeroMemory(m_sAddress, MAX_PATH);
  _mbscpy_s(m_sAddress, MAX_PATH, pAddr);
}

void CLinkThread::SetDevName(unsigned char* pName)
{
  ASSERT(pName);
  m_sDevName = CString(pName);
}

void CLinkThread::SendBeginReq()
{
  m_logDlr.MyTRACE_tm(L"SendBeginReq");

  SetWithFirstItemInMonitoringQueue();

  int nWriteSizeRes = 0;
  int nWriteSize = 0;
  CString sData;

  if (strlen((const char*)m_sAddress) == 0)
  {
    const char szCmdW[6] = {'/', '?', '!', '\r', '\n', '\0'}; // ��������� ������
    unsigned char* pSOH_Req = (unsigned char*)szCmdW;
    nWriteSize = 5;
    nWriteSizeRes = Writer(pSOH_Req, nWriteSize);
    sData = CString(szCmdW);
  }
  else
  {
    const int nDataSize = MAX_PATH + 9;
    unsigned char szCmdW[nDataSize + 1]{};
    unsigned char szBeginStr[3] = {'/', '?', '\0'};
    _mbsncat_s(szCmdW, nDataSize + 1, szBeginStr, 2);
    _mbsncat_s(szCmdW, nDataSize + 1, m_sAddress, _mbslen(m_sAddress));
    unsigned char szEndStr[4] = {'!', '\r', '\n', '\0'}; // BCC ������ � 0 ����
    _mbsncat_s(szCmdW, nDataSize + 1, szEndStr, 3);

    unsigned char* pSOH_Req = (unsigned char*)szCmdW;
    nWriteSize = _mbslen(szCmdW);
    nWriteSizeRes = Writer(pSOH_Req, nWriteSize);

    sData = CString(szCmdW);
  }

  CheckExchTimePeriod();

  bool bWriteRes = (nWriteSizeRes == nWriteSizeRes);
  bool bConfirmation = false;
  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ ������ ������ � �����������!",
                      L" (CLinkThread::SendBeginReq())");
  m_logDlr.MyTRACE_tm(L"SendBeginReq - " + CString(bWriteRes ? L"OK" : L"������ ������ ������ � �����������!"));
}

void CLinkThread::ReceiveIdentification()
{
  m_logDlr.MyTRACE_tm(L"ReceiveIdentification");

  bool bReadRes = true;
  unsigned char szCmdR[25]{};
  unsigned char chEnd = '\n';
  int nRecCount = 0;
  int nInnerErrId = INNER_ID_ERROR_NONE;
  CString sErrNotify = L"������ ������ �������������!";

  nRecCount = Reader(szCmdR, 25, &chEnd);

  if (nRecCount < 8)
  {
    bReadRes = false; // ����� �� ���������� �� ������
    nInnerErrId = INNER_ID_ERROR_IDF_REC;
    sErrNotify += L"\n��������� ������� - ������������ ����������: ����� ���� ����������� ��������� ������ �� ���������.";
  }
  else
  {
    // ������ ����: / XXX Z ������������� CR LF => / A-X Speed# Id \r \n
    /*
    XXX - ������ �� ������������, ���������� ��� ��������� �����/
    Speed#:
    "0" - 300 ���,
    "1" - 600 ���,
    "2" - 1200 ���,
    "3" - 2400 ���,
    "4" - 4800 ���,
    "5" - 9600 ���,
    "6", "7", "8", "9" - ��������������� ��� ������� ����������.
    Id - �������������, ������������ �������������, - �� ����� 16 �������� ��������, ����� "/" � "!".
    */

    CString sDevName(szCmdR + 5);
    if (sDevName.GetLength() >= m_sDevName.GetLength())
    {
      if (m_sDevName != sDevName.Left(m_sDevName.GetLength()))
      {
        bReadRes = false; // ��� ������������� ���������� �� �������� � ����� ���������� �� ������� �����������
        nInnerErrId = INNER_ID_ERROR_DEVICE_NAME;
        sErrNotify += L"\n������� - ������ ���� ��������� (��. ������� \"��������� ����������\" �������� \"��� ��������\").";
      }
    }

    char chSpeed = '0';
    chSpeed = szCmdR[4];

    m_nConfirmSpeed = chSpeed;
    if (m_nConfirmSpeed == '0')
      gRSCOMProp.nBaudRate = 300;
    if (m_nConfirmSpeed == '1')
      gRSCOMProp.nBaudRate = 600;
    if (m_nConfirmSpeed == '2')
      gRSCOMProp.nBaudRate = 1200;
    if (m_nConfirmSpeed == '3')
      gRSCOMProp.nBaudRate = 2400;
    if (m_nConfirmSpeed == '4')
      gRSCOMProp.nBaudRate = 4800;
    if (m_nConfirmSpeed == '5')
      gRSCOMProp.nBaudRate = 9600;
  }

  CheckExchTimePeriod();

  CString sData(szCmdR);
  bool bConfirmation = false;
  FormResultAfterExch(
    bReadRes,
    bConfirmation,
    sData,
    bReadRes ? L"OK" : sErrNotify,
    L" (CLinkThread::ReceiveIdentification()",
    nInnerErrId);

  m_logDlr.MyTRACE_tm(L"ReceiveIdentification - " + CString(bReadRes ? L"OK" : L"������ ������ �������������!"));
}

void CLinkThread::SendConfirmation()
{
  m_logDlr.MyTRACE_tm(L"SendConfirmation");

  const char szCmdW[7] = {'\x6', '0', m_nConfirmSpeed, '1', '\r', '\n', '\0'}; // ACK and boud rate
  int nWriteSize = 6;
  int nWriteSizeRes = 0;
  unsigned char* pSOH_Req = (unsigned char*)szCmdW;
  nWriteSizeRes = Writer(pSOH_Req, nWriteSize);

  CheckExchTimePeriod();

  CString sData(szCmdW);
  bool bWriteRes = (nWriteSizeRes == nWriteSize);
  bool bConfirmation = false;

  if (bWriteRes)
  {
    bool bDisconnect = m_COMPort.Disconnect();
    ASSERT(bDisconnect);

    bool bConnection = m_COMPort.Connect();
    ASSERT(bConnection);
    if (bConnection)
    {
      HWND& hWnd = gpParentForThread->m_hWnd;
      ::PostMessage(hWnd, UM_RECONNECT_WITH_OTHER_SPEED, 0, 0);
      TRACE(L"Reconnect\n");
    }

    bWriteRes = bConnection;
  }

  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ �������� ������������� ����������!",
                      L" (CLinkThread::SendConfirmation()");
  m_logDlr.MyTRACE_tm(L"SendConfirmation - " + CString(bWriteRes ? L"OK" : L"������ �������� ������������� ����������!"));
}

void CLinkThread::ReceivePasswordReqiure()
{
  m_logDlr.MyTRACE_tm(L"ReceivePasswordReqiure");

  bool bReadRes = true;
  bool bBccRec = true;
  unsigned char szCmdR[9]{};
  unsigned char chEnd = '\n';
  int nRecCount = 0;

  nRecCount = Reader(szCmdR, 8, &chEnd);
  unsigned char nBCC = szCmdR[7] & 0x7F;
#ifdef _BCC
  unsigned char nBCC2 = CAi_BCC::CalcBCC(&szCmdR[1], 6) & 0x7F;
  if (nBCC != nBCC2)
    bBccRec = false;
#endif

  if (bBccRec)
  {
    if (nRecCount != 8)
      bReadRes = false; // ����� �� ���������� �� ����������
    else
    {
      // must be: SOH � 0 STX ( ) ��� ���  
      const unsigned char szCmdCmp[9] = {'\x1', 'P', '0', '\x2', '(', ')', '\x3', '0', '\0'};
      szCmdR[7] = '0'; // �������� BCC, ������ ��� ����
      if (_mbscmp(szCmdR, szCmdCmp) != 0)
        bReadRes = false; // ����� �� ���������� �� ����������
    }
  }

  CheckExchTimePeriod();

  CString sData(szCmdR);
  bool bConfirmation = false;
  FormResultAfterExch(bReadRes, bConfirmation, sData,
                      bReadRes ? L"OK" : L"������ ������ ������!",
                      L" (CLinkThread::ReceivePasswordReqiure()");
  m_logDlr.MyTRACE_tm(L"ReceivePasswordReqiure - " + CString(bReadRes ? L"OK" : L"������ ������ ������!"));
}

void CLinkThread::SendPassword()
{
  m_logDlr.MyTRACE_tm(L"SendPassword");

  const int nDataSize = MAX_PATH + 9;
  unsigned char szCmdW[nDataSize + 1]{};
  unsigned char szBeginStr[6] = {'\x1', 'P', '1', '\x2', '(', '\0'};
  _mbsncat_s(szCmdW, nDataSize + 1, szBeginStr, 5);
  _mbsncat_s(szCmdW, nDataSize + 1, m_sPassword, _mbslen(m_sPassword));
  unsigned char szEndStr[4] = {')', '\x3', '0', '\0'}; // BCC ������ � 0 ����
  _mbsncat_s(szCmdW, nDataSize + 1, szEndStr, 3);
  int nWriteSize = _mbslen(szCmdW);

#ifdef _BCC
  szCmdW[nWriteSize - 1] = CAi_BCC::CalcBCC(&szCmdW[1], nWriteSize - 2) & 0x7F; // -2: minus SOH and BCC position
#endif

  int nWriteSizeRes = 0;
  unsigned char* pSOH_Req = (unsigned char*)szCmdW;
  nWriteSizeRes = Writer(pSOH_Req, nWriteSize);

  CheckExchTimePeriod();

  CString sData(szCmdW);
  bool bWriteRes = (nWriteSizeRes == nWriteSize);
  bool bConfirmation = false;

  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ �������� ������!",
                      L" (CLinkThread::SendPassword()");
  m_logDlr.MyTRACE_tm(L"SendPassword - " + CString(bWriteRes ? L"OK" : L"������ �������� ������!"));
}

void CLinkThread::ReceivePasswordResult()
{
  m_logDlr.MyTRACE_tm(L"ReceivePasswordResult");

  bool bReadRes = true;
  unsigned char szCmdR[25]{};
  unsigned char chEnd = '\n';
  int nRecCount = 0;

  nRecCount = Reader(szCmdR, 25, &chEnd);

  if (nRecCount < 1)
    bReadRes = false; // ����� �� ���������� �� ������
  else
  {
    // must be ACK: \x6
    const unsigned char szCmdCmp[2] = {'\x6', '\0'};
    if (_mbscmp(szCmdR, szCmdCmp) != 0)
      bReadRes = false; // ����� �� ���������� �� ����������
  }

  CheckExchTimePeriod();

  CString sData(szCmdR);
  bool bConfirmation = false;
  FormResultAfterExch(bReadRes, bConfirmation, sData,
                      bReadRes ? L"OK" : L"������ ������ ������������� ������!",
                      L" (CLinkThread::ReceivePasswordResult()");
  m_logDlr.MyTRACE_tm(L"ReceivePasswordResult - " + CString(bReadRes ? L"OK" : L"������ ������ ������������� ������!"));
}

void CLinkThread::SendParamsAddresses()
{
  gsSOH_ID = L"";
  const unsigned char* pSOH_Item = GetMonitoringItemFromQueue();
  if (pSOH_Item == nullptr)
    return;
  const unsigned char s1[5] = {'\x1', 'R', '1', '\x2', '\0'}; // ����� - <SOH> <R1> <STX> ������������ ����
  int nS1Size = strlen((const char*)s1);
  const unsigned char s2[3] = {'(', ')', '\0'};
  int nS2Size = strlen((const char*)s2);
  const unsigned char s3[3] = {'\x3', '\xFF', '\0'}; // ����� - <ETX> <BCC> ������������ ����; BCC - ���� FF
  int nS3Size = strlen((const char*)s3);
  int nSOH_ItemSize = strlen((const char*)pSOH_Item);
  gsSOH_ID = (const char*)pSOH_Item;
  int nSizeForSend = nS1Size + nSOH_ItemSize + nS2Size + nS3Size;
  unsigned char* pSOH_Req = new unsigned char[nSizeForSend + 1]{};
  unsigned char* pSOH_ReqTmp = pSOH_Req;
  _mbsncat_s(pSOH_ReqTmp, nS1Size + 1, s1, nS1Size);
  pSOH_ReqTmp += nS1Size;
  _mbsncat_s(pSOH_ReqTmp, nSOH_ItemSize + 1, pSOH_Item, nSOH_ItemSize);
  delete pSOH_Item;
  pSOH_ReqTmp += nSOH_ItemSize;
  _mbsncat_s(pSOH_ReqTmp, nS2Size + 1, s2, nS2Size);
  pSOH_ReqTmp += nS2Size;
  _mbsncat_s(pSOH_ReqTmp, nS3Size + 1, s3, nS3Size);
  pSOH_ReqTmp += nS3Size;

#ifdef _BCC
  *(pSOH_Req + (nSizeForSend - 1)) = CAi_BCC::CalcBCC(pSOH_Req + 1, nSizeForSend - 2) & 0x7F; // -2: minus SOH and BCC position
#endif

  int nWriteSizeRes = 0;

  m_logDlr.MyTRACE_tm(L"SendParamsAddresses1 - " + CString(pSOH_Req));

  nWriteSizeRes = Writer(pSOH_Req, nSizeForSend);

  CheckExchTimePeriod();

  bool bWriteRes = (nWriteSizeRes == nSizeForSend);
  CString sData(pSOH_Req);
  delete pSOH_Req;
  bool bConfirmation = false;

  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ �������� ������� � �������� (������� SOH)!",
                      L" (CLinkThread::SendParamsAddresses()");
  m_logDlr.MyTRACE_tm(L"SendParamsAddresses2 - " + CString(bWriteRes ? L"OK" : L"������ �������� ������� � �������� (������� SOH)!"));
}

void CLinkThread::ReceiveData(ETreadPhase tph)
{
  unsigned char szCmdR[2001]{};
  int nBuffOffset = 0;
  BYTE byte[1]{};
  int nContinueCount = 100;

  unsigned char chEnd = '\x3';
  nBuffOffset = Reader(szCmdR, 2000, nullptr, &chEnd);

  bool bReadRes = false;
  CString sTraceMsg;
  CString sData;
  bool bIgnoreAnalysis = false;

  if (!bIgnoreAnalysis)
  {
    switch (nBuffOffset)
    {
      case 0: sTraceMsg = L" (������ � ������ ���)"; break;

      case 1: // ���� ������ � ������ ����� ���� ������ � ������ ���� �� NAK
        if (szCmdR[0] == '\xF') // NAK
        {
          sData = CString(szCmdR);
          if (tph == tphReceiveMonitoringData) // ��� ����������� ��� ��������, � ������� �� �������� ������
            // ������ � �������, �������������� ����� � ���� ��� ��� ���� �������� ������������ true
            bReadRes = true;
        }
        break;

      default:
        if (nBuffOffset > 8) // ����������� ���������� �����: [STX] [addr1][addr2][addr3][addr4] [(][)] [ETX] [BCC]
        {
          int nPos_STX = 0;
          int nPos_OpenedRoundBracket = 5;
          int nPos_ClosedRoundBracket = nBuffOffset - 3;
          int nPos_ETX = nBuffOffset - 2;
          int nPos_BCC = nBuffOffset - 1;
          if (szCmdR[nPos_STX] != '\x2') // STX
            break;
          if (szCmdR[nPos_OpenedRoundBracket] != '(')
            break;
          if (szCmdR[nPos_ClosedRoundBracket] != ')')
            break;
          if (szCmdR[nPos_ETX] != '\x3') // ETX
            break;

#ifdef _BCC
          unsigned char nBCC = szCmdR[nPos_BCC] & 0x7F;
          unsigned char nBCC2 = CAi_BCC::CalcBCC(szCmdR + 1, nBuffOffset - 2) & 0x7F;
          if (nBCC != nBCC2)
          {
            TRACE(L"---------------> BCC INCORRECT from - %2X  my - %2X\r\n", nBCC, nBCC2);
            break;
          }
#endif

          szCmdR[nPos_ETX] = 0;
          unsigned char* pData = &szCmdR[nPos_STX + 1];
          sData = CString(pData);
          bReadRes = true;
        }
    }
  }

  bool bConfirmation = (tph == tphRecordingOfDataConfirmation) ? true : false;
  FormResultAfterExch(bReadRes, bConfirmation, sData,
                      bReadRes ? L"OK" : L"������ ������ �������� ����������� ����������!",
                      L" (CLinkThread::ReceiveData()");
  m_logDlr.MyTRACE_tm(L"ReceiveData "
                      + CString((tph == tphReceiveMonitoringData) ? L"(monitoring) - " : L"(programming) - ")
                      + CString(bReadRes ? L"OK" : L"������ ������ �������� ����������� ����������!")
                      + sTraceMsg
                      + L" [Data:" + sData + L"]");
}

void CLinkThread::SendData()
{
  gsSOH_ID = L"";
  char* pSendData = nullptr;
  int nDataSize = 0;
  teCodingViews cv = cv_E2;
  if (!GetSendingDataFromQueue(&pSendData, nDataSize, cv))
    return;

  CString sSOH(pSendData);
  vector<CString> cuts;
  if (CAi_Str::CutString(sSOH, L"(", &cuts)
      && cuts.size() > 1)
      gsSOH_ID = cuts[0];

  const unsigned char s1_E2[5] = {'\x01', 'E', '2', '\x02', '\0'}; // ����� - <SOH> <E2> <STX> ������������ ����
  const unsigned char s1_W1[5] = {'\x01', 'W', '1', '\x02', '\0'}; // ����� - <SOH> <W1> <STX> ������������ ����
  const unsigned char* s1 = (cv == cv_E2) ? s1_E2 : s1_W1;
  int nS1Size = strlen((const char*)s1);
  const unsigned char s2[3] = {'\x03', '\xFF', '\0'}; // ����� - <ETX> <BCC> ������������ ����; BCC - ���� FF
  int nS2Size = strlen((const char*)s2);
  int nSizeForSend = nS1Size + nDataSize + nS2Size;
  unsigned char* pSOH_Req = new unsigned char[nSizeForSend + 1]{};
  unsigned char* pSOH_ReqTmp = pSOH_Req;
  CopyMemory(pSOH_ReqTmp, s1, nS1Size);
  pSOH_ReqTmp += nS1Size;
  CopyMemory(pSOH_ReqTmp, pSendData, nDataSize);
  pSOH_ReqTmp += nDataSize;
  CopyMemory(pSOH_ReqTmp, s2, nS2Size);

#ifdef _BCC
  *(pSOH_Req + (nSizeForSend - 1)) = CAi_BCC::CalcBCC(pSOH_Req + 1, nSizeForSend - 2) & 0x7F; // -2: minus SOH and BCC position
#endif

  int nWriteSizeRes = 0;

  m_logDlr.MyTRACE_tm(L"SendData1 - " + CString(pSOH_Req));

  nWriteSizeRes = Writer(pSOH_Req, nSizeForSend);

  CheckExchTimePeriod();

  bool bWriteRes = (nWriteSizeRes == nSizeForSend);
  CString sData(pSOH_Req);
  delete pSOH_Req;
  bool bConfirmation = false;

  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ �������� ������ (������� SOH)!",
                      L" (CLinkThread::SendData()");
  m_logDlr.MyTRACE_tm(L"SendData2 - " + CString(bWriteRes ? L"OK" : L"������ �������� ������� � �������� (������� SOH)!"));
}

void CLinkThread::SendBreakBeforeDisconnection()
{
  m_logDlr.MyTRACE_tm(L"SendBreakBeforeDisconnection");

  unsigned char szCmdW[6] = {'\x1', 'B', '0', '\x3', '0', '\0'}; // SOH B 0 ��� ���
  int nWriteSize = 5;

#ifdef _BCC
  szCmdW[nWriteSize - 1] = CAi_BCC::CalcBCC(&szCmdW[1], nWriteSize - 2) & 0x7F; // -2: minus SOH and BCC position
#endif

  int nWriteSizeRes = 0;
  unsigned char* pSOH_Req = (unsigned char*)szCmdW;
  nWriteSizeRes = Writer(pSOH_Req, nWriteSize);

  CheckExchTimePeriod();

  CString sData(szCmdW);
  bool bWriteRes = (nWriteSizeRes == nWriteSize);
  bool bConfirmation = false;

  FormResultAfterExch(bWriteRes, bConfirmation, sData,
                      bWriteRes ? L"OK" : L"������ �������� �������������� ������� ����������!",
                      L" (CLinkThread::SendBreakBeforeDisconnection()");
  m_logDlr.MyTRACE_tm(L"SendBreakBeforeDisconnection - " + CString(bWriteRes ? L"OK" : L"������ �������� �������������� ������� ����������!"));
}

int CLinkThread::Reader(unsigned char* pInBuff, int nSizeInBuff,
                        unsigned char* pchEnd_1,
                        unsigned char* pchEnd_2)
{
  int nContinueCount = 100;
  int nRecCount = 0;
  int nRecCountTotal = 0;
  int nSizeInBuff_tail = nSizeInBuff;

  //for (int times = 0; times < 3; ++times)
  {
    /*if (times)
    {
      m_logDlr.MyTRACE_tm(L"<�������� ������ ������ ����>");
    }*/

    nRecCount = m_COMPort.Read((pInBuff + nRecCountTotal), nSizeInBuff_tail);
    nSizeInBuff_tail -= nRecCount;
    nRecCountTotal += nRecCount;

    //if (nRecCountTotal >= nSizeInBuff)
    //  break;

    /*if (pchEnd_1)
    {
      if (nRecCountTotal > 1
          && *(pInBuff + nRecCountTotal - 1) == *pchEnd_1)
        break;
    }
    if (pchEnd_2)
    {
      if (nRecCountTotal > 2
          && *(pInBuff + nRecCountTotal - 2) == *pchEnd_2)
          break;
    }*/

    //if (nRecCount == 0)
    //  Sleep(gRSCOMProp.nReadIntervalTimeout);
  }

  if (nRecCountTotal > 0
      && nRecCountTotal <= nSizeInBuff)
  {
    HWND& hWnd = gpParentForThread->m_hWnd;
    unsigned char* pData = new unsigned char[nRecCountTotal + 1]{};
    CopyMemory(pData, pInBuff, nRecCountTotal);
    ::PostMessage(hWnd, UM_READ_DATA_EVENT, (WPARAM)pData, nRecCountTotal);
    return nRecCountTotal;
  }

  return 0;
}

int CLinkThread::Writer(unsigned char* pOutBuff, int nSizeOutBuff)
{
  int nWroteSize = m_COMPort.Write(pOutBuff, nSizeOutBuff);

  int nNeedFor = gRSCOMProp.nReadIntervalTimeout;
  Sleep(nNeedFor);

  if (nWroteSize)
  {
    HWND& hWnd = gpParentForThread->m_hWnd;
    unsigned char* pData = new unsigned char[nWroteSize + 1]{};
    CopyMemory(pData, pOutBuff, nWroteSize);
    ::PostMessage(hWnd, UM_WRITE_DATA_EVENT, (WPARAM)pData, nWroteSize);
  }

  return nWroteSize;
}
