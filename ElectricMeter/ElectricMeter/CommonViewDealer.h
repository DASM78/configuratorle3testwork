#pragma once

#include "DataStruct.h"
#include "Ai_ColorStatic.h"
#include "Ai_ListCtrlEx.h"
#include "Typedefs.h"
#include "XmlAid.h"
#include "Model_.h"
#include <vector>

class CCommonViewDealer
{
public:
  CCommonViewDealer();
  ~CCommonViewDealer();

  void BeforeDestroy();
  virtual void BeforeDestroyClass() = 0;

  // Timer

  virtual void OnTimer250() {} // 250 ms 
  
  // For PostMessage/SendMessage

public:
  void SetGlobalParent(HWND m_hWnd) { m_hWndGlobParent = m_hWnd; }
  HWND GetGlobalParent() { return m_hWndGlobParent; }
private:
  HWND m_hWndGlobParent = nullptr;

public:
  void SetWaitCursor_answer(bool bSet); // ��� ������, �� ������� ������� �������� �� ��� NAK
  void SetWaitCursor_oneWay(bool bSet); // ��� ������ ��� ������, �������� ������������� �������

  // XML tree node for this CView

public:
  void SetXmlNode(CParamsTreeNode* pXmlNode) { m_pXmlNode = pXmlNode; }
  CParamsTreeNode* GetXmlNode() { return m_pXmlNode; }
  void UpdateXmlBackupFile();
private:
  CParamsTreeNode* m_pXmlNode = nullptr;

  // For CListCtrl on view inlay

public:
  void Set�Ai_ListCtrlEx(�Ai_ListCtrlEx *pCtrl) { m_pCtrlEx = pCtrl; }
private:
  �Ai_ListCtrlEx* m_pCtrlEx = nullptr;

public:
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) { return false; /* do not run �Ai_ListCtrlEx OnNMCustomdrawLst */ }
  virtual bool UseAlternativeBgColor(CListCtrl* pCtrl, COLORREF& cNewCololr) { return false; }
  virtual void OnNMClickLst(CListCtrl* pCtrl, int nRow_inView, int nCell) {}
  virtual void OnNMDblclkLst(CListCtrl* pCtrl, int nRow_inView, int nCell) {}
  virtual void OnLvnItemchangedLst(CListCtrl* pCtrl, int nRow_inView, bool bSelected) {}

  // Connection state

public:
  bool IsConectionAdminAccess();
  CString GetAdminPassword();
  CString GetUserPassword();
  void SetAccessInfo(bool bAdminAccess, CString sAdminPass, CString sUserPass);
  void SaveConnectionAccess(bool bAdminAccess, CString sAdminPass, CString sUserPass);

  void SetConnectionState(bool bState);
  void UpdateViewByConnectionState();
  bool IsConnection();
  virtual void OnConnectionStateWasChanged() {}

private:
  bool m_bConnectionState = false;
  bool m_bConnectionAdminAccess = false;
  CString m_sAdminPassword;
  CString m_sUserPassword;

public:
  enum UserRights { rightCommon, rightAdmin };
  void WatchForCtrState(CWnd* pW, UserRights ur); // ����� ��� ����� � ����������� - ctrl �� ��������
  void WatchForCtrState2(CWnd* pW); // ����� ��� ����� � ����������� - ctrl ��������
private:
  std::vector<std::pair<CWnd*, UserRights>> m_watchForCtrState;
  std::vector<CWnd*> m_watchForCtrState2;
  void WatchForCtrState();

  // Device data

    // ...monitoring

public:
  void BeginFormingReadPack(bool bStart);
  void SetParamTypeForRead(mdl::teFrameTypes ft, varOnce_t bOnce = false); // ������������ �� �������
  void SetParamTypeForRead(unsigned int nAddress, varOnce_t bOnce = false); // ������������ �� �������

  void GetDataForRead(dataList_t* pData, varOnce_t* pbIsDataListNew); // ������������� ��� ������� �� ��������
  void SetDataForReadOld();
  void ClearOnceReadingReq();
  void ClearAllReadingReq();
protected:
  dataList_t m_dataList; // �������� ������, ������� ���� ������ �� �������� ��� ������ �������
  bool m_bNewDataList = false;
  bool m_bReadPackIsForming = false;

    // ... send data to device

public:
  void GetDataForSend(std::vector<SEND_UNIT>* pList);

  void BeginFormingSendPack(bool bStart);
  void SendDataToDevice(mdl::teFrameTypes sendType);
  void SendDataToDevice(mdl::teFrameTypes sendType, int nValue);
  void SendDataToDevice(mdl::teFrameTypes sendType, CString sValue);
protected:
  void SetParamTypeForSend(SEND_UNIT* pSU);
  std::vector<SEND_UNIT> m_sendDataList;
  bool m_bSendPackIsForming = false;

public:
  void UpdateMonitoringCtrlView(CAi_ColorStatic* pCtrl);

  // Data store pointer

public:
  void SetPointerToDataFinder(bool(*pFn)(const unsigned int nInAddress, CString& sOutData));

protected:
  bool(*pfnDataStoreFinder)(const unsigned int nInAddress, CString& sOutData);

public:
  void SetParamTypeForCell(int nRow_inList, int nCell, mdl::teFrameTypes ft);

  virtual void UpdateParamMeaningsByReceivedDeviceData(unsigned int nAddress);
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) {}
  void UpdateParamMeaningByReceivedDeviceData(int nRow_inList, int nCell);
  varMeaning_t FindReceivedData(mdl::teFrameTypes ft);
  varMeaning_t FindReceivedData(unsigned int nAddress);
  
  // ����� ������������ ������ �� ������� ��������� ��������� �������� ����� ������ � ��. ����� ������ ��
  // �������������� ��� �������� (���� � �������� ����� ������ �� �������� ��� ������� ����� ������, ��� �������
  // � �� ������ �������).
  // ��������� � �� �������������� ����� ���������. ����� ������� �� ���������� ����������� �-��� AfterClearReceivedData.
  void FormListForClearReceivedData(mdl::teFrameTypes ft);
  void AfterClearReceivedData();
private:
  std::vector<mdl::teFrameTypes> m_clearedList;
  void ClearListForClearReceivedData();
public:
  void ClearReceivedData(bool bComeback = true);
  virtual void DoAfterClearLoadData() {}

public:
  virtual void UpdateInlayByReceivedConfirmation(unsigned int nAddress, bool bResult);
  virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) {}

  void ErrorMsgWasShowed(bool bWas) { m_bErrorMsgWasShowed = bWas; }
  bool WasErrorMsgBeShowed() { return m_bErrorMsgWasShowed; }
private:
  bool m_bErrorMsgWasShowed = false;
};

