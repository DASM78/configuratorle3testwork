#pragma once

#include "afxwin.h"
#include "Resource.h"
#include "XmlAid.h"
#include "Ai_EditCtrlCharFilter.h"

class CMFCButton2_: public CMFCButton
{
public:
  void SetDelayFullTextTooltipSet(bool DelayFullTextTooltipSet)
  {
    m_bDelayFullTextTooltipSet = DelayFullTextTooltipSet;
  }
};

class CConnection_Task_InterfaceChoice_RS : public CDialogEx
{
  DECLARE_DYNAMIC(CConnection_Task_InterfaceChoice_RS)

public:
  CConnection_Task_InterfaceChoice_RS();   // standard constructor
  virtual ~CConnection_Task_InterfaceChoice_RS();

// Dialog Data
  enum { IDD = IDD_CONNECTION_TASK_INTERFACE_CHOICE_RS };

  DECLARE_MESSAGE_MAP()

public:
  void SetGlobalParent(HWND m_hWnd) { m_hWndGlobParent = m_hWnd; }
private:
  HWND m_hWndGlobParent = nullptr;

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  //{{AFX_MSG(CMyDialog)
  virtual BOOL OnInitDialog();
  virtual void OnOK() { }
  virtual void OnCancel() { }
  afx_msg void OnClose() { }
  //}}AFX_MSG

  BOOL UsingSetupAPI2(CListBox *pList);
  void SelectPrevInterface();

public:
  void ConnectionStateWasChanged(bool bConnection);

private:
  CWnd* m_pParent = nullptr;

  CComboBox m_cbxProtocol;

  CAi_EditCtrlCharFilter m_edtDeviceAddress;
public:
  void DeviceAddressWasChanged();

private:
  CListBox m_lstRS_Port;
  CComboBox m_cbxRS_Speed;
  CComboBox m_cbxRS_Parity;
  CComboBox m_cbxRS_BitCount;
  CComboBox m_cbxRS_BytesInterval;
  CMFCButton2_ m_btnSpeedInfo;

  CMFCButton m_btnRefresh;
  afx_msg void OnBnClickedBtnPortRefresh();

public:
  void FillCtrlValue();
  void CheckCtrEnable();
private:
  bool m_bCtrlsDataIsFilling = false;
  void CtrlsDataWasChanged();

public:
  void SetXmlNodePointer(CParamsTreeNode* pNode) { m_pXmlNode = pNode; }
  void OnJustAfterShow();

private:
  CParamsTreeNode* m_pXmlNode = nullptr;

  void LoadDataFromXml();

  bool m_bCanSave = false;
  void PutValueToXmlTree();

  afx_msg void OnCbnSelchangeCbxRsProtocol();
  afx_msg void OnEnChangeEdtRsDeviceAddress();
  afx_msg void OnCbnSelchangeCbxRsSpeed();
  afx_msg void OnCbnSelchangeCbxRsParity();
  afx_msg void OnCbnSelchangeCbxRsBitCount();
  afx_msg void OnCbnSelchangeCbxRsIntervalTimeout();
  afx_msg void OnLbnSelchangeLstRsPort();
};
