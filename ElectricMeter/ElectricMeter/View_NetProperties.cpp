#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "View_NetProperties.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_NetProperties, CCommonViewForm)

namespace ns_NetProp
{

  struct COLUMNS
  {
    int nParamName = -1;
    int nPhaza1 = -1;
    int nPhaza2 = -1;
    int nPhaza3 = -1;
    int nSum = -1;
  }
  g_cols;
  struct ROWS
  {
    int nPowerP = -1;
    int nPowerQ = -1;
    int nPowerS = -1;
    int nPowerCoeff = -1;
    int nVoltageU = -1;
    int nCurrentI = -1;
    int nPhreqF = -1;
  }
  g_rows;

} // namespace ns_NetProp
using namespace ns_NetProp;

CView_NetProperties::CView_NetProperties()
  : CCommonViewForm(CView_NetProperties::IDD, L"CView_NetProperties")
  , m_lstNetProp(&m_plstNetPropEx, dynamic_cast<CCommonViewDealer*>(this))
{
}

CView_NetProperties::~CView_NetProperties()
{
  if (m_plstNetPropEx)
  {
    m_plstNetPropEx->Destroy();
    delete m_plstNetPropEx;
  }
}

BEGIN_MESSAGE_MAP(CView_NetProperties, CCommonViewForm)
	
END_MESSAGE_MAP ()

void CView_NetProperties::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_LST_NET_PROP, m_lstNetProp);
  DDX_Control(pDX, IDC_STC_DIAGRAMS, m_stcDiagrams);
}

void CView_NetProperties::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  m_lstNetProp.ShowHorizScroll(false);
  m_lstNetProp.EnableInterlacedColorScheme(true);

  m_plstNetPropEx = new �Ai_ListCtrlEx();
  m_plstNetPropEx->SetList(&m_lstNetProp, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstNetPropEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  Set�Ai_ListCtrlEx(m_plstNetPropEx);
  
  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstNetProp.SetExtendedStyle(m_lstNetProp.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_plstNetPropEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  ns_NetProp::g_cols.nParamName = m_plstNetPropEx->SetColumnEx(L"��������", LVCFMT_LEFT);
  m_plstNetPropEx->SetColumnWidthStyle(ns_NetProp::g_cols.nParamName, CS_WIDTH_FIX, 150);
  ns_NetProp::g_cols.nPhaza1 = m_plstNetPropEx->SetColumnEx(L"����-1", LVCFMT_LEFT);
  m_plstNetPropEx->SetColumnWidthStyle(ns_NetProp::g_cols.nPhaza1, CS_WIDTH_FIX, 100);
  ns_NetProp::g_cols.nPhaza2 = m_plstNetPropEx->SetColumnEx(L"����-2", LVCFMT_LEFT);
  m_plstNetPropEx->SetColumnWidthStyle(ns_NetProp::g_cols.nPhaza2, CS_WIDTH_FIX, 100);
  ns_NetProp::g_cols.nPhaza3 = m_plstNetPropEx->SetColumnEx(L"����-3", LVCFMT_LEFT);
  m_plstNetPropEx->SetColumnWidthStyle(ns_NetProp::g_cols.nPhaza3, CS_WIDTH_FIX, 100);
  ns_NetProp::g_cols.nSum = m_plstNetPropEx->SetColumnEx(L"�����", LVCFMT_LEFT);
  m_plstNetPropEx->SetColumnWidthStyle(ns_NetProp::g_cols.nSum, CS_WIDTH_FIX, 100);
  m_plstNetPropEx->MatchColumns();

  int nNodeIdx = m_plstNetPropEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  vector<CString> textOfCells(1);

  textOfCells[0] = L"�������� �(���)";
  ns_NetProp::g_rows.nPowerP = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_POWER_P_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_POWER_P_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_POWER_P_T);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nSum, FRAME_TYPE_POWER_P_SUM);

  textOfCells[0] = L"�������� Q(����)";
  ns_NetProp::g_rows.nPowerQ = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_POWER_Q_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_POWER_Q_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_POWER_Q_T);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nSum, FRAME_TYPE_POWER_Q_SUM);

  textOfCells[0] = L"�������� S(���)";
  ns_NetProp::g_rows.nPowerS = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_POWER_S_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_POWER_S_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_POWER_S_T);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nSum, FRAME_TYPE_POWER_S_SUM);

  textOfCells[0] = L"����������� ��������";
  ns_NetProp::g_rows.nPowerCoeff = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_COSFI_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_COSFI_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_COSFI_T);
  SetParamTypeForCell(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nSum, FRAME_TYPE_COSFI_SUM);

  textOfCells[0] = L"���������� U(�)";
  ns_NetProp::g_rows.nVoltageU = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_VOLTAGE_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_VOLTAGE_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_VOLTAGE_T);
  m_plstNetPropEx->SetCellFlag(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nSum, FRAME_TYPE_EMPTY);
  m_plstNetPropEx->SetCellDisable(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nSum);

  textOfCells[0] = L"��� I(A)";
  ns_NetProp::g_rows.nCurrentI = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  SetParamTypeForCell(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_CURRENT_R);
  SetParamTypeForCell(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_CURRENT_S);
  SetParamTypeForCell(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_CURRENT_T);
  m_plstNetPropEx->SetCellFlag(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nSum, FRAME_TYPE_EMPTY);
  m_plstNetPropEx->SetCellDisable(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nSum);

  textOfCells[0] = L"������� F(��)";
  ns_NetProp::g_rows.nPhreqF = m_plstNetPropEx->SetChild(nNodeIdx, &textOfCells);
  m_plstNetPropEx->SetCellFlag(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza1, FRAME_TYPE_EMPTY);
  m_plstNetPropEx->SetCellDisable(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza1);
  m_plstNetPropEx->SetCellFlag(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza2, FRAME_TYPE_EMPTY);
  m_plstNetPropEx->SetCellDisable(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza2);
  m_plstNetPropEx->SetCellFlag(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza3, FRAME_TYPE_EMPTY);
  m_plstNetPropEx->SetCellDisable(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nPhaza3);
  SetParamTypeForCell(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nSum, FRAME_TYPE_FREQUENCY);

  m_lstNetProp.SetSpecColorColumn(ns_NetProp::g_cols.nParamName);
  m_plstNetPropEx->UpdateView();
  m_lstNetProp.CtrlWasCreated();

  //////////////////////////////////////

  m_stcDiagrams.SetStartRGB(210, 220, 200);
  m_stcDiagrams.StopIfEqual(235, 235, 235);
  m_stcDiagrams.WorkWithColor(true, true, true);
  m_stcDiagrams.SetGradient(0.5);
  m_stcDiagrams.SetStep(2);
  m_stcDiagrams.HorizontalOrVertical(CAi_Static::hvVertical);

  ///////////////////////////////////////

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_NetProperties::BeforeDestroyClass()
{
}

void CView_NetProperties::OnSize_()
{
  if (!m_lstNetProp.m_hWnd)
    return;

  CRect rect{};
  GetClientRect(&rect);

  SpreadCtrlToRightSide(&m_lstNetProp, 10);
  SpreadCtrlToRightSide(&m_stcDiagrams, 100);
}

void CView_NetProperties::OnDraw(CDC* pDC)
{
}

bool CView_NetProperties::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_NetProperties::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_NetProperties::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_NetProperties::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_NetProperties::CheckMeaningsViewByConnectionStatus()
{
  vector<int> inactiveTextInColumns;
  if (!IsConnection())
  {
    inactiveTextInColumns.push_back(ns_NetProp::g_cols.nPhaza1);
    inactiveTextInColumns.push_back(ns_NetProp::g_cols.nPhaza2);
    inactiveTextInColumns.push_back(ns_NetProp::g_cols.nPhaza3);
    inactiveTextInColumns.push_back(ns_NetProp::g_cols.nSum);
  }
  m_lstNetProp.SetInactiveTextInColumns(&inactiveTextInColumns);
  m_plstNetPropEx->UpdateView();
}

void CView_NetProperties::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� ���������� ����!", MB_ICONERROR);
    return;
  }

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nPhaza3);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerP, ns_NetProp::g_cols.nSum);

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nPhaza3);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerQ, ns_NetProp::g_cols.nSum);

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nPhaza3);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerS, ns_NetProp::g_cols.nSum);

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nPhaza3);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPowerCoeff, ns_NetProp::g_cols.nSum);
  
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nVoltageU, ns_NetProp::g_cols.nPhaza3);

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza1);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza2);
  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nCurrentI, ns_NetProp::g_cols.nPhaza3);

  UpdateParamMeaningByReceivedDeviceData(ns_NetProp::g_rows.nPhreqF, ns_NetProp::g_cols.nSum);

  m_plstNetPropEx->UpdateView();
}

