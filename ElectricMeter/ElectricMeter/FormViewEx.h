#pragma once

#include "Resource.h"
#include "CommonListCtrl.h"
#include "CommonViewForm.h"
#include "CommonViewDealer.h"

class CFormViewEx: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CFormViewEx)

protected:
  CFormViewEx();           // protected constructor used by dynamic creation
  CFormViewEx(�Ai_ListCtrlEx** pplst);
  virtual ~CFormViewEx();

public:
  enum
  {
    IDD = IDD_MFCLISTVIEW
  };
#ifdef _DEBUG
  virtual void AssertValid() const;
#ifndef _WIN32_WCE
  virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
public:
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnDestroy();
  virtual void OnSize_();
  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override;
  virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
  CMFCListCtrl& GetListCtrl()
  {
    return m_mfcListCtrl;
  }

protected:
  CCommonListCtrl m_mfcListCtrl;
  bool m_bMfcListCtrlActual = true;

  DECLARE_MESSAGE_MAP()
};

