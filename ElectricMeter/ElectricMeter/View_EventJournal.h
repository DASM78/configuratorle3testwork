#pragma once

#include "CommonViewForm.h"
#include "Ai_ColorStatic.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "CommonListCtrl.h"
#include "XmlAid.h"
#include "CommonViewDealer.h"
#include "Model_.h"

class CView_EventJournal: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_EventJournal)

public:
  CView_EventJournal();
  virtual ~CView_EventJournal();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_DATA_EVENT_JOURNAL
  };

  virtual void DoDataExchange(CDataExchange* pDX);
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  // Filter

  CAi_ColorStatic m_stcEvJrFilter;
  CCommonListCtrl m_lstEvJrFilter;
  �Ai_ListCtrlEx* m_plstEvJrFilterEx = nullptr;

  void GetCutOfMeanings();
  virtual void OnLvnItemchangedLst(CListCtrl* pCtrl, int nRow_inView, bool bSelected) override final;

  // Xml

  CParamsTreeNode* m_pEvJourNode = nullptr;
  void TakeValueFromXmlTree();
  void PutValueToXmlTree();

  // Journal

  CAi_ColorStatic m_stcEvJr;
  CCommonListCtrl m_lstEvJr;
  �Ai_ListCtrlEx* m_plstEvJrEx = nullptr;

  void ClearRows();
  void FillInByFilter();
  void ParseReceivedData(mdl::teFrameTypes ft);
  bool m_bParseResult = true;

  // Load

  CMFCButton m_btnLoadJourFromDevice;
  afx_msg void OnBnClickedBtnLoadJourFromDevice();
  virtual void DoAfterClearLoadData() override final;

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
public:
	
};
