#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "View_SolidTableExample.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_SolidTableExample, CFormViewEx)

namespace ns_SolTbl
{
  struct COLUMNS
  {
    int nParamName = -1;
    int nMeaning = -1;
  }
  g_cols;

  struct ROWS
  {
    int nSerialN = -1;
    int nCreateData = -1;
    int nSW_Version = -1;
  }
  g_rows;

} // namespace ns_SolTbl
using namespace ns_SolTbl;

CView_SolidTableExample::CView_SolidTableExample()
: CFormViewEx(&m_plstInfoEx)
{
}

CView_SolidTableExample::~CView_SolidTableExample()
{
  delete m_plstInfoEx;
}

BEGIN_MESSAGE_MAP(CView_SolidTableExample, CFormViewEx)
  ON_WM_WINDOWPOSCHANGING()
  ON_WM_KEYDOWN()
END_MESSAGE_MAP()

#ifdef _DEBUG
void CView_SolidTableExample::AssertValid() const
{
  CFormViewEx::AssertValid();
}

void CView_SolidTableExample::Dump(CDumpContext& dc) const
{
  CFormViewEx::Dump(dc);
}
#endif //_DEBUG

BOOL CView_SolidTableExample::PreCreateWindow(CREATESTRUCT& cs)
{
  cs.style |= LVS_REPORT;
  return CFormViewEx::PreCreateWindow(cs);
}

void CView_SolidTableExample::OnInitialUpdate()
{
  CFormViewEx::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CListCtrl& wndList = GetListCtrl();

  m_mfcListCtrl.ShowHorizScroll(false);
  m_mfcListCtrl.EnableInterlacedColorScheme(true);

  m_plstInfoEx = new �Ai_ListCtrlEx();
  m_plstInfoEx->SetList(&wndList, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstInfoEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    wndList.SetExtendedStyle(wndList.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_plstInfoEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  ns_SolTbl::g_cols.nParamName = m_plstInfoEx->SetColumnEx(L"��������", LVCFMT_LEFT);
  m_plstInfoEx->SetColumnWidthStyle(ns_SolTbl::g_cols.nParamName, CS_WIDTH_FIX, 250);
  ns_SolTbl::g_cols.nMeaning = m_plstInfoEx->SetColumnEx(L"��������"/* + CString(C_MATCH_WIDTH)*/, LVCFMT_LEFT);
  m_plstInfoEx->SetColumnWidthStyle(ns_SolTbl::g_cols.nMeaning, CS_WIDTH_FIX, 250);
  m_plstInfoEx->MatchColumns();

  int nNodeIdx = m_plstInfoEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  vector<CString> textOfCells(2);

  textOfCells[0] = L"�������� �����";
  textOfCells[1] = L"...";
  ns_SolTbl::g_rows.nSerialN = m_plstInfoEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = L"���� ������������";
  textOfCells[1] = L"...";
  ns_SolTbl::g_rows.nCreateData = m_plstInfoEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = L"������ ��";
  textOfCells[1] = L"...";
  ns_SolTbl::g_rows.nCreateData = m_plstInfoEx->SetChild(nNodeIdx, &textOfCells);

  m_mfcListCtrl.SetSpecColorColumn(ns_SolTbl::g_cols.nParamName);
  m_plstInfoEx->UpdateView();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_SolidTableExample::BeforeDestroyClass()
{
}

void CView_SolidTableExample::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos)
{
  CFormViewEx::OnWindowPosChanging(lpwndpos);
}

void CView_SolidTableExample::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  CFormViewEx::OnKeyDown(nChar, nRepCnt, nFlags);
}

bool CView_SolidTableExample::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}