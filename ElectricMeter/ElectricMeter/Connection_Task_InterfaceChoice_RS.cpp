#include "stdafx.h"
#include "Connection_Task_InterfaceChoice_RS.h"
#include "afxdialogex.h"
#include "Ai_CmbBx.h"
#include <vector>
#include "Ai_COMPortList.h"
#include "Ai_Str.h"
#include "RSCOMMng.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;

IMPLEMENT_DYNAMIC(CConnection_Task_InterfaceChoice_RS, CDialogEx)

namespace ns_ConRS
{
  CConnection_Task_InterfaceChoice_RS* gpThis = nullptr;
  void DeviceAddressAfterFilter(CString sText)
  {
    gpThis->DeviceAddressWasChanged();
  }

  struct X_TAGS
  {
    CParamsTreeNode* pRS_SettingsNode = nullptr;
    CString sRS_Settings = L"RS_Settings";
    
    CString sRSProtocol = L"Protocol";
    CString sRSDeviceAddress = L"DeviceAddress";
    CString sRSAttr_PortNumber = L"PortNumber";
    CString sRSAttr_Speed = L"Speed";
    CString sRSAttr_Parity = L"Parity";
    CString sRSAttr_BitCount = L"BitCount";
    CString sRSAttr_IntervalMulty = L"IntervalMulty";
  }
  g_xTags;

} // namespace ns_ConRS

using namespace ns_ConRS;

CConnection_Task_InterfaceChoice_RS::CConnection_Task_InterfaceChoice_RS()
{
}

CConnection_Task_InterfaceChoice_RS::~CConnection_Task_InterfaceChoice_RS()
{
}

void CConnection_Task_InterfaceChoice_RS::DoDataExchange(CDataExchange* pDX)
{
  CDialogEx::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LST_RS_PORT, m_lstRS_Port);
  DDX_Control(pDX, IDC_CBX_RS_SPEED, m_cbxRS_Speed);
  DDX_Control(pDX, IDC_CBX_RS_PARITY, m_cbxRS_Parity);
  DDX_Control(pDX, IDC_CBX_RS_BIT_COUNT, m_cbxRS_BitCount);
  DDX_Control(pDX, IDC_CBX_RS_INTERVAL_TIMEOUT, m_cbxRS_BytesInterval);
  DDX_Control(pDX, IDC_EDT_DEVICE_ADDRESS, m_edtDeviceAddress);
  DDX_Control(pDX, IDC_BTN_PORT_REFRESH, m_btnRefresh);
  DDX_Control(pDX, IDC_CBX_PROTOCOL, m_cbxProtocol);
  DDX_Control(pDX, IDC_BTN_INFO, m_btnSpeedInfo);
}

BEGIN_MESSAGE_MAP(CConnection_Task_InterfaceChoice_RS, CDialogEx)
  ON_CBN_SELCHANGE(IDC_CBX_PROTOCOL, &CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol)
  ON_EN_CHANGE(IDC_EDT_DEVICE_ADDRESS, &CConnection_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress)
  ON_CBN_SELCHANGE(IDC_CBX_RS_SPEED, &CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed)
  ON_CBN_SELCHANGE(IDC_CBX_RS_PARITY, &CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity)
  ON_CBN_SELCHANGE(IDC_CBX_RS_BIT_COUNT, &CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount)
  ON_CBN_SELCHANGE(IDC_CBX_RS_INTERVAL_TIMEOUT, &CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout)
  ON_WM_CLOSE()
  ON_BN_CLICKED(IDC_BTN_PORT_REFRESH, &CConnection_Task_InterfaceChoice_RS::OnBnClickedBtnPortRefresh)
  ON_LBN_SELCHANGE(IDC_LST_RS_PORT, OnLbnSelchangeLstRsPort)
END_MESSAGE_MAP()

BOOL CConnection_Task_InterfaceChoice_RS::OnInitDialog()
{
  CDialogEx::OnInitDialog();
  
  ns_ConRS::gpThis = this;
  CString s;
  s.LoadString(IDS_IGNORED_DEVICE_ADDRESS_CHARS);
  m_edtDeviceAddress.IgnoreSimbols(s);
  m_edtDeviceAddress.SetLimitText(32);
  m_edtDeviceAddress.CtrlTextAfterCheck = ns_ConRS::DeviceAddressAfterFilter;

  //m_edtRS_SysTimeout.SetLimitText(5);
  //m_edtRS_MultySysTimeout.SetLimitText(5);

  m_btnSpeedInfo.SetTooltip(L"�������� ������ ������������ ���������.");
  m_btnSpeedInfo.SetDelayFullTextTooltipSet(FALSE);

  return TRUE;
}

BOOL CConnection_Task_InterfaceChoice_RS::UsingSetupAPI2(CListBox *pList)
{
  vector<CString> coms;
  CAi_COMPortList comPorts;

  if (comPorts.GetCOMPortList(&coms))
  {
  }

  pList->ResetContent();

  if (!coms.empty())
  {
    for (int i = 0; i < (int)coms.size(); ++i)
      pList->AddString(coms[i]);
    return TRUE;
  }
  return FALSE;
}

void CConnection_Task_InterfaceChoice_RS::SelectPrevInterface()
{
  if (!gRSCOMProp.sPortName.IsEmpty())
  {
    int nSel = m_lstRS_Port.SelectString(-1, gRSCOMProp.sPortName);
  }
}

void CConnection_Task_InterfaceChoice_RS::ConnectionStateWasChanged(bool bConnection)
{
  BOOL bEnable = !bConnection;

  CString sValue = CAi_Str::ToString(gRSCOMProp.nBaudRate);
  CAi_CmbBx::SelectItem(&m_cbxRS_Speed, sValue);
  if (!bEnable)
  {
    m_cbxRS_BytesInterval.EnableWindow(FALSE);
  }
  else
    CheckCtrEnable();
}

void CConnection_Task_InterfaceChoice_RS::FillCtrlValue()
{
  m_bCtrlsDataIsFilling = true;
  m_bCanSave = false;

  m_cbxProtocol.SetCurSel(0);

  m_edtDeviceAddress.SetWindowText(gRSCOMProp.sDeviceAddress);

  CString sValue = CAi_Str::ToString(gRSCOMProp.nBaudRate);
  CAi_CmbBx::SelectItem(&m_cbxRS_Speed, sValue);

  switch (gRSCOMProp.nParity)
  {
    case NonParity: sValue = L"���"; break;
    case OddParity: sValue = L"�����"; break;
    case EvenParity: sValue = L"���"; break;
    default: sValue = L"���"; break;
  }
  CAi_CmbBx::SelectItem(&m_cbxRS_Parity, sValue);

  switch (gRSCOMProp.nByteSize)
  {
    case 5/*Data5Bit*/: sValue = L"5"; break;
    case 6/*Data6Bit*/: sValue = L"6"; break;
    case 7/*Data7Bit*/: sValue = L"7"; break;
    case 8/*Data8Bit*/: sValue = L"8"; break;
    default: sValue = L"7"; break;
  }
  CAi_CmbBx::SelectItem(&m_cbxRS_BitCount, sValue);

  sValue = CAi_Str::ToString(gRSCOMProp.nMultyValue);
  CAi_CmbBx::SelectItem(&m_cbxRS_BytesInterval, sValue);
}

void CConnection_Task_InterfaceChoice_RS::CheckCtrEnable()
 {
    int nSel = m_lstRS_Port.GetCurSel();
    BOOL bEnabledCtrls = (nSel >= 0);
    m_cbxRS_BytesInterval.EnableWindow(bEnabledCtrls);
    m_bCanSave = true;
    m_bCtrlsDataIsFilling = false;
}

void CConnection_Task_InterfaceChoice_RS::CtrlsDataWasChanged()
{
  if (m_bCtrlsDataIsFilling)
    return;

  PutValueToXmlTree(); // cut values in tmp string and save them to xml...
  LoadDataFromXml(); // ...take values from xml and put them to gRSCOMProp
  CheckCtrEnable();
}

void CConnection_Task_InterfaceChoice_RS::OnJustAfterShow()
{
  LoadDataFromXml();
  FillCtrlValue();
  UsingSetupAPI2(&m_lstRS_Port);
  SelectPrevInterface();
  CheckCtrEnable();
}

void CConnection_Task_InterfaceChoice_RS::LoadDataFromXml()
{
  m_bCanSave = false;

  // delele old node

  CString sTag = L"RS485_Settings";
  CParamsTreeNode*pTmp = m_pXmlNode->FindFirstNode(sTag);
  if (pTmp)
    m_pXmlNode->DeleteNodes(sTag);

  //

  sTag = g_xTags.sRS_Settings;
  g_xTags.pRS_SettingsNode = m_pXmlNode->FindFirstNode(sTag);
  if (!g_xTags.pRS_SettingsNode)
    g_xTags.pRS_SettingsNode = m_pXmlNode->AddNode(sTag);

  gRSCOMProp.sDeviceAddress = L"";
  CString sXmlValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSDeviceAddress);
  if (!sXmlValue.IsEmpty())
    gRSCOMProp.sDeviceAddress = sXmlValue;

  gRSCOMProp.nPortNumber = 0;
  gRSCOMProp.sPortName = L"";

  sXmlValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSAttr_PortNumber);
  if (!sXmlValue.IsEmpty())
  {
    gRSCOMProp.nPortNumber = CAi_Str::ToInt(sXmlValue.TrimLeft(L"COM"));
    if (gRSCOMProp.nPortNumber > 0)
      gRSCOMProp.sPortName = L"COM" + CAi_Str::ToString(gRSCOMProp.nPortNumber);
  }

  /*gRSCOMProp.nBaudRate = 9600;
  sXmlValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSAttr_Speed);
  if (!sXmlValue.IsEmpty())
    gRSCOMProp.nBaudRate = CAi_Str::ToInt(sXmlValue);*/


  sXmlValue = L"���";
  CString sValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSAttr_Parity);
  if (!sValue.IsEmpty())
    sXmlValue = sValue;

  if (sXmlValue == L"�����")
    gRSCOMProp.nParity = OddParity;
  else
    if (sXmlValue == L"���")
      gRSCOMProp.nParity = EvenParity;
    else
      gRSCOMProp.nParity = NonParity;

  sXmlValue = L"7";
  sValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSAttr_BitCount);
  if (!sValue.IsEmpty())
    sXmlValue = sValue;

  if (sXmlValue == L"5")
    gRSCOMProp.nByteSize = 5;// Data5Bit;
  else
    if (sXmlValue == L"6")
      gRSCOMProp.nByteSize = 6;// Data6Bit;
    else
      if (sXmlValue == L"8")
        gRSCOMProp.nByteSize = 8;// Data8Bit;
      else
        gRSCOMProp.nByteSize = 7;// Data7Bit;


  sXmlValue = g_xTags.pRS_SettingsNode->GetAttributeValue(g_xTags.sRSAttr_IntervalMulty);
  if (!sXmlValue.IsEmpty())
  {
    int val = CAi_Str::ToInt(sXmlValue);
    if (val > 100)
      val = 20;
    gRSCOMProp.nMultyValue = val;
  }

  m_bCanSave = true;
}

void CConnection_Task_InterfaceChoice_RS::PutValueToXmlTree()
{
  if (!m_bCanSave)
    return;

  g_xTags.pRS_SettingsNode->DeleteAllAttributes();

  CString sValue;

  m_edtDeviceAddress.GetWindowText(sValue);
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSDeviceAddress, sValue);

  sValue = L"";
  int nCurSel = m_lstRS_Port.GetCurSel();
  if (nCurSel >= 0)
  {
    m_lstRS_Port.GetText(nCurSel, sValue);
  }
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSAttr_PortNumber, sValue.TrimLeft(L"COM"));
  /*m_cbxRS_Speed.GetWindowText(sValue);
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSAttr_Speed, sValue);*/
  m_cbxRS_Parity.GetWindowText(sValue);
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSAttr_Parity, sValue);
  m_cbxRS_BitCount.GetWindowText(sValue);
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSAttr_BitCount, sValue);

  m_cbxRS_BytesInterval.GetWindowText(sValue);
  g_xTags.pRS_SettingsNode->AddAttribute(g_xTags.sRSAttr_IntervalMulty, sValue);

  ::PostMessage(m_hWndGlobParent, UM_UPDATE_XML_BACKUP_FILE, 0, 0);
}

/*void CConnection_Task_InterfaceChoice_RS::OnLstSelchangeCbxRsPort()
{
  CtrlsDataWasChanged();
}*/

void CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol()
{
  //CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress()
{
  //CtrlsDataWasChanged(); // ��. DeviceAddressWasChanged
}

void CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed()
{
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity()
{
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount()
{
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout()
{
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::DeviceAddressWasChanged()
{
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnBnClickedBtnPortRefresh()
{
  UsingSetupAPI2(&m_lstRS_Port);
  SelectPrevInterface();
  CtrlsDataWasChanged();
}

void CConnection_Task_InterfaceChoice_RS::OnLbnSelchangeLstRsPort()
{
  CtrlsDataWasChanged();
}
