#pragma once

#include "XmlAid_.h"

struct REG_EDIT_ACCESS_DATA
{
  CString sCurrLogin; // { User | Admin }
  CString sUserPassword;
  CString sAdminPassword;
};

class CAppEnviron
{
public:
  CAppEnviron();
  ~CAppEnviron();

public:
  void CheckElectricMeterFolder() throw(...);
private:
  CString m_strElectricMeterFolder;
  CString m_strElectricMeterTmpFolder;

public:
  void CheckAppWorkingFolder(CString strApplicationName) throw(...);
private:
  CString m_strWorkingFolder;

public:
  void CheckAppSettingsFolder() throw(...);
private:
  CString m_strSettingsFolder;

public:
  void SetProductVersion(CString sVer);

  // Xml file (new work style)
private:
  CXmlAid_ m_xmlSettings;
public:
  CXmlAid_* GetXmlSett() { return &m_xmlSettings; }

  // Ini file (old work style; deleted)

private: // ��� �������, �� � ������ ���������� �� ������������; ������������ XML

//public:
  void CreateMainIniFile(CString strFileName) throw(...);
//private:
  CString m_strAppIniFile;

//private:
  CString m_sCurrBlock;

//public:
  void SetCurrBlock(CString sBlock)
  {
    m_sCurrBlock = sBlock;
  }
  CString GetValue_s(CString sValueName, CString sDef = L""); // use m_sCurrBlock
  void SetValue_s(CString sValueName, CString sValue); // use m_sCurrBlock
  int GetValue_i(CString sValueName, int nDef = 0); // use m_sCurrBlock
  void SetValue_i(CString sValueName, int nValue); // use m_sCurrBlock

  // Ini end

  // RegEdit

public:
  void SaveAccessProp(REG_EDIT_ACCESS_DATA& ac);
  void GetAccessProp(REG_EDIT_ACCESS_DATA& ac);

  // RegEdit end

};

