#pragma once

class CConnection_Task_InterfaceChoice_USB : public CDialogEx
{
  DECLARE_DYNAMIC(CConnection_Task_InterfaceChoice_USB)

public:
  CConnection_Task_InterfaceChoice_USB();
  virtual ~CConnection_Task_InterfaceChoice_USB();

// Dialog Data
  enum { IDD = IDD_CONNECTION_TASK_INTERFACE_CHOICE_USB };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()

public:
  void SetGlobalParent(HWND m_hWnd) { m_hWndGlobParent = m_hWnd; }
private:
  HWND m_hWndGlobParent = nullptr;
};
