#include "stdafx.h"
#include "View_Connection.h"
#include "Ai_CmbBx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Inlay_Connection_InterfaceChoice
{
  #define SIDE_OFFSET 5

  struct X_TAGS
  {
    CString sInterfaceChoice = L"InterfaceChoice";
    CParamsTreeNode* pInterfaceChoiceNode = nullptr;
  }
  g_xTags;

} // namespace ns_Inlay_Connection_InterfaceChoice

using namespace ns_Inlay_Connection_InterfaceChoice;

void CView_Connection::FormTask_InterfaceChoice()
{
  m_tabInterfaces.SetItemSize(CSize(22, 140));

  CRect rect, item_rect;

  m_tabInterfaces.GetClientRect(rect);
  m_tabInterfaces.GetItemRect(0, item_rect);

  rect.top += SIDE_OFFSET;
  rect.left += item_rect.Width() + SIDE_OFFSET;
  rect.right -= SIDE_OFFSET;
  rect.bottom -= SIDE_OFFSET;

  // XML

  CString sPrevSelValue = L"RS";
  CParamsTreeNode* pInlaysNode = GetXmlNode();
  CString sTag = g_xTags.sInterfaceChoice;
  g_xTags.pInterfaceChoiceNode = m_pConnectionNode->FindFirstNode(sTag);
  if (!g_xTags.pInterfaceChoiceNode)
    g_xTags.pInterfaceChoiceNode = m_pConnectionNode->AddNode(sTag);

  m_ConnTask_Prot_RS.SetXmlNodePointer(g_xTags.pInterfaceChoiceNode);

  //

  TCITEM itm = {};
  itm.mask = TCIF_TEXT;
  itm.iImage = -1;
  wchar_t s1[100] = L"����������";
  itm.pszText = s1;
  m_tabInterfaces.InsertItem(0, &itm);
  wchar_t s2[100] = L"   USB   ";
  itm.pszText = s2;
  //m_tabInterfaces.InsertItem(1, &itm);

  itm.mask = TCIF_PARAM;
  itm.lParam = (LPARAM)&m_ConnTask_Prot_RS;
  m_tabInterfaces.SetItem(0, &itm);
  if (m_ConnTask_Prot_RS.Create(m_ConnTask_Prot_RS.IDD, &m_tabInterfaces))
  {
    m_ConnTask_Prot_RS.SetWindowPos(nullptr, rect.left, rect.top, rect.Width(), rect.Height(), SWP_NOSIZE | SWP_NOZORDER);
    //m_ConnTask_Prot_RS.MoveWindow(rect, TRUE);
    m_ConnTask_Prot_RS.ShowWindow(SW_SHOW);
    m_tabInterfaces.AddPage(&m_ConnTask_Prot_RS, CString(s1));
  }

  /*itm.lParam = (LPARAM)&m_ConnTask_Prot_USB;
  m_tabInterfaces.SetItem(1, &itm);
  if (m_ConnTask_Prot_USB.Create(m_ConnTask_Prot_USB.IDD, &m_tabInterfaces))
  {
    m_ConnTask_Prot_USB.SetWindowPos(nullptr, rect.left, rect.top, rect.Width(), rect.Height(), SWP_NOSIZE | SWP_NOZORDER);
    //m_ConnTask_Prot_USB.MoveWindow(rect, TRUE);
    m_ConnTask_Prot_USB.ShowWindow(SW_HIDE);
    m_tabInterfaces.AddPage(&m_ConnTask_Prot_USB, CString(s2));
  }*/

  m_tabInterfaces.SetColours(RGB(100, 100, 255), RGB(0, 0, 0));
  m_tabInterfaces.SetFonts();
   
  m_tabInterfaces.SelectItem(L"����������");
}

void CView_Connection::UpdateInterfaceChoice()
{
  ASSERT(g_xTags.pInterfaceChoiceNode);
  m_ConnTask_Prot_RS.OnJustAfterShow();
}

void CView_Connection::UpdateInterfaceChoiceInStatusBar()
{

}