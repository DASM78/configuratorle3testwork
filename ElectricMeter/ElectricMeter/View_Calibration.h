#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Ai_ColorStatic.h"
#include "afxbutton.h"
#include "afxwin.h"
#include "Model_.h"
#include "DoubleEdit.h"
#include <vector>
using namespace mdl;
using namespace std;
class CView_Calibration : public CCommonViewForm, public CCommonViewDealer
{
	DECLARE_DYNCREATE (CView_Calibration)

public:
	CView_Calibration ();
	~CView_Calibration ();

	DECLARE_MESSAGE_MAP ()

	enum
	{
		IDD = IDD_SETTINGS_CALIBRATION
	};

private:
	static const unsigned int cMinCnt = 5;
	static const unsigned int  cMaxCnt = 50;
	double CView_Calibration::GetAverage (unsigned int id);
	virtual void DoDataExchange (CDataExchange* pDX) override final; // DDX/DDV support
	virtual void OnInitialUpdate () override final;
	virtual void BeforeDestroyClass () override final;
	virtual void OnSize_ () override final;
	virtual void OnDraw (CDC* pDC) override final;
	virtual bool OnNMCustomdrawLst (CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
	void	RequestAllCalibrations ();
	void WriteAllCalibration ();
	virtual void CheckMeaningsViewByConnectionStatus ();
	bool m_isSendCoeffAll = false;
#ifdef _DEBUG
	virtual void AssertValid () const override final;
	virtual void Dump (CDumpContext& dc) const override final;
#endif

	afx_msg void OnContextMenu (CWnd*, CPoint point);

	CEdit m_edtTime;

	CDoubleEdit m_editPointCnt;
	
	CDoubleEdit m_editCoeffI1;
	CDoubleEdit m_editCoeffI2;
	CDoubleEdit m_editCoeffI3;
	CDoubleEdit m_editCoeffU1;
	CDoubleEdit m_editCoeffU2;
	CDoubleEdit m_editCoeffU3;


	CDoubleEdit m_currentI1;
	CDoubleEdit m_currentI2;
	CDoubleEdit m_currentI3;
	CDoubleEdit m_currentU1;
	CDoubleEdit m_currentU2;
	CDoubleEdit m_currentU3;

	CDoubleEdit m_averI1;
	CDoubleEdit m_averI2;
	CDoubleEdit m_averI3;
	CDoubleEdit m_averU1;
	CDoubleEdit m_averU2;
	CDoubleEdit m_averU3;

	CDoubleEdit m_err1;
	CDoubleEdit m_err2;
	CDoubleEdit m_err3;
	CDoubleEdit m_err4;
	CDoubleEdit m_err5;
	CDoubleEdit m_err6;

	CDoubleEdit m_editSetup_I_1;
	CDoubleEdit m_editSetup_I_2;
	CDoubleEdit m_editSetup_I_3;
	CDoubleEdit m_editSetup_U_1;
	CDoubleEdit m_editSetup_U_2;
	CDoubleEdit m_editSetup_U_3;

	struct frame_cntrl_pairT
	{
		int id; // ������ ���������� �����
		teFrameTypes teOnline;
		teFrameTypes teCoeff;
		int idOnline; // id �����, � ������� ��������� ���������� ������ ���������
		int idSetup; // id ����� � ������� ����������� ������ ������� �������� ��������� ��� ����������
		int iderr;// id �����, � ������� ���������� ������
		int idCoeff;// id ������������ 
		int idAverage;
	};

	static const frame_cntrl_pairT m_onlineData[];
	static const frame_cntrl_pairT m_manualCoeff[];

	vector <double> m_vMeasurements[6];
	unsigned int m_points;



	CMFCButton m_btnRecordI1;
	CMFCButton m_btnRecordI2;
	CMFCButton m_btnRecordI3;
	CMFCButton m_btnRecordU1;
	CMFCButton m_btnRecordU2;
	CMFCButton m_btnRecordU3;
	CMFCButton m_btnRecordTime;
	CMFCButton m_btnReadAll;

	afx_msg void OnEnKillfocusEdtI1 ();
	afx_msg void OnEnKillfocusEdtI2 ();
	afx_msg void OnEnKillfocusEdtI3 ();
	afx_msg void OnEnKillfocusEdtU1 ();
	afx_msg void OnEnKillfocusEdtU2 ();
	afx_msg void OnEnKillfocusEdtU3 ();
	afx_msg void OnEnKillfocusEdtTime ();

	afx_msg void OnBnClickedBtnRecordI1 ();
	afx_msg void OnBnClickedBtnRecordI2 ();
	afx_msg void OnBnClickedBtnRecordI3 ();
	afx_msg void OnBnClickedBtnRecordU1 ();
	afx_msg void OnBnClickedBtnRecordU2 ();
	afx_msg void OnBnClickedBtnRecordU3 ();
	afx_msg void OnBnClickedBtnRecordTime ();

	CMFCButton m_btnChangeMode;
	afx_msg void OnBnClickedBtnChangeMode ();
	void UpdateAverage ();
	int CalcCoeff (int prevC, double val, double setup);
	void SendDataTo (mdl::teFrameTypes ft, CEdit* pedt = nullptr);
	virtual void OnConnectionStateWasChanged () override final;
	virtual void UpdateParamMeaningsByReceivedDeviceData (mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
	virtual void UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult) override final;

public:
	afx_msg void OnBnClickedBtnReadall ();
	afx_msg void OnBnClickedBtnCalibrate ();	
	afx_msg void OnEnChangeEditPointsCount ();
	afx_msg void OnEnKillfocusEditPointsCount ();
	afx_msg void OnBnClickedBtnWriteall ();	
	afx_msg void OnBnClickedBtnPpsOn ();
	afx_msg void OnBnClickedBtnPpsOff ();
	afx_msg void OnBnClickedBtnReadTime ();
	afx_msg void OnBnClickedBtnPulses();
	afx_msg void OnBnClickedBtnCalibrateClk();
};
