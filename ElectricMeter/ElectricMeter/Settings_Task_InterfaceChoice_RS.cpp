#include "stdafx.h"
#include "ElectricMeter.h"
#include "Settings_Task_InterfaceChoice_RS.h"
#include "afxdialogex.h"
#include "Ai_COMPortList.h"
#include "Ai_Str.h"
#include "Ai_CmbBx.h"
#include "Ai_Font.h"
#include "RSCOMMng.h"

using namespace std;
using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(CSettings_Task_InterfaceChoice_RS, CDialogEx)

namespace ns_SettRS
{
  CSettings_Task_InterfaceChoice_RS* gpThis = nullptr;
  void DeviceAddressAfterFilter(CString sText)
  {
    gpThis->DeviceAddressWasChanged();
  }
  void DeviceAddress2AfterFilter(CString sText)
  {
    gpThis->DeviceAddress2WasChanged();
  }
  void DeviceAddress3AfterFilter (CString sText)
  {
	  gpThis->DeviceAddress3WasChanged ();
  }
  void DeviceAddress4AfterFilter (CString sText)
  {
	  gpThis->DeviceAddress4WasChanged ();
  }

  struct X_TAGS
  {
    CString sInterfaceChoices = L"InterfaceChoices";
    CParamsTreeNode* pRsNode = nullptr;

    CParamsTreeNode* pRs1Node = nullptr;
    CParamsTreeNode* pRs2Node = nullptr;
    CParamsTreeNode* pRs3Node = nullptr;
	CParamsTreeNode* pRs4Node = nullptr;

    CString sRs1_Settings = L"RS1_Settings";
    CString sRs2_Settings = L"RS2_Settings";
    CString sRs3_Settings = L"RS3_Settings";
	CString sRs4_Settings = L"RS4_Settings";

    CString sRsAttr_On = L"On";
    CString sRsAttr_Protocol = L"Protocol";
    CString sRsAttr_DeviceAddress = L"DeviceAddress";
    CString sRsAttr_PortNumber = L"PortNumber";
    CString sRsAttr_Speed = L"Speed";
    CString sRsAttr_Parity = L"Parity";
    CString sRsAttr_BitCount = L"BitCount";
    CString sRsAttr_IntervalMulty = L"IntervalMulty";
  }
  g_xTags;

  COM_PROP g_RsComProp1;
  COM_PROP g_RsComProp2;
  COM_PROP g_RsComProp3;
  COM_PROP g_RsComProp4;

} // namespace ns_SettRS

CSettings_Task_InterfaceChoice_RS::CSettings_Task_InterfaceChoice_RS(CWnd* pParent /*=NULL*/)
  : CDialogEx(CSettings_Task_InterfaceChoice_RS::IDD, pParent)
{

}

CSettings_Task_InterfaceChoice_RS::~CSettings_Task_InterfaceChoice_RS()
{
}

void CSettings_Task_InterfaceChoice_RS::DoDataExchange(CDataExchange* pDX)
{
  CDialogEx::DoDataExchange(pDX);
  
  DDX_Control(pDX, IDC_CHK_COM1, m_chkUseCOM1);
  DDX_Control(pDX, IDC_CBX_RS_SPEED, m_cbxRS_Speed);
  DDX_Control(pDX, IDC_CBX_RS_PARITY, m_cbxRS_Parity);
  DDX_Control(pDX, IDC_CBX_RS_BIT_COUNT, m_cbxRS_BitCount);
  DDX_Control(pDX, IDC_CBX_RS_INTERVAL_TIMEOUT, m_cbxRS_BytesInterval);
  DDX_Control(pDX, IDC_EDT_DEVICE_ADDRESS, m_edtDeviceAddress);
  DDX_Control(pDX, IDC_CBX_PROTOCOL, m_cbxProtocol);

  DDX_Control(pDX, IDC_CHK_COM2, m_chkUseCOM2);
  DDX_Control(pDX, IDC_CBX_RS_SPEED2, m_cbxRS_Speed2);
  DDX_Control(pDX, IDC_CBX_RS_PARITY2, m_cbxRS_Parity2);
  DDX_Control(pDX, IDC_CBX_RS_BIT_COUNT2, m_cbxRS_BitCount2);
  DDX_Control(pDX, IDC_CBX_RS_INTERVAL_TIMEOUT2, m_cbxRS_IntervalTimeout2);
  DDX_Control(pDX, IDC_EDT_DEVICE_ADDRESS2, m_edtDeviceAddress2);
  DDX_Control(pDX, IDC_CBX_PROTOCOL2, m_cbxProtocol2);

  DDX_Control(pDX, IDC_CHK_COM3, m_chkUseCOM3);
  DDX_Control(pDX, IDC_CBX_RS_SPEED3, m_cbxRS_Speed3);
  DDX_Control(pDX, IDC_CBX_RS_PARITY3, m_cbxRS_Parity3);
  DDX_Control(pDX, IDC_CBX_RS_BIT_COUNT3, m_cbxRS_BitCount3);
  DDX_Control(pDX, IDC_CBX_RS_INTERVAL_TIMEOUT3, m_cbxRS_IntervalTimeout3);
  DDX_Control(pDX, IDC_EDT_DEVICE_ADDRESS3, m_edtDeviceAddress3);
  DDX_Control(pDX, IDC_CBX_PROTOCOL3, m_cbxProtocol3);

  DDX_Control (pDX, IDC_CHK_COM4, m_chkUseGsm);
  DDX_Control (pDX, IDC_CBX_RS_SPEED4, m_cbxRS_Speed4);
  DDX_Control (pDX, IDC_CBX_RS_PARITY4, m_cbxRS_Parity4);
  DDX_Control (pDX, IDC_CBX_RS_BIT_COUNT4, m_cbxRS_BitCount4);
  DDX_Control (pDX, IDC_CBX_RS_INTERVAL_TIMEOUT4, m_cbxRS_IntervalTimeout4);
  DDX_Control (pDX, IDC_EDT_DEVICE_ADDRESS4, m_edtDeviceAddress4);
  DDX_Control (pDX, IDC_CBX_PROTOCOL4, m_cbxProtocol4);

}

BEGIN_MESSAGE_MAP(CSettings_Task_InterfaceChoice_RS, CDialogEx)
  ON_BN_CLICKED(IDC_CHK_COM1, &CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom1)
  ON_BN_CLICKED(IDC_CHK_COM2, &CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom2)
  ON_BN_CLICKED(IDC_CHK_COM3, &CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom3)
  ON_BN_CLICKED (IDC_CHK_COM4, &CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom4)

  ON_CBN_SELCHANGE(IDC_CBX_PROTOCOL, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol1)
  ON_EN_CHANGE(IDC_EDT_DEVICE_ADDRESS, &CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress1)
  ON_CBN_SELCHANGE(IDC_CBX_RS_SPEED, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed1)
  ON_CBN_SELCHANGE(IDC_CBX_RS_PARITY, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity1)
  ON_CBN_SELCHANGE(IDC_CBX_RS_BIT_COUNT, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount1)
  ON_CBN_SELCHANGE(IDC_CBX_RS_INTERVAL_TIMEOUT, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout1)

  ON_CBN_SELCHANGE(IDC_CBX_PROTOCOL2, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol2)
  ON_EN_CHANGE(IDC_EDT_DEVICE_ADDRESS2, &CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress2)
  ON_CBN_SELCHANGE(IDC_CBX_RS_SPEED2, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed2)
  ON_CBN_SELCHANGE(IDC_CBX_RS_PARITY2, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity2)
  ON_CBN_SELCHANGE(IDC_CBX_RS_BIT_COUNT2, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount2)
  ON_CBN_SELCHANGE(IDC_CBX_RS_INTERVAL_TIMEOUT2, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout2)

  ON_CBN_SELCHANGE(IDC_CBX_PROTOCOL3, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol3)
  ON_EN_CHANGE(IDC_EDT_DEVICE_ADDRESS3, &CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress3)
  ON_CBN_SELCHANGE(IDC_CBX_RS_SPEED3, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed3)
  ON_CBN_SELCHANGE(IDC_CBX_RS_PARITY3, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity3)
  ON_CBN_SELCHANGE(IDC_CBX_RS_BIT_COUNT3, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount3)
  ON_CBN_SELCHANGE(IDC_CBX_RS_INTERVAL_TIMEOUT3, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout3)

  ON_CBN_SELCHANGE (IDC_CBX_PROTOCOL4, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol4)
  ON_EN_CHANGE (IDC_EDT_DEVICE_ADDRESS4, &CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress4)
  ON_CBN_SELCHANGE (IDC_CBX_RS_SPEED4, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed4)
  ON_CBN_SELCHANGE (IDC_CBX_RS_PARITY4, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity4)
  ON_CBN_SELCHANGE (IDC_CBX_RS_BIT_COUNT4, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount4)
  ON_CBN_SELCHANGE (IDC_CBX_RS_INTERVAL_TIMEOUT4, &CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout4)


//  ON_BN_CLICKED (IDC_CHK_COM4, &CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom4)
ON_BN_CLICKED(IDC_BUTTON1, &CSettings_Task_InterfaceChoice_RS::OnBnClickedButton1)
ON_EN_CHANGE(IDC_EDIT_DNS, &CSettings_Task_InterfaceChoice_RS::OnEnChangeEditDns)
END_MESSAGE_MAP ()

BOOL CSettings_Task_InterfaceChoice_RS::OnInitDialog()
{
  CDialogEx::OnInitDialog();

  ns_SettRS::g_RsComProp1.nPortType = (int)COM_PORT_TYPE_OPTO;
  ns_SettRS::g_RsComProp2.nPortType = (int)COM_PORT_TYPE_RS232;
  ns_SettRS::g_RsComProp3.nPortType = (int)COM_PORT_TYPE_RS232;
  ns_SettRS::g_RsComProp4.nPortType = (int)COM_PORT_TYPE_OFF;

  ns_SettRS::gpThis = this;
  CString s;
  s.LoadString(IDS_IGNORED_DEVICE_ADDRESS_CHARS);
  auto lmbInitDevAddressCtrl = [=](CAi_EditCtrlCharFilter* pCtrl, void(*pF)(CString s))->void
  {
    pCtrl->IgnoreSimbols(s);
    pCtrl->SetLimitText(32);
    pCtrl->CtrlTextAfterCheck = pF;
  };
  lmbInitDevAddressCtrl(&m_edtDeviceAddress, ns_SettRS::DeviceAddressAfterFilter);
  lmbInitDevAddressCtrl(&m_edtDeviceAddress2, ns_SettRS::DeviceAddress2AfterFilter);
  lmbInitDevAddressCtrl(&m_edtDeviceAddress3, ns_SettRS::DeviceAddress3AfterFilter);
  lmbInitDevAddressCtrl (&m_edtDeviceAddress4, ns_SettRS::DeviceAddress4AfterFilter);

  m_rs1.push_back(&m_edtDeviceAddress);
  m_rs1.push_back(&m_cbxRS_Speed);
  m_rs1.push_back(&m_cbxRS_BytesInterval);

  m_rs2.push_back(&m_edtDeviceAddress2);
  m_rs2.push_back(&m_cbxRS_Speed2);
  m_rs2.push_back(&m_cbxRS_IntervalTimeout2);

  m_rs3.push_back(&m_edtDeviceAddress3);
  m_rs3.push_back(&m_cbxRS_Speed3);
  m_rs3.push_back(&m_cbxRS_IntervalTimeout3);

  m_rs4.push_back (&m_edtDeviceAddress4);
  m_rs4.push_back (&m_cbxRS_Speed4);
  m_rs4.push_back (&m_cbxRS_IntervalTimeout4);


  return TRUE;
}


void CSettings_Task_InterfaceChoice_RS::OnPostInit()
{
}

CString CSettings_Task_InterfaceChoice_RS::FormData(teFrameTypes n)
{
 // COM_PROP* pProp = nullptr;
 // switch (n)
 // {
 //   case FRAME_TYPE_PROP_RS1: pProp = &ns_SettRS::g_RsComProp1; break;
 //   case FRAME_TYPE_PROP_RS2: pProp = &ns_SettRS::g_RsComProp2; break;
 //   case FRAME_TYPE_PROP_RS3: pProp = &ns_SettRS::g_RsComProp3; break;
	//case FRAME_TYPE_PROP_RS4: pProp = &ns_SettRS::g_RsComProp4; break;
 // }

 // if (!pProp->nPortNumber)// � ������� �� ������� �����������, � ������� ��� ���� ����������
 //              // ����� ���������� ��� ���� (���1 ��� ���2); ����� ��� ��������� �������� ��
 //              // checkbox - on/off
 //   return L"";

  CString sData;
  
  return sData;
}

void CSettings_Task_InterfaceChoice_RS::GetComData(bool& is1ComEn, bool& is2ComEn, bool& isCom3En, bool& isCom4En, sCommInfo* info, sgsmSettings *gsmSett)
{
	CComboBox* const pspeed[] = { &m_cbxRS_Speed, &m_cbxRS_Speed2, &m_cbxRS_Speed3, &m_cbxRS_Speed4 };
	CComboBox* const pParity[] = { &m_cbxRS_Parity, &m_cbxRS_Parity2, &m_cbxRS_Parity3, &m_cbxRS_Parity4 };
	CComboBox* const pBits[] = { &m_cbxRS_BitCount, &m_cbxRS_BitCount2, &m_cbxRS_BitCount3, &m_cbxRS_BitCount4, };
	CComboBox* const pBytesIval[] = { &m_cbxRS_BytesInterval, &m_cbxRS_IntervalTimeout2, &m_cbxRS_IntervalTimeout3, &m_cbxRS_IntervalTimeout4, };
	CComboBox* const pProto[] = { &m_cbxProtocol, &m_cbxProtocol2, &m_cbxProtocol3, &m_cbxProtocol4, };
	CAi_EditCtrlCharFilter* const pAdr[] = { &m_edtDeviceAddress, &m_edtDeviceAddress2, &m_edtDeviceAddress3, &m_edtDeviceAddress4 };
	
	for (int i = 0; i < MAX_IFACES; i++) {
		info[i].speed = static_cast <eComSpeed> (pspeed[i]->GetCurSel ());
		info[i].parity = static_cast <eParity> (pParity[i]->GetCurSel ());
		info[i].bits = static_cast <eComBits> (pBits[i]->GetCurSel ());
		info[i].packetInterval = static_cast <eIval> (pBytesIval[i]->GetCurSel ());
		info[i].protocol = static_cast <eProto> (pProto[i]->GetCurSel ());
		CString s;
		(((CEdit *)pAdr[i])->GetWindowText (s));
		info[i].comAddr32 = _ttoi (s); 
	}
	CEdit *pedit;
	pedit = (CEdit *)GetDlgItem(IDC_IPADDRESS);
	CString str;
	pedit->GetWindowText(str);
	wcstombs(gsmSett->aServerIP, str.GetBuffer(), sizeof (gsmSett->aServerIP));
	pedit = (CEdit *)GetDlgItem(IDC_EDIT_APN);
	pedit->GetWindowText(str);
	wcstombs(gsmSett->aApn, str.GetBuffer(), sizeof(gsmSett->aApn));

	pedit = (CEdit *)GetDlgItem(IDC_EDIT_DNS);
	pedit->GetWindowText(str);
	wcstombs(gsmSett->aDnsIP, str.GetBuffer(), sizeof(gsmSett->aDnsIP));

	pedit = (CEdit *)GetDlgItem(IDC_EDIT_USSD_BALANCE);
	pedit->GetWindowText(str);
	wcstombs(gsmSett->ussdNum, str.GetBuffer(), sizeof(gsmSett->ussdNum));

	pedit = (CEdit *)GetDlgItem(IDC_EDIT_PORT);
	pedit->GetWindowText(str);


	is1ComEn = true;
	is2ComEn = false;
	isCom3En = false;
	isCom4En = false;
	CButton *pCheck;
	pCheck = (CButton *)GetDlgItem(IDC_CHK_COM2);
	if (pCheck->GetCheck() == BST_CHECKED) {
		is2ComEn = true;
	}
	pCheck = (CButton *)GetDlgItem(IDC_CHK_COM3);
	if (pCheck->GetCheck() == BST_CHECKED) {
		isCom3En = true;
	}
	pCheck = (CButton *)GetDlgItem(IDC_CHK_COM4);
	if (pCheck->GetCheck() == BST_CHECKED) {
		isCom4En = true;
	}

	
	gsmSett->ipPort = wcstoul (str, NULL, 10);


	
}



void CSettings_Task_InterfaceChoice_RS::SetComData(sCfg m_cfg)
{
	eMeterIface isCom1En = m_cfg.sIface[0].iFace;
	eMeterIface isCom2En = m_cfg.sIface[1].iFace;
	eMeterIface isCom3En = m_cfg.sIface[2].iFace;
	eMeterIface isCom4En = m_cfg.sIface[3].iFace;

	sgsmSettings gsmSett = m_cfg.gsmSett;

	bool isGsm = (isCom2En == IFACE_GSM) || (isCom3En == IFACE_GSM) || (isCom4En == IFACE_GSM);
	if (!isGsm) {
		CEdit *pe = (CEdit *)GetDlgItem(IDC_EDIT_DNS);
		pe->EnableWindow(false);
		pe = (CEdit *)GetDlgItem(IDC_EDIT_USSD_BALANCE);
		pe->EnableWindow(false);
		pe = (CEdit *)GetDlgItem(IDC_IPADDRESS);
		pe->EnableWindow(false);
		pe = (CEdit *)GetDlgItem(IDC_EDIT_PORT);
		pe->EnableWindow(false);
		pe = (CEdit *)GetDlgItem(IDC_EDIT_APN);
		pe->EnableWindow(false);
		CButton *cb = (CButton *)GetDlgItem(IDC_CHK_COM4);
		cb->SetCheck(BST_UNCHECKED);

	}
	else { //gsm
		CButton *cb = (CButton *)GetDlgItem(IDC_CHK_COM4);
		cb->EnableWindow(true);
		cb->SetCheck(BST_CHECKED);

	}


	CComboBox* const pspeed[] = { &m_cbxRS_Speed, &m_cbxRS_Speed2, &m_cbxRS_Speed3, &m_cbxRS_Speed4 };
	CComboBox* const pParity[] = { &m_cbxRS_Parity, &m_cbxRS_Parity2, &m_cbxRS_Parity3, &m_cbxRS_Parity4 };
	CComboBox* const pBits[] = { &m_cbxRS_BitCount, &m_cbxRS_BitCount2, &m_cbxRS_BitCount3, &m_cbxRS_BitCount4, };
	CComboBox* const pBytesIval[] = { &m_cbxRS_BytesInterval, &m_cbxRS_IntervalTimeout2, &m_cbxRS_IntervalTimeout3, &m_cbxRS_IntervalTimeout4, };
	CComboBox* const pProto[] = { &m_cbxProtocol, &m_cbxProtocol2, &m_cbxProtocol3, &m_cbxProtocol4, };
	CAi_EditCtrlCharFilter* const pAdr[] = { &m_edtDeviceAddress, &m_edtDeviceAddress2, &m_edtDeviceAddress3, &m_edtDeviceAddress4 };

	m_chkUseCOM1.SetCheck(isCom1En != IFACE_NO);
	m_chkUseCOM2.SetCheck(isCom2En != IFACE_NO);
	m_chkUseCOM3.SetCheck(isCom3En != IFACE_NO);


	m_chkUseGsm.SetCheck(isGsm);


	memcpy(&m_info, m_cfg.aComInfo, MAX_IFACES * sizeof(sCommInfo));
	for (int i = 0; i < MAX_IFACES; i++) {
		pspeed[i]->SetCurSel(m_info[i].speed);
		pParity[i]->SetCurSel(m_info[i].parity);
		pBits[i]->SetCurSel(m_info[i].bits);
		pBytesIval[i]->SetCurSel(m_info[i].packetInterval);
		pProto[i]->SetCurSel(m_info[i].protocol);
		CString adr;
		adr.Format(_T("%u"), m_info[i].comAddr32);
		pAdr[i]->SetWindowText(adr);

		if (m_cfg.sIface[i].iFace == IFACE_E4 ||
			m_cfg.sIface[i].iFace == IFACE_E2
			)
		{
			pspeed[i]->EnableWindow(true);
			pParity[i]->EnableWindow(true);
			pBits[i]->EnableWindow(true);
			pBytesIval[i]->EnableWindow(true);
			pAdr[i]->EnableWindow(true);
		}
		else {
			pspeed[i]->EnableWindow(false);
			pParity[i]->EnableWindow(false);
			pBits[i]->EnableWindow(false);
			pBytesIval[i]->EnableWindow(false);
			pAdr[i]->EnableWindow(false);
		}

	}
	pspeed[0]->EnableWindow(true); // opto speed enabled
	


	CEdit *pEdit;
	wchar_t wstr[64];
	pEdit = (CEdit *)GetDlgItem(IDC_IPADDRESS);
	mbstowcs(wstr, gsmSett.aServerIP, sizeof(gsmSett.aServerIP));
	pEdit->SetWindowText(wstr);

	pEdit = (CEdit *)GetDlgItem(IDC_EDIT_APN);
	mbstowcs(wstr, gsmSett.aApn, sizeof(gsmSett.aApn));
	pEdit->SetWindowText(wstr);

	pEdit = (CEdit *)GetDlgItem(IDC_EDIT_DNS);
	mbstowcs(wstr, gsmSett.aDnsIP, sizeof(gsmSett.aDnsIP));
	pEdit->SetWindowText(wstr);

	pEdit = (CEdit *)GetDlgItem(IDC_EDIT_USSD_BALANCE);
	mbstowcs(wstr, gsmSett.ussdNum, sizeof(gsmSett.ussdNum));
	pEdit->SetWindowText(wstr);

	pEdit = (CEdit *)GetDlgItem(IDC_EDIT_PORT);
	wsprintf(wstr, _T("%d"), gsmSett.ipPort);
	pEdit->SetWindowText(wstr);


}

void CSettings_Task_InterfaceChoice_RS::SetEnableCtrls(vector<CWnd*>& rs, BOOL bEnable)
{
  for (auto i : rs)
    i->EnableWindow(bEnable);
}

void CSettings_Task_InterfaceChoice_RS::CtrlsDataWasChanged()
{
  if (m_bCtrlsDataIsFilling)
    return;

}

void CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom1()
{
  SetEnableCtrls(m_rs1, (m_chkUseCOM1.GetCheck() == 1) ? TRUE : FALSE);
  CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom2()
{
  SetEnableCtrls(m_rs2, (m_chkUseCOM2.GetCheck() == 1) ? TRUE : FALSE);
  CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom3()
{
  SetEnableCtrls(m_rs3, (m_chkUseCOM3.GetCheck() == 1) ? TRUE : FALSE);
  CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnBnClickedChkCom4 ()
{
	SetEnableCtrls (m_rs4, (m_chkUseGsm.GetCheck () == 1) ? TRUE : FALSE);
	CtrlsDataWasChanged ();
}


void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol1()
{
  //CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol2()
{
  //CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol3 ()
{
	//CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsProtocol4 ()
{
	//CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress1()
{
  //CtrlsDataWasChanged(); ��. DeviceAddressWasChanged
}
void CSettings_Task_InterfaceChoice_RS::DeviceAddressWasChanged()
{
  CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress2()
{
  //CtrlsDataWasChanged(); ��. DeviceAddress2WasChanged
}
void CSettings_Task_InterfaceChoice_RS::DeviceAddress2WasChanged()
{
  CtrlsDataWasChanged();
}

void CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress3()
{
  //CtrlsDataWasChanged(); ��. DeviceAddress3WasChanged
}

void CSettings_Task_InterfaceChoice_RS::OnEnChangeEdtRsDeviceAddress4 ()
{
	//CtrlsDataWasChanged(); ��. DeviceAddress3WasChanged
}

void CSettings_Task_InterfaceChoice_RS::DeviceAddress3WasChanged ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::DeviceAddress4WasChanged ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed1()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed2()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed3 ()
{
	CtrlsDataWasChanged ();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsSpeed4 ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity1()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity2()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity3 ()
{
	CtrlsDataWasChanged ();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsParity4 ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount1()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount2()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount3 ()
{
	CtrlsDataWasChanged ();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsBitCount4 ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout1()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout2()
{
  CtrlsDataWasChanged();
}
void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout3 ()
{
	CtrlsDataWasChanged ();
}

void CSettings_Task_InterfaceChoice_RS::OnCbnSelchangeCbxRsIntervalTimeout4 ()
{
	CtrlsDataWasChanged ();
}



void CSettings_Task_InterfaceChoice_RS::OnBnClickedButton1()
{
}


void CSettings_Task_InterfaceChoice_RS::OnEnChangeEditDns()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
