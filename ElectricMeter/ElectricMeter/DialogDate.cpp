// DialogDate.cpp : implementation file
//

#include "stdafx.h"
#include "MFCTestDlg.h"
#include "DialogDate.h"
#include "afxdialogex.h"


// CDialogDate dialog

IMPLEMENT_DYNAMIC(CDialogDate, CDialog)

CDialogDate::CDialogDate(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogDate::IDD, pParent)
{

}

CDialogDate::CDialogDate (SYSTEMTIME *date, CWnd* pParent/* = NULL*/) : CDialog (CDialogDate::IDD, pParent), m_pdate (date)
{

}

CDialogDate::~CDialogDate()
{
}

void CDialogDate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange (pDX);
	DDX_Control (pDX, IDC_MONTHCALENDAR1, m_calendar);
}


BEGIN_MESSAGE_MAP(CDialogDate, CDialog)
	ON_BN_CLICKED (IDOK, &CDialogDate::OnBnClickedOk)
END_MESSAGE_MAP ()


// CDialogDate message handlers


void CDialogDate::OnBnClickedOk ()
{
	// TODO: Add your control notification handler code here
	SYSTEMTIME date;
	m_calendar.GetCurSel (&date);
	*m_pdate = date;
	CDialog::OnOK ();
}
