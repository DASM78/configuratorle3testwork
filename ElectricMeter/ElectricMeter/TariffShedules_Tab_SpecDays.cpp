#include "stdafx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Defines.h"
#include "View_TariffShedules.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TBL_SPEC_DAYS_MAX 32
#define TBL_SPEC_DAYS_MIN 0
#define TBL_SPEC_DAYS_DEFAULT 2

#define TBL_DATA_AND_SPEC_DAY 2

#define TBL_DEFUALT_DATE_TEXT L"01/01"
#define TBL_DEFUALT_SPEC_DAY_SHEDULE_TEXT L"1"

namespace ns_Tab_SpecDays
{
  struct COLUMNS
  {
    int nSpecDayNumber = -1;
    int nDay = -1;
    int nShedule = -1;
  }
  g_columns;
  int g_nComnSignificantCell = -1;

  struct ROWS
  {
    int nCount_using = TBL_SPEC_DAYS_DEFAULT; // load/save via XML file
    int nCount_using_tmp = -1;
  }
  g_rows;

  struct CELL_PROPS
  {
    bool bShow = true; // �������������� ����
    CString sText;
    bool bEditable = false;
    bool bEnable = true;
    COLORREF colorBg = RGB(0, 0, 0);
    COLORREF colorText = RGB(0, 0, 0);
    CellTextStyle textStyle = ctsNone;
  };

  //                                                  i                                 j
  CELL_PROPS g_cellProps[TBL_DATA_AND_SPEC_DAY][TBL_SPEC_DAYS_MAX]{};
  vector<CString> g_cellProps_tmp; // ��������� �������� �������� �� ��������

  CELL_PROPS* GetCellProp(int j /*row*/, int i /*cell*/)
  {
    ASSERT(j < TBL_SPEC_DAYS_MAX);
    ASSERT(i < TBL_DATA_AND_SPEC_DAY);
    return &g_cellProps[i][j];
  }

  struct X_TAGS
  {
    CString sSpecDaysNode = L"TariffShedules_SpecDays";
    CParamsTreeNode* pSpecDaysNode = nullptr;

    CString sTableNode = L"TableData";
    CString sTableAttr_usingRows = L"UsingRows";
    CParamsTreeNode* pTableNode = nullptr;
    CString sSpecDay = L"SpecDays_";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

} // namespace ns_Tab_SpecDays

//using namespace ns_Tab_SpecDays;

bool CView_TariffShedules::IsSheduleUsing_InSpecDaysTbl(int nShedule)
{
  for (int j = 0; j < TBL_SPEC_DAYS_MAX; ++j)
    for (int i = 1; i < TBL_DATA_AND_SPEC_DAY; ++i)
    {
      if (CAi_Str::ToInt(ns_Tab_SpecDays::GetCellProp(j, i)->sText) == nShedule)
        return true;
    }
  return false;
}

void CView_TariffShedules::TakeValueFromXmlTree_SpecDays()
{
  for (int j = 0; j < TBL_SPEC_DAYS_MAX; ++j)
    for (int i = 0; i < TBL_DATA_AND_SPEC_DAY; ++i)
      ns_Tab_SpecDays::GetCellProp(j, i)->sText = (i == 0) ? TBL_DEFUALT_DATE_TEXT : TBL_DEFUALT_SPEC_DAY_SHEDULE_TEXT;

  CString sTag = ns_Tab_SpecDays::g_xTags.sSpecDaysNode;
  ns_Tab_SpecDays::g_xTags.pSpecDaysNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!ns_Tab_SpecDays::g_xTags.pSpecDaysNode)
  {
    ns_Tab_SpecDays::g_xTags.pSpecDaysNode = m_pTariffShedulesNode->AddNode(sTag);
    return; // ��������� �������� �� ���������
  }

  sTag = ns_Tab_SpecDays::g_xTags.sTableNode; // ������ ������; ������� ���...
  ns_Tab_SpecDays::g_xTags.pTableNode = ns_Tab_SpecDays::g_xTags.pSpecDaysNode->FindFirstNode(sTag);
  if (ns_Tab_SpecDays::g_xTags.pTableNode) //...���� �� ����
    return; // ��������� �������� �� ���������

  ns_Tab_SpecDays::g_xTags.pTableNode = ns_Tab_SpecDays::g_xTags.pSpecDaysNode;
 
  // ������� �� ���� pTableNode - unit-� � �������

  ns_Tab_SpecDays::g_rows.nCount_using = TBL_SPEC_DAYS_DEFAULT;

  int j = 0;
  for (; j < TBL_SPEC_DAYS_MAX; ++j)
  {
    CString sTag;
    sTag.Format(L"%s%.2d", ns_Tab_SpecDays::g_xTags.sSpecDay, j + 1);
    CParamsTreeNode* pTfShdSpecDaysTblRowNode = ns_Tab_SpecDays::g_xTags.pTableNode->FindFirstNode(sTag);
    if (!pTfShdSpecDaysTblRowNode)
      break;

    CString sColumnSettings = pTfShdSpecDaysTblRowNode->GetAttributeValue(ns_Tab_SpecDays::g_xTags.sDataSet);
    ParseRowDataString_SpecDays(sColumnSettings, j);
  }

  if (j == 0) // ��� ��� ����. ��� ����, �� ����� ���, ������ ������������ ������ ������ ����� �� ����
    j = TBL_SPEC_DAYS_MIN;
  ns_Tab_SpecDays::g_rows.nCount_using = max(j, TBL_SPEC_DAYS_MIN); // ����� �� ������� �� �������
}

void CView_TariffShedules::ParseRowDataString_SpecDays(CString& sRowData, int j)
{
  vector <CString> cellSettings;
  if (CAi_Str::CutString(sRowData, L";", &cellSettings)
      && cellSettings.size() == TBL_DATA_AND_SPEC_DAY)
  {
    cellSettings[0].Replace(L"-", L"/"); // date

    for (size_t i = 0; i < cellSettings.size(); ++i)
    {
      //����� ����� ��������:
      //- ��� � cellSettings[0] ���������� ����;
      //- � � cellSettings[> 0] ���������� ������.
      CString sText = cellSettings[i];
      if (i > 0) // ��� ������
        sText = sText.TrimLeft(L"0");
      ns_Tab_SpecDays::GetCellProp(j, i)->sText = sText;
    }
  }
}

void CView_TariffShedules::PutValueToXmlTree_SpecDays()
{
  CString sTag = ns_Tab_SpecDays::g_xTags.sSpecDaysNode;
  ns_Tab_SpecDays::g_xTags.pSpecDaysNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!ns_Tab_SpecDays::g_xTags.pSpecDaysNode)
    ns_Tab_SpecDays::g_xTags.pSpecDaysNode = m_pTariffShedulesNode->AddNode(sTag);

  //old ver: sTag = ns_Tab_SpecDays::g_xTags.sTableNode;
  //old ver: ns_Tab_SpecDays::g_xTags.pSpecDaysNode->DeleteNodes(sTag);
  ns_Tab_SpecDays::g_xTags.pSpecDaysNode->DeleteAllNodes();
  //old ver: ns_Tab_SpecDays::g_xTags.pTableNode = ns_Tab_SpecDays::g_xTags.pSpecDaysNode->AddNode(sTag);
  ns_Tab_SpecDays::g_xTags.pTableNode = ns_Tab_SpecDays::g_xTags.pSpecDaysNode;

  //��������� ������� sTableAttr_usingRows, ��-����, ��������� (��������� ������������� ����������),
  //�.�. ��������� � �������� ���������� ����� �������:
  //- ������������ (�������) ������ - �� ���������� ����� ns_Tab_SpecDays::g_xTags.sSpecDay (��. ����), ��� ����� ����������� ������������ �����;
  //������ ��������� ������� �������� ��� ����������� � ��������.

  //old ver: ns_Tab_SpecDays::g_xTags.pTableNode->AddAttribute(ns_Tab_SpecDays::g_xTags.sTableAttr_usingRows, CAi_Str::ToString(ns_Tab_SpecDays::g_rows.nCount_using));

  // ���������� � ���� pTableNode - unit-�� � �������

  vector<CString> res;
  FormStringFrom_SpecDays(res);

  for (int j = 0; j < ns_Tab_SpecDays::g_rows.nCount_using; ++j)
  {
    CString sTagSpecDayN;
    sTagSpecDayN .Format(L"%s%.2d", ns_Tab_SpecDays::g_xTags.sSpecDay, j + 1);
    CParamsTreeNode* pNode_SpecDayN = ns_Tab_SpecDays::g_xTags.pTableNode->AddNode(sTagSpecDayN);
    CString sValue = res[j];
    pNode_SpecDayN->AddAttribute(ns_Tab_SpecDays::g_xTags.sDataSet, sValue);
  }

  /*// �������� node � ���������� ������

  sTag = L"TariffShedules_SpecDays";
  CParamsTreeNode* pTmp = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (pTmp)
    m_pTariffShedulesNode->DeleteNodes(sTag);

  //*/
}

CString CView_TariffShedules::GetXmlTag_SpecDays()
{
  return ns_Tab_SpecDays::g_xTags.sSpecDaysNode;
}

void CView_TariffShedules::FormStringFrom_SpecDays(vector<CString>& res)
{
  for (int j = 0; j < ns_Tab_SpecDays::g_rows.nCount_using; ++j)
  {
    CString sValue;
    for (int i = 0; i < TBL_DATA_AND_SPEC_DAY; ++i)
    {
      if (i > 0)
        sValue += L";";
      CString sText = ns_Tab_SpecDays::GetCellProp(j, i)->sText;
      if (i == 0)
        sText.Replace(L"/", L"-"); // date
      if (i > 0 && sText.GetLength() == 1)
        sText = L"0" + sText;
      sValue += sText;
    }
    res.push_back(sValue);
  }
}

void CView_TariffShedules::CreateSpecDaysTab()
{
  m_plstSpecDays->ShowHorizScroll(true);

  m_plstSpecDaysEx = new �Ai_ListCtrlEx();
  m_plstSpecDaysEx->SetList(m_plstSpecDays, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    //LVS_EX_FULLROWSELECT |
    LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstSpecDaysEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  m_plstSpecDays->MyPreSubclassWindow();

  bool m_bGridLinesStyle = false;
  if (!m_bGridLinesStyle)
    m_plstSpecDays->SetExtendedStyle(m_plstSpecDays->GetExtendedStyle() & ~LVS_EX_GRIDLINES);
  m_plstSpecDays->EnableInterlacedColorScheme(false);

  m_plstSpecDays->m_bShowHeaderContextMenu = false;
  m_plstSpecDays->m_bCanHeaderDrag = false;

  m_plstSpecDaysEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  ns_Tab_SpecDays::g_columns.nSpecDayNumber = m_plstSpecDaysEx->SetColumnEx(L"����", LVCFMT_LEFT);
  m_plstSpecDaysEx->SetColumnWidthStyle(ns_Tab_SpecDays::g_columns.nSpecDayNumber, CS_WIDTH_FIX, 100);
  ns_Tab_SpecDays::g_columns.nDay = m_plstSpecDaysEx->SetColumnEx(L"���� (��/��)", LVCFMT_CENTER);
  m_plstSpecDaysEx->SetColumnWidthStyle(ns_Tab_SpecDays::g_columns.nDay, CS_WIDTH_FIX, 100);
  ns_Tab_SpecDays::g_columns.nShedule = m_plstSpecDaysEx->SetColumnEx(L"������", LVCFMT_CENTER);
  m_plstSpecDaysEx->SetColumnWidthStyle(ns_Tab_SpecDays::g_columns.nShedule, CS_WIDTH_FIX, 80);

  m_plstSpecDaysEx->MatchColumns();

  int nNodeIdx = m_plstSpecDaysEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  ns_Tab_SpecDays::g_nComnSignificantCell = ns_Tab_SpecDays::g_columns.nSpecDayNumber + 1;
  m_plstSpecDaysEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_Tab_SpecDays::g_nComnSignificantCell);

  bool bLightRowColor = true;
  COLORREF colorLight = RGB(230, 240, 251);
  COLORREF colorDark = RGB(185, 205, 251);
  COLORREF color = colorLight;
  COLORREF colorDisableEditableText = RGB(160, 160, 160);
  COLORREF colorEditableText = RGB(60, 180, 60);

  m_pedtDate_InSpecDaysTbl = new CDateTimeEdit_binding(nullptr);
  m_pedtDate_InSpecDaysTbl->CreateEx(WS_EX_CLIENTEDGE, L"EDIT", nullptr, WS_CHILD | ES_AUTOHSCROLL | ES_CENTER, CRect(0, 0, 0, 0), m_plstSpecDays, ID_EDT_DATE_IN_SPEC_DAYS_TBL_CTRL);
  m_pedtDate_InSpecDaysTbl->SetFont(GetFont());
  m_pedtDate_InSpecDaysTbl->SetMask(L"DD/MM");
  m_pedtDate_InSpecDaysTbl->SetLinkingListCtrEx(m_plstSpecDaysEx);

  BIND_TO_CELL bindColumnList{};
  BIND_CTRL_TO_CELL bindCtrl_date{};
  bindCtrl_date.bDisableBind = false;
  bindCtrl_date.nCell = ns_Tab_SpecDays::g_columns.nDay;
  bindCtrl_date.strByDefault = TBL_DEFUALT_DATE_TEXT;
  bindCtrl_date.pEdit = m_pedtDate_InSpecDaysTbl;
  bindColumnList.ctrlToCell.push_back(bindCtrl_date);

  m_pcbxShedules_InSpecDaysTbl = new CComboBox_binding();
  m_pcbxShedules_InSpecDaysTbl->Create_(m_plstSpecDays, ID_CBX_SHEDULES_IN_SPEC_DAYS_TBL_CTRL, false_d(L"sort"), m_plstSpecDaysEx);
  BIND_CTRL_TO_CELL bindCtrl_tariff{};
  bindCtrl_tariff.bDisableBind = false;
  bindCtrl_tariff.nCell = ns_Tab_SpecDays::g_columns.nShedule;
  bindCtrl_tariff.strByDefault = TBL_DEFUALT_SPEC_DAY_SHEDULE_TEXT;
  bindCtrl_tariff.pCmbBox = m_pcbxShedules_InSpecDaysTbl;
  bindColumnList.ctrlToCell.push_back(bindCtrl_tariff);

  vector<CString> textOfCells;
  textOfCells.push_back(L"���� ");
  textOfCells.push_back(TBL_DEFUALT_DATE_TEXT);
  textOfCells.push_back(TBL_DEFUALT_SPEC_DAY_SHEDULE_TEXT);

  for (int j = 0; j < TBL_SPEC_DAYS_MAX; ++j)
  { // ����������� ����� ��� ������, �� �� default ��� ����� ��������� SetChild, � ���������, ��� ������� SetEmptyChild

    bool bStrongRow = true;
    textOfCells[0] = L"���� " + CAi_Str::ToString(j + 1);
    if (j >= ns_Tab_SpecDays::g_rows.nCount_using)
    {
      bStrongRow = false;
      textOfCells[ns_Tab_SpecDays::g_nComnSignificantCell] = L""; // � significant cell ������ ����� ��� SetEmptyChild
      if (j == 0)
        textOfCells[ns_Tab_SpecDays::g_nComnSignificantCell] = TBL_DEFUALT_DATE_TEXT;
    }

    bLightRowColor = !bLightRowColor;
    color = bLightRowColor ? colorLight : colorDark;

    // ���������� ������. ����� ������������ ���������� ������� ����������� �� XML �����

    for (int i = 0; i < TBL_DATA_AND_SPEC_DAY; ++i)
    {
      ns_Tab_SpecDays::CELL_PROPS* pCellProp = ns_Tab_SpecDays::GetCellProp(j, i);
      pCellProp->colorBg = color;
      pCellProp->bEditable = true;
      pCellProp->colorText = colorEditableText;
      pCellProp->textStyle = ctsBold;

      if (j == 0
          || bStrongRow) // ������ ����� �����
      {
        CString sText = pCellProp->sText;
        if (i > 0)
          sText = L"������ " + sText;
        textOfCells[ns_Tab_SpecDays::g_nComnSignificantCell + i] = sText;
      }
    }

    int nRow_inList = -1;
    if (j == 0
        || bStrongRow) // ������ ����� �����
    {
      nRow_inList = m_plstSpecDaysEx->SetChild(nNodeIdx, &textOfCells, 0, &bindColumnList);
      if (j == 0
          && !bStrongRow)
      {
        // ��� ���� - ��. �������� "Descr. 1" � ��������� AppDescription.txt
        m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nSpecDayNumber);
        m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nDay);
        m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nShedule);
      }
    }
    else
      nRow_inList = m_plstSpecDaysEx->SetEmptyChild(nNodeIdx, &textOfCells, 0, &bindColumnList);
  }

  UpdateView_InSpecDaysTbl();
  m_plstSpecDays->CtrlWasCreated();

#ifdef _DEBUG
  MeaningsWasChanged_InSpecDaysTbl();
#endif
}

void CView_TariffShedules::OnDblClick_InSpecDaysTable(int nRow_inView, int nCell)
{
  int i = nCell - 1 - ns_Tab_SpecDays::g_columns.nSpecDayNumber;
  int j = nRow_inView;
  if (ns_Tab_SpecDays::GetCellProp(j, i)->bEditable)
  {
    if (nRow_inView == 0
        && m_plstSpecDaysEx->GetCellText(nRow_inView + 1, ns_Tab_SpecDays::g_nComnSignificantCell).IsEmpty())
        return;

    if (nCell > ns_Tab_SpecDays::g_columns.nDay)
      m_pcbxShedules_InSpecDaysTbl->FillIn(GetSheduleCount_From24HoursTbl());
    m_plstSpecDaysEx->OnDblClick(nRow_inView, nCell);
  }
}

void CView_TariffShedules::CheckSettBtnEnable_ForSpecDaysTbl()
{
  if (ns_Tab_SpecDays::g_rows.nCount_using >= TBL_SPEC_DAYS_MIN
      && ns_Tab_SpecDays::g_rows.nCount_using < TBL_SPEC_DAYS_MAX)
      m_btnAddRow.EnableWindow(TRUE);
  else
    m_btnAddRow.EnableWindow(FALSE);

  if (ns_Tab_SpecDays::g_rows.nCount_using > TBL_SPEC_DAYS_MIN
      && ns_Tab_SpecDays::g_rows.nCount_using <= TBL_SPEC_DAYS_MAX)
      m_btnDelRow.EnableWindow(TRUE);
  else
    m_btnDelRow.EnableWindow(FALSE);
}

void CView_TariffShedules::OnBnClickedBtnAddRow_InSpecDaysTbl()
{
  if (ns_Tab_SpecDays::g_rows.nCount_using < TBL_SPEC_DAYS_MAX)
  {
    bool bRepairFirstRow = false;
    if (ns_Tab_SpecDays::g_rows.nCount_using == 0)
      bRepairFirstRow = true;
    ++ns_Tab_SpecDays::g_rows.nCount_using;

    int nResult = -1;
    int nNodeRow_inView = 0;
    CString sCellText = TBL_DEFUALT_DATE_TEXT;

    if (ns_Tab_SpecDays::g_rows.nCount_using > 1)
      nResult = m_plstSpecDaysEx->InsertTextToChild(nNodeRow_inView, ns_Tab_SpecDays::g_nComnSignificantCell, sCellText);
    else
      nResult = RCHI_SUCCESS;

    if (nResult != RCHI_SUCCESS)
    {
      ASSERT(false);
      ns_Tab_SpecDays::g_rows.nCount_using--;
    }
    else
    {
      int nRow_inView = -1;
      nRow_inView = m_plstSpecDaysEx->GetLastInsertedChildIdx_inList() - 1;
      if (nRow_inView < 0)
        nRow_inView = 0;
      int nRow_inList = nRow_inView + 1;
      int i = 1; // �������� �������
      int j = nRow_inView;
      ns_Tab_SpecDays::CELL_PROPS* pCellProp = ns_Tab_SpecDays::GetCellProp(j, i);
      pCellProp->sText = TBL_DEFUALT_SPEC_DAY_SHEDULE_TEXT;
      m_plstSpecDaysEx->SetCellText(nRow_inList, ns_Tab_SpecDays::g_columns.nShedule, CString(L"������ ") + pCellProp->sText);
      if (bRepairFirstRow)
      {
        m_plstSpecDaysEx->SetCellShowText(nRow_inList, ns_Tab_SpecDays::g_columns.nSpecDayNumber);
        m_plstSpecDaysEx->SetCellShowText(nRow_inList, ns_Tab_SpecDays::g_columns.nDay);
        m_plstSpecDaysEx->SetCellText(nRow_inList, ns_Tab_SpecDays::g_columns.nDay, TBL_DEFUALT_DATE_TEXT);
        m_plstSpecDaysEx->SetCellShowText(nRow_inList, ns_Tab_SpecDays::g_columns.nShedule);
      }
    }

    if (nResult == RCHI_SUCCESS)
    {
      UpdateView_InSpecDaysTbl();

      if (ns_Tab_SpecDays::g_rows.nCount_using > 1)
        m_plstSpecDaysEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, ns_Tab_SpecDays::g_rows.nCount_using - 1, false_d(L"mouse move"));

      MeaningsWasChanged_InSpecDaysTbl();
    }
    CheckSettBtnEnable_ForSpecDaysTbl();
  }
}

void CView_TariffShedules::OnBnClickedBtnDelRow_InSpecDaysTbl()
{
  if (ns_Tab_SpecDays::g_rows.nCount_using > TBL_SPEC_DAYS_MIN)
  {
    int nRow_inView = ns_Tab_SpecDays::g_rows.nCount_using - 1;
    int nRow_inList = nRow_inView + 1;

    if (ns_Tab_SpecDays::g_rows.nCount_using == 1)
    {
      m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nSpecDayNumber);
      m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nDay);
      m_plstSpecDaysEx->SetCellHideText(nRow_inList, ns_Tab_SpecDays::g_columns.nShedule);
    }
    else
      m_plstSpecDaysEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_Tab_SpecDays::g_nComnSignificantCell, -1);

    for (int cell = 0; cell < TBL_DATA_AND_SPEC_DAY; ++cell)
    {
      int i = cell;
      int j = nRow_inView;
      ns_Tab_SpecDays::CELL_PROPS* pCellProp = ns_Tab_SpecDays::GetCellProp(j, i);
      pCellProp->sText = L"";
      int nCell = cell + ns_Tab_SpecDays::g_columns.nSpecDayNumber + 1;
      m_plstSpecDaysEx->SetCellText(nRow_inView + 1, nCell, L"");
    }

    ns_Tab_SpecDays::g_rows.nCount_using--;
    UpdateView_InSpecDaysTbl(); // ����� ������� ���� ��� ������������� bShow
    if (ns_Tab_SpecDays::g_rows.nCount_using > 0)
      m_plstSpecDaysEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, ns_Tab_SpecDays::g_rows.nCount_using - 1, false_d(L"mouse move"));

    MeaningsWasChanged_InSpecDaysTbl();
    CheckSettBtnEnable_ForSpecDaysTbl();
  }
}

void CView_TariffShedules::Check_InSpecDaysTbl(CString& sErrorMsg)
{ // �� ��������: ��. �������� �������� � ����� TariffShedules.txt

  CString sErrorMsgTmp = L"� ���������� ������� \"�������������� ���\" ���������� ������������ ������.\n\n";
  vector<CString> days;

  for (int j = 0; j < ns_Tab_SpecDays::g_rows.nCount_using; ++j)
  {
    ns_Tab_SpecDays::CELL_PROPS* pUnit = ns_Tab_SpecDays::GetCellProp(j, 0);
    if (j == 0) // ������ ������
    {
      days.push_back(pUnit->sText);
      continue;
    }

    for (size_t cmp = 0; cmp < days.size(); ++cmp)
    {
      if (days.at(cmp) == pUnit->sText) // wrong
      {
        CString sMsg;
        sMsg.Format(L"��� - %d � %d.\n������� - ���� ���� ���������.", cmp + 1, days.size() + 1);
        sErrorMsg = sErrorMsgTmp + sMsg;
        return;
      }
    }
    days.push_back(pUnit->sText);
  }
}

void CView_TariffShedules::UpdateView_InSpecDaysTbl(bool bUpdateCellText)
{
  m_plstSpecDaysEx->UpdateView();

  for (int row = 0; row < TBL_SPEC_DAYS_MAX; ++row)
  {
    int nRow_inList = row + 1;
    int j = row;

    bool bVisible = m_plstSpecDaysEx->IsRowVisible(nRow_inList);
    if (bVisible && row == 0 && ns_Tab_SpecDays::g_rows.nCount_using == 0) // ������ ������ ������
      bVisible = false;
    if (!bVisible && row == 0 && ns_Tab_SpecDays::g_rows.nCount_using == 1) // ������ ������ ��������
      bVisible = true;

    int nColor = bVisible ? ns_Tab_SpecDays::GetCellProp(j, 0)->colorBg : RGB(255, 255, 255);
    m_plstSpecDaysEx->SetRowBgColor(nRow_inList, nColor); // ������������ - ����� ��������� �����

    for (int cell = 0; cell < TBL_DATA_AND_SPEC_DAY; ++cell) // �������� ���������, �� ns_Tab_SpecDays::g_columns.nSpecDayNumber + 1, ����� 
    {
      int i = cell;
      int nCell = i + ns_Tab_SpecDays::g_columns.nSpecDayNumber + 1;
      ns_Tab_SpecDays::CELL_PROPS* pCellProp = ns_Tab_SpecDays::GetCellProp(j, i);

      if (!bVisible)
        pCellProp->bShow = false;
      else
      {
        pCellProp->bShow = true;
        if (pCellProp->bEditable)
        {
          m_plstSpecDaysEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          m_plstSpecDaysEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
          if (!pCellProp->bEnable)
            m_plstSpecDaysEx->SetCellDisable(nRow_inList, nCell);
        }
        else
        {
          if (pCellProp->textStyle != ctsNone)
            m_plstSpecDaysEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          if (pCellProp->colorText > 0)
            m_plstSpecDaysEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
        }

        if (bUpdateCellText)
        {
          CString sCellText = m_plstSpecDaysEx->GetCellText(nRow_inList, nCell);
          if (nCell == ns_Tab_SpecDays::g_columns.nDay
              && sCellText != pCellProp->sText)
              m_plstSpecDaysEx->SetCellText(nRow_inList, nCell, pCellProp->sText);
          if (nCell == ns_Tab_SpecDays::g_columns.nShedule
              && sCellText != L"������ " + pCellProp->sText)
            m_plstSpecDaysEx->SetCellText(nRow_inList, nCell, L"������ " + pCellProp->sText);
        }
      }
    }
  }

  m_plstSpecDaysEx->UpdateView();
}

void CView_TariffShedules::MeanWasSeted_InSpecDaysTbl()
{
  MeaningsWasChanged_InSpecDaysTbl();
}

void CView_TariffShedules::MeaningsWasChanged_InSpecDaysTbl()
{
  GetCutOfMeanings_InSpecDaysTbl();
  PutValueToXmlTree_SpecDays();
  UpdateXmlBackupFile();
}

void CView_TariffShedules::GetCutOfMeanings_InSpecDaysTbl()
{
  for (int j = 0; j < TBL_SPEC_DAYS_MAX; ++j)
  {
    int nRow_inView = j;
    int nRow_inList = nRow_inView + 1;
    if (!m_plstSpecDaysEx->IsRowVisible(nRow_inList))
      break;

    if (nRow_inView == 0
        && m_plstSpecDaysEx->GetCellText(nRow_inList, ns_Tab_SpecDays::g_nComnSignificantCell).IsEmpty())
        return;

    for (int i = 0; i < TBL_DATA_AND_SPEC_DAY; ++i)
    {
      int nCell = ns_Tab_SpecDays::g_columns.nSpecDayNumber + 1 + i;
      CString sText = m_plstSpecDaysEx->GetCellText(nRow_inList, nCell);
      sText.TrimLeft(L"������ ");
      ns_Tab_SpecDays::GetCellProp(j, i)->sText = sText;
    }
  }
}

void CView_TariffShedules::ClearAllReqDataInDB_ForSpecDaysTb()
{
  FormListForClearReceivedData(FRAME_TYPE_SDAY_CNT);
  for (int i = (int)FRAME_TYPE_SDAY_01; i < (int)FRAME_TYPE_SDAY_32; ++i)
    FormListForClearReceivedData((teFrameTypes)i);
}

void CView_TariffShedules::ReceiveData_ForSpecDaysTbl()
{
  ns_Tab_SpecDays::g_cellProps_tmp.clear();
  ns_Tab_SpecDays::g_rows.nCount_using_tmp = -1;

  SetParamTypeForRead(FRAME_TYPE_SDAY_CNT, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
}

void CView_TariffShedules::UpdateParamMeaningsByReceivedDeviceData_SpecDays(teFrameTypes ft)
{
  if (ft == FRAME_TYPE_SDAY_CNT)
  {
    varMeaning_t m = FindReceivedData(FRAME_TYPE_SDAY_CNT);
    if (m != L"?")
    {
      int nNewRowCount = CAi_Str::ToInt(m);
      ns_Tab_SpecDays::g_rows.nCount_using_tmp = min(nNewRowCount, TBL_SPEC_DAYS_MAX); // ����� �� ������� �� ��������
      ns_Tab_SpecDays::g_rows.nCount_using_tmp = max(ns_Tab_SpecDays::g_rows.nCount_using_tmp, TBL_SPEC_DAYS_MIN); // ����� �� ������� �� �������
    }

    if (ns_Tab_SpecDays::g_rows.nCount_using_tmp != 0) // ������ � ���� ������� ������ ����� ��������� �� 0, ��� ������ ������ ��� ����������� ����. ��� ��������
      return;
  }

  if (ft >= FRAME_TYPE_SDAY_01
      && ft <= FRAME_TYPE_SDAY_32)
  {
    varMeaning_t m = FindReceivedData(ft);
    ns_Tab_SpecDays::g_cellProps_tmp.push_back(m);
  }

  if (ns_Tab_SpecDays::g_rows.nCount_using_tmp == ns_Tab_SpecDays::g_cellProps_tmp.size()) // ��� �������� ��� ������� ������ 0 �����, �������� �������
  {
    /*
    - ! -
    ������ � ���� ������� ������ ����� ������� �� 0.
    ������������� ���� �� ������� ft == FRAME_TYPE_SDAY_CNT �� ���������� ������� 0, �� �� ��������
    ��� ���� ������� ������ ������������ �� �����.
    �� ���� ������, �.�. ���������� if ������� ����.
    ����� ������ ��������� ��� ������ � �������.

    */

    if (ns_Tab_SpecDays::g_rows.nCount_using_tmp > ns_Tab_SpecDays::g_rows.nCount_using)
    {
      int nCount = ns_Tab_SpecDays::g_rows.nCount_using_tmp - ns_Tab_SpecDays::g_rows.nCount_using;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnAddRow_InSpecDaysTbl();
    }
    if (ns_Tab_SpecDays::g_rows.nCount_using_tmp < ns_Tab_SpecDays::g_rows.nCount_using)
    {
      int nCount = ns_Tab_SpecDays::g_rows.nCount_using - ns_Tab_SpecDays::g_rows.nCount_using_tmp;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnDelRow_InSpecDaysTbl();
    }

    vector <CString> cellSettings;
    if (!ns_Tab_SpecDays::g_cellProps_tmp.empty() // � �������� �� 0 �����
        && CAi_Str::CutString(ns_Tab_SpecDays::g_cellProps_tmp[0], L";", &cellSettings) // ��������
        && cellSettings.size() <= 1) // �.�. > 1-�� ��������
    {
      ASSERT(false);
      AfxMessageBox(L"������ � ���������� ������!", MB_ICONERROR);
      return;
    }

    for (size_t j = 0; j < ns_Tab_SpecDays::g_cellProps_tmp.size(); ++j)
      ParseRowDataString_SpecDays(ns_Tab_SpecDays::g_cellProps_tmp[j], j);

    UpdateView_InSpecDaysTbl(true_d("�������� ����� � ������"));
    MeaningsWasChanged_InSpecDaysTbl();

    if (IsReqItems_SpecDays())
      m_bShowLoadingSuccessResultMsg = true;
  }
}

bool CView_TariffShedules::IsReqItems_SpecDays()
{
  return (ns_Tab_SpecDays::g_rows.nCount_using_tmp > 0);
}

void CView_TariffShedules::FormAllReadRowReq_SpecDays()
{
  teFrameTypes reqFirstItem = FRAME_TYPE_SDAY_01;
  int nNext = static_cast<int>(reqFirstItem);
  for (int i = 0; i < ns_Tab_SpecDays::g_rows.nCount_using_tmp; ++i)
  {
    reqFirstItem = static_cast<teFrameTypes>(nNext);
    SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    nNext += 1;
  }
}