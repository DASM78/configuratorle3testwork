#pragma once

#include "Ai_ListCtrlEx.h"
#include "Ai_ListCtrlPicker.h"
#include "CommonViewDealer.h"
#include "MultiLineHeaderCtrl.h"

#define IDS_EMPTY_CELL_TEXT L" "

// Select one cell http://www.codeproject.com/Articles/28740/WebControls/

class CCommonListCtrl: public CAi_ListCtrlPicker
{
  DECLARE_DYNAMIC(CCommonListCtrl)

public:
  CCommonListCtrl(�Ai_ListCtrlEx** pplstEx, CCommonViewDealer* pDealer);
  virtual ~CCommonListCtrl();

protected:
  DECLARE_MESSAGE_MAP()

private:
  �Ai_ListCtrlEx** m_pplstEx = nullptr;
  CCommonViewDealer* m_pDealer = nullptr;
  bool m_bShowHorizScroll = true;
  bool m_bCreated = false;
  bool m_bUpdating = false;
  int m_nSpecColorColumn = -1;
  std::vector<int> m_systemColorColumns;
  std::vector<int> m_inactiveTextInColumns; // gray text

public:
  void EnableInterlacedColorScheme(bool bEnable);
  COLORREF GetInterlacedColor();
  COLORREF GetSpecColor();
private:
  bool m_bInterlacedColorScheme = false;
  const COLORREF m_nInterlacedColor = RGB(232, 255, 233);
  const COLORREF m_nSpecColor = RGB(255, 226, 13);

public:
  void DestroyAttachedList();
  void ShowHorizScroll(bool bShow);
  void CtrlWasCreated();
  bool IsCtrlCreated();
  void CtrlIsUpdating(bool bIs);
  void SetSpecColorColumn(int nColumn);
  void SetSystemColorColumns(std::vector<int>* pColumns);
  void SetInactiveTextInColumns(std::vector<int>* pColumnList);

private:
  afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp);
  afx_msg void OnNMCustomdrawLst(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickLst(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkLst(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedLst(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnSize(UINT nType, int cx, int cy);

private:
  bool m_bMultiRowColumnHeader = false;
  CMultiLineHeaderCtrl m_smHeader;
  virtual void InitHeader();
public:
  void UseMultiRowColumns();
  virtual CMFCHeaderCtrl & GetHeaderCtrl();
};


