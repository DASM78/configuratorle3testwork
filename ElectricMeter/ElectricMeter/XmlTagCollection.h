#pragma once

class CXmlTagCollection
{
public:
  CXmlTagCollection();
  ~CXmlTagCollection();

  static CString sAppMainRoot;
  static CString sVersions;
  static CString sVersionsAttr_Product;
  static CString sVersionsAttr_CurrXml;
  static CString sInlays;
};

typedef CXmlTagCollection xTag;