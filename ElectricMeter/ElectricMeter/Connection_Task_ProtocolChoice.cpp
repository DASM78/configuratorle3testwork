#include "stdafx.h"
#include "Resource.h"
#include "View_Connection.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CView_Connection::FormTask_ProtocolChoice()
{
  m_tabProtocols.SetItemSize(CSize(22, 120));

  TCITEM itm = {};
  itm.mask = TCIF_TEXT;
  itm.iImage = -1;
  wchar_t s1[100] = L"IEC 61107-2011";
  itm.pszText = s1;
  m_tabProtocols.InsertItem(0, &itm);

  m_tabProtocols.SetColours(RGB(100, 100, 255), RGB(0, 0, 0));
  m_tabProtocols.SetFonts();
}