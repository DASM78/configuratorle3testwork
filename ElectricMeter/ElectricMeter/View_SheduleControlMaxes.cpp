#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "XmlAid.h"
#include "Ai_File.h"
#include "Ai_Str.h"
#include "..\PrivateLibs\resource.h"
#include "DateTimeEdit.h"
#include <memory>
#include "XmlAid.h"
#include "View_SheduleControlMaxes.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_SheduleControlMaxes, CView)

namespace ns_ShedCtrMax
{
  #define POW_MAX_ROW_MAX 12

  struct COLUMNS
  {
    int nMonth = -1;
    int nMorningBegin = -1;
    int nMorningEnd = -1;
    int nEveningBegin = -1;
    int nEveningEnd = -1;
  }
  g_cols;

  vector<int> g_rows;

  struct DATA
  {
    CString sMorningBegin = L"09:00";
    CString sMorningEnd = L"11:00";
    CString sEveningBegin = L"15:00";
    CString sEveningEnd = L"17:00";
  };
  DATA g_data[POW_MAX_ROW_MAX];
  DATA g_data_tmp[POW_MAX_ROW_MAX]; // �� ����� �������� �� ����������

  struct X_TAGS
  {
    CString sPowMaxShedules = L"PowerMaximum";
    CString sShedule = L"Shedule_";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

} // namespace ns_ShedCtrMax
using namespace ns_ShedCtrMax;

CView_SheduleControlMaxes::CView_SheduleControlMaxes()
  : CCommonViewForm(CView_SheduleControlMaxes::IDD, L"CView_SheduleControlMaxes")
  , m_lstPowerMaxSet(&m_plstPowerMaxSetEx, dynamic_cast<CCommonViewDealer*>(this))
{
  m_lstPowerMaxSet.UseMultiRowColumns();
}

CView_SheduleControlMaxes::~CView_SheduleControlMaxes()
{
  if (m_plstPowerMaxSetEx)
  {
    m_plstPowerMaxSetEx->Destroy();
    delete m_plstPowerMaxSetEx;
  }

  if (m_pcbxTime)
  {
    delete m_pcbxTime;
    m_pcbxTime = nullptr;
  }
}

BEGIN_MESSAGE_MAP(CView_SheduleControlMaxes, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_LOAD_POWMAX_FROM_DEVICE, &CView_SheduleControlMaxes::OnBnClickedBtnLoadPowerMaxFromDevice)
  ON_BN_CLICKED(IDC_BTN_RECORD_POWMAX_TO_DEVICE, &CView_SheduleControlMaxes::OnBnClickedBtnRecordPowerMaxToDevice)
  ON_BN_CLICKED(IDC_BTN_OPEN_POWMAX_FROM_FILE, &CView_SheduleControlMaxes::OnBnClickedBtnOpenPowerMaxFromFile)
  ON_BN_CLICKED(IDC_BTN_SAVE_POWMAX_TO_FILE, &CView_SheduleControlMaxes::OnBnClickedBtnSavePowerMaxToFile)
  ON_BN_CLICKED(IDC_BTN_CHECK_TABLE, &CView_SheduleControlMaxes::OnBnClickedBtnCheck)
  ON_MESSAGE(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, &CView_SheduleControlMaxes::MeanWasSetedAfterBindCtrl)
  ON_BN_CLICKED(IDC_BTN_SET_BY_DEF, &CView_SheduleControlMaxes::OnBnClickedBtnSetByDef)
END_MESSAGE_MAP()

void CView_SheduleControlMaxes::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_LST_DATA_POWER, m_lstPowerMaxSet);
  DDX_Control(pDX, IDC_BTN_LOAD_POWMAX_FROM_DEVICE, m_btnLoadPowerMaxFromDevice);
  DDX_Control(pDX, IDC_BTN_RECORD_POWMAX_TO_DEVICE, m_btnRecordPowerMaxToDevice);
  DDX_Control(pDX, IDC_BTN_OPEN_POWMAX_FROM_FILE, m_btnOpenPowerMaxFromSpecXmlFile);
  DDX_Control(pDX, IDC_BTN_SAVE_POWMAX_TO_FILE, m_btnSavePowerMaxToSpecXmlFile);
  DDX_Control(pDX, IDC_BTN_CHECK_TABLE, m_btnCheck);
  DDX_Control(pDX, IDC_BTN_SET_BY_DEF, m_btnSetByDef);
}

void CView_SheduleControlMaxes::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  m_lstPowerMaxSet.ShowHorizScroll(true);

  m_plstPowerMaxSetEx = new �Ai_ListCtrlEx();
  m_plstPowerMaxSetEx->SetList(&m_lstPowerMaxSet, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstPowerMaxSetEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  m_lstPowerMaxSet.MyPreSubclassWindow();

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstPowerMaxSet.SetExtendedStyle(m_lstPowerMaxSet.GetExtendedStyle() & ~LVS_EX_GRIDLINES);
  m_lstPowerMaxSet.EnableInterlacedColorScheme(true);

  m_lstPowerMaxSet.m_bShowHeaderContextMenu = false;
  m_lstPowerMaxSet.m_bCanHeaderDrag = false;

  m_plstPowerMaxSetEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  m_plstPowerMaxSetEx->SetColumnEx(L"", LVCFMT_LEFT);

  ns_ShedCtrMax::g_cols.nMonth = m_plstPowerMaxSetEx->SetColumnEx(L"�����", LVCFMT_LEFT);
  m_plstPowerMaxSetEx->SetColumnWidthStyle(ns_ShedCtrMax::g_cols.nMonth, CS_WIDTH_FIX, 100);
  ns_ShedCtrMax::g_cols.nMorningBegin = m_plstPowerMaxSetEx->SetColumnEx(L"������\n���������", LVCFMT_CENTER);
  m_plstPowerMaxSetEx->SetColumnWidthStyle(ns_ShedCtrMax::g_cols.nMorningBegin, CS_WIDTH_FIX, 100);
  ns_ShedCtrMax::g_cols.nMorningEnd = m_plstPowerMaxSetEx->SetColumnEx(L"���������\n���������", LVCFMT_CENTER);
  m_plstPowerMaxSetEx->SetColumnWidthStyle(ns_ShedCtrMax::g_cols.nMorningEnd, CS_WIDTH_FIX, 100);
  ns_ShedCtrMax::g_cols.nEveningBegin = m_plstPowerMaxSetEx->SetColumnEx(L"������\n���������", LVCFMT_CENTER);
  m_plstPowerMaxSetEx->SetColumnWidthStyle(ns_ShedCtrMax::g_cols.nEveningBegin, CS_WIDTH_FIX, 100);
  ns_ShedCtrMax::g_cols.nEveningEnd = m_plstPowerMaxSetEx->SetColumnEx(L"���������\n���������", LVCFMT_CENTER);
  m_plstPowerMaxSetEx->SetColumnWidthStyle(ns_ShedCtrMax::g_cols.nEveningEnd, CS_WIDTH_FIX, 100);

  m_plstPowerMaxSetEx->MatchColumns();
  m_lstPowerMaxSet.ShowColumn(0, false);

  int nNodeIdx = m_plstPowerMaxSetEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  m_pcbxTime = new CComboBox_binding();
  m_pcbxTime->Create_(&m_lstPowerMaxSet, ID_CBX_POWER_MAX_TBL_CTRL, false_d(L"sort"), m_plstPowerMaxSetEx);
  BIND_TO_CELL bindTime{};
  for (int i = 0; i < 4; ++i)
  {
    int nCell = ns_ShedCtrMax::g_cols.nMonth + 1 + i;
    BIND_CTRL_TO_CELL bindCtrl_time{};
    bindCtrl_time.bDisableBind = false;
    bindCtrl_time.nCell = nCell;
    bindCtrl_time.strByDefault = L"00:00";
    bindCtrl_time.pCmbBox = m_pcbxTime;
    bindTime.ctrlToCell.push_back(bindCtrl_time);
  }

  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    vector<CString> textOfCells(8);
    switch (j)
    {
      case 0: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"������"; break;
      case 1: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"�������"; break;
      case 2: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"����"; break;
      case 3: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"������"; break;
      case 4: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"���"; break;
      case 5: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"����"; break;
      case 6: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"����"; break;
      case 7: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"������"; break;
      case 8: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"��������"; break;
      case 9: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"�������"; break;
      case 10: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"������"; break;
      case 11: textOfCells[ns_ShedCtrMax::g_cols.nMonth] = L"�������"; break;
    }
    ns_ShedCtrMax::g_rows.push_back(m_plstPowerMaxSetEx->SetChild(nNodeIdx, &textOfCells, 0, &bindTime));

    int nRow_inList = ns_ShedCtrMax::g_rows.at(ns_ShedCtrMax::g_rows.size() - 1);
    for (int i = 0; i < 4; ++i)
    {
      int nCell = ns_ShedCtrMax::g_cols.nMonth + 1 + i;
      m_plstPowerMaxSetEx->SetCellTextColor(nRow_inList, nCell, COLOR_EDITABLE_CELL_TEXT);
      m_plstPowerMaxSetEx->SetCellTextStyle(nRow_inList, nCell, ctsBold);
    }
  }

  m_lstPowerMaxSet.SetSpecColorColumn(ns_ShedCtrMax::g_cols.nMonth);

  TakeValueFromXmlTree();
  FillInTable();

  AddCtrlForPositioning(&m_lstPowerMaxSet, movbehConstTopMarginAndVertSizes, movbehConstLeftMarginAndHorizSizes);

  m_lstPowerMaxSet.CtrlWasCreated();

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  // ToolTip

  m_toolTip.Create(this);
  m_toolTip.Activate(TRUE);

  CMFCToolTipInfo params;
  TOOL_TIP_CTRL_INFO toolTipInfo;

  params.m_bVislManagerTheme = TRUE;
  m_toolTip.SetParams(&params);

  toolTipInfo.strDescription = L"��������� ������ � �������";
  toolTipInfo.nBmpResID = IDB_CHECK_CURR;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_CHECK_TABLE, toolTipInfo));
  m_toolTip.AddTool(&m_btnCheck, L"��������� �������");

  toolTipInfo.strDescription = L"������ ��� ������� ���������� �������� �� ���������";
  toolTipInfo.nBmpResID = IDB_CLOCK_ROUND;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_SET_BY_DEF, toolTipInfo));
  m_toolTip.AddTool(&m_btnSetByDef, L"�������� ����� ����������");

  //

  WatchForCtrState(&m_btnLoadPowerMaxFromDevice, rightCommon);
  WatchForCtrState(&m_btnRecordPowerMaxToDevice, rightAdmin);

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_SheduleControlMaxes::BeforeDestroyClass()
{
}

void CView_SheduleControlMaxes::OnSize_()
{
}

void CView_SheduleControlMaxes::OnDraw(CDC* pDC)
{
}

bool CView_SheduleControlMaxes::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

void CView_SheduleControlMaxes::OnNMDblclkLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  if (nCell > ns_ShedCtrMax::g_cols.nMonth)
  {
    //if (nCell == ns_ShedCtrMax::g_cols.nEveningEnd) ... 23:59
    m_plstPowerMaxSetEx->OnDblClick(nRow_inView, nCell);
  }
}

LRESULT CView_SheduleControlMaxes::MeanWasSetedAfterBindCtrl(WPARAM wParam /*HWND parent*/, LPARAM lParam /* Item ctrl ID */)
{
  GetCutOfMeanings();
  PutValueToXmlTree();
  return TRUE;
}

#ifdef _DEBUG
void CView_SheduleControlMaxes::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_SheduleControlMaxes::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_SheduleControlMaxes::OnContextMenu(CWnd*, CPoint point)
{
}

BOOL CView_SheduleControlMaxes::PreTranslateMessage(MSG* pMsg)
{
  switch (pMsg->message)
  {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_MBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MBUTTONUP:
    case WM_MOUSEMOVE:
      m_toolTip.RelayEvent(pMsg);
      break;
  };

  return __super::PreTranslateMessage(pMsg);
}

void CView_SheduleControlMaxes::FillInTable()
{
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    int nRow_inList = j + 1;
    m_plstPowerMaxSetEx->SetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nMorningBegin, ns_ShedCtrMax::g_data[j].sMorningBegin);
    m_plstPowerMaxSetEx->SetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nMorningEnd, ns_ShedCtrMax::g_data[j].sMorningEnd);
    m_plstPowerMaxSetEx->SetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nEveningBegin, ns_ShedCtrMax::g_data[j].sEveningBegin);
    m_plstPowerMaxSetEx->SetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nEveningEnd, ns_ShedCtrMax::g_data[j].sEveningEnd);
  }
  m_plstPowerMaxSetEx->UpdateView();
}

void CView_SheduleControlMaxes::GetCutOfMeanings()
{
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    int nRow_inList = j + 1;
    ns_ShedCtrMax::g_data[j].sMorningBegin = m_plstPowerMaxSetEx->GetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nMorningBegin);
    ns_ShedCtrMax::g_data[j].sMorningEnd = m_plstPowerMaxSetEx->GetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nMorningEnd);
    ns_ShedCtrMax::g_data[j].sEveningBegin = m_plstPowerMaxSetEx->GetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nEveningBegin);
    ns_ShedCtrMax::g_data[j].sEveningEnd = m_plstPowerMaxSetEx->GetCellText(nRow_inList, ns_ShedCtrMax::g_cols.nEveningEnd);
  }
}

void CView_SheduleControlMaxes::ClearDB()
{
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    ns_ShedCtrMax::g_data[j].sMorningBegin = L"00:00";
    ns_ShedCtrMax::g_data[j].sMorningEnd = L"00:00";
    ns_ShedCtrMax::g_data[j].sEveningBegin = L"00:00";
    ns_ShedCtrMax::g_data[j].sEveningEnd = L"00:00";
  }
}

void CView_SheduleControlMaxes::SetDataByDef()
{
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    ns_ShedCtrMax::g_data[j].sMorningBegin = L"09:00";
    ns_ShedCtrMax::g_data[j].sMorningEnd = L"11:00";
    ns_ShedCtrMax::g_data[j].sEveningBegin = L"15:00";
    ns_ShedCtrMax::g_data[j].sEveningEnd = L"17:00";
  }
}

bool CView_SheduleControlMaxes::ParseData(CString& s, CString& sMBegin, CString& sMEnd, CString& sEBegin, CString& sEEnd)
{
  vector<CString> times;

  if (CAi_Str::CutString(s, L";", &times)
      && times.size() == 4)
  {
    for (int i = 0; i < 4; ++i)
    {
      CDateTimeEdit dt;
      dt.SetMask(L"hh:mm", false_d(L"ctrl is invisible"));
      CString sT = dt.GetDateTime(times[i]).Format(L"%H:%M");
      if (times[i] == sT)
      {
        switch (i)
        {
          case 0: sMBegin = sT; break;
          case 1: sMEnd = sT; break;
          case 2: sEBegin = sT; break;
          case 3: sEEnd = sT; break;
        }
      }
    }
    return true;
  }
  return true;
}

CString CView_SheduleControlMaxes::FormData(int nIdx)
{
  return (ns_ShedCtrMax::g_data[nIdx].sMorningBegin + L";" +
          ns_ShedCtrMax::g_data[nIdx].sMorningEnd + L";" +
          ns_ShedCtrMax::g_data[nIdx].sEveningBegin + L";" +
          ns_ShedCtrMax::g_data[nIdx].sEveningEnd);
}

void CView_SheduleControlMaxes::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_SheduleControlMaxes::CheckMeaningsViewByConnectionStatus()
{
}

void CView_SheduleControlMaxes::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� ���������� ���������!", MB_ICONERROR);
    return;
  }

  if (ft >= FRAME_TYPE_SCHED_P_01
      && ft <= FRAME_TYPE_SCHED_P_12)
  {
    int ftCurr = static_cast<int>(ft);
    int ft1 = static_cast<int>(FRAME_TYPE_SCHED_P_01);
    int nIdx = (ftCurr - ft1);
    ASSERT(!(nIdx < 0));
    ASSERT(!(nIdx >= POW_MAX_ROW_MAX));
    ns_ShedCtrMax::DATA& data_tmp = ns_ShedCtrMax::g_data_tmp[nIdx];
    m.TrimRight(L";");
    bool bResult = ParseData(m,
              data_tmp.sMorningBegin,
              data_tmp.sMorningEnd,
              data_tmp.sEveningBegin,
              data_tmp.sEveningEnd);
    if (bResult)
    {
      ns_ShedCtrMax::DATA& data = ns_ShedCtrMax::g_data[nIdx];
      data = data_tmp;
    }


    if (ft == FRAME_TYPE_SCHED_P_12)
    {
      SetWaitCursor_answer(false);
      FillInTable();
      PutValueToXmlTree();
      AfxMessageBox(L"���������� �������� ���������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }
  }
}

void CView_SheduleControlMaxes::TakeValueFromXmlTree(CParamsTreeNode* pMainNode)
{
  ClearDB();
  CString sTag = ns_ShedCtrMax::g_xTags.sPowMaxShedules;

  if (!pMainNode)
  {
    CParamsTreeNode* pInlaysNode = GetXmlNode();
    m_pPowMaxNode = pInlaysNode->FindFirstNode(sTag);
    if (!m_pPowMaxNode)
    {
      m_pPowMaxNode = pInlaysNode->AddNode(sTag);
      return;
    }
    pMainNode = m_pPowMaxNode;
  }

  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    sTag.Format(L"%s%.2d", ns_ShedCtrMax::g_xTags.sShedule, j + 1);
    CParamsTreeNode* pPowMaxShdNode = pMainNode->FindFirstNode(sTag);
    if (pPowMaxShdNode) // ���� ����� ������ � ����������� �� ����������, �� ��������� ��������� �� ���������
    {
      CString sColumnSettings = pPowMaxShdNode->GetAttributeValue(ns_ShedCtrMax::g_xTags.sDataSet);
      ParseData(sColumnSettings,
                ns_ShedCtrMax::g_data[j].sMorningBegin,
                ns_ShedCtrMax::g_data[j].sMorningEnd,
                ns_ShedCtrMax::g_data[j].sEveningBegin,
                ns_ShedCtrMax::g_data[j].sEveningEnd);
    }
  }
}

void CView_SheduleControlMaxes::PutValueToXmlTree()
{
  m_pPowMaxNode->DeleteAllNodes();
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    CString sTag;
    sTag.Format(L"%s%.2d", ns_ShedCtrMax::g_xTags.sShedule, j + 1);
    CParamsTreeNode* pPowMaxShdNode = m_pPowMaxNode->AddNode(sTag);
    if (pPowMaxShdNode)
    {
      CString s = FormData(j);
      pPowMaxShdNode->AddAttribute(ns_ShedCtrMax::g_xTags.sDataSet, s);
    }
  }

  UpdateXmlBackupFile();
}

void CView_SheduleControlMaxes::OnBnClickedBtnOpenPowerMaxFromFile()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(TRUE, _T("xml"), nullptr,
                  OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  dlg.GetOFN().lpstrTitle = L"�������� ���������� �������� ����������";

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    if (CAi_File::IsFileExist(sFileName))
    {
      unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
      CParamsTreeNode *pPowMaxNode = pXmlAid.get()->CreateTreeFromXmlFile(sFileName);
      if (!pPowMaxNode
          || pPowMaxNode->GetName() != ns_ShedCtrMax::g_xTags.sPowMaxShedules)
      {
        CString s;
        s.Format(L"��������� ����:\n%s\n�� �������� ���������� �������� ����������!", sFileName);
        AfxMessageBox(s, MB_ICONWARNING);
        return;
      }

      // ����� �� ������ ������ � ��������� ��

      TakeValueFromXmlTree(pPowMaxNode);

      // ��������� ����� ������ � xml �� ��������� ��

      FillInTable();
      PutValueToXmlTree();

      // �������� �������

      AfxMessageBox(L"����� ���������� �������� ���������� ������� ��������� �� �����!", MB_ICONINFORMATION);
    }
  }
}

void CView_SheduleControlMaxes::OnBnClickedBtnSavePowerMaxToFile()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(FALSE, _T("xml"), nullptr, OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  CString sTitle = L"C��������� ���������� �������� ����������";
  dlg.GetOFN().lpstrTitle = sTitle.GetBuffer(_MAX_PATH);
  CString sFile = L"��-������������. ���������� �������� ����������.xml";
  dlg.GetOFN().lpstrFile = sFile.GetBuffer(_MAX_PATH);

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
    bool bResult = pXmlAid.get()->CreateXmlFileFromTree(sFileName, m_pPowMaxNode);

    CString s;
    if (bResult)
      s.Format(L"����:\n%s\n� ����������� �������� ���������� ������� ��������!", sFileName);
    else
      s.Format(L"������ ���������� ����� � ����������� �������� ����������:\n%s!", sFileName);
    AfxMessageBox(s, bResult ? MB_ICONINFORMATION : MB_ICONWARNING);
  }
}

void CView_SheduleControlMaxes::OnBnClickedBtnLoadPowerMaxFromDevice()
{
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    ns_ShedCtrMax::g_data_tmp[j].sMorningBegin = L"00:00";
    ns_ShedCtrMax::g_data_tmp[j].sMorningEnd = L"00:00";
    ns_ShedCtrMax::g_data_tmp[j].sEveningBegin = L"00:00";
    ns_ShedCtrMax::g_data_tmp[j].sEveningEnd = L"00:00";
  }
  int ft1 = static_cast<int>(FRAME_TYPE_SCHED_P_01);  
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
    FormListForClearReceivedData(static_cast<teFrameTypes>(ft1 + j));
  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_SheduleControlMaxes::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);

  bool bOnceCmd = true;
  int ft1 = static_cast<int>(FRAME_TYPE_SCHED_P_01);
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    teFrameTypes reqFirstItem = static_cast<teFrameTypes>(ft1 + j);
    SetParamTypeForRead(reqFirstItem, bOnceCmd);
  }

  BeginFormingReadPack(false);
}

void CView_SheduleControlMaxes::OnBnClickedBtnRecordPowerMaxToDevice()
{
  if (!CheckData(false_d(L"OK msg isnt showed")))
    return;

  SetWaitCursor_answer(true);

  BeginFormingSendPack(true_d(L"start"));

  int ft1 = static_cast<int>(FRAME_TYPE_SCHED_P_01);
  for (int j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    teFrameTypes reqFirstItem = static_cast<teFrameTypes>(ft1 + j);
    SendDataToDevice(reqFirstItem, FormData(j));
  }
  
  SendDataToDevice(FRAME_TYPE_COMMAND_MAXP_EXECUTE); // Apply new settings

  BeginFormingSendPack(false_d(L"start"));
}

void CView_SheduleControlMaxes::UpdateInlayByReceivedConfirmation(teFrameTypes ft, bool bResult)
{
  if (!bResult)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ ������ � ������� ���������� �������� ����������!\n������ ������ � ������� ��������.", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_COMMAND_MAXP_EXECUTE)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"���������� �������� ���������� ������� �������� � �������!", MB_ICONINFORMATION);
    return;
  }
}

void CView_SheduleControlMaxes::OnBnClickedBtnCheck()
{
  CheckData(true_d(L"show check messages"));
}

bool CView_SheduleControlMaxes::CheckData(bool bMsg)
{
  CDateTimeEdit dt;
  dt.SetMask(L"hh:mm", false_d(L"ctrl is invisible"));

  int i = 0;
  int j = 0;
  for (j = 0; j < POW_MAX_ROW_MAX; ++j)
  {
    COleDateTime tmePrev;
    COleDateTime tmeCurr;

    for (i = 0; i < 4; ++i)
    {
      switch (i)
      {
        case 0: tmePrev = dt.GetDateTime(ns_ShedCtrMax::g_data[j].sMorningBegin); break;
        case 1: tmeCurr = dt.GetDateTime(ns_ShedCtrMax::g_data[j].sMorningEnd); break;
        case 2: tmeCurr = dt.GetDateTime(ns_ShedCtrMax::g_data[j].sEveningBegin); break;
        case 3: tmeCurr = dt.GetDateTime(ns_ShedCtrMax::g_data[j].sEveningEnd); break;
      }
      if (i == 0) continue;
      if (tmeCurr <= tmePrev)
        break;
      tmePrev = tmeCurr;
    }

    if (i < 4) // error
      break;
  }

  if (i < 4) // error
  {
    CString sMonth;
    switch (j)
    {
      case 0: sMonth = L"������"; break;
      case 1: sMonth = L"�������"; break;
      case 2: sMonth = L"����"; break;
      case 3: sMonth = L"������"; break;
      case 4: sMonth = L"���"; break;
      case 5: sMonth = L"����"; break;
      case 6: sMonth = L"����"; break;
      case 7: sMonth = L"������"; break;
      case 8: sMonth = L"��������"; break;
      case 9: sMonth = L"�������"; break;
      case 10: sMonth = L"������"; break;
      case 11: sMonth = L"�������"; break;
    }

    CString sErrorMsg;
    sErrorMsg.Format(L"� ���������� ������� \"���������� �������� ����������\" ���������� ������������ ������.\n\n����� - %s.\n������� - ����� ���� �� �� �����������.", sMonth);
    CString sSeparator = L"\n______________________________\n\n";
    CString sResultMsg = L"������ �� ���������!" + sSeparator + sErrorMsg;
    AfxMessageBox(sResultMsg, MB_ICONERROR);
    return false;
  }

  if (bMsg)
  {
    CString sResultMsg = L"������ ���������!";
    AfxMessageBox(sResultMsg, MB_ICONINFORMATION);
  }

  return true;
}

void CView_SheduleControlMaxes::OnBnClickedBtnSetByDef()
{
  if (AfxMessageBox(L"�������� ��������� ������� \"���������� �������� ����������\" � �������� �� ���������?", MB_ICONQUESTION | MB_OKCANCEL) == IDCANCEL)
    return;
  SetDataByDef();
  FillInTable();
  PutValueToXmlTree();
}
