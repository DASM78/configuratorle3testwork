#include "stdafx.h"
#include "..\PrivateLibs\resource.h"
#include "Resource.h"
#include "Defines.h"
#include "Ai_Str.h"
#include "Ai_Font.h"
#include "Ai_File.h"
#include <memory>
#include "XmlAid.h"
#include "View_TariffShedules.h"

using namespace std;
using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_TariffShedules, CCommonViewForm)

namespace ns_Inlay_TariffShedules
{
  #define UID_ALL_TARIFF_TABS -1
  #define UID_24HOURS_TAB 0
  #define UID_SEASONS_TAB 1
  #define UID_SPEC_DAYS_TAB 2

  struct X_TAGS
  {
    CString sTariffShedules = L"TariffShedules";
    CString sWorkNode = L"WorkMode"; // ��� (Node � Mode) �� ������ � ��������, ��� ���������
    CParamsTreeNode* pWorkNode = nullptr;
    CString sWorkNodeAttr_Multirate = L"Multirate";
  }
  g_xTags;

} // namespace ns_Inlay_TariffShedules

using namespace ns_Inlay_TariffShedules;

CView_TariffShedules::CView_TariffShedules()
  : CCommonViewForm(CView_TariffShedules::IDD, L"CView_TariffShedules")
{
}

CView_TariffShedules::~CView_TariffShedules()
{
  if (m_plst24HoursEx)
  {
    m_plst24HoursEx->Destroy();
    delete m_plst24HoursEx;
  }
  if (m_plst24Hours)
    delete m_plst24Hours;

  if (m_plstSeasonsEx)
  {
    m_plstSeasonsEx->Destroy();
    delete m_plstSeasonsEx;
  }
  if (m_plstSeasons)
    delete m_plstSeasons;

  if (m_plstSpecDaysEx)
  {
    m_plstSpecDaysEx->Destroy();
    delete m_plstSpecDaysEx;
  }
  if (m_plstSpecDays)
    delete m_plstSpecDays;

  if (m_pedtTime_In24HoursTbl)
  {
    delete m_pedtTime_In24HoursTbl;
    m_pedtTime_In24HoursTbl = nullptr;
  }

  if (m_pedtDate_InSeasonsTbl)
  {
    delete m_pedtDate_InSeasonsTbl;
    m_pedtDate_InSeasonsTbl = nullptr;
  }

  if (m_pcbxTariffs_In24HoursTbl)
  {
    delete m_pcbxTariffs_In24HoursTbl;
    m_pcbxTariffs_In24HoursTbl = nullptr;
  }

  if (m_pcbxShedules_InSeasonsTbl)
  {
    delete m_pcbxShedules_InSeasonsTbl;
    m_pcbxShedules_InSeasonsTbl = nullptr;
  }
}

BEGIN_MESSAGE_MAP(CView_TariffShedules, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_LOAD_TARIFF_FROM_DEVICE, &CView_TariffShedules::OnBnClickedBtnLoadTariffFromDevice)
  ON_BN_CLICKED(IDC_BTN_RECORD_TARIFF_TO_DEVICE, &CView_TariffShedules::OnBnClickedBtnRecordTariffToDevice)
  ON_BN_CLICKED(IDC_RDO_ONE_TARIFF_MODE, &CView_TariffShedules::OnBnClickedRdoOneTariffMode)
  ON_BN_CLICKED(IDC_RDO_MULTI_TARIFF_MODE, &CView_TariffShedules::OnBnClickedRdoMultiTariffMode)
  ON_REGISTERED_MESSAGE(AFX_WM_CHANGE_ACTIVE_TAB, OnChangeActiveTab)
  ON_BN_CLICKED(IDC_BTN_SWITCH_ON_TABLE_SETTINGS, &CView_TariffShedules::OnBnClickedBtnSwitchOnTableSettings)
  ON_BN_CLICKED(IDC_BTN_ADD_COLUMN, &CView_TariffShedules::OnBnClickedBtnAddColumn)
  ON_BN_CLICKED(IDC_BTN_DEL_COLUMN, &CView_TariffShedules::OnBnClickedBtnDelColumn)
  ON_BN_CLICKED(IDC_BTN_ADD_ROW, &CView_TariffShedules::OnBnClickedBtnAddRow)
  ON_BN_CLICKED(IDC_BTN_DEL_ROW, &CView_TariffShedules::OnBnClickedBtnDelRow)
  ON_MESSAGE(MSG_MEAN_WAS_SETED_AFTER_BIND_CTRL, &CView_TariffShedules::MeanWasSetedAfterBindCtrl)
  ON_BN_CLICKED(IDC_BTN_CHECK_TARIFF_SHEDULES, &CView_TariffShedules::OnBnClickedBtnCheckAll)
  ON_BN_CLICKED(IDC_BTN_CHECK_CURR_TARIFF_SHEDULES, &CView_TariffShedules::OnBnClickedBtnCheckCurr)
  ON_BN_CLICKED(IDC_BTN_OPEN_TARIFF_FROM_FILE, &CView_TariffShedules::OnBnClickedBtnOpenTariffFromFile)
  ON_BN_CLICKED(IDC_BTN_SAVE_TARIFF_TO_FILE, &CView_TariffShedules::OnBnClickedBtnSaveTariffToFile)
END_MESSAGE_MAP()

void CView_TariffShedules::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STC_TAB_SPACE, m_stcTabSpace);
  DDX_Control(pDX, IDC_STC_CURRENT_TARIFF, m_stcCurrentTariff);
  DDX_Control(pDX, IDC_BTN_LOAD_TARIFF_FROM_DEVICE, m_btnLoadTariffFromDevice);
  DDX_Control(pDX, IDC_BTN_RECORD_TARIFF_TO_DEVICE, m_btnRecordTariffToDevice);
  DDX_Control(pDX, IDC_BTN_OPEN_TARIFF_FROM_FILE, m_btnOpenTariffFromSpecXmlFile);
  DDX_Control(pDX, IDC_BTN_SAVE_TARIFF_TO_FILE, m_btnSaveTariffToSpecXmlFile);
  DDX_Control(pDX, IDC_STC_WORK_MODE, m_stcWorkModeFrame);
  DDX_Control(pDX, IDC_RDO_ONE_TARIFF_MODE, m_rdoOneTariffMode);
  DDX_Control(pDX, IDC_RDO_MULTI_TARIFF_MODE, m_rdoMultiTariffMode);
  DDX_Control(pDX, IDC_BTN_SWITCH_ON_TABLE_SETTINGS, m_btnSwitchOnTableSettings);
  DDX_Control(pDX, IDC_BTN_ADD_ROW, m_btnAddRow);
  DDX_Control(pDX, IDC_BTN_CHECK_TARIFF_SHEDULES, m_btnCheckAll);
  DDX_Control(pDX, IDC_BTN_CHECK_CURR_TARIFF_SHEDULES, m_btnCheckCurr);
  DDX_Control(pDX, IDC_BTN_DEL_ROW, m_btnDelRow);
  DDX_Control(pDX, IDC_BTN_ADD_COLUMN, m_btnAddColumn);
  DDX_Control(pDX, IDC_BTN_DEL_COLUMN, m_btnDelColumn);
}

void CView_TariffShedules::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  // Tariff tabs

  CRect rectTab;
  m_stcTabSpace.GetWindowRect(&rectTab);
  ScreenToClient(&rectTab);

  m_tabs.Create(CMFCTabCtrl::STYLE_3D_ROUNDED, rectTab, this, IDC_TAB_TARIFFS, CMFCTabCtrl::LOCATION_TOP);

  CCommonViewDealer* pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plst24Hours = new CCommonListCtrl(&m_plst24HoursEx, pCmnViewDlr);
  m_plst24Hours->UseMultiRowColumns();
  m_plstSeasons = new CCommonListCtrl(&m_plstSeasonsEx, pCmnViewDlr);
  m_plstSeasons->UseMultiRowColumns();
  m_plstSpecDays = new CCommonListCtrl(&m_plstSpecDaysEx, pCmnViewDlr);
  m_plstSpecDays->UseMultiRowColumns();

  TakeValueFromXmlTree();
  
  int nListStyle = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT | WS_BORDER | WS_TABSTOP;
  for (int tab = 0; tab < 3; ++tab)
  {
    CCommonListCtrl* pListCtrl = nullptr;
    UINT nIDC = 0;
    switch (tab)
    {
      case UID_24HOURS_TAB:
        pListCtrl = m_plst24Hours;
        nIDC = IDC_LST_24HOURS;
        break;
      case UID_SEASONS_TAB:
        pListCtrl = m_plstSeasons;
        nIDC = IDC_LST_SEASONS;
        break;
      case UID_SPEC_DAYS_TAB:
        nIDC = IDC_LST_SPEC_DAYS;
        pListCtrl = m_plstSpecDays;
        break;
      default:
        ASSERT(false);
        return;
        break;
    }
    pListCtrl->Create(nListStyle, CRect(0, 0, 0, 0), &m_tabs, nIDC);
  }

  m_tabs.SetImageList(IDB_TARIFF_TABS, 16, RGB(255, 0, 255));

  m_tabs.AddTab(m_plst24Hours, L"�������� ������� �������", 0, FALSE);
  m_tabs.AddTab(m_plstSeasons, L"������", 1, FALSE);
  m_tabs.AddTab(m_plstSpecDays, L"�������������� ���", 2, FALSE);

  CMFCTabCtrl::Location location = CMFCTabCtrl::LOCATION_TOP;
  m_tabs.SetLocation(location);
  m_tabs.EnableAutoColor(FALSE);
  m_tabs.RecalcLayout();

  Create24HoursTab();
  CreateSeasonsTab();
  CreateSpecDaysTab();

  // Others ctrls
  
  AddCtrlForPositioning_FollowUpRight(&m_btnSwitchOnTableSettings);
  AddCtrlForPositioning_FollowUpRight(&m_btnAddColumn);
  AddCtrlForPositioning_FollowUpRight(&m_btnDelColumn);
  AddCtrlForPositioning_FollowUpRight(&m_btnCheckAll);
  AddCtrlForPositioning_FollowUpRight(&m_btnCheckCurr);
  AddCtrlForPositioning_FollowUpRight(&m_btnAddRow);
  AddCtrlForPositioning_FollowUpRight(&m_btnDelRow);

  AddCtrlForPositioning_Stretchable(&m_stcTabSpace);
  AddCtrlForPositioning_Stretchable(&m_tabs);

  // Create the ToolTip control.

  m_toolTip.Create(this);
  m_toolTip.Activate(TRUE);

  CMFCToolTipInfo params;
  TOOL_TIP_CTRL_INFO toolTipInfo;

  params.m_bVislManagerTheme = TRUE;
  m_toolTip.SetParams(&params);

  toolTipInfo.strDescription = L"�������� ������ ��� �������������� ������ �������";
  toolTipInfo.nBmpResID = IDB_GEAR_ON;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_SWITCH_ON_TABLE_SETTINGS, toolTipInfo));
  m_toolTip.AddTool(&m_btnSwitchOnTableSettings, L"�������� �������");

  toolTipInfo.strDescription = L"�������� ������ � �������";
  toolTipInfo.nBmpResID = IDB_PLUS;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_ADD_ROW, toolTipInfo));
  m_toolTip.AddTool(&m_btnAddRow, L"�������� ������");
  toolTipInfo.strDescription = L"������� ������ �� �������";
  toolTipInfo.nBmpResID = IDB_MINUS;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_DEL_ROW, toolTipInfo));
  m_toolTip.AddTool(&m_btnDelRow, L"������� ������");

  toolTipInfo.strDescription = L"�������� ������� � �������";
  toolTipInfo.nBmpResID = IDB_PLUS;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_ADD_COLUMN, toolTipInfo));
  m_toolTip.AddTool(&m_btnAddColumn, L"�������� �������");
  toolTipInfo.strDescription = L"������� ������� �� �������";
  toolTipInfo.nBmpResID = IDB_MINUS;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_DEL_COLUMN, toolTipInfo));
  m_toolTip.AddTool(&m_btnDelColumn, L"������� �������");

  toolTipInfo.strDescription = L"��������� ������ � ��������";
  toolTipInfo.nBmpResID = IDB_CHECK_ALL;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_CHECK_TARIFF_SHEDULES, toolTipInfo));
  m_toolTip.AddTool(&m_btnCheckAll, L"��������� �������");
  toolTipInfo.strDescription = L"��������� ������ � ������� �������";
  toolTipInfo.nBmpResID = IDB_CHECK_CURR;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_CHECK_CURR_TARIFF_SHEDULES, toolTipInfo));
  m_toolTip.AddTool(&m_btnCheckCurr, L"��������� �������");

  //

  WatchForCtrState(&m_btnLoadTariffFromDevice, rightCommon);
  WatchForCtrState(&m_btnRecordTariffToDevice, rightAdmin);

  CAi_Font::CreateFont_(this, m_biggerFont, L"", FW_BOLD);
  m_stcCurrentTariff.SetFont(&m_biggerFont);
  SetParamTypeForRead(FRAME_TYPE_CURRENT_RATE);

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData(FRAME_TYPE_CURRENT_RATE);

  //

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));

  //

  CheckSettBtnToolTips();
  CheckSettBtnEnable();

  //

  #ifdef _DEBUG
  OnBnClickedBtnSwitchOnTableSettings();
  #endif
}

void CView_TariffShedules::BeforeDestroyClass()
{
}

void CView_TariffShedules::OnSize_()
{
  if (!m_tabs.m_hWnd)
    return;
  m_tabs.RecalcLayout();
  m_tabs.RedrawWindow();
}

void CView_TariffShedules::OnDraw(CDC* pDC)
{
}

bool CView_TariffShedules::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

void CView_TariffShedules::OnNMDblclkLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  if (pCtrl == m_plst24Hours)
    OnDblClick_In24HoursTable(nRow_inView, nCell);
  if (pCtrl == m_plstSeasons)
    OnDblClick_InSeasonsTable(nRow_inView, nCell);
  if (pCtrl == m_plstSpecDays)
    OnDblClick_InSpecDaysTable(nRow_inView, nCell);
}

BOOL CView_TariffShedules::PreTranslateMessage(MSG* pMsg)
{
  switch (pMsg->message)
  {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_MBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MBUTTONUP:
    case WM_MOUSEMOVE:
      m_toolTip.RelayEvent(pMsg);
      break;
  };

  return __super::PreTranslateMessage(pMsg);
}

#ifdef _DEBUG
void CView_TariffShedules::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_TariffShedules::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_TariffShedules::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_TariffShedules::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_TariffShedules::CheckMeaningsViewByConnectionStatus()
{
  UpdateMonitoringCtrlView(&m_stcCurrentTariff);
}

void CView_TariffShedules::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"� �������� ����������� ��������� �������� ����������!", MB_ICONINFORMATION);
    return;
  }

  if (ft == FRAME_TYPE_CURRENT_RATE
      || ft == FRAME_TYPE_EMPTY)
  {
    varMeaning_t m = L"?";
    if (IsConnection())
    {
      m = FindReceivedData(FRAME_TYPE_CURRENT_RATE);
      if (m != L"?")
      {
        int nM = CAi_Str::ToInt(m) + 1;
        m = CAi_Str::ToString(nM);
      }
    }
    varMeaning_t mCurr;
    m_stcCurrentTariff.GetWindowText(mCurr);
    if (mCurr != m)
      m_stcCurrentTariff.SetWindowText(m);

    return;
  }

  if (ft == FRAME_TYPE_RATE_MODE)
  {
    varMeaning_t m = L"?";
    if (IsConnection())
    {
      m = FindReceivedData(ft);
      if (m != L"?")
      {
        int nM = CAi_Str::ToInt(m);
        if (nM == 0)
        {
          m_rdoOneTariffMode.SetCheck(1);
          m_rdoMultiTariffMode.SetCheck(0);
        }
        else
        {
          m_rdoOneTariffMode.SetCheck(0);
          m_rdoMultiTariffMode.SetCheck(1);
        }
      }
    }
    return;
  }

  if (ft >= FRAME_TYPE_SCHED_CNT && ft <= FRAME_TYPE_SCHED_36)
  {
    UpdateParamMeaningsByReceivedDeviceData_24Hours(ft);
    return;
  }

  if (ft >= FRAME_TYPE_SEAS_CNT && ft <= FRAME_TYPE_SEAS_12)
  {
    UpdateParamMeaningsByReceivedDeviceData_Seasons(ft);
    ShowLoadingSuccessResultMsg();
    return;
  }

  if (ft >= FRAME_TYPE_SDAY_CNT && ft <= FRAME_TYPE_SDAY_32)
  {
    UpdateParamMeaningsByReceivedDeviceData_SpecDays(ft);

    if (ft == FRAME_TYPE_SDAY_CNT)
    {
      if (IsReqItems_ForTablesl())
      {
        BeginFormingReadPack(true);

        FormAllReadRowReq_24Hours();
        FormAllReadRowReq_Seasons();
        FormAllReadRowReq_SpecDays();

        BeginFormingReadPack(false);
      }
      else
      {
        SetWaitCursor_answer(false);
        AfxMessageBox(L"������� �� �������� ������ �� �������� �����������!", MB_ICONINFORMATION);
      }
    }

    ShowLoadingSuccessResultMsg();
    return;
  }
}

bool CView_TariffShedules::IsReqItems_ForTablesl()
{
  return (IsReqItems_24Hours()
          || IsReqItems_Seasons()
          || IsReqItems_SpecDays());
}

void CView_TariffShedules::SendRateMode()
{
  BeginFormingSendPack(true_d(L"start"));
  SendDataToDevice(FRAME_TYPE_RATE_MODE, m_rdoOneTariffMode.GetCheck() ? L"0" : L"1"); // Multirate
  BeginFormingSendPack(false_d(L"start"));
}

void CView_TariffShedules::SendAllShedules()
{
  CString sResultMsg;
  if (!CheckDataOfTariffTables(UID_ALL_TARIFF_TABS, sResultMsg)) // send to device: check all tables
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(sResultMsg, MB_ICONERROR);
    return;
  }

  vector<CString> res_24Hours;
  FormStringFrom_24Hours(res_24Hours, true_d(L"������ � ����������: ����������� �������� � ����� ��������"), false_d(L"not for xlm file"));

  vector<CString> res_Seasons;
  FormStringFrom_Seasons(res_Seasons);

  vector<CString> res_SpecDays;
  FormStringFrom_SpecDays(res_SpecDays);

  BeginFormingSendPack(true_d(L"start"));

  int nRowCount = (int)res_24Hours.size();
  SendDataToDevice(FRAME_TYPE_SCHED_CNT, nRowCount);
  for (size_t j = 0; j < res_24Hours.size(); ++j)
    SendDataToDevice((teFrameTypes)(FRAME_TYPE_SCHED_01 + j), res_24Hours[j]);

  nRowCount = (int)res_Seasons.size();
  SendDataToDevice(FRAME_TYPE_SEAS_CNT, nRowCount);
  for (size_t j = 0; j < res_Seasons.size(); ++j)
    SendDataToDevice((teFrameTypes)(FRAME_TYPE_SEAS_01 + j), res_Seasons[j]);

  nRowCount = (int)res_SpecDays.size();
  SendDataToDevice(FRAME_TYPE_SDAY_CNT, nRowCount);
  for (size_t j = 0; j < res_SpecDays.size(); ++j)
    SendDataToDevice((teFrameTypes)(FRAME_TYPE_SDAY_01 + j), res_SpecDays[j]);

  SendDataToDevice(FRAME_TYPE_COMMAND_RATE_EXECUTE); // Apply new tariff settings

  BeginFormingSendPack(false_d(L"start"));
}

void CView_TariffShedules::UpdateInlayByReceivedConfirmation(teFrameTypes ft, bool bResult)
{
  if (!bResult)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ ������ � ������� �������� ����������!\n������ ������ � ������� ��������.", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_COMMAND_RATE_EXECUTE)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"�������� ���������� ������� �������� � �������!", MB_ICONINFORMATION);
    return;
  }

  if (ft == FRAME_TYPE_RATE_MODE)
  {
    SendAllShedules();
    return;
  }
}

void CView_TariffShedules::TakeValueFromXmlTree()
{
  CParamsTreeNode* pInlaysNode = GetXmlNode();
  CString sTag = g_xTags.sTariffShedules;
  m_pTariffShedulesNode = pInlaysNode->FindFirstNode(sTag);
  if (!m_pTariffShedulesNode)
    m_pTariffShedulesNode = pInlaysNode->AddNode(sTag);

  TakeValueFromXmlTree_24Hours();
  TakeValueFromXmlTree_Seasons();
  TakeValueFromXmlTree_SpecDays();

  CString sValue = L"0"; // 0 - ������������; 1 - �������������
  sTag = g_xTags.sWorkNode;
  g_xTags.pWorkNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  // ���� ���� ����� � ����� ��������� ���: <WorkMode/>, ��� ��������� � ���������� ������������: <WorkMode Multirate="0"/>
  if (!g_xTags.pWorkNode)
    g_xTags.pWorkNode = m_pTariffShedulesNode->AddNode(sTag);
  else
  {
    sValue = g_xTags.pWorkNode->GetAttributeValue(g_xTags.sWorkNodeAttr_Multirate);
    if (sValue.IsEmpty())
      sValue = L"0";
    if (sValue != L"1") // �� ������ ���� �� xml ��������� �����
      sValue = L"0";
  }
  if (sValue == L"0")
    m_rdoOneTariffMode.SetCheck(1);
  else
    m_rdoMultiTariffMode.SetCheck(1);
}

void CView_TariffShedules::PutValueToXmlTree()
{
  CString sValue = m_rdoOneTariffMode.GetCheck() ? L"0" : L"1";
  g_xTags.pWorkNode->DeleteAllAttributes();
  g_xTags.pWorkNode->AddAttribute(g_xTags.sWorkNodeAttr_Multirate, sValue);
  UpdateXmlBackupFile();
}

void CView_TariffShedules::OnBnClickedBtnOpenTariffFromFile()
{
  auto lmbExit = [](CString file_path)->void
  {
    CString s;
    s.Format(L"��������� ����:\n%s\n�� �������� �������� �������!", file_path);
    AfxMessageBox(s, MB_ICONWARNING);
  };

  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(TRUE, _T("xml"), nullptr,
                  OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  dlg.GetOFN().lpstrTitle = L"�������� �������� �������";

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();
    if (CAi_File::IsFileExist(sFileName))
    {
      unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
      CParamsTreeNode *pTree = pXmlAid.get()->CreateTreeFromXmlFile(sFileName);
      if (!pTree)
      {
        lmbExit(sFileName);
        return;
      }

      CParamsTreeNode* p24HoursNode = pTree->FindFirstNode(GetXmlTag_24Hours());
      CParamsTreeNode* pSeasonsNode = pTree->FindFirstNode(GetXmlTag_Seasons());
      CParamsTreeNode* pSpecDaysNode = pTree->FindFirstNode(GetXmlTag_SpecDays());

      if (p24HoursNode && pSeasonsNode && pSpecDaysNode)
      {
        CParamsTreeNode *pTmp = m_pTariffShedulesNode;
        m_pTariffShedulesNode = pTree; // �������� ������ ������ �� �����

        // ����� �� ������ ������ � ��������� ��

        TakeValueFromXmlTree_24Hours();
        TakeValueFromXmlTree_Seasons();
        TakeValueFromXmlTree_SpecDays();

        m_pTariffShedulesNode = pTmp; // � ������ ����� ������ ����, �� �������� ���� ������� ������ ������ �������

        // ��������� ����� ������ � xml �� ��������� ��

        PutValueToXmlTree_24Hours();
        PutValueToXmlTree_Seasons();
        PutValueToXmlTree_SpecDays();

        // �������� �������

        ::PostMessage(GetGlobalParent(), UM_RELOAD_CURRENT_VIEW, 0, 0);

        AfxMessageBox(L"����� ��������� ������� ������� ��������� �� �����!", MB_ICONINFORMATION);
        return;
      }

      lmbExit(sFileName);
    }
  }
}

void CView_TariffShedules::OnBnClickedBtnSaveTariffToFile()
{
  CString sFilter = L"���������� ����� (*.xml)|*.xml||||";
  CFileDialog dlg(FALSE, _T("xml"), nullptr, OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
  CString sTitle = L"C��������� �������� �������";
  dlg.GetOFN().lpstrTitle = sTitle.GetBuffer(_MAX_PATH);
  CString sFile = L"��-������������. ������.xml";
  dlg.GetOFN().lpstrFile = sFile.GetBuffer(_MAX_PATH);

  int nID = dlg.DoModal();
  if (nID == IDOK)
  {
    CString sFileName = dlg.GetPathName();

    CString sTag = g_xTags.sWorkNode;
    m_pTariffShedulesNode->DeleteNodes(sTag);

    unique_ptr<CXmlAid> pXmlAid = make_unique<CXmlAid>();
    bool bResult = pXmlAid.get()->CreateXmlFileFromTree(sFileName, m_pTariffShedulesNode);

    g_xTags.pWorkNode = m_pTariffShedulesNode->AddNode(sTag);

    CString s;
    if (bResult)
      s.Format(L"����:\n%s\n� ����������� ������� ������� ��������!", sFileName);
    else
      s.Format(L"������ ���������� ����� � ����������� �������:\n%s!", sFileName);
    AfxMessageBox(s, bResult ? MB_ICONINFORMATION : MB_ICONWARNING);
  }
}

void CView_TariffShedules::OnBnClickedBtnSwitchOnTableSettings()
{
  m_bSwitchOnCurrSettings = !m_bSwitchOnCurrSettings;

  int nImgID = IDB_GEAR_OFF;
  BOOL bEnable = FALSE;
  int nShow = SW_HIDE;

  if (m_bSwitchOnCurrSettings)
  {
    nImgID = IDB_GEAR_ON;
    bEnable = TRUE;
    nShow = SW_SHOW;

    TOOL_TIP_CTRL_INFO toolTipInfo;
    toolTipInfo.strDescription = L"������ ������ ��� �������������� ������ �������";
    toolTipInfo.nBmpResID = IDB_GEAR_OFF;
    m_toolTip.m_toolTCtrlsInfo[IDC_BTN_SWITCH_ON_TABLE_SETTINGS] = toolTipInfo;
    m_toolTip.DelTool(&m_btnSwitchOnTableSettings);
    m_toolTip.AddTool(&m_btnSwitchOnTableSettings, L"�������� �������");
  }
  else
  {
    TOOL_TIP_CTRL_INFO toolTipInfo;
    toolTipInfo.strDescription = L"�������� ������ ��� �������������� ������ �������";
    toolTipInfo.nBmpResID = IDB_GEAR_ON;
    m_toolTip.m_toolTCtrlsInfo[IDC_BTN_SWITCH_ON_TABLE_SETTINGS] = toolTipInfo;
    m_toolTip.DelTool(&m_btnSwitchOnTableSettings);
    m_toolTip.AddTool(&m_btnSwitchOnTableSettings, L"�������� �������");
  }

  int nSleep = 50;
  if (nShow)
  {
    m_btnDelColumn.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnAddColumn.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnDelRow.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnAddRow.ShowWindow(nShow);
    Sleep(nSleep);
    m_btnCheckCurr.ShowWindow(nShow);
    Sleep(nSleep);
    m_btnCheckAll.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
  }
  else
  {
    m_btnCheckAll.ShowWindow(nShow);
    Sleep(nSleep);
    m_btnCheckCurr.ShowWindow(nShow);
    Sleep(nSleep);
    m_btnAddRow.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnDelRow.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnAddColumn.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
    m_btnDelColumn.ShowWindow(nShow);
    Sleep(nSleep);
    UpdateWindow();
  }

  m_btnSwitchOnTableSettings.SetImage(nImgID);
}

void CView_TariffShedules::OnBnClickedBtnAddColumn()
{
  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB: OnBnClickedBtnAddColumn_In24HoursTbl(); break;
  }
}

void CView_TariffShedules::OnBnClickedBtnDelColumn()
{
  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB: OnBnClickedBtnDelColumn_In24HoursTbl(); break;
  }
}

void CView_TariffShedules::OnBnClickedBtnAddRow()
{
  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB: OnBnClickedBtnAddRow_In24HoursTbl(); break;
    case UID_SEASONS_TAB: OnBnClickedBtnAddRow_InSeasonsTbl(); break;
    case UID_SPEC_DAYS_TAB: OnBnClickedBtnAddRow_InSpecDaysTbl(); break;
  }
}

void CView_TariffShedules::OnBnClickedBtnDelRow()
{
  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB: OnBnClickedBtnDelRow_In24HoursTbl(); break;
    case UID_SEASONS_TAB: OnBnClickedBtnDelRow_InSeasonsTbl(); break;
    case UID_SPEC_DAYS_TAB: OnBnClickedBtnDelRow_InSpecDaysTbl(); break;
  }
}

void CView_TariffShedules::OnBnClickedBtnCheckAll()
{
  CString sResultMsg;
  bool bResult = CheckDataOfTariffTables(UID_ALL_TARIFF_TABS, sResultMsg);  // button: check all tables
  AfxMessageBox(sResultMsg, bResult ? MB_ICONINFORMATION : MB_ICONERROR);
}

void CView_TariffShedules::OnBnClickedBtnCheckCurr()
{
  CString sResultMsg;
  bool bResult = CheckDataOfTariffTables(m_nCurrSheduleTabIdx, sResultMsg); // button: check special table
  AfxMessageBox(sResultMsg, bResult ? MB_ICONINFORMATION : MB_ICONERROR);
}

bool CView_TariffShedules::CheckDataOfTariffTables(int nTabIdx, CString& sResultMsg)
{
  CString sErrorMsg;
  CString sErrorMsgAll;
  CString sSeparator = L"\n______________________________\n\n";
  CString sSep;

  int nTabIdx_ = (nTabIdx == -1) ? UID_24HOURS_TAB : nTabIdx;
  switch (nTabIdx_)
  {
    case UID_24HOURS_TAB:
      Check_In24HoursTbl(sErrorMsg);
      if (!sErrorMsg.IsEmpty())
      {
        sErrorMsgAll += sErrorMsg;
        sErrorMsg = L"";
        sSep = sSeparator;
      }
      if (nTabIdx != -1)
        break;

    case UID_SEASONS_TAB:
      Check_InSeasonsTbl(sErrorMsg);
      if (!sErrorMsg.IsEmpty())
      {
        sErrorMsgAll += sSep + sErrorMsg + L"\n";
        sErrorMsg = L"";
        sSep = sSeparator;
      }
      if (nTabIdx != -1)
        break;

    case UID_SPEC_DAYS_TAB:
      Check_InSpecDaysTbl(sErrorMsg);
      if (!sErrorMsg.IsEmpty())
        sErrorMsgAll += sSep + sErrorMsg + L"\n";
      break;
  }

  if (sErrorMsgAll.IsEmpty())
    sResultMsg = L"������ ���������!";
  else
  {
    sResultMsg = L"������ �� ���������!" +sSeparator + sErrorMsgAll;
    return false;
  }

  return true;
}

LRESULT CView_TariffShedules::OnChangeActiveTab(WPARAM wparam, LPARAM lparam)
{
  m_nCurrSheduleTabIdx = (int)wparam;
  Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bSetVariable_d);
  CheckSettBtnToolTips();
  CheckSettBtnEnable();
  return 0;
}

void CView_TariffShedules::CheckSettBtnToolTips()
{
  if (!GetInitialUpdateFlag())
    return;

  TOOL_TIP_CTRL_INFO toolTipInfo;

  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB:
      toolTipInfo.strDescription = L"�������� ������ � ������� \"�������� ������� �������\"";
      toolTipInfo.nBmpResID = IDB_PLUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_ADD_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnAddRow);
      m_toolTip.AddTool(&m_btnAddRow, L"�������� ������");
      toolTipInfo.strDescription = L"������� ������ �� ������� \"�������� ������� �������\"";
      toolTipInfo.nBmpResID = IDB_MINUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_DEL_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnDelRow);
      m_toolTip.AddTool(&m_btnDelRow, L"������� ������");

      toolTipInfo.strDescription = L"�������� ������� � ������� \"�������� ������� �������\"";
      toolTipInfo.nBmpResID = IDB_PLUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_ADD_COLUMN] = toolTipInfo;
      m_toolTip.DelTool(&m_btnAddColumn);
      m_toolTip.AddTool(&m_btnAddColumn, L"�������� ����� ������������");
      toolTipInfo.strDescription = L"������� ������� �� ������� \"�������� ������� �������\"";
      toolTipInfo.nBmpResID = IDB_MINUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_DEL_COLUMN] = toolTipInfo;
      m_toolTip.DelTool(&m_btnDelColumn);
      m_toolTip.AddTool(&m_btnDelColumn, L"������� ����� ������������");
      break;

    case UID_SEASONS_TAB:
      toolTipInfo.strDescription = L"�������� ������ � ������� \"������\"";
      toolTipInfo.nBmpResID = IDB_PLUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_ADD_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnAddRow);
      m_toolTip.AddTool(&m_btnAddRow, L"�������� �����");
      toolTipInfo.strDescription = L"������� ������ �� ������� \"������\"";
      toolTipInfo.nBmpResID = IDB_MINUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_DEL_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnDelRow);
      m_toolTip.AddTool(&m_btnDelRow, L"������� �����");

      m_toolTip.DelTool(&m_btnAddColumn);
      m_toolTip.DelTool(&m_btnDelColumn);
      break;

    case UID_SPEC_DAYS_TAB:
      toolTipInfo.strDescription = L"�������� ������ � ������� \"�������������� ���\"";
      toolTipInfo.nBmpResID = IDB_PLUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_ADD_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnAddRow);
      m_toolTip.AddTool(&m_btnAddRow, L"�������� ����");
      toolTipInfo.strDescription = L"������� ������ �� ������� \"�������������� ���\"";
      toolTipInfo.nBmpResID = IDB_MINUS;
      m_toolTip.m_toolTCtrlsInfo[IDC_BTN_DEL_ROW] = toolTipInfo;
      m_toolTip.DelTool(&m_btnDelRow);
      m_toolTip.AddTool(&m_btnDelRow, L"������� ����");

      m_toolTip.DelTool(&m_btnAddColumn);
      m_toolTip.DelTool(&m_btnDelColumn);
      break;
  }
}

void CView_TariffShedules::CheckSettBtnEnable()
{
  if (!GetInitialUpdateFlag())
    return;

  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB:
      m_btnAddColumn.EnableWindow(TRUE);
      m_btnDelColumn.EnableWindow(TRUE);
      CheckSettBtnEnable_For24HoursTbl();
      break;

    case UID_SEASONS_TAB:
      m_btnAddColumn.EnableWindow(FALSE);
      m_btnDelColumn.EnableWindow(FALSE);
      CheckSettBtnEnable_ForSeasonsTbl();
      break;

    case UID_SPEC_DAYS_TAB:
      m_btnAddColumn.EnableWindow(FALSE);
      m_btnDelColumn.EnableWindow(FALSE);
      CheckSettBtnEnable_ForSpecDaysTbl();
      break;
  }
}

// ���������� - ������������ ��� ��� ������ � �������� ������� ��� ����. ����, ���� ��, �� ������� ����� ������ �� ������� �������� ����� ������
bool CView_TariffShedules::IsSheduleUsing(int nShedule)
{
  return (IsSheduleUsing_InSeasonsTbl(nShedule)
    || IsSheduleUsing_InSpecDaysTbl(nShedule));
}

void CView_TariffShedules::OnBnClickedBtnLoadTariffFromDevice()
{
  ClearAllReqDataInDB_For24HoursTbl();
  ClearAllReqDataInDB_ForSeasonsTbl();
  ClearAllReqDataInDB_ForSpecDaysTb();
  FormListForClearReceivedData(FRAME_TYPE_RATE_MODE);

  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_TariffShedules::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);
  m_bShowLoadingSuccessResultMsg = false;

  SetParamTypeForRead(FRAME_TYPE_RATE_MODE, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
  ReceiveData_For24HoursTbl();
  ReceiveData_ForSeasonsTbl();
  ReceiveData_ForSpecDaysTbl();

  BeginFormingReadPack(false);
}

void CView_TariffShedules::ShowLoadingSuccessResultMsg()
{
  if (m_bShowLoadingSuccessResultMsg)
  {
    m_bShowLoadingSuccessResultMsg = false;
    SetWaitCursor_answer(false);
    AfxMessageBox(L"�������� ���������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
  }
}

void CView_TariffShedules::OnBnClickedBtnRecordTariffToDevice()
{
  SetWaitCursor_answer(true);
  SendRateMode(); // � ������ ����� RateMode, ���� ������� ������������� ������������� �� ������, �� ����� �������� ��� ������
}

void CView_TariffShedules::OnBnClickedRdoOneTariffMode()
{
  PutValueToXmlTree();
}

void CView_TariffShedules::OnBnClickedRdoMultiTariffMode()
{
  PutValueToXmlTree();
}

LRESULT CView_TariffShedules::MeanWasSetedAfterBindCtrl(WPARAM wParam /*HWND parent*/, LPARAM lParam /* Item ctrl ID */)
{
  switch (m_nCurrSheduleTabIdx)
  {
    case UID_24HOURS_TAB: MeanWasSeted_In24HoursTbl(); break;
    case UID_SEASONS_TAB: MeanWasSeted_InSeasonsTbl(); break;
    case UID_SPEC_DAYS_TAB: MeanWasSeted_InSpecDaysTbl(); break;
    default: ASSERT(false);
  }
  return TRUE;
}

