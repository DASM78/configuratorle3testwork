#include "stdafx.h"
#include "DataStore.h"
#include "Ai_Str.h"

using namespace std;
using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CDataStore::CDataStore()
{
  FormDataSetEx();
}

CDataStore::~CDataStore()
{
  ClearReqs();
}
//��� ������???
void CDataStore::FormDataForReqs(formID_t nID, dataList_t* pData)
{
  m_dataForReqForFormID = 0;

  auto iter = m_dataGroups.find(nID);
  if (iter != m_dataGroups.end())
    m_dataGroups.erase(iter);
  m_dataGroups.insert(pair<formID_t, dataList_t>(nID, *pData));
  
  iter = m_dataGroups.find(nID);
  if (iter == m_dataGroups.end())
  {
    ASSERT(false);
    return;
  }

  ClearReqs();

  for (size_t i = 0; i < iter->second.size(); ++i)
  {
    unsigned int nAddress = iter->second.at(i).first;
    bool bOnce = iter->second.at(i).second;
    DataSetEx dSet;
    if (GetDataSetEx(&dSet, nAddress))
    {
      int nSz = strlen(dSet.tD.cAddr);
      if (nSz > 0)
      {
        unsigned char* p = new unsigned char[nSz + 1]{};
        _mbsncat_s(p, nSz + 1, (const unsigned char*)dSet.tD.cAddr, nSz);
        m_dataForReq.push_back(pair<const unsigned char*, bool>(p, bOnce));
      }
    }
  }

  m_dataForReqForFormID = m_dataForReq.empty() ? 0 : nID;
}

vector<pair<const unsigned char*, bool>>* CDataStore::GetDataForReqs()
{
  return &m_dataForReq;
}

formID_t CDataStore::GetCurrReqFormID()
{
  return m_dataForReqForFormID;
}

void CDataStore::ClearReqs()
{
  for (size_t i = 0; i < m_dataForReq.size(); ++i)
    delete m_dataForReq.at(i).first;
  m_dataForReq.clear();
}

bool CDataStore::ParseReceivedMonitoringData(CString& sReqAddr, CString& sData)
{
  vector <CString> dataString;
  CAi_Str::CutString(sData, L"\r\n", &dataString);
  if (dataString.empty())
    return false;

  if (dataString[0] == L"\xF") // NAK
  {
    dataString[0] = sReqAddr + L"(" + dataString[0] + L")";
    TRACE(L".......NA-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-A-AK........\n");
    //ASSERT(false);
    //AfxMessageBox(L"");
  }
  
  bool bDataIsCorrect = false;

  for (size_t i = 0; i < dataString.size(); ++i)
  {
    vector <CString> dataSet;
    CString sD = dataString[i];
    int nF = sD.Find(L"(");
    ASSERT(nF > 0);
    CString sAddress = sD.Left(nF);
    dataSet.push_back(sAddress);
    CString sValue = sD.Right(sD.GetLength() - nF);
    sValue.Delete(0);
    sValue.Delete(sValue.GetLength() - 1);
    dataSet.push_back(sValue);

    ASSERT(dataSet.size() == 2);
    if (dataSet.size() == 2)
    {
      CString sAddr = dataSet[0];
      unsigned int uiAddr = CAi_Str::ToInt_sHex(sAddr);
      unsigned int uiReqAddr = CAi_Str::ToInt_sHex(sReqAddr) + i;

      if (uiAddr != uiReqAddr)
      {
        ASSERT(false); // ����� ����������� ������ ���������� �� ������ ����������� ������ 
        return false;
      }

      bool bRes = CheckAddress(uiAddr);
      if (bRes)
      {
        varMeaning_t meaning = dataSet[1];
        meaning.TrimLeft(L" ");
        meaning.TrimRight(L" ");

        bool bStringUnitType = false;
        DataSetEx ds;
        if (GetDataSetEx(&ds, uiAddr))
          bStringUnitType = (ds.tD.eValueType == TYPE_STRING);

        if (!meaning.IsEmpty()
            && !bStringUnitType) // ���� ��� ��� '������', �� ��������� ��� ����
        { // �����, �������� �����, ���� ����, � ��������� ����

          CString m = meaning;
          m.TrimRight(L"0");
          m.TrimRight(L".");
          m.TrimRight(L"0");
          if (m == L"-") // ������ ������ �������� ���� � �������: -0 ��� -0.0 � �.�.
            meaning.TrimLeft(L"-");
        }

        auto iter = m_receivedDataStore.find(uiAddr);
        if (iter == m_receivedDataStore.end())
          m_receivedDataStore.insert(pair<unsigned int, varMeaning_t>(uiAddr, meaning));
        else
          iter->second = meaning;
        bDataIsCorrect = true;
      }
    }
  }

  return bDataIsCorrect;
}

dataTypesAndVarMeanings_t* CDataStore::GetReceivedDataStore()
{
  return &m_receivedDataStore;
}

void CDataStore::ClearReceivedDataStore()
{
  m_receivedDataStore.clear();
}

void CDataStore::ClearReceivedDataStoreItem(unsigned int nAddress)
{
  auto iter = m_receivedDataStore.begin();
  for (size_t i = 0; i < m_receivedDataStore.size(); ++i, ++iter)
  {
    if (iter->first == nAddress)
    {
      iter->second = L"";
    }
  }
}