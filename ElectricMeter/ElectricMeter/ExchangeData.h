#pragma once

#include "StdAfx.h"

struct EXCH_DATA
{
  bool bResult = true;
  CString sSOHID;
  CString sData;
  int nDataID = 0; // nDataID == nCurrViewID == inlay id
  bool bConfirmation = false;
  CString sMsg;
  int nInnerErrCode = -1;
  DWORD nLastError = 0;

  void Clear()
  {
    bResult = false;
    sSOHID = L"";
    sData = L"";
    nDataID = 0;
    bConfirmation = false;
    sMsg = L"";
    nInnerErrCode = -1;
    nLastError = 0;
  }

  EXCH_DATA& operator=(const EXCH_DATA& e)
  {
    bResult = e.bResult;
    sSOHID = e.sSOHID;
    sData = e.sData;
    nDataID = e.nDataID;
    bConfirmation = e.bConfirmation;
    sMsg = e.sMsg;
    nInnerErrCode = e.nInnerErrCode;
    nLastError = e.nLastError;
    return *this;
  }
};