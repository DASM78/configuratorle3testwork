#include "stdafx.h"
#include "ElectricMeter.h"
#include "MainFrm.h"

using namespace std;
using namespace aux;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)

  // Create

  ON_WM_CREATE()
  ON_COMMAND(ID_VIEW_CUSTOMIZE, OnViewCustomize)
  ON_WM_CLOSE()
  ON_WM_DESTROY()
  ON_WM_SIZE()
  ON_WM_SHOWWINDOW()

  ON_MESSAGE(UM_ON_AFTER_NEW_DOCUMENT, OnAfterNewDocument)

  // Look

  ON_COMMAND_RANGE(ID_VIEW_APPLOOK_2000, ID_VIEW_APPLOOK_2007_4, OnAppLook)
  ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_2000, ID_VIEW_APPLOOK_2007_4, OnUpdateAppLook)
  
  ON_WM_SETCURSOR()
  ON_MESSAGE(UM_SET_CURSOR, OnSetCursor_)
  ON_MESSAGE(UM_SET_CURSOR2, OnSetCursor2_)

  // ToolBar (MnFrmMenu.cpp)

  ON_UPDATE_COMMAND_UI(ID_TBR_CONNECT, OnUpdateTbrConnect)
  ON_COMMAND(ID_TBR_CONNECT, OnTbrConnect)

  // Outlook bar (MnFrmOutlookBar.cpp)

  ON_COMMAND(ID_VIEW_OUTLOOKBAR, OnViewOutlookBar)
  ON_UPDATE_COMMAND_UI(ID_VIEW_OUTLOOKBAR, OnUpdateViewOutlookBar)
  ON_REGISTERED_MESSAGE(AFX_WM_CHANGE_ACTIVE_TAB, OnChangeActiveTab)
  ON_MESSAGE(UM_SELECT_ROW_IN_TAB, OnSelectRowInTab)
  ON_MESSAGE(UM_RESIZE_MSG_FROM_CVIEW, ResizeMsgFromCView)

  // Caption bar (MnFrmCaptionBar.cpp)

  ON_COMMAND(ID_VIEW_CAPTIONBAR, OnViewCaptionBar)
  ON_UPDATE_COMMAND_UI(ID_VIEW_CAPTIONBAR, OnUpdateViewCaptionBar)
  ON_COMMAND(ID_CAPTION_BTN_CONNECT, OnCaptionBtn_Connect)
  ON_COMMAND(ID_CAPTION_BTN_BREAK_CONNECTION, OnCaptionBtn_BreakConnection)
  ON_COMMAND(ID_CAPTION_BTN_DISCONNECT, OnCaptionBtn_Disconnect)
  ON_MESSAGE(ID_CAPTION_BTN_DISCONNECT, OnCaptionBtn_Disconnect)

  // Connection (exchange) with device (MnFrmDevConnector.cpp)

  ON_MESSAGE(UM_DO_EXCHANGE, OnDoExchangeByTimer)
  ON_MESSAGE(UM_EXCHAGE_RESULT_NOTIFY, OnExchangeResultNotify)
  ON_MESSAGE(UM_REFLECT_RESULT_NOTIFY, OnReflectByResultNotify)
  ON_MESSAGE(UM_UPDATE_ACCESS_DATA, UpdateAccessData)
  ON_MESSAGE(UM_READ_DATA_EVENT, ReadDataEvent)
  ON_MESSAGE(UM_WRITE_DATA_EVENT, WriteDataEvent)
  ON_MESSAGE(UM_EXCH_IDENTIFICATION_PASSED_OK, ExchIdentificationPassedOk)
  ON_MESSAGE(UM_RECONNECT_WITH_OTHER_SPEED, ReconnectWithOtherSpeed)

  // Status bar (MnFrmStatusBar.cpp)

  ON_COMMAND(ID_STATBAR_FIELD_DOCUMENT, OnDblClickToDocumentField)

  // Timer (MnFrmTimer.cpp)

  ON_WM_TIMER()

  // Inlays (MnFrmInlays.cpp)

  ON_MESSAGE(UM_RELOAD_CURRENT_VIEW, OnReloadCurrentView)

  // Data store (MnFrmData.cpp)

  ON_MESSAGE(UM_CLEAR_RECEIVED_DATA_STORE_ITEM, OnClearReceivedDataStoreItem)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSave)
  ON_COMMAND(ID_FILE_SAVE, OnSaveDocument)
  ON_COMMAND(ID_FILE_OPEN, OnOpenDocument)

  // XML file (MnFrmXML.cpp)

  ON_MESSAGE(UM_UPDATE_XML_BACKUP_FILE, UpdateXmlBackupFile)

END_MESSAGE_MAP()

CMainFrame::CMainFrame()
{
  m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_2007_3);
  FormExchBinData();
}

CMainFrame::~CMainFrame()
{
  FreeExchBinData();  
}

// ����������� ����� - ������� ������, ������ ������� ������ ���� �������. ��� ���������� �� ��� ����� � �������� OnTimer  ����������, ��� OnTimer
// �������� ������ ����� � ���� ��� ���� ���������. � ����� ��� ���������� ���������, �������� � �������� ��������� �� ����� �� ���������, �� ����������
// ����� �� ���������, ������� �����, ��� �� "���������" � ����� ��������
UINT CMainFrame::Timer250Proc (LPVOID arg)
{
	CMainFrame *pf = (CMainFrame *)arg;
	static int i = 0;
	while (1)
	{		
		::PostMessage (pf->m_hWnd, UM_DO_EXCHANGE, 0, 0);
		Sleep (10);
		if (++i == 25)
		{
			i = 0;
			pf->ActivateByTimer250 ();
			pf->ExchWatchDogTick ();
		}

		if (pf->m_bStopTimer)
			AfxEndThread (0);
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
    return -1;

  m_p250msThread = AfxBeginThread (Timer250Proc, this); // ���������� ������� 250 ��

  //////////////////////////////////////////////////////////////////
  // Look

  OnAppLook(m_nAppLook);

  CMFCToolBar::EnableQuickCustomization();

  // TODO: Define your own basic commands. Be sure, that each pulldown menu have at least one basic command.
  /*
  CList<UINT, UINT>	lstBasicCommands;

  lstBasicCommands.AddTail(ID_FILE_SAVE);
  lstBasicCommands.AddTail(ID_FILE_SAVE_AS);
  lstBasicCommands.AddTail(ID_APP_ABOUT);
  lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
  lstBasicCommands.AddTail(ID_VIEW_CUSTOMIZE);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2000);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_XP);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2003);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS2005);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_WIN_XP);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2007_1);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2007_2);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2007_3);
  lstBasicCommands.AddTail(ID_VIEW_APPLOOK_2007_4);
  */
  /*lstBasicCommands.AddTail (ID_VIEW_TOOLBARS);
  lstBasicCommands.AddTail (ID_FILE_NEW);
  lstBasicCommands.AddTail (ID_FILE_OPEN);
  lstBasicCommands.AddTail (ID_FILE_PRINT);
  lstBasicCommands.AddTail (ID_APP_EXIT);
  lstBasicCommands.AddTail (ID_EDIT_CUT);
  lstBasicCommands.AddTail (ID_EDIT_PASTE);
  lstBasicCommands.AddTail (ID_EDIT_UNDO);
*/

  //CMFCToolBar::SetBasicCommands (lstBasicCommands);

  if (!m_wndMenuBar.Create (this))
  {
    TRACE0("Failed to create menubar\n");
    return -1;      // fail to create
  }

  ////////////////////////////////////////////////////////////
  // Menu & Tool bar & Status bar
  // help: http://www.studfiles.ru/preview/1162311/page:9/

  m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC);

  // Detect color depth. 256 color toolbars can be used in the high or true color modes only (bits per pixel is > 8):
  CClientDC dc (this);
  BOOL bIsHighColor = dc.GetDeviceCaps (BITSPIXEL) > 8;

  UINT uiToolbarHotID = bIsHighColor ? IDB_TOOLBAR256 : 0;
  UINT uiToolbarColdID = bIsHighColor ? IDB_TOOLBARCOLD256 : 0;
  UINT uiMenuID = bIsHighColor ? IDB_MENU_IMAGES : 0;
  
  /*m_wndToolBar.SetSizes(CSize(24, 24), CSize(24, 24));
  m_wndToolBar.SetMenuSizes(CSize(22, 22), CSize(16, 16));

  if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT,
    WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
    !m_wndToolBar.LoadToolBar(IDR_MAINFRAME, uiToolbarColdID, uiMenuID, FALSE, 0, 0, uiToolbarHotID))
  {
    TRACE0("Failed to create toolbar\n");
    return -1;      // fail to create
  }*/

  CreateStatusBar();

  //m_wndToolBar.SetWindowText (_T("������ ������������"));
  // �������������� ��� ������ ���� ��������� ������� ������ � ���� ����������
  //m_wndMenuBar.EnableDocking(CBRS_ALIGN_TOP);
  //m_wndToolBar.EnableDocking(CBRS_ALIGN_TOP);
  //EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM | CBRS_ALIGN_RIGHT); // ���� �� ����� �������
  DockPane(&m_wndMenuBar); // ������� ���� ���� ��������� ���, ����������������� ���� DockPane
  //DockPane(&m_wndToolBar); // ������� ���� ���� ��������� ���, ����������������� ���� DockPane

  /////////////////////////////////////////////////////////////////////////////////////
  // OutlookBar

  if (!CreateOutlookBar())
  {
    TRACE0("Failed to create shortcuts bar\n");
    return -1;      // fail to create
  }
  m_wndOutlookBar.ShowOwnedPopups(FALSE); // �����. ��� �������� ��� ������������� ����, ������������� ��������� ����

  // Outlook bar is created and docking on the left side should be allowed.
  EnableDocking (CBRS_ALIGN_LEFT); // ���� ��� ����� ������� - ������ �����

  /////////////////////////////////////////////////////////////////////////////////////
  // Caption bar

  if (!m_wndCaptionBar.Create(WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS, this, ID_VIEW_CAPTIONBAR, 30))
  {
    TRACE0("Failed to create caption bar\n");
    return -1;      // fail to create
  }

  m_wndCaptionBar.SetFlatBorder();
  // ������ � caption bar
  //m_bmpCaption.LoadBitmap (IDB_CAPTION);
  //m_wndCaptionBar.SetBitmap (m_bmpCaption, RGB (255, 0, 255));

  ///////////////////////////////////////////////////////////////////////////////////////////
  // ��. ����; ���� ������ � ���� ��������� �� ��� �.�. ��������������
  //DockPane(&m_wndMenuBar);
  //DockPane(&m_wndToolBar);
  ///////////////////////////////////////////////////////////////////////////////////////////
  // ���� ������ ������� ����� ������ ����� ������������� ��� ��������� �������
  //m_wndToolBar.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, _T("Customize..."));
  ///////////////////////////////////////////////////////////////////////////////////////////

  StartExchThread(eProtocol61107);
  //SetTimer_(ID_TMR_EXCHANGE_WITH_DEVICE, 10);
  //SetTimer_(ID_TMR_250, 250); //moved to static thread
  SetCaptionBtnView(cbConnect);

  m_bMainFrameWasCreated = true;

  return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CFrameWndEx::PreCreateWindow(cs) )
    return FALSE;
  
  cs.style &= ~FWS_ADDTOTITLE;

  return TRUE;
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext)
{
  // TODO: Add your specialized code here and/or call the base class

  return CFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext);
}

void CMainFrame::OnClose()
{
  if (IsPrgState_OnlineDeviceData())
  {
    SetPrgState(psCloseAppAfterDisconnect);
    DisconnectionFromDevice();
    return;
  }

  StopExchThread();
  CFrameWndEx::OnClose();
}

void CMainFrame::OnCloseAfterDisconnection()
{
  StopExchThread();
  CFrameWndEx::OnClose();
}

void CMainFrame::OnDestroy()
{
  KillTimer_(ID_TMR_EXCHANGE_WITH_DEVICE);
  m_bStopTimer = true;
  CFrameWndEx::OnDestroy();
  FreeOutlookTab();  
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
  CFrameWndEx::OnSize(nType, cx, cy);
}

void CMainFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
  CFrameWndEx::OnShowWindow(bShow, nStatus);

  // Do it once

  static BOOL bUpdatedOnce = FALSE;
  if (bUpdatedOnce)
    return;
  bUpdatedOnce = TRUE;
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
  if (m_bMainFrameWasCreated)
  {
    m_wndCaptionBar.CheckButtonView(&pMsg->pt);
  }

  if (pMsg->message == WM_KEYDOWN)
  {
    if (m_bWaitCursor)
      return TRUE;

    // Shift
    bool bShift = false;
    SHORT bLS = GetAsyncKeyState(VK_LSHIFT);
    SHORT bRS = GetAsyncKeyState(VK_RSHIFT);
    if (bLS || bRS)
      bShift = true;

    // Ctrl
    bool bCtrl = false;
    SHORT bLC = GetAsyncKeyState(VK_LCONTROL);
    SHORT bRC = GetAsyncKeyState(VK_RCONTROL);
    if (bLC || bRC)
      bCtrl = true;

    if (bCtrl && pMsg->wParam == (int)'S')
    {
      OnSaveDocument();
      return TRUE;
    }

    if (bCtrl && pMsg->wParam == (int)'O')
    {
      OnOpenDocument();
      return TRUE;
    }
  }

  switch (pMsg->message)
  {
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONUP:
    case WM_RBUTTONDBLCLK:
    case WM_MBUTTONDOWN:
    case WM_MBUTTONUP:
    case WM_MBUTTONDBLCLK:
      if (m_bWaitCursor)
        return TRUE;
      break;
  }

  return CFrameWndEx::PreTranslateMessage(pMsg);
}

LRESULT CMainFrame::OnAfterNewDocument(WPARAM wParam, LPARAM lParam)
{
  ViewInitialisation((CView*)lParam);
  return TRUE;
}

void CMainFrame::SetProductVersion(CString sVer)
{
  m_sProductVersion = sVer;
  m_appEnviron.SetProductVersion(m_sProductVersion);
}

CString CMainFrame::GetProductVersion()
{
  return m_sProductVersion;
}

void CMainFrame::ShowApp()
{
  // Outlook
  //to do - start debug
  //!! - mark for search

#ifdef _DEBUG
  //ShowOutlookTab(ID_OUTLOOK_CONNECTION_TAB);
  //SelectSpecRowInOutlookTab(ID_OUTLOOK_CONNECTION_TAB, IDD_CONNECTION_SETTINGS);
  
  ShowOutlookTab(ID_OUTLOOK_DATA_TAB);
  SelectSpecRowInOutlookTab(ID_OUTLOOK_DATA_TAB, IDD_DATA_POWER_PROFILE);
  //IDD_DATA_EVENT_JOURNAL);
  // IDD_DATA_POWER_PROFILE


  //ShowOutlookTab(ID_OUTLOOK_DEVICE_SETTINGS_TAB);
  //SelectSpecRowInOutlookTab(ID_OUTLOOK_DEVICE_SETTINGS_TAB, IDD_SETTINGS_SWITCH_OFF_RELE);
#else
  ShowOutlookTab(ID_OUTLOOK_CONNECTION_TAB);
  SelectFirstRowInOutlookTab(ID_OUTLOOK_CONNECTION_TAB);
#endif

  // Caption

  CheckCaptionState();
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
  CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
  CFrameWndEx::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

CAppEnviron* CMainFrame::GetAppEnviron()
{
  return &m_appEnviron;
}

void CMainFrame::OnViewCustomize()
{
  //------------------------------------
  // Create a customize toolbars dialog:
  //------------------------------------
  CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog (this,
    TRUE /* Automatic menus scaning */
    );

  pDlgCust->Create ();
}

void CMainFrame::OnAppLook(UINT id)
{
  CDockingManager::SetDockingMode(DT_SMART);

  m_nAppLook = id;

  switch (m_nAppLook)
  {
    case ID_VIEW_APPLOOK_2000:
      // enable Office 2000 look:
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
      break;

    case ID_VIEW_APPLOOK_XP:
      // enable Office XP look:
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
      break;

    case ID_VIEW_APPLOOK_WIN_XP:
      // enable Windows XP look (in other OS Office XP look will be used):
      CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
      break;

    case ID_VIEW_APPLOOK_2003:
      // enable Office 2003 look:
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    case ID_VIEW_APPLOOK_VS2005:
      // enable VS.NET 2005 look:
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
      CMFCVisualManager::GetInstance();
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    case ID_VIEW_APPLOOK_2007_1:
      // enable Office 2007 look:
      CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    case ID_VIEW_APPLOOK_2007_2:
      // enable Office 2007 look:
      CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    case ID_VIEW_APPLOOK_2007_3:
      // enable Office 2007 look:
      CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    case ID_VIEW_APPLOOK_2007_4:
      // enable Office 2007 look:
      CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
      CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
      CDockingManager::SetDockingMode(DT_SMART);
      break;

    default:
      ASSERT(FALSE);
  }

  CDockingManager* pDockManager = GetDockingManager();
  if (pDockManager != nullptr)
  {
    ASSERT_VALID(pDockManager);
    pDockManager->AdjustPaneFrames();
  }

  RecalcLayout();
  RedrawWindow(nullptr, nullptr, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE);

  theApp.WriteInt(_T("ApplicationLook"), m_nAppLook);
}

void CMainFrame::OnUpdateAppLook(CCmdUI* pCmdUI)
{
  pCmdUI->SetRadio(m_nAppLook == pCmdUI->m_nID);
}

BOOL CMainFrame::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
  if (m_bWaitCursor)
  {
    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
    return TRUE;
  }
  return CFrameWndEx::OnSetCursor(pWnd, nHitTest, message);
}

LRESULT CMainFrame::OnSetCursor_(WPARAM wParam, LPARAM lParam)
{
  switch (wParam)
  {
    case ID_WAIT_CURSOR: m_bWaitCursor = (lParam == 1) ? true : false; break;
  }
  ::SetCursor(AfxGetApp()->LoadStandardCursor(m_bWaitCursor ? IDC_WAIT : IDC_ARROW));

  return TRUE;
}

LRESULT CMainFrame::OnSetCursor2_(WPARAM wParam, LPARAM lParam)
{
  m_bWaitCursor_oneWayCase = (lParam == 1) ? true : false;
  return OnSetCursor_(wParam, lParam);
}

void CMainFrame::SetPrgState(EProgramStates ps)
{
  // ������ � �������� �� ��������� ����������������� ���������

  vector <EProgramStates> delStates;
  delStates.push_back(psNone);

  switch (ps)
  {
    case psNone:
      m_programStates.clear();
      break;
    case psOfflineDeviceData:
      delStates.push_back(psOnlineDeviceData);
      break;
    case psConnectingToDevice:
      break;
    case psOnlineDeviceData:
      delStates.push_back(psConnectingToDevice);
      delStates.push_back(psOfflineDeviceData);
      break;
  }

  for (auto d : delStates)
  {
    for (size_t i = 0; i < m_programStates.size(); ++i)
    {
      if (m_programStates[i] == d)
      {
        m_programStates.erase(m_programStates.begin() + i);
        i--;
      }
    }
  }

  if (find(m_programStates.begin(), m_programStates.end(), ps) == m_programStates.end())
    m_programStates.push_back(ps);

  UpdateConnectionStateForCurrInlay();
}

bool CMainFrame::IsPrgState(EProgramStates dvs)
{
  if (find(m_programStates.begin(), m_programStates.end(), dvs) != m_programStates.end())
    return true;
  return false;
}

bool CMainFrame::IsPrgState_Begin()
{
  if (IsPrgState(psNone))
    return true;
  return false;
}

bool CMainFrame::IsPrgState_OnlineDeviceData()
{
  if (IsPrgState(psOnlineDeviceData))
    return true;
  return false;
}

bool CMainFrame::IsPrgState_ConnectingToDevice()
{
  if (IsPrgState(psConnectingToDevice))
    return true;
  return false;
}

bool CMainFrame::IsPrgState_OfflineDeviceData()
{
  if (IsPrgState(psOfflineDeviceData))
    return true;
  return false;
}

void CMainFrame::SetViewByDisconnect()
{
  if (IsPrgState_OnlineDeviceData())
    SetPrgState(psOfflineDeviceData);
  else
    SetPrgState(psNone);

  int nStatusIdx = GetStatusBarIndex(ID_STATBAR_FIELD_EXCHANGE_RESULT);
  CString sStatusMsg = L"�����: ";
  //m_wndStatusBar.SetPaneText(nStatusIdx, sStatusMsg);
}
