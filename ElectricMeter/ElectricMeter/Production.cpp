// Production.cpp : implementation file
//

#include "stdafx.h"
#include "ElectricMeter.h"
#include "Production.h"
#include "Defines.h"
#include "Resource.h"
#include "Ai_Font.h"
#include <string> 
#include <memory>
#include "ComSettDld.h"
#include "CounterInfo.h"
using namespace mdl;



// CProduction

IMPLEMENT_DYNCREATE (CProduction, CCommonViewForm)

const CProduction::RTYPE <eMeterType> CProduction::radioBtnTypes[] = {
	{ LE1, IDC_RADIO_TYPE_LE1 }, { LE3, IDC_RADIO_TYPE_LE3 } };
const CProduction::RTYPE <eMeterCase> CProduction::radioBtnCases[] = {
	{ P, IDC_RADIO_CASE_P }, { D, IDC_RADIO_CASE_D } };

const CProduction::RTYPE <eMeterAccuracy> CProduction::radioBtnAcc[] = {
	{ ACC_05, IDC_RADIO_ACCURACY05 }, { ACC_10, IDC_RADIO_ACCURACY1 } };
const CProduction::RTYPE <eMeterEnergyKind> CProduction::radioBtnEnergyKind[] = {
	{ ENERGY_A, IDC_RADIO_MEAS_ACTIVE }, { ENERGY_AR, IDC_RADIO_MEAS_ACTIVE_REACTIVE } };
const CProduction::RTYPE <eMeterNomU> CProduction::radioBtnNomU[] = {
	{ U1, IDC_RADIO_U1 }, { U2, IDC_RADIO_U2 }, { U3, IDC_RADIO_U3 }, { U4, IDC_RADIO_U4 } };
const CProduction::RTYPE <eMeterNomI> CProduction::radioBtnNomI[] = {
	{ I1, IDC_RADIO_I1 }, { I2, IDC_RADIO_I2 }, { I3, IDC_RADIO_I3 }, { I4, IDC_RADIO_I4 }, { I5, IDC_RADIO_I5 } };
const CProduction::RTYPE <eMeterOptions> CProduction::checkOptions[] = {
	{ OPT_B, IDC_CHECK_B },
	{ OPT_S, IDC_CHECK_S },
	{ OPT_C, IDC_CHECK_C },
	{ OPT_P, IDC_CHECK_P },
	{ OPT_M, IDC_CHECK_M },
};
const CProduction::COMB_TYPE CProduction::ifaceCombos[] = {
	{ IDC_COMBO_IFACE1, IDC_COMBO_IFACE_VER1, 0 },
	{ IDC_COMBO_IFACE2, IDC_COMBO_IFACE_VER2, 1 },
	{ IDC_COMBO_IFACE3, IDC_COMBO_IFACE_VER3, 2 },
	{ IDC_COMBO_IFACE4, IDC_COMBO_IFACE_VER4, 3 },
};

CProduction::CProduction ()
: CCommonViewForm (CProduction::IDD, L"CView_Production")
{	
	InitCfgStruct ();
}

CProduction::~CProduction ()
{
}

void CProduction::DoDataExchange (CDataExchange* pDX)
{
	CFormView::DoDataExchange (pDX);
	DDX_Control (pDX, IDC_DATETIMEPICKER, m_datetime);
	
	DDX_Control (pDX, IDC_EDIT_FW, m_editFW);
	DDX_Control (pDX, IDC_EDIT_HW, m_editHW);

}

BEGIN_MESSAGE_MAP (CProduction, CFormView)
	ON_BN_CLICKED (IDC_BTN_CLEAR_JOURNAL, &CProduction::OnBnClickedBtnClearJournal)
	ON_BN_CLICKED (IDC_RADIO_TYPE_LE1, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_TYPE_LE3, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_CASE_P, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_CASE_D, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_ACCURACY05, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_ACCURACY1, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_MEAS_ACTIVE, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_MEAS_ACTIVE_REACTIVE, &CProduction::OnAnyBnClicked)

	ON_BN_CLICKED (IDC_CHECK_B, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_CHECK_S, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_CHECK_C, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_CHECK_P, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_CHECK_M, &CProduction::OnAnyBnClicked)

	ON_BN_CLICKED (IDC_RADIO_U1, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_U2, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_U3, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_U4, &CProduction::OnAnyBnClicked)

	ON_BN_CLICKED (IDC_RADIO_I1, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_I2, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_I3, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_I4, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_RADIO_I5, &CProduction::OnAnyBnClicked)


	ON_CBN_SELCHANGE (IDC_COMBO_MODELE_NUMBER, &CProduction::OnAnyBnClicked)

	ON_CBN_SELCHANGE (IDC_COMBO_IFACE1, &CProduction::OnAnyBnClicked)
	//ON_CBN_SELCHANGE (IDC_COMBO_IFACE2, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE2, &CProduction::OnIfaceClicked)
	//ON_CBN_SELCHANGE (IDC_COMBO_IFACE3, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE3, &CProduction::OnIfaceClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE4, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE_VER1, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE_VER2, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE_VER3, &CProduction::OnAnyBnClicked)
	ON_CBN_SELCHANGE (IDC_COMBO_IFACE_VER4, &CProduction::OnAnyBnClicked)
	ON_EN_KILLFOCUS (IDC_EDIT_FW, &CProduction::OnAnyBnClicked)
	ON_EN_KILLFOCUS (IDC_EDIT_HW, &CProduction::OnAnyBnClicked)
	ON_BN_CLICKED (IDC_BTN_CLEAR_COUNTERS, &CProduction::OnBnClickedBtnClearCounters)
	ON_BN_CLICKED (IDC_BTN_WR_CNT_INFO, &CProduction::OnBnClickedBtnWrCntInfo)

	ON_BN_CLICKED (IDC_BTN_READ_INFO, &CProduction::OnBnClickedBtnReadInfo)
	ON_BN_CLICKED (IDC_BTN_SAVE, &CProduction::OnBnClickedBtnSave)
	ON_BN_CLICKED (IDC_BTN_LOAD, &CProduction::OnBnClickedBtnLoad)
	ON_BN_CLICKED(IDC_BTN_COM1, &CProduction::OnBnClickedBtnCom1)
	ON_EN_CHANGE(IDC_EDIT_SERAIL, &CProduction::OnEnChangeEditSerail)
	ON_BN_CLICKED(IDC_BTN_COM2, &CProduction::OnBnClickedBtnCom2)
	ON_BN_CLICKED(IDC_BTN_COM3, &CProduction::OnBnClickedBtnCom3)
END_MESSAGE_MAP()

void CProduction::BeforeDestroyClass ()
{

}

// CProduction diagnostics

#ifdef _DEBUG
void CProduction::AssertValid () const
{
	CFormView::AssertValid ();
}

#ifndef _WIN32_WCE
void CProduction::Dump (CDumpContext& dc) const
{
	CFormView::Dump (dc);
}
#endif
#endif //_DEBUG

void CProduction::UpdateCfgAsControls ()
{
	for (auto i : radioBtnTypes){
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.type = i.type;
	}
	for (auto i : radioBtnCases) {
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.meter_case = i.type;
	}
	for (auto i : radioBtnAcc) {
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.accuracy = i.type;
	}
	for (auto i : radioBtnEnergyKind) {
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.meterEnergyKind = i.type;
	}
	for (auto i : radioBtnNomU) {
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.nomU = i.type;
	}
	for (auto i : radioBtnNomI) {
		if (((CButton *)GetDlgItem (i.cID))->GetCheck ())
			m_cfg.nomI = i.type;
	}
	for (auto i : checkOptions) {
		((CButton *)GetDlgItem (i.cID))->GetCheck () ? m_cfg.options[static_cast <int> (i.type)] = true : m_cfg.options[static_cast <int> (i.type)] = false;
	}
	for (auto i : ifaceCombos)
	{
		m_cfg.sIface[i.idx].iFace = static_cast <eMeterIface> (((CComboBox *)GetDlgItem (i.idc_type))->GetCurSel ());
		m_cfg.sIface[i.idx].ver = static_cast <int> (((CComboBox *)GetDlgItem (i.idc_ver))->GetCurSel ());
	}
	m_cfg.model = static_cast<int> (((CComboBox *)GetDlgItem (IDC_COMBO_MODELE_NUMBER))->GetCurSel ());

	CString t;
	((CEdit *)GetDlgItem (IDC_EDIT_SERAIL))->GetWindowText (t);
	m_cfg.serNum = _ttoi (t);
	// get production date
	SYSTEMTIME date;

	static_cast <CDateTimeCtrl *> (GetDlgItem (IDC_DATETIMEPICKER))->GetTime (&date);
	//m_datetime.GetTime (&date);
	m_cfg.date.year = date.wYear;
	m_cfg.date.month = static_cast <unsigned char> (date.wMonth);
	m_cfg.date.day = static_cast <unsigned char> (date.wDay);

	m_cfg.fwVer = m_editFW.GetVal();
	m_cfg.hwVer = m_editHW.GetVal ();
	UpdateControlsAsCfg ();
}

void CProduction::UpdateControlsAsCfg (void)
{
	for (auto i : radioBtnTypes)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.type == i.type);
	for (auto i : radioBtnCases)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.meter_case == i.type);
	for (auto i : radioBtnAcc)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.accuracy == i.type);
	for (auto i : radioBtnEnergyKind)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.meterEnergyKind == i.type);

	for (auto i : radioBtnNomU)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.nomU == i.type);
	for (auto i : radioBtnNomI)
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.nomI == i.type);
	for (auto i : ifaceCombos)
	{
		((CComboBox *)GetDlgItem (i.idc_type))->SetCurSel (m_cfg.sIface[i.idx].iFace);
		((CComboBox *)GetDlgItem (i.idc_ver))->SetCurSel (m_cfg.sIface[i.idx].ver);
	}
	for (auto i : checkOptions)
	{
		((CButton *)GetDlgItem (i.cID))->SetCheck (m_cfg.options[static_cast <int> (i.type)]);
	}
	((CComboBox *)GetDlgItem (IDC_COMBO_MODELE_NUMBER))->SetCurSel (m_cfg.model);
	CString t;
	t.Format (_T ("%d"), m_cfg.serNum);
	((CEdit *)GetDlgItem (IDC_EDIT_SERAIL))->SetWindowText (t);
	CString strInfo = CCounterInfo::ProduceCounterName (m_cfg);	
	((CEdit *)GetDlgItem (IDC_COUNTER_INFO))->SetWindowText (strInfo);
	m_editFW.SetVal (m_cfg.fwVer);
	m_editHW.SetVal (m_cfg.hwVer);
	UpdateData (true);
}
// CProduction message handlers

void CProduction::OnInitialUpdate ()
{
	m_editFW.SubclassDlgItem (IDC_EDIT_FW, this);
	m_editHW.SubclassDlgItem (IDC_EDIT_HW, this);
	m_datetime.SubclassDlgItem (IDC_DATETIMEPICKER, this);	
	UpdateControlsAsCfg ();
}

void CProduction::UpdateParamMeaningsByReceivedDeviceData (mdl::teFrameTypes ft)
{
	//if (ft == FRAME_TYPE_DEV_INF_TYPE)
	//{
	//	varMeaning_t m = FindReceivedData (ft);
	//	((CEdit *)GetDlgItem (IDC_COUNTER_INFO))->SetWindowText (m);
	//	//	m_edtCounterInfo.SetWindowText (CString ("123"));
	//}

}

void CProduction::UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult)
{
	if (ft == FRAME_TYPE_COMMAND_CLEAR_JOURNAL)
	{
		bResult ? AfxMessageBox (L"������ ������� ������", MB_ICONINFORMATION) : AfxMessageBox (L"��������� �������. �� ���� ��������.", MB_ICONSTOP);
	}
	else if (ft == FRAME_TYPE_COMMAND_CLEAR_COUNTERS)
	{
		bResult ? AfxMessageBox (L"�������� ������� �������", MB_ICONINFORMATION) : AfxMessageBox (L"��������� �������. �� ���� ��������.", MB_ICONSTOP);
	}
	else if (ft == FRAME_TYPE_DEV_INF_STRUCT)
	{
		if (bResult)
		{
			AfxMessageBox (L"������������ �������� ��������, ����� �� ��������� ��������", MB_ICONINFORMATION);
			::PostMessage (GetGlobalParent (), ID_CAPTION_BTN_DISCONNECT, 0, 0);
		}
		else // err
		{
			AfxMessageBox (L"�� ���� �������� ������������, ��������� �������", MB_ICONSTOP);
		}
	}
}

void CProduction::OnBnClickedBtnClearJournal ()
{
	//TODO ������� ��� ��������
	BeginFormingSendPack (true_d (L"start"));
	SendDataToDevice (FRAME_TYPE_COMMAND_CLEAR_JOURNAL);
	BeginFormingSendPack (false_d (L"start"));
}



void CProduction::OnBnClickedBtnClearCounters ()
{
	BeginFormingSendPack (true_d (L"start"));
	SendDataToDevice (FRAME_TYPE_COMMAND_CLEAR_COUNTERS);
	BeginFormingSendPack (false_d (L"start"));
}



void CProduction::OnAnyBnClicked ()
{
	UpdateCfgAsControls ();
}

void CProduction::OnIfaceClicked ()
{

	int id = ((CComboBox *)GetDlgItem (IDC_COMBO_IFACE2))->GetCurSel ();
	if (id == IFACE_O)
		((CComboBox *)GetDlgItem (IDC_COMBO_IFACE2))->SetCurSel (IFACE_NO);
	id = ((CComboBox *)GetDlgItem (IDC_COMBO_IFACE3))->GetCurSel ();
	if (id == IFACE_O)
		((CComboBox *)GetDlgItem (IDC_COMBO_IFACE3))->SetCurSel (IFACE_NO);
	UpdateCfgAsControls ();
}


//
void CProduction::InitCfgStruct ()
{
	m_cfg.type = LE3;
	m_cfg.meter_case = P;

	m_cfg.accuracy = ACC_10;
	m_cfg.meterEnergyKind = ENERGY_AR;
	// ifaces
	for (auto& i : m_cfg.sIface) {
		i.iFace = IFACE_NO;
		i.ver = 1;
	}
	m_cfg.sIface[0].iFace = IFACE_O; // ������ ������ ������
	m_cfg.sIface[0].ver = 1;
	for (auto& i : m_cfg.options)
		i = false;
	m_cfg.options[static_cast <int> (OPT_B)] = true;
	m_cfg.options[static_cast <int> (OPT_S)] = true;
	m_cfg.nomU = U2;
	m_cfg.nomI = I4;
	m_cfg.model = 1;
	m_cfg.serNum = 1;
	m_cfg.fwVer = 1;
	m_cfg.hwVer = 1;
	for (int i = 0; i < sizeof (m_cfg.aComInfo) / sizeof (m_cfg.aComInfo[0]); i++)
	{
		m_cfg.aComInfo[i].speed = BR_9600;
		m_cfg.aComInfo[i].parity = PAR_ODD;
		m_cfg.aComInfo[i].bits = BITS7;
		m_cfg.aComInfo[i].packetInterval = IV_20;
		m_cfg.aComInfo[i].comAddr32 = 1;
		m_cfg.aComInfo[i].protocol = PROTO_61107;

	}
	char *apn = "internet.mts.ru";
	memcpy(&m_cfg.gsmSett.aApn, apn, strlen(apn) + 1);
	char *serverip = "snt.spb.ru";
	memcpy(&m_cfg.gsmSett.aServerIP, serverip, strlen(serverip) + 1);
	m_cfg.gsmSett.ipPort = 29000;
	char *dns = "8.8.8.8";
	memcpy(&m_cfg.gsmSett.aDnsIP, dns, strlen(dns) + 1);
	char *ussd = "*100#";
	memcpy(&m_cfg.gsmSett.ussdNum, ussd, strlen(ussd) + 1);
	


}

void CProduction::OnBnClickedBtnWrCntInfo ()
{
	UpdateCfgAsControls ();
	BeginFormingSendPack (true_d (L"start"));	
	CString str = CCounterInfo::CfgAsAscii (m_cfg);
	SendDataToDevice (FRAME_TYPE_DEV_INF_STRUCT, str);
	BeginFormingSendPack (false_d (L"start"));
}

void CProduction::OnBnClickedBtnReadInfo ()
{
	SetParamTypeForRead (FRAME_TYPE_DEV_INF_STRUCT, true_d (L"������� ������� - ����� ������� ��������� �� �����������"));
}

void CProduction::OnBnClickedBtnSave ()
{
	CString sFilter = L"���������� ����� (*.dat)|*.dat||||";
	CFileDialog dlg (FALSE, _T ("dat"), nullptr, OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
	CString sTitle = L"C��������� ������������ ��������";
	dlg.GetOFN ().lpstrTitle = sTitle.GetBuffer (_MAX_PATH);
	CString sFile = L"��-������������. ������������ ��������.dat";
	dlg.GetOFN ().lpstrFile = sFile.GetBuffer (_MAX_PATH);
	int nID = dlg.DoModal ();
	if (nID == IDOK)
	{
		UpdateCfgAsControls ();
		CFile file (sFile, CFile::modeCreate | CFile::modeWrite);
		file.Write (&m_cfg, sizeof(m_cfg));
		file.Close ();
	}
}

void CProduction::OnBnClickedBtnLoad ()
{
	CString sFilter = L"���������� ����� (*.dat)|*.dat||||";
	CFileDialog dlg (TRUE, _T ("xml"), nullptr,
					 OFN_CREATEPROMPT | OFN_OVERWRITEPROMPT, sFilter);
	dlg.GetOFN ().lpstrTitle = L"�������� ���������� �������� ����������";

	int nID = dlg.DoModal ();
	if (nID == IDOK)
	{
		CString sFileName = dlg.GetPathName ();
		CFile file (sFileName, CFile::modeRead);
		file.Read (&m_cfg, sizeof(m_cfg));
		file.Close ();
		UpdateControlsAsCfg ();
	}
}




void CProduction::OnEnKillfocusEditFw ()
{
	// TODO: Add your control notification handler code here
}


void CProduction::OnEnKillfocusEditHw ()
{
	// TODO: Add your control notification handler code here
}


void CProduction::OnBnClickedBtnCom1()
{
	CComSettDld dlg (m_cfg.aComInfo[0], true);
	if (dlg.DoModal() == IDOK)
	{
		m_cfg.aComInfo[0] = dlg.GetInfo();
	}

}


void CProduction::OnEnChangeEditSerail()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the __super::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CProduction::OnBnClickedBtnCom2()
{
	CComSettDld dlg(m_cfg.aComInfo[1]);
	if (dlg.DoModal() == IDOK)
	{
		m_cfg.aComInfo[1] = dlg.GetInfo();
	}	
}


void CProduction::OnBnClickedBtnCom3()
{
	CComSettDld dlg(m_cfg.aComInfo[2]);
	if (dlg.DoModal() == IDOK)
	{
		m_cfg.aComInfo[2] = dlg.GetInfo();
	}
}
