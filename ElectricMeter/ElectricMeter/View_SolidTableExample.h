#pragma once

#include "afxcview.h"
#include "Ai_ListCtrlEx.h"
#include "FormViewEx.h"

class CView_SolidTableExample: public CFormViewEx
{
  DECLARE_DYNCREATE(CView_SolidTableExample)

public:
  CView_SolidTableExample();
  virtual ~CView_SolidTableExample();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_SOLID_TABLE_EXAMPLE
  };

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  �Ai_ListCtrlEx* m_plstInfoEx = nullptr;

  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
  void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
};
