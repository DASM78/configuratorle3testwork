#pragma once

struct POWPROF_DATA_ROW
{
//#ifdef _DEBUG
  POWPROF_DATA_ROW()
  { }

  POWPROF_DATA_ROW(CString a, CString p, CString rp, CString qp, CString rm, CString qm, CString time, CString date)
    : sA(a)
    , sP(p)
    , sR_plus(rp)
    , sQ_plus(qp)
    , sR_minus(rm)
    , sQ_minus(qm)
    , sTimePeriod(time)
    , sDate(date)
  { }
//#endif

  CString sA;
  CString sP;
  CString sR_plus;
  CString sQ_plus;
  CString sR_minus;
  CString sQ_minus;
  CString sTimePeriod;
  CString sDate;
};