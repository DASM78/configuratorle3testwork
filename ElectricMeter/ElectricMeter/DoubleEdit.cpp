// DoubleEdit.cpp : implementation file
//

#include "stdafx.h"
#include "ElectricMeter.h"
#include "DoubleEdit.h"


// CDoubleEdit

IMPLEMENT_DYNAMIC(CDoubleEdit, CEdit)

CDoubleEdit::CDoubleEdit () : m_val (0)
{
	m_nLastSel = 0;
	precision = 1;
}

CDoubleEdit::~CDoubleEdit()
{
}


BEGIN_MESSAGE_MAP(CDoubleEdit, CEdit)
	ON_WM_CHAR ()
	ON_CONTROL_REFLECT (EN_UPDATE, OnUpdate)
	ON_EN_UPDATE (EN_UPDATE, OnUpdate)
END_MESSAGE_MAP()

// CDoubleEdit message handlers
void CDoubleEdit::OnChar (UINT nChar, UINT nRepCnt, UINT nFlags)
{
	GetWindowText (m_strPrevValue);
	m_nLastSel = GetSel ();
	CEdit::OnChar (nChar, nRepCnt, nFlags);	
}

void CDoubleEdit::OnUpdate ()
{
	CString str;
	GetWindowText (str);
	errno = 0;
	wchar_t* aEndPtr = nullptr;	
	double doubleValue = _tcstod (str, &aEndPtr);
	// �������� ���� ��� �������
	int len = str.GetLength();
	int validLen = aEndPtr - str.GetBuffer();
	if (validLen != len)
	{
		SetWindowText (m_strPrevValue);
		SetSel (m_nLastSel);
	}
}

