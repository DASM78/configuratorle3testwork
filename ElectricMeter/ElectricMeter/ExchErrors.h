#pragma once

#include "ErrorStore.h"

class CExchErrors: public CErrorStore
{
public:
  CExchErrors();
  ~CExchErrors();

private:
  virtual void FormErrorList() final;
};

