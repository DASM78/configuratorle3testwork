#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "Ai_ListCtrlEx.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Model_.h"

class CView_PowerMax: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_PowerMax)

public:
  CView_PowerMax();
  virtual ~CView_PowerMax();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_DATA_POWER_MAX
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  CCommonListCtrl m_lstPowerMax;
  �Ai_ListCtrlEx* m_plstPowerMaxEx = nullptr;
  int m_rdoShowEnergyAs = 0;

  void FormDB();
  void ClearTable();
  void FillTable();
  void ParseReceivedData(mdl::teFrameTypes ft, CString& s);

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();

  // Load

  CMFCButton m_btnLoad;
  bool m_bShowLoadingSuccessResultMsg = false;
  afx_msg void OnBnClickedBtnLoadPowFromDevice();
  virtual void DoAfterClearLoadData() override final;

  // Clear

  CMFCButton m_btnClear;
  afx_msg void OnBnClickedBtnClearPowInDevice();

  // Result exchange

  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
};
