#include "stdafx.h"
#include "CounterInfo.h"

#include <string>

using namespace mdl;
using namespace std;

const CCounterInfo::INFOTYPE <eMeterType> CCounterInfo::meterTypeInfo[] = {
	{ LE1, "LE-1" },
	{ LE3, "LE-3" },
};
const CCounterInfo::INFOTYPE <eMeterCase> CCounterInfo::meterCaseInfo[] = {
	{ P, "P" },
	{ D, "D" },
};
const CCounterInfo::INFOTYPE <eMeterAccuracy> CCounterInfo::meterAccuracyInfo[] = {
	{ ACC_05, "0.5" },
	{ ACC_10, "1.0" },
};
const CCounterInfo::INFOTYPE <eMeterEnergyKind> CCounterInfo::meterKindInfo[] = {
	{ ENERGY_A, "A" },
	{ ENERGY_AR, "AR" },
};
const CCounterInfo::INFOTYPE <eMeterIface> CCounterInfo::meterIfaceInfo[] = {

	{ IFACE_O, "O", false, _T("��������")},
	{ IFACE_E4, "E4", false, _T ("EIA 485") },
	{ IFACE_E2, "E2", false, _T ("EIA 232") },
	{ IFACE_RF, "RF", true, _T ("����������") },
	{ IFACE_PL, "PL", true,  _T ("PLC �����") },
	{ IFACE_PLRF, "PLRF", false, _T ("��������������� �����") },
	{ IFACE_GSM, "GSM", true, _T ("GSM-�����") },
	{ IFACE_MB, "MB", false, _T ("��������� M-Bus") },
	{ IFACE_ETH, "ETH", false, _T ("Ethernet") },
	{ IFACE_WF, "WF", false, _T ("WiFi") },
};

const CCounterInfo::INFOTYPE <eMeterOptions> CCounterInfo::meterOptionsInfo[] = {
	{ OPT_B, "B" },
	{ OPT_S, "S" },
	{ OPT_C, "C" },
	{ OPT_P, "P" },
	{ OPT_M, "M" },
};
const CCounterInfo::INFOTYPE <eMeterNomU> CCounterInfo::meterUInfo[] = {
	{ U1, "1" }, { U2, "2" }, { U3, "3" }, { U4, "4" },
};
const CCounterInfo::INFOTYPE <eMeterNomI> CCounterInfo::meterIInfo[] = {
	{ I1, "1" }, { I2, "2" }, { I3, "3" }, { I4, "4" }, { I5, "5" },
};

// ����������� ��������� ������������ � ascii ��� �������� �� UART
CString CCounterInfo::CfgAsAscii (const sCfg& cfg)
{
	CString str (_T (""));	
	const unsigned char* p = reinterpret_cast <const unsigned char *> (&cfg);
	auto toasc = [](unsigned char d) -> char {
		if (d < 10) return d + '0';
		else return d + '0' + 'A' - '9';
	};
	for (int i = 0; i < sizeof (cfg); i++)
	{
		unsigned char d = *p++;
		char high = toasc (d >> 4);
		char low = toasc (d & 0x0f);
		str += high;
		str += low;
	}
	return str;
}



CCounterInfo::CCounterInfo()
{
}


CCounterInfo::~CCounterInfo()
{
}

sCfg CCounterInfo::AsciiToCfg(CString m)
{
	sCfg cfg;
	auto tochar = [](char h, char l) -> unsigned char
	{
		h -= '0'; if (h > 9) h -= 'A' - '9';
		l -= '0'; if (l > 9) l -= 'A' - '9';
		return (h << 4) | l;
	};

	unsigned char* p = reinterpret_cast <unsigned char *> (&cfg);	
	for (int i = 0; i < sizeof (cfg); i++)
	{
		*p++ = tochar(m[2 * i], m[2 * i + 1]);
	}
	return cfg;
}

CString CCounterInfo::GetFieldNameFromeCfg (teFrameTypes fieldName, const sCfg& cfg)
{
	CString sU[] = {CString(_T("3x57.7/100V")),
		CString (_T ("3x230/400V")),
		CString (_T ("3x(120/208)V � 3x(230/400)V")),
		CString (_T ("3x(57.7)/100)V � 3x(230/400)V")),
	};
	CString sI[] = { CString (_T ("5(10) A")),
		CString (_T ("5(60) A")),
		CString (_T ("5(80) A")),
		CString (_T ("5(100) A")),
		CString (_T ("10(100) A")),
	};
	CString sProto[] = { CString (_T ("IEC61107-2011")) };
	CString sSpeed[] = {
		CString (_T ("200")), 
		CString (_T ("600")),
		CString (_T ("1200")),
		CString (_T ("2400")),
		CString (_T ("4800")),
		CString (_T ("9600")),
	};
	CString sBits[] = {
		CString (_T ("5")),
		CString (_T ("6")),
		CString (_T ("7")),
		CString (_T ("8")),
	};
	CString sPar[] = {
		CString (_T ("��� ��������")),
		CString (_T ("���")),
		CString (_T ("�����")),
	};

	CString sPulses1000  (_T ("1000 pulses | kW~h, 1000 pulses | kVar~h"));
	CString sPulses10000 (_T ("10000 pulses | kW~h, 10000 pulses | kVar~h"));
	
	CString str("");
	int idx = 0;
	int iNumber = 0;
	switch (fieldName)
	{
	case ROW_IDTYPE_DEV_INF_TYPE:
		str = ProduceCounterName (cfg);
		break;
	case ROW_IDTYPE_DEV_INF_SER_N:
		str.Format (_T ("%u"), cfg.serNum);
		break;
	case ROW_IDTYPE_DEV_INF_CR_DATE:
		str.Format (_T ("%02u.%02u.%.u"), cfg.date.day, cfg.date.month, cfg.date.year);
		break;
	case ROW_IDTYPE_DEV_INF_NOM_U:
		str = sU[cfg.nomU];
		break;
	case ROW_IDTYPE_DEV_INF_NOM_M_I:
		str = sI[cfg.nomI];
		break;
	case ROW_IDTYPE_DEV_INF_CONST:
		if (cfg.nomI == I1)
			str = sPulses10000;
		else str = sPulses1000;
		break;
	case ROW_IDTYPE_DEV_INF_R_OFF:
		str = cfg.options[OPT_C] ? CString (_T ("����")) : CString (_T ("���"));
		break;
	case ROW_IDTYPE_DEV_INF_COM1:		
	case ROW_IDTYPE_DEV_INF_COM2:
	case ROW_IDTYPE_DEV_INF_COM3:
	case ROW_IDTYPE_DEV_INF_COM4:		
		switch (fieldName)
		{
		case ROW_IDTYPE_DEV_INF_COM1:
			iNumber = 0;			
			break;
		case ROW_IDTYPE_DEV_INF_COM2:
			iNumber = 1;
			break;
		case ROW_IDTYPE_DEV_INF_COM3:
			iNumber = 2;
			break;
		case ROW_IDTYPE_DEV_INF_COM4:
			iNumber = 3;
			break;
		}
		idx = cfg.sIface[iNumber].iFace;
		str = meterIfaceInfo[idx].longDesc;
		if (meterIfaceInfo[idx].isVer)
		{
			CString ver;
			ver.Format (_T (", ������ %u"), cfg.sIface[iNumber].ver);
			str += ver;
		}
		break;
	case ROW_IDTYPE_DEV_INF_COM1_PR:
	case ROW_IDTYPE_DEV_INF_COM2_PR:
	case ROW_IDTYPE_DEV_INF_COM3_PR:
	case ROW_IDTYPE_DEV_INF_COM4_PR:
		switch (fieldName)
		{
		case ROW_IDTYPE_DEV_INF_COM1_PR:
			iNumber = 0;
			break;
		case ROW_IDTYPE_DEV_INF_COM2_PR:
			iNumber = 1;
			break;
		case ROW_IDTYPE_DEV_INF_COM3_PR:
			iNumber = 2;
			break;
		case ROW_IDTYPE_DEV_INF_COM4_PR:
			iNumber = 3;
			break;
		default:
			break;
		}
		if (cfg.sIface[iNumber].iFace == IFACE_E2 || cfg.sIface[iNumber].iFace == IFACE_E4 || cfg.sIface[iNumber].iFace == IFACE_O) {
			str.Format(_T("%s,%s ���,%s���,��������-%s, �����-%u"), sProto[cfg.aComInfo[iNumber].protocol]
				, sSpeed[cfg.aComInfo[iNumber].speed]
				, sBits[cfg.aComInfo[iNumber].bits]
				, sPar[cfg.aComInfo[iNumber].parity]
				, cfg.aComInfo[iNumber].comAddr32
			);
		}
		break;
	case ROW_IDTYPE_DEV_INF_VER_SW:
		str.Format (_T ("%u"), cfg.fwVer);
		break;
	case ROW_IDTYPE_DEV_INF_VER_FW: // FW ��� hard
		str.Format (_T ("%u"), cfg.hwVer);
		break;

	default:
		break;
	}
	return str;
}

CString CCounterInfo::ProduceCounterName (const sCfg& cfg)
{

	CString str(_T(""));
	string name = "";
	for (auto i : meterTypeInfo)
	{
		if (cfg.type == i.type)
			name += i.desc;
	}

	for (auto i : meterCaseInfo)
	{
		if (cfg.meter_case == i.type)
			name += i.desc;
	}

	name += to_string(cfg.model);
	name += " ";
	for (auto i : meterAccuracyInfo)
	{
		if (cfg.accuracy == i.type)
			name += i.desc;
	}

	for (auto i : meterKindInfo)
	{
		if (cfg.meterEnergyKind == i.type)
			name += i.desc;
	}
	name += " ";

	bool bNonEmptyFound = false;
	for (int ix = 0; ix < MAX_IFACES; ix++)
	{
		for (auto i : meterIfaceInfo)
		{
			if (cfg.sIface[ix].iFace == i.type && i.type != IFACE_O)
			{
				if (bNonEmptyFound)
					name += ".";
				bNonEmptyFound = true;
				name += i.desc;
				if (i.isVer)
				{
					name += to_string(cfg.sIface[ix].ver);
				}
			}
		}
	}
	if (!bNonEmptyFound)
		name += "O"; // so ditry
	name += " ";
	for (int i = 0; i < OPT_MAX; i++)
	{
		if (cfg.options[i])
			name += meterOptionsInfo[i].desc;
	}
	name += " ";
	for (auto i : meterUInfo)
	{
		if (cfg.nomU == i.type)
			name += i.desc;
	}
	for (auto i : meterIInfo)
	{
		if (cfg.nomI == i.type)
			name += i.desc;
	}
	return CString (name.c_str ());
}


