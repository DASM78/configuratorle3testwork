#include "stdafx.h"
#include "XmlTagCollection.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CXmlTagCollection::CXmlTagCollection()
{
}

CXmlTagCollection::~CXmlTagCollection()
{
}

CString xTag::sAppMainRoot = L"LE_Configurator";
CString xTag::sVersions = L"XmlVersions";
CString xTag::sVersionsAttr_Product = L"LE_ConfiguratorApp";
CString xTag::sVersionsAttr_CurrXml = L"CurrXmlFile";
CString xTag::sInlays = L"Inlays";

