#include "stdafx.h"
#include "Resource.h"
#include "Connection_Task_InterfaceChoice_USB.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CConnection_Task_InterfaceChoice_USB, CDialogEx)

CConnection_Task_InterfaceChoice_USB::CConnection_Task_InterfaceChoice_USB()
//	: CDialogEx(CConnection_Task_InterfaceChoice_USB::IDD, pParent)
{
}

CConnection_Task_InterfaceChoice_USB::~CConnection_Task_InterfaceChoice_USB()
{
}

void CConnection_Task_InterfaceChoice_USB::DoDataExchange(CDataExchange* pDX)
{
  CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CConnection_Task_InterfaceChoice_USB, CDialogEx)
END_MESSAGE_MAP()
