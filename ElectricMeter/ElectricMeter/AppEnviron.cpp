#include "stdafx.h"
#include "Ai_File.h"
#include "Ai_Str.h"
#include "Ai_Registry.h"
#include "resource.h"
#include "AppEnviron.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CAppEnviron::CAppEnviron()
{
  CheckElectricMeterFolder();
  CheckAppWorkingFolder(L"");
  CheckAppSettingsFolder();

  //m_xmlSettings.CheckSettingsFile(m_strSettingsFolder, L"LE_Configurator.xml", L"LE_Configurator_backup.xml");
  TRACE(L"\n\n\n***********************************************************************************\n\n\
                                                                           ����� ������ �� ElectricMeter_backup.xml \n\n\
 ***********************************************************************************\n\n");

  CString sSettingsXmlFile = m_strSettingsFolder + L"\\��-������������_backup.xml";
  m_xmlSettings.SetBackupFile(sSettingsXmlFile);
  m_xmlSettings.LoadTreeFromFile(sSettingsXmlFile);

  //m_xmlSettings.CheckSettingsFile(m_strSettingsFolder, L"��-������������_backup.xml", L"��-������������_backup.xml");

  //CreateMainIniFile(L"LE_Configurator.ini");
}

CAppEnviron::~CAppEnviron()
{
}

void CAppEnviron::CheckElectricMeterFolder() throw(...)
{
  m_strElectricMeterTmpFolder = m_strElectricMeterFolder = CAi_File::GetWinLocalSettingsApplicationDataPath();
  if (m_strElectricMeterFolder.IsEmpty())
    throw false;
  m_strElectricMeterFolder += L"\\LE_Configurator";
  if (!CAi_File::ExamineFolder(m_strElectricMeterFolder))
    throw false;
  m_strElectricMeterTmpFolder += L"\\Tmp";
  if (!CAi_File::ExamineFolder(m_strElectricMeterTmpFolder))
    throw false;
}

void CAppEnviron::CheckAppWorkingFolder(CString strApplicationName) throw(...)
{
  CString strPathToWorking = m_strElectricMeterFolder + L"\\" + strApplicationName;
  if (!CAi_File::ExamineFolder(strPathToWorking))
    throw false;
  m_strWorkingFolder = strPathToWorking;
}

void CAppEnviron::CheckAppSettingsFolder() throw(...)
{
  CString strPathToWinAppSett = m_strWorkingFolder + L"Application Settings";
  if (!CAi_File::ExamineFolder(strPathToWinAppSett))
    throw false;
  m_strSettingsFolder = strPathToWinAppSett;
}

void CAppEnviron::SetProductVersion(CString sVer)
{
  m_xmlSettings.SetProductVersion(sVer);
}

void CAppEnviron::CreateMainIniFile(CString strFileName) throw(...)
{
  CString strIniFilePath = m_strSettingsFolder + L"\\" + strFileName;
  if (!CAi_File::ExamineFile(strIniFilePath))
    throw false;
  m_strAppIniFile = strIniFilePath;
}

CString CAppEnviron::GetValue_s(CString sValueName, CString sDef)
{
  ASSERT(!m_sCurrBlock.IsEmpty());
  wchar_t sData[MAX_PATH]{};
  GetPrivateProfileString(m_sCurrBlock, sValueName, sDef, sData, MAX_PATH - 1, m_strAppIniFile);
  return CString(sData);
}

void CAppEnviron::SetValue_s(CString sValueName, CString sValue)
{
  ASSERT(!m_sCurrBlock.IsEmpty());
  WritePrivateProfileString(m_sCurrBlock, sValueName, sValue, m_strAppIniFile);
}

int CAppEnviron::GetValue_i(CString sValueName, int nDef)
{
  ASSERT(!m_sCurrBlock.IsEmpty());
  return (int)GetPrivateProfileInt(m_sCurrBlock, sValueName, nDef, m_strAppIniFile);
}

void CAppEnviron::SetValue_i(CString sValueName, int nValue)
{
  ASSERT(!m_sCurrBlock.IsEmpty());
  WritePrivateProfileString(m_sCurrBlock, sValueName, CAi_Str::ToString(nValue), m_strAppIniFile);
}

void SaveDataToRegEdit(GET_REG_PARAM& gp)
{
  CAi_Registry reg;
  GET_REG_PARAM gpCheck = gp;

  CAi_Registry::GetRegResult result = reg.GetRegParam(&gpCheck);
  if (result != CAi_Registry::grrSuccess)
    reg.CreateAndSetValue(&gp);
  else
    if (result == CAi_Registry::grrSuccess)
      reg.SetValue(&gp);
}

CString GetDataFromRegEdit(GET_REG_PARAM& gp)
{
  CString sMean = gp.strMean;
  gp.strMean = L"";
  CAi_Registry reg;
  CAi_Registry::GetRegResult result = reg.GetRegParam(&gp);
  if (result != CAi_Registry::grrSuccess)
  {
    gp.strMean = sMean;
    reg.CreateAndSetValue(&gp);
    return sMean;
  }
  return gp.strMean;
}

void CAppEnviron::SaveAccessProp(REG_EDIT_ACCESS_DATA& ac)
{
  CString sRegEditAppName;
  sRegEditAppName.LoadString(IDS_REG_EDIT_APP_NAME);
  GET_REG_PARAM gp;
  gp.hKey = HKEY_CURRENT_USER;
  gp.strSubKey = L"Software\\" + sRegEditAppName + L"\\Properties";
  gp.nType = REG_SZ;

  gp.strParamName = L"CurrentLogin";
  CString sCurrLogin = ac.sCurrLogin;
  if (sCurrLogin.IsEmpty())
    sCurrLogin = L"User";
  gp.strMean = sCurrLogin;
  SaveDataToRegEdit(gp);

  gp.strParamName = L"UserPassword";
  CString sUserPassword = ac.sUserPassword;
  if (sUserPassword.IsEmpty())
    sUserPassword = L"User";
  gp.strMean = sUserPassword;
  SaveDataToRegEdit(gp);

  gp.strParamName = L"AdminPassword";
  CString sAdminPassword = ac.sAdminPassword;
  if (sAdminPassword.IsEmpty())
    sAdminPassword = L"Admin";
  gp.strMean = sAdminPassword;
  SaveDataToRegEdit(gp);
}

void CAppEnviron::GetAccessProp(REG_EDIT_ACCESS_DATA& ac)
{
  ac.sCurrLogin = L"User";
  ac.sUserPassword = L"User";
  ac.sAdminPassword = L"Admin";

  CString sRegEditAppName;
  sRegEditAppName.LoadString(IDS_REG_EDIT_APP_NAME);
  GET_REG_PARAM gp;
  gp.hKey = HKEY_CURRENT_USER;
  gp.strSubKey = L"Software\\" + sRegEditAppName + L"\\Properties";
  gp.nType = REG_SZ;

  gp.strParamName = L"CurrentLogin";
  gp.strMean = ac.sCurrLogin;
  ac.sCurrLogin = GetDataFromRegEdit(gp);

  gp.strParamName = L"UserPassword";
  gp.strMean = ac.sUserPassword;
  ac.sUserPassword = GetDataFromRegEdit(gp);

  gp.strParamName = L"AdminPassword";
  gp.strMean = ac.sAdminPassword;
  ac.sAdminPassword = GetDataFromRegEdit(gp);
}