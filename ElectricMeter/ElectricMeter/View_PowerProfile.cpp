#include "stdafx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Ai_Font.h"
#include "Defines.h"
#include "View_PowerProfile.h"
#include <functional>

using namespace std;
using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CTabCtrlOvrdZ, CTabCtrlOvrd)

IMPLEMENT_DYNCREATE(CView_PowerProfile, CCommonViewForm)

namespace ns_Inlay_PowerProfile
{
#define UID_GRAPH_TAB 0
#define UID_TABLE_TAB 1

  struct X_TAGS
  {
    CString sPowerProfile = L"PowerProfile";
    CString sViewNode = L"View";
    CParamsTreeNode* pViewNode = nullptr;
    CString sViewNodeAttr_Energy = L"Energy";
    CString sViewNodeAttr_A = L"A";
    CString sViewNodeAttr_R_plus = L"R_plus";
    CString sViewNodeAttr_R_minus = L"R_minus";
    CString sZoomX = L"ZoomX";
    CString sZoomY = L"ZoomY";
  }
  g_xTags;

} // ns_Inlay_PowerProfile

using namespace ns_Inlay_PowerProfile;

CTabCtrlOvrdZ::CTabCtrlOvrdZ(CView_PowerProfile* pPP)
  : m_pPP(pPP)
{
}

CTabCtrlOvrdZ::~CTabCtrlOvrdZ()
{
}

CView_PowerProfile::CView_PowerProfile()
  : CCommonViewForm(CView_PowerProfile::IDD, L"CView_PowerProfile")
  , m_tabs(this)
{
}

CView_PowerProfile::~CView_PowerProfile()
{
  if (m_plstTableEx)
  {
    m_plstTableEx->Destroy();
    delete m_plstTableEx;
  }
  if (m_plstTable)
    delete m_plstTable;

  if (m_pGraphView)
    delete m_pGraphView;

  if (m_pbtnPlusH)
    delete m_pbtnPlusH;
  if (m_pbtnPlusV)
    delete m_pbtnPlusV;
  if (m_pbtnMinusH)
    delete m_pbtnMinusH;
  if (m_pbtnMinusV)
    delete m_pbtnMinusV;
  if (m_pcbxZoomH)
    delete m_pcbxZoomH;
  if (m_pcbxZoomV)
    delete m_pcbxZoomV;
}

BEGIN_MESSAGE_MAP(CTabCtrlOvrdZ, CTabCtrlOvrd)
  ON_BN_CLICKED(ID_BTN_ZOOM_H_PLUS, &CTabCtrlOvrdZ::OnBnClickedZoomHPlus)
  ON_BN_CLICKED(ID_BTN_ZOOM_H_MINUS, &CTabCtrlOvrdZ::OnBnClickedZoomHMinus)
  ON_BN_CLICKED(ID_BTN_ZOOM_V_PLUS, &CTabCtrlOvrdZ::OnBnClickedZoomVPlus)
  ON_BN_CLICKED(ID_BTN_ZOOM_V_MINUS, &CTabCtrlOvrdZ::OnBnClickedZoomVMinus)
  ON_CBN_SELCHANGE(ID_CBX_ZOOM_H, &CTabCtrlOvrdZ::OnCbxSelchangeZoomH)
  ON_CBN_SELCHANGE(ID_CBX_ZOOM_V, &CTabCtrlOvrdZ::OnCbxSelchangeZoomV)
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(CView_PowerProfile, CCommonViewForm)
  ON_REGISTERED_MESSAGE(AFX_WM_CHANGE_ACTIVE_TAB, OnChangeActiveTab)
  ON_BN_CLICKED(IDC_BTN_LOAD_POWER_PROFILE_FROM_DEV, &CView_PowerProfile::OnBnClickedBtnLoadPowerProfileFromDevice)
  ON_BN_CLICKED(IDC_BTN_CLEAR_POW_PROFILE_IN_DEVICE, &CView_PowerProfile::OnBnClickedBtnClearPowerProfileInDevice)
  ON_BN_CLICKED(IDC_RDO_POWPROF_ENERGY, &CView_PowerProfile::OnBnClickedRdoPowprofEnergy)
  ON_BN_CLICKED(IDC_RDO_POWPROF_POWER, &CView_PowerProfile::OnBnClickedRdoPowprofPower)
  ON_BN_CLICKED(IDC_CHK_POWPROF_A, &CView_PowerProfile::OnBnClickedChkPowprofA)
  ON_BN_CLICKED(IDC_CHK_POWPROF_R_PLUS, &CView_PowerProfile::OnBnClickedChkPowprofRPlus)
  ON_BN_CLICKED(IDC_CHK_POWPROF_R_MINUS, &CView_PowerProfile::OnBnClickedChkPowprofRMinus)

  //ON_BN_CLICKED(ID_BTN_ZOOM_H_PLUS, &CView_PowerProfile::OnBnClickedZoomHPlus)
END_MESSAGE_MAP()

void CView_PowerProfile::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STC_POWER_PROFILE_TAB_SPACE, m_stcTabSpace);
  DDX_Control(pDX, IDC_BTN_LOAD_POWER_PROFILE_FROM_DEV, m_btnLoadPowerProfileFromDevice);
  DDX_Control(pDX, IDC_BTN_CLEAR_POW_PROFILE_IN_DEVICE, m_btnClearPowerProfileInDevice);
  DDX_Control(pDX, IDC_STC_BEGIN_POWER_PROF_INTERVAL, m_stcBeginInterval);
  DDX_Control(pDX, IDC_STC_END_POWER_PROF_INTERVAL, m_stcEndInterval);
  DDX_Control(pDX, IDC_RDO_POWPROF_ENERGY, m_rdoEnergyView);
  DDX_Control(pDX, IDC_RDO_POWPROF_POWER, m_rdoPowerView);
  DDX_Control(pDX, IDC_STC_GRAPH_SELECT, m_stcGraphSelect);
  DDX_Control(pDX, IDC_CHK_POWPROF_A, m_chkA);
  DDX_Control(pDX, IDC_CHK_POWPROF_R_PLUS, m_chkR_plus);
  DDX_Control(pDX, IDC_CHK_POWPROF_R_MINUS, m_chkR_minus);
  DDX_Control(pDX, IDC_STC_POWPROF_A_COLOR, m_stcA_color);
  DDX_Control(pDX, IDC_STC_POWPROF_R_PLUS_COLOR, m_stcR_plus_color);
  DDX_Control(pDX, IDC_STC_POWPROF_R_MINUS_COLOR, m_stcR_minus_color);
  DDX_Control(pDX, IDC_STC_GR_VIEW_SEP, m_stcGrViewSep);
  
}

void CView_PowerProfile::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CRect rectTab;
  m_stcTabSpace.GetWindowRect(&rectTab);
  ScreenToClient(&rectTab);

  m_tabs.Create(CMFCTabCtrl::STYLE_3D_ROUNDED, rectTab, this, IDC_TAB_POWER_PROFILE, CMFCTabCtrl::LOCATION_TOP);

  CCommonViewDealer* pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plstTable = new CCommonListCtrl(&m_plstTableEx, pCmnViewDlr);
  m_plstTable->UseMultiRowColumns();
  CreateView_InGraphView();

  int nListStyle = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT | WS_BORDER | WS_TABSTOP;
  m_plstTable->Create(nListStyle, CRect(0, 0, 0, 0), &m_tabs, IDC_TAB_POWER_PROFILE_TABLE);

  m_tabs.SetImageList(IDB_POWER_PROFILE_TABS, 16, RGB(255, 0, 255));

  m_tabs.AddTab(m_pGraphView, L"������", 0);
  m_tabs.AddTab(m_plstTable, L"�������", 1);

  CMFCTabCtrl::Location location = CMFCTabCtrl::LOCATION_TOP;
  m_tabs.SetLocation(location);
  m_tabs.EnableAutoColor(FALSE);
  m_tabs.RecalcLayout();
  
  AddCtrlForPositioning_Stretchable(&m_stcTabSpace);
  AddCtrlForPositioning_Stretchable(&m_tabs);

  CreateTableTab();

  CAi_Font::CreateFont_(this, m_biggerFont, L"", FW_BOLD);
  m_stcBeginInterval.SetFont(&m_biggerFont);
  m_stcEndInterval.SetFont(&m_biggerFont);

  WatchForCtrState(&m_btnLoadPowerProfileFromDevice, rightCommon);
  WatchForCtrState(&m_btnClearPowerProfileInDevice, rightAdmin);

  CheckMeaningsViewByConnectionStatus();

  TakeValueFromXmlTree();
  m_chkA.SetCheck((m_currPPPV_A == ecppv2A_on) ? 1 : 0);
  m_chkR_plus.SetCheck((m_currPPPV_R_plus == ecppv2R_plus_on) ? 1 : 0);
  m_chkR_minus.SetCheck((m_currPPPV_R_minus == ecppv2R_minus_on) ? 1 : 0);
  switch (m_currPPPV_Energy)
  {
    case ecppvEnergy: OnBnClickedRdoPowprofEnergy(); break;
    case ecppvPower: OnBnClickedRdoPowprofPower(); break;
    default: ASSERT(false);
  }

  FillDataOnTabs();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_PowerProfile::BeforeDestroyClass()
{
}

void CView_PowerProfile::OnSize_()
{
  if (!m_tabs.m_hWnd)
    return;
  m_tabs.RecalcLayout();
  m_tabs.RedrawWindow();

  if (m_pGraphView)
  {
    CRect r{};
    m_tabs.GetClientRect(r);
    r.top += m_tabs.GetTabsHeight() + 2;
    r.left += 2;
    r.right -= 2;
    r.bottom -= 2;
    m_pGraphView->MoveWindow_(r);
  }

  OnSize_Tab_Graph();
}

void CView_PowerProfile::OnDraw(CDC* pDC)
{
}

bool CView_PowerProfile::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_PowerProfile::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_PowerProfile::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_PowerProfile::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_PowerProfile::TakeValueFromXmlTree()
{
  CParamsTreeNode* pInlaysNode = GetXmlNode();
  CString sTag = g_xTags.sPowerProfile;
  m_pPowerProfileNode = pInlaysNode->FindFirstNode(sTag);
  if (!m_pPowerProfileNode)
    m_pPowerProfileNode = pInlaysNode->AddNode(sTag);

  m_currPPPV_Energy = ecppvEnergy;
  m_currPPPV_A = ecppv2A_on;
  m_currPPPV_R_plus = ecppv2R_plus_on;
  m_currPPPV_R_minus = ecppv2R_minus_on;
  sTag = g_xTags.sViewNode;
  g_xTags.pViewNode = m_pPowerProfileNode->FindFirstNode(sTag);
  // ���� ���� ����� � ����� ��������� ���: <View/>, ��� ��������� � ���������� ������������:
  // <View Energy="1" A="1" R_plus="1", R_minus="1"/>
  if (!g_xTags.pViewNode)
    g_xTags.pViewNode = m_pPowerProfileNode->AddNode(sTag);
  else
  {
    function<void(CString, int&, int, int)> lmbInit = [=](CString s, int& dst, int m1, int m2) ->void
    {
      CString sValue = L"1";
      sValue = g_xTags.pViewNode->GetAttributeValue(s);
      if (sValue.IsEmpty())
        sValue = L"1";
      if (sValue != L"0" && sValue != L"1") // �� ������ ���� �� xml ��������� �����
        sValue = L"1";
      dst = (sValue == L"1") ? m1 : m2;
    };

    lmbInit(g_xTags.sViewNodeAttr_Energy, (int&)m_currPPPV_Energy, (int)ecppvEnergy, (int)ecppvPower);
    lmbInit(g_xTags.sViewNodeAttr_A, (int&)m_currPPPV_A, (int)ecppv2A_on, (int)ecppv2A_off);
    lmbInit(g_xTags.sViewNodeAttr_R_plus, (int&)m_currPPPV_R_plus, (int)ecppv2R_plus_on, (int)ecppv2R_plus_off);
    lmbInit(g_xTags.sViewNodeAttr_R_minus, (int&)m_currPPPV_R_minus, (int)ecppv2R_minus_on, (int)ecppv2R_minus_off);
  }

  SetZoomX(g_xTags.pViewNode->GetAttributeValue(g_xTags.sZoomX));
  SetZoomY(g_xTags.pViewNode->GetAttributeValue(g_xTags.sZoomY));
}

void CView_PowerProfile::PutValueToXmlTree()
{
  g_xTags.pViewNode->DeleteAllAttributes();
  
  CString sValue = (m_currPPPV_Energy == ecppvEnergy) ? L"1" : L"0";
  g_xTags.pViewNode->AddAttribute(g_xTags.sViewNodeAttr_Energy, sValue);
  sValue = (m_currPPPV_A == ecppv2A_on) ? L"1" : L"0";
  g_xTags.pViewNode->AddAttribute(g_xTags.sViewNodeAttr_A, sValue);
  sValue = (m_currPPPV_R_plus == ecppv2R_plus_on) ? L"1" : L"0";
  g_xTags.pViewNode->AddAttribute(g_xTags.sViewNodeAttr_R_plus, sValue);
  sValue = (m_currPPPV_R_minus == ecppv2R_minus_on) ? L"1" : L"0";
  g_xTags.pViewNode->AddAttribute(g_xTags.sViewNodeAttr_R_minus, sValue);
  g_xTags.pViewNode->AddAttribute(g_xTags.sZoomX, GetZoomX());
  g_xTags.pViewNode->AddAttribute(g_xTags.sZoomY, GetZoomY());

  UpdateXmlBackupFile();
}

LRESULT CView_PowerProfile::OnChangeActiveTab(WPARAM wparam, LPARAM lparam)
{
  m_nCurrPowerProfileTabIdx = (int)wparam;
  Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bSetVariable_d);
  BOOL bAR = TRUE;

  switch (m_nCurrPowerProfileTabIdx)
  {
    case UID_TABLE_TAB: bAR = FALSE; break;
    case UID_GRAPH_TAB: break;
  }
  m_nCurrPowerProfileTabIdx_prev = m_nCurrPowerProfileTabIdx;

  int nShow = bAR ? SW_SHOW : SW_HIDE;

  m_stcGraphSelect.ShowWindow(nShow);
  m_stcGrViewSep.ShowWindow(nShow);
  m_stcA_color.ShowWindow(nShow);
  m_stcR_plus_color.ShowWindow(nShow);
  m_stcR_minus_color.ShowWindow(nShow);
  m_rdoEnergyView.ShowWindow(nShow);
  m_rdoPowerView.ShowWindow(nShow);
  m_chkA.ShowWindow(nShow);
  m_chkR_plus.ShowWindow(nShow);
  m_chkR_minus.ShowWindow(nShow);

  m_pbtnPlusH->ShowWindow(nShow);
  m_pbtnMinusH->ShowWindow(nShow);
  m_pbtnPlusV->ShowWindow(nShow);
  m_pbtnMinusV->ShowWindow(nShow);
  m_pcbxZoomH->ShowWindow(nShow);
  m_pcbxZoomV->ShowWindow(nShow);

  CheckView();

  return 0;
}

void CView_PowerProfile::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_PowerProfile::CheckMeaningsViewByConnectionStatus()
{
  if (!IsConnection())
  {
    if (!IsTestViewData())
      m_grhData.AnalisysData(nullptr);
  }

  UpdateMonitoringCtrlView(&m_stcBeginInterval);
  UpdateMonitoringCtrlView(&m_stcEndInterval);

  CheckMeaningsViewByConnectionStatus_InTableTbl();
  CheckMeaningsViewByConnectionStatus_InGraphView();
}

void CView_PowerProfile::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);

  if (ft == FRAME_TYPE_COMMAND_ENERGY_INTERVALS_RESET)
  {
    SetWaitCursor_answer(false);

    if (m == L"\xF")
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      AfxMessageBox(L"������ ������� ������ �� �������� ��������� � ��������!", MB_ICONERROR);
      return;
    }

    AfxMessageBox(L"������ �� �������� ��������� ������� � �������!");
    return;
  }

  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� �������� ���������!", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_INTERVAL_ENERGY_CNT)
  {
    UpdMeaningsByRecvdDeviceData_InTableTbl(ft);

    if (IsReqItems_ForTableTbl())
    {
      BeginFormingReadPack(true);
      FormAllReqsOfItems_ForTableTbl();
      BeginFormingReadPack(false);
    }
    else
    {
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������� �� �������� ������ �� �������� ���������!", MB_ICONINFORMATION);
    }
  }

  if (ft >= FRAME_TYPE_INTERVAL_ENERGY_001 && ft <= FRAME_TYPE_INTERVAL_ENERGY_171)
  {
    UpdMeaningsByRecvdDeviceData_InTableTbl(ft);

    if (IsNewData_InTableTbl())
    {
      FillDataOnTabs();

      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ �� �������� ��������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }
  }
}

void CView_PowerProfile::FillDataOnTabs()
{
  CString sBegin = L"?";
  CString sEnd = L"?";
  GetTimeIntervals_FromTableTbl(sBegin, sEnd);

  m_stcBeginInterval.SetWindowText(sBegin);
  m_stcEndInterval.SetWindowText(sEnd);

  FillIn_InTableTbl();
  if (!IsTestViewData())
  {
    vector<POWPROF_DATA_ROW>* p = GetReceivedDataList_FromTableTbl();
    CreateGraph_InGraphView(p);
  }
}

void CView_PowerProfile::OnBnClickedBtnLoadPowerProfileFromDevice()
{
  ClearAllReqDataInDB_ForTableTbl();
  // ����� ������� ���������� �-� DoAfterClearLoadData
}

void CView_PowerProfile::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);

  ClearUsingCtrls();
  ClearAllRows_InTableTbl();
  ClearTestViewData();
  ReceiveData_ForTableTbl();

  BeginFormingReadPack(false);
}

void CView_PowerProfile::OnBnClickedBtnClearPowerProfileInDevice()
{
  if (AfxMessageBox(L"�������� ������ �� �������� ��������� � �������?", MB_ICONQUESTION | MB_OKCANCEL) != IDOK)
    return;

  SetWaitCursor_oneWay(true);

  ClearAllRows_InTableTbl();
  ClearUsingCtrls();

  BeginFormingSendPack(true_d(L"start"));
  SendDataToDevice(FRAME_TYPE_COMMAND_ENERGY_INTERVALS_RESET);
  BeginFormingSendPack(false_d(L"start"));
}

void CView_PowerProfile::ClearUsingCtrls()
{
  SetZoomX(L"0");
  SetZoomY(L"0");
  PutValueToXmlTree();
  m_grhData.AnalisysData(nullptr);

  CString sBegin = L"?";
  CString sEnd = L"?";
  m_stcBeginInterval.SetWindowText(sBegin);
  m_stcEndInterval.SetWindowText(sEnd);
}

void CView_PowerProfile::OnBnClickedRdoPowprofEnergy()
{
  m_rdoPowerView.SetCheck(0);
  m_rdoEnergyView.SetCheck(1);
  m_currPPPV_Energy = ecppvEnergy;
  PutValueToXmlTree();
  CheckView();
}

void CView_PowerProfile::OnBnClickedRdoPowprofPower()
{
  m_rdoEnergyView.SetCheck(0);
  m_rdoPowerView.SetCheck(1);
  m_currPPPV_Energy = ecppvPower;
  PutValueToXmlTree();
  CheckView();
}

void CView_PowerProfile::CheckView()
{
  switch (m_nCurrPowerProfileTabIdx)
  {
    case UID_GRAPH_TAB: CheckView_InGraphView();  break;
    case UID_TABLE_TAB: CheckView_InTableTbl();  break;
  }
}

void CView_PowerProfile::OnBnClickedChkPowprofA()
{
  m_currPPPV_A = m_chkA.GetCheck() ? ecppv2A_on : ecppv2A_off;
  PutValueToXmlTree();
  CheckView();
}

void CView_PowerProfile::OnBnClickedChkPowprofRPlus()
{
  m_currPPPV_R_plus = m_chkR_plus.GetCheck() ? ecppv2R_plus_on : ecppv2R_plus_off;
  PutValueToXmlTree();
  CheckView();
}

void CView_PowerProfile::OnBnClickedChkPowprofRMinus()
{
  m_currPPPV_R_minus = m_chkR_minus.GetCheck() ? ecppv2R_minus_on : ecppv2R_minus_off;
  PutValueToXmlTree();
  CheckView();
}
