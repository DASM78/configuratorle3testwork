#include "StdAfx.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CMainFrame::SetTimer_ (UINT_PTR nIDEvent, UINT uElapse)
{
	SetTimer (nIDEvent, uElapse, nullptr);
}

void CMainFrame::KillTimer_ (UINT_PTR nIDEvent)
{
	KillTimer (nIDEvent);
}

void CMainFrame::OnTimer (UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case ID_TMR_CONNECTION_INDICATOR:
		PushIndicator ();
		break;

	case ID_TMR_EXCHANGE_WITH_DEVICE:
		::PostMessage (m_hWnd, UM_DO_EXCHANGE, 0, 0);
		break;

	case ID_TMR_250:
		//moved to static thread
		//ActivateByTimer250();
		// ExchWatchDogTick();
		break;
	}
}
