#include "stdafx.h"
#include "Defines.h"
#include "Ai_Time.h"
#include "PowerProfileGraphsData.h"

using namespace std;
using namespace Gdiplus;

namespace ns_GrD
{
  map<EPowerProfileType, vector<CUT_POINT>> data;
  double g_fMaxValue = 0.0;
  double g_fUnitsInPixel = 0.0;

  void InsertMark(CString& sDay, CString sTime, int n)
  {
    CUT_POINT p;
    p.sDate = sDay;
    p.sTime = sTime;
    p.nY = n;
    p.fY = (double)n;
    data[pptA].push_back(p);
    data[pptR_plus].push_back(p);
    data[pptR_minus].push_back(p);
    data[pptP].push_back(p);
    data[pptQ_plus].push_back(p);
    data[pptQ_minus].push_back(p);
  }

} // namespace ns_GrD

CPowerProfileGraphsData::CPowerProfileGraphsData()
{
}

CPowerProfileGraphsData::~CPowerProfileGraphsData()
{
}

map<EPowerProfileType, vector<CUT_POINT>>* CPowerProfileGraphsData::GetDataPtr()
{
  if (ns_GrD::data.empty())
    return nullptr;

  return &ns_GrD::data;
}

// if nullptr - clear all data
void CPowerProfileGraphsData::AnalisysData(vector<POWPROF_DATA_ROW>* p)
{
  ns_GrD::data.clear();
  if (!p)
    return;
  if (p->empty())
    return;
  
  vector<POWPROF_DATA_ROW> d = *p; // ������ �����, �.�. ����� �������� �����
  // � 00:00 (����� ���) �� 25:59,
  // ������ ��� ��� ������ CTime (������� ������� ������������ �����) 00:00 ��� ������ ��� 
  p = &d;

  bool bResult = true;
  CString sErrorMsg;
  int nErrorRow = -1;

  // ��������� �� ����� ���� � ������� ������ CTime

  auto lmbTime = [&](CString& sDate, CString& sTime)->CTime
  {
    auto lmbCnv = [](CString& s)->int
    {
      return CAi_Str::ToInt(s);
    };

    // �� �������� ���� ���:
    //    ����: 17-08-2017
    //    ��������� ��������: 21:26

    vector<CString> dateStr;
    if (CAi_Str::CutString(sDate, L"-", &dateStr)
        && dateStr.size() == 3)
    {
      vector<CString> timeStr;
      if (CAi_Str::CutString(sTime, L":", &timeStr)
          && timeStr.size() == 2)
      {
        return CTime(lmbCnv(dateStr[2]), lmbCnv(dateStr[1]),
                     lmbCnv(dateStr[0]), lmbCnv(timeStr[0]), lmbCnv(timeStr[1]), 0);
      }
      else
        sErrorMsg = L"������������ ������ � ��������.";
    }
    else
      sErrorMsg = L"������������ ������ � �����.";

    bResult = false;
    return CTime(0);
  };

  // �������� ���� �� ������������, ���� ���� ������, ��� �������������

  vector<pair<size_t, __time64_t>> lstTimes; // �������� ������; <������ � �������� ������, �����>

  for (size_t i = 0; i < p->size(); ++i)
  {
    POWPROF_DATA_ROW& d_i = p->at(i);

    if (d_i.sTimePeriod == L"00:00")
      d_i.sTimePeriod = L"23:59";

    CTime time_i(lmbTime(d_i.sDate, d_i.sTimePeriod));
    if (!bResult)
    {
      AfxDebugBreak();
      continue;
    }
    lstTimes.push_back(pair<size_t, __time64_t>(i, time_i.GetTime()));
  }

  // ���������� ������� ������� ������� �������� �� �����������
  
  if (!lstTimes.empty())
  {
    for (size_t i = 0; i < lstTimes.size() - 1; ++i)
    {
      for (size_t j = 0; j < lstTimes.size() - i - 1; ++j)
        if (lstTimes[j].second > lstTimes[j + 1].second)
          swap(lstTimes[j], lstTimes[j + 1]); // ������ �������� �������
    }
  }

  // ��������� ������ �� ����. ���� ��� ��������� - ������ �����

  vector<pair<CString, vector<size_t>>> lstDays; // <����, ������� � �������� ������>
  CString sDatePrev;
  vector<size_t> day_points;
  vector<size_t>* pDay_points = nullptr;

  for (size_t i = 0; i < lstTimes.size(); ++i)
  {
    size_t& nSrcIdx = lstTimes[i].first;
    POWPROF_DATA_ROW& d_i = p->at(nSrcIdx);
    CString sDate = d_i.sDate; // ����, � ��� �� ����
    if (i == 0) // ������ ���� ������� for
    {
      sDatePrev = sDate;
      lstDays.push_back(pair<CString, vector<size_t>>(sDate, day_points));
      pDay_points = &lstDays[lstDays.size() - 1].second;
    }
    
    if (sDatePrev != sDate) // ���� ���� �� ����� - ������� ��������� ����
    {
      sDatePrev = sDate;
      size_t& nSrcIdxPrev = lstTimes[i - 1].first;
      POWPROF_DATA_ROW& d_prev = p->at(nSrcIdxPrev);
      CString sStartTime = L"00:00"; // ����� �� ����� ������� ���������� ���� � __time64_t ����������� CTime
      // ����������� CTime(sData �� ������, sStartTime)
      __time64_t nTime_day_prev = lmbTime(d_prev.sDate, sStartTime).GetTime();
      __time64_t nTime_day = lmbTime(d_i.sDate, sStartTime).GetTime();
      __time64_t nTime_1Day = 86400; // 60 * 60 * 24;
      if ((nTime_day_prev + nTime_1Day) != nTime_day) // ��������� ���� ���� �� �� ������� - ��������� ����/���
        lstDays.push_back(pair<CString, vector<size_t>>(L"Break", day_points)); // ������� ����� ����������
      lstDays.push_back(pair<CString, vector<size_t>>(sDate, day_points));
      pDay_points = &lstDays[lstDays.size() - 1].second;
    }

    pDay_points->push_back(nSrcIdx);
  }

  // ������ ������� ��� �� �����������. ����� �������������� �� ����� ���������

  #define COUNT_HALF_HOURS_PER_DAY 49
  #define COUNT_HALF_HOURS_PER_DAY_ 48

  int nOneMinute = 60; // ������
  int nHalfHour = 60 * 30;
  __time64_t hlf_hr[COUNT_HALF_HOURS_PER_DAY] {};
  int hh30 = 0;
  for (; hh30 < (COUNT_HALF_HOURS_PER_DAY - 1); ++hh30)
    hlf_hr[hh30] = nHalfHour * hh30;
  hlf_hr[hh30] = hlf_hr[hh30 - 1] + (nHalfHour - nOneMinute); // ������ ��� ��������� ������� ����� - 23:59, ����
  // � �������� �������� ��� 00:00

  double fMax = 0;

  for (size_t i = 0; i < lstDays.size(); ++i)
  {
    vector<size_t> pointsByHalfHours[COUNT_HALF_HOURS_PER_DAY_]; // ���������� ��������� �� ����,
    // ��������� ����� (��� ������ ����) ��� ������� pointsByHalfHours[i] ����� ���� ��������, ��
    // �������� �������, ����� ����� � �������� ��������� ��������, �.�. � �������:
    // 01.12.17 05:30 ... �������� ...
    // 01.12.17 05:32 ... �������� ...
    // 01.12.17 05:57 ... �������� ...
    // ����� pointsByHalfHours[i] ����� �������� �� �� ������ ��������, � ����� �� ����� ��������� ����� 
    // ������� ������������ �������� ��� ������� �������

    CString& sDay = lstDays[i].first;
    if (sDay != L"Break")
    {
      CString sStartTime = L"00:00"; // ������ ���
      CTime timeDay_begin = lmbTime(sDay, sStartTime); // CTime (� ������ __time64_t) ����������� ��� ��
      // ������ sDay, ����� � __time64_t ����� ����������� 30 ����� (1800) � ��������������� - ���� ��
      // ����� ����� � ������ �� ����; ���� ����� ������� � ������ ��������, �� �������� �����
      // CUT_MARK_HALF_HOUR_SPACE
      __time64_t nTimeDay_begin = timeDay_begin.GetTime(); // ������ ��� � ���� __time64_t
      vector<size_t>& srcIdxs = lstDays[i].second; // �������� ��������� �� ��������� ����

      for (size_t j = 0; j < srcIdxs.size(); ++j)
      {
        size_t& nSrcIdx = srcIdxs[j]; // ������� ����� �������� �� ����
        __time64_t nTime_point = 0;
        for (auto& lsttimes : lstTimes)
        {
          if (lsttimes.first == nSrcIdx)
          {
            nTime_point = lsttimes.second; // �� ������� �������� ������� ������ item �� �������� ������
            break;
          }
        }
        ASSERT(nTime_point);
#ifdef _DEBUG
        CTime ttmp(nTime_point);
        CString sTtmp = ttmp.Format(L"%d-%m-%Y;%H:%M");
        sTtmp = sTtmp;
#endif

        __time64_t nTime_point_ = nTime_point - nTimeDay_begin;
        int nHalfHourIdx = 1;
        __time64_t nTime_point_prev = 0;
        for (; nHalfHourIdx < COUNT_HALF_HOURS_PER_DAY; ++nHalfHourIdx)
        {
          // ����������� � ������ �������� ����������� ����� �� �������� ������

          if (nTime_point_ > nTime_point_prev && nTime_point_ <= hlf_hr[nHalfHourIdx])
            break;
          nTime_point_prev = hlf_hr[nHalfHourIdx];
        }
        --nHalfHourIdx;
        if (nHalfHourIdx >= COUNT_HALF_HOURS_PER_DAY_) // ���� ����� �� ���������, �� ����� ���� �� ���������
        {
          AfxDebugBreak();
          continue;
        }
        pointsByHalfHours[nHalfHourIdx].push_back(nSrcIdx);
      }
    }

    // ���������� ��������� �������

    for (size_t j = 0; j < COUNT_HALF_HOURS_PER_DAY - 1; ++j)
    {
      if (sDay == L"Break")
      {
        ns_GrD::InsertMark(sDay, L"00:01", CUT_MARK_DAYS_SPACE); // 00:00
        break;
      }

      tm tM{};
      tm* pTm = CTime(hlf_hr[j]).GetGmtTm(&tM);
      CString sTime;
      sTime.Format(L"%.2d:%.2d", pTm->tm_hour, pTm->tm_min);
      vector<size_t>& hh = pointsByHalfHours[j];
      if (hh.empty()) // ������� �� �������� - �������� �����
      {
        ns_GrD::InsertMark(sDay, sTime, CUT_MARK_HALF_HOUR_SPACE);
        continue;
      }

      if (hh.size() == 1) // � ��� ������ hh.size() > 1 (����� ����� ����� �� �������) ��������� ������������ ����
      {
        size_t& nSrcIdx = hh[0];
        POWPROF_DATA_ROW& dSrc = p->at(nSrcIdx);
        CUT_POINT c;
        c.nIdxInSouce = nSrcIdx;
        ASSERT(dSrc.sDate == sDay);
        c.SetData(sDay, sTime, false_d(L"����� �� �����"));

        fMax = max(fMax, c.SetValue(dSrc.sA));
        ns_GrD::data[pptA].push_back(c);
        fMax = max(fMax, c.SetValue(dSrc.sR_plus));
        ns_GrD::data[pptR_plus].push_back(c);
        fMax = max(fMax, c.SetValue(dSrc.sR_minus));
        ns_GrD::data[pptR_minus].push_back(c);
        fMax = max(fMax, c.SetValue(dSrc.sP));
        ns_GrD::data[pptP].push_back(c);
        fMax = max(fMax, c.SetValue(dSrc.sQ_plus));
        ns_GrD::data[pptQ_plus].push_back(c);
        fMax = max(fMax, c.SetValue(dSrc.sQ_minus));
        ns_GrD::data[pptQ_minus].push_back(c);
        continue;
      }

      size_t& nSrcIdx = hh[0];
      size_t bestValue[6] = { nSrcIdx, nSrcIdx, nSrcIdx, nSrcIdx, nSrcIdx, nSrcIdx };
      auto lmbCmp = [](CString& s1, CString& s2)->bool
      {
        double fY1 = CAi_Str::ToFloat(s1);
        double fY2 = CAi_Str::ToFloat(s2);
        return (fY1 > fY2);

      };
      for (size_t h = 1; h < hh.size(); ++h) // � ������� ��������� ������� � ���������� ��������
      {
        size_t& nSrcIdx = hh[h];
        POWPROF_DATA_ROW& dSrc = p->at(nSrcIdx);
        bestValue[0] = lmbCmp(dSrc.sA, p->at(bestValue[0]).sA) ? nSrcIdx : bestValue[0];
        bestValue[1] = lmbCmp(dSrc.sR_plus, p->at(bestValue[1]).sR_plus) ? nSrcIdx : bestValue[1];
        bestValue[2] = lmbCmp(dSrc.sR_minus, p->at(bestValue[2]).sR_minus) ? nSrcIdx : bestValue[2];
        bestValue[3] = lmbCmp(dSrc.sP, p->at(bestValue[3]).sP) ? nSrcIdx : bestValue[3];
        bestValue[4] = lmbCmp(dSrc.sQ_plus, p->at(bestValue[4]).sQ_plus) ? nSrcIdx : bestValue[4];
        bestValue[5] = lmbCmp(dSrc.sQ_minus, p->at(bestValue[5]).sQ_minus) ? nSrcIdx : bestValue[5];
      }

      CUT_POINT c;
      c.nIdxesInSouce = hh;
      size_t& nSrcIdx0 = hh[0];
      POWPROF_DATA_ROW& dSrc0 = p->at(nSrcIdx0);
      ASSERT(dSrc0.sDate == sDay);
      c.SetData(sDay, sTime, true_d(L"����� ���������"));

      fMax = max(fMax, c.SetValue(p->at(bestValue[0]).sA));
      c.nIdxInSouce = bestValue[0];
      ns_GrD::data[pptA].push_back(c);
      fMax = max(fMax, c.SetValue(p->at(bestValue[1]).sR_plus));
      c.nIdxInSouce = bestValue[1];
      ns_GrD::data[pptR_plus].push_back(c);
      fMax = max(fMax, c.SetValue(p->at(bestValue[2]).sR_minus));
      c.nIdxInSouce = bestValue[2];
      ns_GrD::data[pptR_minus].push_back(c);
      fMax = max(fMax, c.SetValue(p->at(bestValue[3]).sP));
      c.nIdxInSouce = bestValue[3];
      ns_GrD::data[pptP].push_back(c);
      fMax = max(fMax, c.SetValue(p->at(bestValue[4]).sQ_plus));
      c.nIdxInSouce = bestValue[4];
      ns_GrD::data[pptQ_plus].push_back(c);
      fMax = max(fMax, c.SetValue(p->at(bestValue[5]).sQ_minus));
      c.nIdxInSouce = bestValue[5];
      ns_GrD::data[pptQ_minus].push_back(c);
    }

  }

  ns_GrD::g_fMaxValue = fMax;
}

void CPowerProfileGraphsData::FormPointsByHeight(int nHeight)
{
  // ������� �����. - ������� � ����� ������� ������

  double fMax = (ns_GrD::g_fMaxValue == 0.0) ? 1.0 : ns_GrD::g_fMaxValue;
  double fPixelsInOneUnit = nHeight / fMax;
  ns_GrD::g_fUnitsInPixel = fMax / nHeight;

  // ������� ��� �������� ����������� �����. � ������� � �������� ��� �������� 1�1

  for (auto& type : ns_GrD::data)
  {
    for (auto& p : type.second)
    {
      if (!p.sDate.IsEmpty())
      {
        p.fY_ = static_cast<REAL>(fPixelsInOneUnit * p.fY);
        p.nY_ = (int)(fPixelsInOneUnit * p.fY);
      }
    }
  }
}

void CPowerProfileGraphsData::GetDataValue(double& fMax, double& fUnitsInPixel)
{
  fMax = ns_GrD::g_fMaxValue;
  fUnitsInPixel = ns_GrD::g_fUnitsInPixel;
}