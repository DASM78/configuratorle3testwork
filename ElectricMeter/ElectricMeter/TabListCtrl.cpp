#include "stdafx.h"
#include <vector>
#include "Resource.h"
#include "Ai_ListCtrlEx.h"
#include "TabListCtrl.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CTabListCtrl::CTabListCtrl(CWnd* pParentMng, int id)
  : m_pParentMng(pParentMng)
  , m_nID(id)
{
}

CTabListCtrl::~CTabListCtrl()
{
}

BEGIN_MESSAGE_MAP(CTabListCtrl, CMFCListCtrl)
  ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CTabListCtrl::OnNMCustomdrawLst)
  ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, &CTabListCtrl::OnLvnItemchangedLst)
END_MESSAGE_MAP()

void CTabListCtrl::Free()
{
  m_pListEx->Destroy();
  delete m_pListEx;
}

int CTabListCtrl::GetID()
{
  return m_nID;
}

void CTabListCtrl::OnNMCustomdrawLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  // ���������� �������, ����� ��� �������������� ���������� �������.
  *pResult = CDRF_DODEFAULT;
  LPNMLVCUSTOMDRAW pNMCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
  int nRow = pNMCD->iSubItem;
  int nCell = -1;

  if ((CDDS_ITEMPREPAINT | CDDS_SUBITEM) == pNMCD->nmcd.dwDrawStage)
  {
    nRow = pNMCD->nmcd.dwItemSpec;
    nCell = pNMCD->iSubItem;
    if (nCell < 0)
      return;
  }
  if (nRow < 0)
    return;
  int nCntRows = GetItemCount();
  if (nCntRows == 0)
    return;
  /*if (IsUpdatingView()) // �������� ������������ �������� ������� ����� (��������, ������������ ����) - ���� ��� �� ����������, �� ��������� ���
  return;*/

  bool bSelected = false;
  vector<int> selectedViewItems;
  m_pListEx->GetSelectedItems(&selectedViewItems);
  for (size_t i = 0; i < selectedViewItems.size(); ++i)
  {
    if (selectedViewItems[i] == nRow)
    {
      bSelected = true;
      break;
    }
  }

  int nColorBk = -1;
  //if (m_bInterlacedColorScheme)
  {
    if (nRow % 2)
      //nColorBk = RGB(0x9A, 0xAE, 0xC9);
      nColorBk = RGB(232, 255, 233);
    else
      nColorBk = RGB(0xC0, 0xD9, 0xFB);
  }
  ASSERT(m_pListEx);
  m_pListEx->OnCustomDrawEx(pNMHDR, pResult, nColorBk, RGB(20, 20, 200));

  /*if (bSelected)
  {
    nColorBk = RGB(0xCC, 0xFF, 0xCC);
    pNMCD->clrTextBk = nColorBk;
    *pResult |= CDRF_DOERASE;
    return;
  }*/

  if (bSelected)
  {
    bool bUpdateAgain = false;

    // unselect previous

    if (m_nPrevBoldRowText_inView != -1
        && m_nPrevBoldRowText_inView != nRow)
    {
      CellTextStyle st = m_pListEx->GetCellTextStyle(m_nPrevBoldRowText_inView + 1, m_columns.nDescr);
      if (st != ctsNone)
      {
        m_pListEx->SetCellTextStyle(m_nPrevBoldRowText_inView + 1, m_columns.nDescr, ctsNone);
        bUpdateAgain = true;
      }
    }

    // select and save new


    CellTextStyle st = m_pListEx->GetCellTextStyle(nRow + 1, m_columns.nDescr);
    if (st != ctsBold)
    {
      m_pListEx->SetCellTextStyle(nRow + 1, m_columns.nDescr, ctsBold);
      m_nPrevBoldRowText_inView = nRow;
      bUpdateAgain = true;
    }

    if (bUpdateAgain)
      m_pListEx->UpdateView();
  }
}

void CTabListCtrl::OnLvnItemchangedLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  if (m_pListEx->IsUpdatingView())
    return;

  if (!m_bFirstAppearance)
    return;

  *pResult = 0;
  LPNMLISTVIEW pNMCD = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  int nRow = pNMCD->iItem;
  if (nRow < 0)
    return;

  if (pNMCD->uNewState & LVIS_SELECTED)
  {
    //OnLvnItemchangedLst(pNMHDR, pResult, nRow);

    int nRow_inList = nRow + 1;
    m_dataSelectedItem.sDescr = m_pListEx->GetCellText(nRow_inList, m_columns.nDescr);
    m_dataSelectedItem.nItemID = m_pListEx->GetCellFlag(nRow_inList, m_columns.nDescr);
    ::PostMessage(m_pParentMng->m_hWnd, UM_SELECT_ROW_IN_TAB, 0, m_nID);
  }
}

void CTabListCtrl::InsertRowsInOutlookTabLists(CWnd* pParent)
{
  m_pParent = pParent;

  m_pListEx = new �Ai_ListCtrlEx();
  m_pListEx->SetList(this, pParent->m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_pListEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = false;
  if (!m_bGridLinesStyle)
    SetExtendedStyle(GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_pListEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  m_columns.nDescr = m_pListEx->SetColumnEx(L"��������������", LVCFMT_CENTER);
  m_pListEx->SetColumnWidthStyle(0, CS_WIDTH_FIX, 150);
  m_pListEx->UpdateView();
  m_pListEx->MatchColumns();

  m_pListEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  vector<CString> textOfCells;
  int nRow = 0;

  switch (m_nID)
  {
    case ID_OUTLOOK_CONNECTION_TAB:
      textOfCells.push_back(L"��������� ����������");
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_CONNECTION_SETTINGS);
      /*textOfCells[0] = L"������� ������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_UNDER_CONSTRUCTION); // IDD_CONNECTION_MONITORING);*/
      break;

    case ID_OUTLOOK_DATA_TAB:
      textOfCells.push_back(L"���������� � ��������");
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_INFO_ABOUT_DEVICE);
      textOfCells[0] = L"�������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_ENERGY);
      textOfCells[0] = L"��������� ����";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_NET_PROPERTIES);

	  textOfCells[0] = L"�������� ���������� ����";
	  nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
	  m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_NET_CONTROL_SETTINGS);

      textOfCells[0] = L"������ �������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_EVENT_JOURNAL);
      textOfCells[0] = L"��������� ���������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_POWER_MAX);
      textOfCells[0] = L"������� ���������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_DATA_POWER_PROFILE);
      break;

    case ID_OUTLOOK_DEVICE_SETTINGS_TAB:
      textOfCells.push_back(L"���");
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_LCD);
      textOfCells[0] = L"�����";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_TIME);
      textOfCells[0] = L"�������� ����������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_TARIFF_SHEDULES);
      textOfCells[0] = L"���������� �������� ����������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_SHEDULE_CONTROL_MAXES);
      textOfCells[0] = L"���� ����������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_SWITCH_OFF_RELE);
      textOfCells[0] = L"����������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_INTERFACES);
      //VIEW_CALIBRATION; to preprocessor
#ifdef VIEW_CALIBRATION
//#if 0
      textOfCells[0] = L"����������";
      nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
      m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_SETTINGS_CALIBRATION);

	  textOfCells[0] = L"������������";
	  nRow = m_pListEx->SetChild(m_columns.nDescr, &textOfCells);
	  m_pListEx->SetCellFlag(nRow, m_columns.nDescr, IDD_PRODUCTION);
#endif
      break;

    default: ASSERT(false);
  }

  m_pListEx->UpdateView();
  m_pListEx->MatchHeight();

  m_bFirstAppearance = true;
}

void CTabListCtrl::Resize()
{
  CRect r{};
  m_pParent->GetClientRect(&r);

  int nWidth = GetColumnWidth(m_columns.nDescr);
  nWidth = r.Width() - 2;
  SetColumnWidth(m_columns.nDescr, nWidth);
}

bool CTabListCtrl::IsSelectedRow()
{
  return m_pListEx->IsSelectedRows();
}

void CTabListCtrl::SelectFirstRow()
{
  m_pListEx->SelectRow(0);
}

void CTabListCtrl::SelectSpecRow(int nDescrID)
{
  int nItemCount = GetItemCount();
  for (int i = 0; i < nItemCount; ++i)
  {
    int nRow_inList = i + 1;
    int nID = m_pListEx->GetCellFlag(nRow_inList, m_columns.nDescr);
    if (nID == nDescrID)
      m_pListEx->SelectRow(i);
  }
}