#include "stdafx.h"
#include "Defines.h"
#include "FormViewEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CFormViewEx, CCommonViewForm)

CFormViewEx::CFormViewEx()
  : CCommonViewForm(CFormViewEx::IDD, L"CFormViewEx")
  , m_mfcListCtrl(nullptr, dynamic_cast<CCommonViewDealer*>(this))
{
  ASSERT(false);
}

CFormViewEx::CFormViewEx(�Ai_ListCtrlEx** pplst)
  : CCommonViewForm(CFormViewEx::IDD, L"CFormViewEx")
  , m_mfcListCtrl(pplst, dynamic_cast<CCommonViewDealer*>(this))
{
}

CFormViewEx::~CFormViewEx()
{
}

void CFormViewEx::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFormViewEx, CCommonViewForm)
  ON_WM_CREATE()
  ON_WM_DESTROY()
  ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CFormViewEx diagnostics

#ifdef _DEBUG
void CFormViewEx::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

#ifndef _WIN32_WCE
void CFormViewEx::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif
#endif //_DEBUG

// CFormViewEx message handlers

int CFormViewEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CCommonViewForm::OnCreate(lpCreateStruct) == -1)
    return -1;

  if (!m_mfcListCtrl.Create(WS_CHILD | WS_VISIBLE | LVS_REPORT |
    LVS_SHOWSELALWAYS, CRect(0, 0, 0, 0), this, ID_MFCLISTCTRL))
    return -1;

  if (m_bMfcListCtrlActual)
  {
    CListCtrl* pLC = (CListCtrl*)&m_mfcListCtrl;
    CHeaderCtrl* pHeader = pLC->GetHeaderCtrl();
    if (pHeader != nullptr)
      pHeader->ModifyStyle(HDS_BUTTONS, 0);    // disable the sorting.
  }

  return 0;
}

void CFormViewEx::OnDestroy()
{
  m_bMfcListCtrlActual = false;
  CCommonViewForm::OnDestroy();
  VERIFY(m_mfcListCtrl.DestroyWindow());
  m_mfcListCtrl.DestroyAttachedList();
}

void CFormViewEx::OnSize_()
{
  if (m_bMfcListCtrlActual)
  {
    if (m_mfcListCtrl.GetSafeHwnd() != nullptr)
    {
      CRect rct{};
      GetClientRect(&rct);
      m_mfcListCtrl.MoveWindow(0, 0, rct.Width(), rct.Height());
    }
  }
}

BOOL CFormViewEx::OnEraseBkgnd(CDC* pDC)
{
  // return CCommonViewForm::OnEraseBkgnd(pDC);

  return TRUE;
}

void CFormViewEx::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  SetScrollSizes(MM_TEXT, CSize(0, 0));

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CFormViewEx::BeforeDestroyClass()
{
}

BOOL CFormViewEx::OnCmdMsg(UINT nID, int nCode,
                            void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
  if (m_bMfcListCtrlActual)
  {
    if (m_mfcListCtrl.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
      return TRUE;
  }

  return CCommonViewForm::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}