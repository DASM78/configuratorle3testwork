#include "stdafx.h"
#include <locale.h>
#include "ElectricMeter.h"
#include "MainFrm.h"
#include "ElectricMeterDoc.h"
#include "ElectricMeterView.h"
#include "AboutDlg.h"
#include "Ai_File.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CElectricMeterApp theApp;

CElectricMeterApp::CElectricMeterApp()
  : CWinAppEx(TRUE /* m_bResourceSmartUpdate */)
#ifdef _DEBUG
  , m_bAdminRegim(false)
#endif
  , m_bFlag1(false)
{
#ifdef _DEBUG
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

  // TODO: �������� ���� ������ �������������� ���������� ������� ����������� ��������������; �������������
  // ������ ��� ������: �����������.�����������.����������.���������������
  SetAppID(_T("CompanyNameX_Rus.LE_Configurator.App.1"));
}

BEGIN_MESSAGE_MAP(CElectricMeterApp, CWinAppEx)
  ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
  //ON_COMMAND(ID_FILE_NEW, CWinAppEx::OnFileNew)
  //ON_COMMAND(ID_FILE_OPEN, CWinAppEx::OnFileOpen)
END_MESSAGE_MAP()

BOOL CElectricMeterApp::InitInstance()
{
  HANDLE MUTEX = CreateMutex(nullptr, FALSE, L"The application of LE_Configurator");
  if (MUTEX)
  {
    /*if (GetLastError() == ERROR_ALREADY_EXISTS)
    {
      ReleaseMutex(MUTEX);
      CloseHandle(MUTEX);
      AfxMessageBox("�������� ��������� ���������� - �������� ���������", MB_ICONWARNING);
      return FALSE;
    }*/
  }
  ReleaseMutex(MUTEX);
  CloseHandle(MUTEX);

  CreateMutex(nullptr, FALSE, L"The application of LE_Configurator");

#ifdef _DEBUG
  //AfxMessageBox(L"LE-Configurator.exe");
#endif

  // �����������

  CString sLocaleLanguage = L"Russian_Russia.1251";
  _wsetlocale(LC_ALL, sLocaleLanguage);
  _wsetlocale(LC_NUMERIC, L"C");

  // Help

  CString strHelpFilePath = m_pszHelpFilePath;
  CString strHelpFile = L"��-������������.chm";

  delete m_pszHelpFilePath;
  m_pszHelpFilePath = nullptr;
  strHelpFilePath.MakeLower();
  strHelpFilePath.Replace(strHelpFile, /*L"Ex\\Help\\" + */strHelpFile);
  m_pszHelpFilePath = new const wchar_t[MAX_PATH + 1];
  if (m_pszHelpFilePath != nullptr)
  {
    ZeroMemory((void*)m_pszHelpFilePath, MAX_PATH + 1);
    wcscpy_s((wchar_t*)m_pszHelpFilePath, strHelpFilePath.GetLength() + 1, strHelpFilePath.GetBuffer());
  }
  TRACE(CString(m_pszHelpFilePath) + L"\n");

  // ������������� ��������� OLE

  if (!AfxOleInit())
  {
    AfxMessageBox(IDP_OLE_INIT_FAILED);
    return FALSE;
  }

  // Other registry

  AfxEnableControlContainer();

  EnableTaskbarInteraction(FALSE);

  // ��� ������������� �������� ���������� RichEdit ��������� ����� AfxInitRichEdit2()	
  // AfxInitRichEdit2();

  // ����������� �������������
  // ���� ��� ����������� �� ������������ � ���������� ��������� ������
  // ��������� ������������ �����, ���������� ������� �� ����������
  // ���������� ��������� �������������, ������� �� ���������
  // �������� ������ �������, � ������� �������� ���������
  // TODO: ������� �������� ��� ������ �� ���-������ ����������,
  // �������� �� �������� �����������

  CString sRegEditAppName;
  sRegEditAppName.LoadString(IDS_REG_EDIT_APP_NAME);
  SetRegistryKey(sRegEditAppName);
  LoadStdProfileSettings(10);  // ��������� ����������� ��������� INI-����� (������� MRU); 20 - The number of recently used files to track
  //SetRegistryBase (_T("Settings"));

  InitContextMenuManager();
  InitShellManager();

  InitKeyboardManager();

  InitTooltipManager();
  CMFCToolTipInfo ttParams;
  ttParams.m_bVislManagerTheme = TRUE;
  theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL, RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

  // Parse args and create RUNTIME_CLASSes

  ParseCommArgs();

  CSingleDocTemplate* pDocTemplate;
  pDocTemplate = new CSingleDocTemplate(
    IDR_MAINFRAME,
    RUNTIME_CLASS(CElectricMeterDoc),
    RUNTIME_CLASS(CMainFrame),       // main SDI frame window
    RUNTIME_CLASS(CElectricMeterView));
  if (!pDocTemplate)
    return FALSE;
  AddDocTemplate(pDocTemplate);

  int NumArgs;
  PWSTR *ppArgv = CommandLineToArgvW(GetCommandLineW(), &NumArgs);
  m_sPathApp = ppArgv[0];
  int n = m_sPathApp.ReverseFind(_T('\\'));
  if (n != -1)
    m_sPathApp = m_sPathApp.Left(n);

  // GDIplus

  Gdiplus::GdiplusStartupInput gdiplusStartupInput;
  Gdiplus::GdiplusStartup(&m_GdiplusToken, &gdiplusStartupInput, nullptr);

  // Parse command line for standard shell commands, DDE, file open

  CCommandLineInfo cmdInfo;
  ParseCommandLine(cmdInfo);

  // Dispatch commands specified on the command line

  if (!ProcessShellCommand(cmdInfo))
    return FALSE;

  // The one and only window has been initialized, so show and update it.

  m_pMainWnd->ShowWindow(SW_SHOW);
  m_pMainWnd->UpdateWindow();

  CMainFrame* pMF = (CMainFrame*)m_pMainWnd;

  CString sExeFile = CAi_File::GetAppPathName();
  CAi_File f;
  f.DetectFileVersionInfo(sExeFile);
  pMF->SetProductVersion(f.GetProductVersion());

  pMF->ShowApp();

  return TRUE;
}

void CElectricMeterApp::ParseCommArgs()
{
  int NumArgs;
  PWSTR *ppArgv = CommandLineToArgvW(GetCommandLineW(), &NumArgs);
  for (int i = 1; i < NumArgs; ++i)
  {
    if (ppArgv[i][0] == _T('/') || ppArgv[i][0] == _T('-'))
    {
#ifdef _DEBUG
      if (_wcsicmp(ppArgv[i] + 1, _T("admin")) == 0)
        m_bAdminRegim = true;

      if (_wcsicmp(ppArgv[i] + 1, _T("Flag1")) == 0)
        m_bFlag1 = true;
#endif
    }
  }
}

int CElectricMeterApp::ExitInstance() 
{
  return CWinAppEx::ExitInstance();
}

void CElectricMeterApp::OnAppAbout()
{
  CAboutDlg aboutDlg;
  CMainFrame* pMF = (CMainFrame*)m_pMainWnd;
  aboutDlg.SetProductVersion(pMF->GetProductVersion());
  aboutDlg.DoModal();
}