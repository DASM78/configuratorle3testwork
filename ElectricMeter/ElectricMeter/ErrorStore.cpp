#include "StdAfx.h"
#include "ErrorStore.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CErrorStore::CErrorStore()
{
  m_errors.push_back(APP_ERROR(0, L"OK"));
  m_errors.push_back(APP_ERROR(1, L"Undefine error", L"TRUE"));
  FormErrorList();
}

CErrorStore::~CErrorStore()
{
  /*
  if( !(cond) )\
  {printf( "assertion error line %d, file(%s)\n", \
  __LINE__, __FILE__ );}
  */
}

void CErrorStore::SetLastError(int nID, CString str)
{
  m_pLastError = NULL;
  
  m_unknown.nID = -1;
  m_unknown.sDescription = L"";
  m_unknown.sDescription2 = L"";
  
  for (auto i : m_errors)
  {
    if (i.nID == nID)
      m_pLastError = &i;
  }

  if (!m_pLastError)
  {
    m_unknown.nID = nID;
    m_unknown.sDescription = str;
  }
}

APP_ERROR* CErrorStore::GetLastError()
{
  APP_ERROR* pLastError = m_pLastError;
  m_pLastError = NULL;

  if (!pLastError)
    pLastError = &m_unknown;

  m_unknown.nID = -1;
  m_unknown.sDescription = L"";
  m_unknown.sDescription2 = L"";

  if (pLastError->nID != -1)
    m_prevLastError = *pLastError;

  return pLastError;
}

void CErrorStore::ShowLastError()
{
}

void CErrorStore::Throw()
{
  //throw new CErrorStore();
}