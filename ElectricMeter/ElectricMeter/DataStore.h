#pragma once

#include "Model_.h"
#include "Typedefs.h"
#include "DataStruct.h"
#include <map>
#include <vector>

class CDataStore
{
public:
  CDataStore();
  ~CDataStore();

private:
  std::map <formID_t, dataList_t> m_dataGroups; // ID ������� / ������ ��� ������������

public:
  void FormDataForReqs(formID_t nID, dataList_t* pData); // �� ����� �������� ����������� ���������� ������
  std::vector<std::pair<const unsigned char*, varOnce_t>>* GetDataForReqs();
  formID_t GetCurrReqFormID();
private:
  std::vector<std::pair<const unsigned char*, varOnce_t>> m_dataForReq; // ������� ��������: �����, � �������/��� �������
  formID_t m_dataForReqForFormID = 0; // ��������� ��� ����� ������� ������������ m_dataForReq
  void ClearReqs();

public:
  bool ParseReceivedMonitoringData(CString& sReqAddr, CString& sData);
  dataTypesAndVarMeanings_t* GetReceivedDataStore();
  void ClearReceivedDataStore();
  void ClearReceivedDataStoreItem(unsigned int nAddress);
private:
  dataTypesAndVarMeanings_t m_receivedDataStore;
};

