#pragma once

#include "ProtocolBase.h"
#include "Typedefs.h"
#include "Thread_61107.h"
#include <vector>

class CProtocol_61107: public CProtocolBase
{
public:
  CProtocol_61107(CWnd* pParent);
  ~CProtocol_61107();

private:
  CWnd* m_pParent = nullptr;

public:
  virtual void Connect(CONNECT_INIT* pInit) override final;
  virtual void Disconnect() override final;

  // *** ������ ����������� ��� ������� ��������� ***
  
public:
  // ������� ���� ������� ������ (����� �� �����������) ���� �������������� ������ �������� ������ ��� ����� ������� TimerTick
  void SetMonitoringReqs(int nDataID, std::vector<std::pair<const unsigned char*, varOnce_t>>* pList);
private:
    // ���������� ������ ������� ����������� �, � ���������������� ������, ������������� �� TimerTick
  int m_nMonDataID = 0;
  std::vector<std::pair<const unsigned char*, varOnce_t>> m_monList;
  bool m_bIsMonListNew = false;

public:
  void SetRecordReq(std::vector<SEND_UNIT>* pList);
private:
  // ���������� ������ ������ �� ������ �, � ���������������� ������, ������������� �� TimerTick
  std::vector<SEND_UNIT> m_recList;
  bool IsRecordingReq();

  // *** ... ***

public:
  virtual LRESULT TimerTick() override final;

  virtual LRESULT OnResultNotify(WPARAM wParam, LPARAM lParam) override final;

private:
  void On�onnectionResultNotify(EXCH_DATA* pExchData);
  void OnDisconnectionResultNotify(int nTreadPhase, bool bResult);

  void RunInitialisationBranch();
  void OnInitialisationResultNotify(int nTreadPhase, EXCH_DATA* pExchData);

  bool RunRecordingBranch();
  void OnRecordingResultNotify(int nTreadPhase, EXCH_DATA* pExchData);
  bool ParseReceivedSendConfirmationData(CString& sReqAddr, CString& sData);

  bool RunMonitoringBranch();
  void OnMonitoringResultNotify(int nTreadPhase, EXCH_DATA* pExchData);

};
