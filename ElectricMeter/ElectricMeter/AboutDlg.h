#pragma once
#include "afxwin.h"

class CAboutDlg: public CDialog
{
public:
  CAboutDlg();

  // Dialog Data
  //{{AFX_DATA(CAboutDlg)
  enum
  {
    IDD = IDD_ABOUTBOX
  };
  //}}AFX_DATA

  // Implementation
protected:
  //{{AFX_MSG(CAboutDlg)
  // No message handlers
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()

  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CAboutDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL
  virtual BOOL OnInitDialog();

private:
  CString m_sProductVersion;
  CStatic m_stcVersion;


public:
  void SetProductVersion(CString sVer);
  afx_msg void OnBnClickedBtnLogo();
};