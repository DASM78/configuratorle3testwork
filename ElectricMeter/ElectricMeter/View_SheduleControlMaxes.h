#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "Ai_ListCtrlEx.h"
#include "Ai_ToolTip.h"
#include "CommonListCtrl.h"
#include "ComboBox_binding.h"
#include "CommonViewDealer.h"
#include "Model_.h"

class CView_SheduleControlMaxes: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_SheduleControlMaxes)

public:
  CView_SheduleControlMaxes();
  virtual ~CView_SheduleControlMaxes();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_SETTINGS_SHEDULE_CONTROL_MAXES
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  virtual void OnNMDblclkLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  afx_msg LRESULT MeanWasSetedAfterBindCtrl(WPARAM wParam /*HWND parent*/, LPARAM lParam /* Item ctrl ID */);

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);
  virtual BOOL PreTranslateMessage(MSG* pMsg);

  CCommonListCtrl m_lstPowerMaxSet;
  �Ai_ListCtrlEx* m_plstPowerMaxSetEx = nullptr;

  CComboBox_binding* m_pcbxTime = nullptr;
  void FillInTable();
  void GetCutOfMeanings();
  void ClearDB();
  void SetDataByDef();
  bool ParseData(CString& s, CString& sMBegin, CString& sMEnd, CString& sEBegin, CString& sEEnd);
  CString FormData(int nIdx);

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;

  // Xml

  CParamsTreeNode* m_pPowMaxNode = nullptr;
  void TakeValueFromXmlTree(CParamsTreeNode* pMainNode = nullptr);
  void PutValueToXmlTree();

  CMFCButton m_btnOpenPowerMaxFromSpecXmlFile;
  CMFCButton m_btnSavePowerMaxToSpecXmlFile;
  afx_msg void OnBnClickedBtnOpenPowerMaxFromFile();
  afx_msg void OnBnClickedBtnSavePowerMaxToFile();

  // Record/Load

  CMFCButton m_btnLoadPowerMaxFromDevice;
  CMFCButton m_btnRecordPowerMaxToDevice;
  afx_msg void OnBnClickedBtnLoadPowerMaxFromDevice();
  virtual void DoAfterClearLoadData() override final;

  afx_msg void OnBnClickedBtnRecordPowerMaxToDevice();
  virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) override final;

  // Check

  CMFCButton m_btnCheck;
  CAi_ToolTip m_toolTip;
  afx_msg void OnBnClickedBtnCheck();
  bool CheckData(bool bMsg);
  
  CMFCButton m_btnSetByDef;
  afx_msg void OnBnClickedBtnSetByDef();
};
