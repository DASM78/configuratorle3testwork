#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "CommonListCtrl.h"
#include "XmlAid.h"
#include "CommonViewDealer.h"
#include "Model_.h"
#include "CounterInfo.h"

class CView_InfoAboutDevice: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_InfoAboutDevice)

public:
  CView_InfoAboutDevice();
  virtual ~CView_InfoAboutDevice();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_DATA_INFO_ABOUT_DEVICE
  };

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  virtual void DoDataExchange(CDataExchange* pDX);
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

  CCommonListCtrl m_lstInfo;
  �Ai_ListCtrlEx* m_plstInfoEx = nullptr;

  void ClearRows();
  void FillInByFilter();
  void ParseReceivedData(mdl::teFrameTypes ft);

  // Load

  CMFCButton m_btnLoadInfoFromDevice;
  afx_msg void OnBnClickedBtnLoadInfoFromDevice();
  virtual void DoAfterClearLoadData() override final;

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  sCfg m_cfg; // ��� ������� ��������� �������� ��������
public:
	afx_msg void OnLvnItemchangedLstDeviceAbout (NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonClearErr();
};
