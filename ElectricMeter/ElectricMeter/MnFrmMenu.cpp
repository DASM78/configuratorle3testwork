#include "StdAfx.h"
#include "Resource.h"
#include "ElectricMeterDoc.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CMainFrame::OnUpdateTbrConnect(CCmdUI* pCmdUI)
{
  pCmdUI->SetCheck(IsPrgState_OnlineDeviceData());
  pCmdUI->Enable(!IsPrgState_ConnectingToDevice());
}

void CMainFrame::OnTbrConnect()
{
  if (IsPrgState_OnlineDeviceData())
    DisconnectionFromDevice();
  else
  {
    if (CheckDeviceAddress())
      ConnectionToDevice();
  }
}

BOOL CMainFrame::OnDrawMenuImage(CDC* pDC,
                                 const CMFCToolBarMenuButton* pMenuButton,
                                 const CRect& rectImage)
{
  ASSERT_VALID(pDC);
  ASSERT_VALID(pMenuButton);

  bool bPaint = false;
  int nID = 0;
  nID = nID;

  //switch (pMenuButton->m_nID)
  //{
    /*
    case ID_FILE_MRU_FILE1:
    nID = IDB_FILE_MRU_FILE1;
    bPaint = true;
    break;
    */

    /*
    case ID_APP_EXIT:
      nID = IDB_APP_EXIT;
      bPaint = true;
      break;
      */

    /*
    case ID_APP_ABOUT:
      nID = IDB_APP_ABOUT;
      bPaint = true;
      break;
      */
  //};

  if (bPaint)
  {
    bPaint = false;

    //CreateBitmapMask(pDC, nID, rectImage);
    CBitmap bmp;
    bmp.LoadBitmap(nID);

    CDC srcDC; // select current bitmap into a compatible CDC
    srcDC.CreateCompatibleDC(nullptr);
    CBitmap* pOldBitmap = srcDC.SelectObject(&bmp);
    pDC->BitBlt(rectImage.left + 2, rectImage.top + 2, rectImage.Width() - 2, rectImage.Height() - 2, &srcDC, 0, 0, SRCCOPY); // BitBlt to pane rect
    srcDC.SelectObject(pOldBitmap);

    return TRUE;
  }

  return FALSE;
}
