#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Ai_ColorStatic.h"
#include "afxbutton.h"
#include "afxwin.h"
#include "Model_.h"
#include "CounterInfo.h"
#include "DoubleEdit.h"
#include <string>
using namespace std;
// CProduction form view

class CProduction : public CCommonViewForm, public CCommonViewDealer
{
	DECLARE_DYNCREATE (CProduction)
protected:
	CProduction ();           // protected constructor used by dynamic creation
	virtual ~CProduction ();

public:
	enum { IDD = IDD_PRODUCTION };
private:
	virtual void BeforeDestroyClass () override final;
#ifdef _DEBUG
	virtual void AssertValid () const;
#ifndef _WIN32_WCE
	virtual void Dump (CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP ()
public:
	virtual void DoDataExchange (CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnBnClickedBtnClearJournal ();
	afx_msg void OnAnyBnClicked ();
	afx_msg void OnIfaceClicked ();
private:
	
	void OnInitialUpdate ();
	virtual void UpdateParamMeaningsByReceivedDeviceData (mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
	virtual void UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult) override final;
	void InitCfgStruct ();
	
	void UpdateControlsAsCfg (void);
	void UpdateCfgAsControls ();	
	
	


	
	CDoubleEdit m_editFW;
	CDoubleEdit m_editHW;
	CParamsTreeNode* m_pCounterConfigNode = nullptr;
	CDateTimeCtrl m_datetime;	
public:	
	
	afx_msg void OnBnClickedBtnClearCounters();
	afx_msg void OnBnClickedBtnWrCntInfo();	
	afx_msg void OnBnClickedBtnReadInfo ();
	afx_msg void OnBnClickedBtnSave ();
	afx_msg void OnBnClickedBtnLoad ();
	afx_msg void OnEnKillfocusEditFw ();
	afx_msg void OnEnKillfocusEditHw ();

private:
	
	template <class typen> struct RTYPE
	{
		typen type;
		int cID;
	};
	template <class typen> struct INFOTYPE
	{
		typen type;
		string desc;
		bool isVer;// optional
	};
	struct COMB_TYPE
	{
		int idc_type;
		int idc_ver;
		int idx;
	};
	

	static const COMB_TYPE  ifaceCombos[MAX_IFACES];

	static  const RTYPE <eMeterType>  radioBtnTypes[];
	static  const RTYPE <eMeterCase>  radioBtnCases[];
	static  const RTYPE <eMeterAccuracy>  radioBtnAcc[];
	static const RTYPE <eMeterEnergyKind> radioBtnEnergyKind[];
	static const RTYPE <eMeterNomU> radioBtnNomU[];
	static const RTYPE <eMeterNomI> radioBtnNomI[];
	static const RTYPE <eMeterOptions> checkOptions[];
	sCfg m_cfg;
public:
	afx_msg void OnBnClickedBtnCom1();
	afx_msg void OnEnChangeEditSerail();
	afx_msg void OnBnClickedBtnCom2();
	afx_msg void OnBnClickedBtnCom3();
};


