#pragma once

class CElectricMeterDoc: public CDocument
{
protected: // create from serialization only
  CElectricMeterDoc();
  virtual ~CElectricMeterDoc();

  DECLARE_DYNCREATE(CElectricMeterDoc)
  DECLARE_MESSAGE_MAP()

private:
  virtual void Serialize(CArchive& ar);

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
  virtual void OnDocumentEvent(DocumentEvent deEvent);

  void OnCloseDocumentByReopen();
  virtual void OnCloseDocument();

public:
  bool IsUnsavedData();
};