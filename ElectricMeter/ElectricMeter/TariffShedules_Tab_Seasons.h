#pragma once

class CTariffShedules_Tab_Seasons : public CDialogEx
{
	DECLARE_DYNAMIC(CTariffShedules_Tab_Seasons)

public:
	CTariffShedules_Tab_Seasons(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTariffShedules_Tab_Seasons();

// Dialog Data
	enum { IDD = IDD_SETTINGS_TARIFF_SHEDULES_TAB_SEASONS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
