#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Ai_ColorStatic.h"
#include "afxbutton.h"
#include "afxwin.h"
#include "Model_.h"

class CView_SwitchOffRele: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_SwitchOffRele)

public:
  CView_SwitchOffRele();
  ~CView_SwitchOffRele();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_SETTINGS_SWITCH_OFF_RELE
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX) override final; // DDX/DDV support
  virtual void OnInitialUpdate() override final;
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_() override final;
  virtual void OnDraw(CDC* pDC) override final;
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const override final;
  virtual void Dump(CDumpContext& dc) const override final;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  CAi_ColorStatic m_stcReleState;
  CFont m_bigFont;
  CMFCButton m_btnSwitchOn;
  CMFCButton m_btnSwitchOff;
  afx_msg void OnBnClickedBtnSwitchOn();
  afx_msg void OnBnClickedBtnSwitchOff();

  // Xml

  CParamsTreeNode* m_pPowLimitNode = nullptr;
  void TakeValueFromXmlTree(CParamsTreeNode* pMainNode = nullptr);
  void PutValueToXmlTree();

  //

  void FillInCtrls();

  CMFCButton m_btnLoadPowerLimit;
  virtual void DoAfterClearLoadData() override final;
  int m_nPowerAndEnergyWasRecorded = 0;
  CMFCButton m_btnRecordPowerLimit;

  afx_msg void OnBnClickedBtnLoadPowLimitFromDevice();
  afx_msg void OnBnClickedBtnRecordPowLimitToDevice();

  CString FormDataPower();
  bool ParseDataPower(CString s);
  CString FormDataEnergy();
  bool ParseDataEnergy(CString s);

  CButton m_chkPowerLimit;
  CButton m_chkEnergyLimit;
  afx_msg void OnBnClickedSwitchOnPowerLimit();
  afx_msg void OnBnClickedSwitchOnEnergyLimit();
  void CheckState_PowerCtrls();
  void CheckState_EnergyCtrls();

  CEdit m_edtPowLmt_power;
  CEdit m_edtPowLmt_durationOver;
  CEdit m_edtPowLmt_durationOff;
  afx_msg void OnEnKillfocusEdtPower();
  afx_msg void OnEnKillfocusEdtDurOver();
  afx_msg void OnEnKillfocusEdtDurOff();

  CEdit m_edtEnrgLmt_T1;
  CButton m_chkEnrgLmt_T1;
  CEdit m_edtEnrgLmt_T2;
  CButton m_chkEnrgLmt_T2;
  CEdit m_edtEnrgLmt_T3;
  CButton m_chkEnrgLmt_T3;
  CEdit m_edtEnrgLmt_T4;
  CButton m_chkEnrgLmt_T4;
  CEdit m_edtEnrgLmt_T5;
  CButton m_chkEnrgLmt_T5;
  CEdit m_edtEnrgLmt_T6;
  CButton m_chkEnrgLmt_T6;
  CEdit m_edtEnrgLmt_T7;
  CButton m_chkEnrgLmt_T7;
  CEdit m_edtEnrgLmt_T8;
  CButton m_chkEnrgLmt_T8;
  afx_msg void OnEnKillfocusEdtT1();
  afx_msg void OnEnKillfocusEdtT2();
  afx_msg void OnEnKillfocusEdtT3();
  afx_msg void OnEnKillfocusEdtT4();
  afx_msg void OnEnKillfocusEdtT5();
  afx_msg void OnEnKillfocusEdtT6();
  afx_msg void OnEnKillfocusEdtT7();
  afx_msg void OnEnKillfocusEdtT8();
  afx_msg void OnBnClickedChkEOverT1();
  afx_msg void OnBnClickedChkEOverT2();
  afx_msg void OnBnClickedChkEOverT3();
  afx_msg void OnBnClickedChkEOverT4();
  afx_msg void OnBnClickedChkEOverT5();
  afx_msg void OnBnClickedChkEOverT6();
  afx_msg void OnBnClickedChkEOverT7();
  afx_msg void OnBnClickedChkEOverT8();

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) override final;
};
