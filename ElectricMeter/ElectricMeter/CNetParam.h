#pragma once


#include "CommonViewForm.h"
#include "afxcmn.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "Ai_ColorStatic.h"
#include "afxbutton.h"
#include "afxwin.h"
#include "Model_.h"
#include "CounterInfo.h"
#include "DoubleEdit.h"
#include <string>

using namespace std;

// CNetParam dialog

class CNetParam  : public CCommonViewForm, public CCommonViewDealer
{
	DECLARE_DYNCREATE(CNetParam)

public:
	CNetParam();   // standard constructor
	virtual ~CNetParam();

// Dialog Data

	enum { IDD = IDD_NET_CONTROL_SETTINGS };

	CCommonListCtrl m_lstEvJr;
	СAi_ListCtrlEx* m_plstEvJrEx = nullptr;

protected:
	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
	virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) override final;
	void CNetParam::OnInitialUpdate();
	void CNetParam::ParseReceivedData(teFrameTypes ft);	
	void FillInByFilter();
	void CNetParam::ClearRows();
	virtual void BeforeDestroyClass() override final;

	virtual void OnSize_();
	virtual void OnDraw(CDC* pDC);
	virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

	//void DoAfterClearLoadData();
	

	DECLARE_MESSAGE_MAP()
public:
	void CNetParam::CheckMeaningsViewByConnectionStatus();
	void CNetParam::OnConnectionStateWasChanged();
	afx_msg void OnBnClickedBtnReadParamSettings();
	afx_msg void OnBnClickedBtnWriteParamSettings();
	afx_msg void OnBnClickedBtnReadJrn();	
	
	
//	afx_msg void OnLvnItemchangedLstEvents(NMHDR *pNMHDR, LRESULT *pResult);
protected:
	
	CDoubleEdit m_paramLowV;	
	CDoubleEdit  m_paramHighV;
	CDoubleEdit   m_paramLowF;
	CDoubleEdit   m_paramHighF;
	CDoubleEdit   m_paramAvTimeV;
	CDoubleEdit   m_paramAvTimeF;
	bool bWaitRead = false;
public:
	CDoubleEdit m_xxxx;
	CButton m_btnRecParam;
};
