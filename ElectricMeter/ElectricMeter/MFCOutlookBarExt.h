#pragma once

#include "afxoutlookbar.h"

class CMFCOutlookBarExt : public CMFCOutlookBar
{
  DECLARE_DYNAMIC(CMFCOutlookBarExt)

public:
  CMFCOutlookBarExt();
  ~CMFCOutlookBarExt();

  DECLARE_MESSAGE_MAP()

private:
  virtual BOOL AllowShowOnPaneMenu() const { return FALSE; }
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

