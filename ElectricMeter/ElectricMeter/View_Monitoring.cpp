#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "View_Monitoring.h"

using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_Monitoring, CCommonViewForm)

CView_Monitoring::CView_Monitoring()
: CCommonViewForm(CView_Monitoring::IDD, L"CView_Monitoring")
{
}

CView_Monitoring::~CView_Monitoring()
{
}

BEGIN_MESSAGE_MAP(CView_Monitoring, CCommonViewForm)
END_MESSAGE_MAP()

void CView_Monitoring::DoDataExchange(CDataExchange* pDX)
{
}

void CView_Monitoring::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();
  
  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_Monitoring::BeforeDestroyClass()
{
}

void CView_Monitoring::OnSize_()
{
}

void CView_Monitoring::OnDraw(CDC* pDC)
{
}

bool CView_Monitoring::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_Monitoring::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_Monitoring::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_Monitoring::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_Monitoring::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_Monitoring::CheckMeaningsViewByConnectionStatus()
{
}

void CView_Monitoring::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
}
