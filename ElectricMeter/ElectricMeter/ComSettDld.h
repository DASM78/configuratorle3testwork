#pragma once
#include "CounterInfo.h"

// CComSettDld dialog

class CComSettDld : public CDialog
{
	DECLARE_DYNAMIC(CComSettDld)

public:
	CComSettDld(const sCommInfo& info, bool isOpto = false,  CWnd *pParent = NULL);   // standard constructor
	sCommInfo GetInfo() { return m_info; }
	virtual ~CComSettDld();

// Dialog Data
	enum { IDD = IDD_DLD_RS232_SETTINGS };

protected:
	CComSettDld(CWnd* pParent = NULL);   // standard constructor
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	bool m_isOpto;
	sCommInfo m_info;
	CComboBox m_comboSpeed;
	CComboBox m_comboParity;
	CComboBox m_combobits;
	CComboBox m_comboIval;
	CComboBox m_comboProto;
	CEdit m_editNetAddr;
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnEnChangeEdtDeviceAddress ();
};
