#include "stdafx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "ComboBox_binding.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CComboBox_binding, CComboBox)

CComboBox_binding::CComboBox_binding()
{
}

CComboBox_binding::~CComboBox_binding()
{
}

/*
Each message-map entry takes the following form:

ON_Notification( id, memberFxn )

where id specifies the child-window ID of the combo-box control sending the notification and memberFxn is the name of the parent member function you have written to handle the notification.

The parent's function prototype is as follows:

afx_msg void memberFxn( );

The order in which certain notifications will be sent cannot be predicted. In particular, a CBN_SELCHANGE notification may occur either before or after a CBN_CLOSEUP notification.

Potential message-map entries are the following:
�ON_CBN_CLOSEUP   (Windows 3.1 and later.) The list box of a combo box has closed. This notification message is not sent for a combo box that has the CBS_SIMPLE style.
�ON_CBN_DBLCLK   The user double-clicks a string in the list box of a combo box. This notification message is only sent for a combo box with the CBS_SIMPLE style. For a combo box with the CBS_DROPDOWN or CBS_DROPDOWNLIST style, a double-click cannot occur because a single click hides the list box.
�ON_CBN_DROPDOWN   The list box of a combo box is about to drop down (be made visible). This notification message can occur only for a combo box with the CBS_DROPDOWN or CBS_DROPDOWNLIST style.
�ON_CBN_EDITCHANGE   The user has taken an action that may have altered the text in the edit-control portion of a combo box. Unlike the CBN_EDITUPDATE message, this message is sent after Windows updates the screen. It is not sent if the combo box has the CBS_DROPDOWNLIST style.
�ON_CBN_EDITUPDATE   The edit-control portion of a combo box is about to display altered text. This notification message is sent after the control has formatted the text but before it displays the text. It is not sent if the combo box has the CBS_DROPDOWNLIST style.
�ON_CBN_ERRSPACE   The combo box cannot allocate enough memory to meet a specific request.
�ON_CBN_SELENDCANCEL   (Windows 3.1 and later.) Indicates the user's selection should be canceled. The user clicks an item and then clicks another window or control to hide the list box of a combo box. This notification message is sent before the CBN_CLOSEUP notification message to indicate that the user's selection should be ignored. The CBN_SELENDCANCEL or CBN_SELENDOK notification message is sent even if the CBN_CLOSEUP notification message is not sent (as in the case of a combo box with the CBS_SIMPLE style).
�ON_CBN_SELENDOK   The user selects an item and then either presses the ENTER key or clicks the DOWN ARROW key to hide the list box of a combo box. This notification message is sent before the CBN_CLOSEUP message to indicate that the user's selection should be considered valid. The CBN_SELENDCANCEL or CBN_SELENDOK notification message is sent even if the CBN_CLOSEUP notification message is not sent (as in the case of a combo box with the CBS_SIMPLE style).
�ON_CBN_KILLFOCUS   The combo box is losing the input focus.
�ON_CBN_SELCHANGE   The selection in the list box of a combo box is about to be changed as a result of the user either clicking in the list box or changing the selection by using the arrow keys. When processing this message, the text in the edit control of the combo box can only be retrieved via GetLBText or another similar function. GetWindowText cannot be used.
�ON_CBN_SETFOCUS   The combo box receives the input focus.

If you create a CComboBox object within a dialog box (through a dialog resource), the CComboBox object is automatically destroyed when the user closes the dialog box.

If you embed a CComboBox object within another window object, you do not need to destroy it. If you create the CComboBox object on the stack, it is destroyed automatically. If you create the CComboBox object on the heap by using the new function, you must call delete on the object to destroy it when the Windows combo box is destroyed.

Note   If you want to handle WM_KEYDOWN and WM_CHAR messages, you have to subclass the combo box's edit and list box controls, derive classes from CEdit and CListBox, and add handlers for those messages to the derived classes. For more information, see http://support.microsoft.com/default.aspx?scid=kb;en-us;Q174667 and CWnd::SubclassWindow.
*/

BEGIN_MESSAGE_MAP(CComboBox_binding, CComboBox)
  ON_CONTROL_REFLECT(CBN_SELENDOK, OnSelItemAndPressOK) // ENTER or mouse click
  ON_CONTROL_REFLECT(CBN_SELENDCANCEL, OnPressCancel) // ESC
  //ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelChange) // arrow UP/DOWN or mouse click
  //ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCbnCloseup) // ctrl closed
END_MESSAGE_MAP()

BOOL CComboBox_binding::Create_(CWnd* pParent, int nID, bool bSort, �Ai_ListCtrlEx* pListCtrEx)
{
  m_pListCtrEx = pListCtrEx;
  DWORD nStyle = WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL;

  if (bSort)
    nStyle |= CBS_SORT;

  BOOL bResult = Create(nStyle, CRect(0, 0, 100, 4 * 300), (CWnd*)pParent, nID);
  if (bResult)
    SetFont(pParent->GetFont(), TRUE);

  m_nID = nID;
  switch (m_nID)
  {
    case ID_CBX_TARIFFS_IN_24HOURS_TBL_CTRL: FillIn(8); break;
    case ID_CBX_SHEDULES_IN_SEASONS_TBL_CTRL: FillIn(2); break;
    case ID_CBX_SHEDULES_IN_SPEC_DAYS_TBL_CTRL: FillIn(2); break;
    case ID_CBX_POWER_MAX_TBL_CTRL: FillIn(-1); break;
  }


  return bResult;
}

void CComboBox_binding::SelectItem(CComboBox* pCmbBx, CString str)
{
  ASSERT(pCmbBx);

  CString strLBText;
  int nCount = pCmbBx->GetCount();
  for (int i = 0; i < nCount; ++i)
  {
    pCmbBx->GetLBText(i, strLBText);
    if (strLBText == str)
    {
      pCmbBx->SetCurSel(i);
      break;
    }
  }
}

void CComboBox_binding::OnSelItemAndPressOK()
{
  m_pListCtrEx->OnSelchangeCmbbox();
}

void CComboBox_binding::OnPressCancel()
{
  m_pListCtrEx->OnCloseUpCmbbox();
}

void CComboBox_binding::FillIn(int nRowCount)
{
  ResetContent();

  switch (m_nID)
  {
    case ID_CBX_TARIFFS_IN_24HOURS_TBL_CTRL:
      AddString(L"���");
      for (int i = 0; i < nRowCount; ++i)
        AddString(L"����� " + CAi_Str::ToString(i + 1));
      break;

    case ID_CBX_SHEDULES_IN_SEASONS_TBL_CTRL:
    case ID_CBX_SHEDULES_IN_SPEC_DAYS_TBL_CTRL:
      for (int i = 0; i < nRowCount; ++i)
        AddString(L"������ " + CAi_Str::ToString(i + 1));
      break;

    case ID_CBX_POWER_MAX_TBL_CTRL:
      for (int i = 0; i < 24; ++i)
      {
        CString sT;
        sT.Format(L"%.2d:00", i);
        AddString(sT);
        sT.Format(L"%.2d:30", i);
        AddString(sT);
      }
      break;
  }
}
