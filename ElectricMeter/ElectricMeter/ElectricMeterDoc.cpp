#include "stdafx.h"
#include "Resource.h"
#include "ElectricMeterDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CElectricMeterDoc, CDocument)

CElectricMeterDoc::CElectricMeterDoc()
{
}

CElectricMeterDoc::~CElectricMeterDoc()
{
}

BEGIN_MESSAGE_MAP(CElectricMeterDoc, CDocument)
END_MESSAGE_MAP()

void CElectricMeterDoc::Serialize(CArchive& ar)
{
  if (ar.IsStoring())
  {
  }
  else
  {
  }
}

#ifdef _DEBUG
void CElectricMeterDoc::AssertValid() const
{
  CDocument::AssertValid();
}

void CElectricMeterDoc::Dump(CDumpContext& dc) const
{
  CDocument::Dump(dc);
}
#endif //_DEBUG

BOOL CElectricMeterDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
  if (!CDocument::OnOpenDocument(lpszPathName))
    return FALSE;

  OnCloseDocumentByReopen();

  return TRUE;
}

void CElectricMeterDoc::OnDocumentEvent(DocumentEvent deEvent)
{
  switch (deEvent)
  {
    case onAfterOpenDocument:
      break;
    case onAfterNewDocument:
      {
        POSITION pos = GetFirstViewPosition();
        CView* pView = GetNextView(pos);
        ::SendMessage(AfxGetMainWnd()->m_hWnd, UM_ON_AFTER_NEW_DOCUMENT, 0, (LPARAM)pView);
      }
      break;

    case onAfterCloseDocument:
      break;
  };
}

void CElectricMeterDoc::OnCloseDocumentByReopen()
{
  SetModifiedFlag(FALSE);
}

void CElectricMeterDoc::OnCloseDocument()
{
    SetModifiedFlag(FALSE);
  __super::OnCloseDocument();
}

bool CElectricMeterDoc::IsUnsavedData()
{
  return (IsModified() == TRUE) ? true : false;
}

