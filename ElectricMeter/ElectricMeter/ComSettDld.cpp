// ComSettDld.cpp : implementation file
//

#include "stdafx.h"
#include "ElectricMeter.h"
#include "ComSettDld.h"
#include "afxdialogex.h"


// CComSettDld dialog

IMPLEMENT_DYNAMIC(CComSettDld, CDialog)

CComSettDld::CComSettDld(const sCommInfo& info, bool isOpto, CWnd *pParent) : CDialog(CComSettDld::IDD, pParent)
{
	m_info = info;
	m_isOpto = isOpto;
}

CComSettDld::CComSettDld(CWnd* pParent /*=NULL*/)
	: CDialog(CComSettDld::IDD, pParent)
{

}

CComSettDld::~CComSettDld()
{
}

void CComSettDld::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBX_RS_SPEED, m_comboSpeed);	
	DDX_Control(pDX, IDC_CBX_RS_PARITY, m_comboParity);
	DDX_Control(pDX, IDC_CBX_RS_BIT_COUNT, m_combobits);
	DDX_Control(pDX, IDC_CBX_RS_INTERVAL_TIMEOUT, m_comboIval);	
	DDX_Control(pDX, IDC_EDT_DEVICE_ADDRESS, m_editNetAddr);
	DDX_Control(pDX, IDC_CBX_PROTOCOL, m_comboProto);
}


BEGIN_MESSAGE_MAP(CComSettDld, CDialog)
	ON_EN_CHANGE (IDC_EDT_DEVICE_ADDRESS, &CComSettDld::OnEnChangeEdtDeviceAddress)
END_MESSAGE_MAP ()


// CComSettDld message handlers


BOOL CComSettDld::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_comboSpeed.SetCurSel(m_info.speed);
	m_comboParity.SetCurSel(m_info.parity);
	m_combobits.SetCurSel(m_info.bits);
	m_comboIval.SetCurSel(m_info.packetInterval);
	m_comboProto.SetCurSel(m_info.protocol);
	CString addr;
	addr.Format(_T("%u"), m_info.comAddr32);
	m_editNetAddr.SetWindowText(addr);
	
	//if (m_info.protocol == PROTO_61107)
	//{
	//	m_comboParity.EnableWindow (false);
	//	m_combobits.EnableWindow (false);
	//	m_comboIval.EnableWindow (false);
	//	m_comboProto.EnableWindow (false);
	//}
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CComSettDld::OnOK()
{
	m_info.speed = static_cast <eComSpeed> (m_comboSpeed.GetCurSel());
	m_info.bits = static_cast <eComBits>(m_combobits.GetCurSel());
	m_info.packetInterval = static_cast <eIval>(m_comboIval.GetCurSel());
	m_info.parity = static_cast <eParity> (m_comboParity.GetCurSel());
	m_info.protocol = static_cast <eProto> (m_comboProto.GetCurSel());
	CString s;
	(((CEdit *) &m_editNetAddr)->GetWindowText(s));
	m_info.comAddr32 = _ttoi(s);
	CDialog::OnOK();
}


void CComSettDld::OnEnChangeEdtDeviceAddress ()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
