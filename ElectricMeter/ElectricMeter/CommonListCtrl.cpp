#include "stdafx.h"
#include "Resource.h"
#include "CommonListCtrl.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CCommonListCtrl, CAi_ListCtrlPicker)

CCommonListCtrl::CCommonListCtrl(�Ai_ListCtrlEx** pplstEx, CCommonViewDealer* pDealer)
  : m_pplstEx(pplstEx)
  , m_pDealer(pDealer)
{
}

CCommonListCtrl::~CCommonListCtrl()
{
}

BEGIN_MESSAGE_MAP(CCommonListCtrl, CAi_ListCtrlPicker)
  ON_WM_NCCALCSIZE()
  ON_WM_SIZE()
  ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, &CCommonListCtrl::OnNMCustomdrawLst)
  ON_NOTIFY_REFLECT(NM_CLICK, &CCommonListCtrl::OnNMClickLst)
  ON_NOTIFY_REFLECT(NM_DBLCLK, &CCommonListCtrl::OnNMDblclkLst)
  ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, &CCommonListCtrl::OnLvnItemchangedLst)
END_MESSAGE_MAP()


void CCommonListCtrl::EnableInterlacedColorScheme(bool bEnable)
{
  m_bInterlacedColorScheme = bEnable;
}

COLORREF CCommonListCtrl::GetInterlacedColor()
{
  return m_nInterlacedColor;
}

COLORREF CCommonListCtrl::GetSpecColor()
{
  return m_nSpecColor;
}

void CCommonListCtrl::ShowHorizScroll(bool bShow)
{
  m_bShowHorizScroll = bShow;
}

void CCommonListCtrl::CtrlWasCreated()
{
  m_bCreated = true;
}

bool CCommonListCtrl::IsCtrlCreated()
{
  return m_bCreated;
}

void CCommonListCtrl::CtrlIsUpdating(bool bIs)
{
  m_bUpdating = bIs;
}

void CCommonListCtrl::SetSpecColorColumn(int nColumn)
{
  m_nSpecColorColumn = nColumn;
}

void CCommonListCtrl::SetSystemColorColumns(vector<int>* pColumns)
{
  m_systemColorColumns = *pColumns;
}

void CCommonListCtrl::SetInactiveTextInColumns(vector<int>* pColumnList)
{
  m_inactiveTextInColumns = *pColumnList;
}

void CCommonListCtrl::DestroyAttachedList()
{
  ((�Ai_ListCtrlEx*)(*m_pplstEx))->Destroy();
}

void CCommonListCtrl::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS* lpncsp)
{
  if (!m_bShowHorizScroll)
  {
    ModifyStyle(WS_HSCROLL, 0);
    //m_bHideHorizontalScrollbar = false;
  }

  __super::OnNcCalcSize(bCalcValidRects, lpncsp);
}

void CCommonListCtrl::OnNMCustomdrawLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  // ���������� �������, ����� ��� �������������� ���������� �������.
  *pResult = CDRF_DODEFAULT;
  LPNMLVCUSTOMDRAW pNMCD = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);
  int nRow = pNMCD->iSubItem;
  int nCell = -1;

  if ((CDDS_ITEMPREPAINT | CDDS_SUBITEM) == pNMCD->nmcd.dwDrawStage)
  {
    nRow = pNMCD->nmcd.dwItemSpec;
    nCell = pNMCD->iSubItem;
    if (nCell < 0)
      return;
  }

  if (nRow < 0)
    return;
  int nCntRows = GetItemCount();
  if (nCntRows == 0)
    return;
  if ((*m_pplstEx)->IsUpdatingView()) // �������� ������������ �������� ������� ����� (��������, ������������ ����) - ���� ��� �� ����������, �� ��������� ���
    return;

  bool bEnableCell = true;
  if (nCell >= 0)
  {
    int nRow_inList = (*m_pplstEx)->CnvIdxViewToList(nRow);
    bEnableCell = (*m_pplstEx)->IsCellEnable(nRow_inList, nCell);
  }

  // Bg color

  int nColorBk = -1;
  if (m_bInterlacedColorScheme
      && bEnableCell)
  {
    if (nRow % 2)
      nColorBk = GetInterlacedColor();
  }

  if (m_nSpecColorColumn != -1)
  {
    if (nCell == m_nSpecColorColumn)
      nColorBk = GetSpecColor();
    if (find(m_systemColorColumns.begin(), m_systemColorColumns.end(), nCell) != m_systemColorColumns.end())
      nColorBk = GetSysColor(COLOR_BTNFACE);
  }

  if (GetItemState(pNMCD->nmcd.dwItemSpec, LVIS_SELECTED) == LVIS_SELECTED)
  {
    COLORREF cNewCololr = 0;
    if (m_pDealer->UseAlternativeBgColor(this, cNewCololr))
    {
      pNMCD->nmcd.uItemState &= ~CDIS_SELECTED; // deselect item to enable custom coloring
      nColorBk = cNewCololr;
    }
  }

  // Text color

  int nTextColor = -1;
  if (!m_inactiveTextInColumns.empty())
  {
    if (find(m_inactiveTextInColumns.begin(), m_inactiveTextInColumns.end(), nCell) != m_inactiveTextInColumns.end())
      nTextColor = COLOR_LCE_GRAY;
  }

  //
  if (m_pDealer->OnNMCustomdrawLst(this, nRow, nCell))
    (*m_pplstEx)->OnCustomDrawEx(pNMHDR, pResult, nColorBk, nTextColor);
}

void CCommonListCtrl::OnNMClickLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  *pResult = 0;
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
  if (pNMItemActivate->iItem < 0 || pNMItemActivate->iSubItem < 0)
    return;
  m_pDealer->OnNMClickLst(this, pNMItemActivate->iItem, pNMItemActivate->iSubItem);
}

void CCommonListCtrl::OnNMDblclkLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  *pResult = 0;
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
  if (pNMItemActivate->iItem < 0 || pNMItemActivate->iSubItem < 0)
    return;

  m_pDealer->OnNMDblclkLst(this, pNMItemActivate->iItem, pNMItemActivate->iSubItem);
}

/*bool CCommonListCtrl::OnNMDblclkLstWithoutON_NOTIFY_REFLECT(MSG* pMsg)
{
  if (pMsg->message == WM_LBUTTONDBLCLK
      && pMsg->hwnd == m_hWnd)
  {
    int nRow = -1;
    int nCell = -1;
    if (IsPointInCell(&pMsg->pt, nRow, nCell))
      OnNMDblclkLst(nRow, nCell);
  }
  return false;
}*/

void CCommonListCtrl::OnLvnItemchangedLst(NMHDR *pNMHDR, LRESULT *pResult)
{
  if ((*m_pplstEx)->IsUpdatingView())
    return;

  if (!m_bCreated)
    return;

  if (m_bUpdating)
    return;

  *pResult = 0;
  LPNMLISTVIEW pNMCD = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  int nRow = pNMCD->iItem;
  if (nRow < 0)
    return;

  bool bSelected = (pNMCD->uNewState == (LVIS_SELECTED | LVIS_FOCUSED));
  m_pDealer->OnLvnItemchangedLst(this, nRow, bSelected);
}

void CCommonListCtrl::OnSize(UINT nType, int cx, int cy)
{
  __super::OnSize(nType, cx, cy);

  if (m_smHeader.GetSafeHwnd() != nullptr)
    m_smHeader.RedrawWindow();
}

void CCommonListCtrl::InitHeader()
{
  if (m_bMultiRowColumnHeader)
    m_smHeader.SubclassDlgItem(0, this);
}

void CCommonListCtrl::UseMultiRowColumns()
{
  m_bMultiRowColumnHeader = true;
}

CMFCHeaderCtrl& CCommonListCtrl::GetHeaderCtrl()
{
  return m_smHeader;
}

