#pragma once

#include "CommonViewForm.h"

/*
����� ��� ���������� ������ �������� ���� �������.
�.�. ��� ��������, ����������� ��� ���� ����� �������� (������� ���) ���������� �������, ����� ��� ����� ����������� � �����������
����� ���������� �������. � ����� ����������, ���-�� ������������ ������� � ��������� � �������. ��� ��������� ��������� ����� �����
������� ���������� �� ����������� �������� ���� �������.
������������, ��������, CView_TariffShedules ��� ����������� ������ �� xml �����.
*/

class CView_ReloadCurrent: public CCommonViewForm
{
  DECLARE_DYNCREATE(CView_ReloadCurrent)

public:
  CView_ReloadCurrent();
  virtual ~CView_ReloadCurrent();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_RELOAD_CURRENT_VIEW
  };

  virtual void OnInitialUpdate();
  virtual void OnDraw(CDC* pDC);
  void OnDrawBackground(CDC* pDC, CRect rect);

  CFont m_fBold;
  
#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);
public:
  //virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = nullptr);
};
