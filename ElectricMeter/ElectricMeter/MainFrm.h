#pragma once

#include "TabListCtrl.h"
#include "MFCCaptionBarExt.h"
#include "MFCOutlookBarExt.h"
#include "CommonViewForm.h"
#include "AppEnviron.h"
#include "ProtocolTypes.h"
#include "DataStore.h"
#include <vector>
#include "auxthreads.h"

#define CFrameWndEx CFrameWndEx

class CMainFrame : public CFrameWndEx
{
	friend class CElectricMeterDoc;
	friend class CView_Connection; // like CElectricMeterView (i.e. main view)

protected:

	// Create

	CMainFrame ();
	virtual ~CMainFrame ();
	DECLARE_DYNCREATE (CMainFrame)
	DECLARE_MESSAGE_MAP ()

private:
	CWinThread *m_p250msThread = nullptr;
	bool m_bTimerDisabled = false;
	
	bool m_bMainFrameWasCreated = false;
	static UINT Timer250Proc (LPVOID arg);

	afx_msg int OnCreate (LPCREATESTRUCT lpCreateStruct);
	virtual BOOL PreCreateWindow (CREATESTRUCT& cs);
	virtual BOOL LoadFrame (UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = nullptr, CCreateContext* pContext = nullptr);
	afx_msg void OnClose ();
	afx_msg void OnCloseAfterDisconnection ();
	afx_msg void OnDestroy ();
	afx_msg void OnSize (UINT nType, int cx, int cy);
	afx_msg void OnShowWindow (BOOL bShow, UINT nStatus);
	virtual BOOL PreTranslateMessage (MSG* pMsg);
	LRESULT OnAfterNewDocument (WPARAM wParam, LPARAM lParam);

public:
	void SetProductVersion (CString sVer);
	CString GetProductVersion ();
private:
	CString m_sProductVersion;

public:
	void ShowApp ();
	bool m_bStopTimer = false;
private:
	// Diagnostics

#ifdef _DEBUG
	virtual void AssertValid () const;
	virtual void Dump (CDumpContext& dc) const;
#endif

	// Application work folrers and files (xml, ini and etc.)

private:
	CAppEnviron m_appEnviron;
	LRESULT UpdateXmlBackupFile (WPARAM wParam, LPARAM lParam);

public:
	CAppEnviron* GetAppEnviron ();

	// Look

private:
	UINT	m_nAppLook;

	afx_msg void OnViewCustomize ();
	afx_msg void OnAppLook (UINT id);
	afx_msg void OnUpdateAppLook (CCmdUI* pCmdUI);

	bool m_bWaitCursor = false;
	bool m_bWaitCursor_oneWayCase = false;
	afx_msg BOOL OnSetCursor (CWnd* pWnd, UINT nHitTest, UINT message);
	LRESULT OnSetCursor_ (WPARAM wParam, LPARAM lParam);
	LRESULT OnSetCursor2_ (WPARAM wParam, LPARAM lParam);

	// Menu and ToolBar (MnFrmMenu.cpp)

private:
	CMFCMenuBar m_wndMenuBar;
	CMFCToolBar m_wndToolBar;

	afx_msg void OnUpdateTbrConnect (CCmdUI* pCmdUI);
	afx_msg void OnTbrConnect ();
	virtual BOOL OnDrawMenuImage (CDC* pDC, const CMFCToolBarMenuButton* pMenuButton, const CRect& rectImage);

	// StatusBar (MnFrmStatusBar.cpp)

private:
	CMFCStatusBar	m_wndStatusBar;
	bool CreateStatusBar ();
	int GetStatusBarIndex (UINT nResID);
	afx_msg void OnDblClickToDocumentField ();
	void SetStatusBarText (int nFieldIndex, CString sText);

	// OutlookBar (MnFrmOutlookBar.cpp)

private:
	CMFCOutlookBarExt  m_wndOutlookBar;
	CMFCOutlookBarTabCtrl* m_pShortcutsBarContainer = nullptr;
	std::vector<CTabListCtrl*> m_outlookTabs;

	BOOL CreateOutlookBar ();
	void AddOutlookTab (int nTabID);
	void FreeOutlookTab ();
	void ShowOutlookTab (int nTabID);
	void SelectFirstRowInOutlookTab (int nTabID);
	void SelectSpecRowInOutlookTab (int nTabID, int nInlayID);
	afx_msg void OnViewOutlookBar ();
	afx_msg void OnUpdateViewOutlookBar (CCmdUI* pCmdUI);
	afx_msg void OnOutlookBarShortcut (UINT id);
	LRESULT OnChangeActiveTab (WPARAM wparam, LPARAM lparam);
	LRESULT OnSelectRowInTab (WPARAM wParam, LPARAM lParam);
	LRESULT ResizeMsgFromCView (WPARAM wParam, LPARAM lParam);

	// CaptionBar (MnFrmCaptionBar.cpp)

private:
	CMFCCaptionBarExt  m_wndCaptionBar;
	CBitmap m_bmpCaption;

	afx_msg void OnViewCaptionBar ();
	afx_msg void OnUpdateViewCaptionBar (CCmdUI* pCmdUI);
	void SetCaptionBarText (CString sText);
	void CheckCaptionState ();
	void SetCaptionBtnView (ECaptionButton cb);

	int m_nIndicatorPercentRate = 0;
	void ShowIndicatorOfConnection ();
	enum ECapBarIndicatorRes { indicBreak, indicOK };
	void HideCaptionBarIndicator (ECapBarIndicatorRes indicRes);
	void PushIndicator ();
	afx_msg void OnCaptionBtn_Connect ();
	afx_msg void OnCaptionBtn_BreakConnection ();
	afx_msg void OnCaptionBtn_Disconnect ();
	LRESULT OnCaptionBtn_Disconnect (WPARAM wParam, LPARAM lParam);

	// Different Views

	void SetViewByDisconnect ();

	// Program states

public:
	enum EProgramStates
	{
		psNone, // ������ �� ��������� - ��������� ��������� ���������, ���� ��������� �� �����
		psOfflineDeviceData,
		psConnectingToDevice,
		psOnlineDeviceData,
		psCloseAppAfterDisconnect
	};
private:
	std::vector <EProgramStates> m_programStates;
public:
	void SetPrgState (EProgramStates dvs);
	bool IsPrgState (EProgramStates dvs);
	bool IsPrgState_Begin ();
	bool IsPrgState_OnlineDeviceData ();
	bool IsPrgState_ConnectingToDevice ();
	bool IsPrgState_OfflineDeviceData ();

	// Timer

	void SetTimer_ (UINT_PTR nIDEvent, UINT uElapse);
	void KillTimer_ (UINT_PTR nIDEvent);
	afx_msg void OnTimer (UINT_PTR nIDEvent);

	// Inlays (MnFrmInlays.cpp)

private:
	CView* m_pCurrActiveView = nullptr;
	void SetActiveView_ (CView* p);
	CView* GetActiveView_ ();

	int GetCurrentViewID ();
	bool IsCurrentView ();
	void SelectViewByTabItemID (int nTabItemID);
	void ViewInitialisation (CView* pView);
	LRESULT OnReloadCurrentView (WPARAM wParam, LPARAM lParam);
	aux::mutex m_mutexSwitchView;
	bool WasTabSwitchedInInlay ();

	void UpdateConnectionStateForCurrInlay ();
	void UpdateInlayByReceivedParamMeanings (unsigned int nAddress);
	void UpdateInlayByReceivedConfirmation (unsigned int nAddress, bool bResult);

	void GetSendingData (std::vector<SEND_UNIT>* pList);
	void ActivateByTimer250 ();

	// Connection with device (MnFrmDevConnector.cpp)

	void StartExchThread (EProtocolTypes pt);
	void StopExchThread ();

	void ConnectionToDevice ();
	LRESULT ReconnectWithOtherSpeed (WPARAM wParam, LPARAM lParam);
	void DisconnectionFromDevice ();

	bool CheckDeviceAddress ();

	void FreeMonitoringReq (); // �������� ������� ��� ������������ �������; ���������� �� ��������
	LRESULT OnDoExchangeByTimer (WPARAM wParam, LPARAM lParam); // ���������� �������� ������, ������������ ��������
	LRESULT OnExchangeResultNotify (WPARAM wParam, LPARAM lParam); // ��������� �� ������ ������ � ���������� ������
	LRESULT OnReflectByResultNotify (WPARAM wParam, LPARAM lParam); // ��������� ��� MainFrame ����� ��������� ��������� ������ (����� OnExchangeResultNotify)
	LRESULT ExchIdentificationPassedOk (WPARAM wParam, LPARAM lParam);

	void PrepareExchData_Protocol61107 ();
	void ParseMonitoringData_Protocol61107 (void* pData); // ��������� ��������� ������

	LRESULT UpdateAccessData (WPARAM wParam, LPARAM lParam);

	LRESULT ReadDataEvent (WPARAM wParam, LPARAM lParam);
	LRESULT WriteDataEvent (WPARAM wParam, LPARAM lParam);
	void FormExchBinData ();
	void FreeExchBinData ();

	bool m_bCanLookForKeepAlive = false;
	int m_nExchKeepAlive = 0; // ��. �������� � ������� ExchWatchDogTick(...)  
	void ExchWatchDogTick (); // by 250 ms timer
	void GetDeviceTimeLikeTestExchange_Protocol61107 ();

	// Data store (MnFrmData.cpp)

public:
	CDataStore* GetDataStore () { return &m_dataStore; }
private:
	CDataStore m_dataStore;

	LRESULT OnClearReceivedDataStoreItem (WPARAM wParam, LPARAM lParam);
	afx_msg void OnUpdateFile (CCmdUI *pCmdUI);
	afx_msg void OnUpdateFileSave (CCmdUI *pCmdUI);
	afx_msg void OnSaveDocument ();
	afx_msg void OnSaveDocumentAs ();

	afx_msg void OnOpenDocument ();
	LRESULT OnOpenSpecXmlFile (WPARAM wParam, LPARAM lParam);
	bool OpenXmlDoc ();
};

extern bool DataStoreFinder (const unsigned int nInAddress, CString& sOutData);
