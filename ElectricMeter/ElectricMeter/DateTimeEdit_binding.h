#pragma once

#include "Ai_ListCtrlEx.h"
#include "DateTimeEdit.h"

class CDateTimeEdit_binding: public CDateTimeEdit
{
public:
  CDateTimeEdit_binding(void* pBeforeSetFunc);
  ~CDateTimeEdit_binding();

  void SetLinkingListCtrEx(�Ai_ListCtrlEx* pListCtrEx)
  {
    m_pListCtrEx = pListCtrEx;
  }

private:
  �Ai_ListCtrlEx* m_pListCtrEx = nullptr;
  void(*BeforeSet)();

  virtual bool OnKeyUp_Enter() override final;
  virtual bool OnKeyUp_Esc() override final;
  virtual void SaveEditableData() override final;

  enum
  {
    keywaspressedNone,
    keywaspressedEnter,
    keywaspressedEsc,
  }
  m_keywaspressed = keywaspressedNone;
};

