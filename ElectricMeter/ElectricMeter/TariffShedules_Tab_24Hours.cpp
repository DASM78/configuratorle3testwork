#include "stdafx.h"
#include "Resource.h"
#include <algorithm>
#include "Ai_Str.h"
#include "Defines.h"
#include "View_TariffShedules.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Tab_24H
{
  #define TBL_24HOURS_POINT_MIN 1
  #define TBL_24HOURS_POINT_MAX 12
  #define TBL_24HOURS_POINT_DEFAULT 2
  #define TBL_24HOURS_SHEDULE_MIN 1
  #define TBL_24HOURS_SHEDULE_MAX 36
  #define TBL_24HOURS_SHEDULE_DEFAULT 2

  #define TBL_DEFUALT_TIME_TEXT L"00:00"
  #define TBL_DEFUALT_TARIFF_TEXT L"���"

  #define COLOR_SCHEME_01_OF_ROWS_EXCHANGEABLE COLOR_LIGHT_LIGHT_BLUE
  #define COLOR_SCHEME_02_OF_ROWS_EXCHANGEABLE COLOR_LIGHT_BLUE

  struct COLUMNS
  {
    int nScheduleNumber = -1;
    int nCount_using = TBL_24HOURS_POINT_DEFAULT; // load/save via xml file
  }
  g_columns;
  int g_nComnSignificantCell = -1;

  struct ROWS
  {
    int nCount_using = TBL_24HOURS_SHEDULE_DEFAULT; // load/save via xml file
    int nCount_using_tmp = -1; // ��������� �������� �������� �� ��������
  }
  g_rows;

  struct CELL_PROPS
  {
    bool bShow = true; // �������������� ����
    CString sText;
    bool bEditable = false;
    bool bEnable = true;
    bool bTimeCell = true; // false - Tariff
    COLORREF colorBg = RGB(0, 0, 0);
    COLORREF colorText = RGB(0, 0, 0);
    CellTextStyle textStyle = ctsNone;
  };
  
  //                                               i                                             j
  CELL_PROPS g_cellProps[TBL_24HOURS_POINT_MAX][TBL_24HOURS_SHEDULE_MAX * 2] {}; // * 2 - ����� �����
  vector<CString> g_cellProps_tmp; // ��������� �������� �������� �� ��������

  CELL_PROPS* GetCellProp(int j /*row*/, int i /*cell*/)
  {
    ASSERT(j < TBL_24HOURS_SHEDULE_MAX * 2);
    ASSERT(i < TBL_24HOURS_POINT_MAX);
    return &g_cellProps[i][j];
  }

  // pair<CELL_PROPS*, CELL_PROPS*> - first - time; second - shedule
  pair<CELL_PROPS*, CELL_PROPS*> GetRowCellUnit(int nShedule /*Attention! It's a double row*/, int nPoint)
  {
    int nTimeRowCell  = nShedule * 2; // ������ ��������� �� �����
    int nTariffRowCell = nTimeRowCell + 1; // ������ ��������� �� �����
    if (nTimeRowCell < TBL_24HOURS_SHEDULE_MAX * 2
        && nTariffRowCell < TBL_24HOURS_SHEDULE_MAX * 2
        && nPoint < TBL_24HOURS_POINT_MAX)
        return pair<CELL_PROPS*, CELL_PROPS*>(GetCellProp(nTimeRowCell, nPoint), GetCellProp(nTariffRowCell, nPoint));
    return pair<CELL_PROPS*, CELL_PROPS*>(nullptr, nullptr);
  }

  struct X_TAGS
  {       
    CString s24HoursNode = L"TariffShedules_24Hours";
    CParamsTreeNode* p24HoursNode = nullptr;

    CString sTableNode = L"TableData";
    CString sTableAttr_usingColumns = L"UsingColumns";
    CString sTableAttr_usingRows = L"UsingRows";
    CParamsTreeNode* pTableNode = nullptr;
    CString sShedule = L"Shedule_";
    CString s24H = L"Hours24_";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

  CELL_PROPS* g_pCellProp_EditedTime = nullptr;
  void BeforeSetFunc() // �������� ��� ��������� ����� ���������
  {
  }

} // namespace ns_Tab_24H

using namespace ns_Tab_24H;

int CView_TariffShedules::GetSheduleCount_From24HoursTbl()
{
  return g_rows.nCount_using;
}

void CView_TariffShedules::FreeBDCellProps_24Hours()
{
  for (int j = 0; j < TBL_24HOURS_SHEDULE_MAX * 2; j += 2) // + 2 - ����� �����
  {
    for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
    {
      GetCellProp(j, i)->sText = TBL_DEFUALT_TIME_TEXT;
      GetCellProp(j + 1, i)->sText = TBL_DEFUALT_TARIFF_TEXT;
    }
  }
}

void CView_TariffShedules::TakeValueFromXmlTree_24Hours()
{
  FreeBDCellProps_24Hours();

  CString sTag = g_xTags.s24HoursNode;
  g_xTags.p24HoursNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!g_xTags.p24HoursNode)
  {
    g_xTags.p24HoursNode = m_pTariffShedulesNode->AddNode(sTag);
    return; // ��������� �������� �� ���������
  }

  sTag = g_xTags.sTableNode;
  g_xTags.pTableNode = g_xTags.p24HoursNode->FindFirstNode(sTag);
  if (!g_xTags.pTableNode)
  {
    g_xTags.pTableNode = g_xTags.p24HoursNode;
    TakeValueFromXmlTree_24Hours_newXmlVer(); // �������� ��� ����� ������, �.�. g_xTags.sTableNode � ����� ������ ���� �� ������
    // old ver: g_xTags.pTableNode = g_xTags.p24HoursNode->AddNode(sTag);
    return; // ��������� �������� �� ���������
  }

  // ������ ������ �� �������, ��������� ��� ����, � ������� ���������

  CString sTableAttr_usingColumns = g_xTags.pTableNode->GetAttributeValue(g_xTags.sTableAttr_usingColumns);
  g_columns.nCount_using = min(CAi_Str::ToInt(sTableAttr_usingColumns), TBL_24HOURS_POINT_MAX); // ����� �� ������� �� ��������
  g_columns.nCount_using = max(g_columns.nCount_using, TBL_24HOURS_POINT_MIN); // ����� �� ������� �� �������
  CString sTableAttr_usingRows = g_xTags.pTableNode->GetAttributeValue(g_xTags.sTableAttr_usingRows);
  g_rows.nCount_using = min(CAi_Str::ToInt(sTableAttr_usingRows), TBL_24HOURS_SHEDULE_MAX); // ����� �� ������� �� ��������
  g_rows.nCount_using = max(g_rows.nCount_using, TBL_24HOURS_SHEDULE_MIN); // ����� �� ������� �� �������

  for (int j = 0; j < g_rows.nCount_using * 2; j += 2) // + 2 - ����� �����
  {
    for (int i = 0; i < g_columns.nCount_using; ++i)
    {
      CELL_PROPS* pTime = GetCellProp(j, i);
      pTime->sText = TBL_DEFUALT_TIME_TEXT;
      CELL_PROPS* pTariff = GetCellProp(j + 1, i);
      pTariff->sText = TBL_DEFUALT_TARIFF_TEXT;
    }
  }

  // ������� �� ���� pTableNode - unit-� � �������

  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    sTag = g_xTags.sShedule + CAi_Str::ToString(j + 1);
    CParamsTreeNode* pTfShd24HoursTblRowNode = g_xTags.pTableNode->FindFirstNode(sTag);
    if (pTfShd24HoursTblRowNode) // ���� ����� ������ � ����������� �� ����������, �� ��������� ��������� �� ���������
    {
      CString sColumnSettings = pTfShd24HoursTblRowNode->GetAttributeValue(g_xTags.sDataSet);
      ParseRowDataString_24Hours(sColumnSettings, j);
    }
  }
}

void CView_TariffShedules::TakeValueFromXmlTree_24Hours_newXmlVer()
{
  // ������� �� ���� pTableNode - unit-� � �������

  g_rows.nCount_using = TBL_24HOURS_SHEDULE_DEFAULT;
  g_columns.nCount_using = TBL_24HOURS_POINT_DEFAULT;

  int j = 0;
  for (; j < TBL_24HOURS_SHEDULE_MAX; ++j)
  {
    if (j == TBL_24HOURS_SHEDULE_MAX - 1)
      break;

    CString sTag;
    sTag.Format(L"%s%.2d", g_xTags.sShedule, j + 1);
    CParamsTreeNode* pTfShd24HoursTblRowNode = g_xTags.pTableNode->FindFirstNode(sTag);
    if (!pTfShd24HoursTblRowNode)
    {
      sTag.Format(L"%s%.2d", g_xTags.s24H, j + 1); // new ver
      pTfShd24HoursTblRowNode = g_xTags.pTableNode->FindFirstNode(sTag);
      if (!pTfShd24HoursTblRowNode)
        break;
    }

    CString sColumnSettings = pTfShd24HoursTblRowNode->GetAttributeValue(g_xTags.sDataSet);
    ParseRowDataString_24Hours_newXmlVer(sColumnSettings, j);
  }

  if (j == 0)
    j = TBL_24HOURS_POINT_DEFAULT;
  g_rows.nCount_using = max(j, TBL_24HOURS_SHEDULE_MIN); // ����� �� ������� �� �������
}

void CView_TariffShedules::ParseRowDataString_24Hours(CString& sRowData, int j)
{
  vector <CString> cellSettings;
  if (CAi_Str::CutString(sRowData, L";", &cellSettings)
      && cellSettings.size() > 1)
  {
    int nCorrectColumnCount = CAi_Str::ToInt(cellSettings[0]);
    cellSettings.erase(cellSettings.begin());

    size_t minCell = (size_t)min(g_columns.nCount_using, nCorrectColumnCount);
    minCell = (size_t)min(minCell, cellSettings.size());

    for (size_t i = 0; i < minCell; ++i)
    {
      vector <CString> timeAndTariff;
      if (CAi_Str::CutString(cellSettings[i], L",", &timeAndTariff)
          && timeAndTariff.size() == 2)
      {
        CString sTime = timeAndTariff[0];
        CString sTariff = timeAndTariff[1];
        pair<CELL_PROPS*, CELL_PROPS*> unit = GetRowCellUnit(j, i);
        if (unit.first != nullptr
            && unit.second != nullptr)
        {
          if (i == 0) // ��� ������� ������� ����� ������ 00:00 � ���� �� XML, �� �����-��(!) ������� ������ ���-�� ������, ����� ��� ���������
            sTime = TBL_DEFUALT_TIME_TEXT;

          unit.first->sText = sTime;
          unit.second->sText = sTariff.TrimLeft(L"0");
        }
      }
    }
  }
}

void CView_TariffShedules::ParseRowDataString_24Hours_newXmlVer(CString& sRowData, int j)
{
  vector <CString> cellSettings;
  if (CAi_Str::CutString(sRowData, L";", &cellSettings)
      && cellSettings.size() > 1)
  {
    int nColumnCount = static_cast<int>(cellSettings.size());
    size_t nMinCellCount = (size_t)min(TBL_24HOURS_POINT_MAX, nColumnCount); // ����� �� ������� �� �������� (���� ������ � xml ������ ����������� - ��� ���������)
    g_columns.nCount_using = max(TBL_24HOURS_POINT_MIN, static_cast<int>(nMinCellCount)); // ����� ���������� ���������� ������������ � ������������ ������ 

    for (size_t i = 0; i < nMinCellCount; ++i)
    {
      vector <CString> timeAndTariff;
      if (CAi_Str::CutString(cellSettings[i], L",", &timeAndTariff)
          && timeAndTariff.size() == 2)
      {
        CString sTime = timeAndTariff[0];
        CString sTariff = timeAndTariff[1];
        pair<CELL_PROPS*, CELL_PROPS*> unit = GetRowCellUnit(j, i);
        if (unit.first != nullptr
            && unit.second != nullptr)
        {
          if (i == 0) // ��� ������� ������� ����� ������ 00:00 � ���� �� XML, �� �����-��(!) ������� ������ ���-�� ������, ����� ��� ���������
            sTime = TBL_DEFUALT_TIME_TEXT;

          unit.first->sText = sTime;
          unit.second->sText = sTariff.TrimLeft(L"0");
        }
      }
    }
  }
}

void CView_TariffShedules::PutValueToXmlTree_24Hours()
{
  CString sTag = g_xTags.s24HoursNode;
  g_xTags.p24HoursNode = m_pTariffShedulesNode->FindFirstNode(sTag);
  if (!g_xTags.p24HoursNode)
    g_xTags.p24HoursNode = m_pTariffShedulesNode->AddNode(sTag);

  //old ver: sTag = g_xTags.sTableNode;
  g_xTags.p24HoursNode->DeleteAllNodes();
  //old ver: g_xTags.pTableNode = g_xTags.p24HoursNode->AddNode(sTag);
  g_xTags.pTableNode = g_xTags.p24HoursNode;

  /*
  ��������� �������� sTableAttr_usingColumns � sTableAttr_usingRows, ��-����, ��������� (��������� ������������� ����������),
  �.�. ��������� � ��������� ���������� ����� �������:
   - ������������ (�������) ������ - �� ���������� ����� g_xTags.sShedule (��. ����), ��� ����� ����������� ������������ �����;
   - ������������ (�������) ������� - ��������� �������� ��������� g_xTags.sDataSet.
  ������ ��������� �������� ��������� ��� ����������� � ��������.
  */
  //old ver: g_xTags.pTableNode->AddAttribute(g_xTags.sTableAttr_usingColumns, CAi_Str::ToString(g_columns.nCount_using));
  //old ver: g_xTags.pTableNode->AddAttribute(g_xTags.sTableAttr_usingRows, CAi_Str::ToString(g_rows.nCount_using));

  // ���������� � ���� pTableNode - unit-�� � �������

  vector<CString> res;
  FormStringFrom_24Hours(res, false_d(L"�������� ������ �� ������"), true_d(L"for xml file"));

  for (int j = 0; j < (int)res.size(); ++j)
  {
    CString sTagSheduleN;
    sTagSheduleN.Format(L"%s%.2d" , g_xTags.s24H, j + 1);
    CParamsTreeNode* pNode_SheduleN = g_xTags.pTableNode->AddNode(sTagSheduleN);
    CString sValue = res[j];
    pNode_SheduleN->AddAttribute(g_xTags.sDataSet, sValue);
  }
}

CString CView_TariffShedules::GetXmlTag_24Hours()
{
  return g_xTags.s24HoursNode;
}

void CView_TariffShedules::FormStringFrom_24Hours(vector<CString>& res, bool bCheck, bool bForXml)
{
  int nCorrectColumnCount = g_columns.nCount_using;

  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    if (bCheck)
    {
      nCorrectColumnCount = 0;

      for (int i = 0; i < g_columns.nCount_using; ++i)
      {
        pair<CELL_PROPS*, CELL_PROPS*> unit = GetRowCellUnit(j, i);
        CString sTariff = unit.second->sText;
        if (sTariff == TBL_DEFUALT_TARIFF_TEXT) // "���"
          break; // ���� �������� "���", �� � ������ ������� � ����������� �� ����� ��� ������ � ������� ��� ������� ������
        ++nCorrectColumnCount;
      }
    }

    if (nCorrectColumnCount > 0)
    {
      CString sValue;
      if (!bForXml)
        sValue = CAi_Str::ToString(nCorrectColumnCount);

      for (int i = 0; i < nCorrectColumnCount; ++i)
      {
        if (bForXml)
        {
          if (i > 0)
            sValue += L";";
        }
        else
          sValue += L";";

        pair<CELL_PROPS*, CELL_PROPS*> unit = GetRowCellUnit(j, i);
        CString& sTime = unit.first->sText;
        CString sTariff = unit.second->sText;

        if (sTariff.GetLength() == 1)
          sTariff = L"0" + sTariff;
        sValue += (sTime + L"," + sTariff);
      }
      res.push_back(sValue);
    }
  }
}

void CView_TariffShedules::Create24HoursTab()
{
  m_plst24Hours->ShowHorizScroll(true);

  m_plst24HoursEx = new �Ai_ListCtrlEx();
  m_plst24HoursEx->SetList(m_plst24Hours, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT |
    LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plst24HoursEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  m_plst24Hours->MyPreSubclassWindow();

  bool m_bGridLinesStyle = false;
  if (!m_bGridLinesStyle)
    m_plst24Hours->SetExtendedStyle(m_plst24Hours->GetExtendedStyle() & ~LVS_EX_GRIDLINES);
  m_plst24Hours->EnableInterlacedColorScheme(false);

  m_plst24Hours->m_bShowHeaderContextMenu = false;
  m_plst24Hours->m_bCanHeaderDrag = false;

  m_plst24HoursEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  g_columns.nScheduleNumber = m_plst24HoursEx->SetColumnEx(L"������", LVCFMT_LEFT);
  m_plst24HoursEx->SetColumnWidthStyle(g_columns.nScheduleNumber, CS_WIDTH_FIX, 100);

  CString sr1 = L"������"; // ����� ����� "�" �� �������
  CString se1 = L"�����o"; // ����� ����� "�" �� ����������; ����� "������" ����������� � ���� ������ �����
  CString s2 = L"�����";
  for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
  {
    CString s1 = sr1;
    if (i % 2)
      s1 = se1;

    int nPoint = m_plst24HoursEx->SetColumnEx(s1 + L"\n" + s2, LVCFMT_CENTER);
    m_plst24HoursEx->SetColumnWidthStyle(nPoint, CS_WIDTH_FIX, 80);
  }

  m_plst24HoursEx->MatchColumns();

  for (int i = TBL_24HOURS_POINT_MAX; i > g_columns.nCount_using; i--)
  {
    int nColumns = g_columns.nScheduleNumber + i;
    m_plst24Hours->ShowColumn(nColumns, false);
  }

  int nNodeIdx = m_plst24HoursEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  g_nComnSignificantCell = g_columns.nScheduleNumber + 1;
  m_plst24HoursEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, g_nComnSignificantCell);

  vector<CString> textOfCells_time;
  textOfCells_time.push_back(L"������ ");
  for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
  {
    CString sCellText;
    if (i < g_columns.nCount_using)
      sCellText = TBL_DEFUALT_TIME_TEXT;
    textOfCells_time.push_back(sCellText);
  }
  
  vector<CString> textOfCells_tariff;
  textOfCells_tariff.push_back(L"");
  for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
  {
    CString sCellText;
    if (i < g_columns.nCount_using)
      sCellText = TBL_DEFUALT_TARIFF_TEXT;
    textOfCells_tariff.push_back(sCellText);
  }

  bool bLightRowColor = true;
  COLORREF colorLight = COLOR_SCHEME_01_OF_ROWS_EXCHANGEABLE;
  COLORREF colorDark = COLOR_SCHEME_02_OF_ROWS_EXCHANGEABLE;
  COLORREF color = colorLight;
  COLORREF colorDisableEditableText = COLOR_DISABLE_EDITABLE_CELL_TEXT;
  COLORREF colorEditableText = COLOR_EDITABLE_CELL_TEXT;
  
  m_pedtTime_In24HoursTbl = new CDateTimeEdit_binding(nullptr);
  m_pedtTime_In24HoursTbl->CreateEx(WS_EX_CLIENTEDGE, L"EDIT", nullptr, WS_CHILD | ES_AUTOHSCROLL | ES_CENTER, CRect(0, 0, 0, 0), m_plst24Hours, ID_EDT_TIME_IN_24HOURS_TBL_CTRL);
  m_pedtTime_In24HoursTbl->SetFont(GetFont());
  m_pedtTime_In24HoursTbl->SetMask(L"hh:mm");
  m_pedtTime_In24HoursTbl->SetLinkingListCtrEx(m_plst24HoursEx);
  BIND_TO_CELL bindColumnList_time{};
  for (int i = 0; i < TBL_24HOURS_POINT_MAX - 1; ++i) // - 1: ������ ������� - ����� �� �������������
  {
    int nCell = g_columns.nScheduleNumber + 2 + i; // + 2: ������ ������� - ����� �� �������������
    BIND_CTRL_TO_CELL bindCtrl_time{};
    bindCtrl_time.bDisableBind = false;
    bindCtrl_time.nCell = nCell;
    bindCtrl_time.strByDefault = TBL_DEFUALT_TIME_TEXT;
    bindCtrl_time.pEdit = m_pedtTime_In24HoursTbl;
    bindColumnList_time.ctrlToCell.push_back(bindCtrl_time);
  }

  m_pcbxTariffs_In24HoursTbl = new CComboBox_binding();
  m_pcbxTariffs_In24HoursTbl->Create_(m_plst24Hours, ID_CBX_TARIFFS_IN_24HOURS_TBL_CTRL, false_d(L"sort"), m_plst24HoursEx);
  BIND_TO_CELL bindColumnList_tariff{};
  for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
  {
    int nCell = g_nComnSignificantCell + i;
    BIND_CTRL_TO_CELL bindCtrl_tariff{};
    bindCtrl_tariff.bDisableBind = false;
    bindCtrl_tariff.nCell = nCell;
    bindCtrl_tariff.strByDefault = TBL_DEFUALT_TARIFF_TEXT;
    bindCtrl_tariff.pCmbBox = m_pcbxTariffs_In24HoursTbl;
    bindColumnList_tariff.ctrlToCell.push_back(bindCtrl_tariff);
  }

  for (int j = 0, nOrder = 1; j < TBL_24HOURS_SHEDULE_MAX * 2; j += 2, ++nOrder) // * 2 - ����� �����
  { // ����������� ����� ��� ������, �� �� default ��� ����� ��������� SetChild, � ���������, ��� ������� SetEmptyChild

    bool bStrongRow = true;
    textOfCells_time[0] = L"������ " + CAi_Str::ToString(nOrder);
    if (nOrder > g_rows.nCount_using)
    {
      bStrongRow = false;
      textOfCells_time[g_nComnSignificantCell] = L""; // � significant cell ������ ����� ��� SetEmptyChild
      textOfCells_tariff[g_nComnSignificantCell] = L""; // � significant cell ������ ����� ��� SetEmptyChild
    }

    bLightRowColor = !bLightRowColor;
    color = bLightRowColor ? colorLight : colorDark;

    // ���������� ������ "������" � �������������� �������� �������

        //... ����� ������������ ���������� ������� ����������� �� XML �����

    for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
    {
      CELL_PROPS* pCellProp = GetCellProp(j, i);
      pCellProp->colorBg = color;
      pCellProp->bEditable = true;
      pCellProp->colorText = colorEditableText;
      
      // ������������� ����� ������ ��������������� � ����� �� ����
      // � ����� � ������ ������� ������ 00:00 � �� �������������
      pCellProp->bEditable = (i == 0) ? false : true; 
      pCellProp->colorText = (i == 0) ? colorDisableEditableText : colorEditableText;

      pCellProp->textStyle = (i == 0) ? ctsNone : ctsBold;

      bool bStrongCell = (i < g_columns.nCount_using);
      if (bStrongRow // ������ ����� �����
          && bStrongCell) // ������� ����� �������
        textOfCells_time[g_nComnSignificantCell + i] = pCellProp->sText;
    }

    int nRow_inList = -1;
    if (bStrongRow) // ������ ����� �����
      nRow_inList = m_plst24HoursEx->SetChild(nNodeIdx, &textOfCells_time, 0, &bindColumnList_time);
    else
      nRow_inList = m_plst24HoursEx->SetEmptyChild(nNodeIdx, &textOfCells_time, 0, &bindColumnList_time);
    
    // ���������� ������ � �������������� �������� �������

        //... ����� ������������ ���������� ������� ����������� �� XML �����

    for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
    {
      CELL_PROPS* pCellProp = GetCellProp(j + 1, i);
      pCellProp->colorBg = color;
      pCellProp->bEditable = true;
      pCellProp->colorText = colorEditableText;
      pCellProp->textStyle = ctsBold;

      bool bStrongCell = (i < g_columns.nCount_using);
      if (bStrongRow // ������ ����� �����
          && bStrongCell) // ������� ����� �������
      {
        CString sTariff = pCellProp->sText;
        if (sTariff != TBL_DEFUALT_TARIFF_TEXT)
          sTariff = (L"����� " + sTariff);
        textOfCells_tariff[g_nComnSignificantCell + i] = sTariff;
      }
    }

    if (bStrongRow) // ������ ����� �����
      nRow_inList = m_plst24HoursEx->SetChild(nNodeIdx, &textOfCells_tariff, 0, &bindColumnList_tariff);
    else
      nRow_inList = m_plst24HoursEx->SetEmptyChild(nNodeIdx, &textOfCells_tariff, 0, &bindColumnList_tariff);
  }

  UpdateView_In24HoursTbl();
  m_plst24Hours->CtrlWasCreated();

#ifdef _DEBUG
  MeaningsWasChanged_In24HoursTbl();
#endif
}

void CView_TariffShedules::OnDblClick_In24HoursTable(int nRow_inView, int nCell)
{
  int j = nRow_inView;
  int i = nCell - g_nComnSignificantCell;
  CELL_PROPS* pCellProp = GetCellProp(j, i);
  if (pCellProp->bEditable)
  {
    g_pCellProp_EditedTime = nullptr;
    if (pCellProp->bTimeCell)
      g_pCellProp_EditedTime = pCellProp;
    
    m_plst24HoursEx->OnDblClick(nRow_inView, nCell);
  }
}

void CView_TariffShedules::CheckSettBtnEnable_For24HoursTbl()
{
  if (g_rows.nCount_using >= TBL_24HOURS_SHEDULE_MIN
      && g_rows.nCount_using < TBL_24HOURS_SHEDULE_MAX)
      m_btnAddRow.EnableWindow(TRUE);
  else
    m_btnAddRow.EnableWindow(FALSE);

  if (g_rows.nCount_using > TBL_24HOURS_SHEDULE_MIN
      && g_rows.nCount_using <= TBL_24HOURS_SHEDULE_MAX)
      m_btnDelRow.EnableWindow(TRUE);
  else
    m_btnDelRow.EnableWindow(FALSE);

  if (g_columns.nCount_using >= TBL_24HOURS_POINT_MIN
      && g_columns.nCount_using < TBL_24HOURS_POINT_MAX)
    m_btnAddColumn.EnableWindow(TRUE);
  else
    m_btnAddColumn.EnableWindow(FALSE);

  if (g_columns.nCount_using > TBL_24HOURS_POINT_MIN
      && g_columns.nCount_using <= TBL_24HOURS_POINT_MAX)
    m_btnDelColumn.EnableWindow(TRUE);
  else
    m_btnDelColumn.EnableWindow(FALSE);
}

void CView_TariffShedules::OnBnClickedBtnAddColumn_In24HoursTbl()
{
  if (g_columns.nCount_using < TBL_24HOURS_POINT_MAX)
  {
    ++g_columns.nCount_using;
    int nColumns = g_columns.nScheduleNumber + g_columns.nCount_using;
    m_plst24Hours->ShowColumn(nColumns, true);
    SetTextInColumnsAfterChangeVisible_In24HoursTbl(nColumns, true_d(L"set default text in cells"));
    UpdateView_In24HoursTbl(); // ����� ������� ���� ��� ������������� bShow
    MeaningsWasChanged_In24HoursTbl();
    CheckSettBtnEnable_For24HoursTbl();
  }
}

void CView_TariffShedules::OnBnClickedBtnDelColumn_In24HoursTbl()
{
  if (g_columns.nCount_using > TBL_24HOURS_POINT_MIN)
  {
    int nColumns = g_columns.nScheduleNumber + g_columns.nCount_using;
    m_plst24Hours->ShowColumn(nColumns, false);
    g_columns.nCount_using--;
    SetTextInColumnsAfterChangeVisible_In24HoursTbl(nColumns, false_d(L"clear text in cells"));
    UpdateView_In24HoursTbl(); // ����� ������� ���� ��� ������������� bShow
    MeaningsWasChanged_In24HoursTbl();
    CheckSettBtnEnable_For24HoursTbl();
  }
}

void CView_TariffShedules::SetTextInColumnsAfterChangeVisible_In24HoursTbl(int nCol, bool bSet)
{
  int i = g_columns.nCount_using - 1;
  int nCell = nCol;
  for (int j = 0; j < TBL_24HOURS_SHEDULE_MAX * 2; j += 2) // * 2 - ����� �����
  {
    int nRow_inList = j + 1;
    int j_ = j;
    CString sCellText = bSet ? TBL_DEFUALT_TIME_TEXT : L"";

    for (int unit = 0; unit < 2; ++unit)
    {
      CELL_PROPS* pCellProp = GetCellProp(j_, i);
      pCellProp->sText = sCellText;
      m_plst24HoursEx->SetCellText(nRow_inList, nCell, sCellText);

      sCellText = bSet ? TBL_DEFUALT_TARIFF_TEXT : L"";
      ++j_;
      nRow_inList += 1;
    }
  }
}

void CView_TariffShedules::Check_In24HoursTbl(CString& sErrorMsg)
{ // �� ��������: ��. �������� �������� � ����� [filter: __info/]TariffShedules.txt

  CString sErrorMsgTmp = L"� ���������� ������� \"�������� ������� �������\" ���������� ������������ ������.\n\n";
  auto lmdError = [&](int iRow, size_t iCol, CString s)->void
  {
    CString sMsg;
    sMsg.Format(L"������ - %d.\n������� \"������ �����\" - %d.\n������� - %s", iRow, iCol, s);
    sErrorMsg = sErrorMsgTmp + sMsg;
  };

  typedef pair<CELL_PROPS*, CELL_PROPS*> unit_t;
  vector<vector<unit_t>> units_dpsS; // diapasonS

  for (int j = 0; j < g_rows.nCount_using; ++j)
  {
    int nRow = j + 1;
    vector<unit_t> units; // ��� �������� ���� <00:00/���> ���� <�����/������> �� ��������

    int i = 0;
    for (; i < g_columns.nCount_using; ++i) // ������� ��������
      units.push_back(GetRowCellUnit(j, i));

    // �������� ����, ����� ���� ������ ���� <00:00/���> ���� <�����/������>

    CString s0 = TBL_DEFUALT_TIME_TEXT;
    CString sNo = TBL_DEFUALT_TARIFF_TEXT;
    const auto iBegin = units.begin();
    const auto iEnd = units.end();

    i = 0;
    auto iX = find_if(iBegin, iEnd, [&](const unit_t& u)
    {
      if (i++ == 0) // ���� ��� �� ������ ����, �� ������ ���� ������ <00:00/������>
      {
        if (u.first->sText == s0 && u.second->sText != sNo) return false; // ok
      }
      else // ���� ��� �� ������ ����, �� ������ ���� ������ <�����/������>
      {
        if (u.first->sText == s0 && u.second->sText == sNo) return false; // ok; � �� ������� ������� ��� ���������
        if (u.first->sText != s0 && u.second->sText != sNo) return false; // ok
      }
      return true; // wrong
    });
    if (iX != iEnd)
    {
      lmdError(nRow, iX - iBegin + 1, L"������ ����� ��� ������ ���� ����� ��� �������.");
      return;
    }

    // ����� ���������� ��������: <00:00/���>

    vector<unit_t> units_dps; // diapason

     iX = find_if(iBegin, iEnd, [&](const unit_t& u)
    {
      if (u.first->sText == s0 && u.second->sText == sNo) return true; // wrong
      else units_dps.push_back(u);
      return false; // ok
    });
    if (units_dps.empty())
    {
      ASSERT(false); // ��� ������ ����������� �������������� ����������
      lmdError(nRow, iX - iBegin + 1, L"������ ������.");
      return;
    }

    // ���� ������ �� ����������� - ������ ������� �����, ��� ��� ������ ���� <00:00/���>

    if (iX != iEnd)
    {
      iX = find_if(iX, iEnd, [&](const unit_t& u)
      {
        if (u.first->sText != s0 || u.second->sText != sNo) return true; // wrong
        return false; // ok
      });
      if (iX != iEnd)
      {
        lmdError(nRow, iX - iBegin + 1, L"������ ��������� ������ ����� ��������� �������� �������������.");
        return;
      }
    }

    // �������� �������. ��������� ��� ����� ���� �� �����������

    const auto iBeginDps = units_dps.begin();
    const auto iEndDps = units_dps.end();
    CDateTimeEdit tmeUtil;
    tmeUtil.SetMask(L"hh:mm", false_d("��������� ctrl"));
    COleDateTime tmeUnitPrev;
    i = 0;
    iX = find_if(iBeginDps, iEndDps, [&](const unit_t& u)
    {
      COleDateTime tmeUnitCurr = tmeUtil.GetDateTime(u.first->sText);
      if (i++ > 0 && tmeUnitPrev >= tmeUnitCurr) return true; // wrong
      tmeUnitPrev = tmeUtil.GetDateTime(u.first->sText);
      return false; // ok
    });
    if (iX != iEndDps)
    {
      lmdError(nRow, iX - iBeginDps + 1, L"����� ���� �� �� �����������.");
      return;
    }

    // �������� ���������. �������� �� ��, ��� �������� �������� ��� ����������� � ���������

    const auto iBeginDpsS = units_dpsS.begin();
    const auto iEndDpsS = units_dpsS.end();
    auto iXS = find_if(iBeginDpsS, iEndDpsS, [&](const vector<unit_t>& dpsColl) // �������� �� ���������
    {
      if (units_dps.size() != dpsColl.size()) return false; // ok
      for (size_t cmp = 0; cmp < units_dps.size(); ++cmp)
      {
        if (units_dps.at(cmp).first->sText != dpsColl.at(cmp).first->sText
            || units_dps.at(cmp).second->sText != dpsColl.at(cmp).second->sText)
            return false; // ok
      }
      return true; // wrong
    });
    if (iXS != iEndDpsS)
    {
      CString sMsg;
      sMsg.Format(L"������� - %d � %d.\n������� - ������� ���������.", iXS - iBeginDpsS + 1, units_dpsS.size() + 1);
      sErrorMsg = sErrorMsgTmp + sMsg;
      return;
    }

    // �������� �������� � ���������

    units_dpsS.push_back(units_dps);

  } // look over of row (������� �����)
}

void CView_TariffShedules::OnBnClickedBtnAddRow_In24HoursTbl()
{
  if (g_rows.nCount_using < TBL_24HOURS_SHEDULE_MAX)
  {
    ++g_rows.nCount_using;

    int nResult = -1;
    int nNodeRow_inView = 0;
    CString sCellText = TBL_DEFUALT_TIME_TEXT;
    for (int unit = 0; unit < 2; ++unit) // ������ ���� �����
    {
      nResult = m_plst24HoursEx->InsertTextToChild(nNodeRow_inView, g_nComnSignificantCell, sCellText);
      if (nResult != RCHI_SUCCESS)
      {
        ASSERT(false);
        g_rows.nCount_using--;
        break;
      }
      
      int nRow_inView = m_plst24HoursEx->GetLastInsertedChildIdx_inList() - 1;
      for (int cell = 0; cell < TBL_24HOURS_POINT_MAX; ++cell) // �������� ���������, �� g_nComnSignificantCell, �����
      {
        int i = cell;
        int j = nRow_inView;
        CELL_PROPS* pCellProp = GetCellProp(j, i);
        pCellProp->sText = sCellText;
        int nCell = cell + g_nComnSignificantCell;
        m_plst24HoursEx->SetCellText(nRow_inView + 1, nCell, sCellText);
      }

      sCellText = TBL_DEFUALT_TARIFF_TEXT;
    }

    if (nResult == RCHI_SUCCESS)
    {
      UpdateView_In24HoursTbl();
      m_plst24HoursEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, (g_rows.nCount_using * 2) - 2, false_d(L"mouse move"));

      MeaningsWasChanged_In24HoursTbl();
    }
    CheckSettBtnEnable_For24HoursTbl();
  }
}

void CView_TariffShedules::OnBnClickedBtnDelRow_In24HoursTbl()
{
  if (g_rows.nCount_using > TBL_24HOURS_POINT_MIN)
  {
    if (IsSheduleUsing(g_rows.nCount_using))
    {
      AfxMessageBox(L"������ " + CAi_Str::ToString(g_rows.nCount_using) + L" ������������!", MB_ICONWARNING);
      return;
    }

    int nRow_inView = (g_rows.nCount_using * 2) - 1;
    for (int unit = 0; unit < 2; ++unit) // ������ ���� �����
    {
      m_plst24HoursEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, g_nComnSignificantCell, -1);
      for (int cell = 0; cell < TBL_24HOURS_POINT_MAX; ++cell) // �������� ���������, �� g_nComnSignificantCell, ����� 
      {
        int i = cell;
        int j = nRow_inView;
        CELL_PROPS* pCellProp = GetCellProp(j, i);
        pCellProp->sText = L"";
        int nCell = cell + g_nComnSignificantCell;
        m_plst24HoursEx->SetCellText(nRow_inView + 1, nCell, L"");
      }
      nRow_inView--;
    }
    g_rows.nCount_using--;
    UpdateView_In24HoursTbl(); // ����� ������� ���� ��� ������������� bShow

    m_plst24HoursEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, (g_rows.nCount_using * 2) - 2, false_d(L"mouse move"));

    MeaningsWasChanged_In24HoursTbl();
  }
  CheckSettBtnEnable_For24HoursTbl();
}

void CView_TariffShedules::UpdateView_In24HoursTbl(bool bUpdateCellText)
{
  m_plst24HoursEx->UpdateView();

  int nShedRow = 0;
  for (int row = 0; row < TBL_24HOURS_SHEDULE_MAX * 2; ++row) // * 2 - ����� �����
  {
    ++nShedRow;
    if (nShedRow == 3)
      nShedRow = 1;

    int nRow_inList = row + 1;
    int j = row;

    m_plst24HoursEx->SetRowBgColor(nRow_inList, GetCellProp(j, 0)->colorBg); // ������������ - ����� ��������� �����

    for (int cell = 0; cell < TBL_24HOURS_POINT_MAX; ++cell) // �������� ���������, �� g_nComnSignificantCell, ����� 
    {
      int i = cell;
      int nCell = i + g_nComnSignificantCell;
      CELL_PROPS* pCellProp = GetCellProp(j, i);

      if (!m_plst24HoursEx->IsRowVisible(nRow_inList))
        pCellProp->bShow = false;
      else
      {
        pCellProp->bShow = true;
        if (pCellProp->bEditable)
        {
          m_plst24HoursEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          m_plst24HoursEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
          if (!pCellProp->bEnable)
            m_plst24HoursEx->SetCellDisable(nRow_inList, nCell);
        }
        else
        {
          if (pCellProp->textStyle != ctsNone)
            m_plst24HoursEx->SetCellTextStyle(nRow_inList, nCell, pCellProp->textStyle);
          if (pCellProp->colorText > 0)
            m_plst24HoursEx->SetCellTextColor(nRow_inList, nCell, pCellProp->colorText);
        }

        if (bUpdateCellText)
        {
          CString sCellText = m_plst24HoursEx->GetCellText(nRow_inList, nCell);
          if (sCellText != pCellProp->sText)
          {
            CString sText = pCellProp->sText;
            if (nShedRow == 2
                && sText != TBL_DEFUALT_TARIFF_TEXT)
                sText = L"����� " + sText;
            m_plst24HoursEx->SetCellText(nRow_inList, nCell, sText);
          }
        }
      }
    }
  }

  m_plst24HoursEx->UpdateView();
}

void CView_TariffShedules::MeanWasSeted_In24HoursTbl()
{
  MeaningsWasChanged_In24HoursTbl();
}

void CView_TariffShedules::MeaningsWasChanged_In24HoursTbl()
{
  GetCutOfMeanings_In24HoursTbl();
  PutValueToXmlTree_24Hours();
  UpdateXmlBackupFile();
}

void CView_TariffShedules::GetCutOfMeanings_In24HoursTbl()
{
  for (int j = 0; j < TBL_24HOURS_SHEDULE_MAX * 2; j += 2) // * 2 - ����� �����
  {
    int nRow_inList_Time = j + 1;
    if (!m_plst24HoursEx->IsRowVisible(nRow_inList_Time))
      break;

    for (int i = 0; i < TBL_24HOURS_POINT_MAX; ++i)
    {
      CELL_PROPS* pCellProp_Time = GetCellProp(j, i);
      CELL_PROPS* pCellProp_Tariff = GetCellProp(j + 1, i);
      int nCell = i + g_nComnSignificantCell;

      pCellProp_Time->sText = m_plst24HoursEx->GetCellText(nRow_inList_Time, nCell);
      int nRow_inList_Tariff = nRow_inList_Time + 1;
      pCellProp_Tariff->sText = m_plst24HoursEx->GetCellText(nRow_inList_Tariff, nCell);
      pCellProp_Tariff->sText.TrimLeft(L"����� ");
    }
  }
}

void CView_TariffShedules::ClearAllReqDataInDB_For24HoursTbl()
{
  FormListForClearReceivedData(FRAME_TYPE_SCHED_CNT);
  for (int i = (int)FRAME_TYPE_SCHED_01; i < (int)FRAME_TYPE_SCHED_36; ++i)
    FormListForClearReceivedData((teFrameTypes)i);
}

void CView_TariffShedules::ReceiveData_For24HoursTbl()
{
  g_cellProps_tmp.clear();
  g_rows.nCount_using_tmp = -1;

  SetParamTypeForRead(FRAME_TYPE_SCHED_CNT, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
}

void CView_TariffShedules::UpdateParamMeaningsByReceivedDeviceData_24Hours(teFrameTypes ft)
{
  if (ft == FRAME_TYPE_SCHED_CNT)
  {
    varMeaning_t m = FindReceivedData(FRAME_TYPE_SCHED_CNT);
    if (m != L"?")
    {
      int nNewRowCount = CAi_Str::ToInt(m);
      g_rows.nCount_using_tmp = min(nNewRowCount, TBL_24HOURS_SHEDULE_MAX); // ����� �� ������� �� ��������
      g_rows.nCount_using_tmp = max(g_rows.nCount_using_tmp, TBL_24HOURS_SHEDULE_MIN); // ����� �� ������� �� �������
    }
    return;
  }

  if (ft >= FRAME_TYPE_SCHED_01
      && ft <= FRAME_TYPE_SCHED_36)
  {
    varMeaning_t m = FindReceivedData(ft);
    g_cellProps_tmp.push_back(m);
  }

  if (g_rows.nCount_using_tmp == g_cellProps_tmp.size()) // ��� ��������, �������� �������
  {
    if (g_rows.nCount_using_tmp > g_rows.nCount_using)
    {
      int nCount = g_rows.nCount_using_tmp - g_rows.nCount_using;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnAddRow_In24HoursTbl();
    }
    if (g_rows.nCount_using_tmp < g_rows.nCount_using)
    {
      int nCount = g_rows.nCount_using - g_rows.nCount_using_tmp;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnDelRow_In24HoursTbl();
    }

    int nCount_using_tmp = GetMaxColumnCount_InReceivedData_24Hours();
    FormRightSideByMaxColumn_24Hours(nCount_using_tmp);
    nCount_using_tmp = min(nCount_using_tmp, TBL_24HOURS_POINT_MAX); // ����� �� ������� �� ��������
    nCount_using_tmp = max(nCount_using_tmp, TBL_24HOURS_POINT_MIN); // ����� �� ������� �� �������

    if (nCount_using_tmp > g_columns.nCount_using)
    {
      int nCount = nCount_using_tmp - g_columns.nCount_using;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnAddColumn_In24HoursTbl();
    }
    if (nCount_using_tmp < g_columns.nCount_using)
    {
      int nCount = g_columns.nCount_using - nCount_using_tmp;
      for (int i = 0; i < nCount; ++i)
        OnBnClickedBtnDelColumn_In24HoursTbl();
    }

    for (size_t j = 0; j < g_cellProps_tmp.size(); ++j)
      ParseRowDataString_24Hours(g_cellProps_tmp[j], j);

    UpdateView_In24HoursTbl(true_d("�������� ����� � ������"));
    MeaningsWasChanged_In24HoursTbl();
  }
}

bool CView_TariffShedules::IsReqItems_24Hours()
{
  return (g_rows.nCount_using_tmp > 0);
}

int CView_TariffShedules::GetMaxColumnCount_InReceivedData_24Hours()
{
  int nColumnsCount = TBL_24HOURS_POINT_MIN;

  for (size_t j = 0; j < g_cellProps_tmp.size(); ++j)
  {
    vector <CString> cellSettings;
    if (CAi_Str::CutString(g_cellProps_tmp[j], L";", &cellSettings)
        && cellSettings.size() <= 1)
    {
      ASSERT(false);
      return TBL_24HOURS_POINT_MIN;
    }
    nColumnsCount = max(nColumnsCount, CAi_Str::ToInt(cellSettings[0])); // ����� ������������
    nColumnsCount = min(nColumnsCount, TBL_24HOURS_POINT_MAX); // �� ��������� �� TBL_24HOURS_POINT_MAX
  }

  return nColumnsCount;
}

// ��� ��������� ������ ������� �� ��������, ���� ������ ������� �������� ���� ������
void CView_TariffShedules::FormRightSideByMaxColumn_24Hours(int nMaxColumn)
{
  for (size_t j = 0; j < g_cellProps_tmp.size(); ++j)
  {
    vector <CString> cellSettings;
    CAi_Str::CutString(g_cellProps_tmp[j], L";", &cellSettings);
    int nCurrRowColumnCount = CAi_Str::ToInt(cellSettings[0]);
    if (nCurrRowColumnCount < nMaxColumn)
    {
      CString sRes = CAi_Str::ToString(nMaxColumn);

      for (int i = 0; i < nCurrRowColumnCount; ++i)
        sRes += L";" + cellSettings[i + 1];

      for (int i = nCurrRowColumnCount; i < nMaxColumn; ++i)
        sRes += L";" + CString(TBL_DEFUALT_TIME_TEXT) + L"," + CString(TBL_DEFUALT_TARIFF_TEXT);

      g_cellProps_tmp[j] = sRes;
    }
  }
}

void CView_TariffShedules::FormAllReadRowReq_24Hours()
{
  teFrameTypes reqFirstItem = FRAME_TYPE_SCHED_01;
  int nNext = static_cast<int>(reqFirstItem);
  for (int i = 0; i < g_rows.nCount_using_tmp; ++i)
  {
    reqFirstItem = static_cast<teFrameTypes>(nNext);
    SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    nNext += 1;
  }
}