#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "View_Connection.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_Connection, CCommonViewForm)

namespace ns_Inlay_Connection
{
#define SIDE_OFFSET 5

  struct X_TAGS
  {
    CString sConnection = L"Connection";
    CString sDeviceType = L"DeviceType";
  }
  g_xTags;

} // namespace ns_Inlay_Connection

using namespace ns_Inlay_Connection;

CView_Connection::CView_Connection()
  : CCommonViewForm(CView_Connection::IDD, L"CView_Connection")
{
}

CView_Connection::~CView_Connection()
{
}

BEGIN_MESSAGE_MAP(CView_Connection, CCommonViewForm)
  ON_WM_CONTEXTMENU()

  // Printing

  /*
  ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
  */

  ON_CBN_SELCHANGE(IDC_CBX_DEVICES, &CView_Connection::OnCbxChangeEdtDeviceAddress)
  ON_BN_CLICKED(IDC_CHK_SHOW_PASSWORD, &CView_Connection::OnBnClickedChkShowPassword)
  ON_CBN_SELCHANGE(IDC_CBX_LOGIN, &CView_Connection::OnCbnSelchangeCbxLogin)
END_MESSAGE_MAP()

void CView_Connection::DoDataExchange(CDataExchange* pDX)
{
  CFormView::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STC_DEVICE_CAP, m_stcDevicesCap);
  DDX_Control(pDX, IDC_CBX_DEVICES, m_cbxDevices);
  DDX_Control(pDX, IDC_STC_ACCESS_LEVEL_CAP, m_stcAccessLevelCap);
  DDX_Control(pDX, IDC_CBX_LOGIN, m_cbxLogin);
  DDX_Control(pDX, IDC_EDT_PASSWORD, m_edtPassword);
  DDX_Control(pDX, IDC_STC_INTERFACES_CAP, m_stcInterfacesCap);
  DDX_Control(pDX, IDC_TAB_INTERFACES, m_tabInterfaces);
  DDX_Control(pDX, IDC_CHK_SHOW_PASSWORD, m_chkShowPassword);
}

#ifdef _DEBUG
void CView_Connection::AssertValid() const
{
  CCommonViewForm::AssertValid();
  //ASSERT(m_pPerson);
  //ASSERT_VALID(m_pPerson);
}

void CView_Connection::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

/*void CView_Connection::OnFilePrintPreview()
{
AFXPrintPreview (this);
}

BOOL CView_Connection::OnPreparePrinting(CPrintInfo* pInfo)
{
// default preparation
return DoPreparePrinting(pInfo);
}

void CView_Connection::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
// TODO: add extra initialization before printing
}

void CView_Connection::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
// TODO: add cleanup after printing
}*/

void CView_Connection::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  // XML

  CParamsTreeNode* pInlaysNode = GetXmlNode();
  CParamsTreeNode* pDevTypeNode = nullptr;
  if (pInlaysNode)
  {
    CString sTag = g_xTags.sConnection;
    m_pConnectionNode = pInlaysNode->FindFirstNode(sTag);
    if (!m_pConnectionNode)
      m_pConnectionNode = pInlaysNode->AddNode(sTag);
    sTag = g_xTags.sDeviceType;
    pDevTypeNode = m_pConnectionNode->FindFirstNode(sTag);
    if (!pDevTypeNode)
      pDevTypeNode = m_pConnectionNode->AddNode(sTag);
  }

  //

  m_ConnTask_Prot_RS.SetGlobalParent(GetGlobalParent());
  m_ConnTask_Prot_USB.SetGlobalParent(GetGlobalParent());

  // There is only one view ever, so it only needs to do the initial update once--otherwise the application is  resized needlessly.
  //static BOOL bUpdatedOnce = FALSE;
  //if (bUpdatedOnce)
  //  return;
  //bUpdatedOnce = TRUE;

  m_stcDevicesCap.SetBkColor(GetTaskCaptionColor());
  m_stcAccessLevelCap.SetBkColor(GetTaskCaptionColor());
  m_stcInterfacesCap.SetBkColor(GetTaskCaptionColor());

  FormTask_DeviceChoice();
  FillTask_DeviceChoiceFromXml(pDevTypeNode);

  FormTask_AccessChoice();
  FillTask_AccessChoiceFromRegedit();

  FormTask_InterfaceChoice();
  UpdateInterfaceChoice();
  UpdateInterfaceChoiceInStatusBar();

  m_cbxDevices.SetFocus();

  WatchForCtrState2(&m_cbxDevices);
  WatchForCtrState2(&m_cbxLogin);
  WatchForCtrState2(&m_edtPassword);
  WatchForCtrState2(&m_chkShowPassword);

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_Connection::BeforeDestroyClass()
{
}

void CView_Connection::OnSize_()
{
  if (!m_tabInterfaces.m_hWnd)
    return;

  CRect rect{};
  GetClientRect(&rect);

  int nMinWidth = 100;
  m_tabInterfaces.GetClientRect(&rect);
  nMinWidth = rect.Width();

  SpreadCtrlToRightSide(&m_stcDevicesCap, nMinWidth);
  SpreadCtrlToRightSide(&m_stcAccessLevelCap, nMinWidth);
  SpreadCtrlToRightSide(&m_stcInterfacesCap, nMinWidth);

  Invalidate();
}

void CView_Connection::OnDraw(CDC* pDC)
{
  return;

  /*
  CRect rectClient;
  GetClientRect (rectClient);

  pDC->FillSolidRect (rectClient, ::GetSysColor (COLOR_3DFACE));
  */

  const int iOffset = 20;

  CFont* pFontOld = (CFont*)pDC->SelectStockObject(DEFAULT_GUI_FONT);
  ASSERT(pFontOld != nullptr);

  CRect rectClient;
  GetClientRect(&rectClient);

  CRect rectText = rectClient;
  rectText.DeflateRect(iOffset, iOffset);
  pDC->DrawText(L"Test", rectText, DT_CALCRECT | DT_WORDBREAK);

  rectText.OffsetRect((rectClient.Width() - rectText.Width() - 2 * iOffset) / 2,
                      (rectClient.Height() - rectText.Height() - 2 * iOffset) / 2);

  CRect rectFrame = rectText;
  rectFrame.InflateRect(iOffset, iOffset);

  CDrawingManager dm(*pDC);

  dm.FillGradient2(rectFrame, RGB(246, 241, 178), RGB(234, 223, 67), 35);

  pDC->Draw3dRect(rectFrame, RGB(168, 158, 18), RGB(168, 158, 18));

  pDC->SetBkMode(TRANSPARENT);
  pDC->SetTextColor(0);
  pDC->DrawText(L"Test", rectText, DT_WORDBREAK);
  pDC->SelectObject(pFontOld);
}

void CView_Connection::OnConnectionStateWasChanged()
{
  m_ConnTask_Prot_RS.ConnectionStateWasChanged(IsConnection());
}