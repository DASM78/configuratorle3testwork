#include "stdafx.h"

#include "Protocol_61107.h"
#include "auxthreads.h"
#include "MessageSet.h"
#include "MessagesIds.h"
#include "Resource.h"
#include "Defines.h"

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <iterator>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace aux;
using namespace std;

namespace ns_Protl_61107
{
  mutex gMutex;

} // namespace ns_Protl_61107

CProtocol_61107::CProtocol_61107(CWnd* pParent)
  : m_pParent(pParent)
{
  CreateExchThread(m_pParent);
}

CProtocol_61107::~CProtocol_61107()
{
  DestroyExchThread();
}

void CProtocol_61107::Connect(CONNECT_INIT* pInit)
{
  SetPassword(pInit->pchPassword);
  SetDevAddress(pInit->pchDevAddress);
  SetDevName(pInit->pchDevName);

  //ShowIndicatorOfConnection(); // TEMPORARY: display connection process; it is need uncomment
  SetThreadPhase(tphConnect);
  SetEvent_();
}

void CProtocol_61107::Disconnect()
{
  critical_section cs(ns_Protl_61107::gMutex);
  SetMonitoringReqs(0, nullptr); // ��������� �������
  SetRecordReq(nullptr);
  SetNextThreadPhase(tphPrepareDisconnection);
}

void CProtocol_61107::SetMonitoringReqs(int nDataID, vector<pair<const unsigned char*, varOnce_t>>* pList)
{
  critical_section cs(ns_Protl_61107::gMutex);
  m_nMonDataID = nDataID;
  m_monList.clear();
  if (pList)
    m_monList = *pList;
  m_bIsMonListNew = true;
}

bool CProtocol_61107::IsRecordingReq()
{
  critical_section cs(ns_Protl_61107::gMutex);
  return !m_recList.empty();
}

void CProtocol_61107::SetRecordReq(vector<SEND_UNIT>* pList)
{
  critical_section cs(ns_Protl_61107::gMutex);
  if (pList == nullptr)
  {
    m_recList.clear();
    return;
  }
  for (size_t i = 0; i < pList->size(); ++i)
    m_recList.push_back(pList->at(i));
}

LRESULT CProtocol_61107::TimerTick()
{
  critical_section cs(ns_Protl_61107::gMutex);

  if (!CheckTimePeriodBetweenReq())
    return FALSE;

  ETreadPhase thPh_next = GetNextThreadPhase();
  ETreadPhase thPh_current = thPh_next;
  SetNextThreadPhase(tphNone);

  switch (thPh_next)
  {
    case tphNone: return FALSE;

    case tphMonitoringOfData:
      if (RunRecordingBranch()) // ���� ���� ������ �� ������
        thPh_current = tphRecordingOfData; // �� ������� ����� ����� - ������ ������
      else
        if (!RunMonitoringBranch()) // ���� ��� ������ ��� �����������
        {
          thPh_current = tphNone; // �� ������� ����� ����� - ������ ���
          SetNextThreadPhase(tphMonitoringOfData); // �� ����. ���� ����� ��������� - ��� �� ���� ��� ����������� (�� ����� ���� (����) � ��� ������)
        }
      break;

    case tphPrepareDisconnection: // ���� ��������� ������ �� ����������
      thPh_current = tphSendBreakBeforeDisconnection; // �� ������� ����� ����� - �������� �������� break
      SetNextThreadPhase(tphNone); //  
      break;
    case tphDisconnect:
      SetNextThreadPhase(tphNone); //  
      break;
  }

  SetThreadPhase(thPh_current);
  SetEvent_();
  return TRUE;
}

LRESULT CProtocol_61107::OnResultNotify(WPARAM wParam, LPARAM lParam)
{
  critical_section cs(ns_Protl_61107::gMutex);

  MESSAGE_SET* pMessageSet = (MESSAGE_SET*)lParam;
  EXCH_DATA* pExchData = (EXCH_DATA*)pMessageSet->pData;

  bool bDisconnecting = false;
  ETreadPhase thPh_next = GetNextThreadPhase();
  if (thPh_next >= tphPrepareDisconnection)
    bDisconnecting = true;

  int nAdvancedMsgID = wParam;
  switch (nAdvancedMsgID)
  {
    case UM_CONNECTION_NOTIFY:
      On�onnectionResultNotify(pExchData);
      break;

      // section begin -------------
      // ���������� ������ ������ ������������� ���� �������� ��������� ���������� �� �������� 

    case UM_INITIALISATION_NOTIFY:
      if (!bDisconnecting)
        OnInitialisationResultNotify(pMessageSet->nData, pExchData);
      break;

    case UM_MONITORING_NOTIFY:
      if (!bDisconnecting)
        OnMonitoringResultNotify(pMessageSet->nData, pExchData);
      break;

    case UM_RECORDING_NOTIFY:
      if (!bDisconnecting)
        OnRecordingResultNotify(pMessageSet->nData, pExchData);
      break;

    // section end --------

    case UM_DISCONNECTION_NOTIFY:
      OnDisconnectionResultNotify(pMessageSet->nData, pExchData->bResult);
      break;
  }

  delete pExchData;
  delete pMessageSet;

  return pExchData->bResult ? TRUE : FALSE;
}

void CProtocol_61107::On�onnectionResultNotify(EXCH_DATA* pExchData)
{
  MESSAGE_SET* pMsgSet = new MESSAGE_SET(pExchData->bResult);
  ASSERT(pMsgSet);
  HWND& hWnd = m_pParent->m_hWnd;
  ::PostMessage(hWnd, UM_REFLECT_RESULT_NOTIFY, UM_CONNECTION_NOTIFY, (LPARAM)pMsgSet);

  if (pExchData->bResult)
    RunInitialisationBranch();
}

void CProtocol_61107::RunInitialisationBranch()
{
  SetThreadPhase(tphNone);
  SetNextThreadPhase(tphBeginInitialisation);
}

void CProtocol_61107::OnDisconnectionResultNotify(int nTreadPhase, bool bResult)
{
  ETreadPhase threadRes = (ETreadPhase)nTreadPhase;
  switch (threadRes)
  {
    case tphSendBreakBeforeDisconnection:
      SetNextThreadPhase(tphDisconnect);
      break;

    case tphDisconnect:
    {
      gRSCOMProp.nBaudRate = 300;
      SetNextThreadPhase(tphReset);

      MESSAGE_SET* pMsgSet = new MESSAGE_SET(bResult);
      ASSERT_AND_RETURN(pMsgSet);
      HWND& hWnd = m_pParent->m_hWnd;
      ::PostMessage(hWnd, UM_REFLECT_RESULT_NOTIFY, UM_DISCONNECTION_NOTIFY, (LPARAM)pMsgSet);
    }
    break;
  }
}

void CProtocol_61107::OnInitialisationResultNotify(int nTreadPhase, EXCH_DATA* pExchData)
{
  ETreadPhase threadRes = (ETreadPhase)nTreadPhase;

  if (!pExchData->bResult)
  {
    Disconnect();

    MESSAGE_SET* pMsgSet = nullptr;

    switch (threadRes)
    {
      case tphReceivePasswordResult:
        pMsgSet = new MESSAGE_SET(L"��������� ������ �� ���������!");
        break;

      case tphReceiveIdentificationOfInitialisation:
      {
        switch (pExchData->nInnerErrCode)
        {
          case INNER_ID_ERROR_IDF_REC:
          case INNER_ID_ERROR_DEVICE_NAME:
            break;
        }
      }
      default:
        pMsgSet = new MESSAGE_SET(pExchData->sMsg.IsEmpty() ? 
                                  L"������ ������!" :
                                  pExchData->sMsg);
        break;
    }

    ASSERT_AND_RETURN(pMsgSet);
    ::PostMessage(m_pParent->m_hWnd,
                  UM_REFLECT_RESULT_NOTIFY, UM_REFLECT_RESULT_STR_NOTIFY,
                  (LPARAM)pMsgSet);
    return;
  }

  switch (threadRes)
  {
    case tphBeginInitialisation:
      SetNextThreadPhase(tphReceiveIdentificationOfInitialisation); break;
    case tphReceiveIdentificationOfInitialisation:
      SetNextThreadPhase(tphSendConfirmationAboutInitialisation); break;
    case tphSendConfirmationAboutInitialisation:
      SetNextThreadPhase(tphReceivePasswordReqiure); break;
    case tphReceivePasswordReqiure:
      SetNextThreadPhase(tphSendPassword); break;
    case tphSendPassword:
      SetNextThreadPhase(tphReceivePasswordResult); break;
    case tphReceivePasswordResult:
      ::PostMessage(m_pParent->m_hWnd, UM_EXCH_IDENTIFICATION_PASSED_OK, 0, 0);
      SetNextThreadPhase(tphMonitoringOfData); break;
  }
}

bool CProtocol_61107::RunRecordingBranch()
{
  if (IsRecordingReq())
  {
    AddToSendingQueue(&m_recList[0]);
    m_recList.erase(m_recList.begin());
    return true;
  }
  return false;
}

void CProtocol_61107::OnRecordingResultNotify(int nTreadPhase, EXCH_DATA* pExchData)
{
  ETreadPhase threadRes = (ETreadPhase)nTreadPhase;
  switch (threadRes)
  {
    case tphRecordingOfData:
      if (!pExchData->bResult)
      {
        SetRecordReq(nullptr);

        EXCH_DATA* pExchDataForOuter = new EXCH_DATA();
        ASSERT_AND_RETURN(pExchDataForOuter);
        *pExchDataForOuter = *pExchData;
        MESSAGE_SET* pMsgSet = new MESSAGE_SET(pExchDataForOuter, 0);
        ASSERT_AND_RETURN(pMsgSet);
        ::PostMessage(m_pParent->m_hWnd,
                      UM_REFLECT_RESULT_NOTIFY, UM_PARSE_RECEIVED_DATA_NOTIFY,
                      (LPARAM)pMsgSet);

        SetNextThreadPhase(tphMonitoringOfData);
        return;
      }
      SetNextThreadPhase(tphRecordingOfDataConfirmation); // ����� ������ �������� �������������
      break;

    case tphRecordingOfDataConfirmation:
      {
        if (pExchData->bResult)
        {
          pExchData->bResult = ParseReceivedSendConfirmationData(pExchData->sSOHID, pExchData->sData);
          TypeTRACEMsg(L"ParseReceivedSendConfirmationData - " + CString(pExchData->bResult ? L"OK" : L"ERROR") + L"\n");
        }

        if (!pExchData->bResult)
          SetRecordReq(nullptr);

        EXCH_DATA* pExchDataForOuter = new EXCH_DATA();
        ASSERT_AND_RETURN(pExchDataForOuter);
        *pExchDataForOuter = *pExchData;
        MESSAGE_SET* pMsgSet = new MESSAGE_SET(pExchDataForOuter, 0);
        ASSERT_AND_RETURN(pMsgSet);
        ::PostMessage(m_pParent->m_hWnd,
                      UM_REFLECT_RESULT_NOTIFY, UM_PARSE_RECEIVED_DATA_NOTIFY,
                      (LPARAM)pMsgSet);
      }
      SetNextThreadPhase(tphMonitoringOfData);
      break;
  }
}

bool CProtocol_61107::ParseReceivedSendConfirmationData(CString& sReqAddr, CString& sData)
{
  vector <CString> dataString;
  CAi_Str::CutString(sData, L"\r\n", &dataString);
  if (dataString.empty())
    return false;
  if (dataString.size() != 1)
  {
    ASSERT(false);
    return false;
  }

  if (dataString[0] == L"\xF") // NAK
    return false;

  vector <CString> dataSet;
  CAi_Str::CutString(dataString[0], L"(", &dataSet);
  ASSERT(dataSet.size() == 2);
  if (dataSet.size() != 2)
    return false;

  CString sAddr = dataSet[0];
  if (sReqAddr != sAddr)
  {
    ASSERT(false); // ����� ����������� ������ ���������� �� ������ ����������� ������ - �������� ������������������ ��������
    return false;
  }

  return true;
}

bool CProtocol_61107::RunMonitoringBranch()
{
  if (m_bIsMonListNew)
  {
    m_bIsMonListNew = false;
    SetToMonitoringQueue(m_nMonDataID, m_monList.empty() ? nullptr : &m_monList);
  }

  return (m_nMonDataID > 0 && !m_monList.empty());
}

void CProtocol_61107::OnMonitoringResultNotify(int nTreadPhase, EXCH_DATA* pExchData)
{
  CString sMsg;
  enum { aRepeat, aBreak, aContinue } nextAction = aContinue;

  if (!pExchData->bResult)
  {
    if (pExchData->sSOHID.IsEmpty()) // ����� ��� �� �����, �  ���������� �������
      nextAction = aRepeat;
    else
    {
      sMsg = L"������ ������!";
      nextAction = aBreak;
    }
  }


  if (nextAction == aBreak)
  {
    MESSAGE_SET* pMsgSet = new MESSAGE_SET(sMsg);
    //ASSERT_AND_RETURN(pMsgSet);
    //::PostMessage(m_pParent->m_hWnd,
    //              UM_REFLECT_RESULT_NOTIFY, UM_REFLECT_RESULT_INFO_NOTIFY,
    //              (LPARAM)pMsgSet);

    ::PostMessage(m_pParent->m_hWnd,
                  UM_REFLECT_RESULT_NOTIFY, UM_REFLECT_RESULT_STR_NOTIFY,
                  (LPARAM)pMsgSet);
    SetNextThreadPhase(tphPrepareDisconnection);
    return;
  }

  if (nextAction == aRepeat)
  {
    SetNextThreadPhase(tphMonitoringOfData);
    return;
  }

  // ... else will be continue;

  ETreadPhase threadRes = (ETreadPhase)nTreadPhase;
  switch (threadRes)
  {
    case tphMonitoringOfData: SetNextThreadPhase(tphReceiveMonitoringData); break;

    case tphReceiveMonitoringData:
      {
        EXCH_DATA* pExchDataForOuter = new EXCH_DATA();
        ASSERT_AND_RETURN(pExchDataForOuter);
        *pExchDataForOuter = *pExchData;
        MESSAGE_SET* pMsgSet = new MESSAGE_SET(pExchDataForOuter, 0);
        ASSERT_AND_RETURN(pMsgSet);
        ::PostMessage(m_pParent->m_hWnd,
                      UM_REFLECT_RESULT_NOTIFY, UM_PARSE_RECEIVED_DATA_NOTIFY,
                      (LPARAM)pMsgSet);
      }
      SetNextThreadPhase(tphMonitoringOfData);
      break;
  }
}
