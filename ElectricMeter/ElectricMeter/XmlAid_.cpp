#include "stdafx.h"
#include "XmlAid_.h"
#include "VersionInterface.h"
#include "Ai_File.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_XmlAid
{
  CString g_sCurrentRightVersion = L"1.1.0";

} // namespace ns_XmlAid

using namespace ns_XmlAid;

CXmlAid_::CXmlAid_()
{
}

CXmlAid_::~CXmlAid_()
{
  if (m_pMain)
  {
    delete m_pMain;
    m_pMain = nullptr;
  }
}

void CXmlAid_::SetBackupFile(CString sPathFile)
{
  m_sXmlBackupFile = sPathFile;
  bool bCreate = false;

  if (!CAi_File::IsFileExist(m_sXmlBackupFile))
    bCreate = true;
  else
    if (!CheckXmlFile(m_sXmlBackupFile))
    {
      CAi_File::DeleteFile_(m_sXmlBackupFile);
      bCreate = true;
    }

  if (bCreate)
  {
    CAi_File::ExamineFile(m_sXmlBackupFile);

    m_pMain = new CParamsTreeNode(L"");
    m_pMain->SetName(xTag::sAppMainRoot);
    m_pMain->SetValue(L"");

    InitNodes();

    CreateXmlFileFromTree_(xmlBackupFile);

    delete m_pMain;
    m_pMain = nullptr;
  }
}

bool CXmlAid_::CheckXmlFile(CString sPathFile)
{
  CParamsTreeNode* pMainTmp = CreateTreeFromXmlFile(sPathFile);
  if (pMainTmp->GetName() != xTag::sAppMainRoot)
    return false;

  CParamsTreeNode* pVersions = pMainTmp->FindFirstNode(xTag::sVersions);
  if (pVersions)
  {
    CString sProductVersionFromXml = pVersions->GetAttributeValue(xTag::sVersionsAttr_Product);
    CString sXmlVersionFromXml = pVersions->GetAttributeValue(xTag::sVersionsAttr_CurrXml);
    if (sProductVersionFromXml.IsEmpty()
        || sXmlVersionFromXml.IsEmpty())
        return false;

    // g_sCurrentRightVersion < sXmlVersionFromXml
    if (CVersionInterface::CompareVersions(g_sCurrentRightVersion, sXmlVersionFromXml) == -1)
      return false;
  }

  return true;
}

bool CXmlAid_::LoadTreeFromFile(CString sPathFile)
{
  if (m_pMain)
  {
    CreateXmlFileFromTree_(xmlBackupFile); // ��������� ��� ���� � �����
    m_pMain->DeleteAllNodes();
  }

  if (!CheckXmlFile(sPathFile))
  {
    ASSERT(false);
    return false;
  }

  m_pMain = CreateTreeFromXmlFile(sPathFile);
  InitNodes();

  return true;
}

void CXmlAid_::InitNodes()
{
  m_pVersions = m_pMain->FindFirstNode(xTag::sVersions);
  if (m_pVersions)
  {
    m_sProductVersionFromXml = m_pVersions->GetAttributeValue(xTag::sVersionsAttr_Product);
    m_sXmlVersionFromXml = m_pVersions->GetAttributeValue(xTag::sVersionsAttr_CurrXml);
  }
  else
  {
    m_pVersions = m_pMain->AddNode(xTag::sVersions);
    m_pVersions->AddAttribute(xTag::sVersionsAttr_Product, L"0.0.0.1 (����� ���)");
    m_pVersions->AddAttribute(xTag::sVersionsAttr_CurrXml, L"0.0.0");
    m_sProductVersionFromXml = m_pVersions->GetAttributeValue(xTag::sVersionsAttr_Product);
    m_sXmlVersionFromXml = m_pVersions->GetAttributeValue(xTag::sVersionsAttr_CurrXml);
  }

  m_pInlays = m_pMain->FindFirstNode(xTag::sInlays);
  if (!m_pInlays)
    m_pInlays = m_pMain->AddNode(xTag::sInlays);
}

void CXmlAid_::SetProductVersion(CString sVer)
{
  m_sCurrentProductVersion = sVer;
}

CParamsTreeNode* CXmlAid_::GetMainNode()
{
  return m_pMain;
}

CParamsTreeNode* CXmlAid_::GetInlaysNode()
{
  return m_pInlays;
}

void CXmlAid_::SetUserFile(CString& sXmlUserFile)
{
  m_sXmlUserFile = sXmlUserFile;
}

bool CXmlAid_::CreateXmlFileFromTree_(EXmlFile xmlFileType)
{
  CString sXmlFile = m_sXmlBackupFile;

  switch (xmlFileType)
  {
    case xmlUserFile:
      sXmlFile = m_sXmlUserFile;
      if (sXmlFile.IsEmpty())
      {
        ASSERT(false);
        return false;
      }
      break;
  }

  if (m_pVersions)
  {
    m_pVersions->DeleteAllAttributes();
    m_pVersions->AddAttribute(xTag::sVersionsAttr_Product, m_sCurrentProductVersion);
    m_pVersions->AddAttribute(xTag::sVersionsAttr_CurrXml, m_sCurrentXmlVersion);
  }

  return CreateXmlFileFromTree(sXmlFile, m_pMain);
}