// CNetParam.cpp : implementation file
//

#include "stdafx.h"
#include "ElectricMeter.h"
#include "CNetParam.h"
#include "afxdialogex.h"
#include "stdafx.h"
#include "ElectricMeter.h"
#include "Production.h"
#include "Defines.h"
#include "Resource.h"
#include "Ai_Font.h"
#include "Ai_Str.h"
#include <string> 
#include <memory>
#include "ComSettDld.h"
#include "CounterInfo.h"
#include "MainFrm.h"
using namespace mdl;

// CNetParam dialog


IMPLEMENT_DYNCREATE(CNetParam, CCommonViewForm)

namespace ns_JourParam
{

	struct COLUMNS_FILTER
	{
		int nCheck = -1;
		int nType = -1;
	}
	g_colsFilter;

	struct JOUR_FILTER
	{
		JOUR_FILTER(tNetEvents i, CString name)
			:
			evType(i)
			, sName(name)
		{ }

		int nCheck = 1;
		tNetEvents evType = TYPE_EVENT_NET_LAST;
		CString sName;
	};
	vector <JOUR_FILTER> g_rowsFilter;

	struct COLUMNS
	{
		int nDate = -1;
		int nTime = -1;
		int nDuration = -1;
		int nPhase = -1;
		int nReason = -1;
		int nTrsh = -1;
		int nAvTime = -1;
	}
	g_cols;
	int g_nComnSignificantCell = -1;

	struct DATA
	{
		CString sDate;
		CString sTime;
		CString sDuration;
		CString nPhase;
		CString reason;
		CString trsh;
		CString avTime;
		JOUR_FILTER* pFltr = nullptr;
	};
	vector <DATA> g_data;
	int MAX_EVENT_JOUNAL = 0;
	int g_nRow_from = 0;

	struct X_TAGS
	{
		CString sEvJour = L"EventJournal";
		CString sFilter = L"Filter_";
		CString sDataSet = L"DataSet";
	}
	g_xTags;

} // namespace ns_Jour



void CreateFilterNetParam()
{
	if (!ns_JourParam::g_rowsFilter.empty())
		return;

	auto lbmAdd = [&](tNetEvents evT, CString sName)->void
	{
		ns_JourParam::g_rowsFilter.push_back(ns_JourParam::JOUR_FILTER(evT, sName));
	};

	lbmAdd(TYPE_EVENT_NET_VOLTAGE_DROP_LOW, L"Падение напряжения ниже %f в фазе %d");
	lbmAdd(TYPE_EVENT_NET_VOLTAGE_OVER_HIGH, L"Падение напряжения ниже %f в фазе %d");
	lbmAdd(TYPE_EVENT_NET_FREQ_DROP_LOW, L"Появление напряжения в фазе");
	lbmAdd(TYPE_EVENT_NET_FREQ_OVER_HIGH, L"Пропадание напряжения в фазе");
}

ns_JourParam::JOUR_FILTER* GetLinkFilter(teEventTypes evType)
{
	if (ns_JourParam::g_rowsFilter.empty())
	{
		ASSERT(false);
		return nullptr;
	}

	for (size_t i = 0; i < ns_JourParam::g_rowsFilter.size(); ++i)
	{
		if (ns_JourParam::g_rowsFilter.at(i).evType == evType)
			return &ns_JourParam::g_rowsFilter[i];
	}

	ASSERT(false);
	return nullptr;
}



void CNetParam::OnInitialUpdate()
{
	CCommonViewForm::OnInitialUpdate();


	WatchForCtrState(&m_btnRecParam, CCommonViewDealer::rightAdmin);

	m_lstEvJr.ShowHorizScroll(false);
	m_lstEvJr.EnableInterlacedColorScheme(true);


	m_plstEvJrEx = new СAi_ListCtrlEx();
	m_plstEvJrEx->SetList(&m_lstEvJr, m_hWnd);
	DWORD lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
	DWORD nStyleEx =
		LVS_EX_FULLROWSELECT
		| LVS_EX_SUBITEMIMAGES
		| LVS_EX_BORDERSELECT
		| LVS_EX_DOUBLEBUFFER
		| LVS_EX_GRIDLINES;


	m_plstEvJrEx->CreateList(false, nStyleEx);

	bool m_bGridLinesStyle = true;


	m_bGridLinesStyle = true;
	if (!m_bGridLinesStyle)
		m_lstEvJr.SetExtendedStyle(m_lstEvJr.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

	m_plstEvJrEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

	m_plstEvJrEx->SetColumnEx(L"", LVCFMT_LEFT);
	ns_JourParam::g_cols.nDate = m_plstEvJrEx->SetColumnEx(L"Дата", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nDate, CS_WIDTH_SET_BY_TEXT, 70);

	ns_JourParam::g_cols.nTime = m_plstEvJrEx->SetColumnEx(L"Время", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nTime, CS_WIDTH_SET_BY_TEXT, 70);

	ns_JourParam::g_cols.nDuration = m_plstEvJrEx->SetColumnEx(L"Длительность, сек", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nDuration, CS_WIDTH_SET_BY_TEXT, 120);

	ns_JourParam::g_cols.nPhase = m_plstEvJrEx->SetColumnEx(L"Фаза", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nPhase, CS_WIDTH_FIX, 70);

	ns_JourParam::g_cols.nReason = m_plstEvJrEx->SetColumnEx(L"Событие", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nReason, CS_WIDTH_SET_BY_TEXT, 150);

	ns_JourParam::g_cols.nTrsh = m_plstEvJrEx->SetColumnEx(L"Уровень, В(Гц)", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nTrsh, CS_WIDTH_SET_BY_TEXT, 120);

	ns_JourParam::g_cols.nAvTime = m_plstEvJrEx->SetColumnEx(L"Время контроля, сек", LVCFMT_CENTER);
	m_plstEvJrEx->SetColumnWidthStyle(ns_JourParam::g_cols.nAvTime, CS_WIDTH_SET_BY_TEXT, 120);

	m_plstEvJrEx->MatchColumns();



	m_plstEvJrEx->SetNode(nullptr, nullptr, true); // при работе с парам. функций nRow_inList надо делать + 1 - т.к. первая присутствующая строка узел пустая и невидимая
	ns_JourParam::g_nComnSignificantCell = ns_JourParam::g_cols.nDate;
	m_plstEvJrEx->SetNodeStyle(0, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_JourParam::g_nComnSignificantCell);

	vector<CString> textOfCells2(4);
	textOfCells2[1] = IDS_EMPTY_CELL_TEXT;

	CreateFilterNetParam();
	vector<CString> textOfCells(2);
	for (auto i : ns_JourParam::g_rowsFilter)
	{
		textOfCells[1] = i.sName;
	}
	int nRow_inList = 1;
	// Для первой строки
	nRow_inList = m_plstEvJrEx->SetChild(0, &textOfCells2);
	// см. описание "Descr. 1" в документе AppDescription.txt
	//m_plstEvJrEx->SetCellHideText(nRow_inList, ns_JourParam::g_nComnSignificantCell);

	int nMaxEventTypes = static_cast<int>(ns_JourParam::g_rowsFilter.size());
	ns_JourParam::MAX_EVENT_JOUNAL = 2000;
	// ... для остальных строк


	ns_JourParam::g_nComnSignificantCell = ns_JourParam::g_cols.nDate;

	m_plstEvJrEx->SetEmptyChild(ns_JourParam::MAX_EVENT_JOUNAL);



	m_plstEvJrEx->UpdateView();
	m_lstEvJr.CtrlWasCreated();

	AddCtrlForPositioning(&m_lstEvJr, movbehConstTopAndBottomMargins, movbehConstLeftAndRigthMargins);


	SetInitialUpdateFlag(true_d(L"Эта функция отработала"));

}


void CNetParam::BeforeDestroyClass()
{

}


void CNetParam::UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult)
{
	if (ft == FRAME_TYPE_NET_CONTROL_SETTINGS) {
		AfxMessageBox(L"Записано!", MB_ICONINFORMATION);
	}
}

CNetParam::CNetParam()
	: CCommonViewForm(CNetParam::IDD, L"CNetParam"), m_lstEvJr(&m_plstEvJrEx, dynamic_cast<CCommonViewDealer*>(this))
{
	m_paramAvTimeF.SetVal(30.);
	m_paramAvTimeF.SetPrecion(2);
	m_paramAvTimeV.SetVal(60.);
	m_paramAvTimeV.SetPrecion(2);
	m_paramLowV.SetVal(210.);
	m_paramLowV.SetPrecion(2);
	m_paramHighV.SetVal(250.);
	m_paramHighV.SetPrecion(2);
	m_paramLowF.SetVal(49.);
	m_paramLowF.SetPrecion(2); 
	m_paramHighF.SetVal(51.);
	m_paramHighF.SetPrecion(2); 
}

CNetParam::~CNetParam()
{
}

void CNetParam::DoDataExchange(CDataExchange* pDX)
{

	DDX_Control(pDX, IDC_LST_EVENTS, m_lstEvJr);

	DDX_Control(pDX, IDC_EDIT_HIGHV, m_paramHighV);

	DDX_Control(pDX, IDC_EDIT_LOWV, m_paramLowV);

	DDX_Control(pDX, IDC_EDIT_HIGHF, m_paramHighF);

	DDX_Control(pDX, IDC_EDIT_LOWF, m_paramLowF);

	DDX_Control(pDX, IDC_EDIT_AVERTV, m_paramAvTimeV);

	DDX_Control(pDX, IDC_EDIT_AVERTF, m_paramAvTimeF);


	DDX_Control(pDX, IDC_BTN_WR, m_btnRecParam);
}

void CNetParam::OnSize_()
{
}



void CNetParam::OnDraw(CDC* pDC)
{
	return;
}

bool CNetParam::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
	return true;
}



BEGIN_MESSAGE_MAP(CNetParam, CFormView)
	ON_BN_CLICKED(IDC_BTN_READ, &CNetParam::OnBnClickedBtnReadParamSettings)
	ON_BN_CLICKED(IDC_BTN_WR, &CNetParam::OnBnClickedBtnWriteParamSettings)
	ON_BN_CLICKED(IDC_BTN_READ_JRN, &CNetParam::OnBnClickedBtnReadJrn)
	//	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LST_EVENTS, &CNetParam::OnLvnItemchangedLstEvents)
END_MESSAGE_MAP()


// CNetParam message handlers



void CNetParam::OnConnectionStateWasChanged()
{
	CheckMeaningsViewByConnectionStatus();
}


void CNetParam::CheckMeaningsViewByConnectionStatus()
{
	if (!IsConnection())
	{
		ClearAllReadingReq();
	}
	else
	{
		OnBnClickedBtnReadParamSettings();
	}
}

void CNetParam::OnBnClickedBtnReadParamSettings()
{
	bWaitRead = true;
	SetParamTypeForRead(FRAME_TYPE_NET_CONTROL_SETTINGS, true);
	// TODO: Add your control notification handler code here
}



// пишем новые параметры контроля сети
void CNetParam::OnBnClickedBtnWriteParamSettings()
{
	UpdateData(); // get new values
	UpdateData(false); // and back

	CString paramStr;
	paramStr.Format(_T("%.2f,%.2f,%.2f,%.2f,%.2f,%.2f"), m_paramLowV.GetVal(), m_paramHighV.GetVal(), m_paramLowF.GetVal(), m_paramHighF.GetVal(), m_paramAvTimeV.GetVal(), m_paramAvTimeF.GetVal());
	SendDataToDevice(FRAME_TYPE_NET_CONTROL_SETTINGS, paramStr);


	// TODO: Add your control notification handler code here
}

int totalPacks = 0;
void CNetParam::OnBnClickedBtnReadJrn()
{

	ClearRows();

	CMainFrame* pMf = (CMainFrame*)::AfxGetMainWnd();
	pMf->GetDataStore()->ClearReceivedDataStore(); //TODO FUCKKKKKKKKKKKKKKKKKKKKKKKKKK
	

	ns_JourParam::g_data.clear();
	BeginFormingSendPack(true_d(L"start"));
	totalPacks = 0;
	SetParamTypeForRead(FRAME_TYPE_PARAM_PACK_COUNT_F, true);
	BeginFormingSendPack(false_d(L"start"));
}

//void CNetParam::DoAfterClearLoadData()
//{
//	//BeginFormingReadPack(true);
//	//ErrorMsgWasShowed(false_d(L"ошибка на прием еще не проанализирована"));
//	//SetWaitCursor_answer(true);
//
//	//BeginFormingReadPack(false);
//
//
//}


void CNetParam::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{

	if (WasErrorMsgBeShowed())
	{
		SetWaitCursor_answer(false);
		return;
	}
	varMeaning_t m = FindReceivedData(ft);
	if (m == L"\xF")
	{
		ErrorMsgWasShowed(true_d(L"ошибка уже проанализирована"));
		SetWaitCursor_answer(false);
		AfxMessageBox(L"Ошибка загрузки записей журнала!", MB_ICONERROR);
		return;
	}


	int nCount = CAi_Str::ToInt(m);

	int nStart;
	int nEnd;
	if (ft == FRAME_TYPE_NET_CONTROL_SETTINGS) {
		m = FindReceivedData(ft);
		vector<CString> data;
		CAi_Str::CutString(m, L",", &data);
		m_paramLowV.SetVal(_wtof(data[0]));
		m_paramHighV.SetVal(_wtof(data[1]));
		m_paramLowF.SetVal(_wtof(data[2]));
		m_paramHighF.SetVal(_wtof(data[3]));
		m_paramAvTimeV.SetVal(_wtof(data[4]));
		m_paramAvTimeF.SetVal(_wtof(data[5]));
		UpdateData(false);

	}


	else if (ft == FRAME_TYPE_PARAM_PACK_COUNT_V) {
		totalPacks += nCount;
		nStart = static_cast<int>(FRAME_TYPE_PARAM_CONTROLV_1);
		nEnd = static_cast<int>(FRAME_TYPE_PARAM_CONTROLV_1) + nCount - 1;
		for (int i = nStart; i <= nEnd; ++i) {
			SetParamTypeForRead(static_cast<teFrameTypes>(i), true);
		}
		if (totalPacks == 0) {
			AfxMessageBox(L"Нет событий!", MB_ICONINFORMATION);
		}
	}

	else if (ft == FRAME_TYPE_PARAM_PACK_COUNT_F) {
		nStart = static_cast<int>(FRAME_TYPE_PARAM_CONTROLF_1);
		nEnd = static_cast<int>(FRAME_TYPE_PARAM_CONTROLF_1) + nCount - 1;
		totalPacks += nCount;
		for (int i = nStart; i <= nEnd; ++i) {
			SetParamTypeForRead(static_cast<teFrameTypes>(i), true);
		}
		// TODO WTF
		// почему то не могу послать вместе с запросом частоты, кто то очищаеет очередь, возможно таймер
		SetParamTypeForRead(FRAME_TYPE_PARAM_PACK_COUNT_V, true);
	}


	else if (ft >= FRAME_TYPE_PARAM_CONTROLV_1 && ft <= FRAME_TYPE_PARAM_CONTROLV_20 ||
		ft >= FRAME_TYPE_PARAM_CONTROLF_1 && ft <= FRAME_TYPE_PARAM_CONTROLF_20) {
		ParseReceivedData(ft);
		SetWaitCursor_answer(false); // снять курсор ожидания
	}

}


void CNetParam::ParseReceivedData(teFrameTypes ft)
{
	unsigned int nAddress = 0;
	if (GetDataFrameAddress(ft, nAddress))
	{
		/*

		8B00(01-01-18,01:00,1,0;
		01-01-2001,01:30,3,0;
		01-01-2001,02:00,5,0;
		01-01-2001,04:00,1,0;)
		8B01(01-01-2001,04:30,2,0;
		01-01-2001,05:00,8,0;
		01-01-2001,06:30,6,0;
		01-01-2001,07:00,10,0;)
		-//-
		8B0F(01-01-2001,17:30,4,0;
		01-01-2001,18:00,7,0;
		01-01-2001,18:30,12,0;
		01-01-2001,19:30,15,0;)
		*/
		int nExtendedAddresses = GetExtendedAddresses(ft) + 1; // 1 - itself
		for (int e = 0; e < nExtendedAddresses; ++e)
		{
			varMeaning_t mData = FindReceivedData(nAddress + e);
			if (mData == L"?")
			{

				break;
			}

			mData.TrimRight(L";");
			vector<CString> data;
			if (CAi_Str::CutString(mData, L";", &data))
			{
				for (size_t k = 0; k < data.size(); ++k)
				{
					vector<CString> eventItem;
					if (CAi_Str::CutString(data.at(k), L",", &eventItem)
						&& eventItem.size() == 7)
					{
						ns_JourParam::DATA data;
						data.sDate = eventItem[0];
						data.sTime = eventItem[1];
						
						if (eventItem[2] == CString("0"))
							data.sDuration = CString("2");
						else
							data.sDuration = eventItem[2];
						CString phases[] = { _T("Фаза-1"), _T("Фаза-2"), _T("Фаза-3") };
						int iphase = _wtoi(eventItem[3]);
						if (_wtoi(eventItem[4]) < 2) {
							data.nPhase = phases[iphase]; // пишем только о напряженияъх
						}
						else {
							data.nPhase = CString(_T(""));
						}

						CString reasons[] = { _T("Падение напряжения"), _T("Превышение напряжения"), _T("Падение частоты"), _T("Превышение частоты") };
						int ireason = _wtoi(eventItem[4]);
						data.reason = reasons[ireason];
						data.trsh = eventItem[5];
						data.avTime = eventItem[6];
						ns_JourParam::g_data.push_back(data);
					}

					//		// form filter link 

					//		int nId = CAi_Str::ToInt(sId);
					//		int nNone = static_cast<int>(TYPE_EVENT_NONE);
					//		if (nId == nNone)
					//		{
					//			m_bParseResult = false;
					//			continue;
					//		}

					//		int nStart = static_cast<int>(TYPE_EVENT_POWER_ON);
					//		int nEnd = static_cast<int>(TYPE_EVENT_LAST);
					//		for (int j = nStart; j < nEnd; ++j)
					//		{
					//			if (nId == j)
					//			{
					//				teEventTypes evType = static_cast<teEventTypes>(j);
					//				data.pFltr = GetLinkFilter(evType);
					//			}
					//		}

					//		if (data.pFltr)
					//			ns_Jour::g_data.push_back(data);
					//	}
				}
			}

		}
	}
	FillInByFilter();
}

//void CNetParam::OnLvnItemchangedLstEvents(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
//	// TODO: Add your control notification handler code here
//	*pResult = 0;
//}


void CNetParam::FillInByFilter()
{
	int j = 0;
	for (auto i : ns_JourParam::g_data)
	{


		int nRow_inList = j + 1;
		//if (j == 0)
		//{
		//	m_plstEvJrEx->SetCellShowText(nRow_inList, ns_JourParam::g_nComnSignificantCell);
		//	m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_nComnSignificantCell, i.sDate);
		//}
		//else
		//{
		//	int nNodeRow_inView = 0;
		//	m_plstEvJrEx->InsertTextToChild(nNodeRow_inView, ns_JourParam::g_nComnSignificantCell, i.sDate);
		//}
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nDate, i.sDate);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nTime, i.sTime);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nDuration, i.sDuration);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nPhase, i.nPhase);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nReason, i.reason);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nTrsh, i.trsh);
		m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nAvTime, i.avTime);

		//m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nName, i.pFltr->sName);
		++j;

	}
	m_plstEvJrEx->UpdateView();
}


void CNetParam::ClearRows()
{
	int nCntSeted = m_plstEvJrEx->GetSetedCountOfChild(0);
	//if (nCntSeted > 0)
	{
		auto lmbClearCells = [=](int nRow_inList)->void
		{
			CString s = L"";
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nDate, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nTime, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nDuration, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nPhase, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nReason, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nAvTime, s);
			m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nTrsh, s);
		};

		// для первой строки
		int nRow_inList = 1;
		lmbClearCells(nRow_inList);
		//m_plstEvJrEx->SetCellText(nRow_inList, ns_JourParam::g_cols.nDate, IDS_EMPTY_CELL_TEXT);
		//m_plstEvJrEx->SetCellHideText(nRow_inList, ns_JourParam::g_nComnSignificantCell);
		//// для остальных строк
		for (int j = 0; j < ns_JourParam::MAX_EVENT_JOUNAL; ++j)
		{
			nRow_inList = j + 1;
			if (!m_plstEvJrEx->IsRowVisible(nRow_inList))
				break;

			int nRow_inView = j;
			m_plstEvJrEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_JourParam::g_nComnSignificantCell, -1);
			lmbClearCells(nRow_inList);
		}
	}
	m_plstEvJrEx->UpdateView();
}

