#include "StdAfx.h"
#include "Resource.h"
#include "auxthreads.h"
#include "ProtocolBase.h"
#include "Protocol_61107.h"
#include "ProtocolTypes.h"
#include "CommonViewDealer.h"
#include "MessageSet.h"
#include "MainFrm.h"
#include "Defines.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace aux;

namespace ns_Conn
{
	EProtocolTypes gCurrentProtocol = eProtocolNone;
	CProtocolBase* gpProtocol = nullptr;

	vector<pair<unsigned char*, int>> gReadBinData;
	vector<pair<unsigned char*, int>> gWriteBinData;

} // namespace ns_Conn


void CMainFrame::StartExchThread (EProtocolTypes pt)
{
	FreeExchBinData ();
	ns_Conn::gCurrentProtocol = pt;
	switch (ns_Conn::gCurrentProtocol)
	{
	case eProtocol61107: ns_Conn::gpProtocol = new CProtocol_61107 (this); break;
	}
}

void CMainFrame::StopExchThread ()
{
	delete ns_Conn::gpProtocol;
	ns_Conn::gpProtocol = nullptr;
}

void CMainFrame::ConnectionToDevice ()
{
	PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 1);
	m_bCanLookForKeepAlive = false;
	m_nExchKeepAlive = 0;

	bool bErrorPort = false;
	if (gRSCOMProp.sPortName.IsEmpty ())
		bErrorPort = true;
	if (gRSCOMProp.sPortName == L"RS" && gRSCOMProp.nPortNumber == 0) // gRSCOMProp.nPortNumber ������ ���� > 0
		bErrorPort = true;
	if (bErrorPort)
	{
		PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 0);
		AfxMessageBox (L"�� ������ ���� ��� �����������!", MB_ICONASTERISK);
		return;
	}

	SetPrgState (psConnectingToDevice);

	auto lmbFromUnicodeToASCII = [](CString& sSrc)->unsigned char*
	{
		char *pDst = new char[MAX_PATH + 1]{};
		CAi_Str::ToBuffers (sSrc, &pDst, MAX_PATH);
		return (unsigned char*)pDst;
	};

	REG_EDIT_ACCESS_DATA ac;
	m_appEnviron.GetAccessProp (ac);
	bool bAdmin = (ac.sCurrLogin == L"Admin");

	CONNECT_INIT init;

	CString& sPassword = bAdmin ? ac.sAdminPassword : ac.sUserPassword;
	init.pchPassword = lmbFromUnicodeToASCII (sPassword);

	CString& sDevAddress = gRSCOMProp.sDeviceAddress;
	init.pchDevAddress = lmbFromUnicodeToASCII (sDevAddress);

	CString sAssocDevName = GetOnAssocDevName ()->sInExch;
	init.pchDevName = lmbFromUnicodeToASCII (sAssocDevName);

	ns_Conn::gpProtocol->Connect (&init);
}

LRESULT CMainFrame::ReconnectWithOtherSpeed (WPARAM wParam, LPARAM lParam)
{
	UpdateConnectionStateForCurrInlay ();
	return TRUE;
}

void CMainFrame::DisconnectionFromDevice ()
{
	TRACE (L" D I S C O N N E C T !\n");

	PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 1);
	m_bCanLookForKeepAlive = false;
	m_nExchKeepAlive = 0;

	ns_Conn::gpProtocol->Disconnect ();
}

bool CMainFrame::CheckDeviceAddress ()
{
	/*if (gRSCOMProp.sDeviceAddress.IsEmpty())
	{
	AfxMessageBox(L"�� ������ ����� ����������!", MB_ICONERROR);
	ShowOutlookTab(ID_OUTLOOK_CONNECTION_TAB);
	SelectSpecRowInOutlookTab(ID_OUTLOOK_CONNECTION_TAB, IDD_CONNECTION_SETTINGS);
	Invalidate();
	return false;
	}*/
	return true;
}

void CMainFrame::FreeMonitoringReq ()
{
	if (!IsPrgState_OnlineDeviceData ()) // ��� ����������� � ��������
		return;
}

LRESULT CMainFrame::OnDoExchangeByTimer (WPARAM wParam, LPARAM lParam)
{
	if (!IsPrgState_OnlineDeviceData ()) // ��� ����������� � ��������
		return FALSE;

	switch (ns_Conn::gCurrentProtocol)
	{
	case eProtocol61107: PrepareExchData_Protocol61107 (); break;
	}

	return ns_Conn::gpProtocol->TimerTick ();
}

LRESULT CMainFrame::OnExchangeResultNotify (WPARAM wParam, LPARAM lParam)
{
	return ns_Conn::gpProtocol->OnResultNotify (wParam, lParam);
}

LRESULT CMainFrame::OnReflectByResultNotify (WPARAM wParam, LPARAM lParam)
{
	MESSAGE_SET* pMessageSet = (MESSAGE_SET*)lParam;
	bool bCloseApp = false;

	int nAdvancedMsgID = wParam;
	switch (nAdvancedMsgID)
	{
	case UM_CONNECTION_NOTIFY:
	{
								 bool bConnectionState = (pMessageSet->nData == 1) ? true : false;
								 if (bConnectionState) // ������
								 {
									 SetPrgState (psOnlineDeviceData);
									 UpdateConnectionStateForCurrInlay ();
								 }
								 else // ������ �����������
								 {
									 PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 0);
									 SetViewByDisconnect (); // ������ �����������
									 AfxMessageBox (L"�� ������� ������������ � COM �����!\n��������� ������� - ������ ������������ ����� COM �����.", MB_ICONERROR);
								 }
								 //HideCaptionBarIndicator(indicOK);
								 CheckCaptionState ();
	}
		break;

	case UM_DISCONNECTION_NOTIFY:
	{
									PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 0);

									bool bDisconnectionState = (pMessageSet->nData == 1) ? true : false;
									SetViewByDisconnect (); // after disconnect
									GetDataStore ()->ClearReceivedDataStore ();
									unsigned int nAddress = 0;
									GetDataFrameAddress (FRAME_TYPE_EMPTY, nAddress);
									UpdateInlayByReceivedParamMeanings (nAddress);
									//HideCaptionBarIndicator(indicOK);
									CheckCaptionState ();

									if (IsPrgState (psCloseAppAfterDisconnect))
										bCloseApp = true;
	}
		break;

	case UM_PARSE_RECEIVED_DATA_NOTIFY:
	{
										  switch (ns_Conn::gCurrentProtocol)
										  {
										  case eProtocol61107: ParseMonitoringData_Protocol61107 ((void*)pMessageSet->pData); break;
										  }
	}
		break;

	case UM_REFLECT_RESULT_STR_NOTIFY:
	{
										 CString& sMsg = pMessageSet->sData;
										 if (!sMsg.IsEmpty ())
											 AfxMessageBox (sMsg, MB_ICONERROR);
	}
		break;

	case UM_REFLECT_RESULT_INFO_NOTIFY:
	{
										  int nStatusIdx = GetStatusBarIndex (ID_STATBAR_FIELD_EXCHANGE_RESULT);
										  //SetStatusBarText(nStatusIdx, pMessageSet->sData);
	}
		break;
	}

	delete pMessageSet->pData;
	delete pMessageSet;

	if (bCloseApp)
		OnCloseAfterDisconnection ();

	return TRUE;
}

LRESULT CMainFrame::ExchIdentificationPassedOk (WPARAM wParam, LPARAM lParam)
{
	PostMessage (UM_SET_CURSOR, ID_WAIT_CURSOR, 0);
	m_bCanLookForKeepAlive = true;
	return TRUE;
}

void CMainFrame::PrepareExchData_Protocol61107 ()
{
	critical_section cs (m_mutexSwitchView);

	CProtocol_61107* pProtocol_61107 = dynamic_cast<CProtocol_61107*>(ns_Conn::gpProtocol);

	// Record data

	vector<SEND_UNIT> sendList;
	GetSendingData (&sendList);
	if (!sendList.empty ())
		pProtocol_61107->SetRecordReq (&sendList);

	// Monitoring data

	if (!IsCurrentView ()) // ���� ��� ������ ������������ �������, �� current view �� �����
	{
		pProtocol_61107->SetMonitoringReqs (0, nullptr); // ��������� �������
		return;
	}

	CView* pView = GetActiveView_ ();
	if (!pView)
	{
		ASSERT (false);
		return;
	}
	CCommonViewDealer* pCurrView = dynamic_cast<CCommonViewDealer*>(pView);
	if (!pCurrView)
	{
		ASSERT (false);
		return;
	}

	dataList_t reqData;
	bool bIsDataListNew = false;
	pCurrView->GetDataForRead (&reqData, &bIsDataListNew);

	if (!bIsDataListNew)
		return; // �������� ������ ��� �����

	pCurrView->SetDataForReadOld ();
	pCurrView->ClearOnceReadingReq ();

	CCommonViewForm* pVF = dynamic_cast<CCommonViewForm*>(pCurrView);
	int nVFID = 0;
	if (pVF)
		nVFID = pVF->GetID ();
	int nCurrViewID = GetCurrentViewID (); // �������� ������ ������� �������
	if (nCurrViewID > 0
		&& nVFID == nCurrViewID)
	{
		GetDataStore ()->FormDataForReqs (nCurrViewID, &reqData);
		vector<pair<const unsigned char*, varOnce_t>>* pList = GetDataStore ()->GetDataForReqs ();
		pProtocol_61107->SetMonitoringReqs (nCurrViewID, pList);
	}
}

void CMainFrame::ParseMonitoringData_Protocol61107 (void* pData)
{
	/*
	����� ������ �� �������� ���� ���� �����:
	- �� ������ �� �����������, � ����� ������ ��������� ������ ���������:
	- ���������, ��� �������� ����� ����� ����� �����, ��� � � ����������� ������;
	- ���� ������ �� ����� - ������ ��������� � �����;
	- ���� ������ ����� - ����� �������� ������ (�����) � ��������� � �� DataStruct
	- ������� UpdateInlayByReceivedParamMeanings ��� ������ ���������� ������ �� �������;
	- ����� ������ ������ � �������, �� ������ ������� ������� ������ ������������� � ���������� ��������  �� ������, ���������:
	- ��������� ��� ������������� �� NAK (\xF), ��� ��������, ��� ���������� ������ �� ���������;
	- ���� ������� � ������ NAK - ������ ��������� � ������� � (�) - ��. ����;
	- ���� � ������ �� NAK, �� ��������� ����� �������������, ������� ������ ���� ����� ������ ������������ � ������� ������;
	- ���� ������ �� ����� - ������ ��������� � ������� � (�) - ��. ����;
	- (�) ������� UpdateInlayByReceivedConfirmation ���, ������ ������������, �������������� � ����������;

	��������! ��� �������� ��� ������ ������ � ������� ������������ ����� ��� ������, ��� ������������
	��������� �������������� ��� ������ � ��������� ���� ����������� ������ - ������� ������
	� ������� ���������������� ��� ������.

	*/

	EXCH_DATA* pExchData = (EXCH_DATA*)pData;
	ASSERT_AND_RETURN (pExchData);
	int nStatusIdx = GetStatusBarIndex (ID_STATBAR_FIELD_EXCHANGE_RESULT);
	bool bMonitoringData = !pExchData->bConfirmation;

	unsigned int nAddress = 0;
	if (!Convert_sAddressTo_nAddress (pExchData->sSOHID, nAddress))
		pExchData->bResult = false;

	if (bMonitoringData)
	{
		if (!pExchData->bResult)
		{
			CString s = L"������ ������ ������! ";
			TRACE (s + L"/r/n");
			SetStatusBarText (nStatusIdx, s + pExchData->sMsg); // Error msg in Status bar
		}
		else
		{
			if (GetDataStore ()->ParseReceivedMonitoringData (pExchData->sSOHID, pExchData->sData))
			{
				UpdateInlayByReceivedParamMeanings (nAddress);
			}
			else
			{
				CString s = L"�������� ������ �� ���������!";
				TRACE (s + L"/r/n");
				SetStatusBarText (nStatusIdx, s);
			}
		}
	}
	else
	{
		UpdateInlayByReceivedConfirmation (nAddress, pExchData->bResult);
		if (!pExchData->bResult)
		{
			CString s = L"���������� ������ �� ���������!";
			TRACE (s + L"/r/n");
			SetStatusBarText (nStatusIdx, s);
		}
	}
}

LRESULT CMainFrame::UpdateAccessData (WPARAM wParam, LPARAM lParam)
{
	REG_EDIT_ACCESS_DATA ac = *((REG_EDIT_ACCESS_DATA*)lParam);
	m_appEnviron.SaveAccessProp (ac);
	return TRUE;
}

LRESULT CMainFrame::ReadDataEvent (WPARAM wParam, LPARAM lParam)
{
	unsigned char* pData = (unsigned char*)wParam;
	int nDataSize = lParam;
	ns_Conn::gReadBinData.push_back ({ pData, nDataSize });

	//CString trace(pData);
	//TRACE(trace + L"\n");

	return TRUE;
}

LRESULT CMainFrame::WriteDataEvent (WPARAM wParam, LPARAM lParam)
{
	m_nExchKeepAlive = 0;
	unsigned char* pData = (unsigned char*)wParam;
	int nDataSize = lParam;
	ns_Conn::gWriteBinData.push_back ({ pData, nDataSize });

	if (m_bWaitCursor_oneWayCase)
		PostMessage (UM_SET_CURSOR2, ID_WAIT_CURSOR, 0);

	//CString trace(pData);
	//TRACE(trace + L"\n");

	return TRUE;
}

void CMainFrame::FormExchBinData ()
{
	ns_Conn::gReadBinData.reserve (65536);
	ns_Conn::gWriteBinData.reserve (65536);
}

void CMainFrame::FreeExchBinData ()
{
	for (auto& i : ns_Conn::gReadBinData)
		delete i.first;
	ns_Conn::gReadBinData.clear ();

	for (auto& i : ns_Conn::gWriteBinData)
		delete i.first;
	ns_Conn::gWriteBinData.clear ();
}

void CMainFrame::ExchWatchDogTick ()
{
	// ��������� ��������� m_nExchKeepAlive � 0.
	// ����� ������ ������ - �������� �����������, �������������, ������������� ������, �.�.
	// ��� �������� � ������������ ������ ������� ��������� ��� ���������� ����� �����
	// ����������� ������ ������� 250-�� ���������������.
	// ����� ����� � ��������� 0 - 120 - ��� ����� 30 ������.
	// ���� ���������� �����, ����� m_nExchKeepAlive ����������� � ��������� ��������� 0.
	// ���� �� ���������� 120 (30-�� ������) ������ �� ����, ������������ �������������� ����� - 
	// ������ ������� �� �������� ��� ������ ���������� ����-������.
	// ��� ��������� �� �������� m_nExchKeepAlive ����������� � 0 � ���������� ������� ��
	// ���������.
	// ���� ������ �� ����� ������, �� ������� �������, ��� ����� �������� � �����������
	// � ��������� ��������� - ������� ������ ������ ������ ������������� �� �������� 300 ���.

	if (!m_bCanLookForKeepAlive)
		return;

	++m_nExchKeepAlive;
	if (m_nExchKeepAlive >= 120) // ��� 30 ���.
	{
		m_nExchKeepAlive = 0;

		switch (ns_Conn::gCurrentProtocol)
		{
		case eProtocol61107: GetDeviceTimeLikeTestExchange_Protocol61107 (); break;
		}
	}
}

void CMainFrame::GetDeviceTimeLikeTestExchange_Protocol61107 ()
{
	unsigned int nAddress = 0;
	if (!GetDataFrameAddress (FRAME_TYPE_DATE_TIME, nAddress))
	{
		ASSERT (false);
		return;
	}

	unsigned int nFakeViewID = 0x7FFFFFFF;
	dataList_t reqData;
	varOnce_t bOnce = true_d (L"������� ������");
	reqData.push_back ({ nAddress, bOnce });
	GetDataStore ()->FormDataForReqs (nFakeViewID, &reqData);
	vector<pair<const unsigned char*, varOnce_t>>* pList = GetDataStore ()->GetDataForReqs ();
	CProtocol_61107* pProtocol_61107 = dynamic_cast<CProtocol_61107*>(ns_Conn::gpProtocol);
	pProtocol_61107->SetMonitoringReqs (nFakeViewID, pList);

	TRACE (L"........................ Tick!\n");
}