#pragma once


// CDoubleEdit

class CDoubleEdit : public CEdit
{
	DECLARE_DYNAMIC(CDoubleEdit)

public:
	CDoubleEdit();
	virtual ~CDoubleEdit();
	double GetVal () { 
		CString tmp;
		wchar_t* aEndPtr = nullptr;
		GetWindowText (tmp);
		m_val = _tcstod (tmp, &aEndPtr);		
		return m_val;
	}
	void GetVal (int& val)
	{
		val = static_cast <int> (GetVal());
	}
	void GetVal (double& val)
	{
		val =  static_cast <double> (GetVal ());;
	}

	void SetVal (double val) {
		iSDoubleView = true;
		m_val = val;
		if (this->m_hWnd) {
			CString str;
			if (precision == 1) {
				str.Format(_T("%3.1f"), m_val);				
			}
			else if (precision == 2) {
				str.Format(_T("%3.2f"), m_val);
			}
			else {
				str.Format(_T("%3.3f"), m_val);
			}
			SetWindowText(str);
		}
	}
	void SetVal (int val) {
		m_val = val;
		iSDoubleView = false;
		if (this->m_hWnd) {			
			CString str;
			str.Format(_T("%d"), static_cast <int> (m_val));
			SetWindowText(str);
		}
	}
	void SetVal (unsigned int val) {
		iSDoubleView = false;
		m_val = val;
		if (this->m_hWnd) {
			CString str;
			str.Format(_T("%d"), static_cast <unsigned int> (m_val));
			SetWindowText(str);
		}
	}
	void SetPrecion(int p) {
		precision = p;
	}
	void UpdateWindow ();
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnChar (UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdate ();	
	
private:
	CString m_strPrevValue;
	int m_nLastSel;
	double m_val;
	int precision;
	bool iSDoubleView;
};


