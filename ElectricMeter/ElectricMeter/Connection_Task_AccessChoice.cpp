﻿#include "stdafx.h"
#include "View_Connection.h"
#include "Ai_CmbBx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_AcCh
{
  bool g_bShowPassword = false;

  CView_Connection* gpViewConn = nullptr;
  void PasswordAfterFilter(CString sText)
  {
    gpViewConn->PasswordWasChanged();
  }

} // namespace ns_AcCh

void CView_Connection::PasswordWasChanged()
{
  SaveAccessChoiceValueToRegedit();
}

void CView_Connection::FormTask_AccessChoice()
{
  ns_AcCh::gpViewConn = this;
  CString s;
  s.LoadString(IDS_IGNORED_PASSWORD_CHARS);
  m_edtPassword.IgnoreSimbols(s);
  m_edtPassword.SetLimitText(30);
  m_edtPassword.CtrlTextAfterCheck = ns_AcCh::PasswordAfterFilter;
}

void CView_Connection::FillTask_AccessChoiceFromRegedit()
{
  m_bCanSave = false;

  bool bAdmin = IsConectionAdminAccess();
  CAi_CmbBx::SelectItem(&m_cbxLogin, bAdmin ? L"Admin" : L"User");
  m_edtPassword.SetPasswordChar('*');
  CString sPassword = bAdmin ? GetAdminPassword() : GetUserPassword();
  m_edtPassword.SetWindowText(sPassword);
  int nValue = ns_AcCh::g_bShowPassword ? 1 : 0;
  m_chkShowPassword.SetCheck(nValue);
  if (nValue)
  {
    m_edtPassword.SetPasswordChar(0);
    m_edtPassword.Invalidate();
  }

  m_bCanSave = true;
}

void CView_Connection::SaveAccessChoiceValueToRegedit()
{
  if (!m_bCanSave)
    return;

  CString sLogin;
  m_cbxLogin.GetWindowText(sLogin);
  bool bAdminLogin = (sLogin == L"Admin");
  CString sAdminPassword = GetAdminPassword();
  CString sUserPassword = GetUserPassword();
  CString& sPassword = bAdminLogin ? sAdminPassword : sUserPassword;
  m_edtPassword.GetWindowText(sPassword);
  SaveConnectionAccess(bAdminLogin, sAdminPassword, sUserPassword);
  TRACE(L"SaveAccessChoiceValueToRegedit. Admin psw-%s User psw-%s\n", sAdminPassword, sUserPassword);
}

void CView_Connection::OnCbnSelchangeCbxLogin()
{
  m_bCanSave = false;

  CString sLogin;
  m_cbxLogin.GetWindowText(sLogin);
  bool bAdminLogin = (sLogin == L"Admin");
  m_edtPassword.SetWindowText(bAdminLogin ? GetAdminPassword() : GetUserPassword());

  m_bCanSave = true;

  SaveAccessChoiceValueToRegedit();
}

void CView_Connection::OnBnClickedChkShowPassword()
{
  m_bCanSave = false;

  int nValue = m_chkShowPassword.GetCheck();
  if (nValue)
    m_edtPassword.SetPasswordChar(0);
  else
    m_edtPassword.SetPasswordChar('*');
  m_edtPassword.Invalidate();
  ns_AcCh::g_bShowPassword = (nValue == 0) ? false : true;
  
  m_bCanSave = true;
}