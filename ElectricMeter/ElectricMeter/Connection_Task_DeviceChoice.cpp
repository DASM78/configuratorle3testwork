#include "stdafx.h"
#include "View_Connection.h"
#include "Ai_CmbBx.h"
#include "Model_.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace mdl;

namespace ns_DevCho
{
#define SIDE_OFFSET 5

  struct X_TAGS
  {
    CParamsTreeNode* pDevTypeNode = nullptr;
    CString sAttr_DevType = L"DeviceName";
  }
  g_xTags;

} // namespace ns_DevCho

void CView_Connection::FormTask_DeviceChoice()
{
}

void CView_Connection::FillTask_DeviceChoiceFromXml(CParamsTreeNode* pDevTypeNode)
{
  m_bCanSave = false;

  ns_DevCho::g_xTags.pDevTypeNode = pDevTypeNode;
  CString sSelName = ns_DevCho::g_xTags.pDevTypeNode->GetAttributeValue(ns_DevCho::g_xTags.sAttr_DevType);

  bool bNameIs = false;
  for (int i = 0; i < SIZE_OF_ASSOC_DEV_NAMES; ++i)
  {
    const AssocDevNames* pAssoc = GetAssocDevNames(i);
    CString sName = pAssoc->sInApp;
    if (sName == sSelName)
    {
      bNameIs = true;
      SetOnAssocDevNames(sName);
    }
    m_cbxDevices.InsertString(i, sName);
  }

  if (!bNameIs)
    sSelName = GetAssocDevNames(0)->sInApp;
  CAi_CmbBx::SelectItem(&m_cbxDevices, sSelName);

  m_bCanSave = true;
}

void CView_Connection::SaveDeviceChoiceValueToXml()
{
  if (!m_bCanSave)
    return;

  CString sSelName;
  m_cbxDevices.GetWindowText(sSelName);
  ns_DevCho::g_xTags.pDevTypeNode->DeleteAllAttributes();
  ns_DevCho::g_xTags.pDevTypeNode->AddAttribute(ns_DevCho::g_xTags.sAttr_DevType, sSelName);
  UpdateXmlBackupFile();

  for (int i = 0; i < SIZE_OF_ASSOC_DEV_NAMES; ++i)
  {
    const AssocDevNames* pAssoc = GetAssocDevNames(i);
    CString sName = pAssoc->sInApp;
    if (sName == sSelName)
      SetOnAssocDevNames(sName);
  }
}

void CView_Connection::OnCbxChangeEdtDeviceAddress()
{
  SaveDeviceChoiceValueToXml();
}