#include "stdafx.h"
#include "Ai_Str.h"
#include "Resource.h"
#include "AppEnviron.h"
#include "CommonViewDealer.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_CmnDlr
{
  REG_EDIT_ACCESS_DATA g_accessInfo;

} // namespace ns_CmnDlr

CCommonViewDealer::CCommonViewDealer()
{
}

CCommonViewDealer::~CCommonViewDealer()
{
}

void CCommonViewDealer::BeforeDestroy()
{
  BeginFormingReadPack(true);

  m_sendDataList.clear();

  ClearAllReadingReq();
  m_bNewDataList = true;

  BeforeDestroyClass();

  BeginFormingReadPack(false);
}

void CCommonViewDealer::SetWaitCursor_answer(bool bSet)
{
  ::PostMessage(m_hWndGlobParent, UM_SET_CURSOR, ID_WAIT_CURSOR, bSet ? 1 : 0);
}

void CCommonViewDealer::SetWaitCursor_oneWay(bool bSet)
{
  ::PostMessage(m_hWndGlobParent, UM_SET_CURSOR2, ID_WAIT_CURSOR, bSet ? 1 : 0);
}

void CCommonViewDealer::UpdateXmlBackupFile()
{
  ::PostMessage(m_hWndGlobParent, UM_UPDATE_XML_BACKUP_FILE, 0, 0);
}

void CCommonViewDealer::SetAccessInfo(bool bAdminAccess, CString sAdminPass, CString sUserPass)
{
  m_bConnectionAdminAccess = bAdminAccess;
  m_sAdminPassword = sAdminPass;
  m_sUserPassword = sUserPass;
}

void CCommonViewDealer::SaveConnectionAccess(bool bAdminAccess, CString sAdminPass, CString sUserPass)
{
  m_bConnectionAdminAccess = bAdminAccess;
  m_sAdminPassword = sAdminPass;
  m_sUserPassword = sUserPass;

  ns_CmnDlr::g_accessInfo.sCurrLogin = m_bConnectionAdminAccess ? L"Admin" : L"User";
  ns_CmnDlr::g_accessInfo.sUserPassword = sUserPass;
  ns_CmnDlr::g_accessInfo.sAdminPassword = sAdminPass;
  ::PostMessage(m_hWndGlobParent, UM_UPDATE_ACCESS_DATA, 0, (LPARAM)&ns_CmnDlr::g_accessInfo);
}

void CCommonViewDealer::SetConnectionState(bool bState)
{
  m_bConnectionState = bState;
}

void CCommonViewDealer::UpdateViewByConnectionState()
{
  if (m_bConnectionState
      && !m_dataList.empty())
      m_bNewDataList = true;

  WatchForCtrState();
  OnConnectionStateWasChanged();
}

bool CCommonViewDealer::IsConnection()
{
  return m_bConnectionState;
}

bool CCommonViewDealer::IsConectionAdminAccess()
{
  return m_bConnectionAdminAccess;
}

CString CCommonViewDealer::GetAdminPassword()
{
  return m_sAdminPassword;
}

CString CCommonViewDealer::GetUserPassword()
{
  return m_sUserPassword;
}

void CCommonViewDealer::WatchForCtrState(CWnd* pW, UserRights ur)
{
  m_watchForCtrState.push_back({pW, ur});
}

void CCommonViewDealer::WatchForCtrState2(CWnd* pW)
{
  m_watchForCtrState2.push_back(pW);
}

void CCommonViewDealer::WatchForCtrState()
{
  for (auto& i : m_watchForCtrState)
  {
    BOOL bEnable = IsConnection() ? TRUE : FALSE;
    if (bEnable && i.second == rightAdmin)
      bEnable = IsConectionAdminAccess();

    i.first->EnableWindow(bEnable);
  }

  for (auto i : m_watchForCtrState2)
    i->EnableWindow(IsConnection() ? FALSE : TRUE);
}

void CCommonViewDealer::BeginFormingReadPack(bool bStart)
{
  m_bReadPackIsForming = bStart;
}

void CCommonViewDealer::SetParamTypeForRead(teFrameTypes ft, varOnce_t bOnce)
{
  unsigned int nAddress = 0;
  if (GetDataFrameAddress(ft, nAddress))
    SetParamTypeForRead(nAddress, bOnce);
}

void CCommonViewDealer::SetParamTypeForRead(unsigned int nAddress, varOnce_t bOnce)
{
  for (size_t i = 0; i < m_dataList.size(); ++i)
  {
    if (m_dataList[i].first == nAddress) // ��� ���� � ������� � ��� �� ��������� �������
      return;
  }
  m_dataList.push_back(pair<unsigned int, varOnce_t>(nAddress, bOnce));
  m_bNewDataList = true;
}

void CCommonViewDealer::GetDataForRead(dataList_t* pData, bool* pbIsDataListNew)
{
  if (m_bReadPackIsForming)
    return;

  *pData = m_dataList;
  *pbIsDataListNew = m_bNewDataList;
}

void CCommonViewDealer::SetDataForReadOld()
{
  m_bNewDataList = false;
}

void CCommonViewDealer::ClearOnceReadingReq()
{
  for (size_t i = 0; i < m_dataList.size(); ++i)
  {
    if (m_dataList[i].second) // varOnce_t
    {
      m_dataList.erase(m_dataList.begin() + i);
      --i;
    }
  }
}

void CCommonViewDealer::ClearAllReadingReq()
{
  m_dataList.clear();
}

void CCommonViewDealer::GetDataForSend(vector<SEND_UNIT>* pList)
{
  if (m_bSendPackIsForming)
    return;
  if (m_sendDataList.empty())
    return;
  ASSERT(pList);
  *pList = m_sendDataList;
  m_sendDataList.clear();
}

void CCommonViewDealer::BeginFormingSendPack(bool bStart)
{
  m_bSendPackIsForming = bStart;
}

void CCommonViewDealer::SendDataToDevice(teFrameTypes sendType)
{
  SendDataToDevice(sendType, L"");
}

void CCommonViewDealer::SendDataToDevice(teFrameTypes sendType, int nValue)
{
  SendDataToDevice(sendType, CAi_Str::ToString(nValue));
}

void CCommonViewDealer::SendDataToDevice(teFrameTypes sendType, CString sValue)
{
  if (sValue.IsEmpty())
  {
    USES_CONVERSION;
    sValue = L"()";
    CSendUnit s(sendType, W2A(sValue.GetBuffer()));
    SetParamTypeForSend(s.Get());
  }
  else
  {
    USES_CONVERSION;
    sValue = L"(" + sValue + L")";
    CSendUnit s(sendType, W2A(sValue.GetBuffer()));
    SetParamTypeForSend(s.Get());
  }
}

void CCommonViewDealer::SetParamTypeForSend(SEND_UNIT* pSU)
{
  if (pSU != nullptr)
  {
    SEND_UNIT su{};
    su = *pSU;
    m_sendDataList.push_back(su);
  }
}

void CCommonViewDealer::UpdateMonitoringCtrlView(CAi_ColorStatic* pCtrl)
{
  COLORREF cText = RGB(0, 0, 10);
  COLORREF cBg = RGB(255, 255, 217);
  if (!IsConnection())
  {
    cText = RGB(100, 100, 100); // grey
    cBg = RGB(220, 220, 220);
  }
  pCtrl->SetTxColor(cText);
  pCtrl->SetBkColor(cBg);
}

/*void CCommonViewDealer::SetPointerToReceivedDataStore(const dataTypesAndVarMeanings_t* pReceivedDataStore)
{
  const_cast<const dataTypesAndVarMeanings_t*>(m_pReceivedDataStore) = pReceivedDataStore;
}*/

void CCommonViewDealer::SetPointerToDataFinder(bool(*pFn)(const unsigned int nInAddress, CString& sOutData))
{
  pfnDataStoreFinder = pFn;
}

void CCommonViewDealer::SetParamTypeForCell(int nRow_inList, int nCell, teFrameTypes ft)
{
  SetParamTypeForRead(ft);
  m_pCtrlEx->SetCellFlag(nRow_inList, nCell, ft);
}

void CCommonViewDealer::UpdateParamMeaningsByReceivedDeviceData(unsigned int nAddress)
{
  teFrameTypes ft = FRAME_TYPE_EMPTY;
  int nExIndex = 0;
  GetDataFrameType(nAddress, ft, nExIndex);
  UpdateParamMeaningsByReceivedDeviceData(ft);
}

void CCommonViewDealer::UpdateParamMeaningByReceivedDeviceData(int nRow_inList, int nCell)
{
  ASSERT(m_pCtrlEx);
  teFrameTypes ft = (teFrameTypes)m_pCtrlEx->GetCellFlag(nRow_inList, nCell);
  varMeaning_t m = FindReceivedData(ft);
  m_pCtrlEx->SetCellText(nRow_inList, nCell, m);
}

varMeaning_t CCommonViewDealer::FindReceivedData(teFrameTypes ft)
{
  unsigned int nAddress = 0;
  if (!GetDataFrameAddress(ft, nAddress))
  {
    ASSERT(false);
    return L"?";
  }
  return FindReceivedData(nAddress);
}

varMeaning_t CCommonViewDealer::FindReceivedData(unsigned int nAddress)
{
  CString sMeaning;
  if (pfnDataStoreFinder(nAddress, sMeaning))
    return sMeaning;

  /*if (m_pReceivedDataStore != nullptr
      && !m_pReceivedDataStore->empty())
  {
    auto iter = m_pReceivedDataStore->begin();
    for (size_t i = 0; i < m_pReceivedDataStore->size(); ++i, ++iter)
    {
      if (iter->first == nAddress)
        return iter->second;
    }
  }*/

  return L"?";
}

void CCommonViewDealer::FormListForClearReceivedData(teFrameTypes ft)
{
  m_clearedList.push_back(ft);
}

void CCommonViewDealer::AfterClearReceivedData()
{
  ClearListForClearReceivedData();
  DoAfterClearLoadData();
}

void CCommonViewDealer::ClearListForClearReceivedData()
{
  m_clearedList.clear();
}

void CCommonViewDealer::ClearReceivedData(bool bComeback)
{
  ::PostMessage(m_hWndGlobParent, UM_CLEAR_RECEIVED_DATA_STORE_ITEM, bComeback ? 1 : 0, (LPARAM)&m_clearedList);
}

void CCommonViewDealer::UpdateInlayByReceivedConfirmation(unsigned int nAddress, bool bResult)
{
  teFrameTypes ft = FRAME_TYPE_EMPTY;
  int nExIndex = 0;
  GetDataFrameType(nAddress, ft, nExIndex);
  UpdateInlayByReceivedConfirmation(ft, bResult);
}
