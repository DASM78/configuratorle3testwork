#pragma once
#include "afxdtctl.h"


// CDialogDate dialog

class CDialogDate : public CDialog
{
	DECLARE_DYNAMIC(CDialogDate)

public:
	CDialogDate (CWnd* pParent = NULL);   // standard constructor
	CDialogDate (SYSTEMTIME *date, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDialogDate();

// Dialog Data
	enum { IDD = IDD_DIALOG_DATE };

protected:
	
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	SYSTEMTIME *m_pdate;
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk ();
	CMonthCalCtrl m_calendar;
};
