#include "StdAfx.h"
#include "Resource.h"
#include "MainFrm.h"
#include "Ai_File.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static UINT indicators[] =
{
  ID_SEPARATOR           // status line indicator
  //ID_INDICATOR_CAPS,
  //ID_INDICATOR_NUM,
  //ID_INDICATOR_SCRL,
};

static UINT statusFields[] =
{
  //ID_STATBAR_CONNECTION_INDICATOR,
  ID_STATBAR_FIELD_DOCUMENT,
  //ID_STATBAR_FIELD_EXCHANGE_RESULT,
  //ID_STATBAR_FIELD_PORT
};

bool CMainFrame::CreateStatusBar()
{
  if (!m_wndStatusBar.Create(this, WS_CHILD | WS_VISIBLE | CBRS_BOTTOM/* | CBRS_TOOLTIPS*/)
      // ��������� ��������� ����-���� �� ������� indicators
      || !m_wndStatusBar.SetIndicators(statusFields,
      sizeof(statusFields) / sizeof(UINT))
      )
  {
    TRACE0("Failed to create status bar\n");
    return false;      // fail to create
  }

  m_wndStatusBar.ShowOwnedPopups(FALSE); // �����. ��� �������� ��� ������������� ����, ������������� ��������� ����

  m_wndStatusBar.EnablePaneDoubleClick();

  m_wndStatusBar.SetPaneStyle(GetStatusBarIndex(ID_STATBAR_FIELD_DOCUMENT), SBPS_STRETCH); // ��������� ������ ������

  return true;
}

int CMainFrame::GetStatusBarIndex(UINT nResID)
{
  int nIndicatorCount = sizeof(statusFields) / sizeof(UINT);
  for (int i = 0; i < nIndicatorCount; ++i)
  {
    if (statusFields[i] == nResID)
      return i;
  }
  return 0;
}

void CMainFrame::OnDblClickToDocumentField()
{
  CString sPathFile = m_wndStatusBar.GetPaneText(GetStatusBarIndex(ID_STATBAR_FIELD_DOCUMENT));
  sPathFile.TrimLeft(L".");
  if (!sPathFile.IsEmpty())
  {
    if (CAi_File::IsFileExist(sPathFile))
      ShellExecute(nullptr, _T("open"), nullptr, nullptr, sPathFile, SW_SHOWNORMAL);
  }
}

void CMainFrame::SetStatusBarText(int nFieldIndex, CString sText)
{
  /*CString sStatusMsg = m_wndStatusBar.GetPaneText(nFieldIndex);
  sStatusMsg.TrimLeft(L" ");
  sStatusMsg.TrimRight(L" ");
  if (sStatusMsg != sText)
    m_wndStatusBar.SetPaneText(nFieldIndex, sText);*/
}