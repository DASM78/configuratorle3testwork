#pragma once

#include <windows.h>

namespace aux
{

  class mutex
  {
    CRITICAL_SECTION cs;
  public:
    void lock()
    {
      EnterCriticalSection(&cs);
    }
    void unlock()
    {
      LeaveCriticalSection(&cs);
    }
    mutex()
    {
      InitializeCriticalSection(&cs);
    }
    ~mutex()
    {
      DeleteCriticalSection(&cs);
    }
  };

  class critical_section
  {
    mutex& m;
  public:
    critical_section(mutex& guard): m(guard)
    {
      m.lock();
    }
    ~critical_section()
    {
      m.unlock();
    }
  };

  struct _event
  {
    HANDLE h;
    _event()
    {
      h = CreateEvent(nullptr, FALSE, FALSE, nullptr);
    }
    _event(LPCTSTR eventname)
    {
      h = CreateEvent(nullptr, FALSE, FALSE, eventname);
    }
    ~_event()
    {
      CloseHandle(h);
    }
    void signal()
    {
      SetEvent(h);
    }
    bool wait(unsigned int ms = INFINITE)
    {
      return WaitForSingleObject(h, ms) == WAIT_OBJECT_0;
    }

  };

  inline void yield()
  {
    Sleep(0);
  }
}
