#include "stdafx.h"
#include "Resource.h"
#include "Ai_Bmp.h"
#include "Ai_Str.h"
#include "MFCCaptionBarExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMFCCaptionBarExt::CMFCCaptionBarExt()
{
  m_bmpConnect.LoadBitmap(IDB_CONNECT);
  m_bmpConnectMouseOver.LoadBitmap(IDB_CONNECT_MOUSE_OVER);
  m_bmpConnectClick.LoadBitmap(IDB_CONNECT_CLICK);
  m_bmpConnection.LoadBitmap(IDB_CONNECTION);
  m_bmpConnectionMouseOver.LoadBitmap(IDB_CONNECTION_MOUSE_OVER);
  m_bmpConnectionClick.LoadBitmap(IDB_CONNECTION_CLICK);
}

CMFCCaptionBarExt::~CMFCCaptionBarExt()
{
}

BEGIN_MESSAGE_MAP(CMFCCaptionBarExt, CMFCCaptionBar)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSELEAVE()
END_MESSAGE_MAP()

void CMFCCaptionBarExt::SetBgColor(EBgColor bg)
{
  if (m_bIndicatorMode)
    m_bgPreviousColor = bg;
  else
    m_bgColor = bg;
}

void CMFCCaptionBarExt::EnableIndicatorMode(bool bEnable)
{
  if (bEnable)
  {
    m_bgPreviousColor = m_bgColor;
    SetBgColor(clrLGreen_Green);
    m_bIndicatorMode = true;
  }
  else
  {
    m_bIndicatorMode = false;
    SetBgColor(m_bgPreviousColor);
  }
}

void CMFCCaptionBarExt::SetIndicatorPos(BYTE nPos)
{
  m_nIndicatorPos = nPos;
}

void CMFCCaptionBarExt::OnDrawBackground(CDC* pDC, CRect rect)
{
  COLOR16 nR_vrtx1 = 0xFF00;
  COLOR16 nG_vrtx1 = 0xFF00;
  COLOR16 nB_vrtx1 = 0xFF00;

  COLOR16 nR_vrtx2 = 0xEF00; // 239
  COLOR16 nG_vrtx2 = 0xEF00;
  COLOR16 nB_vrtx2 = 0xEF00;

  COLOR16 nR_vrtxIndic1 = 0xFF00;
  COLOR16 nG_vrtxIndic1 = 0xFF00;
  COLOR16 nB_vrtxIndic1 = 0xFF00;

  COLOR16 nR_vrtxIndic2 = 0xEF00; // 239
  COLOR16 nG_vrtxIndic2 = 0xEF00;
  COLOR16 nB_vrtxIndic2 = 0xEF00;

  switch (m_bgColor)
  {
    case clrGray_LGray:
      nR_vrtx1 = 0xC800; // 200
      nG_vrtx1 = 0xC800;
      nB_vrtx1 = 0xC800;
      break;

    case clrFree_Free:
      nR_vrtx1 = 0xEE00; // 200
      nG_vrtx1 = 0xC800;
      nB_vrtx1 = 0xEE00;
      break;

    case clrWhite_LGray:
      nR_vrtx1 = 0xFF00; // 255
      nG_vrtx1 = 0xFF00;
      nB_vrtx1 = 0xFF00;
      break;

    case clrYelow_LYellow:
      nR_vrtx1 = 0xEC00; // 236
      nG_vrtx1 = 0xE200; // 226
      nB_vrtx1 = 0x5900; // 89

      nR_vrtx2 = 0xFF00; // 255
      nG_vrtx2 = 0xFF00;
      nB_vrtx2 = 0xDC00; // 220
      break;

    case clrBlue_LBlue:
      nR_vrtx1 = 0xA000; // 160
      nG_vrtx1 = 0xC000; // 192
      nB_vrtx1 = 0xEC00; // 236

      nR_vrtx2 = 0xA600; // 6
      nG_vrtx2 = 0xFF00; // 174
      nB_vrtx2 = 0xFF00;
      break;

    case clrLGreen_Green:
      nR_vrtx1 = 0xC800;
      nG_vrtx1 = 0xC800;
      nB_vrtx1 = 0xC800;

      nR_vrtx2 = 0xC800; // 239
      nG_vrtx2 = 0xC800;
      nB_vrtx2 = 0xC800;

      nR_vrtxIndic1 = 0xDA00; // 218
      nG_vrtxIndic1 = 0xFD00; // 253
      nB_vrtxIndic1 = 0xE200; // 226

      nR_vrtxIndic2 = 0xB100; // 177
      nG_vrtxIndic2 = 0xC100; // 193
      nB_vrtxIndic2 = 0x0300; // 3
      break;
  }

  CRect rectIndic = rect;
  if (m_bIndicatorMode)
  {
    if (m_nIndicatorPos < 100)
    {
      float nWidth = (float)rectIndic.Width(); // 100%
      float _1Prc = (nWidth / 100);
      rectIndic.right = rectIndic.left + (int)(_1Prc * m_nIndicatorPos);
    }    
  }

  TRIVERTEX vertex[2] = {
    {rect.left, rect.top, nR_vrtx1, nG_vrtx1, nB_vrtx1, 0x0000},
    {rect.right, rect.bottom, nR_vrtx2, nG_vrtx2, nB_vrtx2, 0x0000}
  };
  TRIVERTEX vertexIndic[2] = {
    {rectIndic.left, rectIndic.top, nR_vrtxIndic1, nG_vrtxIndic1, nB_vrtxIndic1, 0x0000},
    {rectIndic.right, rectIndic.bottom, nR_vrtxIndic2, nG_vrtxIndic2, nB_vrtxIndic2, 0x0000}
  };
  GRADIENT_RECT grRect = {0, 1};

  pDC->GradientFill(vertex, 2, &grRect, 1, GRADIENT_FILL_RECT_H);
  
  if (m_bIndicatorMode)
    pDC->GradientFill(vertexIndic, 2, &grRect, 1, GRADIENT_FILL_RECT_H);
}

void CMFCCaptionBarExt::OnDrawText(CDC* pDC, CRect rect, const CString& strText)
{
  m_hFont = CreateBoldWindowFont(m_hWnd);
  pDC->SetTextColor(RGB(33, 44, 168));
  pDC->DrawText(strText, &rect, DT_VCENTER);
}

HFONT CMFCCaptionBarExt::CreateBoldWindowFont(HWND window)
{
  const HFONT font = (HFONT)SendMessage(WM_GETFONT, 0, 0);
  LOGFONT fontAttributes = {0};
  ::GetObject(font, sizeof(fontAttributes), &fontAttributes);
  wcscpy_s(fontAttributes.lfFaceName, 32, L"Verdana");
  fontAttributes.lfWeight = FW_SEMIBOLD;
  fontAttributes.lfQuality = CLEARTYPE_NATURAL_QUALITY;
  fontAttributes.lfHeight = -13;

  return ::CreateFontIndirect(&fontAttributes);
}

void CMFCCaptionBarExt::OnDrawButton(CDC* pDC, CRect rect, const CString& strButton, BOOL bEnabled)
{
  CBitmap* pBmp = &m_bmpConnect;

  switch (m_btnType)
  {
    case cbConnect:
      pBmp = m_bMouseOver ? &m_bmpConnectMouseOver : &m_bmpConnect;
      break;

    case cbConnectClick:
      pBmp = &m_bmpConnectClick;
      break;

    case cbBreakConnection:
      break;

    case cbConnection:
      pBmp = m_bMouseOver ? &m_bmpConnectionMouseOver : &m_bmpConnection;
      break;

    case cbConnectionClick:
      pBmp = &m_bmpConnectionClick;
      break;

    default:
      ASSERT(false);
  }

  m_rectConnect = rect;
  CAi_Bmp bmpAddin;
  bmpAddin.SetBMP(pBmp);
  CSize sz{};
  bmpAddin.GetBitmapSize(IDB_CONNECT, &sz);
  bmpAddin.MoveOutBMPToDC(pDC->m_hDC, &sz);

  ++m_nRedrawButton;
}

void CMFCCaptionBarExt::CheckButtonView(POINT* pPt)
{
  bool bChangeView = false;
  CRect rectConnect{};
  rectConnect = m_rectConnect;
  ClientToScreen(&rectConnect);
  rectConnect.left -= 4;
  if (pPt->x >= rectConnect.left
      && pPt->x <= rectConnect.right
      && pPt->y >= rectConnect.top
      && pPt->y <= rectConnect.bottom)
  {
    if (!m_bMouseOver)
    {
      bChangeView = true;
      m_bMouseOver = true;
      ++m_nNeedRedrawButton;
    }
  }
  else
  {
    if (m_bMouseOver)
    {
      bChangeView = true;
      m_bMouseOver = false;
      ++m_nNeedRedrawButton;
    }
  }

  if (!bChangeView) // ������ �� ���������, ��� ��������� ����� ���������
  {
    if (m_nNeedRedrawButton > 0) // �� ������������ ����� ���� �� ��������� ������ ��������� �� ���������
    {
      m_nNeedRedrawButton--;
      Invalidate();
    }
  }
}

BOOL CMFCCaptionBarExt::PreTranslateMessage(MSG* pMsg)
{
  return CMFCCaptionBar::PreTranslateMessage(pMsg);
}

void CMFCCaptionBarExt::OnLButtonDown(UINT nFlags, CPoint point)
{
  m_btnTypePrev = m_btnType;
  SetBtnType((m_btnType == cbConnect) ? cbConnectClick : cbConnectionClick);
  CMFCCaptionBar::OnLButtonDown(nFlags, point);
}

void CMFCCaptionBarExt::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (m_btnTypePrev != cbNone)
  {
    SetBtnType(m_btnTypePrev);
    m_btnTypePrev = cbNone;
  }
  CMFCCaptionBar::OnLButtonUp(nFlags, point);
}

void CMFCCaptionBarExt::OnMouseLeave()
{
  if (m_btnTypePrev != cbNone)
  {
    SetBtnType(m_btnTypePrev);
    m_btnTypePrev = cbNone;
  }
  CMFCCaptionBar::OnMouseLeave();
}
