#pragma once

#include "Ai_ListCtrlEx.h"

class CComboBox_binding : public CComboBox
{
  DECLARE_DYNAMIC(CComboBox_binding)

public:
  CComboBox_binding();
  virtual ~CComboBox_binding();

  BOOL Create_(CWnd* pParent, int nID, bool bSort, �Ai_ListCtrlEx* pListCtrEx);

protected:
  DECLARE_MESSAGE_MAP()

private:
  �Ai_ListCtrlEx* m_pListCtrEx = nullptr;
  int m_nID = -1;

  afx_msg void OnSelItemAndPressOK();
  afx_msg void OnPressCancel();
  //afx_msg void OnSelChange();
  //afx_msg void OnCbnCloseup();

  bool m_bCloseUpWas = false;

public:
  void SelectItem(CComboBox* pCmbBx, CString str);
  void FillIn(int nRowCount);
};


