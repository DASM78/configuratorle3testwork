#include "stdafx.h"
#include <atlbase.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <time.h>
#include "Resource.h"
#include "Defines.h"
#include "Ai_Font.h"
#include "Ai_Str.h"
#include "View_Time.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_Time, CCommonViewForm)

namespace ns_Tm
{
	struct X_TAGS
	{
		CString sTime = L"Time";
		CParamsTreeNode* pTimeNode = nullptr;

		CString sUser_Settings = L"User_Settings";
		CParamsTreeNode* pUserSetNode = nullptr;
		CString sCheckTime = L"CheckTime";
		CString sUserTime = L"Time";
		CString sCheckDate = L"CheckDate";
		CString sUserDate = L"Date";
		CString sCheckSeason = L"CheckSeasons";
		CString sUserSeason = L"Seasons";
	}
	g_xTags;

} // namespace ns_Tm

CView_Time::CView_Time()
	: CCommonViewForm(IDD_SETTINGS_TIME, L"CView_Time")
{
}

CView_Time::~CView_Time()
{
}

void CView_Time::DoDataExchange(CDataExchange* pDX)
{
	CCommonViewForm::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STC_TIME_COL, m_stcTimeCol);
	DDX_Control(pDX, IDC_STC_DATE_COL, m_stcDateCol);
	DDX_Control(pDX, IDC_STC_SEASON_COL, m_stcSeasonCol);
	DDX_Control(pDX, IDC_STC_RECORD_COL, m_stcRecordCol);
	DDX_Control(pDX, IDC_STC_DEVICE_ROW, m_stcDeviceRow);
	DDX_Control(pDX, IDC_STC_SYSTEM_ROW, m_stcSystemRow);
	DDX_Control(pDX, IDC_STC_USER_ROW, m_stcUserRow);
	DDX_Control(pDX, IDC_STC_DEVICE_TIME, m_stcDeviceTime);
	DDX_Control(pDX, IDC_STC_DEVICE_DATE, m_stcDeviceDate);
	DDX_Control(pDX, IDC_STC_DEVICE_SEASON, m_stcDeviceSeason);
	DDX_Control(pDX, IDC_STC_SYSTEM_TIME, m_stcSystemTime);
	DDX_Control(pDX, IDC_STC_SYSTEM_DATE, m_stcSystemDate);
	DDX_Control(pDX, IDC_STC_SYSTEM_SEASON, m_stcSystemSeason);
	DDX_Control(pDX, IDC_TIME_CHANGER, m_datUserTime);
	DDX_Control(pDX, IDC_DATE_CHANGER, m_datUserDate);
	DDX_Control(pDX, IDC_RDO_USE_SEASONS, m_rdoUserOnSeason);
	DDX_Control(pDX, IDC_RDO_NO_USE_SEASONS, m_rdoUserOffSeason);
	DDX_Control(pDX, IDC_CHK_CHANGE_TIME, m_chkUserTime);
	DDX_Control(pDX, IDC_CHK_CHANGE_DATE, m_chkUserDate);
	DDX_Control(pDX, IDC_CHK_USE_SEASONS, m_chkUserSeason);
	DDX_Control(pDX, IDC_BTN_RECORD_TIME_TO_DEVICE, m_btnRecordSystem);
	DDX_Control(pDX, IDC_BTN_RECORD_TIME_TO_DEVICE2, m_btnRecordUser);
}

BEGIN_MESSAGE_MAP(CView_Time, CCommonViewForm)
	ON_BN_CLICKED(IDC_CHK_CHANGE_TIME, &CView_Time::OnBnClickedChkChangeTime)
	ON_BN_CLICKED(IDC_CHK_CHANGE_DATE, &CView_Time::OnBnClickedChkChangeDate)
	ON_BN_CLICKED(IDC_CHK_USE_SEASONS, &CView_Time::OnBnClickedChkChangeSeason)
	ON_BN_CLICKED(IDC_RDO_USE_SEASONS, &CView_Time::OnBnClickedRdoUseSeasons)
	ON_BN_CLICKED(IDC_BTN_RECORD_TIME_TO_DEVICE, &CView_Time::OnBnClickedBtnRecordSystemTimeToDevice)
	ON_BN_CLICKED(IDC_BTN_RECORD_TIME_TO_DEVICE2, &CView_Time::OnBnClickedBtnRecordUserTimeToDevice)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_TIME_CHANGER, &CView_Time::OnDtnDatetimechangeTimeChanger)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATE_CHANGER, &CView_Time::OnDtnDatetimechangeDateChanger)
END_MESSAGE_MAP()

void CView_Time::OnInitialUpdate()
{
	CCommonViewForm::OnInitialUpdate();

	SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

	// XML

	int nCnkTime = 0;
	int nCnkDate = 0;
	int nCnkSeason = 0;
	CTime timeTime(2017, 1, 1, 0, 0, 0);
	__time64_t nUserTime = timeTime.GetTime();
	__time64_t nUserDate = timeTime.GetTime();
	bool bSeasonOn = false;

	CParamsTreeNode* pInlaysNode = GetXmlNode();
	if (pInlaysNode)
	{
		CString sTag = ns_Tm::g_xTags.sTime;
		ns_Tm::g_xTags.pTimeNode = pInlaysNode->FindFirstNode(sTag);
		if (!ns_Tm::g_xTags.pTimeNode)
		{
			ns_Tm::g_xTags.pTimeNode = pInlaysNode->AddNode(sTag);
			sTag = ns_Tm::g_xTags.sUser_Settings;
			ns_Tm::g_xTags.pUserSetNode = ns_Tm::g_xTags.pTimeNode->AddNode(sTag);
		}
		else
		{
			sTag = ns_Tm::g_xTags.sUser_Settings;
			ns_Tm::g_xTags.pUserSetNode = ns_Tm::g_xTags.pTimeNode->FindFirstNode(sTag);
			if (!ns_Tm::g_xTags.pUserSetNode)
				ns_Tm::g_xTags.pUserSetNode = ns_Tm::g_xTags.pTimeNode->AddNode(sTag);
			else
			{
				CParamsTreeNode* pX = ns_Tm::g_xTags.pUserSetNode;

				CString sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sUserTime);
				swscanf_s(sTmp.GetBuffer(), L"%d", &nUserTime);
				sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sUserDate);
				swscanf_s(sTmp.GetBuffer(), L"%d", &nUserDate);
				sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sUserSeason);
				bSeasonOn = (sTmp == L"1") ? true : false;

				sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sCheckTime);
				nCnkTime = CAi_Str::ToInt(sTmp);
				sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sCheckDate);
				nCnkDate = CAi_Str::ToInt(sTmp);
				sTmp = pX->GetAttributeValue(ns_Tm::g_xTags.sCheckSeason);
				nCnkSeason = CAi_Str::ToInt(sTmp);
			}
		}
	}

	// Ctrls

	CAi_Font::CreateFont_(this, m_biggerFont, L"", FW_BOLD);
	CAi_Font::CreateFont_(this, m_biggerFont2, L"", FW_BOLD);// , 15);

	m_stcTimeCol.SetFont(&m_biggerFont2);
	m_stcDateCol.SetFont(&m_biggerFont2);
	m_stcSeasonCol.SetFont(&m_biggerFont2);
	m_stcRecordCol.SetFont(&m_biggerFont2);
	m_stcDeviceRow.SetFont(&m_biggerFont2);
	m_stcSystemRow.SetFont(&m_biggerFont2);
	m_stcUserRow.SetFont(&m_biggerFont2);

	m_stcDeviceTime.SetFont(&m_biggerFont);
	m_stcDeviceDate.SetFont(&m_biggerFont);
	m_stcDeviceSeason.SetFont(&m_biggerFont);

	m_chkUserTime.SetCheck(nCnkTime);
	m_datUserTime.EnableWindow(nCnkTime);
	m_chkUserDate.SetCheck(nCnkDate);
	m_datUserDate.EnableWindow(nCnkDate);
	m_chkUserSeason.SetCheck(nCnkSeason);
	m_rdoUserOnSeason.EnableWindow(nCnkSeason);
	m_rdoUserOffSeason.EnableWindow(nCnkSeason);

	m_datUserDate.SetFormat(_T("dd-MM-yy"));
	m_datUserTime.SetTime(nUserTime);
	m_datUserDate.SetTime(nUserDate);
	if (bSeasonOn)
	{
		m_rdoUserOnSeason.SetCheck(1);
		m_rdoUserOffSeason.SetCheck(0);
	}
	else
	{
		m_rdoUserOnSeason.SetCheck(0);
		m_rdoUserOffSeason.SetCheck(1);
	}

	// Exchange

	SetParamTypeForRead(FRAME_TYPE_DATE_TIME);
	SetParamTypeForRead(FRAME_TYPE_TIME_SEASON_ON);

	WatchForCtrState(&m_btnRecordSystem, rightAdmin);
	WatchForCtrState(&m_btnRecordUser, rightAdmin);

	CheckMeaningsViewByConnectionStatus();
	UpdateParamMeaningsByReceivedDeviceData();

	SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}


void CView_Time::BeforeDestroyClass()
{

}

void CView_Time::OnSize_()
{
}

void CView_Time::OnDraw(CDC* pDC)
{
}

bool CView_Time::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
	return true;
}

void CView_Time::OnTimer250()
{
	//TODO CS
	__timeb32 tbForBlock;
	_ftime32_s(&tbForBlock);
	CTime currTime(tbForBlock.time);
	CString sDate;
	sDate.Format(L"%s", currTime.Format(L"%d-%m-%Y"));
	CString sTime;
	//sTime.Format(L"%s.%hu", currTime.Format(L"%H:%M:%S"), tbForBlock.millitm);
	sTime.Format(L"%s", currTime.Format(L"%H:%M:%S"));
	CString sDateIn;
	m_stcSystemDate.GetWindowText(sDateIn);
	if (sDateIn != sDate)
		m_stcSystemDate.SetWindowText(sDate);
	CString sTimeIn;
	m_stcSystemTime.GetWindowText(sTimeIn);
	if (sTimeIn != sTime)
		m_stcSystemTime.SetWindowText(sTime);
}

#ifdef _DEBUG
void CView_Time::AssertValid() const
{
	CCommonViewForm::AssertValid();
}

void CView_Time::Dump(CDumpContext& dc) const
{
	CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_Time::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_Time::PutValueToXmlTree()
{
	if (!GetInitialUpdateFlag())
		return;

	CParamsTreeNode* pX = ns_Tm::g_xTags.pUserSetNode;
	pX->DeleteAllAttributes();

	int nCnkTime = m_chkUserTime.GetCheck();
	pX->AddAttribute(ns_Tm::g_xTags.sCheckTime, CAi_Str::ToString(nCnkTime));
	if (nCnkTime)
	{
		CTime timeTime;
		DWORD dwResult = m_datUserTime.GetTime(timeTime);
		if (dwResult == GDT_VALID)
		{
			LONG nT = static_cast<LONG>(timeTime.GetTime());
			pX->AddAttribute(ns_Tm::g_xTags.sUserTime, CAi_Str::ToString(nT));
		}
	}

	int nCnkDate = m_chkUserDate.GetCheck();
	pX->AddAttribute(ns_Tm::g_xTags.sCheckDate, CAi_Str::ToString(nCnkDate));
	if (nCnkDate)
	{
		CTime timeDate;
		DWORD dwResult = m_datUserDate.GetTime(timeDate);
		if (dwResult == GDT_VALID)
		{
			LONG nT = static_cast<LONG>(timeDate.GetTime());
			pX->AddAttribute(ns_Tm::g_xTags.sUserDate, CAi_Str::ToString(nT));
		}
	}

	int nCnkSeason = m_chkUserSeason.GetCheck();
	pX->AddAttribute(ns_Tm::g_xTags.sCheckSeason, CAi_Str::ToString(nCnkSeason));
	if (nCnkSeason)
	{
		int nOnSeason = m_rdoUserOnSeason.GetCheck();
		pX->AddAttribute(ns_Tm::g_xTags.sUserSeason, CAi_Str::ToString(nOnSeason));
	}

	UpdateXmlBackupFile();
}

void CView_Time::OnConnectionStateWasChanged()
{
	CheckMeaningsViewByConnectionStatus();
}

void CView_Time::CheckMeaningsViewByConnectionStatus()
{
	UpdateMonitoringCtrlView(&m_stcDeviceTime);
	UpdateMonitoringCtrlView(&m_stcDeviceDate);
	UpdateMonitoringCtrlView(&m_stcDeviceSeason);
}

void CView_Time::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
	if (WasErrorMsgBeShowed())
	{
		SetWaitCursor_answer(false);
		return;
	}

	varMeaning_t m = FindReceivedData(ft);
	if (m == L"\xF")
	{
		ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
		SetWaitCursor_answer(false);
		AfxMessageBox(L"������ �������� ������ � �������!", MB_ICONERROR);
		return;
	}

	if (ft == FRAME_TYPE_DATE_TIME)
	{
		CString sTime;
		CString sDate;
		vector <CString> timeDate;
		if (CAi_Str::CutString(m, L";", &timeDate)
			&& timeDate.size() == 2)
		{
			sDate = timeDate[0];
			sTime = timeDate[1];
		}

		varMeaning_t mCurr;
		m_stcDeviceTime.GetWindowText(mCurr);
		if (mCurr != sTime)
			m_stcDeviceTime.SetWindowText(sTime);

		m_stcDeviceDate.GetWindowText(mCurr);
		if (mCurr != sDate)
			m_stcDeviceDate.SetWindowText(sDate);
	}
}

void CView_Time::OnBnClickedChkChangeTime()
{
	int nCnkTime = m_chkUserTime.GetCheck();
	m_datUserTime.EnableWindow(nCnkTime);
	PutValueToXmlTree();
}

void CView_Time::OnBnClickedChkChangeDate()
{
	int nCnkDate = m_chkUserDate.GetCheck();
	m_datUserDate.EnableWindow(nCnkDate);
	PutValueToXmlTree();
}

void CView_Time::OnBnClickedChkChangeSeason()
{
	int nCnkSeason = m_chkUserSeason.GetCheck();
	m_rdoUserOnSeason.EnableWindow(nCnkSeason);
	m_rdoUserOffSeason.EnableWindow(nCnkSeason);
	PutValueToXmlTree();
}

void CView_Time::OnBnClickedRdoUseSeasons()
{
	PutValueToXmlTree();
}

void CView_Time::OnDtnDatetimechangeTimeChanger(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	PutValueToXmlTree();
	*pResult = 0;
}


void CView_Time::OnDtnDatetimechangeDateChanger(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	PutValueToXmlTree();
	*pResult = 0;
}

void CView_Time::UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult)
{
	if (ft == FRAME_TYPE_COMMAND_SET_DATE_TIME)
	{
		AfxMessageBox(L"����� ����� � ���� ��������", MB_ICONINFORMATION);
	}
}

void CView_Time::OnBnClickedBtnRecordSystemTimeToDevice()
{
	SetWaitCursor_oneWay(true);

	__timeb32 tbForBlock;
	_ftime32_s(&tbForBlock);
	CTime currTime(tbForBlock.time);
	CString sTime;
	sTime.Format(L"%s.%hu", currTime.Format(L"%d-%m-%Y;%H:%M:%S"), tbForBlock.millitm);
	BeginFormingSendPack(true_d(L"start"));
	SendDataToDevice(FRAME_TYPE_COMMAND_SET_DATE_TIME, sTime);
	BeginFormingSendPack(false_d(L"start"));
}

void CView_Time::OnBnClickedBtnRecordUserTimeToDevice()
{
	SetWaitCursor_oneWay(true);

	CString sTime;
	int nCnkTime = m_chkUserTime.GetCheck();
	if (nCnkTime)
	{
		CTime timeTime;
		DWORD dwResult = m_datUserTime.GetTime(timeTime);
		if (dwResult == GDT_VALID)
			sTime.Format(L"%s.0", timeTime.Format(L"%H:%M:%S"));
	}

	CString sDate;
	int nCnkDate = m_chkUserDate.GetCheck();
	if (nCnkDate)
	{
		CTime timeDate;
		DWORD dwResult = m_datUserDate.GetTime(timeDate);
		if (dwResult == GDT_VALID)
			sDate.Format(L"%s", timeDate.Format(L"%d-%m-%Y"));
	}

	CString sSeason;
	int nCnkSeason = m_chkUserSeason.GetCheck();
	if (nCnkSeason)
	{
		int nOnSeason = m_rdoUserOnSeason.GetCheck();
		sSeason = CAi_Str::ToString(nOnSeason);
	}

	BeginFormingSendPack(true_d(L"start"));

	if (!sDate.IsEmpty() || !sTime.IsEmpty())
	{
		CString sDateTime = sDate + L";" + sTime;
		SendDataToDevice(FRAME_TYPE_COMMAND_SET_DATE_TIME, sDateTime);
	}

	if (!sSeason.IsEmpty())
		SendDataToDevice(FRAME_TYPE_TIME_SEASON_ON, sSeason);

	BeginFormingSendPack(false_d(L"start"));
}