﻿#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "Ai_Font.h"
#include "View_Interfaces.h"
#include "CounterInfo.h"
using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE (CView_Interfaces, CCommonViewForm)

namespace ns_Inerf
{
#define SIDE_OFFSET 5

	struct X_TAGS
	{
		CString sInterfaces = L"DeviceInterfaces";
	}
	g_xTags;

	int g_nStateShowPassword = 0;
	int g_nNewAdminCheck = 1;

	CView_Interfaces* gpViewInerf = nullptr;

	void OldUserPasswordAfterFilter (CString sText)
	{
		gpViewInerf->OldUserPasswordWasChanged (sText);
	}
	void NewUserPasswordAfterFilter (CString sText)
	{
	}
	void OldAdminPasswordAfterFilter (CString sText)
	{
		gpViewInerf->OldAdminPasswordWasChanged (sText);
	}
	void NewAdminPasswordAfterFilter (CString sText)
	{
	}
	void NewUser2PasswordAfterFilter (CString sText)
	{
	}

} // namespace ns_Inerf

CView_Interfaces::CView_Interfaces ()
: CCommonViewForm (CView_Interfaces::IDD, L"CView_Interfaces")
{
	ns_Inerf::gpViewInerf = this;
	m_edtOldPasswordUser.CtrlTextAfterCheck = ns_Inerf::OldUserPasswordAfterFilter;
	m_edtNewPasswordUser.CtrlTextAfterCheck = ns_Inerf::NewUserPasswordAfterFilter;
	m_edtOldPasswordAdmin.CtrlTextAfterCheck = ns_Inerf::OldAdminPasswordAfterFilter;
	m_edtNewPasswordAdmin.CtrlTextAfterCheck = ns_Inerf::NewAdminPasswordAfterFilter;
	m_edtNewPasswordUser2.CtrlTextAfterCheck = ns_Inerf::NewUser2PasswordAfterFilter;
}

CView_Interfaces::~CView_Interfaces ()
{
}

BEGIN_MESSAGE_MAP (CView_Interfaces, CCommonViewForm)
	ON_WM_CONTEXTMENU ()

	ON_BN_CLICKED (IDC_CHK_SHOW_PASSWORD, &CView_Interfaces::OnBnClickedChkShowPassword)
	ON_BN_CLICKED (IDC_BTN_RECORD_PASSW_TO_DEVICE, &CView_Interfaces::OnBnClickedBtnRecordPasswToDevice)
	ON_BN_CLICKED (IDC_BTN_RECORD_TO_DEVICE, &CView_Interfaces::OnBnClickedBtnRecordToDevice)
	ON_BN_CLICKED (IDC_RDO_NEW_PSW_FOR_ADMIN, &CView_Interfaces::OnBnClickedRdoNewAdminPassword)
	ON_BN_CLICKED (IDC_RDO_NEW_PSW_FOR_USER, &CView_Interfaces::OnBnClickedRdoNewUserPassword)
	ON_BN_CLICKED (IDC_BTN_READ, &CView_Interfaces::OnBnClickedBtnRead)
END_MESSAGE_MAP ()

void CView_Interfaces::DoDataExchange (CDataExchange* pDX)
{
	CFormView::DoDataExchange (pDX);
	
	DDX_Control (pDX, IDC_STC_DEV_INTERFACES_CAP, m_stcInterfacesCap);
	DDX_Control (pDX, IDC_STC_PASSWORD_CAP, m_stcPasswordCap);
	DDX_Control (pDX, IDC_TAB_INTERFACES, m_tabInterfaces);
	DDX_Control (pDX, IDC_CHK_SHOW_PASSWORD, m_chkShowPassword);
	DDX_Control (pDX, IDC_EDT_OLD_PASSW_USER, m_edtOldPasswordUser);
	DDX_Control (pDX, IDC_EDT_NEW_PASSW_USER, m_edtNewPasswordUser);
	DDX_Control (pDX, IDC_EDT_OLD_PASSW_ADMIN, m_edtOldPasswordAdmin);
	DDX_Control (pDX, IDC_EDT_NEW_PASSW_ADMIN, m_edtNewPasswordAdmin);
	DDX_Control (pDX, IDC_EDT_NEW_PASSW_USER2, m_edtNewPasswordUser2);
	DDX_Control (pDX, IDC_BTN_RECORD_TO_DEVICE, m_btnRecordInterfacesToDevice);
	DDX_Control (pDX, IDC_BTN_RECORD_PASSW_TO_DEVICE, m_btnRecordPasswordsToDevice);
	DDX_Control (pDX, IDC_BTN_USER_OLD_PASSWORD, m_btnUserPasswordError);
	DDX_Control (pDX, IDC_BTN_ADMIN_OLD_PASSWORD, m_btnAdminPasswordError);
	DDX_Control (pDX, IDC_RDO_NEW_PSW_FOR_ADMIN, m_rdoNewAdminPassword);
	DDX_Control (pDX, IDC_RDO_NEW_PSW_FOR_USER, m_rdoNewUserPassword);
}

void CView_Interfaces::OnInitialUpdate ()
{
	CCommonViewForm::OnInitialUpdate ();
	WatchForCtrState(&m_btnRecordInterfacesToDevice, rightAdmin); 
	SetInitialUpdateFlag (false_d (L"Эта функция НЕ отработала"));

	// XML

	//

	m_SettTask_Prot_RS.SetGlobalParent (GetGlobalParent ());

	m_stcPasswordCap.SetBkColor (GetTaskCaptionColor ());
	m_stcInterfacesCap.SetBkColor (GetTaskCaptionColor ());

	AddCtrlForPositioning (&m_stcPasswordCap, movbehConstTopMarginAndVertSizes, movbehConstLeftAndRigthMargins);
	AddCtrlForPositioning (&m_stcInterfacesCap, movbehConstTopMarginAndVertSizes, movbehConstLeftAndRigthMargins);

	FormTask_InterfaceSets ();
	m_SettTask_Prot_RS.OnPostInit ();

	// Password properties

	m_chkShowPassword.SetCheck (ns_Inerf::g_nStateShowPassword);
	CheckShowPasswordState ();
	m_btnUserPasswordError.SetTooltip (L"Старые пароли не совпадают!");
	m_btnUserPasswordError.SetDelayFullTextTooltipSet (FALSE);
	m_btnAdminPasswordError.SetTooltip (L"Старые пароли не совпадают!");
	m_btnAdminPasswordError.SetDelayFullTextTooltipSet (FALSE);

	CString s;
	s.LoadString (IDS_IGNORED_PASSWORD_CHARS);
	m_edtOldPasswordUser.IgnoreSimbols (s);
	m_edtNewPasswordUser.IgnoreSimbols (s);
	m_edtOldPasswordAdmin.IgnoreSimbols (s);
	m_edtNewPasswordAdmin.IgnoreSimbols (s);
	m_edtNewPasswordUser2.IgnoreSimbols (s);
	m_edtOldPasswordUser.SetLimitText (30);
	m_edtNewPasswordUser.SetLimitText (30);
	m_edtOldPasswordAdmin.SetLimitText (30);
	m_edtNewPasswordAdmin.SetLimitText (30);
	m_edtNewPasswordUser2.SetLimitText (30);

	if (ns_Inerf::g_nNewAdminCheck)
	{
		m_rdoNewAdminPassword.SetCheck (1);
		OnBnClickedRdoNewAdminPassword ();
	}
	else
	{
		m_rdoNewUserPassword.SetCheck (1);
		OnBnClickedRdoNewUserPassword ();
	}

	// 

	//WatchForCtrState (&m_btnRecordInterfacesToDevice, rightAdmin);
	WatchForCtrState (&m_btnRecordPasswordsToDevice, rightCommon);
	CheckMeaningsViewByConnectionStatus ();

	SetInitialUpdateFlag (true_d (L"Эта функция отработала"));
}

void CView_Interfaces::FormTask_InterfaceSets ()
{
	CRect rect, item_rect;

	m_tabInterfaces.GetClientRect (rect);
	m_tabInterfaces.GetItemRect (0, item_rect);

	rect.top += item_rect.Height () + SIDE_OFFSET;
	rect.left += SIDE_OFFSET;
	rect.right -= SIDE_OFFSET;
	rect.bottom -= SIDE_OFFSET;

	TCITEM itm = {};
	itm.mask = TCIF_TEXT;
	itm.iImage = -1;
	wchar_t s1[100] = L"Интерфейсы";
	itm.pszText = s1;
	m_tabInterfaces.InsertItem (0, &itm);
	wchar_t s2[100] = L"   USB   ";
	itm.pszText = s2;
	//m_tabInterfaces.InsertItem(1, &itm);

	itm.mask = TCIF_PARAM;
	itm.lParam = (LPARAM)&m_SettTask_Prot_RS;
	m_tabInterfaces.SetItem (0, &itm);
	if (m_SettTask_Prot_RS.Create (m_SettTask_Prot_RS.IDD, &m_tabInterfaces))
	{
		m_SettTask_Prot_RS.SetWindowPos (nullptr, rect.left, rect.top, rect.Width (), rect.Height (), SWP_NOSIZE | SWP_NOZORDER);
		m_SettTask_Prot_RS.MoveWindow (rect, TRUE);
		m_SettTask_Prot_RS.ShowWindow (SW_SHOW);
		m_tabInterfaces.AddPage (&m_SettTask_Prot_RS, CString (s1));
	}

	m_tabInterfaces.SetColours (RGB (100, 100, 255), RGB (0, 0, 0));
	m_tabInterfaces.SetFonts ();

	m_tabInterfaces.SelectItem (L"Интерфейсы");
}

void CView_Interfaces::BeforeDestroyClass ()
{
}

void CView_Interfaces::OnSize_ ()
{
}

void CView_Interfaces::OnDraw (CDC* pDC)
{
	return;
}

void CView_Interfaces::OnBnClickedChkShowPassword ()
{
	ns_Inerf::g_nStateShowPassword = m_chkShowPassword.GetCheck ();
	CheckShowPasswordState ();
}

void CView_Interfaces::CheckShowPasswordState ()
{
	m_bCanSave = false;

	if (ns_Inerf::g_nStateShowPassword)
	{
		m_edtOldPasswordUser.SetPasswordChar (0);
		m_edtNewPasswordUser.SetPasswordChar (0);
		m_edtOldPasswordAdmin.SetPasswordChar (0);
		m_edtNewPasswordAdmin.SetPasswordChar (0);
		m_edtNewPasswordUser2.SetPasswordChar (0);
	}
	else
	{
		m_edtOldPasswordUser.SetPasswordChar ('*');
		m_edtNewPasswordUser.SetPasswordChar ('*');
		m_edtOldPasswordAdmin.SetPasswordChar ('*');
		m_edtNewPasswordAdmin.SetPasswordChar ('*');
		m_edtNewPasswordUser2.SetPasswordChar ('*');
	}

	m_edtOldPasswordUser.Invalidate ();
	m_edtNewPasswordUser.Invalidate ();
	m_edtOldPasswordAdmin.Invalidate ();
	m_edtNewPasswordAdmin.Invalidate ();
	m_edtNewPasswordUser2.Invalidate ();

	m_bCanSave = true;
}

void CView_Interfaces::CheckUserEnable (bool bEnable)
{
	m_edtOldPasswordUser.EnableWindow (bEnable);
	m_edtNewPasswordUser.EnableWindow (bEnable);
}

void CView_Interfaces::CheckAdminEnable (bool bEnable)
{
	m_edtOldPasswordAdmin.EnableWindow (bEnable);

	if (bEnable)
	{
		if (ns_Inerf::g_nNewAdminCheck)
			OnBnClickedRdoNewAdminPassword ();
		else
			OnBnClickedRdoNewUserPassword ();
	}
	else
	{
		m_edtNewPasswordAdmin.EnableWindow (bEnable);
		m_edtNewPasswordUser2.EnableWindow (bEnable);
	}

	m_rdoNewAdminPassword.EnableWindow (bEnable);
	m_rdoNewUserPassword.EnableWindow (bEnable);
}

void CView_Interfaces::OldUserPasswordWasChanged (CString sText)
{
	if (sText.IsEmpty ())
	{
		m_btnUserPasswordError.ShowWindow (SW_HIDE);
		return;
	}

	CString sUserPassword = GetUserPassword ();
	if (sText == sUserPassword)
		m_btnUserPasswordError.ShowWindow (SW_HIDE);
	else
		m_btnUserPasswordError.ShowWindow (SW_SHOW);
}

void CView_Interfaces::OldAdminPasswordWasChanged (CString sText)
{
	if (sText.IsEmpty ())
	{
		m_btnAdminPasswordError.ShowWindow (SW_HIDE);
		return;
	}

	CString sAdminPassword = GetAdminPassword ();
	if (sText == sAdminPassword)
		m_btnAdminPasswordError.ShowWindow (SW_HIDE);
	else
		m_btnAdminPasswordError.ShowWindow (SW_SHOW);
}

void CView_Interfaces::OnBnClickedRdoNewAdminPassword ()
{
	m_rdoNewUserPassword.SetCheck (0);
	m_edtNewPasswordUser2.EnableWindow (FALSE);
	m_edtNewPasswordAdmin.EnableWindow (TRUE);
	ns_Inerf::g_nNewAdminCheck = 1;
}

void CView_Interfaces::OnBnClickedRdoNewUserPassword ()
{
	m_rdoNewAdminPassword.SetCheck (0);
	m_edtNewPasswordAdmin.EnableWindow (FALSE);
	m_edtNewPasswordUser2.EnableWindow (TRUE);
	ns_Inerf::g_nNewAdminCheck = 0;
}

void CView_Interfaces::OnConnectionStateWasChanged ()
{
	CheckMeaningsViewByConnectionStatus ();
}

void CView_Interfaces::CheckMeaningsViewByConnectionStatus ()
{
	vector<int> inactiveTextInColumns;
	if (IsConnection ())
	{		
		CheckUserEnable (!IsConectionAdminAccess ());
		CheckAdminEnable (IsConectionAdminAccess ());
	}
	else // disconnection
	{
		m_btnRecordInterfacesToDevice.EnableWindow (false);
		CheckUserEnable (false);
		CheckAdminEnable (false);
	}
}


void CView_Interfaces::UpdateParamMeaningsByReceivedDeviceData (mdl::teFrameTypes ft)
{
	varMeaning_t m = FindReceivedData (ft);
	if (ft == FRAME_TYPE_DEV_INF_STRUCT) // получили структуру с инфой о счетчике
	{
		m_cfg = CCounterInfo::AsciiToCfg (m);
//		m_SettTask_Prot_RS.SetComData (m_cfg.sIface[0].iFace != IFACE_NO, m_cfg.sIface[1].iFace != IFACE_NO,
									   //m_cfg.sIface[2].iFace != IFACE_NO, m_cfg.sIface[3].iFace != IFACE_NO, m_cfg.aComInfo, m_cfg.gsmSett);
		m_SettTask_Prot_RS.SetComData(m_cfg);

		m_btnRecordInterfacesToDevice.EnableWindow (true);

	}

}
void CView_Interfaces::OnBnClickedBtnRead ()
{
	BeginFormingReadPack (true);
	//	SetWaitCursor_answer (true);
	SetParamTypeForRead (FRAME_TYPE_DEV_INF_STRUCT, true);
	BeginFormingReadPack (false);
}

void CView_Interfaces::OnBnClickedBtnRecordToDevice ()
{
	SetWaitCursor_answer (true);

	BeginFormingSendPack (true_d (L"start"));
	bool enabled[4];
	sCommInfo info[4];
	m_SettTask_Prot_RS.GetComData (enabled[0], enabled[1], enabled[2], enabled[3], info, &m_cfg.gsmSett);
	memcpy (m_cfg.aComInfo, info, sizeof (m_cfg.aComInfo));	
	CString str = CCounterInfo::CfgAsAscii (m_cfg);
	SendDataToDevice (FRAME_TYPE_DEV_INF_STRUCT_NO_JUMPER, str);
	//SendDataToDevice(FRAME_TYPE_COMMAND_RS_PROP_EXECUTE); // Apply new settings
	BeginFormingSendPack (false_d (L"start"));
}

void CView_Interfaces::OnBnClickedBtnRecordPasswToDevice ()
{
	CString sNewPassword;
	teFrameTypes ft = FRAME_TYPE_EMPTY;
	teFrameTypes ftExec = FRAME_TYPE_EMPTY;

	bool bCheckAdmin = IsConectionAdminAccess ();
	if (bCheckAdmin)
	{
		CString sOldPassword;
		m_edtOldPasswordAdmin.GetWindowText (sOldPassword);
		if (sOldPassword.IsEmpty ())
		{
			AfxMessageBox (L"Для пользователя Admin - не задан старый пароль!", MB_ICONERROR);
			return;
		}
		CString sAdminPassword = GetAdminPassword ();
		if (sOldPassword != sAdminPassword)
		{
			AfxMessageBox (L"Для пользователя Admin - не совпадает старый пароль!", MB_ICONERROR);
			return;
		}

		CString sNewPasswordAdmin;
		m_edtNewPasswordAdmin.GetWindowText (sNewPasswordAdmin);
		CString sNewPasswordUser2;
		m_edtNewPasswordUser2.GetWindowText (sNewPasswordUser2);
		if (sNewPasswordAdmin.IsEmpty () && sNewPasswordUser2.IsEmpty ())
		{
			AfxMessageBox (L"Для пользователя Admin - не задан ни один из новых паролей!", MB_ICONERROR);
			return;
		}

		if (ns_Inerf::g_nNewAdminCheck)
		{
			sNewPassword = sNewPasswordAdmin;
			ft = FRAME_TYPE_NEW_ADMIN_PASSW;
			ftExec = FRAME_TYPE_COMMAND_ADMIN_PASSW_EXECUTE;
		}
		else
		{
			sNewPassword = sNewPasswordUser2;
			ft = FRAME_TYPE_NEW_USER_PASSW;
			ftExec = FRAME_TYPE_COMMAND_USER_PASSW_EXECUTE;
		}
	}
	else
	{
		CString sOldPassword;
		m_edtOldPasswordUser.GetWindowText (sOldPassword);
		if (sOldPassword.IsEmpty ())
		{
			AfxMessageBox (L"Для пользователя User - не задан старый пароль!", MB_ICONERROR);
			return;
		}
		CString sUserPassword = GetUserPassword ();
		if (sOldPassword != sUserPassword)
		{
			AfxMessageBox (L"Для пользователя User - не совпадает старый пароль!", MB_ICONERROR);
			return;
		}

		CString sNewPasswordUser;
		m_edtNewPasswordUser.GetWindowText (sNewPasswordUser);
		if (sNewPasswordUser.IsEmpty ())
		{
			AfxMessageBox (L"Для пользователя User - не задан новый пароль!", MB_ICONERROR);
			return;
		}

		ft = FRAME_TYPE_NEW_USER_PASSW;
		sNewPassword = sNewPasswordUser;
		ftExec = FRAME_TYPE_COMMAND_USER_PASSW_EXECUTE;
	}

	SetWaitCursor_answer (true);

	BeginFormingSendPack (true_d (L"start"));

	SendDataToDevice (ft, sNewPassword);
	SendDataToDevice (ftExec); // Apply new settings

	BeginFormingSendPack (false_d (L"start"));
}

void CView_Interfaces::UpdateInlayByReceivedConfirmation (teFrameTypes ft, bool bResult)
{
	if (ft == FRAME_TYPE_DEV_INF_STRUCT || ft == FRAME_TYPE_DEV_INF_STRUCT_NO_JUMPER)
	{
		if (bResult)
		{
			AfxMessageBox (L"Конфигурация счетчика записана, связь со счетчиком прервана", MB_ICONINFORMATION);
			::PostMessage (GetGlobalParent (), ID_CAPTION_BTN_DISCONNECT, 0, 0);
		}
		else // err
		{
			AfxMessageBox (L"Не могу записать конфигурацию, проверьте джампер", MB_ICONSTOP);
			SetWaitCursor_answer (false);
		}
		return;
	}

	
	if (ft == FRAME_TYPE_NEW_ADMIN_PASSW
		|| ft == FRAME_TYPE_NEW_USER_PASSW
		|| ft == FRAME_TYPE_COMMAND_USER_PASSW_EXECUTE
		|| ft == FRAME_TYPE_COMMAND_ADMIN_PASSW_EXECUTE)
	{
		if (!bResult)
		{
			SetWaitCursor_answer (false);
			AfxMessageBox (L"Ошибка записи в счетчик нового пароля!\nЗапись данных в счетчик прервана.", MB_ICONERROR);
			return;
		}

		if (ft == FRAME_TYPE_COMMAND_USER_PASSW_EXECUTE)
		{
			SetWaitCursor_answer (false);
			::PostMessage (GetGlobalParent (), ID_CAPTION_BTN_DISCONNECT, 0, 0);

			AfxMessageBox (L"Новый пароль для пользователя User УСПЕШНО записан в счетчик!\n\nСвязь с счетчиком будет разорвана...", MB_ICONINFORMATION);
			return;
		}

		if (ft == FRAME_TYPE_COMMAND_ADMIN_PASSW_EXECUTE)
		{
			SetWaitCursor_answer (false);
			::PostMessage (GetGlobalParent (), ID_CAPTION_BTN_DISCONNECT, 0, 0);

			AfxMessageBox (L"Новый пароль для пользователя Admin УСПЕШНО записан в счетчик!\n\nСвязь с счетчиком будет разорвана...", MB_ICONINFORMATION);
			return;
		}
	}
}


