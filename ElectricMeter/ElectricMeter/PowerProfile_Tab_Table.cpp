#include "StdAfx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Defines.h"
#include "PowerProfileDataRow.h"
#include "View_PowerProfile.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Tab_Table
{
  #define COUNT_OF_POWPROF_DAYS 128
  #define COUNT_OF_POWPROF_MAX ((24*2)*COUNT_OF_POWPROF_DAYS)

  struct COLUMNS
  {
    int nN = -1;
    int nA = -1;
    int nP = -1;
    int nR_plus = -1;
    int nQ_plus = -1;
    int nR_minus = -1;
    int nQ_minus = -1;
    int nTimePeriod = -1;
    int nDate = -1;
  }
  g_columns;

  struct ROWS
  {
    vector<int> in_List;
    int nCountReceivedItems = 0; // ������� ���� �������� �� ��������
    int nCountReceivedItemsDid = 0; // ������� ��������
  }
  g_rows;

  int g_nComnSignificantCell = -1;

  bool g_bCurViewIsEnergy = true; // 1 - energy, 0 - power

  vector<POWPROF_DATA_ROW> g_data;
  bool g_bIsNewData = false;

} // ns_Tab_Table

void CView_PowerProfile::CreateTableTab()
{
  m_plstTable->ShowHorizScroll(false);
  m_plstTable->EnableInterlacedColorScheme(true);

  m_plstTableEx = new �Ai_ListCtrlEx();
  m_plstTableEx->SetList(m_plstTable, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstTableEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  Set�Ai_ListCtrlEx(m_plstTableEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_plstTable->SetExtendedStyle(m_plstTable->GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_plstTableEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  m_plstTableEx->SetColumnEx(L""); // 0 column
  ns_Tab_Table::g_columns.nN = m_plstTableEx->SetColumnEx(L"�", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nN, CS_WIDTH_FIX, 40);
  ns_Tab_Table::g_columns.nA = m_plstTableEx->SetColumnEx(L"�\n���*�", LVCFMT_CENTER);
  int nCmnColWidth = 70;
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nA, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nP = m_plstTableEx->SetColumnEx(L"�\n���", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nP, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nR_plus = m_plstTableEx->SetColumnEx(L"R+\n����*�", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nR_plus, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nQ_plus = m_plstTableEx->SetColumnEx(L"Q+\n����", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nQ_plus, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nR_minus = m_plstTableEx->SetColumnEx(L"R-\n����*�", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nR_minus, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nQ_minus = m_plstTableEx->SetColumnEx(L"Q-\n����", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nQ_minus, CS_WIDTH_FIX, nCmnColWidth);
  ns_Tab_Table::g_columns.nTimePeriod = m_plstTableEx->SetColumnEx(L"��������� ��������", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nTimePeriod, CS_WIDTH_FIX, 150);
  ns_Tab_Table::g_columns.nDate = m_plstTableEx->SetColumnEx(L"����", LVCFMT_CENTER);
  m_plstTableEx->SetColumnWidthStyle(ns_Tab_Table::g_columns.nDate, CS_WIDTH_FIX, 80);

  m_plstTable->ShowColumn(0, false);
  m_plstTableEx->MatchColumns();

  int nNodeIdx = m_plstTableEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  ns_Tab_Table::g_nComnSignificantCell = ns_Tab_Table::g_columns.nN;
  m_plstTableEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_Tab_Table::g_nComnSignificantCell);

  vector<CString> textOfCells;
  textOfCells.push_back(L""); // 0
  textOfCells.push_back(IDS_EMPTY_CELL_TEXT); // order
  textOfCells.push_back(L""); // A
  textOfCells.push_back(L""); // P
  textOfCells.push_back(L""); // R+
  textOfCells.push_back(L""); // Q+
  textOfCells.push_back(L""); // R-
  textOfCells.push_back(L""); // Q-
  textOfCells.push_back(L""); // period
  textOfCells.push_back(L""); // date

  // ��� ������ ������
  int nRow_inList = m_plstTableEx->SetChild(nNodeIdx, &textOfCells);
  // ��. �������� "Descr. 1" � ��������� AppDescription.txt
  m_plstTableEx->SetCellHideText(nRow_inList, ns_Tab_Table::g_nComnSignificantCell);
  // ... ��� ��������� �����
  m_plstTableEx->SetEmptyChild(COUNT_OF_POWPROF_MAX - 1);

  m_plstTableEx->UpdateView();
  m_plstTable->CtrlWasCreated();
}

void CView_PowerProfile::FillIn_InTableTbl()
{
  for (size_t j = 0; j < ns_Tab_Table::g_data.size(); ++j)
  {
    int nRow_inList = j + 1;

    CString sN;
    sN.Format(L"%d", nRow_inList);
    
    if (j == 0)
    {
      m_plstTableEx->SetCellShowText(nRow_inList, ns_Tab_Table::g_nComnSignificantCell);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_nComnSignificantCell, sN);
    }
    else
    {
      int nNodeRow_inView = 0;
      m_plstTableEx->InsertTextToChild(nNodeRow_inView, ns_Tab_Table::g_nComnSignificantCell, sN);
    }

    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nA, ns_Tab_Table::g_data[j].sA);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nP, ns_Tab_Table::g_data[j].sP);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nR_plus, ns_Tab_Table::g_data[j].sR_plus);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nQ_plus, ns_Tab_Table::g_data[j].sQ_plus);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nR_minus, ns_Tab_Table::g_data[j].sR_minus);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nQ_minus, ns_Tab_Table::g_data[j].sQ_minus);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nTimePeriod, ns_Tab_Table::g_data[j].sTimePeriod);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nDate, ns_Tab_Table::g_data[j].sDate);
  }

  m_plstTableEx->UpdateView();
}

void CView_PowerProfile::CheckMeaningsViewByConnectionStatus_InTableTbl()
{
  if (!IsConnection())
    ClearAllRows_InTableTbl();
}

void CView_PowerProfile::ClearAllRows_InTableTbl()
{
  if (m_plstTableEx->GetSetedCountOfChild(0) > 0)
  {
    auto lmbClearCells = [=](int nRow_inList)->void
    {
      CString s = L"";
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nN, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nA, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nP, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nR_plus, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nQ_plus, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nR_minus, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nQ_minus, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nTimePeriod, s);
      m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nDate, s);
    };

    // ��� ������ ������
    int nRow_inList = 1;
    lmbClearCells(nRow_inList);
    m_plstTableEx->SetCellText(nRow_inList, ns_Tab_Table::g_columns.nN, IDS_EMPTY_CELL_TEXT);
    m_plstTableEx->SetCellHideText(nRow_inList, ns_Tab_Table::g_nComnSignificantCell);

    for (int j = 1; j < COUNT_OF_POWPROF_MAX; ++j)
    {
      nRow_inList = j + 1;
      if (!m_plstTableEx->IsRowVisible(nRow_inList))
        break;

      int nRow_inView = j;
      m_plstTableEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_Tab_Table::g_nComnSignificantCell, -1);
      lmbClearCells(nRow_inList);
    }
  }

  ns_Tab_Table::g_data.clear();
  ns_Tab_Table::g_rows.nCountReceivedItems = 0;
  ns_Tab_Table::g_rows.nCountReceivedItemsDid = 0;
  ns_Tab_Table::g_bIsNewData = false;

  m_plstTableEx->UpdateView();
}

void CView_PowerProfile::UpdMeaningsByRecvdDeviceData_InTableTbl(teFrameTypes& ft)
{
  if (ft == FRAME_TYPE_INTERVAL_ENERGY_CNT)
  {
    varMeaning_t m = FindReceivedData(ft);
    if (m != L"?")
    {
      int nNewRowCount = CAi_Str::ToInt(m);

      ns_Tab_Table::g_rows.nCountReceivedItems = min(nNewRowCount, COUNT_OF_POWPROF_MAX); // ����� �� ������� �� ��������
      ns_Tab_Table::g_rows.nCountReceivedItems = max(ns_Tab_Table::g_rows.nCountReceivedItems, 0); // ����� �� ������� �� �������
    }
    return;
  }

  if (ft >= FRAME_TYPE_INTERVAL_ENERGY_001 && ft <= FRAME_TYPE_INTERVAL_ENERGY_171)
  {
    bool bAllOk = false;

    unsigned int nAddress = 0;
    if (GetDataFrameAddress(ft, nAddress))
    {
      /*
        5801(01-01-2001;01:00;A+INT1;R+INT1;R-INT1;
                01-01-2001;01:30;A+INT2;R+INT2;R-INT2;
                01-01-2001;02:00;A+INT3;R+INT3;R-INT3)
        <CR><LF>
        5802(01-01-2001;02:30;A+INT4;R+INT4;R-INT4;
                01-01-2001;03:00;A+INT5;R+INT5;R-INT5;
                01-01-2001;03:30;A+INT6;R+INT6;R-INT6)
         <CR><LF>
        -//-
        580C(01-01-2001;17:30;A+INT34;R+INT34;R-INT34;
                01-01-2001;18:00;A+INT35;R+INT35;R-INT35;
                01-01-2001;18:30;A+INT36;R+INT36;R-INT36)
      */

      int nExtendedAddresses = GetExtendedAddresses(ft) + 1; // 1 - itself
      for (int e = 0; e < nExtendedAddresses; ++e)
      {
        varMeaning_t mData = FindReceivedData(nAddress + e);
        if (mData == L"?")
        {
          if (e > 0)
            bAllOk = true;
          break;
        }
        mData.TrimRight(L";");
        vector<CString> data;
        if (CAi_Str::CutString(mData, L";", &data))
        {
          bAllOk = true;
          int nHalfHourCount = 0;
          switch (data.size())
          {
            case 5: nHalfHourCount = 1; break;
            case 10: nHalfHourCount = 2; break;
            case 15: nHalfHourCount = 3; break;
            default: ASSERT(false); bAllOk = false; break;
          }

          for (int k = 0; k < nHalfHourCount; ++k)
          {
            auto lmbPower = [=](CString* pS)->CString
            {
              double fM = CAi_Str::ToFloat(*pS) * 2;
              int nPrecision = CAi_Str::GetPrecision(*pS);
              return CAi_Str::ToString((float)fM, nPrecision, CAi_Str::fsrFull);
            };

            int nIdx = k * 5;
            POWPROF_DATA_ROW ppD;
            ppD.sDate = data[nIdx + 0];
            ppD.sTimePeriod = data[nIdx + 1];
            ppD.sA = data[nIdx + 2];
            ppD.sP = lmbPower(&data[nIdx + 2]);
            ppD.sR_plus = data[nIdx + 3];
            ppD.sQ_plus = lmbPower(&data[nIdx + 3]);
            ppD.sR_minus = data[nIdx + 4];
            ppD.sQ_minus = lmbPower(&data[nIdx + 4]);

            ns_Tab_Table::g_data.push_back(ppD);
          }
        }
      }
    }

    if (bAllOk)
      ++ns_Tab_Table::g_rows.nCountReceivedItemsDid;
  }

  if (ns_Tab_Table::g_rows.nCountReceivedItems == ns_Tab_Table::g_rows.nCountReceivedItemsDid) // ��� ��������, �������� �������
    ns_Tab_Table::g_bIsNewData = true;
}

void CView_PowerProfile::ClearAllReqDataInDB_ForTableTbl()
{
  FormListForClearReceivedData(FRAME_TYPE_INTERVAL_ENERGY_CNT);
  for (int i = (int)FRAME_TYPE_INTERVAL_ENERGY_001;
       i < (int)FRAME_TYPE_INTERVAL_ENERGY_171; ++i)
       FormListForClearReceivedData((teFrameTypes)i);

  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_PowerProfile::ReceiveData_ForTableTbl()
{
  SetParamTypeForRead(FRAME_TYPE_INTERVAL_ENERGY_CNT, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
}

void CView_PowerProfile::FormAllReqsOfItems_ForTableTbl()
{
  teFrameTypes reqFirstItem = FRAME_TYPE_INTERVAL_ENERGY_001;
  int nNext = static_cast<int>(reqFirstItem);
  for (int i = 0; i < ns_Tab_Table::g_rows.nCountReceivedItems; ++i)
  {
    reqFirstItem = static_cast<teFrameTypes>(nNext);
    SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    nNext += 1;
  }
}

bool CView_PowerProfile::IsReqItems_ForTableTbl()
{
  return (ns_Tab_Table::g_rows.nCountReceivedItems > 0);
}

bool CView_PowerProfile::IsNewData_InTableTbl()
{
  bool bIs = ns_Tab_Table::g_bIsNewData;
  ns_Tab_Table::g_bIsNewData = false;
  return bIs;
}

void CView_PowerProfile::GetTimeIntervals_FromTableTbl(CString& sBegin, CString& sEnd)
{
  // ������� �������� ��������� ������ - ������, �����
  if (!ns_Tab_Table::g_data.empty())
  {
    sBegin = ns_Tab_Table::g_data[0].sTimePeriod + L"; " + ns_Tab_Table::g_data[0].sDate;
    size_t iEnd = ns_Tab_Table::g_data.size() - 1;
    sEnd = ns_Tab_Table::g_data[iEnd].sTimePeriod + L"; " + ns_Tab_Table::g_data[iEnd].sDate;
  }
}

vector<POWPROF_DATA_ROW>* CView_PowerProfile::GetReceivedDataList_FromTableTbl()
{
  return &ns_Tab_Table::g_data;
}

void CView_PowerProfile::CheckView_InTableTbl()
{
  bool bRefill = false;

  if (ns_Tab_Table::g_bCurViewIsEnergy)
  {
    if (m_currPPPV_Energy != ecppvEnergy)
    {
      bRefill = true;
      ns_Tab_Table::g_bCurViewIsEnergy = false;
    }
  }
  else
  {
    if (m_currPPPV_Energy != ecppvPower)
    {
      bRefill = true;
      ns_Tab_Table::g_bCurViewIsEnergy = true;
    }
  }
}