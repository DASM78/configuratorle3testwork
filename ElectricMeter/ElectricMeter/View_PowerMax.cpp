#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "XmlAid.h"
#include "Ai_File.h"
#include "Ai_Str.h"
#include "..\PrivateLibs\resource.h"
#include "View_PowerMax.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_PowerMax, CView)

namespace ns_PowMax
{
  struct COLUMNS
  {
    int nDate = -1;
    int nA_morning = -1;
    int nA_evening = -1;
    int nR_plus_morning = -1;
    int nR_plus_evening = -1;
    int nR_minus_morning = -1;
    int nR_minus_evening = -1;
  }
  g_cols;

  vector<int> g_rows;
  int g_nRow_from = 0;
  int g_nRowCount_from = 0;

  struct DATA
  {
    CString sDate;
    CString sA_morning;
    CString sA_evening;
    CString sR_plus_morning;
    CString sR_plus_evening;
    CString sR_minus_morning;
    CString sR_minus_evening;
  };

  vector <DATA> g_data;

} // namespace ns_PowMax
using namespace ns_PowMax;

CView_PowerMax::CView_PowerMax()
  : CCommonViewForm(CView_PowerMax::IDD, L"CView_PowerMax")
  , m_lstPowerMax(&m_plstPowerMaxEx, dynamic_cast<CCommonViewDealer*>(this))
{
  m_lstPowerMax.UseMultiRowColumns();
}

CView_PowerMax::~CView_PowerMax()
{
  if (m_plstPowerMaxEx)
  {
    m_plstPowerMaxEx->Destroy();
    delete m_plstPowerMaxEx;
  }
}

BEGIN_MESSAGE_MAP(CView_PowerMax, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_LOAD_POW_FROM_DEVICE, &CView_PowerMax::OnBnClickedBtnLoadPowFromDevice)
  ON_BN_CLICKED(IDC_BTN_CLEAR_POW_IN_DEVICE, &CView_PowerMax::OnBnClickedBtnClearPowInDevice)
END_MESSAGE_MAP()

void CView_PowerMax::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_LST_DATA_POWER, m_lstPowerMax);
  DDX_Control(pDX, IDC_BTN_LOAD_POW_FROM_DEVICE, m_btnLoad);
  DDX_Control(pDX, IDC_BTN_CLEAR_POW_IN_DEVICE, m_btnClear);
}

void CView_PowerMax::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  m_lstPowerMax.ShowHorizScroll(true);

  m_plstPowerMaxEx = new �Ai_ListCtrlEx();
  m_plstPowerMaxEx->SetList(&m_lstPowerMax, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstPowerMaxEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  m_lstPowerMax.MyPreSubclassWindow();

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstPowerMax.SetExtendedStyle(m_lstPowerMax.GetExtendedStyle() & ~LVS_EX_GRIDLINES);
  m_lstPowerMax.EnableInterlacedColorScheme(true);

  m_lstPowerMax.m_bShowHeaderContextMenu = false;
  m_lstPowerMax.m_bCanHeaderDrag = false;

  m_plstPowerMaxEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  m_plstPowerMaxEx->SetColumnEx(L"", LVCFMT_LEFT);
  ns_PowMax::g_cols.nDate = m_plstPowerMaxEx->SetColumnEx(L"����", LVCFMT_LEFT);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nDate, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nA_morning = m_plstPowerMaxEx->SetColumnEx(L"P ���. \n���", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nA_morning, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nA_evening = m_plstPowerMaxEx->SetColumnEx(L"P ���. \n���", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nA_evening, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nR_plus_morning = m_plstPowerMaxEx->SetColumnEx(L"Q+ ���. \n����", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nR_plus_morning, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nR_plus_evening = m_plstPowerMaxEx->SetColumnEx(L"Q+ ���. \n����", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nR_plus_evening, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nR_minus_morning = m_plstPowerMaxEx->SetColumnEx(L"Q- ���. \n����", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nR_minus_morning, CS_WIDTH_FIX, 100);
  ns_PowMax::g_cols.nR_minus_evening = m_plstPowerMaxEx->SetColumnEx(L"Q- ���. \n����", LVCFMT_CENTER);
  m_plstPowerMaxEx->SetColumnWidthStyle(ns_PowMax::g_cols.nR_minus_evening, CS_WIDTH_FIX, 100);

  m_plstPowerMaxEx->MatchColumns();
  m_lstPowerMax.ShowColumn(0, false);

  int nNodeIdx = m_plstPowerMaxEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  for (int j = 0; j < 13; ++j)
  {
    vector<CString> textOfCells(8);
    ns_PowMax::g_rows.push_back(m_plstPowerMaxEx->SetChild(nNodeIdx, &textOfCells));
  }

  m_plstPowerMaxEx->UpdateView();
  m_lstPowerMax.CtrlWasCreated();

  FillTable();
  CheckMeaningsViewByConnectionStatus();

  WatchForCtrState(&m_btnLoad, rightCommon);
  WatchForCtrState(&m_btnClear, rightAdmin);

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_PowerMax::BeforeDestroyClass()
{
}

void CView_PowerMax::OnSize_()
{
}

void CView_PowerMax::OnDraw(CDC* pDC)
{
}

bool CView_PowerMax::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_PowerMax::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_PowerMax::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_PowerMax::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_PowerMax::FormDB()
{
  for (int j = 0; j < 13; ++j) // rows
  {
    ns_PowMax::g_data.push_back(ns_PowMax::DATA());
    if (j == 0)
      ns_PowMax::g_data.at(j).sDate = L"������� �����";
  }
}

void CView_PowerMax::ClearTable()
{
  ns_PowMax::g_data.clear();
  FillTable();
}

void CView_PowerMax::FillTable()
{
  if (ns_PowMax::g_data.empty())
    FormDB();

  for (int j = 0; j < 13; ++j) // rows
  {
    ns_PowMax::DATA& data = ns_PowMax::g_data.at(j);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nDate, data.sDate);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nA_morning, data.sA_morning);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nA_evening, data.sA_evening);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nR_plus_morning, data.sR_plus_morning);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nR_plus_evening, data.sR_plus_evening);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nR_minus_morning, data.sR_minus_morning);
    m_plstPowerMaxEx->SetCellText(ns_PowMax::g_rows[j], ns_PowMax::g_cols.nR_minus_evening, data.sR_minus_evening);
  }
  m_plstPowerMaxEx->UpdateView();
}

void CView_PowerMax::ParseReceivedData(teFrameTypes ft, CString& s)
{
  // 01-01-2001;P_MORN;Q+MORN;Q-MORN;P_EVNG;Q+EVNG;Q-EVNG;

  int ftCurr = static_cast<int>(ft);
  int ft1 = static_cast<int>(FRAME_TYPE_MAXIMUM_POWER_01);
  int nIdx = (ftCurr - ft1) + 1; // + 1 �.�. ������ ��� ������� ���������;
  
  ns_PowMax::DATA& data = ns_PowMax::g_data.at(nIdx);

  CString sD = L"";
  data.sDate = sD;
  data.sA_morning = sD;
  data.sA_evening = sD;
  data.sR_plus_morning = sD;
  data.sR_plus_evening = sD;
  data.sR_minus_morning = sD;
  data.sR_minus_evening = sD;

  s.TrimRight(L";");
  vector<CString> strs;
  if (CAi_Str::CutString(s, L";", &strs)
      && strs.size() == 7)
  {
    data.sDate = strs[0];
    data.sA_morning = strs[1];
    data.sR_plus_morning = strs[2];
    data.sR_minus_morning = strs[3];
    data.sA_evening = strs[4];
    data.sR_plus_evening = strs[5];
    data.sR_minus_evening = strs[6];
  }
}

void CView_PowerMax::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_PowerMax::CheckMeaningsViewByConnectionStatus()
{
  ClearTable();
  /*vector<int> inactiveTextInColumns;
  if (!IsConnection())
  {
    for (int i = 0; i < 6; ++i)
      inactiveTextInColumns.push_back(ns_PowMax::g_cols.nDate + i);
  }
  m_lstPowerMax.SetInactiveTextInColumns(&inactiveTextInColumns);*/
  m_plstPowerMaxEx->UpdateView();
}

void CView_PowerMax::OnBnClickedBtnLoadPowFromDevice()
{
  FormListForClearReceivedData(FRAME_TYPE_POWER_P_MAX_MORNING);
  FormListForClearReceivedData(FRAME_TYPE_POWER_P_MAX_EVENING);
  FormListForClearReceivedData(FRAME_TYPE_POWER_QP_MAX_MORNING);
  FormListForClearReceivedData(FRAME_TYPE_POWER_QP_MAX_EVENING);
  FormListForClearReceivedData(FRAME_TYPE_POWER_QM_MAX_MORNING);
  FormListForClearReceivedData(FRAME_TYPE_POWER_QM_MAX_EVENING);
  FormListForClearReceivedData(FRAME_TYPE_MAXIMUM_POWER_CNT);

  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_PowerMax::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);

  ClearTable();
  g_nRow_from = 0;
  g_nRowCount_from = 0;

  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);
  m_bShowLoadingSuccessResultMsg = false;
  bool bOnceCmd = true;

  SetParamTypeForRead(FRAME_TYPE_POWER_P_MAX_MORNING, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_POWER_P_MAX_EVENING, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_POWER_QP_MAX_MORNING, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_POWER_QP_MAX_EVENING, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_POWER_QM_MAX_MORNING, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_POWER_QM_MAX_EVENING, bOnceCmd);

  SetParamTypeForRead(FRAME_TYPE_MAXIMUM_POWER_CNT, bOnceCmd);

  BeginFormingReadPack(false);
}

void CView_PowerMax::OnBnClickedBtnClearPowInDevice()
{
  if (AfxMessageBox(L"�������� ������ �� ���������� ��������� � �������?", MB_ICONQUESTION | MB_OKCANCEL) != IDOK)
    return;

  SetWaitCursor_oneWay(true);

  BeginFormingSendPack(true_d(L"start"));
  SendDataToDevice(FRAME_TYPE_COMMAND_MAX_POWERS_RESET);
  BeginFormingSendPack(false_d(L"start"));
}

void CView_PowerMax::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);

  if (ft == FRAME_TYPE_COMMAND_MAX_POWERS_RESET)
  {
    SetWaitCursor_answer(false);

    if (m == L"\xF")
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      AfxMessageBox(L"������ ������� ������ �� ���������� ��������� � ��������!", MB_ICONERROR);
      return;
    }

    ClearTable();
    AfxMessageBox(L"������ �� ���������� ��������� ������� � �������!");
    return;
  }

  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� ���������� ���������!", MB_ICONERROR);
    return;
  }

  ASSERT(!ns_PowMax::g_data.empty());

  if (ft >= FRAME_TYPE_POWER_P_MAX_MORNING
      && ft <= FRAME_TYPE_POWER_QM_MAX_EVENING)
  {
    if (ft == FRAME_TYPE_POWER_P_MAX_MORNING)
      ns_PowMax::g_data.at(0).sA_morning = m;
    if (ft == FRAME_TYPE_POWER_P_MAX_EVENING)
      ns_PowMax::g_data.at(0).sA_evening = m;
    if (ft == FRAME_TYPE_POWER_QP_MAX_MORNING)
      ns_PowMax::g_data.at(0).sR_plus_morning = m;
    if (ft == FRAME_TYPE_POWER_QP_MAX_EVENING)
      ns_PowMax::g_data.at(0).sR_plus_evening = m;
    if (ft == FRAME_TYPE_POWER_QM_MAX_MORNING)
      ns_PowMax::g_data.at(0).sR_minus_morning = m;
    if (ft == FRAME_TYPE_POWER_QM_MAX_EVENING)
      ns_PowMax::g_data.at(0).sR_minus_evening = m;

    FillTable();
  }

  if (ft == FRAME_TYPE_MAXIMUM_POWER_CNT)
  {
    varMeaning_t m = FindReceivedData(ft);
    int nCount = CAi_Str::ToInt(m);
    ns_PowMax::g_nRow_from = max(nCount, 0); // ����� �� ������� �� �������
    ns_PowMax::g_nRow_from = min(ns_PowMax::g_nRow_from, 12); // ����� �� ������� �� ��������
    if (ns_PowMax::g_nRow_from == 0)
    {
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ �� ���������� ��������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
      return;
    }

    teFrameTypes reqFirstItem = FRAME_TYPE_MAXIMUM_POWER_01;
    int nNext = static_cast<int>(reqFirstItem);
    for (int i = 0; i < ns_PowMax::g_nRow_from; ++i)
    {
      reqFirstItem = static_cast<teFrameTypes>(nNext);
      SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
      nNext += 1;
    }
    return;
  }

  if (ft >= FRAME_TYPE_MAXIMUM_POWER_01
      && ft <= FRAME_TYPE_MAXIMUM_POWER_12)
  {
    ParseReceivedData(ft, m);
    ++g_nRowCount_from;
    if (g_nRowCount_from == g_nRow_from)
    {
      SetWaitCursor_answer(false);
      FillTable();
      AfxMessageBox(L"������ �� ���������� ��������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }
  }
}