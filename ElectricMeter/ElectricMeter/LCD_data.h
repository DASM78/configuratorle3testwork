#pragma once

#include "XmlAid.h"
#include <vector>

class CLCD_data
{
public:
  CLCD_data(CParamsTreeNode* pInlaysNode);
  ~CLCD_data();

private:
  void FormData();

  CParamsTreeNode* m_pInlaysNode;
  CParamsTreeNode* m_pLcdNode = nullptr;
  void TakeValueFromXmlTree(CParamsTreeNode* pMainNode);
  void PutValueToXmlTree();
public:
  bool OpenLcdToFile();
  void SaveLcdToFile();

  void ParseReceivedData(size_t nForPage, CString& sData);
  int GetSignificantPageCount();
  void FormData(size_t nForPage, CString& sData);

private:
  std::vector<CString> CheckFrames(CString& sGroupXmlSign, std::vector<CString>* pFrames);

public:
  std::vector<CString> GetPages();
  std::vector<CString> GetSets(CString& sForPage, bool bTakeSign = false);
  std::vector<CString> GetFrames(CString& sForPage);
  int GetMaxCount(CString& sForPage);

private:
  CString GetPageXmlSign(size_t orderIdx);
  CString GetChildXmlSign(CString& sFrame);
  CString GetChildEnumId(CString& sFrame);
  CString GetChildFrame(CString& sSign);
  CString GetChildFrame2(CString& sEnumId);

public:
  void UpdateFrames(CString& sForPage, std::vector<CString>* pFrames);

  void IncludeAllForPage(CString& sForPage);
  void FreeAllForPage(CString& sForPage);
};