#include "stdafx.h"
#include "Resource.h"
#include "MainFrm.h"
#include "View_Connection.h"
#include "View_Energy.h"
#include "View_NetProperties.h"
#include "View_EventJournal.h"
#include "View_Monitoring.h"
#include "View_PowerProfile.h"
#include "View_PowerMax.h"
#include "View_SheduleControlMaxes.h"
#include "View_InfoAboutDevice.h"
#include "View_UnderConstruction.h"
#include "View_EventJournal.h"
#include "View_SwitchOffRele.h"
#include "View_LCD.h"
#include "View_Time.h"
#include "View_TariffShedules.h"
#include "View_ReloadCurrent.h"
#include "View_Interfaces.h"
#include "View_Calibration.h"
#include "Production.h"
#include "CommonViewForm.h"
#include "CNetParam.h"

using namespace mdl;
using namespace aux;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CMainFrame::SetActiveView_(CView* p)
{
	ASSERT(p);
	m_pCurrActiveView = p;
}

CView* CMainFrame::GetActiveView_()
{
	if (!m_pCurrActiveView) // ����� ���������
		m_pCurrActiveView = GetActiveView(); // ������������� �ElectricMeterView
	return m_pCurrActiveView;
}

int CMainFrame::GetCurrentViewID()
{
	CView* pActiveView = GetActiveView_();
	CCommonViewForm* pVF = reinterpret_cast<CCommonViewForm*>(pActiveView);
	if (pActiveView && pVF)
		return pVF->GetID();
	return 0;
}

bool CMainFrame::IsCurrentView()
{
	int nCurrViewID = GetCurrentViewID();
	if (nCurrViewID <= 0)
		return false;
	return true;
}



void CMainFrame::SelectViewByTabItemID(int nTabItemID)
{
	critical_section cs(m_mutexSwitchView);
	CWaitCursor waitCur;

	int nCurrentView = GetCurrentViewID();
	if (nTabItemID == nCurrentView) // ���� ��� ����!!!
	{
		CView* pActiveView = GetActiveView_();
		if (pActiveView)
			pActiveView->SendMessage(WM_SIZE);
		return;
	}

	CView* pOldActiveView = GetActiveView_();
	if (pOldActiveView)
	{
		::SetWindowLong(pOldActiveView->m_hWnd, GWL_ID, nCurrentView);
		pOldActiveView->ShowWindow(SW_HIDE);
	}

	CRuntimeClass* pNewViewClass = nullptr;

	switch (nTabItemID)
	{
	case IDD_CONNECTION_SETTINGS:
		pNewViewClass = RUNTIME_CLASS(CView_Connection);
		break;

	case IDD_CONNECTION_MONITORING:
		pNewViewClass = RUNTIME_CLASS(CView_Monitoring);
		break;

	case IDD_DATA_INFO_ABOUT_DEVICE:
		pNewViewClass = RUNTIME_CLASS(CView_InfoAboutDevice);
		break;

	case IDD_DATA_ENERGY:
		pNewViewClass = RUNTIME_CLASS(CView_Energy);
		break;

	case IDD_DATA_NET_PROPERTIES:
		pNewViewClass = RUNTIME_CLASS(CView_NetProperties);
		break;

	case IDD_NET_CONTROL_SETTINGS:
		pNewViewClass = RUNTIME_CLASS(CNetParam);
		break;
	case IDD_DATA_EVENT_JOURNAL:
		pNewViewClass = RUNTIME_CLASS(CView_EventJournal);
		break;


	case IDD_DATA_POWER_MAX:
		pNewViewClass = RUNTIME_CLASS(CView_PowerMax);
		break;

	case IDD_DATA_POWER_PROFILE:
		pNewViewClass = RUNTIME_CLASS(CView_PowerProfile);
		break;

	case IDD_SETTINGS_LCD:
		pNewViewClass = RUNTIME_CLASS(CView_LCD);
		break;

	case IDD_SETTINGS_TIME:
		pNewViewClass = RUNTIME_CLASS(CView_Time);
		break;

	case IDD_SETTINGS_TARIFF_SHEDULES:
		pNewViewClass = RUNTIME_CLASS(CView_TariffShedules);
		break;

	case IDD_SETTINGS_SWITCH_OFF_RELE:
		pNewViewClass = RUNTIME_CLASS(CView_SwitchOffRele);
		break;

	case IDD_SETTINGS_SHEDULE_CONTROL_MAXES:
		pNewViewClass = RUNTIME_CLASS(CView_SheduleControlMaxes);
		break;

	case IDD_SETTINGS_INTERFACES:
		pNewViewClass = RUNTIME_CLASS(CView_Interfaces);
		break;

	case IDD_SETTINGS_CALIBRATION:
		pNewViewClass = RUNTIME_CLASS(CView_Calibration);
		break;
	case IDD_PRODUCTION:
		pNewViewClass = RUNTIME_CLASS(CProduction);
		break;
	case IDD_RELOAD_CURRENT_VIEW:
		pNewViewClass = RUNTIME_CLASS(CView_ReloadCurrent);
		break;
	default:
		pNewViewClass = RUNTIME_CLASS(CView_UnderConstruction);
		break;
	}
	// ���� ��������� ��������� �������, � ������ ������, ������������� ������. ��� ���� �����, �� ����� ����� � ��� ����.
	//  ���� ������ ��������

	m_bTimerDisabled = true;
	// ����� ������� ����� ���
	CCreateContext context;
	context.m_pNewViewClass = pNewViewClass;
	context.m_pCurrentDoc = nullptr;
	CView* pNewView = STATIC_DOWNCAST(CView, CreateView(&context));

	SetActiveView_(pNewView);

	// ������������� ������ ����

	ViewInitialisation(pNewView);
	SetActiveView(pNewView);
	// ������� ������ ���

	if (pOldActiveView)
	{
		FreeMonitoringReq();

		pOldActiveView->ShowWindow(SW_HIDE);
		CCommonViewDealer* pOldViewDealer = dynamic_cast<CCommonViewDealer*>(pOldActiveView);
		if (pOldViewDealer)
			pOldViewDealer->BeforeDestroy();
		CCommonViewForm* pOldView = dynamic_cast<CCommonViewForm*>(pOldActiveView);
		if (pOldView)
			pOldView->BeforeClose();
		pOldActiveView->DestroyWindow();
	}
	pNewView->ShowWindow(SW_SHOW);
	pNewView->OnInitialUpdate();
	RecalcLayout();

	// ������������� �� ����� �����������

	CCommonViewDealer* pCurrView = dynamic_cast<CCommonViewDealer*>(pNewView);
	if (pCurrView)
	{
		UpdateConnectionStateForCurrInlay();
		unsigned int nAddress = 0;
		GetDataFrameAddress(FRAME_TYPE_EMPTY, nAddress);
		UpdateInlayByReceivedParamMeanings(nAddress);
	}
	m_bTimerDisabled = false;
}

void CMainFrame::ViewInitialisation(CView* pView)
{
	if (!pView)
	{
		ASSERT(pView);
		return;
	}

	CCommonViewDealer* pCurrView = dynamic_cast<CCommonViewDealer*>(pView);
	if (pCurrView)
	{
		pCurrView->SetConnectionState(IsPrgState_OnlineDeviceData());

		pCurrView->SetGlobalParent(m_hWnd);
		pCurrView->SetXmlNode(GetAppEnviron()->GetXmlSett()->GetInlaysNode());
		pCurrView->SetPointerToDataFinder(DataStoreFinder);

		REG_EDIT_ACCESS_DATA ac;
		m_appEnviron.GetAccessProp(ac);
		bool bAdmin = (ac.sCurrLogin == L"Admin");
		pCurrView->SetAccessInfo(bAdmin, ac.sAdminPassword, ac.sUserPassword);
	}
	CCommonViewForm* pViewVF = dynamic_cast<CCommonViewForm*>(pView);
	if (pViewVF)
		pViewVF->SetGlobalParent(m_hWnd);

}

LRESULT CMainFrame::OnReloadCurrentView(WPARAM wParam, LPARAM lParam)
{
	int nCurrViewID = GetCurrentViewID();
	SelectViewByTabItemID(IDD_RELOAD_CURRENT_VIEW);
	SelectViewByTabItemID(nCurrViewID);
	return 1;
}

bool CMainFrame::WasTabSwitchedInInlay()
{
	CView* pActiveView = GetActiveView_();
	if (pActiveView)
	{
		CCommonViewForm* pVF = dynamic_cast<CCommonViewForm*>(pActiveView);
		if (pVF
			&& pVF->If_AFX_WM_CHANGE_ACTIVE_TAB_Was())
		{
			pVF->Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bUnsetVariable_d);
			return true;
		}
	}
	return false;
}

void CMainFrame::UpdateConnectionStateForCurrInlay()
{
	CView* pActiveView = GetActiveView_();
	if (!pActiveView)
		return;

	CCommonViewDealer* pCurrViewDlr = dynamic_cast<CCommonViewDealer*>(pActiveView);
	if (pCurrViewDlr)
	{
		pCurrViewDlr->SetConnectionState(IsPrgState_OnlineDeviceData());
		pCurrViewDlr->UpdateViewByConnectionState();
	}
}

void CMainFrame::UpdateInlayByReceivedParamMeanings(unsigned int nAddress)
{
	CView* pActiveView = GetActiveView_();
	if (!pActiveView)
	{
		ASSERT(false);
		return;
	}

	CCommonViewDealer* pCurrViewDlr = dynamic_cast<CCommonViewDealer*>(pActiveView);
	if (pCurrViewDlr)
		pCurrViewDlr->UpdateParamMeaningsByReceivedDeviceData(nAddress);
}

void CMainFrame::UpdateInlayByReceivedConfirmation(unsigned int nAddress, bool bResult)
{
	CView* pActiveView = GetActiveView_();
	if (!pActiveView)
	{
		ASSERT(false);
		return;
	}

	CCommonViewDealer* pCurrViewDlr = dynamic_cast<CCommonViewDealer*>(pActiveView);
	if (pCurrViewDlr)
		pCurrViewDlr->UpdateInlayByReceivedConfirmation(nAddress, bResult);
}

void CMainFrame::GetSendingData(vector<SEND_UNIT>* pList)
{
	if (!IsCurrentView())
		return;

	CView* pActiveView = GetActiveView_();
	if (pActiveView)
	{
		CCommonViewDealer* pCurrView = dynamic_cast<CCommonViewDealer*>(pActiveView);
		if (pList && pCurrView)
			pCurrView->GetDataForSend(pList);
	}
}

void CMainFrame::ActivateByTimer250()
{
	if (m_bTimerDisabled)
		return;
	CView* pActiveView = GetActiveView_();
	if (pActiveView)
	{
		CCommonViewDealer* pCurrView = dynamic_cast<CCommonViewDealer*>(pActiveView);
		if (pCurrView)
			pCurrView->OnTimer250();
	}
}

