#include "stdafx.h"
#include "Defines.h"
#include "Resource.h"
#include "Ai_Font.h"
#include "Ai_Str.h"
#include "View_SwitchOffRele.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_SwitchOffRele, CCommonViewForm)

namespace ns_PowLimit
{
  // ����� ��������

  int g_chkP = 0;
  CString g_sP_value = L"1000";
  CString g_sP_durationOver = L"60";
  CString g_sP_durationOff = L"5";

  // ����� �������

  int g_chkE = 0;

  CString g_sE_T1 = L"0";
  int g_cnkE_T1_over = 0;
  CString g_sE_T2 = L"0";
  int g_cnkE_T2_over = 0;
  CString g_sE_T3 = L"0";
  int g_cnkE_T3_over = 0;
  CString g_sE_T4 = L"0";
  int g_cnkE_T4_over = 0;
  CString g_sE_T5 = L"0";
  int g_cnkE_T5_over = 0;
  CString g_sE_T6 = L"0";
  int g_cnkE_T6_over = 0;
  CString g_sE_T7 = L"0";
  int g_cnkE_T7_over = 0;
  CString g_sE_T8 = L"0";
  int g_cnkE_T8_over = 0;


  struct X_TAGS
  {
    CString sPowEnergyLimit = L"PowerAndEnergyLimits";

    CString sPowerSettings = L"Power_Settings";
    CParamsTreeNode* pPowerSettingsNode = nullptr;
    CString sPAttr_On = L"On";
    CString sPAttr_P = L"P_value";
    CString sPAttr_DurOver = L"DurOver";
    CString sPAttr_DurOff = L"DurOff";

    CString sEnergySettings = L"Energy_Settings";
    CParamsTreeNode* pEnergySettingsNode = nullptr;
    CString sEAttr_On = L"On";
    CString sEAttr_T1 = L"T1";
    CString sEAttr_T1On = L"T1On";
    CString sEAttr_T2 = L"T2";
    CString sEAttr_T2On = L"T2On";
    CString sEAttr_T3 = L"T3";
    CString sEAttr_T3On = L"T3On";
    CString sEAttr_T4 = L"T4";
    CString sEAttr_T4On = L"T4On";
    CString sEAttr_T5 = L"T5";
    CString sEAttr_T5On = L"T5On";
    CString sEAttr_T6 = L"T6";
    CString sEAttr_T6On = L"T6On";
    CString sEAttr_T7 = L"T7";
    CString sEAttr_T7On = L"T7On";
    CString sEAttr_T8 = L"T8";
    CString sEAttr_T8On = L"T8On";
  }
  g_xTags;

} // namespace ns_PowLimit

void SetDefaultValues()
{
  ns_PowLimit::g_chkP = 0;
  ns_PowLimit::g_sP_value = L"1000";
  ns_PowLimit::g_sP_durationOver = L"60";
  ns_PowLimit::g_sP_durationOff = L"5";

  ns_PowLimit::g_chkE = 0;
  ns_PowLimit::g_sE_T1 = L"0";
  ns_PowLimit::g_cnkE_T1_over = 0;
  ns_PowLimit::g_sE_T2 = L"0";
  ns_PowLimit::g_cnkE_T2_over = 0;
  ns_PowLimit::g_sE_T3 = L"0";
  ns_PowLimit::g_cnkE_T3_over = 0;
  ns_PowLimit::g_sE_T4 = L"0";
  ns_PowLimit::g_cnkE_T4_over = 0;
  ns_PowLimit::g_sE_T5 = L"0";
  ns_PowLimit::g_cnkE_T5_over = 0;
  ns_PowLimit::g_sE_T6 = L"0";
  ns_PowLimit::g_cnkE_T6_over = 0;
  ns_PowLimit::g_sE_T7 = L"0";
  ns_PowLimit::g_cnkE_T7_over = 0;
  ns_PowLimit::g_sE_T8 = L"0";
  ns_PowLimit::g_cnkE_T8_over = 0;
}

CView_SwitchOffRele::CView_SwitchOffRele()
: CCommonViewForm(CView_SwitchOffRele::IDD, L"CView_SwitchOffRele")
{
}

CView_SwitchOffRele::~CView_SwitchOffRele()
{
}

BEGIN_MESSAGE_MAP(CView_SwitchOffRele, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_SWITCH_ON, &CView_SwitchOffRele::OnBnClickedBtnSwitchOn)
  ON_BN_CLICKED(IDC_BTN_SWITCH_OFF, &CView_SwitchOffRele::OnBnClickedBtnSwitchOff)
  ON_BN_CLICKED(IDC_CHK_POWER_LIMIT, &CView_SwitchOffRele::OnBnClickedSwitchOnPowerLimit)
  ON_BN_CLICKED(IDC_CHK_ENERGY_LIMIT, &CView_SwitchOffRele::OnBnClickedSwitchOnEnergyLimit)
  ON_EN_KILLFOCUS(IDC_EDT_POWER, &CView_SwitchOffRele::OnEnKillfocusEdtPower)
  ON_EN_KILLFOCUS(IDC_EDT_P_DURATION_OVER, &CView_SwitchOffRele::OnEnKillfocusEdtDurOver)
  ON_EN_KILLFOCUS(IDC_EDT_P_DURATION_OFF, &CView_SwitchOffRele::OnEnKillfocusEdtDurOff)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T1, &CView_SwitchOffRele::OnEnKillfocusEdtT1)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T2, &CView_SwitchOffRele::OnEnKillfocusEdtT2)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T3, &CView_SwitchOffRele::OnEnKillfocusEdtT3)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T4, &CView_SwitchOffRele::OnEnKillfocusEdtT4)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T5, &CView_SwitchOffRele::OnEnKillfocusEdtT5)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T6, &CView_SwitchOffRele::OnEnKillfocusEdtT6)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T7, &CView_SwitchOffRele::OnEnKillfocusEdtT7)
  ON_EN_KILLFOCUS(IDC_EDT_ENRG_T8, &CView_SwitchOffRele::OnEnKillfocusEdtT8)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T1, &CView_SwitchOffRele::OnBnClickedChkEOverT1)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T2, &CView_SwitchOffRele::OnBnClickedChkEOverT2)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T3, &CView_SwitchOffRele::OnBnClickedChkEOverT3)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T4, &CView_SwitchOffRele::OnBnClickedChkEOverT4)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T5, &CView_SwitchOffRele::OnBnClickedChkEOverT5)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T6, &CView_SwitchOffRele::OnBnClickedChkEOverT6)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T7, &CView_SwitchOffRele::OnBnClickedChkEOverT7)
  ON_BN_CLICKED(IDC_CHK_E_OVER_T8, &CView_SwitchOffRele::OnBnClickedChkEOverT8)
  ON_BN_CLICKED(IDC_BTN_LOAD_POW_LIMIT_FROM_DEVICE, &CView_SwitchOffRele::OnBnClickedBtnLoadPowLimitFromDevice)
  ON_BN_CLICKED(IDC_BTN_RECORD_POW_LIMIT_TO_DEVICE, &CView_SwitchOffRele::OnBnClickedBtnRecordPowLimitToDevice)
END_MESSAGE_MAP()

void CView_SwitchOffRele::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_BTN_SWITCH_ON, m_btnSwitchOn);
  DDX_Control(pDX, IDC_BTN_SWITCH_OFF, m_btnSwitchOff);
  DDX_Control(pDX, IDC_STC_RELE_STATE, m_stcReleState);
  DDX_Control(pDX, IDC_CHK_POWER_LIMIT, m_chkPowerLimit);
  DDX_Control(pDX, IDC_BTN_LOAD_POW_LIMIT_FROM_DEVICE, m_btnLoadPowerLimit);
  DDX_Control(pDX, IDC_BTN_RECORD_POW_LIMIT_TO_DEVICE, m_btnRecordPowerLimit);
  DDX_Control(pDX, IDC_CHK_ENERGY_LIMIT, m_chkEnergyLimit);
  DDX_Control(pDX, IDC_EDT_POWER, m_edtPowLmt_power);
  DDX_Control(pDX, IDC_EDT_P_DURATION_OVER, m_edtPowLmt_durationOver);
  DDX_Control(pDX, IDC_EDT_P_DURATION_OFF, m_edtPowLmt_durationOff);
  DDX_Control(pDX, IDC_EDT_ENRG_T1, m_edtEnrgLmt_T1);
  DDX_Control(pDX, IDC_EDT_ENRG_T2, m_edtEnrgLmt_T2);
  DDX_Control(pDX, IDC_EDT_ENRG_T3, m_edtEnrgLmt_T3);
  DDX_Control(pDX, IDC_EDT_ENRG_T4, m_edtEnrgLmt_T4);
  DDX_Control(pDX, IDC_EDT_ENRG_T5, m_edtEnrgLmt_T5);
  DDX_Control(pDX, IDC_EDT_ENRG_T6, m_edtEnrgLmt_T6);
  DDX_Control(pDX, IDC_EDT_ENRG_T7, m_edtEnrgLmt_T7);
  DDX_Control(pDX, IDC_EDT_ENRG_T8, m_edtEnrgLmt_T8);
  DDX_Control(pDX, IDC_CHK_E_OVER_T1, m_chkEnrgLmt_T1);
  DDX_Control(pDX, IDC_CHK_E_OVER_T2, m_chkEnrgLmt_T2);
  DDX_Control(pDX, IDC_CHK_E_OVER_T3, m_chkEnrgLmt_T3);
  DDX_Control(pDX, IDC_CHK_E_OVER_T4, m_chkEnrgLmt_T4);
  DDX_Control(pDX, IDC_CHK_E_OVER_T5, m_chkEnrgLmt_T5);
  DDX_Control(pDX, IDC_CHK_E_OVER_T6, m_chkEnrgLmt_T6);
  DDX_Control(pDX, IDC_CHK_E_OVER_T7, m_chkEnrgLmt_T7);
  DDX_Control(pDX, IDC_CHK_E_OVER_T8, m_chkEnrgLmt_T8);
}

void CView_SwitchOffRele::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CFont* pFont = GetFont();
  LOGFONT lf = {0};
  pFont->GetLogFont(&lf);
  int nFontH = lf.lfHeight * 3;
  CAi_Font::CreateFont_(this, m_bigFont, L"", FW_BOLD, DEFAULT_QUALITY, nFontH);
  m_stcReleState.SetFont(&m_bigFont);

  SetParamTypeForRead(FRAME_TYPE_RELAY_POS);

  WatchForCtrState(&m_btnSwitchOn, rightAdmin);
  WatchForCtrState(&m_btnSwitchOff, rightAdmin);

  WatchForCtrState(&m_btnLoadPowerLimit, rightAdmin);
  WatchForCtrState(&m_btnRecordPowerLimit, rightAdmin);

  m_edtPowLmt_power.SetLimitText(5);
  m_edtPowLmt_durationOver.SetLimitText(5);
  m_edtPowLmt_durationOff.SetLimitText(5);
  m_edtEnrgLmt_T1.SetLimitText(5);
  m_edtEnrgLmt_T2.SetLimitText(5);
  m_edtEnrgLmt_T3.SetLimitText(5);
  m_edtEnrgLmt_T4.SetLimitText(5);
  m_edtEnrgLmt_T5.SetLimitText(5);
  m_edtEnrgLmt_T6.SetLimitText(5);
  m_edtEnrgLmt_T7.SetLimitText(5);
  m_edtEnrgLmt_T8.SetLimitText(5);

  TakeValueFromXmlTree();
  FillInCtrls();

  WatchForCtrState(&m_chkPowerLimit, rightAdmin);
  WatchForCtrState(&m_chkEnergyLimit, rightAdmin);
  WatchForCtrState(&m_edtPowLmt_power, rightAdmin);
  WatchForCtrState(&m_edtPowLmt_durationOver, rightAdmin);
  WatchForCtrState(&m_edtPowLmt_durationOff, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T1, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T2, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T3, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T4, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T5, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T6, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T7, rightAdmin);
  WatchForCtrState(&m_edtEnrgLmt_T8, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T1, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T2, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T3, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T4, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T5, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T6, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T7, rightAdmin);
  WatchForCtrState(&m_chkEnrgLmt_T8, rightAdmin);

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_SwitchOffRele::BeforeDestroyClass()
{
}

void CView_SwitchOffRele::OnSize_()
{
}

void CView_SwitchOffRele::OnDraw(CDC* pDC)
{
}

bool CView_SwitchOffRele::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_SwitchOffRele::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_SwitchOffRele::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_SwitchOffRele::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_SwitchOffRele::OnBnClickedBtnSwitchOn()
{
  SetWaitCursor_oneWay(true);
   
  BeginFormingSendPack(true_d(L"start"));
  SendDataToDevice(FRAME_TYPE_COMMAND_RELAY_ON);
  BeginFormingSendPack(false_d(L"start"));
}

void CView_SwitchOffRele::OnBnClickedBtnSwitchOff()
{
  SetWaitCursor_oneWay(true);

  BeginFormingSendPack(true_d(L"start"));
  SendDataToDevice(FRAME_TYPE_COMMAND_RELAY_OFF);
  BeginFormingSendPack(false_d(L"start"));
}

void CView_SwitchOffRele::TakeValueFromXmlTree(CParamsTreeNode* pMainNode) 
{
  SetDefaultValues();

  CString sTag = ns_PowLimit::g_xTags.sPowEnergyLimit;

  if (!pMainNode)
  {
    CParamsTreeNode* pInlaysNode = GetXmlNode();
    m_pPowLimitNode = pInlaysNode->FindFirstNode(sTag);
    if (!m_pPowLimitNode)
    {
      m_pPowLimitNode = pInlaysNode->AddNode(sTag);

      sTag = ns_PowLimit::g_xTags.sPowerSettings;
      ns_PowLimit::g_xTags.pPowerSettingsNode = m_pPowLimitNode->AddNode(sTag);

      sTag = ns_PowLimit::g_xTags.sEnergySettings;
      ns_PowLimit::g_xTags.pEnergySettingsNode = m_pPowLimitNode->AddNode(sTag);

      return;
    }
  }

  sTag = ns_PowLimit::g_xTags.sPowerSettings;
  ns_PowLimit::g_xTags.pPowerSettingsNode = m_pPowLimitNode->FindFirstNode(sTag);
  if (!ns_PowLimit::g_xTags.pPowerSettingsNode)
  {
    ns_PowLimit::g_xTags.pPowerSettingsNode = m_pPowLimitNode->AddNode(sTag);
  }
  else
  {
    CParamsTreeNode* pX = ns_PowLimit::g_xTags.pPowerSettingsNode;
    CString sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sPAttr_On);
    if (sData == L"1")
      ns_PowLimit::g_chkP = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sPAttr_P);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sP_value = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sPAttr_DurOver);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sP_durationOver = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sPAttr_DurOff);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sP_durationOff = sData;
  }

  sTag = ns_PowLimit::g_xTags.sEnergySettings;
  ns_PowLimit::g_xTags.pEnergySettingsNode = m_pPowLimitNode->FindFirstNode(sTag);
  if (!ns_PowLimit::g_xTags.pEnergySettingsNode)
  {
    ns_PowLimit::g_xTags.pEnergySettingsNode = m_pPowLimitNode->AddNode(sTag);
  }
  else
  {
    CParamsTreeNode* pX = ns_PowLimit::g_xTags.pEnergySettingsNode;
    CString sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_On);
    if (sData == L"1")
      ns_PowLimit::g_chkE = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T1);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T1 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T1On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T1_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T2);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T2 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T2On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T2_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T3);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T3 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T3On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T3_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T4);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T4 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T4On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T4_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T5);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T5 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T5On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T5_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T6);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T6 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T6On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T6_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T7);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T7 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T7On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T7_over = 1;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T8);
    if (!sData.IsEmpty())
      ns_PowLimit::g_sE_T8 = sData;
    sData = pX->GetAttributeValue(ns_PowLimit::g_xTags.sEAttr_T8On);
    if (sData == L"1")
      ns_PowLimit::g_cnkE_T8_over = 1;
  }
}

void CView_SwitchOffRele::PutValueToXmlTree() 
{
  CParamsTreeNode* pX = ns_PowLimit::g_xTags.pPowerSettingsNode;
  pX->DeleteAllAttributes();
  pX->AddAttribute(ns_PowLimit::g_xTags.sPAttr_On, CAi_Str::ToString(ns_PowLimit::g_chkP));
  pX->AddAttribute(ns_PowLimit::g_xTags.sPAttr_P, ns_PowLimit::g_sP_value);
  pX->AddAttribute(ns_PowLimit::g_xTags.sPAttr_DurOver, ns_PowLimit::g_sP_durationOver);
  pX->AddAttribute(ns_PowLimit::g_xTags.sPAttr_DurOff, ns_PowLimit::g_sP_durationOff);

  pX = ns_PowLimit::g_xTags.pEnergySettingsNode;
  pX->DeleteAllAttributes();
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_On, CAi_Str::ToString(ns_PowLimit::g_chkE));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T1, ns_PowLimit::g_sE_T1);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T1On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T1_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T2, ns_PowLimit::g_sE_T2);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T2On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T2_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T3, ns_PowLimit::g_sE_T3);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T3On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T3_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T4, ns_PowLimit::g_sE_T4);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T4On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T4_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T5, ns_PowLimit::g_sE_T5);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T5On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T5_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T6, ns_PowLimit::g_sE_T6);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T6On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T6_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T7, ns_PowLimit::g_sE_T7);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T7On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T7_over));
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T8, ns_PowLimit::g_sE_T8);
  pX->AddAttribute(ns_PowLimit::g_xTags.sEAttr_T8On, CAi_Str::ToString(ns_PowLimit::g_cnkE_T8_over));

  UpdateXmlBackupFile();
}

void CView_SwitchOffRele::FillInCtrls()
{
  m_chkPowerLimit.SetCheck(ns_PowLimit::g_chkP);
  CheckState_PowerCtrls();
  m_chkEnergyLimit.SetCheck(ns_PowLimit::g_chkE);
  CheckState_EnergyCtrls();
  m_edtPowLmt_power.SetWindowText(ns_PowLimit::g_sP_value);
  m_edtPowLmt_durationOver.SetWindowText(ns_PowLimit::g_sP_durationOver);
  m_edtPowLmt_durationOff.SetWindowText(ns_PowLimit::g_sP_durationOff);
  m_edtEnrgLmt_T1.SetWindowText(ns_PowLimit::g_sE_T1);
  m_edtEnrgLmt_T2.SetWindowText(ns_PowLimit::g_sE_T2);
  m_edtEnrgLmt_T3.SetWindowText(ns_PowLimit::g_sE_T3);
  m_edtEnrgLmt_T4.SetWindowText(ns_PowLimit::g_sE_T4);
  m_edtEnrgLmt_T5.SetWindowText(ns_PowLimit::g_sE_T5);
  m_edtEnrgLmt_T6.SetWindowText(ns_PowLimit::g_sE_T6);
  m_edtEnrgLmt_T7.SetWindowText(ns_PowLimit::g_sE_T7);
  m_edtEnrgLmt_T8.SetWindowText(ns_PowLimit::g_sE_T8);
  m_chkEnrgLmt_T1.SetCheck(ns_PowLimit::g_cnkE_T1_over);
  m_chkEnrgLmt_T2.SetCheck(ns_PowLimit::g_cnkE_T2_over);
  m_chkEnrgLmt_T3.SetCheck(ns_PowLimit::g_cnkE_T3_over);
  m_chkEnrgLmt_T4.SetCheck(ns_PowLimit::g_cnkE_T4_over);
  m_chkEnrgLmt_T5.SetCheck(ns_PowLimit::g_cnkE_T5_over);
  m_chkEnrgLmt_T6.SetCheck(ns_PowLimit::g_cnkE_T6_over);
  m_chkEnrgLmt_T7.SetCheck(ns_PowLimit::g_cnkE_T7_over);
  m_chkEnrgLmt_T8.SetCheck(ns_PowLimit::g_cnkE_T8_over);
}

void CView_SwitchOffRele::OnBnClickedBtnLoadPowLimitFromDevice()
{
  FormListForClearReceivedData(FRAME_TYPE_POWER_LIMIT);
  FormListForClearReceivedData(FRAME_TYPE_ENERGY_LIMIT);

  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_SwitchOffRele::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);

  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);
  bool bOnceCmd = true;

  SetParamTypeForRead(FRAME_TYPE_POWER_LIMIT, bOnceCmd);
  SetParamTypeForRead(FRAME_TYPE_ENERGY_LIMIT, bOnceCmd);

  BeginFormingReadPack(false);
}

void CView_SwitchOffRele::OnBnClickedBtnRecordPowLimitToDevice()
{
  SetWaitCursor_answer(true);

  BeginFormingSendPack(true_d(L"start"));

  m_nPowerAndEnergyWasRecorded = 0;

  SendDataToDevice(FRAME_TYPE_POWER_LIMIT, FormDataPower());
  SendDataToDevice(FRAME_TYPE_ENERGY_LIMIT, FormDataEnergy());
  SendDataToDevice(FRAME_TYPE_COMMAND_LIMITS_POW_ENRG_EXECUTE); // Apply new settings

  BeginFormingSendPack(false_d(L"start"));
}

CString CView_SwitchOffRele::FormDataPower()
{
  CString sData =
    CAi_Str::ToString(ns_PowLimit::g_chkP) + CString(L";") +
    ns_PowLimit::g_sP_value + CString(L";") +
    ns_PowLimit::g_sP_durationOver + CString(L";") +
    ns_PowLimit::g_sP_durationOff;
  return sData;
}

bool CView_SwitchOffRele::ParseDataPower(CString s)
{
  vector<CString> res;
  s.TrimRight(L";");
  CAi_Str::CutString(s, L";", &res);
  if (res.size() != 4)
  {
    AfxMessageBox(L"������ � ������ ������ ��������!", MB_ICONERROR);
    return false;
  }

  int i = 0;
  ns_PowLimit::g_chkP = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sP_value = res[i++];
  ns_PowLimit::g_sP_durationOver = res[i++];
  ns_PowLimit::g_sP_durationOff = res[i++];

  return true;
}

CString CView_SwitchOffRele::FormDataEnergy()
{
  CString sData =
    CAi_Str::ToString(ns_PowLimit::g_chkE) + CString(L";") +
    ns_PowLimit::g_sE_T1 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T1_over) + CString(L";") +
    ns_PowLimit::g_sE_T2 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T2_over) + CString(L";") +
    ns_PowLimit::g_sE_T3 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T3_over) + CString(L";") +
    ns_PowLimit::g_sE_T4 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T4_over) + CString(L";") +
    ns_PowLimit::g_sE_T5 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T5_over) + CString(L";") +
    ns_PowLimit::g_sE_T6 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T6_over) + CString(L";") +
    ns_PowLimit::g_sE_T7 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T7_over) + CString(L";") +
    ns_PowLimit::g_sE_T8 + CString(L";") +
    CAi_Str::ToString(ns_PowLimit::g_cnkE_T8_over);
  return sData;
}

bool CView_SwitchOffRele::ParseDataEnergy(CString s)
{
  vector<CString> res;
  s.TrimRight(L";");
  CAi_Str::CutString(s, L";", &res);
  if (res.size() != 17)
  {
    AfxMessageBox(L"������ � ������ ������ �������!", MB_ICONERROR);
    return false;
  }

  int i = 0;
  ns_PowLimit::g_chkE = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T1 = res[i++];
  ns_PowLimit::g_cnkE_T1_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T2 = res[i++];
  ns_PowLimit::g_cnkE_T2_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T3 = res[i++];
  ns_PowLimit::g_cnkE_T3_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T4 = res[i++];
  ns_PowLimit::g_cnkE_T4_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T5 = res[i++];
  ns_PowLimit::g_cnkE_T5_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T6 = res[i++];
  ns_PowLimit::g_cnkE_T6_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T7 = res[i++];
  ns_PowLimit::g_cnkE_T7_over = CAi_Str::ToInt(res[i++]);
  ns_PowLimit::g_sE_T8 = res[i++];
  ns_PowLimit::g_cnkE_T8_over = CAi_Str::ToInt(res[i++]);

  return true;
}

void CView_SwitchOffRele::OnBnClickedSwitchOnPowerLimit()
{
  ns_PowLimit::g_chkP = m_chkPowerLimit.GetCheck();
  CheckState_PowerCtrls();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedSwitchOnEnergyLimit()
{
  ns_PowLimit::g_chkE = m_chkEnergyLimit.GetCheck();
  CheckState_EnergyCtrls();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::CheckState_PowerCtrls()
{
  BOOL bEnable = ns_PowLimit::g_chkP ? TRUE : FALSE;
  m_edtPowLmt_power.EnableWindow(bEnable);
  m_edtPowLmt_durationOver.EnableWindow(bEnable);
  m_edtPowLmt_durationOff.EnableWindow(bEnable);
}

void CView_SwitchOffRele::CheckState_EnergyCtrls()
{
  BOOL bEnable = ns_PowLimit::g_chkE ? TRUE : FALSE;
  m_edtEnrgLmt_T1.EnableWindow(bEnable);
  m_chkEnrgLmt_T1.EnableWindow(bEnable);
  m_edtEnrgLmt_T2.EnableWindow(bEnable);
  m_chkEnrgLmt_T2.EnableWindow(bEnable);
  m_edtEnrgLmt_T3.EnableWindow(bEnable);
  m_chkEnrgLmt_T3.EnableWindow(bEnable);
  m_edtEnrgLmt_T4.EnableWindow(bEnable);
  m_chkEnrgLmt_T4.EnableWindow(bEnable);
  m_edtEnrgLmt_T5.EnableWindow(bEnable);
  m_chkEnrgLmt_T5.EnableWindow(bEnable);
  m_edtEnrgLmt_T6.EnableWindow(bEnable);
  m_chkEnrgLmt_T6.EnableWindow(bEnable);
  m_edtEnrgLmt_T7.EnableWindow(bEnable);
  m_chkEnrgLmt_T7.EnableWindow(bEnable);
  m_edtEnrgLmt_T8.EnableWindow(bEnable);
  m_chkEnrgLmt_T8.EnableWindow(bEnable);
}

void CView_SwitchOffRele::OnEnKillfocusEdtPower()
{
  m_edtPowLmt_power.GetWindowText(ns_PowLimit::g_sP_value);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtDurOver()
{
  m_edtPowLmt_durationOver.GetWindowText(ns_PowLimit::g_sP_durationOver);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtDurOff()
{
  m_edtPowLmt_durationOff.GetWindowText(ns_PowLimit::g_sP_durationOff);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT1()
{
  m_edtEnrgLmt_T1.GetWindowText(ns_PowLimit::g_sE_T1);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT2()
{
  m_edtEnrgLmt_T2.GetWindowText(ns_PowLimit::g_sE_T2);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT3()
{
  m_edtEnrgLmt_T3.GetWindowText(ns_PowLimit::g_sE_T3);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT4()
{
  m_edtEnrgLmt_T4.GetWindowText(ns_PowLimit::g_sE_T4);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT5()
{
  m_edtEnrgLmt_T5.GetWindowText(ns_PowLimit::g_sE_T5);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT6()
{
  m_edtEnrgLmt_T6.GetWindowText(ns_PowLimit::g_sE_T6);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT7()
{
  m_edtEnrgLmt_T7.GetWindowText(ns_PowLimit::g_sE_T7);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnEnKillfocusEdtT8()
{
  m_edtEnrgLmt_T8.GetWindowText(ns_PowLimit::g_sE_T8);
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT1()
{
  ns_PowLimit::g_cnkE_T1_over = m_chkEnrgLmt_T1.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT2()
{
  ns_PowLimit::g_cnkE_T2_over = m_chkEnrgLmt_T2.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT3()
{
  ns_PowLimit::g_cnkE_T3_over = m_chkEnrgLmt_T3.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT4()
{
  ns_PowLimit::g_cnkE_T4_over = m_chkEnrgLmt_T4.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT5()
{
  ns_PowLimit::g_cnkE_T5_over = m_chkEnrgLmt_T5.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT6()
{
  ns_PowLimit::g_cnkE_T6_over = m_chkEnrgLmt_T6.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT7()
{
  ns_PowLimit::g_cnkE_T7_over = m_chkEnrgLmt_T7.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnBnClickedChkEOverT8()
{
  ns_PowLimit::g_cnkE_T8_over = m_chkEnrgLmt_T8.GetCheck();
  PutValueToXmlTree();
}

void CView_SwitchOffRele::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_SwitchOffRele::CheckMeaningsViewByConnectionStatus()
{
  UpdateMonitoringCtrlView(&m_stcReleState);

  if (IsConnection() && IsConectionAdminAccess())
  {
    CheckState_PowerCtrls();
    CheckState_EnergyCtrls();
  }
}

void CView_SwitchOffRele::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);

  if (ft == FRAME_TYPE_RELAY_POS)
  {
    if (m == L"\xF")
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ �������� ������ �� ���� ����������!", MB_ICONERROR);
      return;
    }

    if (IsConnection())
    {
      m = FindReceivedData(ft);
      if (m == L"1")
        m = L"I";
      else
        if (m == L"0")
          m = L"O";
    }

    varMeaning_t mCurr;
    m_stcReleState.GetWindowText(mCurr);
    if (mCurr != m)
      m_stcReleState.SetWindowText(m);
  }

  if (ft == FRAME_TYPE_POWER_LIMIT)
  {
    if (m == L"\xF")
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ �������� ������ �� ������ ��������!", MB_ICONERROR);
      return;
    }

    if (ParseDataPower(m))
    {
      PutValueToXmlTree();
      FillInCtrls();
    }
  }

  if (ft == FRAME_TYPE_ENERGY_LIMIT)
  {
    if (m == L"\xF")
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ �������� ������ �� ������ �������!", MB_ICONERROR);
      return;
    }

    if (ParseDataEnergy(m))
    {
      PutValueToXmlTree();
      FillInCtrls();
      AfxMessageBox(L"��� ������ �� ������� �������� � ������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }

    SetWaitCursor_answer(false);
  }
}

void CView_SwitchOffRele::UpdateInlayByReceivedConfirmation(teFrameTypes ft, bool bResult)
{
  if (ft == FRAME_TYPE_POWER_LIMIT)
  {
    if (!bResult)
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ ������ ������ �� ������ ��������!", MB_ICONERROR);
      return;
    }
    else
      ++m_nPowerAndEnergyWasRecorded;
  }

  if (ft == FRAME_TYPE_ENERGY_LIMIT)
  {
    if (!bResult)
    {
      ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������ ������ ������ �� ������ �������!", MB_ICONERROR);
      return;
    }
    else
      ++m_nPowerAndEnergyWasRecorded;
  }

  if (ft == FRAME_TYPE_COMMAND_LIMITS_POW_ENRG_EXECUTE)
  {
    SetWaitCursor_answer(false);
    if (m_nPowerAndEnergyWasRecorded == 2)
      AfxMessageBox(L"������ �� ������� �������� � ������� ������� �������� � �������!", MB_ICONINFORMATION);
  }
}
