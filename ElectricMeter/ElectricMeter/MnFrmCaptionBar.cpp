#include "StdAfx.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CMainFrame::OnViewCaptionBar()
{
  ShowPane(&m_wndCaptionBar,
           !(m_wndCaptionBar.IsVisible()),
           FALSE, TRUE);
  RecalcLayout();
}

void CMainFrame::OnUpdateViewCaptionBar(CCmdUI* pCmdUI)
{
  pCmdUI->SetCheck(m_wndCaptionBar.IsVisible());
}

void CMainFrame::SetCaptionBarText(CString sText)
{
  m_wndCaptionBar.m_clrBarBackground = RGB(255, 255, 255);
  m_wndCaptionBar.SetText(sText, CMFCCaptionBar::ALIGN_CENTER);
  m_wndCaptionBar.RedrawWindow();
}

void CMainFrame::CheckCaptionState()
{
  if (IsPrgState_Begin())
    m_wndCaptionBar.SetBgColor(CMFCCaptionBarExt::clrBlue_LBlue);
    //m_wndCaptionBar.SetBgColor(CMFCCaptionBarExt::clrGray_LGray);

  if (IsPrgState_OnlineDeviceData())
  {
    m_wndCaptionBar.SetBgColor(CMFCCaptionBarExt::clrYelow_LYellow);
    SetCaptionBtnView(cbConnection);
  }

  if (IsPrgState_OfflineDeviceData())
  {
    m_wndCaptionBar.SetBgColor(CMFCCaptionBarExt::clrBlue_LBlue);
    SetCaptionBtnView(cbConnect);
  }

  m_wndCaptionBar.Invalidate();
}

void CMainFrame::SetCaptionBtnView(ECaptionButton cb)
{
  CString sLabel;
  int nID = 0;
  bool bOffDropDownArrow = false;
  CString sShortToolTip;
  CString sToolTip;

  switch (cb)
  {
    case cbConnect:
      sLabel = L"TEXT";
      nID = ID_CAPTION_BTN_CONNECT;
      sShortToolTip = L"������������";
      sToolTip = L"������������ � ��������";
      break;

    case cbBreakConnection:
      sLabel = L"TEXT";
      nID = ID_CAPTION_BTN_BREAK_CONNECTION;
      sShortToolTip = L"��������";
      sToolTip = L"�������� ����������� � ��������";
      break;

    case cbConnection:
      sLabel = L"TEXT";
      nID = ID_CAPTION_BTN_DISCONNECT;
      sShortToolTip = L"�����������";
      sToolTip = L"����������� �� ��������";
      break;

    default:
      ASSERT(false);
  }

  m_wndCaptionBar.RemoveButton();
  m_wndCaptionBar.SetBtnType(cb);
  m_wndCaptionBar.SetButton(sLabel, nID, CMFCCaptionBar::ALIGN_LEFT, bOffDropDownArrow);
  m_wndCaptionBar.SetButtonToolTip(sShortToolTip, sToolTip);
  m_wndCaptionBar.EnableButton();
}

void CMainFrame::ShowIndicatorOfConnection()
{
  SetCaptionBtnView(cbBreakConnection);
  SetTimer_(ID_TMR_CONNECTION_INDICATOR, 10);
  m_nIndicatorPercentRate = 0;
  m_wndCaptionBar.EnableIndicatorMode(true);
  m_wndCaptionBar.SetFlatBorder();
  PushIndicator();
}

void CMainFrame::HideCaptionBarIndicator(ECapBarIndicatorRes indicRes)
{
  KillTimer_(ID_TMR_CONNECTION_INDICATOR);
  //m_wndCaptionBar.SetButtonToolTip(L"");

  if (indicRes == indicOK)
  {
    m_wndCaptionBar.SetIndicatorPos(100);
    Sleep(700);
  }

  //m_wndCaptionBar.EnableButton(FALSE);
  m_wndCaptionBar.SetFlatBorder(FALSE);
  m_wndCaptionBar.EnableIndicatorMode(false);
  //m_wndCaptionBar.RemoveButton();
}

void CMainFrame::PushIndicator()
{
  if (m_nIndicatorPercentRate >= 100)
  {
    m_nIndicatorPercentRate = 0;// 100;
    return;
  }
  //CString sLabel;
  //sLabel.Format(L"�����������...%d", ++m_nIndicatorPercentRate);
  //sLabel += L"%";
  //BOOL bOffDropDownArrow = FALSE;
  //m_wndCaptionBar.SetButton(sLabel, ID_CAPTION_BTN_BREAK_CONNECTION, CMFCCaptionBar::ALIGN_LEFT, bOffDropDownArrow);
  //m_wndCaptionBar.SetButtonToolTip(L"��������", L"�������� ����������� � ��������");
  //m_wndCaptionBar.EnableButton();
  m_wndCaptionBar.Invalidate();

  m_wndCaptionBar.SetIndicatorPos((BYTE)m_nIndicatorPercentRate);

  // TEMPORARY: display connection process; it is need delete
  //if (m_nIndicatorPercentRate == 100)
  //  m_deviceConnector.Connect();
}

void CMainFrame::OnCaptionBtn_Connect()
{
  OnTbrConnect();
}

void CMainFrame::OnCaptionBtn_BreakConnection()
{
  HideCaptionBarIndicator(indicBreak);
  DisconnectionFromDevice();
}

void CMainFrame::OnCaptionBtn_Disconnect()
{
  OnTbrConnect();
}

LRESULT CMainFrame::OnCaptionBtn_Disconnect(WPARAM wParam, LPARAM lParam)
{
  OnCaptionBtn_Disconnect();
  return TRUE;
}