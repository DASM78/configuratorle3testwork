#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include <vector>
#include "Ai_Str.h"
#include "View_EventJournal.h"

using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_EventJournal, CCommonViewForm)

using namespace std;

namespace ns_Jour
{

  struct COLUMNS_FILTER
  {
    int nCheck = -1;
    int nType = -1;
  }
  g_colsFilter;

  struct JOUR_FILTER
  {
    JOUR_FILTER(CString id, teEventTypes i, CString name)
      : sXmlId(id)
      , evType(i)
      , sName(name)
    { }

    int nCheck = 1;
    teEventTypes evType = TYPE_EVENT_NONE;
    CString sXmlId;
    CString sName;
  };
  vector <JOUR_FILTER> g_rowsFilter;

  struct COLUMNS
  {
    int nDate = -1;
    int nTime = -1;
    int nName = -1;
  }
  g_cols;
  int g_nComnSignificantCell = -1;

  struct DATA
  {
    CString sDate;
    CString sTime;
    CString sReserved;

    JOUR_FILTER* pFltr = nullptr;
  };
  vector <DATA> g_data;
  int MAX_EVENT_JOUNAL = 0;
  int g_nRow_from = 0;

  struct X_TAGS
  {
    CString sEvJour = L"EventJournal";
    CString sFilter = L"Filter_";
    CString sDataSet = L"DataSet";
  }
  g_xTags;

} // namespace ns_Jour

void CreateFilter()
{
  if (!ns_Jour::g_rowsFilter.empty())
    return;

  auto lbmAdd = [&](teEventTypes evT, CString sSign, CString sName)->void
  {
    ns_Jour::g_rowsFilter.push_back(ns_Jour::JOUR_FILTER(sSign, evT, sName));
  };

  //lbmAdd(L"None", TYPE_EVENT_NONE, L"None");
  lbmAdd(TYPE_EVENT_POWER_ON, L"PwrOn", L"��������� �������");
  lbmAdd(TYPE_EVENT_POWER_OFF, L"PwrOff", L"���������� �������");

  lbmAdd(TYPE_EVENT_VOLTAGE_ON_PHASE1, L"UOnPh1", L"��������� ���������� � ���� 1");
  lbmAdd(TYPE_EVENT_VOLTAGE_OFF_PHASE1, L"UOffPh1", L"���������� ���������� � ���� 1");
  lbmAdd(TYPE_EVENT_VOLTAGE_ON_PHASE2, L"UOnPh2", L"��������� ���������� � ���� 2");
  lbmAdd(TYPE_EVENT_VOLTAGE_OFF_PHASE2, L"UOffPh2", L"���������� ���������� � ���� 2");
  lbmAdd(TYPE_EVENT_VOLTAGE_ON_PHASE3, L"UOnPh3", L"��������� ���������� � ���� 3");
  lbmAdd(TYPE_EVENT_VOLTAGE_OFF_PHASE3, L"UOffPh3", L"���������� ���������� � ���� 3");

  lbmAdd(TYPE_EVENT_CURRENT_ON_WITH_VOLTAGE_ON_PHASE1, L"IOnUOnPh1", L"��������� ���� ��� ������� ���������� � ���� 1");
  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_ON_PHASE1, L"IOffUOnPh1", L"���������� ���� ��� ������� ���������� � ���� 1");
  lbmAdd(TYPE_EVENT_CURRENT_ON_WITH_VOLTAGE_ON_PHASE2, L"IOnUOnPh2", L"��������� ���� ��� ������� ���������� � ���� 2");
  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_ON_PHASE2, L"IOffUOnPh2", L"���������� ���� ��� ������� ���������� � ���� 2");
  lbmAdd(TYPE_EVENT_CURRENT_ON_WITH_VOLTAGE_ON_PHASE3, L"IOnUOnPh3", L"��������� ���� ��� ������� ���������� � ���� 3");
  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_ON_PHASE3, L"IOffUOnPh3", L"���������� ���� ��� ������� ���������� � ���� 3");  

  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_OFF_PHASE1, L"IOffUOffPh1", L"���������� ���� ��� ���������� ���������� � ���� 1");  
  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_OFF_PHASE2, L"IOffUOffPh2", L"���������� ���� ��� ���������� ���������� � ���� 2");  
  lbmAdd(TYPE_EVENT_CURRENT_OFF_WITH_VOLTAGE_OFF_PHASE3, L"IOffUOffPh3", L"���������� ���� ��� ���������� ���������� � ���� 3");


  lbmAdd(TYPE_EVENT_CURRENT_DIRECTION_CHANGE_PHASE1, L"IDirPh1", L"��������� ����������� ���� � ���� 1");
  lbmAdd(TYPE_EVENT_CURRENT_DIRECTION_CHANGE_PHASE2, L"IDirPh2", L"��������� ����������� ���� � ���� 2");
  lbmAdd(TYPE_EVENT_CURRENT_DIRECTION_CHANGE_PHASE3, L"IDirPh3", L"��������� ����������� ���� � ���� 3");

  lbmAdd(TYPE_EVENT_DATE_TIME_CHANGE, L"Date", L"��������� ������� � ����");
  lbmAdd(TYPE_EVENT_COVER_OFF, L"CoverOff", L"������ ������ ��������");
  lbmAdd(TYPE_EVENT_COVER_ON, L"CoverOn", L"��������� ������ ��������");
  lbmAdd(TYPE_EVENT_TERMINAL_COVER_OFF, L"CoverCOff", L"������ ������ �������� �������");
  lbmAdd(TYPE_EVENT_TERMINAL_COVER_ON, L"CoverCOn", L"��������� ������ �������� �������");
  lbmAdd(TYPE_EVENT_ENERGY_INTERVALS_RESET, L"ClrPrPw", L"������� �������� ���������");
  lbmAdd(TYPE_EVENT_MAX_POWERS_RESET, L"ClrMxPw", L"������� ���������� ���������");
  lbmAdd(TYPE_EVENT_RATE_SCHEDULES_CHANGE, L"SetTarif", L"��������� ��������� ����������");
  lbmAdd(TYPE_EVENT_POWER_SCHEDULES_CHANGE, L"SetMxPw", L"��������� ���������� ���������� ���������");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS, L"BeginOvLmtPw", L"������ ���������� ������ ��������");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS, L"EndOvLmtPw", L"��������� ���������� ������ ��������");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE1, L"BeginOvLmtEnT1", L"������ ���������� ������ ������� �� ������ 1");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE1, L"EndOvLmtEnT1", L"��������� ���������� ������ ������� �� ������ 1");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE2, L"BeginOvLmtEnT2", L"������ ���������� ������ ������� �� ������ 2");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE2, L"EndOvLmtEnT2", L"��������� ���������� ������ ������� �� ������ 2");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE3, L"BeginOvLmtEnT3", L"������ ���������� ������ ������� �� ������ 3");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE3, L"EndOvLmtEnT3", L"��������� ���������� ������ ������� �� ������ 3");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE4, L"BeginOvLmtEnT4", L"������ ���������� ������ ������� �� ������ 4");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE4, L"EndOvLmtEnT4", L"��������� ���������� ������ ������� �� ������ 4");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE5, L"BeginOvLmtEnT5", L"������ ���������� ������ ������� �� ������ 5");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE5, L"EndOvLmtEnT5", L"��������� ���������� ������ ������� �� ������ 5");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE6, L"BeginOvLmtEnT6", L"������ ���������� ������ ������� �� ������ 6");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE6, L"EndOvLmtEnT6", L"��������� ���������� ������ ������� �� ������ 6");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE7, L"BeginOvLmtEnT7", L"������ ���������� ������ ������� �� ������ 7");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE7, L"EndOvLmtEnT7", L"��������� ���������� ������ ������� �� ������ 7");
  lbmAdd(TYPE_EVENT_BEGIN_OF_POWER_EXCESS_RATE8, L"BeginOvLmtEnT8", L"������ ���������� ������ ������� �� ������ 8");
  lbmAdd(TYPE_EVENT_END_OF_POWER_EXCESS_RATE8, L"EndOvLmtEnT8", L"��������� ���������� ������ ������� �� ������ 8");
  lbmAdd(TYPE_EVENT_BEGIN_MAGN_FIELD, L"BeginMagnet", L"������ ����������� ��������");
  lbmAdd(TYPE_EVENT_END_MAGN_FIELD, L"EndMagnet", L"��������� ����������� ��������");
}

ns_Jour::JOUR_FILTER* GetLinkFilter(teEventTypes evType)
{
  if (ns_Jour::g_rowsFilter.empty())
  {
    ASSERT(false);
    return nullptr;
  }

  for (size_t i = 0; i < ns_Jour::g_rowsFilter.size(); ++i)
  {
    if (ns_Jour::g_rowsFilter.at(i).evType == evType)
      return &ns_Jour::g_rowsFilter[i];
  }

  ASSERT(false);
  return nullptr;
}

CView_EventJournal::CView_EventJournal()
  : CCommonViewForm(CView_EventJournal::IDD, L"CView_EventJournal")
  , m_lstEvJrFilter(&m_plstEvJrFilterEx, dynamic_cast<CCommonViewDealer*>(this))
  , m_lstEvJr(&m_plstEvJrEx, dynamic_cast<CCommonViewDealer*>(this))
{
}

CView_EventJournal::~CView_EventJournal()
{
  if (m_plstEvJrFilterEx)
  {
    m_plstEvJrFilterEx->Destroy();
    delete m_plstEvJrFilterEx;
  }

  if (m_plstEvJrEx)
  {
    m_plstEvJrEx->Destroy();
    delete m_plstEvJrEx;
  }

  //delete m_pImageList;
}

BEGIN_MESSAGE_MAP(CView_EventJournal, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_LOAD_JOURNAL_FROM_DEVICE, &CView_EventJournal::OnBnClickedBtnLoadJourFromDevice)
END_MESSAGE_MAP()

void CView_EventJournal::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_LST_EVENT_JOURNAL_FILTER, m_lstEvJrFilter);
  DDX_Control(pDX, IDC_LST_EVENT_JOUNAL, m_lstEvJr);
  DDX_Control(pDX, IDC_STC_JOURNAL_FILTER, m_stcEvJrFilter);
  DDX_Control(pDX, IDC_STC_JOURNAL, m_stcEvJr);
  DDX_Control(pDX, IDC_BTN_LOAD_JOURNAL_FROM_DEVICE, m_btnLoadJourFromDevice);
}

void CView_EventJournal::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  // Filter

  m_lstEvJrFilter.ShowHorizScroll(false);
  m_lstEvJrFilter.EnableInterlacedColorScheme(true);

  m_plstEvJrFilterEx = new �Ai_ListCtrlEx();
  m_plstEvJrFilterEx->SetList(&m_lstEvJrFilter, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = true;
  m_plstEvJrFilterEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstEvJrFilter.SetExtendedStyle(m_lstEvJrFilter.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  ns_Jour::g_colsFilter.nCheck = m_plstEvJrFilterEx->SetColumnEx(L"���.", LVCFMT_LEFT);
  m_plstEvJrFilterEx->SetColumnWidthStyle(ns_Jour::g_colsFilter.nCheck, CS_WIDTH_FIX, 40);
  ns_Jour::g_colsFilter.nType = m_plstEvJrFilterEx->SetColumnEx(L"�������", LVCFMT_LEFT);
  m_plstEvJrFilterEx->SetColumnWidthStyle(ns_Jour::g_colsFilter.nType, CS_WIDTH_FIX, 370);
  m_plstEvJrFilterEx->MatchColumns();

  int nNodeIdx = m_plstEvJrFilterEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  CreateFilter();
  vector<CString> textOfCells(2);
  for (auto i : ns_Jour::g_rowsFilter)
  {
    textOfCells[1] = i.sName;
    m_plstEvJrFilterEx->SetChild(nNodeIdx, &textOfCells);
  }

  m_plstEvJrFilterEx->UpdateView();

  TakeValueFromXmlTree();

  int j = -1;
  int nRow_inList = 1;
  for (auto i : ns_Jour::g_rowsFilter)
  {
    m_lstEvJrFilter.SetCheck(++j, i.nCheck);
    COLORREF color = (i.nCheck == 1) ? COLOR_LCE_BLACK : COLOR_LCE_GRAY;
    m_plstEvJrFilterEx->SetCellTextColor(nRow_inList, ns_Jour::g_colsFilter.nType, color);
    ++nRow_inList;
  }

  m_plstEvJrFilterEx->UpdateView();
  m_lstEvJrFilter.CtrlWasCreated();

  // Journal

  m_lstEvJr.ShowHorizScroll(false);
  m_lstEvJr.EnableInterlacedColorScheme(true);

  m_plstEvJrEx = new �Ai_ListCtrlEx();
  m_plstEvJrEx->SetList(&m_lstEvJr, m_hWnd);
  lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bCheckBoxesInFirstColumn = false;
  m_plstEvJrEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstEvJr.SetExtendedStyle(m_lstEvJr.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_plstEvJrEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  m_plstEvJrEx->SetColumnEx(L"", LVCFMT_LEFT);
  ns_Jour::g_cols.nDate = m_plstEvJrEx->SetColumnEx(L"����", LVCFMT_CENTER);
  m_plstEvJrEx->SetColumnWidthStyle(ns_Jour::g_cols.nDate, CS_WIDTH_FIX, 90);
  ns_Jour::g_cols.nTime = m_plstEvJrEx->SetColumnEx(L"�����", LVCFMT_CENTER);
  m_plstEvJrEx->SetColumnWidthStyle(ns_Jour::g_cols.nTime, CS_WIDTH_FIX, 90);
  ns_Jour::g_cols.nName = m_plstEvJrEx->SetColumnEx(L"�������", LVCFMT_LEFT);
  m_plstEvJrEx->SetColumnWidthStyle(ns_Jour::g_cols.nName, CS_WIDTH_FIX, 600);
  m_plstEvJrEx->MatchColumns();

  nNodeIdx = m_plstEvJrEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  ns_Jour::g_nComnSignificantCell = ns_Jour::g_cols.nDate;
  m_plstEvJrEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_Jour::g_nComnSignificantCell);

  vector<CString> textOfCells2(4);  
  textOfCells2[1] = IDS_EMPTY_CELL_TEXT;
  // ��� ������ ������
  nRow_inList = m_plstEvJrEx->SetChild(nNodeIdx, &textOfCells2);
  // ��. �������� "Descr. 1" � ��������� AppDescription.txt
  m_plstEvJrEx->SetCellHideText(nRow_inList, ns_Jour::g_nComnSignificantCell);

  int nMaxEventTypes = static_cast<int>(ns_Jour::g_rowsFilter.size());
  int nMaxEventsInType = 400;
  ns_Jour::MAX_EVENT_JOUNAL = nMaxEventTypes * nMaxEventsInType;
  // ... ��� ��������� �����
  m_plstEvJrEx->SetEmptyChild(ns_Jour::MAX_EVENT_JOUNAL);

  m_plstEvJrEx->UpdateView();
  m_lstEvJr.CtrlWasCreated();

  m_stcEvJrFilter.SetBkColor(COLOR_LIGHT_BLUE);
  m_stcEvJr.SetBkColor(COLOR_LIGHT_BLUE);

  AddCtrlForPositioning(&m_stcEvJr, movbehConstTopMarginAndVertSizes, movbehConstLeftAndRigthMargins);
  AddCtrlForPositioning(&m_lstEvJrFilter, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);
  AddCtrlForPositioning(&m_lstEvJr, movbehConstTopAndBottomMargins, movbehConstLeftAndRigthMargins);
  
  FillInByFilter();

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  WatchForCtrState(&m_btnLoadJourFromDevice, rightCommon);

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_EventJournal::BeforeDestroyClass()
{
}

void CView_EventJournal::OnSize_()
{
}



void CView_EventJournal::OnDraw(CDC* pDC)
{
  return;
}

bool CView_EventJournal::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_EventJournal::AssertValid() const
{	
  CCommonViewForm::AssertValid();
}

void CView_EventJournal::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);  
}
#endif //_DEBUG

void CView_EventJournal::GetCutOfMeanings()
{
  int j = -1;
  for (auto& i : ns_Jour::g_rowsFilter)	  
  {
    BOOL bCheck = m_lstEvJrFilter.GetCheck(++j);
    i.nCheck = bCheck ? 1 : 0;
  }
}

void CView_EventJournal::OnLvnItemchangedLst(CListCtrl* pCtrl, int nRow_inView, bool bSelected)
{
  if (pCtrl->m_hWnd == m_lstEvJr.m_hWnd)
    return;

  int j = -1;
  for (auto i : ns_Jour::g_rowsFilter)
  {
    BOOL bCheck = m_lstEvJrFilter.GetCheck(++j);
    int nRow_inList = j + 1;
    COLORREF color = bCheck ? COLOR_LCE_BLACK : COLOR_LCE_GRAY;
    m_plstEvJrFilterEx->SetCellTextColor(nRow_inList, ns_Jour::g_colsFilter.nType, color);
  }

  m_plstEvJrFilterEx->UpdateView();

  GetCutOfMeanings();
  PutValueToXmlTree();

  ClearRows();
  FillInByFilter();
}

void CView_EventJournal::TakeValueFromXmlTree()
{
  CParamsTreeNode* pInlaysNode = GetXmlNode();
  CString sTag = ns_Jour::g_xTags.sEvJour;
  m_pEvJourNode = pInlaysNode->FindFirstNode(sTag);
  if (!m_pEvJourNode)
  {
    m_pEvJourNode = pInlaysNode->AddNode(sTag);
    return;
  }

  for (size_t j = 0; j < ns_Jour::g_rowsFilter.size(); ++j)
  {
    sTag.Format(L"%s%.2d", ns_Jour::g_xTags.sFilter, j + 1);
    CParamsTreeNode* pFilter = m_pEvJourNode->FindFirstNode(sTag);
    if (pFilter) // ���� ����� ������ � ����������� �� ����������, �� ��������� ��������� �� ���������
    {
      CString sFilter = pFilter->GetAttributeValue(ns_Jour::g_xTags.sDataSet);
      vector<CString> strs;
      CAi_Str::CutString(sFilter, L";", &strs);
      if (strs.size() == 2)
      {
        CString sFilterName = strs[0];
        CString sCheck = strs[1];
        int nCheck = (sCheck == L"0") ? 0 : 1;
        for (auto& i : ns_Jour::g_rowsFilter)
        {
          if (i.sXmlId == sFilterName)
          {
            i.nCheck = nCheck;
            break;
          }
        }
      }
    }
  }
}


void CView_EventJournal::PutValueToXmlTree()
{
  m_pEvJourNode->DeleteAllNodes();
  for (size_t j = 0; j < ns_Jour::g_rowsFilter.size(); ++j)
  {
    CString sTag;
    sTag.Format(L"%s%.2d", ns_Jour::g_xTags.sFilter, j + 1);
    CParamsTreeNode* pFilter = m_pEvJourNode->AddNode(sTag);
    if (pFilter)
    {
      CString s = ns_Jour::g_rowsFilter.at(j).sXmlId + CString(L";") +
        ((ns_Jour::g_rowsFilter.at(j).nCheck == 1) ? L"1" : L"0");
      pFilter->AddAttribute(ns_Jour::g_xTags.sDataSet, s);
    }
  }
  UpdateXmlBackupFile();
}

void CView_EventJournal::ClearRows()
{
  int nCntSeted = m_plstEvJrEx->GetSetedCountOfChild(0);
  if (nCntSeted > 0)
  {
    auto lmbClearCells = [=](int nRow_inList)->void
    {
      CString s = L"";
      m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_cols.nTime, s);
      m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_cols.nName, s);
    };

    // ��� ������ ������
    int nRow_inList = 1;
    lmbClearCells(nRow_inList);
    m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_cols.nDate, IDS_EMPTY_CELL_TEXT);
    m_plstEvJrEx->SetCellHideText(nRow_inList, ns_Jour::g_nComnSignificantCell);	
    //// ��� ��������� �����
    for (int j = 1; j < ns_Jour::MAX_EVENT_JOUNAL; ++j)
    {
      nRow_inList = j + 1;
      if (!m_plstEvJrEx->IsRowVisible(nRow_inList))
        break;

      int nRow_inView = j;
      m_plstEvJrEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_Jour::g_nComnSignificantCell, -1);
      lmbClearCells(nRow_inList);
    }
  }
	 m_plstEvJrEx->UpdateView();
}

void CView_EventJournal::FillInByFilter()
{
  int j = 0;
  for (auto i : ns_Jour::g_data)
  {
    if (i.pFltr->nCheck)
    {
      int nRow_inList = j + 1;
      if (j == 0)
      {
        m_plstEvJrEx->SetCellShowText(nRow_inList, ns_Jour::g_nComnSignificantCell);
        m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_nComnSignificantCell, i.sDate);
      }
      else
      {
        int nNodeRow_inView = 0;
        m_plstEvJrEx->InsertTextToChild(nNodeRow_inView, ns_Jour::g_nComnSignificantCell, i.sDate);
      }
      m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_cols.nTime, i.sTime);
      m_plstEvJrEx->SetCellText(nRow_inList, ns_Jour::g_cols.nName, i.pFltr->sName);
      ++j;
    }
  }
  m_plstEvJrEx->UpdateView();
}

void CView_EventJournal::ParseReceivedData(teFrameTypes ft)
{
  unsigned int nAddress = 0;
  if (GetDataFrameAddress(ft, nAddress))
  {
    /*


    8B00(01-01-18,01:00,1,0;
            01-01-2001,01:30,3,0;
            01-01-2001,02:00,5,0;
            01-01-2001,04:00,1,0;)
    8B01(01-01-2001,04:30,2,0;
            01-01-2001,05:00,8,0;
            01-01-2001,06:30,6,0;
            01-01-2001,07:00,10,0;)
    -//-
    8B0F(01-01-2001,17:30,4,0;
            01-01-2001,18:00,7,0;
            01-01-2001,18:30,12,0;
            01-01-2001,19:30,15,0;)
    */
    int nExtendedAddresses = GetExtendedAddresses(ft) + 1; // 1 - itself
    for (int e = 0; e < nExtendedAddresses; ++e)
    {
      varMeaning_t mData = FindReceivedData(nAddress + e);
      if (mData == L"?")
      {
        m_bParseResult = false;
        break;
      }

      mData.TrimRight(L";");
      vector<CString> data;
      if (CAi_Str::CutString(mData, L";", &data))
      {
        for (size_t k = 0; k < data.size(); ++k)
        {
          vector<CString> eventItem;
          if (CAi_Str::CutString(data.at(k), L",", &eventItem)
              && eventItem.size() == 4)
          {
            ns_Jour::DATA data;
            data.sDate = eventItem[0];
            data.sTime = eventItem[1];
            CString sId = eventItem[2];
            data.sReserved = eventItem[3];
            
            // form filter link 

            int nId = CAi_Str::ToInt(sId);
            int nNone = static_cast<int>(TYPE_EVENT_NONE);
            if (nId == nNone)
            {
              m_bParseResult = false;
              continue;
            }

            int nStart = static_cast<int>(TYPE_EVENT_POWER_ON);
            int nEnd = static_cast<int>(TYPE_EVENT_LAST);
            for (int j = nStart; j < nEnd; ++j)
            {
              if (nId == j)
              {
                teEventTypes evType = static_cast<teEventTypes>(j);
                data.pFltr = GetLinkFilter(evType);
              }
            }

            if (data.pFltr)
              ns_Jour::g_data.push_back(data);
          }
        }
      }
    }
  }
}

void CView_EventJournal::OnBnClickedBtnLoadJourFromDevice()
{
  ns_Jour::g_data.clear();
  ns_Jour::g_nRow_from = 0;
  m_bParseResult = true;
  ClearRows();

  // ������� ������������ ��������� � ��

  FormListForClearReceivedData(FRAME_TYPE_EVENT_CNT);
  int nStart = static_cast<int>(FRAME_TYPE_EVENT_001);
  int nEnd = static_cast<int>(FRAME_TYPE_EVENT_160);

  for (int i = nStart; i <= nEnd; ++i)
    FormListForClearReceivedData(static_cast<teFrameTypes>(i));
  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_EventJournal::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);

  bool bOnceCmd = true;
  SetParamTypeForRead(FRAME_TYPE_EVENT_CNT, bOnceCmd);

  BeginFormingReadPack(false);
}

void CView_EventJournal::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_EventJournal::CheckMeaningsViewByConnectionStatus()
{
  vector<int> inactiveTextInColumns;
  if (!IsConnection())
  {
    inactiveTextInColumns.push_back(ns_Jour::g_cols.nDate);
    inactiveTextInColumns.push_back(ns_Jour::g_cols.nTime);
    inactiveTextInColumns.push_back(ns_Jour::g_cols.nName);
  }
  m_lstEvJr.SetInactiveTextInColumns(&inactiveTextInColumns);
  m_plstEvJrEx->UpdateView();
}

void CView_EventJournal::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������� �������!", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_EVENT_CNT)
  {
    varMeaning_t m = FindReceivedData(ft);
    int nCount = CAi_Str::ToInt(m);
    ns_Jour::g_nRow_from = max(nCount, 0); // ����� �� ������� �� �������
    ns_Jour::g_nRow_from = min(ns_Jour::g_nRow_from, ns_Jour::MAX_EVENT_JOUNAL); // ����� �� ������� �� ��������
    if (ns_Jour::g_nRow_from == 0)
    {
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������� �� �������� ������� � ������� �������!", MB_ICONINFORMATION);
      return;
    }

    teFrameTypes reqFirstItem = FRAME_TYPE_EVENT_001;
    int nNext = static_cast<int>(reqFirstItem);
    for (int i = 0; i < ns_Jour::g_nRow_from; ++i)
    {
      reqFirstItem = static_cast<teFrameTypes>(nNext);
      SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
      nNext += 1;
    }
    return;
  }

  if (ft >= FRAME_TYPE_EVENT_001
      && ft <= FRAME_TYPE_EVENT_160)
  {
    ParseReceivedData(ft);

    int ft1 = static_cast<int>(FRAME_TYPE_EVENT_001);
    int ftCurr = static_cast<int>(ft);

    if ((ftCurr - ft1 + 1) == ns_Jour::g_nRow_from)
    {
      SetWaitCursor_answer(false);
      FillInByFilter();
      if (m_bParseResult)
        AfxMessageBox(L"��� ������ ������� ������� ��������� �� ��������.\n������ � �������� ������!", MB_ICONERROR);
      else
        AfxMessageBox(L"��� ������ ������� ������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }
  }
}



