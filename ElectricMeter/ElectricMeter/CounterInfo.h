#pragma once
#include "Model_.h"
#include <string>

using std::string;
using mdl::teFrameTypes;
// ����� ������ �������, ��� ����� �������� �� ������� CCounterInfo � CProduction � ����� ���� �� ����. �� ������ ���� �������� � ����� �������
typedef enum : char { LE1, LE3 } eMeterType;
typedef enum : char  { P, D } eMeterCase;
typedef enum : char { ACC_05, ACC_10 } eMeterAccuracy;
typedef enum : char { ENERGY_A, ENERGY_AR } eMeterEnergyKind;
typedef enum : char { IFACE_O, IFACE_E4, IFACE_E2, IFACE_RF, IFACE_PL, IFACE_PLRF, IFACE_GSM, IFACE_MB, IFACE_ETH, IFACE_WF, IFACE_NO } eMeterIface;
typedef enum : char { OPT_B, OPT_S, OPT_C, OPT_P, OPT_M, OPT_MAX } eMeterOptions;
typedef enum : char { U1, U2, U3, U4 } eMeterNomU;
typedef enum : char { I1, I2, I3, I4, I5 } eMeterNomI;
typedef enum : unsigned char { BR_300, BR_600, BR_1200, BR_2400, BR_4800, BR_9600, BR_MAX } eComSpeed;
typedef enum : unsigned char  { PAR_NONE, PAR_ODD, PAR_EVEN, } eParity;
typedef enum : unsigned char  { BITS5, BITS6, BITS7, BITS8 } eComBits;
typedef enum : unsigned char  { IV_10, IV_20, IV_30, IV_40, IV_50, IV_70, IV_80, IV_90, IV_100 } eIval;
typedef enum : unsigned char  { PROTO_61107 } eProto;

#pragma pack (1)
static const int MAX_IFACES = 4;
typedef struct
{
	eMeterIface iFace;
	unsigned char ver;
} tiface;

typedef struct
{
	unsigned char day;
	unsigned char month;
	unsigned short year;
} tDate;



template <class typen> struct RTYPE
{
	typen type;
	int cID;
};

typedef struct
{
	eComSpeed speed;
	eParity parity;
	eComBits bits;
	eIval packetInterval;
	union
	{
		unsigned char comAddr[4];
		unsigned int comAddr32;
	};
	eProto protocol;
	
} sCommInfo;

#define IP_STR_LEN 24
#define APN_STR_LEN 16
typedef struct
{
	char aServerIP[IP_STR_LEN];
	char aDnsIP[IP_STR_LEN];
	char aApn[APN_STR_LEN];
	unsigned short ipPort;	
	char ussdNum[8];
} sgsmSettings;

struct   sCfg
{	
	eMeterType type;
	eMeterCase meter_case;
	eMeterAccuracy accuracy;
	eMeterEnergyKind meterEnergyKind;
	tiface  sIface[MAX_IFACES];
	bool options[OPT_MAX];
	eMeterNomU nomU;
	eMeterNomI nomI;
	int model;
	unsigned int serNum;
	tDate date;
	unsigned short fwVer;
	unsigned short hwVer;
	sCommInfo aComInfo[4];
	sgsmSettings gsmSett;
};
#pragma pack ()	

using mdl::teFrameTypes;

class CCounterInfo
{
public:
	static sCfg AsciiToCfg(CString m);
	static CString GetFieldNameFromeCfg(teFrameTypes teFrame, const sCfg& cfg);
	static CString ProduceCounterName(const sCfg& cfg);
	static CString CfgAsAscii(const sCfg& cfg);
	CCounterInfo();
	~CCounterInfo();


	template <class typen> struct INFOTYPE
	{
		typen type;
		string desc;
		bool isVer;// optional
		CString longDesc;
	};

	static const INFOTYPE <eMeterType> meterTypeInfo[];
	static const INFOTYPE <eMeterCase> meterCaseInfo[];
	static const INFOTYPE <eMeterAccuracy> meterAccuracyInfo[];
	static const INFOTYPE <eMeterEnergyKind> meterKindInfo[];
	static const INFOTYPE <eMeterIface> meterIfaceInfo[];
	static const INFOTYPE <eMeterOptions> meterOptionsInfo[];
	static const INFOTYPE <eMeterNomU> meterUInfo[];
	static const INFOTYPE <eMeterNomI> meterIInfo[];		
};

