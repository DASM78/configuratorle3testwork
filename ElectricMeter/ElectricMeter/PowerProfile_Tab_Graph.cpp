#include "StdAfx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Ai_Font.h"
#include "Ai_File.h"
#include "Defines.h"
#include "PowerProfileDataRow.h"
#include "View_PowerProfile.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define X_ZOOMS 20
#define Y_ZOOMS 10

namespace ns_TGr
{
  int g_xZooms[X_ZOOMS] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
  int g_yZooms[Y_ZOOMS] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  int g_nXZoomIdx = 0;
  int g_nYZoomIdx = 0;
  CString g_sCbxTxt = L"x";

  int g_nXBtnMargin = 2;
  int g_nYBtnMargin = 2;
  int g_nXBtnSpace = 2;
  int g_nBtnHeight = 20;
  int g_nBtnWidth = 36;
  int g_nCbxWidth = 45;

} // namespace ns_TGr

vector<POWPROF_DATA_ROW> gTest;

bool CView_PowerProfile::IsTestViewData()
{
  return !gTest.empty();
}

void CView_PowerProfile::ClearTestViewData()
{
  gTest.clear();
}

void FormVirtualData()
{
  gTest.clear();

  CString sFileName = L".\\GraphTestData.txt";
  #ifdef _DEBUG
  sFileName = L"..\\Debug\\GraphTestData.txt";
  #endif
  if (CAi_File::IsFileExist(sFileName))
  {
    CString sData;
    vector<CString> rows;
    if (CAi_File::GetFileLength(sFileName) > 0
        && CAi_File::ReadFileToString(sFileName, &sData)
        && CAi_Str::CutString(sData, L"\r\n", &rows)
        && !rows.empty())
    {
      for (auto& row : rows)
      {
        vector<CString> data;
        if (CAi_Str::CutString(row, L";", &data)
            && !data.empty()
            && data.size() == 8)
        {
          for (auto& v : data)
          {
            v.TrimLeft(L"\t");
            v.TrimRight(L"\t");
          }
          gTest.push_back(POWPROF_DATA_ROW(
            data[0],
            data[1],
            data[2],
            data[3],
            data[4],
            data[5],
            data[6],
            data[7]
            ));
        }
      }
    }
  }
  /*
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"1", L"0", L"1", L"1", L"1", L"00:30", L"01-01-2017")); // 1
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"0", L"16", L"10", L"2", L"01:00", L"01-01-2017")); // 2
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"10", L"0", L"16", L"1", L"3", L"01:30", L"01-01-2017")); // 3
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"1", L"16", L"11", L"1", L"02:00", L"01-01-2017")); // 4
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"1", L"1", L"16", L"11", L"1", L"02:30", L"01-01-2017")); // 5
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"1", L"16", L"11", L"2", L"03:00", L"01-01-2017")); // 6
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"1", L"16", L"11", L"3", L"03:30", L"01-01-2017")); // 7
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"1", L"10", L"16", L"11", L"4", L"04:00", L"01-01-2017")); // 8
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"11", L"14", L"04:30", L"01-01-2017")); // 9
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"10", L"2", L"7", L"11", L"1", L"05:00", L"01-01-2017")); // 10
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"3", L"8", L"1", L"1", L"05:30", L"01-01-2017")); // 11
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"1", L"4", L"9", L"1", L"1", L"06:00", L"01-01-2017")); // 12
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"5", L"16", L"0", L"1", L"06:30", L"01-01-2017")); // 13
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"0", L"16", L"1", L"2", L"07:00", L"01-01-2017")); // 14
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"0", L"16", L"1", L"3", L"07:30", L"01-01-2017")); // 15
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"1", L"1", L"08:00", L"01-01-2017")); // 16
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"1", L"1", L"08:30", L"01-01-2017")); // 17
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"20", L"2", L"09:00", L"01-01-2017")); // 18
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"1", L"16", L"18", L"3", L"9:30", L"01-01-2017")); // 19
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"10", L"16", L"17", L"4", L"10:00", L"01-01-2017")); // 20
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"16", L"14", L"10:30", L"01-01-2017")); // 21
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"2", L"7", L"13", L"1", L"11:00", L"01-01-2017")); // 22
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"3", L"8", L"13", L"13", L"11:30", L"01-01-2017")); // 23
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"5", L"9", L"13", L"13", L"12:00", L"01-01-2017")); // 24
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"5", L"16", L"13", L"1", L"23:29", L"01-01-2017")); // 25
  gTest.push_back(POWPROF_DATA_ROW(L"13", L"1", L"6", L"16", L"1", L"1", L"13:10", L"03-01-2017")); // 26
  gTest.push_back(POWPROF_DATA_ROW(L"13", L"1", L"16", L"1", L"1", L"1", L"13:10", L"03-01-2017")); // 27
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"1", L"0", L"1", L"1", L"1", L"00:30", L"04-03-2017")); // 1
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"0", L"16", L"10", L"2", L"01:00", L"04-03-2017")); // 2
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"10", L"0", L"16", L"1", L"3", L"01:30", L"04-03-2017")); // 3
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"1", L"16", L"11", L"1", L"02:00", L"04-03-2017")); // 4
  gTest.push_back(POWPROF_DATA_ROW(L"0", L"1", L"1", L"16", L"11", L"1", L"02:30", L"04-03-2017")); // 5
  gTest.push_back(POWPROF_DATA_ROW(L"10", L"1", L"1", L"16", L"11", L"2", L"03:00", L"04-03-2017")); // 6
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"1", L"16", L"11", L"3", L"03:30", L"04-03-2017")); // 7
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"1", L"10", L"16", L"11", L"4", L"04:00", L"04-03-2017")); // 8
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"11", L"14", L"04:30", L"04-03-2017")); // 9
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"10", L"2", L"7", L"11", L"1", L"05:00", L"04-03-2017")); // 10
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"3", L"8", L"1", L"1", L"05:30", L"04-03-2017")); // 11
  gTest.push_back(POWPROF_DATA_ROW(L"15", L"1", L"4", L"9", L"1", L"1", L"06:00", L"04-03-2017")); // 12
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"5", L"16", L"0", L"1", L"06:30", L"04-03-2017")); // 13
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"0", L"16", L"1", L"2", L"07:00", L"04-03-2017")); // 14
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"0", L"16", L"1", L"3", L"07:30", L"04-03-2017")); // 15
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"1", L"1", L"08:00", L"04-03-2017")); // 16
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"1", L"1", L"08:30", L"04-03-2017")); // 17
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"20", L"2", L"09:00", L"04-03-2017")); // 18
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"1", L"16", L"18", L"3", L"9:30", L"04-03-2017")); // 19
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"10", L"16", L"17", L"4", L"10:00", L"04-03-2017")); // 20
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"1", L"16", L"16", L"14", L"10:30", L"04-03-2017")); // 21
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"10", L"2", L"7", L"13", L"1", L"11:00", L"04-03-2017")); // 22
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"3", L"8", L"13", L"13", L"11:30", L"04-03-2017")); // 23
  gTest.push_back(POWPROF_DATA_ROW(L"5", L"1", L"5", L"9", L"13", L"13", L"12:00", L"04-03-2017")); // 24
  */
}

void CView_PowerProfile::CreateView_InGraphView()
{
  m_pGraphView = new CPowerProfileGraphs();
  m_pGraphView->SetDataObj(&m_grhData);

  CRect rSize = CRect(0, 0, 0, 0);
  const DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_HSCROLL | WS_VSCROLL;
  m_pGraphView->Create(NULL, NULL, dwStyle, CRect(0, 0, 0, 0), &m_tabs, IDC_TAB_POWER_PROFILE_GRAPH);

  auto lmbNewButton = [&](CMFCButton** pBtn, int nID, int nIDB, int nIDB_HL)
  {
    *pBtn = new CMFCButton();
    (*pBtn)->Create(L"", WS_CHILD | WS_VISIBLE, CRect(0, 0, 0, 0), &m_tabs, nID);
    (*pBtn)->m_nFlatStyle = CMFCButton::BUTTONSTYLE_NOBORDERS;
    (*pBtn)->m_bDrawFocus = FALSE;
    (*pBtn)->SetImage(nIDB, nIDB_HL);
  };

  lmbNewButton(&m_pbtnPlusH, ID_BTN_ZOOM_H_PLUS, IDB_ZOOM_H_PLUS, IDB_ZOOM_H_PLUS_HL);
  lmbNewButton(&m_pbtnMinusH, ID_BTN_ZOOM_H_MINUS, IDB_ZOOM_H_MINUS, IDB_ZOOM_H_MINUS_HL);
  lmbNewButton(&m_pbtnPlusV, ID_BTN_ZOOM_V_PLUS, IDB_ZOOM_V_PLUS, IDB_ZOOM_V_PLUS_HL);
  lmbNewButton(&m_pbtnMinusV, ID_BTN_ZOOM_V_MINUS, IDB_ZOOM_V_MINUS, IDB_ZOOM_V_MINUS_HL);

  ASSERT(ns_TGr::g_nXZoomIdx >= 0);
  ASSERT(ns_TGr::g_nXZoomIdx < X_ZOOMS);
  ASSERT(ns_TGr::g_nYZoomIdx >= 0);
  ASSERT(ns_TGr::g_nYZoomIdx < Y_ZOOMS);

  CAi_Font::CreateFont_(this, m_fntCmbx, L"Verdana", FW_MEDIUM, PROOF_QUALITY, 12);

  auto lmbNewComboBox = [&](CAi_CmbBx** pCbx, int nID, int zooms[], int cnt, int& zidx)
  {
    *pCbx = new CAi_CmbBx();
    (*pCbx)->Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), &m_tabs, nID);
    for (int i = 0; i < cnt; ++i)
      (*pCbx)->AddString(ns_TGr::g_sCbxTxt + CAi_Str::ToString(zooms[i]));
    (*pCbx)->SelectItem(ns_TGr::g_sCbxTxt + CAi_Str::ToString(zooms[zidx]));
    (*pCbx)->SetFont(&m_fntCmbx);
  };

  lmbNewComboBox(&m_pcbxZoomH, ID_CBX_ZOOM_H, ns_TGr::g_xZooms, X_ZOOMS, ns_TGr::g_nXZoomIdx);
  lmbNewComboBox(&m_pcbxZoomV, ID_CBX_ZOOM_V, ns_TGr::g_yZooms, Y_ZOOMS, ns_TGr::g_nYZoomIdx);

  m_pGraphView->SetColorForAR(&m_stcA_color, &m_stcR_plus_color, &m_stcR_minus_color);

  FormVirtualData();
  if (!gTest.empty())
    m_grhData.AnalisysData(&gTest);

  SetZoomX();
  SetZoomY();
}

void CView_PowerProfile::OnSize_Tab_Graph()
{
  if (m_pbtnPlusH)
  {
    CRect rCl = CRect(0, 0, 0, 0);
    m_tabs.GetClientRect(rCl);
    int x = rCl.right - ns_TGr::g_nBtnWidth - ns_TGr::g_nXBtnMargin;
    int y = rCl.top + ns_TGr::g_nYBtnMargin;

    m_pbtnPlusH->MoveWindow(x, y, ns_TGr::g_nBtnWidth, ns_TGr::g_nBtnHeight);
    x -= (ns_TGr::g_nXBtnMargin + ns_TGr::g_nCbxWidth);
    m_pcbxZoomH->MoveWindow(x, y, ns_TGr::g_nCbxWidth, ns_TGr::g_nBtnHeight);
    x -= (ns_TGr::g_nXBtnMargin + ns_TGr::g_nBtnWidth);
    m_pbtnMinusH->MoveWindow(x, y, ns_TGr::g_nBtnWidth, ns_TGr::g_nBtnHeight);

    x -= ((ns_TGr::g_nXBtnMargin * 4) + ns_TGr::g_nBtnWidth);
    m_pbtnPlusV->MoveWindow(x, y, ns_TGr::g_nBtnWidth, ns_TGr::g_nBtnHeight);
    x -= (ns_TGr::g_nXBtnMargin + ns_TGr::g_nCbxWidth);
    m_pcbxZoomV->MoveWindow(x, y, ns_TGr::g_nCbxWidth, ns_TGr::g_nBtnHeight);
    x -= (ns_TGr::g_nXBtnMargin + ns_TGr::g_nBtnWidth);
    m_pbtnMinusV->MoveWindow(x, y, ns_TGr::g_nBtnWidth, ns_TGr::g_nBtnHeight);
  }
}

void CView_PowerProfile::CheckView_InGraphView()
{
  ASSERT(m_pGraphView);
  m_pGraphView->SetCurrView((m_currPPPV_Energy == ecppvEnergy) ? true : false,
                      (m_currPPPV_A == ecppv2A_on) ? true : false,
                      (m_currPPPV_R_plus == ecppv2R_plus_on) ? true : false,
                      (m_currPPPV_R_minus == ecppv2R_minus_on) ? true : false);
  m_pGraphView->Invalidate();
}

void CView_PowerProfile::CheckMeaningsViewByConnectionStatus_InGraphView()
{
  if (!IsConnection())
  {
    ASSERT(m_pGraphView);
    m_pGraphView->ClearCurves();
  }
}

void CView_PowerProfile::CreateGraph_InGraphView(vector<POWPROF_DATA_ROW>* pData)
{
  m_grhData.AnalisysData(pData);
  ASSERT(m_pGraphView);
  m_pGraphView->Invalidate();
}

CString CView_PowerProfile::GetZoomX()
{
  return CAi_Str::ToString(ns_TGr::g_nXZoomIdx);
}

void CView_PowerProfile::SetZoomX(CString s)
{
  ns_TGr::g_nXZoomIdx = CAi_Str::ToInt(s);
  ns_TGr::g_nXZoomIdx = max(0, ns_TGr::g_nXZoomIdx); // ����� �� ���� ���� 0
  ns_TGr::g_nXZoomIdx = min(X_ZOOMS - 1, ns_TGr::g_nXZoomIdx); // ����� �� ���� ���� X_ZOOMS - 1
  SetZoomX();
}

void CView_PowerProfile::SetZoomX()
{
  m_pcbxZoomH->SelectItem(ns_TGr::g_sCbxTxt +
                          CAi_Str::ToString(ns_TGr::g_xZooms[ns_TGr::g_nXZoomIdx]));

  m_pGraphView->SetZoomX(ns_TGr::g_xZooms[ns_TGr::g_nXZoomIdx]);
}

CString CView_PowerProfile::GetZoomY()
{
  return CAi_Str::ToString(ns_TGr::g_nYZoomIdx);
}

void CView_PowerProfile::SetZoomY(CString s)
{
  ns_TGr::g_nYZoomIdx = CAi_Str::ToInt(s);
  ns_TGr::g_nYZoomIdx = max(0, ns_TGr::g_nYZoomIdx); // ����� �� ���� ���� 0
  ns_TGr::g_nYZoomIdx = min(Y_ZOOMS - 1, ns_TGr::g_nYZoomIdx); // ����� �� ���� ���� Y_ZOOMS - 1
  SetZoomY();
}

void CView_PowerProfile::SetZoomY()
{
  m_pcbxZoomV->SelectItem(ns_TGr::g_sCbxTxt +
                          CAi_Str::ToString(ns_TGr::g_yZooms[ns_TGr::g_nYZoomIdx]));

  m_pGraphView->SetZoomY(ns_TGr::g_yZooms[ns_TGr::g_nYZoomIdx]);
}

void CTabCtrlOvrdZ::OnBnClickedZoomHPlus()
{
  m_pPP->OnBnClickedZoomHPlus();
}
void CTabCtrlOvrdZ::OnBnClickedZoomHMinus()
{
  m_pPP->OnBnClickedZoomHMinus();
}
void CTabCtrlOvrdZ::OnBnClickedZoomVPlus()
{
  m_pPP->OnBnClickedZoomVPlus();
}
void CTabCtrlOvrdZ::OnBnClickedZoomVMinus()
{
  m_pPP->OnBnClickedZoomVMinus();
}

void CTabCtrlOvrdZ::OnCbxSelchangeZoomH()
{
  m_pPP->OnCbxSelchangeZoomH();
}

void CTabCtrlOvrdZ::OnCbxSelchangeZoomV()
{
  m_pPP->OnCbxSelchangeZoomV();
}

void CView_PowerProfile::OnBnClickedZoomHPlus()
{
  ++ns_TGr::g_nXZoomIdx;
  if (ns_TGr::g_nXZoomIdx >= X_ZOOMS)
    ns_TGr::g_nXZoomIdx = 0;
  SetZoomX();
  PutValueToXmlTree();
}

void CView_PowerProfile::OnBnClickedZoomHMinus()
{
  --ns_TGr::g_nXZoomIdx;
  if (ns_TGr::g_nXZoomIdx < 0)
    ns_TGr::g_nXZoomIdx = X_ZOOMS - 1;
  SetZoomX();
  PutValueToXmlTree();
}

void CView_PowerProfile::OnBnClickedZoomVPlus()
{
  ++ns_TGr::g_nYZoomIdx;
  if (ns_TGr::g_nYZoomIdx >= Y_ZOOMS)
    ns_TGr::g_nYZoomIdx = 0;
  SetZoomY();
  PutValueToXmlTree();
}

void CView_PowerProfile::OnBnClickedZoomVMinus()
{
  --ns_TGr::g_nYZoomIdx;
  if (ns_TGr::g_nYZoomIdx < 0)
    ns_TGr::g_nYZoomIdx = Y_ZOOMS - 1;
  SetZoomY();
  PutValueToXmlTree();
}

void CView_PowerProfile::OnCbxSelchangeZoomH()
{
  CString sValue;
  m_pcbxZoomH->GetWindowText(sValue);
  sValue.TrimLeft(ns_TGr::g_sCbxTxt);
  int nValue = CAi_Str::ToInt(sValue);
  ns_TGr::g_nXZoomIdx = 0;
  for (auto& z : ns_TGr::g_xZooms)
  {
    if (z == nValue)
      break;
    ++ns_TGr::g_nXZoomIdx;
  }
  ASSERT(ns_TGr::g_nXZoomIdx < X_ZOOMS);
  if (ns_TGr::g_nXZoomIdx == X_ZOOMS)
    ns_TGr::g_nXZoomIdx = 0;
  SetZoomX();
  PutValueToXmlTree();
}

void CView_PowerProfile::OnCbxSelchangeZoomV()
{
  CString sValue;
  m_pcbxZoomV->GetWindowText(sValue);
  sValue.TrimLeft(ns_TGr::g_sCbxTxt);
  int nValue = CAi_Str::ToInt(sValue);
  ns_TGr::g_nYZoomIdx = 0;
  for (auto& z : ns_TGr::g_yZooms)
  {
    if (z == nValue)
      break;
    ++ns_TGr::g_nYZoomIdx;
  }
  ASSERT(ns_TGr::g_nYZoomIdx < Y_ZOOMS);
  if (ns_TGr::g_nYZoomIdx == Y_ZOOMS)
    ns_TGr::g_nYZoomIdx = 0;
  SetZoomY();
  PutValueToXmlTree();
}
