#include "StdAfx.h"
#include "Resource.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BOOL CMainFrame::CreateOutlookBar()
{
  CMFCOutlookBarTabCtrl::EnableAnimation();

  const int nInitialWidth = 250;
  const CString strCaption = _T("�������� ���������");

  if (!m_wndOutlookBar.Create(strCaption, this,
    CRect(0, 0, nInitialWidth, nInitialWidth),
    ID_VIEW_OUTLOOKBAR, WS_CHILD | WS_VISIBLE | CBRS_LEFT))
  {
    TRACE0("Failed to create outlook bar\n");
    return FALSE;      // fail to create
  }

  m_wndOutlookBar.EnableGripper(TRUE);

  m_pShortcutsBarContainer = DYNAMIC_DOWNCAST(CMFCOutlookBarTabCtrl, m_wndOutlookBar.GetUnderlyingWindow());
  if (m_pShortcutsBarContainer == nullptr)
  {
    TRACE0("Cannot get outlook bar container\n");
    return FALSE;
  }

  AddOutlookTab(ID_OUTLOOK_CONNECTION_TAB);
  AddOutlookTab(ID_OUTLOOK_DATA_TAB);
  AddOutlookTab(ID_OUTLOOK_DEVICE_SETTINGS_TAB);  

  return TRUE;
}

void CMainFrame::ShowOutlookTab(int nTabID)
{
  for (size_t i = 0; i < m_outlookTabs.size(); ++i)
  {
    if (m_outlookTabs[i]->GetID() == nTabID)
      m_pShortcutsBarContainer->SetActiveTab(i);
  }
}

void CMainFrame::SelectFirstRowInOutlookTab(int nTabID)
{
  for (size_t i = 0; i < m_outlookTabs.size(); ++i)
  {
    if (m_outlookTabs[i]->GetID() == nTabID)
    {
      m_outlookTabs[i]->SelectFirstRow();
      break;
    }
  }
}

void CMainFrame::SelectSpecRowInOutlookTab(int nTabID, int nInlayID)
{
  for (size_t i = 0; i < m_outlookTabs.size(); ++i)
  {
    if (m_outlookTabs[i]->GetID() == nTabID)
    {
      m_outlookTabs[i]->SelectSpecRow(nInlayID);
      break;
    }
  }
}

void CMainFrame::AddOutlookTab(int nTabID)
{
  CTabListCtrl* pList = new CTabListCtrl(this, nTabID);

  // CMFCListCtrl

  CRect r(0, 0, 0, 0);
  const DWORD dwStyle =
    WS_CHILD | 
    WS_VISIBLE | 
    LVS_REPORT | 
    LVS_NOCOLUMNHEADER |
    LVS_SINGLESEL |
    LVS_SHOWSELALWAYS;
  pList->CreateEx(0, dwStyle, r, &m_wndOutlookBar, nTabID);

  // Others

  CString sTabName;

  switch (nTabID)
  {
    case ID_OUTLOOK_CONNECTION_TAB:
      sTabName = L"�����������";
      break;

    case ID_OUTLOOK_DATA_TAB:
      sTabName = L"������ ��������";
      break;

    case ID_OUTLOOK_DEVICE_SETTINGS_TAB:
      sTabName = L"��������� ��������";
      break;

    default: ASSERT(false);     
  }

  m_pShortcutsBarContainer->AddTab(pList, sTabName, (UINT)-1, FALSE);
  m_outlookTabs.push_back(pList);

  // Set rows

  pList->InsertRowsInOutlookTabLists(&m_wndOutlookBar);
}

void CMainFrame::FreeOutlookTab()
{
  for (auto pTab : m_outlookTabs)
  {
    pTab->Free();
    delete pTab;
  }
  m_outlookTabs.clear();
}

void CMainFrame::OnViewOutlookBar()
{
  ShowPane(&m_wndOutlookBar, !(m_wndOutlookBar.IsVisible()), FALSE, TRUE);
  RecalcLayout();
}

void CMainFrame::OnUpdateViewOutlookBar(CCmdUI* pCmdUI)
{
  pCmdUI->SetCheck(m_wndOutlookBar.IsVisible());
}
 
LRESULT CMainFrame::OnChangeActiveTab(WPARAM wparam, LPARAM lparam)
{
  if (WasTabSwitchedInInlay())
    return FALSE;

  size_t nTabIdx = (size_t)wparam;
  if (nTabIdx < m_outlookTabs.size())
  {
    if (!m_outlookTabs[nTabIdx]->IsSelectedRow())
      m_outlookTabs[nTabIdx]->SelectFirstRow();
    else
      OnSelectRowInTab(0, m_outlookTabs[nTabIdx]->GetID());
  }

  return TRUE;
}

LRESULT CMainFrame::OnSelectRowInTab(WPARAM wParam, LPARAM lParam)
{
  int nTabID = lParam;

  for (size_t i = 0; i < m_outlookTabs.size(); ++i)
  {
    if (m_outlookTabs[i]->GetID() == nTabID)
    {
      SetCaptionBarText(m_outlookTabs[i]->m_dataSelectedItem.sDescr);
      SelectViewByTabItemID(m_outlookTabs[i]->m_dataSelectedItem.nItemID);
    }
  }

  return TRUE;
}

LRESULT CMainFrame::ResizeMsgFromCView(WPARAM wParam, LPARAM lParam)
{
  // ResizeOutlookTabs

  for (auto pTab : m_outlookTabs)
    pTab->Resize();

  // Resize native view

  CView* pActiveView = GetActiveView_();
  if (pActiveView)
  {
    CCommonViewForm* pView = dynamic_cast<CCommonViewForm*>(pActiveView);
    if (pView)
      pView->OnSize((void*)lParam);
  }

  return TRUE;
}
