#include "stdafx.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

LRESULT CMainFrame::UpdateXmlBackupFile(WPARAM wParam, LPARAM lParam)
{
  GetAppEnviron()->GetXmlSett()->CreateXmlFileFromTree_(CXmlAid_::xmlBackupFile);
  return TRUE;
}