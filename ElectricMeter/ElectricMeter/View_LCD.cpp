#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "LCD_data.h"
#include "Ai_Str.h"
#include "Typedefs.h"
#include "View_LCD.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_LCD
{

  struct PAGE_COLUMNS
  {
    int nPage = -1;
  }
  g_pg_cols;
  struct PAGE_ROWS
  {
    int nCyclIndic = -1;
    int nUserFrame = -1;
    int nEnergySet = -1;
    int nArchEnergy = -1;
    int nMaxPower = -1;
    int nNetSet = -1;
    int nDateTime = -1;
    int nServiceFrames = -1;
  }
  g_pg_rows;
  int g_nSelectedPageRow = 0;

  struct SET_COLUMNS
  {
    int nCheck = -1;
    int nFrame = -1;
  }
  g_st_cols;
  int g_nComnSignSetCell = -1;
  #define MAX_SETS 250

  struct FRAME_COLUMNS
  {
    int nFrame = -1;
  }
  g_fr_cols;
  int g_nComnSignFrameCell = -1;
  #define MAX_FRAMES 250

  int g_nPageCnt_from = 0;

} // namespace ns_LCD

IMPLEMENT_DYNCREATE(CView_LCD, CCommonViewForm)

CView_LCD::CView_LCD()
  : CCommonViewForm(CView_LCD::IDD, L"CView_LCD")
  , m_lstPg(&m_plstPgEx, dynamic_cast<CCommonViewDealer*>(this))
  , m_lstSt(&m_plstStEx, dynamic_cast<CCommonViewDealer*>(this))
  , m_lstFr(&m_plstFrEx, dynamic_cast<CCommonViewDealer*>(this))
{
}

CView_LCD::~CView_LCD()
{
  if (m_plstPgEx)
  {
    m_plstPgEx->Destroy();
    delete m_plstPgEx;
  }

  if (m_plstStEx)
  {
    m_plstStEx->Destroy();
    delete m_plstStEx;
  }

  if (m_plstFrEx)
  {
    m_plstFrEx->Destroy();
    delete m_plstFrEx;
  }

  delete m_pData;
}

BEGIN_MESSAGE_MAP(CView_LCD, CCommonViewForm)
  ON_BN_CLICKED(IDC_BTN_LOAD_LCD_FROM_DEVICE, CView_LCD::OnBnClickedBtnLoadLcdFromDevice)
  ON_BN_CLICKED(IDC_BTN_RECORD_LCD_TO_DEVICE, CView_LCD::OnBnClickedBtnRecordLcdToDevice)
  ON_BN_CLICKED(IDC_BTN_OPEN_LCD_FROM_FILE, &CView_LCD::OnBnClickedBtnOpenLcdFromFile)
  ON_BN_CLICKED(IDC_BTN_SAVE_LCD_TO_FILE, &CView_LCD::OnBnClickedBtnSaveLcdToFile)
  ON_BN_CLICKED(IDC_BTN_LCD_PG_UNSEL_ALL, &CView_LCD::OnBnClickedBtnPgUnselAll)
  ON_BN_CLICKED(IDC_BTN_LCD_PG_SEL_ALL, &CView_LCD::OnBnClickedBtnPgSelAll)
  ON_BN_CLICKED(IDC_BTN_LCD_ST_SEL_ALL, &CView_LCD::OnBnClickedBtnStSelAll)
  ON_BN_CLICKED(IDC_BTN_LCD_ST_UNSEL_ALL, &CView_LCD::OnBnClickedBtnStUnselAll)
  ON_NOTIFY(NM_SETFOCUS, IDC_LST_LCD_FRAMES, &CView_LCD::OnNMSetfocusLstLcdFrames)
END_MESSAGE_MAP()

void CView_LCD::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_BTN_LOAD_LCD_FROM_DEVICE, m_btnLoadLcdFromDevice);
  DDX_Control(pDX, IDC_BTN_RECORD_LCD_TO_DEVICE, m_btnRecordLcdToDevice);
  DDX_Control(pDX, IDC_BTN_OPEN_LCD_FROM_FILE, m_btnOpenLcdFromSpecXmlFile);
  DDX_Control(pDX, IDC_BTN_SAVE_LCD_TO_FILE, m_btnSaveLcdToSpecXmlFile);
  DDX_Control(pDX, IDC_STC_LCD_PAGES, m_stcPages);
  DDX_Control(pDX, IDC_STC_LCD_SET, m_stcSets);
  DDX_Control(pDX, IDC_STC_LCD_FRAMES, m_stcFrames);
  DDX_Control(pDX, IDC_BTN_LCD_PG_UNSEL_ALL, m_btnPgUnselAll);
  DDX_Control(pDX, IDC_BTN_LCD_PG_SEL_ALL, m_btnPgSelAll);
  DDX_Control(pDX, IDC_BTN_LCD_ST_SEL_ALL, m_btnStSelAll);
  DDX_Control(pDX, IDC_BTN_LCD_ST_UNSEL_ALL, m_btnStUnselAll);
  DDX_Control(pDX, IDC_LST_LCD_PAGES, m_lstPg);
  DDX_Control(pDX, IDC_LST_LCD_ITEMS_ALL, m_lstSt);
  DDX_Control(pDX, IDC_LST_LCD_FRAMES, m_lstFr);
  DDX_Control(pDX, IDC_STC_SET, m_stcSet);
  DDX_Control(pDX, IDC_STC_FRAME, m_stcFrame);
}

void CView_LCD::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CParamsTreeNode* pInlaysNode = GetXmlNode();
  m_pData = new CLCD_data(pInlaysNode);

  // Tables

  CreatePageTable();
  CreateSetTable();
  CreateFrameTable();

  m_lstPg.CtrlWasCreated();
  m_lstSt.CtrlWasCreated();
  m_lstFr.CtrlWasCreated();

  // Create the ToolTip control.

  m_toolTip.Create(this);
  m_toolTip.Activate(TRUE);

  CMFCToolTipInfo params;
  TOOL_TIP_CTRL_INFO toolTipInfo;

  params.m_bVislManagerTheme = TRUE;
  m_toolTip.SetParams(&params);

  m_btnPgUnselAll.SetImage(IDB_UNCHECK_ALL);
  toolTipInfo.strDescription = L"�������� ��� ����� ��� ���� �������";
  toolTipInfo.nBmpResID = IDB_UNCHECK_ALL;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_LCD_PG_UNSEL_ALL, toolTipInfo));
  m_toolTip.AddTool(&m_btnPgUnselAll, L"�������� ���");

  m_btnPgSelAll.SetImage(IDB_CHECK_ALL);
  toolTipInfo.strDescription = L"�������� ��� ����� ��� ���� �������";
  toolTipInfo.nBmpResID = IDB_CHECK_ALL;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_LCD_PG_SEL_ALL, toolTipInfo));
  m_toolTip.AddTool(&m_btnPgSelAll, L"�������� ���");

  m_btnStUnselAll.SetImage(IDB_UNCHECK_ALL);
  toolTipInfo.strDescription = L"�������� ��� �����";
  toolTipInfo.nBmpResID = IDB_UNCHECK_ALL;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_LCD_ST_UNSEL_ALL, toolTipInfo));
  m_toolTip.AddTool(&m_btnStUnselAll, L"�������� ���");

  m_btnStSelAll.SetImage(IDB_CHECK_ALL);
  toolTipInfo.strDescription = L"�������� ��� �����";
  toolTipInfo.nBmpResID = IDB_CHECK_ALL;
  m_toolTip.m_toolTCtrlsInfo.insert(pair<int, TOOL_TIP_CTRL_INFO>(IDC_BTN_LCD_ST_SEL_ALL, toolTipInfo));
  m_toolTip.AddTool(&m_btnStSelAll, L"�������� ���");

  //

  AddCtrlForPositioning(&m_stcSet, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);
  AddCtrlForPositioning(&m_stcFrame, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);
  AddCtrlForPositioning(&m_lstSt, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);
  AddCtrlForPositioning(&m_lstFr, movbehConstTopAndBottomMargins, movbehConstLeftMarginAndHorizSizes);

  m_stcPages.SetBkColor(COLOR_LIGHT_BLUE);
  m_stcSets.SetBkColor(COLOR_LIGHT_BLUE);
  m_stcFrames.SetBkColor(COLOR_LIGHT_BLUE);

  WatchForCtrState(&m_btnLoadLcdFromDevice, rightCommon);
  WatchForCtrState(&m_btnRecordLcdToDevice, rightAdmin);

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  //

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));

  m_plstPgEx->SelectRow(ns_LCD::g_nSelectedPageRow);
  CheckBoldPageType();
}

void CView_LCD::CreatePageTable()
{
  m_lstPg.ShowHorizScroll(false);
  m_lstPg.EnableInterlacedColorScheme(true);

  m_plstPgEx = new �Ai_ListCtrlEx();
  m_plstPgEx->SetList(&m_lstPg, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstPgEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstPg.SetExtendedStyle(m_lstPg.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  ns_LCD::g_pg_cols.nPage = m_plstPgEx->SetColumnEx(L"��������", LVCFMT_LEFT);
  m_plstPgEx->SetColumnWidthStyle(ns_LCD::g_pg_cols.nPage, CS_WIDTH_FIX, 200);
  m_plstPgEx->MatchColumns();

  int nNodeIdx = m_plstPgEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  vector<CString> textOfCells(1);
  vector<CString> pages = m_pData->GetPages();
  ASSERT(pages.size() == 8);
  textOfCells[0] = pages[0];
  ns_LCD::g_pg_rows.nCyclIndic = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[1];
  ns_LCD::g_pg_rows.nUserFrame = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[2];
  ns_LCD::g_pg_rows.nEnergySet = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[3];
  ns_LCD::g_pg_rows.nArchEnergy = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[4];
  ns_LCD::g_pg_rows.nMaxPower = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[5];
  ns_LCD::g_pg_rows.nNetSet = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[6];
  ns_LCD::g_pg_rows.nDateTime = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = pages[7];
  ns_LCD::g_pg_rows.nServiceFrames = m_plstPgEx->SetChild(nNodeIdx, &textOfCells);

  m_plstPgEx->UpdateView();
}

void CView_LCD::CreateSetTable()
{
  m_lstSt.ShowHorizScroll(false);
  m_lstSt.EnableInterlacedColorScheme(true);

  m_plstStEx = new �Ai_ListCtrlEx();
  m_plstStEx->SetList(&m_lstSt, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = true;
  m_plstStEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstSt.SetExtendedStyle(m_lstSt.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  ns_LCD::g_st_cols.nCheck = m_plstStEx->SetColumnEx(L"���.", LVCFMT_LEFT);
  m_plstStEx->SetColumnWidthStyle(ns_LCD::g_st_cols.nCheck, CS_WIDTH_FIX, 35);
  ns_LCD::g_st_cols.nFrame = m_plstStEx->SetColumnEx(L"����", LVCFMT_LEFT);
  m_plstStEx->SetColumnWidthStyle(ns_LCD::g_st_cols.nFrame, CS_WIDTH_FIX, 365);
  m_plstStEx->MatchColumns();

  int nNodeIdx = m_plstStEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  ns_LCD::g_nComnSignSetCell = ns_LCD::g_st_cols.nFrame;
  m_plstStEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_LCD::g_nComnSignSetCell);

  vector<CString> textOfCells(2);
  textOfCells[1] = IDS_EMPTY_CELL_TEXT;
  // ��� ������ ������
  int nRow_inList = m_plstStEx->SetChild(nNodeIdx, &textOfCells);
  // ��. �������� "Descr. 1" � ��������� AppDescription.txt
  m_plstStEx->SetCellHideText(nRow_inList, ns_LCD::g_nComnSignSetCell);

  // ... ��� ��������� �����
  m_plstStEx->SetEmptyChild(MAX_SETS);

  m_plstStEx->UpdateView();
}

void CView_LCD::CreateFrameTable()
{
  m_lstFr.ShowHorizScroll(false);
  m_lstFr.EnableInterlacedColorScheme(true);

  m_plstFrEx = new �Ai_ListCtrlEx();
  m_plstFrEx->SetList(&m_lstFr, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstFrEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_lstFr.SetExtendedStyle(m_lstFr.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  ns_LCD::g_fr_cols.nFrame = m_plstFrEx->SetColumnEx(L"����� �������� \"...\"", LVCFMT_LEFT);
  m_plstFrEx->SetColumnWidthStyle(ns_LCD::g_fr_cols.nFrame, CS_WIDTH_FIX, 365);
  m_plstFrEx->MatchColumns();

  int nNodeIdx = m_plstFrEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  ns_LCD::g_nComnSignFrameCell = ns_LCD::g_fr_cols.nFrame;
  m_plstFrEx->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, ns_LCD::g_nComnSignFrameCell);

  vector<CString> textOfCells(1);
  textOfCells[0] = IDS_EMPTY_CELL_TEXT;
  // ��� ������ ������
  int nRow_inList = m_plstFrEx->SetChild(nNodeIdx, &textOfCells);
  // ��. �������� "Descr. 1" � ��������� AppDescription.txt
  m_plstFrEx->SetCellHideText(nRow_inList, ns_LCD::g_nComnSignFrameCell);

  // ... ��� ��������� �����
  m_plstFrEx->SetEmptyChild(MAX_FRAMES);

  m_plstFrEx->UpdateView();
}

void CView_LCD::OnLvnItemchangedLst(CListCtrl* pCtrl, int nRow_inView, bool bSelected)
{
  if (!GetInitialUpdateFlag())
    return;
  if (m_bIgnoreOnLvnItemchangedLst)
    return;

  if (pCtrl == &m_lstPg && m_lstPg.IsCtrlCreated())
  {
    if (bSelected)
    {
      m_bIgnoreOnLvnItemchangedLst = true;

      m_plstStEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, 0, false_d(L"mouse move"));

      ns_LCD::g_nSelectedPageRow = nRow_inView;
      int nRow_inList = nRow_inView + 1;
      CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);

      RefillSetByPageChanged(sPageName);
      RefillFrameByPageChanged(sPageName);
      CheckOnSetsByFrameList(sPageName);

      m_bIgnoreOnLvnItemchangedLst = false;

      m_plstFrEx->ChangeColumnName(ns_LCD::g_fr_cols.nFrame, L"����� �������� \"" + sPageName + L"\"");
    }
  }

  if (pCtrl == &m_lstSt && m_lstSt.IsCtrlCreated()
      && m_lstFr.IsCtrlCreated())
  {
    m_bIgnoreOnLvnItemchangedLst = true;

    if (CheckOnLimit())
    {
      InsertFrame();
      SaveFrames();
      UpdateXmlBackupFile();
      CheckBoldPageType();
    }
    else
    {
      m_lstSt.SetCheck(nRow_inView, 0);
    }

    m_bIgnoreOnLvnItemchangedLst = false;
  }

  if (pCtrl == &m_lstFr && m_lstFr.IsCtrlCreated())
  {
    if (bSelected)
    {
      int nRow_inList = nRow_inView + 1;
      CString sFrameName = m_plstFrEx->GetCellText(nRow_inList, ns_LCD::g_fr_cols.nFrame);

      m_bIgnoreOnLvnItemchangedLst = true;

      SelectSetBySelectFrame(sFrameName);

      m_bIgnoreOnLvnItemchangedLst = false;
    }
  }
}

void CView_LCD::ReselectCurrentSelection()
{
  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(sel.size() == 1);
  int nRow_inView = sel[0];
  OnLvnItemchangedLst(&m_lstPg, nRow_inView, true_d(L"it is like selection"));
}

void CView_LCD::OnNMSetfocusLstLcdFrames(NMHDR *pNMHDR, LRESULT *pResult)
{
  vector<int> sel;
  m_plstFrEx->GetSelectedItems(&sel);
  if (!sel.empty() && sel.size() == 1)
  {
    int nRow_inList = sel[0] + 1;
    CString sFrameName = m_plstFrEx->GetCellText(nRow_inList, ns_LCD::g_fr_cols.nFrame);
    SelectSetBySelectFrame(sFrameName);
  }
}

void CView_LCD::RefillSetByPageChanged(CString& sPageName)
{
  // ������� �����

  // ��� ��������� �����
  for (int j = MAX_SETS - 1; j > 0; --j)
  {
    int nRow_inList = j + 1;
    if (!m_plstStEx->IsRowVisible(nRow_inList))
      continue;

    int nRow_inView = j;
    m_plstStEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_LCD::g_nComnSignSetCell, -1);
    m_lstSt.SetCheck(nRow_inView, 0);
  }

  // ��� ������ ������
  int nRow_inList = 1;
  m_plstStEx->SetCellText(nRow_inList, ns_LCD::g_nComnSignSetCell, IDS_EMPTY_CELL_TEXT);
  m_plstStEx->SetCellHideText(nRow_inList, ns_LCD::g_nComnSignSetCell);
  int nRow_inView = nRow_inList - 1;
  m_lstSt.SetCheck(nRow_inView, 0);

  m_plstStEx->UpdateView();

  // ����� ������ ������

  vector<CString> sets = m_pData->GetSets(sPageName);
  nRow_inList = 1;
  for (auto& i : sets)
  {
    int nResult = RCHI_SUCCESS;
    int nNodeRow_inView = 0;
    CString& sCellText = i;

    if (nRow_inList > 1)
      nResult = m_plstStEx->InsertTextToChild(nNodeRow_inView, ns_LCD::g_nComnSignSetCell, sCellText);
    else
    {
      m_plstStEx->SetCellShowText(nRow_inList, ns_LCD::g_nComnSignSetCell);
      m_plstStEx->SetCellText(nRow_inList, ns_LCD::g_st_cols.nFrame, sCellText);
    }
    ++nRow_inList;
    ASSERT(nResult == RCHI_SUCCESS);
  }

  m_plstStEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, 0, false_d(L"mouse move"));
}

void CView_LCD::RefillFrameByPageChanged(CString& sPageName)
{
  ClearFrames();

  vector<CString> frames = m_pData->GetFrames(sPageName);
  int nRow_inList = 1;
  for (auto& i : frames)
  {
    int nResult = RCHI_SUCCESS;
    int nNodeRow_inView = 0;
    CString& sCellText = i;

    if (nRow_inList > 1)
      nResult = m_plstFrEx->InsertTextToChild(nNodeRow_inView, ns_LCD::g_nComnSignFrameCell, sCellText);
    else
    {
      m_plstFrEx->SetCellShowText(nRow_inList, ns_LCD::g_nComnSignFrameCell);
      m_plstFrEx->SetCellText(nRow_inList, ns_LCD::g_fr_cols.nFrame, sCellText);
    }
    ++nRow_inList;
    ASSERT(nResult == RCHI_SUCCESS);
  }

  m_plstFrEx->UpdateView();
}

void CView_LCD::SelectSetBySelectFrame(CString& sFrameName)
{
  int nNode_inView = 0;
  vector <CString> checkOn;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);
  for (int i = 0; i < nSetedItems; ++i)
  {
    int nRow_inList = i + 1;
    CString sSetName = m_plstStEx->GetCellText(nRow_inList, ns_LCD::g_st_cols.nFrame);
    if (sSetName == sFrameName)
    {
      int& nRow_inView = i;
      m_plstStEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, nRow_inView, false_d(L"mouse move"));
      return;
    }
  }
}

void CView_LCD::CheckBoldPageType()
{
  int nNode_inView = 0;
  int nSetedItems = m_plstPgEx->GetSetedCountOfChild(nNode_inView);
  for (int j = 0; j < nSetedItems; ++j)
  {
    int nRow_inList = j + 1;
    CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
    size_t nFrameCount = m_pData->GetFrames(sPageName).size();
    bool bEqual = (m_pData->GetSets(sPageName).size() == nFrameCount);
    if (!bEqual)
    {
      int nMax = m_pData->GetMaxCount(sPageName);
      ASSERT(static_cast<int>(nFrameCount) <= nMax);
      bEqual = (nMax == static_cast<int>(nFrameCount));
    }

    CellTextStyle txtStyle = m_plstPgEx->GetCellTextStyle(nRow_inList, ns_LCD::g_pg_cols.nPage);
    if (bEqual && txtStyle != CellTextStyle::ctsBold)
    {
      m_plstPgEx->SetCellTextStyle(nRow_inList, ns_LCD::g_pg_cols.nPage, CellTextStyle::ctsBold);
      m_plstPgEx->UpdateView();
    }
    if (!bEqual && txtStyle == CellTextStyle::ctsBold)
    {
      m_plstPgEx->SetCellTextStyle(nRow_inList, ns_LCD::g_pg_cols.nPage, CellTextStyle::ctsNone);
      m_plstPgEx->UpdateView();
    }
  }
}

void CView_LCD::CheckOnSetsByFrameList(CString& sPageName)
{
  // ������� check on

  vector<CString> frames = m_pData->GetFrames(sPageName);
  int nNode_inView = 0;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);

  for (int j = 0; j < nSetedItems; ++j)
  {
    int nRow_inView = j;
    m_lstSt.SetCheck(nRow_inView, 0);

    int nRow_inList = j + 1;
    CString sSet = m_plstStEx->GetCellText(nRow_inList, ns_LCD::g_st_cols.nFrame);
    for (auto& i : frames)
    {
      if (sSet == i)
      {
        m_lstSt.SetCheck(nRow_inView, 1);
        break;
      }
    }
  }
}

void CView_LCD::ClearFrames()
{
  // ��� ��������� �����
  for (int j = MAX_FRAMES - 1; j > 0; --j)
  {
    int nRow_inList = j + 1;
    if (!m_plstFrEx->IsRowVisible(nRow_inList))
      continue;

    int nRow_inView = j;
    m_plstFrEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_LCD::g_nComnSignFrameCell, -1);
  }

  // ��� ������ ������
  int nRow_inList = 1;
  m_plstFrEx->SetCellText(nRow_inList, ns_LCD::g_nComnSignFrameCell, IDS_EMPTY_CELL_TEXT);
  m_plstFrEx->SetCellHideText(nRow_inList, ns_LCD::g_nComnSignFrameCell);


  m_plstFrEx->UpdateView();
}

bool CView_LCD::CheckOnLimit()
{
  CString sPageName = GetCurrPage();
  int nCheckOnCount = 0;
  int nMaxCount = 0;
  GetMaxAndOnCount(sPageName, nMaxCount, nCheckOnCount);
  if (nCheckOnCount > nMaxCount)
    return false;
  return true;
}

CString CView_LCD::GetCurrPage()
{
  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(sel.size() == 1);
  int nRow_inList = sel[0] + 1;
  CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
  return sPageName;
}

void CView_LCD::GetMaxAndOnCount(CString& sPageName, int&nMax, int& nOn)
{
  int nNode_inView = 0;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);
  nOn = 0;

  for (int i = 0; i < nSetedItems; ++i)
  {
    if (m_lstSt.GetCheck(i) == 1)
      ++nOn;
  }

  sPageName = GetCurrPage();
  nMax = m_pData->GetMaxCount(sPageName);
}

void CView_LCD::InsertFrame()
{
  // Grab check on set items

  int nNode_inView = 0;
  vector <CString> checkOn;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);
  for (int i = 0; i < nSetedItems; ++i)
  {
    if (m_lstSt.GetCheck(i) == 1)
    {
      int nRow_inList = i + 1;
      checkOn.push_back(m_plstStEx->GetCellText(nRow_inList, ns_LCD::g_st_cols.nFrame));
    }
  }

  // Update frame list

  if (checkOn.empty()) // free list
  {
    ClearFrames();
    return;
  }

  nSetedItems = m_plstFrEx->GetSetedCountOfChild(nNode_inView);

  if (checkOn.size() < static_cast<size_t>(nSetedItems)) // erase row(s)
  {
    int nDelCount = nSetedItems - checkOn.size();
    int nRow_inList = nSetedItems; // last seted
    int nRow_inView = nRow_inList - 1;
    for (int i = nDelCount; i > 0; --i)
    {
      m_plstFrEx->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, ns_LCD::g_nComnSignFrameCell, -1);
      --nRow_inView;
    }
  }

  if (checkOn.size() > static_cast<size_t>(nSetedItems) // add row(s)
      && checkOn.size() > 1)
  {
    int nAddCount = checkOn.size() - nSetedItems;
    int nRow_inList = nSetedItems; // last seted
    int nRow_inView = nRow_inList - 1;
    int nNodeRow_inView = 0;
    for (int i = 0; i < nAddCount; ++i)
      m_plstFrEx->InsertTextToChild(nNodeRow_inView, ns_LCD::g_nComnSignFrameCell, L" ");
    m_plstFrEx->UpdateView();
  }

  int nRow_inList = 1;
  for (auto& i : checkOn) // fillin new list
  {
    m_plstFrEx->SetCellText(nRow_inList, ns_LCD::g_nComnSignFrameCell, i);
    if (nRow_inList == 1)
      m_plstFrEx->SetCellShowText(nRow_inList, ns_LCD::g_nComnSignFrameCell);
    ++nRow_inList;
  }
  m_plstFrEx->UpdateView();
}

void CView_LCD::SaveFrames()
{
  // Grab check on set items

  int nNode_inView = 0;
  vector <CString> frames;
  int nSetedItems = m_plstFrEx->GetSetedCountOfChild(nNode_inView);
  for (int i = 0; i < nSetedItems; ++i)
  {
    int nRow_inList = i + 1;
    CString sFrame = m_plstFrEx->GetCellText(nRow_inList, ns_LCD::g_fr_cols.nFrame);
    sFrame.TrimRight(IDS_EMPTY_CELL_TEXT);
    if (!sFrame.IsEmpty())
      frames.push_back(sFrame);
  }

  // Save

  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(sel.size() == 1);
  int nRow_inList = sel[0] + 1;
  CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
  m_pData->UpdateFrames(sPageName, &frames);
}

void CView_LCD::BeforeDestroyClass()
{
}

void CView_LCD::OnSize_()
{
}

void CView_LCD::OnDraw(CDC* pDC)
{
}

BOOL CView_LCD::PreTranslateMessage(MSG* pMsg)
{
  switch (pMsg->message)
  {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_MBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MBUTTONUP:
    case WM_MOUSEMOVE:
      m_toolTip.RelayEvent(pMsg);
      break;
  };

  return __super::PreTranslateMessage(pMsg);
}

bool CView_LCD::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

bool CView_LCD::UseAlternativeBgColor(CListCtrl* pCtrl, COLORREF& cNewCololr)
{
  if (pCtrl == &m_lstPg && m_lstPg.IsCtrlCreated())
  {
    if (GetFocus() != &m_lstPg)
    {
      cNewCololr = COLOR_LCE_RED_YELLOW;
      return true;
    }
  }

  if (pCtrl == &m_lstSt && m_lstSt.IsCtrlCreated())
  {
    if (GetFocus() != &m_lstSt)
    {
      cNewCololr = COLOR_LCE_RED_YELLOW;
      return true;
    }
  }

  return false;
}

#ifdef _DEBUG
void CView_LCD::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_LCD::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_LCD::OnContextMenu(CWnd*, CPoint point)
{
}

void CView_LCD::OnBnClickedBtnOpenLcdFromFile()
{
  if (m_pData->OpenLcdToFile())
    ReselectCurrentSelection();
}

void CView_LCD::OnBnClickedBtnSaveLcdToFile()
{
  m_pData->SaveLcdToFile();
}

void CView_LCD::OnBnClickedBtnPgUnselAll()
{
  if (AfxMessageBox(L"��������� ����� ��� ������ ��������?", MB_ICONQUESTION) == IDCANCEL)
    return;

  OnPgUnselAll();
}

void CView_LCD::OnPgUnselAll()
{
  m_bIgnoreOnLvnItemchangedLst = true;

  int nNode_inView = 0;
  int nSetedItems = m_plstPgEx->GetSetedCountOfChild(nNode_inView);
  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(!sel.empty() && (sel.size() == 1));

  for (int j = 0; j < nSetedItems; ++j)
  {
    if (j == sel[0])
      continue;
    int nRow_inList = j + 1;
    CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
    m_pData->FreeAllForPage(sPageName);
  }

  OnStUnselAll(); // ��� ������� �������

  m_bIgnoreOnLvnItemchangedLst = false;
}

void CView_LCD::OnBnClickedBtnPgSelAll()
{
  if (AfxMessageBox(L"�������� ������������ ����� ������ ��� ������ ��������?", MB_ICONQUESTION) == IDCANCEL)
    return;

  OnStUnselAll(); // ����� ��������� (���� ��� ����) ���������� ���������������� �� �� ������ ���������

  m_bIgnoreOnLvnItemchangedLst = true;

  int nNode_inView = 0;
  int nSetedItems = m_plstPgEx->GetSetedCountOfChild(nNode_inView);
  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(!sel.empty() && (sel.size() == 1));

  for (int j = 0; j < nSetedItems; ++j)
  {
    if (j == sel[0])
      continue;
    int nRow_inList = j + 1;
    CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
    m_pData->IncludeAllForPage(sPageName);
  }

  OnStSelAll();

  m_bIgnoreOnLvnItemchangedLst = false;
}

void CView_LCD::OnBnClickedBtnStUnselAll()
{
  if (AfxMessageBox(L"���������� ����� ��� ������� ��������?", MB_ICONQUESTION) == IDCANCEL)
    return;
  OnStUnselAll();
}

void CView_LCD::OnStUnselAll()
{
  m_bIgnoreOnLvnItemchangedLst = true;

  int nNode_inView = 0;
  vector <CString> checkOn;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);
  for (int i = 0; i < nSetedItems; ++i)
  {
    int nRow_inView = i;
    if (m_lstSt.GetCheck(nRow_inView) == 1)
      m_lstSt.SetCheck(nRow_inView, 0);
  }

  InsertFrame();
  SaveFrames();
  UpdateXmlBackupFile();
  CheckBoldPageType();

  m_bIgnoreOnLvnItemchangedLst = false;
}

void CView_LCD::OnBnClickedBtnStSelAll()
{
  if (AfxMessageBox(L"�������� ������������ ����� ������ ��� ������� ��������?", MB_ICONQUESTION) == IDCANCEL)
    return;
  OnStSelAll();
}

void CView_LCD::OnStSelAll()
{
  OnStUnselAll(); // ����� ��������� (���� ��� ����) ���������� ���������������� �� �� ������ ���������

  m_bIgnoreOnLvnItemchangedLst = true;

  int nNode_inView = 0;
  vector <CString> checkOn;
  int nSetedItems = m_plstStEx->GetSetedCountOfChild(nNode_inView);

  vector<int> sel;
  m_plstPgEx->GetSelectedItems(&sel);
  ASSERT(!sel.empty() && (sel.size() == 1));
  int nRow_inList = sel[0] + 1;
  CString sPageName = m_plstPgEx->GetCellText(nRow_inList, ns_LCD::g_pg_cols.nPage);
  int nMayOnMaxCount = m_pData->GetMaxCount(sPageName);

  int nCanOn = min(nSetedItems, nMayOnMaxCount);

  for (int i = 0; i < nCanOn; ++i)
  {
    int nRow_inView = i;
    if (m_lstSt.GetCheck(nRow_inView) == 0)
      m_lstSt.SetCheck(nRow_inView, 1);
  }

  InsertFrame();
  SaveFrames();
  UpdateXmlBackupFile();
  CheckBoldPageType();

  m_bIgnoreOnLvnItemchangedLst = false;
}

void CView_LCD::OnBnClickedBtnLoadLcdFromDevice()
{
  OnPgUnselAll();

  // ������� ������������ ��������� � ��

  FormListForClearReceivedData(FRAME_TYPE_FGROUP_CNT);
  int nStart = static_cast<int>(FRAME_TYPE_FGROUP_001);
  int nEnd = static_cast<int>(FRAME_TYPE_FGROUP_010);
  for (int i = nStart; i <= nEnd; ++i)
    FormListForClearReceivedData(static_cast<teFrameTypes>(i));
  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_LCD::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);

  bool bOnceCmd = true;
  SetParamTypeForRead(FRAME_TYPE_FGROUP_CNT, bOnceCmd);

  BeginFormingReadPack(false);
}

void CView_LCD::OnBnClickedBtnRecordLcdToDevice()
{
  int nSignPageCnt = m_pData->GetSignificantPageCount();
  if (nSignPageCnt == 0)
  {
    AfxMessageBox(L"������ �� �������!\n��� �� ����� �������� ������� ��������� �����.", MB_ICONERROR);
    return;
  }

  SetWaitCursor_answer(true);

  BeginFormingSendPack(true_d(L"start"));

  int nStart = static_cast<int>(FRAME_TYPE_FGROUP_001);
  int nEnd = static_cast<int>(FRAME_TYPE_FGROUP_010);
  SendDataToDevice(FRAME_TYPE_FGROUP_CNT, nSignPageCnt);

  int i = 0;
  for (int j = nStart; j <= nEnd; ++j, ++i)
  {
    teFrameTypes evType = static_cast<teFrameTypes>(j);
    CString sData;
    m_pData->FormData(i, sData);
    SendDataToDevice(evType, sData);
  }

  SendDataToDevice(FRAME_TYPE_COMMAND_FGROUP_EXECUTE); // Apply new settings

  BeginFormingSendPack(false_d(L"start"));
}

void CView_LCD::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_LCD::CheckMeaningsViewByConnectionStatus()
{
}

void CView_LCD::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� ��������� ���!", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_FGROUP_CNT)
  {
    int nCount = CAi_Str::ToInt(m); // ��� �������� ������ ��������� ���������� ������� ������� �������� �����
    if (nCount == 0)
    {
      SetWaitCursor_answer(false);
      AfxMessageBox(L"������� �� �������� ������ �� ��������� ���!", MB_ICONINFORMATION);
      return;
    }

    int nStart = static_cast<int>(FRAME_TYPE_FGROUP_001);
    int nEnd = static_cast<int>(FRAME_TYPE_FGROUP_010);
    int nMaxPageCnt = nEnd - nStart + 1;
    ns_LCD::g_nPageCnt_from = nMaxPageCnt;
    for (int j = nStart; j <= nEnd; ++j)
    {
      teFrameTypes evType = static_cast<teFrameTypes>(j);
      SetParamTypeForRead(evType, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    }
    return;
  }

  if (ft >= FRAME_TYPE_FGROUP_001
      && ft <= FRAME_TYPE_FGROUP_010)
  {
    int ft1 = static_cast<int>(FRAME_TYPE_FGROUP_001);
    int ftCurr = static_cast<int>(ft);
    int nOrder = ftCurr - ft1;
    m_pData->ParseReceivedData(nOrder, m); // - 1 - for zero-based

    if ((nOrder + 1) == ns_LCD::g_nPageCnt_from)
    {
      SetWaitCursor_answer(false);

      ReselectCurrentSelection();
      SaveFrames();
      UpdateXmlBackupFile();

      AfxMessageBox(L"��� ������ �� ��������� ��� ������� ��������� �� ��������!", MB_ICONINFORMATION);
    }
  }
}

void CView_LCD::UpdateInlayByReceivedConfirmation(teFrameTypes ft, bool bResult)
{
  if (!bResult)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ ������ � ������� ������ �� ��������� ���!\n������ ������ � ������� ��������.", MB_ICONERROR);
    return;
  }

  if (ft == FRAME_TYPE_COMMAND_FGROUP_EXECUTE)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �� ��������� ��� ������� �������� � �������!", MB_ICONINFORMATION);
    return;
  }
}
