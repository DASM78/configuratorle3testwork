#include "StdAfx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Defines.h"
#include "View_Energy.h"
#include <vector>

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Tab_DMY_Data
{
  #define COUNT_OF_ENERGY_TARIFFS 8
  #define COUNT_OF_DAILY_MAX 128
  #define COUNT_OF_MONTH_MAX 12
  #define COUNT_OF_YEAR_MAX 10

  struct COLUMNS
  {
    int nOrder = -1;
    int nDate = -1;
    int nEnergy = -1;
    int tariffs[COUNT_OF_ENERGY_TARIFFS];
    int nSum = -1;
  }
  g_columns;
  int g_nComnSignificantCell = -1;

  struct ROWS
  {
    int nA_plus = -1;
    int nR_plus = -1;
    int nR_minus = -1;

    int nCount_new_daily = -1;
    int nCount_new_monthly = -1;
    int nCount_new_year = -1;
  }
  g_rows;

  struct DMY_DATA_ROW
  {
    CString sTarrifs[COUNT_OF_ENERGY_TARIFFS];
    CString sSum;
  };
  struct DMY_DATA_GROUP
  {
    CString sDate;
    DMY_DATA_ROW A_plus;
    DMY_DATA_ROW R_plus;
    DMY_DATA_ROW R_minus;
  };
  DMY_DATA_GROUP g_EmplyData;
  vector<DMY_DATA_GROUP> g_dailyData;
  vector<DMY_DATA_GROUP> g_monthlyData;
  vector<DMY_DATA_GROUP> g_forYearsData;

  bool g_bRecreatDailyTbl = false;
  bool g_bRecreatMonthlyTbl = false;
  bool g_bRecreatForYearsTbl = false;

} // ns_Tab_DMY_Data

using namespace ns_Tab_DMY_Data;

void CView_Energy::GetDMYs(CCommonListCtrl*** pLst, �Ai_ListCtrlEx*** pLstEx, DMY_List& dmy)
{
  switch (dmy)
  {
    case dmyDaily: *pLst = &m_plstDailyData; *pLstEx = &m_plstDailyDataEx; break;
    case dmyMonthly: *pLst = &m_plstMonthlyData; *pLstEx = &m_plstMonthlyDataEx; break;
    case dmyForYears: *pLst = &m_plstDateForYears; *pLstEx = &m_plstDateForYearsEx; break;
  }
}

vector<DMY_DATA_GROUP>* GetDMYsDataStore(CView_Energy::DMY_List& dmy)
{
  vector<DMY_DATA_GROUP>* pRet = &g_monthlyData;
  switch (dmy)
  {
    case CView_Energy::dmyDaily: pRet = &g_dailyData; break;
    case CView_Energy::dmyMonthly: pRet = &g_monthlyData; break;
    case CView_Energy::dmyForYears: pRet = &g_forYearsData; break;
  }
  return pRet;
}

int GetDMYsItemCount(CView_Energy::DMY_List& dmy)
{
  return (int)GetDMYsDataStore(dmy)->size();
}

DMY_DATA_GROUP& GetDMYsData(unsigned int nGroupIndex, CView_Energy::DMY_List& dmy)
{
  if ((int)nGroupIndex >= GetDMYsItemCount(dmy))
  {
    ASSERT(false);
    return g_EmplyData;
  }
  return GetDMYsDataStore(dmy)->at(nGroupIndex);
}

void CView_Energy::CreateDMY_DataTab(DMY_List dmy)
{
  CCommonListCtrl** pLst = nullptr;
  �Ai_ListCtrlEx** pLstEx = nullptr;
  GetDMYs(&pLst, &pLstEx, dmy);

  (*pLst)->ShowHorizScroll(false);

  *pLstEx = new �Ai_ListCtrlEx();
  (*pLstEx)->SetList((*pLst), m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  (*pLstEx)->CreateList(bCheckBoxesInFirstColumn, nStyleEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    (*pLst)->SetExtendedStyle((*pLst)->GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  (*pLstEx)->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  (*pLstEx)->SetColumnEx(L""); // 0 column
  g_columns.nOrder = (*pLstEx)->SetColumnEx(L"�", LVCFMT_CENTER);
  (*pLstEx)->SetColumnWidthStyle(g_columns.nOrder, CS_WIDTH_FIX, 30);
  g_columns.nDate = (*pLstEx)->SetColumnEx(L"����", LVCFMT_CENTER);
  (*pLstEx)->SetColumnWidthStyle(g_columns.nDate, CS_WIDTH_FIX, 100);
  g_columns.nEnergy = (*pLstEx)->SetColumnEx(L"�������", LVCFMT_CENTER);
  (*pLstEx)->SetColumnWidthStyle(g_columns.nEnergy, CS_WIDTH_FIX, 100);
  for (int i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
  {
    g_columns.tariffs[i] = (*pLstEx)->SetColumnEx(L"������\n" + CAi_Str::ToString(i + 1), LVCFMT_CENTER);
    (*pLstEx)->SetColumnWidthStyle(g_columns.tariffs[i], CS_WIDTH_FIX, 60);
  }
  g_columns.nSum = (*pLstEx)->SetColumnEx(L"�����", LVCFMT_CENTER);
  (*pLstEx)->SetColumnWidthStyle(g_columns.nSum, CS_WIDTH_FIX, 100);

  (*pLst)->ShowColumn(0, false);
  (*pLstEx)->MatchColumns();

  int nNodeIdx = (*pLstEx)->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������
  g_nComnSignificantCell = g_columns.nEnergy;
  (*pLstEx)->SetNodeStyle(nNodeIdx, NS_USE_COMN_SIGNIFICANT_CELL | NS_UNWRAP_ONLY_SETED, 0, g_nComnSignificantCell);

  int nItemCount = 0;
  switch (dmy)
  {
    case CView_Energy::dmyDaily: nItemCount = COUNT_OF_DAILY_MAX; break;
    case CView_Energy::dmyMonthly: nItemCount = COUNT_OF_MONTH_MAX; break;
    case CView_Energy::dmyForYears: nItemCount = COUNT_OF_YEAR_MAX; break;
  }
  nItemCount *= 3;

  //for (int j = 0; j < nItemCount; ++j)
  {
    vector<CString> textOfCells;
    textOfCells.push_back(L""); // 0
    textOfCells.push_back(L""); // order
    textOfCells.push_back(L""); // date
    textOfCells.push_back(IDS_EMPTY_CELL_TEXT); // energy name
    for (int i = g_columns.tariffs[0]; i < g_columns.tariffs[0] + COUNT_OF_ENERGY_TARIFFS; ++i)
      textOfCells.push_back(L""); // tariffs
    textOfCells.push_back(L""); // sum

    // ��� ������ ������
    int nRow_inList = (*pLstEx)->SetChild(nNodeIdx, &textOfCells);
    // ��� ���� - ��. �������� "Descr. 1" � ��������� AppDescription.txt
    (*pLstEx)->SetCellHideText(nRow_inList, g_nComnSignificantCell);
    textOfCells[g_columns.nEnergy] = L"";
    (*pLstEx)->SetEmptyChild(nItemCount - 1);
  }
 
  (*pLstEx)->UpdateView();
  (*pLst)->CtrlWasCreated();
}

void CView_Energy::FillInDMY_DataTab(DMY_List dmy)
{
  CCommonListCtrl** pLst = nullptr;
  �Ai_ListCtrlEx** pLstEx = nullptr;
  GetDMYs(&pLst, &pLstEx, dmy);

  int nItemCount = GetDMYsItemCount(dmy);
  int nRow_inList = 1;
  for (int groupIdx = 0; groupIdx < nItemCount; ++groupIdx)
  {
    CString sOrder = CAi_Str::ToString(groupIdx + 1);
    DMY_DATA_GROUP& data = GetDMYsData(groupIdx, dmy);
    CString sDate = data.sDate;
    COLORREF nSpecColor = (*pLst)->GetSpecColor();
    COLORREF nSpecColor2 = nSpecColor;
    int nSysColor = GetSysColor(COLOR_BTNFACE);
    COLORREF nInterlacedGreen = (*pLst)->GetInterlacedColor();

    for (int energy = 0; energy < 3; ++energy)
    {
      CString sE;
      switch (energy)
      {
        case 0: sE = L"A+ (���*�)"; break;
        case 1: sE = L"R+ (����*�)"; break;
        case 2: sE = L"R- (����*�)"; break;
      }
      if (groupIdx == 0 && energy == 0)
      {
        (*pLstEx)->SetCellShowText(nRow_inList, g_nComnSignificantCell);
        (*pLstEx)->SetCellText(nRow_inList, g_nComnSignificantCell, sE);
      }
      else
      {
        int nNodeRow_inView = 0;
        (*pLstEx)->InsertTextToChild(nNodeRow_inView, g_nComnSignificantCell, sE);
      }

      (*pLstEx)->SetCellText(nRow_inList, g_columns.nOrder, sOrder);
      sOrder = L"";
      (*pLstEx)->SetCellText(nRow_inList, g_columns.nDate, sDate);
      sDate = L"";

      for (int t = 0, i = g_columns.tariffs[0]; i < g_columns.tariffs[0] + COUNT_OF_ENERGY_TARIFFS; ++i, ++t)
      {
        CString* pS = nullptr;
        switch (energy)
        {
          case 0: pS = &data.A_plus.sTarrifs[t]; break;
          case 1: pS = &data.R_plus.sTarrifs[t]; break;
          case 2: pS = &data.R_minus.sTarrifs[t]; break;
        }
        if (pS)
          (*pLstEx)->SetCellText(nRow_inList, i, *pS);
      }

      {
        CString* pS = nullptr;
        switch (energy)
        {
          case 0: pS = &data.A_plus.sSum; break;
          case 1: pS = &data.R_plus.sSum; break;
          case 2: pS = &data.R_minus.sSum; break;
        }
        if (pS)
          (*pLstEx)->SetCellText(nRow_inList, g_columns.nSum, *pS);
      }

      (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nOrder, nSpecColor2);
      (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nDate, nSpecColor2);
      (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nEnergy, nSpecColor);
      if (groupIdx % 2)
      {
        for (int t = 0, i = g_columns.tariffs[0]; i < g_columns.tariffs[0] + COUNT_OF_ENERGY_TARIFFS; ++i, ++t)
          (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.tariffs[t], nInterlacedGreen);
        (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nSum, nInterlacedGreen);
      }
      nSpecColor2 = nSysColor;

      ++nRow_inList;
    }
  }

  (*pLstEx)->UpdateView();
}

void CView_Energy::CheckMeaningsViewByConnectionStatus_InDMY_Tbl(DMY_List dmy)
{
  if (!IsConnection())
    ClearAllDMY_Rows(dmy);
}

void CView_Energy::ClearAllDMY_Rows(DMY_List dmy)
{
  CCommonListCtrl** pLst = nullptr;
  �Ai_ListCtrlEx** pLstEx = nullptr;
  GetDMYs(&pLst, &pLstEx, dmy);

  if ((*pLstEx)->GetSetedCountOfChild(0) > 0)
  {
    int nItemCount = 0;
    switch (dmy)
    {
      case CView_Energy::dmyDaily: nItemCount = COUNT_OF_DAILY_MAX; break;
      case CView_Energy::dmyMonthly: nItemCount = COUNT_OF_MONTH_MAX; break;
      case CView_Energy::dmyForYears: nItemCount = COUNT_OF_YEAR_MAX; break;
    }
    nItemCount *= 3;

    // ��� ������ ������
    int nRow_inList = 1;
    for (int i = g_columns.tariffs[0]; i < g_columns.tariffs[0] + COUNT_OF_ENERGY_TARIFFS; ++i)
    {
      (*pLstEx)->SetCellText(nRow_inList, i, L"");
    }
    (*pLstEx)->SetCellHideText(nRow_inList, g_nComnSignificantCell);
    (*pLstEx)->SetCellText(nRow_inList, g_columns.nOrder, L"");
    (*pLstEx)->SetCellText(nRow_inList, g_columns.nDate, L"");
    (*pLstEx)->SetCellText(nRow_inList, g_nComnSignificantCell, IDS_EMPTY_CELL_TEXT);
    (*pLstEx)->SetCellText(nRow_inList, g_columns.nSum, L"");
    COLORREF nWhiteColor = RGB(255, 255, 255);
    (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nOrder, nWhiteColor);
    (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nDate, nWhiteColor);
    (*pLstEx)->SetCellBgColor(nRow_inList, g_columns.nEnergy, nWhiteColor);
    // ...

    for (int j = 1; j < nItemCount; ++j)
    {
      nRow_inList = j + 1;
      if (!(*pLstEx)->IsRowVisible(nRow_inList))
        continue;

      int nRow_inView = j;
      (*pLstEx)->ClearChild(nRow_inView, CRF_BY_INIT_CHAIN, nullptr, 1, g_nComnSignificantCell, -1);
      for (int i = g_columns.tariffs[0]; i < g_columns.tariffs[0] + COUNT_OF_ENERGY_TARIFFS; ++i)
        (*pLstEx)->SetCellText(nRow_inList, i, L"");
    }
  }

  switch (dmy)
  {
    case CView_Energy::dmyDaily: g_rows.nCount_new_daily = -1; break;
    case CView_Energy::dmyMonthly: g_rows.nCount_new_monthly = -1; break;
    case CView_Energy::dmyForYears: g_rows.nCount_new_year = -1; break;
  }

  vector<DMY_DATA_GROUP>* pDMY = GetDMYsDataStore(dmy);
  pDMY->clear();

  (*pLstEx)->UpdateView();
}

void CView_Energy::UpdMeaningsByRecvdDeviceData_InDMY_Tbl(teFrameTypes& ft, DMY_List dmy)
{
  teFrameTypes ftReqMaxCount = FRAME_TYPE_MONTH_ENERGY_CNT;
  switch (dmy)
  {
    case CView_Energy::dmyDaily: ftReqMaxCount = FRAME_TYPE_DAY_ENERGY_CNT; break;
    case CView_Energy::dmyMonthly: ftReqMaxCount = FRAME_TYPE_MONTH_ENERGY_CNT;  break;
    case CView_Energy::dmyForYears: ftReqMaxCount = FRAME_TYPE_YEAR_ENERGY_CNT; break;
  }

  if (ft == ftReqMaxCount)
  {
    varMeaning_t m = FindReceivedData(ftReqMaxCount);
    if (m != L"?")
    {
      int nNewRowCount = CAi_Str::ToInt(m);

      int defMaxCount = COUNT_OF_MONTH_MAX;
      int* pRefToCountStore = &g_rows.nCount_new_monthly;
      switch (dmy)
      {
        case CView_Energy::dmyDaily:
          defMaxCount = COUNT_OF_DAILY_MAX;
          pRefToCountStore = &g_rows.nCount_new_daily;
          break;
        case CView_Energy::dmyMonthly:
          defMaxCount = COUNT_OF_MONTH_MAX;
          pRefToCountStore = &g_rows.nCount_new_monthly;
          break;
        case CView_Energy::dmyForYears:
          defMaxCount = COUNT_OF_YEAR_MAX;
          pRefToCountStore = &g_rows.nCount_new_year;
          break;
      }

      *pRefToCountStore = min(nNewRowCount, defMaxCount); // ����� �� ������� �� ��������
      *pRefToCountStore = max(*pRefToCountStore, 0); // ����� �� ������� �� �������
    }

    return;
  }

  vector<DMY_DATA_GROUP>* pDMY = GetDMYsDataStore(dmy);

  if ((ft >= FRAME_TYPE_DAY_ENERGY_001 && ft <= FRAME_TYPE_DAY_ENERGY_128)
      || (ft >= FRAME_TYPE_MONTH_ENERGY_01 && ft <= FRAME_TYPE_MONTH_ENERGY_32)
      || (ft >= FRAME_TYPE_YEAR_ENERGY_01 && ft <= FRAME_TYPE_YEAR_ENERGY_10))
  {
    unsigned int nAddress = 0;
    if (GetDataFrameAddress(ft, nAddress))
    {
      varMeaning_t mData1 = FindReceivedData(nAddress);
      varMeaning_t mData2 = FindReceivedData(nAddress + 1);
      varMeaning_t mData3 = FindReceivedData(nAddress + 2);

      mData1.TrimRight(L";");
      mData2.TrimRight(L";");
      mData3.TrimRight(L";");

      vector<CString> data1;
      vector<CString> data2;
      vector<CString> data3;

      if (CAi_Str::CutString(mData1, L";", &data1)
          && CAi_Str::CutString(mData2, L";", &data2)
            && CAi_Str::CutString(mData3, L";", &data3))
      {
        if (data1.size() == 10 && data2.size() == 9 && data3.size() == 9)
        {
          DMY_DATA_GROUP dg;

          // *** data1

          dg.sDate = data1[0];

/*
5001(01-01-2001;
A+SUM;R+SUM;R-SUM;A+SUM1;R+SUM1;R-SUM1;A+SUM2;R+SUM2;R-SUM2;)
<CR><LF>
5002(A+SUM3;R+SUM3;R-SUM3;A+SUM4;R+SUM4;R-SUM4;A+SUM5;R+SUM5;R-SUM5;)
<CR><LF>
5003(A+SUM6;R+SUM6;R-SUM6;A+SUM7;R+SUM7;R-SUM7;A+SUM8;R+SUM8;R-SUM8;)
*/
          dg.A_plus.sSum = data1[1];
          dg.R_plus.sSum = data1[2];
          dg.R_minus.sSum = data1[3];

          dg.A_plus.sTarrifs[0] = data1[4];
          dg.R_plus.sTarrifs[0] = data1[5];
          dg.R_minus.sTarrifs[0] = data1[6];

          dg.A_plus.sTarrifs[1] = data1[7];
          dg.R_plus.sTarrifs[1] = data1[8];
          dg.R_minus.sTarrifs[1] = data1[9];

          // *** data2

          dg.A_plus.sTarrifs[2] = data2[0];
          dg.R_plus.sTarrifs[2] = data2[1];
          dg.R_minus.sTarrifs[2] = data2[2];

          dg.A_plus.sTarrifs[3] = data2[3];
          dg.R_plus.sTarrifs[3] = data2[4];
          dg.R_minus.sTarrifs[3] = data2[5];

          dg.A_plus.sTarrifs[4] = data2[6];
          dg.R_plus.sTarrifs[4] = data2[7];
          dg.R_minus.sTarrifs[4] = data2[8];

          // *** data3

          dg.A_plus.sTarrifs[5] = data3[0];
          dg.R_plus.sTarrifs[5] = data3[1];
          dg.R_minus.sTarrifs[5] = data3[2];

          dg.A_plus.sTarrifs[6] = data3[3];
          dg.R_plus.sTarrifs[6] = data3[4];
          dg.R_minus.sTarrifs[6] = data3[5];

          dg.A_plus.sTarrifs[7] = data3[6];
          dg.R_plus.sTarrifs[7] = data3[7];
          dg.R_minus.sTarrifs[7] = data3[8];

          // ***

          pDMY->push_back(dg);
        }
      }
    }
  }

  int* pRefToCountStore = &g_rows.nCount_new_monthly;
  switch (dmy)
  {
    case CView_Energy::dmyDaily: pRefToCountStore = &g_rows.nCount_new_daily; break;
    case CView_Energy::dmyMonthly: pRefToCountStore = &g_rows.nCount_new_monthly; break;
    case CView_Energy::dmyForYears: pRefToCountStore = &g_rows.nCount_new_year; break;
  }

  if (*pRefToCountStore == pDMY->size()) // ��� ��������, �������� �������
  {
    bool* pbRecreateTbl = nullptr;

    switch (dmy)
    {
      case CView_Energy::dmyDaily: pbRecreateTbl = &g_bRecreatDailyTbl; break;
      case CView_Energy::dmyMonthly: pbRecreateTbl = &g_bRecreatMonthlyTbl;  break;
      case CView_Energy::dmyForYears: pbRecreateTbl = &g_bRecreatForYearsTbl; break;
    }

    if (pbRecreateTbl)
      *pbRecreateTbl = true;
  }
}

void CView_Energy::FormAllReqsOfItemsOfDMY_Tbl(DMY_List dmy)
{
  teFrameTypes reqFirstItem = FRAME_TYPE_MONTH_ENERGY_01;
  int* pRefToCountStore = &g_rows.nCount_new_monthly;

  switch (dmy)
  {
    case CView_Energy::dmyDaily:
      pRefToCountStore = &g_rows.nCount_new_daily;
      reqFirstItem = FRAME_TYPE_DAY_ENERGY_001;
      break;
    case CView_Energy::dmyMonthly:
      pRefToCountStore = &g_rows.nCount_new_monthly;
      reqFirstItem = FRAME_TYPE_MONTH_ENERGY_01;
      break;
    case CView_Energy::dmyForYears:
      pRefToCountStore = &g_rows.nCount_new_year;
      reqFirstItem = FRAME_TYPE_YEAR_ENERGY_01;
      break;
  }
  int nNext = static_cast<int>(reqFirstItem);
  for (int i = 0; i < *pRefToCountStore; ++i)
  {
    reqFirstItem = static_cast<teFrameTypes>(nNext);
    SetParamTypeForRead(reqFirstItem, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
    nNext += 1;
  }
}

bool CView_Energy::IsReqItems_ForDMY_Tbl()
{
  return (g_rows.nCount_new_daily > 0
          || g_rows.nCount_new_monthly > 0
          || g_rows.nCount_new_year > 0);
}

int CView_Energy::GetReqItemsFrom_ForDMY_Tbl(DMY_List dmy)
{
  switch (dmy)
  {
    case CView_Energy::dmyDaily: return g_rows.nCount_new_daily;
    case CView_Energy::dmyMonthly: return g_rows.nCount_new_monthly;
    case CView_Energy::dmyForYears: return g_rows.nCount_new_year;
  }
  return 0;
}

bool CView_Energy::IsRecreateDMY_Tbl(DMY_List dmy)
{
  bool* pbRecreateTbl = nullptr;

  switch (dmy)
  {
    case CView_Energy::dmyDaily: pbRecreateTbl = &g_bRecreatDailyTbl; break;
    case CView_Energy::dmyMonthly: pbRecreateTbl = &g_bRecreatMonthlyTbl; break;
    case CView_Energy::dmyForYears: pbRecreateTbl = &g_bRecreatForYearsTbl; break;
  }

  if (pbRecreateTbl)
    if (*pbRecreateTbl)
    {
      *pbRecreateTbl = false;
      return true;
    }

  return false;
}

void CView_Energy::ClearAllReqDataInDB_ForDMY_Tbl()
{
  FormListForClearReceivedData(FRAME_TYPE_DAY_ENERGY_CNT);
  FormListForClearReceivedData(FRAME_TYPE_MONTH_ENERGY_CNT);
  FormListForClearReceivedData(FRAME_TYPE_YEAR_ENERGY_CNT);

  for (int i = (int)FRAME_TYPE_DAY_ENERGY_001; i < (int)FRAME_TYPE_DAY_ENERGY_128; ++i)
    FormListForClearReceivedData((teFrameTypes)i);
  for (int i = (int)FRAME_TYPE_MONTH_ENERGY_01; i < (int)FRAME_TYPE_MONTH_ENERGY_32; ++i)
    FormListForClearReceivedData((teFrameTypes)i);
  for (int i = (int)FRAME_TYPE_YEAR_ENERGY_01; i < (int)FRAME_TYPE_YEAR_ENERGY_10; ++i)
    FormListForClearReceivedData((teFrameTypes)i);

  ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_Energy::ReceiveData_ForDMY_Tbl(DMY_List dmy)
{
  teFrameTypes ft = FRAME_TYPE_EMPTY;
  switch (dmy)
  {
    case CView_Energy::dmyDaily: ft = FRAME_TYPE_DAY_ENERGY_CNT; break;
    case CView_Energy::dmyMonthly: ft = FRAME_TYPE_MONTH_ENERGY_CNT; break;
    case CView_Energy::dmyForYears: ft = FRAME_TYPE_YEAR_ENERGY_CNT; break;
  }

  SetParamTypeForRead(ft, true_d(L"������� ������� - ����� ������� ��������� �� �����������"));
}