#include "stdafx.h"
#include <gdiplus.h>
#include "ElectricMeter.h"
#include "PowerProfileGraphs.h"
#include "Ai_Str.h"
#include <map>
#include <tuple>
#include "Tmplts.h"
#include "Defines.h"

using namespace Gdiplus;
using namespace std;

IMPLEMENT_DYNAMIC(CPowerProfileGraphs, CWnd)

#define PP_SIGNAL_COUNT 6
#define PP_BREAK_COUNT 10

namespace ns_Gr
{
  CAi_GdiPlus g_g;

  Color g_colorBg(97-20, 116-20, 150-20);
  Color g_colorBg2(97, 116, 150);

  const COLORREF g_color_AP = RGB(255, 130, 00);
  const COLORREF g_color_RQ_plus = RGB(0xFF, 0xED, 0x00);
  const COLORREF g_color_RQ_minus = RGB(0x5F, 0xAF, 0xFF);
  const ARGB g_argbAP = Color::MakeARGB(255, GetRValue(g_color_AP), GetGValue(g_color_AP), GetBValue(g_color_AP));
  const ARGB g_argbRQ_plus = Color::MakeARGB(255, GetRValue(g_color_RQ_plus), GetGValue(g_color_RQ_plus), GetBValue(g_color_RQ_plus));
  const ARGB g_argbRQ_minus = Color::MakeARGB(255, GetRValue(g_color_RQ_minus), GetGValue(g_color_RQ_minus), GetBValue(g_color_RQ_minus));

  bool g_bCurViewIsEnergy = true; // 1 - energy, 0 - power
  bool g_bDraw_AP = true;
  bool g_bDraw_RQ_plus = true;
  bool g_bDraw_RQ_minus = true;

  // zoom

  int g_nViewHeight = 0;
  int g_nZoomX = 1;
  int g_nZoomY = 1;

  // scrolling

  int g_nXSegmentCount = 0;
  int g_nXSegmentStart = 0;

  int g_nYPosStart = -1;

  // grid

  vector<Point> g_yPosGuidlines; // y position

  struct DAYS_POS
  {
    CString sDate;
    int nBegin; // x position
    int nHalf; // x position
    map<int, CString> hours; // x position, hour
    vector<int> halfHours; // x position
  };
  vector<DAYS_POS> g_xDaysPos;

} // ns_Gr

CPowerProfileGraphs::CPowerProfileGraphs()
{
  //ns_Gr::g_bSquare = !ns_Gr::g_bSquare;
}

CPowerProfileGraphs::~CPowerProfileGraphs()
{
}

void CPowerProfileGraphs::DoDataExchange(CDataExchange* pDX)
{
  CWnd::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPowerProfileGraphs, CWnd)
  ON_WM_PAINT()
  ON_WM_ERASEBKGND()
  ON_WM_HSCROLL()
  ON_WM_VSCROLL()
END_MESSAGE_MAP()

#ifdef _DEBUG
void CPowerProfileGraphs::AssertValid() const
{
  CWnd::AssertValid();
}

#ifndef _WIN32_WCE
void CPowerProfileGraphs::Dump(CDumpContext& dc) const
{
  CWnd::Dump(dc);
}
#endif
#endif //_DEBUG

void CPowerProfileGraphs::SetDataObj(CPowerProfileGraphsData* p)
{
  m_pDataObj = p;
}

bool CPowerProfileGraphs::SetCurrView(
  bool bCurViewIsEnergy,
  bool bDraw_AP,
  bool bDraw_RQ_plus,
  bool bDraw_RQ_minus)
{
  bool bRedraw = false;

  auto lmbCheck = [&](bool in, bool& dst) ->void
  {
    if (in != dst)
      bRedraw = true;
    dst = in;
  };

  lmbCheck(bCurViewIsEnergy, ns_Gr::g_bCurViewIsEnergy);
  lmbCheck(bDraw_AP, ns_Gr::g_bDraw_AP);
  lmbCheck(bDraw_RQ_plus, ns_Gr::g_bDraw_RQ_plus);
  lmbCheck(bDraw_RQ_minus, ns_Gr::g_bDraw_RQ_minus);

  return bRedraw;
}

bool CPowerProfileGraphs::IsCurViewEnergy()
{
  return ns_Gr::g_bCurViewIsEnergy;
}

void CPowerProfileGraphs::MoveWindow_(CRect& r)
{
  SetWindowPos(NULL, r.left, r.top, r.Width(), r.Height(), SWP_NOACTIVATE | SWP_NOZORDER);
}

void CPowerProfileGraphs::SetColorForAR(CAi_ColorStatic* pA, CAi_ColorStatic* pR_plus, CAi_ColorStatic* pR_minus)
{
  pA->SetWindowText(L"");
  pR_plus->SetWindowText(L"");
  pR_minus->SetWindowText(L"");

  pA->SetBkColor(ns_Gr::g_color_AP);
  pR_plus->SetBkColor(ns_Gr::g_color_RQ_plus);
  pR_minus->SetBkColor(ns_Gr::g_color_RQ_minus);
}

void CPowerProfileGraphs::ClearCurves()
{
  Invalidate();
}

BOOL CPowerProfileGraphs::OnEraseBkgnd(CDC* pDC)
{
  /*TRACE(L"OnEraseBkgnd\r\n");
  RECT rect;
  ::GetClientRect(m_hWnd, &rect);
  pDC->FillRect(&rect, &CBrush(RGB(97, 116, 150)));
  return CScrollView::OnEraseBkgnd(pDC);*/
  return 1;
}

void CPowerProfileGraphs::OnPaint()
{
  //TRACE(L"OnPaint_1\r\n");

  CPaintDC dc(this);
  Graphics graphicsOrig(dc.GetSafeHdc());
  graphicsOrig.SetPageUnit(UnitPixel);
  CRect rClient;
  GetClientRect(&rClient);
  Bitmap bmp(rClient.Width(), rClient.Height(), &graphicsOrig);
  Bitmap * pBmp = &bmp;
  Graphics g(pBmp);
  g.SetPageUnit(UnitPixel);
  GraphicsState gs = g.Save();
  ns_Gr::g_g.Set(rClient, &g);

  ns_Gr::g_g.Clear(ns_Gr::g_colorBg);
  g.SetSmoothingMode(SmoothingModeAntiAlias); // ��������� �����������
  CreateCoordinateAxises(); // ��������� ������������ ����

  CRect rGraph = ns_Gr::g_g.GetGraphRect(); // ������� ��������� ������������ �����, ���� ����� ���������� �������
  ns_Gr::g_nViewHeight = rGraph.Height() * ns_Gr::g_nZoomY;
  m_pDataObj->FormPointsByHeight(ns_Gr::g_nViewHeight); // ��� ����� ������ ��������������� �������� �������� ������� ������� ��������� �� ������ * �� �������

  // Scrolling handle

  CalcPointCount();
  SetScrollings();

  // Draw

  map<EPowerProfileType, vector<CUT_POINT>>* pData = m_pDataObj->GetDataPtr();
  if (pData)
  {
    DrawYGuidlines(g);

    bool bGrayBreakMarksWasDrawn = false;
    bool bDateMarksWasDrawn = false;
    CString sPrevDay;
    typedef tuple<PointF, PointF, bool> x1y1x2y2; // bool if point of many values in this half hour
    vector<x1y1x2y2> pointf;
    map<EPowerProfileType, vector<CUT_POINT>>::iterator iter = pData->begin();

    for (size_t d = 0; d < pData->size(); ++d, ++iter)
    {
      vector<CUT_POINT>* pPoints = &iter->second;

      // ��������� ������ �� ������� �������

      REAL nLineThickness = 2.0f;
      Pen penAP(ns_Gr::g_argbAP, nLineThickness);
      Pen penRQ_plus(ns_Gr::g_argbRQ_plus, nLineThickness);
      Pen penRQ_minus(ns_Gr::g_argbRQ_minus, nLineThickness);
      Pen penBreak1(0xD05F5F5F);
      Pen penBreak2(0xD0909090);
      SolidBrush brushAP(Color(255, GetRValue(ns_Gr::g_color_AP), GetGValue(ns_Gr::g_color_AP), GetBValue(ns_Gr::g_color_AP)));
      SolidBrush brushRQ_plus(Color(255, GetRValue(ns_Gr::g_color_RQ_plus), GetGValue(ns_Gr::g_color_RQ_plus), GetBValue(ns_Gr::g_color_RQ_plus)));
      SolidBrush brushRQ_minus(Color(255, GetRValue(ns_Gr::g_color_RQ_minus), GetGValue(ns_Gr::g_color_RQ_minus), GetBValue(ns_Gr::g_color_RQ_minus)));
      Pen* pPen = nullptr;
      SolidBrush* pBrush = nullptr;

      switch (iter->first)
      {
        case pptA:
          if (!ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_AP)
            break;
          pPen = &penAP;
          pBrush = &brushAP;
          break;

        case pptR_plus:
          if (!ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_RQ_plus)
            break;
          pPen = &penRQ_plus;
          pBrush = &brushRQ_plus;
          break;

        case pptR_minus:
          if (!ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_RQ_minus)
            break;
          pPen = &penRQ_minus;
          pBrush = &brushRQ_minus;
          break;

        case pptP:
          if (ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_AP)
            break;
          pPen = &penAP;
          pBrush = &brushAP;
          break;

        case pptQ_plus:
          if (ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_RQ_plus)
            break;
          pPen = &penRQ_plus;
          pBrush = &brushRQ_plus;
          break;

        case pptQ_minus:
          if (ns_Gr::g_bCurViewIsEnergy || !ns_Gr::g_bDraw_RQ_minus)
            break;
          pPen = &penRQ_minus;
          pBrush = &brushRQ_minus;
          break;
      }

      if (!pBrush)
        continue;

      // ��������� ������� ���� �� �������

      int nX_step = ns_Gr::g_nZoomX; // ��� �� � ��� ����� �����
      int nX = rGraph.left - nX_step; // ������ ��� ��������� �� � �� rGraph.left �� rGraph.right
      size_t iStart = ns_Gr::g_nXSegmentStart; // ������� ���� ���������� ����� �� �
      const int& nXLast = rGraph.right; // ��� � ���� ���`��� �������� (rGraph.right)
      REAL nY_prev = -1;

      for (size_t i = iStart; i < pPoints->size(); ++i)
      {
        auto& pp = pPoints->at(i);

        nX += nX_step; // �������
        if (nX > nXLast)
          break;

        if (!bDateMarksWasDrawn)
        {
          CString sDate;
          if (CompareValues(pp.sDate, sPrevDay, sDate))
            sPrevDay = sDate;
          DrawXGuidline(g, sDate, pp.sTime, nX);
        }

        INT& nY_mark = pp.nY; // ����� �� � - break ��� ������� �����
        if ((nY_mark == CUT_MARK_DAYS_SPACE) || (nY_mark == CUT_MARK_HALF_HOUR_SPACE)) // break �����
        {
          DrawCurvePoints(g, pPen, pBrush, pointf);
          pointf.clear();
          nY_prev = -1;

          // ��������� ��������� (break)

          int xx = (nX_step * ((nY_mark == CUT_MARK_DAYS_SPACE) ? 1 : 1));
          int x = 0;
          for (; x < xx; ++x)
          {
            int x_ = nX + x;
            if (x_ > nXLast)
              break;

            if (!bGrayBreakMarksWasDrawn)
            {
              const int& nTop = rGraph.top; // ������������ �����
              const int& nBottom = rGraph.bottom; // ������������ ����
              g.DrawLine((nY_mark == CUT_MARK_DAYS_SPACE) ? &penBreak1 : &penBreak2, Point(x_, nBottom), Point(x_, nTop)); // ������ ����� �����
            }
          }
          nX += (nY_mark == CUT_MARK_DAYS_SPACE) ? (x - nX_step) : 0;
          continue; // end draw space mark
        }

        auto lmbAddCoupleOfPoints = [&](int x1, REAL y1, int x2, REAL y2)->void
        {
          PointF pf1(static_cast<REAL>(x1), y1);
          PointF pf2(static_cast<REAL>(x2), y2);
          x1y1x2y2 xyxy(pf1, pf2, pp.bManyPoints);
          pointf.push_back(xyxy);
        };

        REAL nY_ = pp.fY_; // ����� �� �;

        if (nY_prev > -1 && nY_prev != nY_)
          lmbAddCoupleOfPoints(nX, nY_prev, nX, nY_); // ������� �� y

        lmbAddCoupleOfPoints(nX, nY_, nX + nX_step, nY_); // ����� (�������) �� � (�����, ���� ������� 1; �������, ���� ������� > 1)
        nY_prev = nY_;

      } // end curve`s points

      bGrayBreakMarksWasDrawn = true;
      bDateMarksWasDrawn = true;

      DrawCurvePoints(g, pPen, pBrush, pointf);
      pointf.clear();

    } // end draw curve
  }

  Status s = graphicsOrig.DrawImage(&bmp, 0, 0);
  ASSERT(s == S_OK);
  g.Restore(gs);
  //TRACE(L"OnPaint_2\r\n\r\n");
}

void CPowerProfileGraphs::DrawCurvePoints(Graphics& g,
                                          Pen* pPen,
                                          Brush* pBrush,
                                          vector<tuple<PointF, PointF, bool>>& pointf)
{
  if (pointf.empty())
    return;

  /*

  ��� ��������� ����������:
  - �������, ������� ����� ���� ������ ��� �������� �������� ��������;
  - ������� �� � (���������) �� ����� ����� (�������) � ������ (�������).

  ��������� ���������������� ������� �� �������������� ������������ ������� ���������.
  ��� ��� ������ ����� ���������� � ���� ���������, �� ������� �.�. �����:
  - x1 != x2 � y1 == y2 - �������������� ������� ��������;
  - x1 == x2 � y1 != y2 - ������������ ������� ��������;
  - x1 == x2 � y1 == y2 - ���� �����, ��� ������ ������� ��� �������� 1�1;
  - x1 != x2 � y1 != y2 - ��� ���� �� ������, �.�. ��� �����;

  ������ (� pointf �������) ������ ���������� � � �������.

  Descr.1 (������ �-��������������� �������)
  ����� ������� ����� �� ����������� � ������� ��������� ������������ ����� ��� ����.
  ��� ���� ������ ����������� ��������� �������:
  �) ��� ����� ���� ������ ���������:
  �1) ����� ������ ��������� ����� �������;
  �1) ����� ������ ��������� ����� ��������� break ���������
  �) ��� ����� ������ ����� �� ���� ��� ��� ����� ������� �-������� (����� ��������� ���� ��������
  ����� �������� ��� �������, ���� ����� ��� ���� ���������).
  � ����� �� ���� ������� ������ ����������� - res.empty().
  ����������� ������� - ePosition == eUnknown ���� ��� ������ �).
  ����������� ������� - ePosition == eTopOut ���� ��� ����� �), ����� ������ ���� ������ �� y �����.
  ����������� ������� - ePosition == eBottomOut ���� ��� ����� �), ����� ������ ���� ������ �� y ����.

  Descr.2 (������ �-������������� �������)
  �.�. ������ ���������� � �, �� ePosition � ������ ��� ������������ ���, ������� � � �������
  �� ������ ���� ������� ePosition == eUnknown;
  ���� ���������� ������� ����� ���� ���������� �������, �� ������ ����������� ������� -
  p1.Y < nTop;
  ���� ���������� ������� ����� ���� ���������� �������, �� ������ ����������� ������� -
  p1.Y > nBottom;

  */

  vector<pair<PointF, bool>> res;
  auto lmbDrawSegments = [&]()->void
  {
    if (res.empty())
      return;

    vector<PointF> dots;
    for (auto& r : res)
    {
      dots.push_back(r.first);
      if (r.second) // ��� ���� ����, ��� �� ������� �������� ��������� �������� ������������, �� �������
        // ���� ������� ���� ������������ (��. bManyPoints)
      {
        if (ns_Gr::g_nZoomX >= 4) // ������ ����� � ������ ��������
        {
          INT d = ns_Gr::g_nZoomX - 2;
          if (d > 5)
            d = 5;
          INT x = INT(r.first.X);
          INT y = INT(r.first.Y) - (d + 2); // �������� ������ � �������� � 2 �������
          g.FillEllipse(pBrush, Rect(x, y, d, d));
        }
      }
    }

    PointF* pf = &dots[0];
    g.DrawCurve(pPen, pf, dots.size(), 0.0f);
    res.clear();
  };

  CRect rGraph = ns_Gr::g_g.GetGraphRect(); // ������� ��������� ������������ �����, ���� ����� ���������� �������

  REAL nBottom = static_cast<REAL>(GetYBottomHide());
  REAL nTop = nBottom + static_cast<REAL>(rGraph.Height()); // ������������ �����

  enum
  {
    eUnknown, // ������� �� ��������
    eInto, // ������� � ������� ���������
    eTopOut, // ������ (�������) ���� ������ �� ����������� �
    eBottomOut // ������ (�������) ���� ����� �� ����������� �
  }
  ePosition = eUnknown;

  auto lmbTakeSegment = [&](PointF& p1, PointF& p2, bool bManyPoints)->void
  {
    // �������� ����� �� � � ��������� ���������: �������� ������ �� � ��� ��� � � �� ��� � GDI ���������� ��������� ������ ����
    p1.Y -= nBottom; // �������� ��������, ������� ������ ���������� ����������
    p1.Y = static_cast<REAL>(rGraph.bottom) - p1.Y; // ������� ����� ����������� �� ���� ������� ������� (��� ����� ������ ��� �) �����
    p2.Y -= nBottom;
    p2.Y = static_cast<REAL>(rGraph.bottom) - p2.Y;

    res.push_back(pair<PointF, bool>(p1, bManyPoints));
    res.push_back(pair<PointF, bool>(p2, false));
  };

  for (auto& p : pointf)
  {
    PointF p1 = get<0>(p);
    PointF p2 = get<1>(p);
    bool bManyPoints = get<2>(p);
    ASSERT(!(p1.X != p2.X && p1.Y != p2.Y));

    if (p1.X != p2.X || // ������ �-��������������� �������; ��. ���� Descr.1
        (p1.X == p2.X && p1.Y == p2.Y))
    {
      if (p1.Y < nBottom) // �� ����������� �� ����
      {
        ASSERT(res.empty());
        ASSERT(ePosition == eUnknown || ePosition == eBottomOut);
        ePosition = eBottomOut;
      }
      else
      if (p1.Y > nTop) // �� ����������� �� �����
      {
        ASSERT(res.empty());
        ASSERT(ePosition == eUnknown || ePosition == eTopOut);
        ePosition = eTopOut;
      }
      else
      {
        ASSERT(ePosition == eUnknown || ePosition == eInto);
        lmbTakeSegment(p1, p2, bManyPoints);
        ePosition = eInto;
      }
    }

    if (p1.Y != p2.Y) // ������ �-������������� �������; ��. ���� Descr.2
    {
      bool bTakeYSeg = false;
      bool bDrawCast = false;

      switch (ePosition)
      {
        case eUnknown: ASSERT(false); break;

        case eTopOut:
          ASSERT(p1.Y > nTop);
          if (p2.Y > nTop) // ������� ��� ��� ��� ������� ���������
            break;
          bTakeYSeg = true; // ������� ��� ���� �����
          p1.Y = nTop;
          if (p2.Y < nBottom) // ������������ ������� �������� �������� ������� ���������
          {
            p2.Y = nBottom;
            bDrawCast = true;
            ePosition = eBottomOut;
          }
          else
            ePosition = eInto;
          break;

        case eBottomOut:
          ASSERT(p1.Y < nBottom);
          if (p2.Y < nBottom) // ������� ��� ��� ��� ������� ���������
            break;
          bTakeYSeg = true; // ������� ��� ���� �����
          p1.Y = nBottom;
          if (p2.Y > nTop) // ������������ ������� �������� �������� ������� ���������
          {
            p2.Y = nTop;
            bDrawCast = true;
            ePosition = eTopOut;
          }
          else
            ePosition = eInto;
          break;

        case eInto:
          ASSERT(p1.Y <= nTop && p1.Y >= nBottom);
          bTakeYSeg = true;
          if (p2.Y > nTop) // ������������ ������� ������ �����
          {
            p2.Y = nTop;
            bDrawCast = true;
            ePosition = eTopOut;
          }
          if (p2.Y < nBottom) // ������������ ������� ������ ����
          {
            p2.Y = nBottom;
            bDrawCast = true;
            ePosition = eBottomOut;
          }
          break;

        default: ASSERT(false);
      }

      if (bTakeYSeg)
      {
        lmbTakeSegment(p1, p2, false_d(L"�� Y ��������������� ����� �� �������������"));
        if (bDrawCast)
          lmbDrawSegments();
      }
    }
  }

  lmbDrawSegments();
}

void CPowerProfileGraphs::CreateCoordinateAxises()
{
  CAi_GdiPlus& gg = ns_Gr::g_g;
  gg.UseFont();
  gg.UseBrushColor();

  CString sX = IsCurViewEnergy() ? L"�������, ���*� (����*�)" : L"��������, �� (���)";
  CString sY = L"�����";
  gg.DrawCoordinateAxises(sX, sY, 70);
}

void CPowerProfileGraphs::DrawYGuidlines(Graphics& g)
{
  CAi_GdiPlus& gg = ns_Gr::g_g;
  
  int nCountPieces = 6;
  double fMaxV = 0.0;
  double fUnitsInPixel = 0.0;
  m_pDataObj->GetDataValue(fMaxV, fUnitsInPixel);
  double fHSizeInView = fMaxV / ns_Gr::g_nZoomY;
  double fStep = fHSizeInView / nCountPieces;
  double fStart = GetYBottomHide() * fUnitsInPixel;

  vector<CString> t;
  if (fMaxV == 0.0)
  {
    t.push_back(L"0.0");
    t.push_back(L"1.0");
  }
  else
  {
    for (int i = 0; i < nCountPieces + 1; ++i)
    {
      if (fStart > fMaxV)
        fStart = fMaxV;
      t.push_back(CAi_Str::ToString(fStart));
      fStart += fStep;
    }
  }

  gg.DrawYGuidlines(t);
}

void CPowerProfileGraphs::DrawXGuidline(Gdiplus::Graphics& g, CString& sDate, CString& sTime, int nX)
{
  if (sDate == L"Break")
    return;

  CAi_GdiPlus& gg = ns_Gr::g_g;
  CRect rGraph = gg.GetGraphRect();
  SolidBrush brush(Color(0x45, 255, 255, 255));
  SolidBrush brushT(Color::White);
  Pen pen(Color(0xE0, 255, 255, 255), 1);
  Pen pen2(Color(0xE0, 255, 255, 255), 1);
  pen.SetDashStyle(DashStyleDashDotDot);

  Gdiplus::Font fnt(L"Arial", 10);
  INT yTop = rGraph.top - 10;
  INT yBottom = rGraph.bottom + 7;
  INT xFrameLeft = nX - 7;
  INT nFrameW = 14;
  bool bText = false;

  if (sTime == L"00:00")
  {
    g.FillRectangle(&brush, Rect(xFrameLeft, yBottom, nFrameW, sDate.GetLength() * 7));
    g.DrawLine(&pen, nX, yBottom, nX, yTop);
    RectF rTx(REAL(xFrameLeft - 3), REAL(yBottom + 1), 20, REAL(sDate.GetLength() * 8));
    StringFormat sf;
    sf.SetFormatFlags(StringFormatFlagsDirectionVertical);
    g.DrawString(sDate, sDate.GetLength(), &fnt, rTx, &sf, &brushT);
  }

  Gdiplus::Font fnt2(L"Arial", 8);
  nFrameW = 29;
  INT nFrameH = 13;
  SolidBrush brushH1(Color(0x45, 255, 255, 255));
  SolidBrush brushH2(Color(0x15, 255, 255, 255));
  SolidBrush* pBbrushH = &brushH1;
  int nBtmAdv = 17;

  if (ns_Gr::g_nZoomX == 1)
  {
    if (sTime != L"12:00")
      return;
    bText = true;
  }
  else
  {
    if (sTime == L"00:00")
      return;
    if (sTime.Right(2) != L"00")
      return;

    if (ns_Gr::g_nZoomX == 2)
    {
      if (sTime == L"12:00")
        bText = true;
      else
        return;
    }

    if (ns_Gr::g_nZoomX >= 3)
    {
      if (sTime == L"06:00"
          || sTime == L"12:00"
          || sTime == L"18:00")
      {
        bText = true;
        yBottom += 20;
        nBtmAdv += 20;
      }

      if (ns_Gr::g_nZoomX >= 4)
      {
        pBbrushH = &brushH2;

        if (sTime == L"03:00"
            || sTime == L"09:00"
            || sTime == L"15:00"
            || sTime == L"21:00")
          bText = true;

        if (ns_Gr::g_nZoomX >= 15)
          bText = true;

      }
    }
  }

  if (bText)
  {
    g.FillRectangle(&brush, Rect(xFrameLeft - 7, yBottom, nFrameW, nFrameH));
    PointF pTx(REAL(xFrameLeft - 8), REAL(yBottom));
    g.DrawString(sTime, sTime.GetLength(), &fnt2, pTx, &brushT);
    g.DrawLine(&pen2, nX, yBottom, nX, yBottom - (nBtmAdv - 12));
  }

  if (ns_Gr::g_nZoomX == 1)
    g.DrawLine(&pen, nX, yBottom, nX, yTop);
  else
  {
    int nX_step = ns_Gr::g_nZoomX;
    g.FillRectangle(pBbrushH, Rect(nX, yTop, nX_step, rGraph.Height() + nBtmAdv));
  }
}

void CPowerProfileGraphs::CalcPointCount()
{
  // X
   
  ns_Gr::g_nXSegmentCount = 0;
  int nCount = 0;

  map<EPowerProfileType, vector<CUT_POINT>>* pData = m_pDataObj->GetDataPtr();
  if (!pData)
    return;

  map<EPowerProfileType, vector<CUT_POINT>>::iterator iter = pData->begin();
  for (size_t d = 0; d < pData->size(); ++d, ++iter)
  {
    vector<CUT_POINT>* pPoints = &iter->second;
    nCount = (int)pPoints->size();
    break;
  }

  ns_Gr::g_nXSegmentCount = nCount;

  // Y

  // --- )
}

void CPowerProfileGraphs::SetScrollings()
{
  CRect rClient{};
  GetClientRect(rClient);

  CRect rGraph = ns_Gr::g_g.GetGraphRect();
  int nWidthPerSeg = rGraph.Width() / ns_Gr::g_nZoomX; // ������� ��������� ���������� � ������ ������� ������� �������
  int nSegOverRight = ns_Gr::g_nXSegmentCount - nWidthPerSeg; // ������� ��������� ��� �� ������
  if (nSegOverRight < 0)
    nSegOverRight = 0;
  int nMax = rClient.Width() + nSegOverRight; // ������� ����� ������ ��� ����� ����� �������
  if (nSegOverRight == 0) // ��� ����� �����������
  {
    nMax = 0; // ������ scroll
    ns_Gr::g_nXSegmentStart = 0;
  }
  else
  {
    if (ns_Gr::g_nXSegmentStart > nSegOverRight)
      ns_Gr::g_nXSegmentStart = nSegOverRight;
  }
  int nPos = ns_Gr::g_nXSegmentStart;

  SCROLLINFO sih{};
  sih.cbSize = sizeof(SCROLLINFO);
  sih.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
  sih.nMin = 0;
  sih.nMax = nMax;
  sih.nPage = rClient.Width();
  sih.nPos = nPos;
  sih.nTrackPos = 0;
  SetScrollInfo(SB_HORZ, &sih);

  nMax = rGraph.Height() * ns_Gr::g_nZoomY;
  if (ns_Gr::g_nZoomY == 1)
  {
    nMax = 0;
    ns_Gr::g_nYPosStart = -1;
  }
  else
  {
    if (ns_Gr::g_nYPosStart == -1) // ������ ��������� scrolling-�
      ns_Gr::g_nYPosStart = nMax;
  }

  SCROLLINFO siv{};
  siv.cbSize = sizeof(SCROLLINFO);
  siv.fMask = SIF_PAGE | SIF_POS | SIF_RANGE;
  siv.nMin = 0;
  siv.nMax = nMax;
  siv.nPage = rGraph.Height();
  siv.nPos = ns_Gr::g_nYPosStart;
  siv.nTrackPos = 0;
  SetScrollInfo(SB_VERT, &siv);
}

int CPowerProfileGraphs::GetYBottomHide()
{
  int minpos;
  int maxpos;
  GetScrollRange(SB_VERT, &minpos, &maxpos);
  maxpos = GetScrollLimit(SB_VERT);
  int curpos = GetScrollPos(SB_VERT);
  return maxpos - curpos;
}

void CPowerProfileGraphs::SetZoomX(int z)
{
  ns_Gr::g_nZoomX = z;
  Invalidate();
}

void CPowerProfileGraphs::SetZoomY(int z)
{
  ns_Gr::g_nZoomY = z;
  Invalidate();
}

void CPowerProfileGraphs::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  // Get the minimum and maximum scroll-bar positions. 
  int minpos;
  int maxpos;
  GetScrollRange(SB_HORZ, &minpos, &maxpos);
  maxpos = GetScrollLimit(SB_HORZ);

  // Get the current position of scroll box. 
  int curpos = GetScrollPos(SB_HORZ);

  // Determine the new position of scroll box. 
  switch (nSBCode)
  {
    case SB_LEFT:      // Scroll to far left.
      curpos = minpos;
      break;

    case SB_RIGHT:      // Scroll to far right.
      curpos = maxpos;
      break;

    case SB_ENDSCROLL:   // End scroll. 
      break;

    case SB_LINELEFT:      // Scroll left. 
      if (curpos > minpos)
        curpos--;
      break;

    case SB_LINERIGHT:   // Scroll right. 
      if (curpos < maxpos)
        curpos++;
      break;

    case SB_PAGELEFT:    // Scroll one page left.
    {
      // Get the page size. 
      SCROLLINFO   info;
      GetScrollInfo(SB_HORZ, &info, SIF_ALL);

      if (curpos > minpos)
        curpos = max(minpos, curpos - (int)info.nPage);
    }
    break;

    case SB_PAGERIGHT:      // Scroll one page right.
    {
      // Get the page size. 
      SCROLLINFO   info;
      GetScrollInfo(SB_HORZ, &info, SIF_ALL);

      if (curpos < maxpos)
        curpos = min(maxpos, curpos + (int)info.nPage);
    }
    break;

    case SB_THUMBPOSITION: // Scroll to absolute position. nPos is the position
      curpos = nPos;      // of the scroll box at the end of the drag operation. 
      break;

    case SB_THUMBTRACK:   // Drag scroll box to specified position. nPos is the
      curpos = nPos;     // position that the scroll box has been dragged to. 
      break;
  }

  // Set the new position of the thumb (scroll box).
  SetScrollPos(SB_HORZ, curpos);

  CWnd::OnHScroll(nSBCode, nPos, pScrollBar);

  ns_Gr::g_nXSegmentStart = curpos;
  Invalidate();
}

void CPowerProfileGraphs::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  // Get the minimum and maximum scroll-bar positions. 
  int minpos;
  int maxpos;
  GetScrollRange(SB_VERT, &minpos, &maxpos);
  maxpos = GetScrollLimit(SB_VERT);

  // Get the current position of scroll box. 
  int curpos = GetScrollPos(SB_VERT);

  // Determine the new position of scroll box. 
  switch (nSBCode)
  {
    case SB_TOP:      // Scroll to far left.
      curpos = minpos;
      break;

    case SB_BOTTOM:      // Scroll to far right.
      curpos = maxpos;
      break;

    case SB_ENDSCROLL:   // End scroll. 
      break;

    case SB_LINEUP:      // Scroll left. 
      if (curpos > minpos)
        curpos--;
      break;

    case SB_LINEDOWN:   // Scroll right. 
      if (curpos < maxpos)
        curpos++;
      break;

    case SB_PAGEUP:    // Scroll one page left.
    {
      // Get the page size. 
      SCROLLINFO info;
      GetScrollInfo(SB_VERT, &info, SIF_ALL);

      if (curpos > minpos)
        curpos = max(minpos, curpos - (int)info.nPage);
    }
    break;

    case SB_PAGEDOWN:      // Scroll one page right.
    {
      // Get the page size. 
      SCROLLINFO info;
      GetScrollInfo(SB_VERT, &info, SIF_ALL);

      if (curpos < maxpos)
        curpos = min(maxpos, curpos + (int)info.nPage);
    }
    break;

    case SB_THUMBPOSITION: // Scroll to absolute position. nPos is the position
      curpos = nPos;      // of the scroll box at the end of the drag operation. 
      break;

    case SB_THUMBTRACK:   // Drag scroll box to specified position. nPos is the
      curpos = nPos;     // position that the scroll box has been dragged to. 
      break;
  }

  // Set the new position of the thumb (scroll box).
  SetScrollPos(SB_VERT, curpos);

  CWnd::OnVScroll(nSBCode, nPos, pScrollBar);

  ns_Gr::g_nYPosStart = curpos;
  Invalidate();
}