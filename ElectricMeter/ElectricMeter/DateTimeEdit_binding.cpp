#include "stdafx.h"
#include "Typedefs.h"
#include "DateTimeEdit_binding.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace DateTimeEdit_bindCtrl
{
  //void(* BeforeSet)();
}

using namespace DateTimeEdit_bindCtrl;

CDateTimeEdit_binding::CDateTimeEdit_binding(void* pBeforeSetFunc)
{
  BeforeSet = (void(*)())pBeforeSetFunc;
}

CDateTimeEdit_binding::~CDateTimeEdit_binding()
{
}

bool CDateTimeEdit_binding::OnKeyUp_Enter()
{
  m_keywaspressed = keywaspressedEnter;
  if (m_pListCtrEx)
  {
    //BeforeSet();
    m_pListCtrEx->EditBindCtrlKillFocus(bSetVariable_d); // ��������� �������� �� ������
  }
  return true;
}

bool CDateTimeEdit_binding::OnKeyUp_Esc()
{
  m_keywaspressed = keywaspressedEsc;
  if (m_pListCtrEx)
    m_pListCtrEx->EditBindCtrlKillFocus(bUnsetVariable_d); // ��������� �������� �� ���������
  return true;
}

void CDateTimeEdit_binding::SaveEditableData()
{
  if (!m_pListCtrEx)
  {
    m_keywaspressed = keywaspressedNone;
    return;
  }

  switch (m_keywaspressed)
  {
    case keywaspressedNone: // ������ KillFocus ��� Enter ��� Esc
      m_pListCtrEx->EditBindCtrlKillFocus(bSetVariable_d); // ��������� �������� �� ������
      break;

    case keywaspressedEnter:
      m_keywaspressed = keywaspressedNone;
      break;

    case keywaspressedEsc:
      m_keywaspressed = keywaspressedNone;
      break;
  }
    
}
