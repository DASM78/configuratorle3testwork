#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "Ai_ListCtrlEx.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "TabCtrlOvrd.h"
#include "Ai_ColorStatic.h"
#include "Ai_CmbBx.h"
#include "afxwin.h"
#include "PowerProfileGraphs.h"
#include "PowerProfileGraphsData.h"
#include "Model_.h"
#include <vector>

class CView_PowerProfile;

class CTabCtrlOvrdZ: public CTabCtrlOvrd
{
  DECLARE_DYNCREATE(CTabCtrlOvrdZ)
public:
  CTabCtrlOvrdZ() { }
  CTabCtrlOvrdZ(CView_PowerProfile* pPP);
  ~CTabCtrlOvrdZ();
  
  DECLARE_MESSAGE_MAP()

  CView_PowerProfile* m_pPP = nullptr;

  afx_msg void OnBnClickedZoomHPlus();
  afx_msg void OnBnClickedZoomHMinus();
  afx_msg void OnBnClickedZoomVPlus();
  afx_msg void OnBnClickedZoomVMinus();

  afx_msg void OnCbxSelchangeZoomH();
  afx_msg void OnCbxSelchangeZoomV();
};

class CView_PowerProfile: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_PowerProfile)

public:
  CView_PowerProfile();
  virtual ~CView_PowerProfile();

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_DATA_POWER_PROFILE
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnSize_Tab_Graph();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  // Xml

  CParamsTreeNode* m_pPowerProfileNode = nullptr;
  void TakeValueFromXmlTree();
  void PutValueToXmlTree();

  // Tab

  CStatic m_stcTabSpace;
  CTabCtrlOvrdZ m_tabs;
  LRESULT OnChangeActiveTab(WPARAM wparam, LPARAM lparam);
  int m_nCurrPowerProfileTabIdx = 0;
  int m_nCurrPowerProfileTabIdx_prev = -1;

  CPowerProfileGraphs* m_pGraphView = nullptr;
  
  CPowerProfileGraphsData m_grhData;
  void CreateView_InGraphView();
  void CreateGraph_InGraphView(std::vector<POWPROF_DATA_ROW>* pData);

  CCommonListCtrl* m_plstTable = nullptr;
  �Ai_ListCtrlEx* m_plstTableEx = nullptr;
  void CreateTableTab();
  void FillIn_InTableTbl();
  void ClearAllRows_InTableTbl();

  void ClearAllReqDataInDB_ForTableTbl();
  void ReceiveData_ForTableTbl();
  void FormAllReqsOfItems_ForTableTbl();
  bool IsReqItems_ForTableTbl();
  
  bool IsNewData_InTableTbl();
  void GetTimeIntervals_FromTableTbl(CString& sBegin, CString& sEnd);
  std::vector<POWPROF_DATA_ROW>* GetReceivedDataList_FromTableTbl();

private:
  virtual void OnConnectionStateWasChanged() override final;

  void CheckMeaningsViewByConnectionStatus();
  void CheckMeaningsViewByConnectionStatus_InGraphView();
  void CheckMeaningsViewByConnectionStatus_InTableTbl();

  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  void UpdMeaningsByRecvdDeviceData_InTableTbl(mdl::teFrameTypes& ft);
  void UpdMeaningsByRecvdDeviceData_InGraphView(mdl::teFrameTypes& ft);
  void FillDataOnTabs();

  bool IsTestViewData();
  void ClearTestViewData();

  // Load

  CMFCButton m_btnLoadPowerProfileFromDevice;
  afx_msg void OnBnClickedBtnLoadPowerProfileFromDevice();
  virtual void DoAfterClearLoadData() override final;

  CAi_ColorStatic m_stcBeginInterval;
  CAi_ColorStatic m_stcEndInterval;
  CFont m_biggerFont;

  // Clear

  CMFCButton m_btnClearPowerProfileInDevice;
  afx_msg void OnBnClickedBtnClearPowerProfileInDevice();

  void ClearUsingCtrls();

  // View

  CStatic m_stcGraphSelect;
  CStatic m_stcGrViewSep;
  CButton m_rdoEnergyView;
  CButton m_rdoPowerView;

  afx_msg void OnBnClickedRdoPowprofEnergy();
  afx_msg void OnBnClickedRdoPowprofPower();

  enum ECurrPPView_Energy
  {
    ecppvNone,
    ecppvEnergy,
    ecppvPower
  };
  ECurrPPView_Energy m_currPPPV_Energy = ecppvNone;

  void CheckView();
  void CheckView_InGraphView();
  void CheckView_InTableTbl();

  CAi_ColorStatic m_stcA_color;
  CAi_ColorStatic m_stcR_plus_color;
  CAi_ColorStatic m_stcR_minus_color;

  enum ECurrPPView_AR
  {
    ecppv2A_on,
    ecppv2A_off,
    ecppv2R_plus_on,
    ecppv2R_plus_off,
    ecppv2R_minus_on,
    ecppv2R_minus_off
  };
  ECurrPPView_AR m_currPPPV_A = ecppv2A_on;
  ECurrPPView_AR m_currPPPV_R_plus = ecppv2R_plus_on;
  ECurrPPView_AR m_currPPPV_R_minus = ecppv2R_minus_on;

  CButton m_chkA;
  CButton m_chkR_plus;
  CButton m_chkR_minus;

  afx_msg void OnBnClickedChkPowprofA();
  afx_msg void OnBnClickedChkPowprofRPlus();
  afx_msg void OnBnClickedChkPowprofRMinus();

  // Zooms

private:
  CMFCButton* m_pbtnPlusH = nullptr;
  CMFCButton* m_pbtnMinusH = nullptr;
  CMFCButton* m_pbtnPlusV = nullptr;
  CMFCButton* m_pbtnMinusV = nullptr;
  CAi_CmbBx* m_pcbxZoomH = nullptr;
  CAi_CmbBx* m_pcbxZoomV = nullptr;
  CFont m_fntCmbx;

 public:
  CString GetZoomX();
  void SetZoomX(CString s);
  void SetZoomX();

  CString GetZoomY();
  void SetZoomY(CString s);
  void SetZoomY();

  void OnBnClickedZoomHPlus();
  void OnBnClickedZoomHMinus();
  void OnBnClickedZoomVPlus();
  void OnBnClickedZoomVMinus();

  afx_msg void OnCbxSelchangeZoomH();
  afx_msg void OnCbxSelchangeZoomV();
};
