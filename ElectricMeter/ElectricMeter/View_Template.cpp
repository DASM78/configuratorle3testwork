#include "stdafx.h"
#include "ElectricMeter.h"
#include "View_Template.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_Template, CCommonViewForm)

CView_Template::CView_Template()
: CCommonViewForm(IDD_DATA_NET_PROPERTIES)
{
}

CView_Template::~CView_Template()
{
}

BEGIN_MESSAGE_MAP(CView_Template, CCommonViewForm)
END_MESSAGE_MAP()

void CView_Template::DoDataExchange(CDataExchange* pDX)
{
}

void CView_Template::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();
}

void CView_Template::OnSize_()
{
  //if (!m_lstEnergy.m_hWnd)
  //  return;

  CRect rect{};
  GetClientRect(&rect);

  //SpreadCtrlToRightSide(&m_lstEnergy, this, 100);
}

void CView_Template::OnDraw(CDC* pDC)
{
}

#ifdef _DEBUG
void CView_Template::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_Template::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

