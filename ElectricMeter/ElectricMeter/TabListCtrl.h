#pragma once
#include "afxlistctrl.h"
#include "Ai_ListCtrlEx.h"

class CTabListCtrl : public CMFCListCtrl
{
public:
  CTabListCtrl(CWnd* pParentMng, int id);
  ~CTabListCtrl();

protected:
  DECLARE_MESSAGE_MAP()

private:
  const int m_nID = 0;
  �Ai_ListCtrlEx* m_pListEx = nullptr;
  CWnd* m_pParentMng = nullptr;
  CWnd* m_pParent = nullptr;
  bool m_bFirstAppearance = false;
  int m_nPrevBoldRowText_inView = -1;

  struct COLUMNS
  {
    int nDescr = 0;
  }
  m_columns;

  afx_msg void OnNMCustomdrawLst(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedLst(NMHDR *pNMHDR, LRESULT *pResult);

public:
  void Free();
  int GetID();

  void InsertRowsInOutlookTabLists(CWnd* pParent);
  void Resize();
  bool IsSelectedRow();
  void SelectFirstRow();
  void SelectSpecRow(int nDescrID);

  struct DATA_OF_SELECTED_ITEM
  {
    int nItemID = 0;
    CString sDescr;
  }
  m_dataSelectedItem;
};

