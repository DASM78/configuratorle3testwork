#pragma once

#include "StdAfx.h"
#include "Model_.h"
#include <map>
#include <vector>
#include "Typedefs.h"

typedef std::vector<std::pair<unsigned int, varOnce_t>> dataList_t;
typedef std::map<unsigned int, varMeaning_t> dataTypesAndVarMeanings_t; // address, meaning

struct SEND_UNIT
{
  mdl::teFrameTypes sendType = mdl::FRAME_TYPE_EMPTY;
  mdl::teCodingViews codingViews = mdl::cv_None;
  char* pszData = nullptr;
  int nDataSize = 0;

  SEND_UNIT& operator=(const SEND_UNIT& su)
  {
    if (this == &su)
      return *this;

    sendType = su.sendType;
    nDataSize = su.nDataSize;
    pszData = new char[nDataSize + 1]{};
    codingViews = su.codingViews;
    CopyMemory(pszData, su.pszData, nDataSize);
    return *this;
  }
};

class CSendUnit // ����� ������� SEND_UNIT ��� ������������� ������������ ���� new/delete
{
public:
  SEND_UNIT m_u;

  CSendUnit(mdl::teFrameTypes sendType, char* value = nullptr)
  {
    mdl::DataSetEx ds;
    unsigned int nAddress = 0;
    if (GetDataFrameAddress(sendType, nAddress)
        && GetDataSetEx(&ds, nAddress))
    {
      m_u.sendType = sendType;
      m_u.codingViews = ds.tD.eCodingViews;
      char* szAddr = ds.tD.cAddr;
      int nSize_addr = strlen(szAddr);
      m_u.nDataSize = nSize_addr;
      int nSize_value = 0;
      if (value)
      {
        nSize_value = strlen(value);
        m_u.nDataSize += nSize_value;
      }
      m_u.pszData = new char[m_u.nDataSize + 1]{}; // +1 - ���-�� ��� ������� (���������) ������� ���� � ������ �� ����������� ����� ����� ������
      CopyMemory(m_u.pszData, szAddr, nSize_addr);
      if (value)
        CopyMemory(m_u.pszData + nSize_addr, value, nSize_value);
    }
  }
  ~CSendUnit()
  {
    delete m_u.pszData;
    m_u.pszData = nullptr;
  }
  SEND_UNIT* Get()
  {
    if (m_u.sendType == mdl::FRAME_TYPE_EMPTY)
      return nullptr;
    return &m_u;
  }
};