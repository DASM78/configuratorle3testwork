#pragma once

#include "afxcmn.h"
#include "afxbutton.h"
#include "afxwin.h"
#include "Ai_ColorStatic.h"
#include "CommonViewForm.h"
#include "CommonViewDealer.h"
#include "Model_.h"

class CView_Time: public CCommonViewForm, public CCommonViewDealer
{
	
  DECLARE_DYNCREATE(CView_Time)

public:
  CView_Time();
  virtual ~CView_Time();

  enum { IDD = IDD_SETTINGS_TIME };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  DECLARE_MESSAGE_MAP()
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  virtual void OnTimer250() override final;

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  // XML

  void PutValueToXmlTree();

  //

  CFont m_biggerFont;
  CFont m_biggerFont2;

  CStatic m_stcTimeCol;
  CStatic m_stcDateCol;
  CStatic m_stcSeasonCol;
  CStatic m_stcRecordCol;
  CStatic m_stcDeviceRow;
  CStatic m_stcSystemRow;
  CStatic m_stcUserRow;

  CAi_ColorStatic m_stcDeviceTime;
  CAi_ColorStatic m_stcDeviceDate;
  CAi_ColorStatic m_stcDeviceSeason;

  CStatic m_stcSystemTime;
  CStatic m_stcSystemDate;
  CStatic m_stcSystemSeason;

  CDateTimeCtrl m_datUserTime;
  CDateTimeCtrl m_datUserDate;
  CButton m_rdoUserOnSeason;
  CButton m_rdoUserOffSeason;
  CButton m_chkUserTime;
  CButton m_chkUserDate;
  CButton m_chkUserSeason;

  CMFCButton m_btnRecordSystem;
  CMFCButton m_btnRecordUser;

  afx_msg void OnBnClickedChkChangeTime();
  afx_msg void OnBnClickedChkChangeDate();
  afx_msg void OnBnClickedChkChangeSeason();
  afx_msg void OnBnClickedRdoUseSeasons();

  afx_msg void OnDtnDatetimechangeTimeChanger(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnDtnDatetimechangeDateChanger(NMHDR *pNMHDR, LRESULT *pResult);

  // Record

  virtual void OnConnectionStateWasChanged() override final;
  void UpdateInlayByReceivedConfirmation (mdl::teFrameTypes ft, bool bResult);
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  afx_msg void OnBnClickedBtnRecordSystemTimeToDevice();
  afx_msg void OnBnClickedBtnRecordUserTimeToDevice();
};
