#pragma once

#include "CommonViewForm.h"

class CView_Template: public CCommonViewForm
{
  DECLARE_DYNCREATE(CView_Template)

public:
  CView_Template();
  virtual ~CView_Template();

  DECLARE_MESSAGE_MAP()

private:

  enum
  {
    IDD = IDD_DATA_NET_PROPERTIES
  };

  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);  // overridden to draw this view

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif
};
