#pragma once

#include "CommonViewForm.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "CommonListCtrl.h"
#include "CommonViewDealer.h"
#include "afxbutton.h"
#include "Ai_TabCtrl.h"
#include "Ai_ColorStatic.h"
#include "Ai_Static.h"
#include "Ai_ToolTip.h"
#include "DateTimeEdit_binding.h"
#include "ComboBox_binding.h"
#include "TabCtrlOvrd.h"
#include "Model_.h"
#include <vector>

class CView_TariffShedules: public CCommonViewForm, public CCommonViewDealer
{
public:
  CView_TariffShedules();
  virtual ~CView_TariffShedules();

  DECLARE_DYNCREATE(CView_TariffShedules)

  DECLARE_MESSAGE_MAP()

  enum
  {
    IDD = IDD_SETTINGS_TARIFF_SHEDULES
  };

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();
  virtual void OnDraw(CDC* pDC);
  virtual bool OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  virtual void OnNMDblclkLst(CListCtrl* pCtrl, int nRow_inView, int nCell) override final;
  virtual BOOL PreTranslateMessage(MSG* pMsg);

#ifdef _DEBUG
  virtual void AssertValid() const;
  virtual void Dump(CDumpContext& dc) const;
#endif

  afx_msg void OnContextMenu(CWnd*, CPoint point);

  virtual void OnConnectionStateWasChanged() override final;
  void CheckMeaningsViewByConnectionStatus();
  virtual void UpdateParamMeaningsByReceivedDeviceData(mdl::teFrameTypes ft = mdl::FRAME_TYPE_EMPTY) override final;
  bool IsReqItems_ForTablesl();

  void UpdateParamMeaningsByReceivedDeviceData_24Hours(mdl::teFrameTypes ft);
  bool IsReqItems_24Hours();
  int GetMaxColumnCount_InReceivedData_24Hours();
  void FormRightSideByMaxColumn_24Hours(int nMaxColumn);
  void FormAllReadRowReq_24Hours();
  void UpdateParamMeaningsByReceivedDeviceData_Seasons(mdl::teFrameTypes ft);
  bool IsReqItems_Seasons();
  void FormAllReadRowReq_Seasons();
  void UpdateParamMeaningsByReceivedDeviceData_SpecDays(mdl::teFrameTypes ft);
  bool IsReqItems_SpecDays();
  void FormAllReadRowReq_SpecDays();

  void SendRateMode();
  void SendAllShedules();
  virtual void UpdateInlayByReceivedConfirmation(mdl::teFrameTypes ft, bool bResult) override final;

  void EnableCtrlByStrongExchange(BOOL bEnable);

  // Xml file

private:
  CParamsTreeNode* m_pTariffShedulesNode = nullptr;
  void TakeValueFromXmlTree();

  void FreeBDCellProps_24Hours();
  void TakeValueFromXmlTree_24Hours();
  void TakeValueFromXmlTree_24Hours_newXmlVer();
  void ParseRowDataString_24Hours(CString& sRowData, int j);
  void ParseRowDataString_24Hours_newXmlVer(CString& sRowData, int j);

  void TakeValueFromXmlTree_Seasons();
  void TakeValueFromXmlTree_Seasons_newXmlVer();
  void ParseRowDataString_Seasons(CString& sRowData, int j);

  void TakeValueFromXmlTree_SpecDays();
  //void TakeValueFromXmlTree_SpecDays_newXmlVer();
  void ParseRowDataString_SpecDays(CString& sRowData, int j);

  void PutValueToXmlTree();
  void PutValueToXmlTree_24Hours();
  void PutValueToXmlTree_Seasons();
  void PutValueToXmlTree_SpecDays();

  CString GetXmlTag_24Hours();
  CString GetXmlTag_Seasons();
  CString GetXmlTag_SpecDays();

  void FormStringFrom_24Hours(std::vector<CString>& res, bool bCheck, bool bForXml);
  void FormStringFrom_Seasons(std::vector<CString>& res);
  void FormStringFrom_SpecDays(std::vector<CString>& res);

  CMFCButton m_btnOpenTariffFromSpecXmlFile;
  CMFCButton m_btnSaveTariffToSpecXmlFile;
  afx_msg void OnBnClickedBtnOpenTariffFromFile();
  afx_msg void OnBnClickedBtnSaveTariffToFile();

  // Shedule settings (add/hide columns; add/hide rows)

private:
  CMFCButton m_btnSwitchOnTableSettings;
  bool m_bSwitchOnCurrSettings = false;
  afx_msg void OnBnClickedBtnSwitchOnTableSettings();

  CMFCButton m_btnCheckAll;
  CMFCButton m_btnCheckCurr;
  CMFCButton m_btnAddRow;
  CMFCButton m_btnDelRow;
  CMFCButton m_btnAddColumn;
  CMFCButton m_btnDelColumn;

  CAi_ToolTip m_toolTip;
  
  afx_msg void OnBnClickedBtnAddColumn();
  afx_msg void OnBnClickedBtnDelColumn();
  afx_msg void OnBnClickedBtnAddRow();
  afx_msg void OnBnClickedBtnDelRow();
  afx_msg void OnBnClickedBtnCheckAll();
  afx_msg void OnBnClickedBtnCheckCurr();
  bool CheckDataOfTariffTables(int nTabIdx, CString& sResultMsg);

  void CheckSettBtnToolTips();
  void CheckSettBtnEnable();

  void CheckSettBtnEnable_For24HoursTbl();
  void OnBnClickedBtnAddColumn_In24HoursTbl();
  void OnBnClickedBtnDelColumn_In24HoursTbl();
  void OnBnClickedBtnAddRow_In24HoursTbl();
  void OnBnClickedBtnDelRow_In24HoursTbl();
  void SetTextInColumnsAfterChangeVisible_In24HoursTbl(int nCol, bool bSet);

  void CheckSettBtnEnable_ForSeasonsTbl();
  void OnBnClickedBtnAddRow_InSeasonsTbl();
  void OnBnClickedBtnDelRow_InSeasonsTbl();

  void CheckSettBtnEnable_ForSpecDaysTbl();
  void OnBnClickedBtnAddRow_InSpecDaysTbl();
  void OnBnClickedBtnDelRow_InSpecDaysTbl();

  void Check_In24HoursTbl(CString& sErrorMsg);
  void Check_InSeasonsTbl(CString& sErrorMsg);
  void Check_InSpecDaysTbl(CString& sErrorMsg);


  // Tabs

  CStatic m_stcTabSpace;
  CTabCtrlOvrd m_tabs;
  LRESULT OnChangeActiveTab(WPARAM wparam, LPARAM lparam);
  int m_nCurrSheduleTabIdx = 0;

  CCommonListCtrl* m_plst24Hours;
  �Ai_ListCtrlEx* m_plst24HoursEx = nullptr;
  void Create24HoursTab();
  void OnDblClick_In24HoursTable(int nRow_inView, int nCell);
  int GetSheduleCount_From24HoursTbl();

  CCommonListCtrl* m_plstSeasons;
  �Ai_ListCtrlEx* m_plstSeasonsEx = nullptr;
  void CreateSeasonsTab();
  void OnDblClick_InSeasonsTable(int nRow_inView, int nCell);

  CCommonListCtrl* m_plstSpecDays;
  �Ai_ListCtrlEx* m_plstSpecDaysEx = nullptr;
  void CreateSpecDaysTab();
  void OnDblClick_InSpecDaysTable(int nRow_inView, int nCell);

  bool IsSheduleUsing(int nShedule);
  bool IsSheduleUsing_InSeasonsTbl(int nShedule);
  bool IsSheduleUsing_InSpecDaysTbl(int nShedule);

  void UpdateView_In24HoursTbl(bool bUpdateCellText = false);
  void UpdateView_InSeasonsTbl(bool bUpdateCellText = false);
  void UpdateView_InSpecDaysTbl(bool bUpdateCellText = false);

  afx_msg LRESULT MeanWasSetedAfterBindCtrl(WPARAM wParam /*HWND parent*/, LPARAM lParam /* Item ctrl ID */);
  void MeanWasSeted_In24HoursTbl();
  void MeanWasSeted_InSeasonsTbl();
  void MeanWasSeted_InSpecDaysTbl();

  void MeaningsWasChanged_In24HoursTbl();
  void MeaningsWasChanged_InSeasonsTbl();
  void MeaningsWasChanged_InSpecDaysTbl();

  void GetCutOfMeanings_In24HoursTbl();
  void GetCutOfMeanings_InSeasonsTbl();
  void GetCutOfMeanings_InSpecDaysTbl();

    //... bind ctrls

private:
  CDateTimeEdit_binding* m_pedtTime_In24HoursTbl = nullptr;
  CDateTimeEdit_binding* m_pedtDate_InSeasonsTbl = nullptr;
  CDateTimeEdit_binding* m_pedtDate_InSpecDaysTbl = nullptr;

private:
  CComboBox_binding* m_pcbxTariffs_In24HoursTbl = nullptr;
  CComboBox_binding* m_pcbxShedules_InSeasonsTbl = nullptr;
  CComboBox_binding* m_pcbxShedules_InSpecDaysTbl = nullptr;

  // Below ctrls

  CAi_ColorStatic m_stcCurrentTariff;
  CFont m_biggerFont;

  CStatic m_stcWorkModeFrame;
  CButton m_rdoOneTariffMode;
  CButton m_rdoMultiTariffMode;
  afx_msg void OnBnClickedRdoOneTariffMode();
  afx_msg void OnBnClickedRdoMultiTariffMode();

  CMFCButton m_btnLoadTariffFromDevice;
  afx_msg void OnBnClickedBtnLoadTariffFromDevice();
  virtual void DoAfterClearLoadData() override final;
  bool m_bShowLoadingSuccessResultMsg = false;
  void ShowLoadingSuccessResultMsg();

  CMFCButton m_btnRecordTariffToDevice;
  afx_msg void OnBnClickedBtnRecordTariffToDevice();

  void ClearAllReqDataInDB_For24HoursTbl();
  void ReceiveData_For24HoursTbl();
  void ClearAllReqDataInDB_ForSeasonsTbl();
  void ReceiveData_ForSeasonsTbl();
  void ClearAllReqDataInDB_ForSpecDaysTb();
  void ReceiveData_ForSpecDaysTbl();
};
