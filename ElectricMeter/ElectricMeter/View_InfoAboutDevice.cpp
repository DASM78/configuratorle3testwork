#include "stdafx.h"
#include "Resource.h"
#include "Defines.h"
#include "Ai_Str.h"
#include "View_InfoAboutDevice.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_InfoAboutDevice, CCommonViewForm)

namespace ns_InfAb
{
#define MAX_DEV_INFO 17

	struct COLUMNS
	{
		int nParamName = -1;
		int nMeaning = -1;
	}
	g_cols;

	vector <int> g_rows;

	vector<CString> g_data;

} // namespace ns_InfAb
using namespace ns_InfAb;

CView_InfoAboutDevice::CView_InfoAboutDevice()
	: CCommonViewForm(CView_InfoAboutDevice::IDD, L"CView_InfoAboutDevice")
	, m_lstInfo(&m_plstInfoEx, dynamic_cast<CCommonViewDealer*>(this))
{
}

CView_InfoAboutDevice::~CView_InfoAboutDevice()
{
	delete m_plstInfoEx;
}

BEGIN_MESSAGE_MAP(CView_InfoAboutDevice, CCommonViewForm)
	ON_BN_CLICKED(IDC_BTN_LOAD_ABOUT_FROM_DEVICE, &CView_InfoAboutDevice::OnBnClickedBtnLoadInfoFromDevice)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LST_DEVICE_ABOUT, &CView_InfoAboutDevice::OnLvnItemchangedLstDeviceAbout)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_ERR, &CView_InfoAboutDevice::OnBnClickedButtonClearErr)
END_MESSAGE_MAP()

void CView_InfoAboutDevice::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_LST_DEVICE_ABOUT, m_lstInfo);
	DDX_Control(pDX, IDC_BTN_LOAD_ABOUT_FROM_DEVICE, m_btnLoadInfoFromDevice);
}

#ifdef _DEBUG
void CView_InfoAboutDevice::AssertValid() const
{
	CCommonViewForm::AssertValid();
}

void CView_InfoAboutDevice::Dump(CDumpContext& dc) const
{
	CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_InfoAboutDevice::OnInitialUpdate()
{
	CCommonViewForm::OnInitialUpdate();

	SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

	m_lstInfo.ShowHorizScroll(false);
	m_lstInfo.EnableInterlacedColorScheme(true);

	m_plstInfoEx = new �Ai_ListCtrlEx();
	m_plstInfoEx->SetList(&m_lstInfo, m_hWnd);
	LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
	lResult = lResult;

	DWORD nStyleEx =
		LVS_EX_FULLROWSELECT
		| LVS_EX_SUBITEMIMAGES
		| LVS_EX_BORDERSELECT
		| LVS_EX_DOUBLEBUFFER
		| LVS_EX_GRIDLINES;

	bool bCheckBoxesInFirstColumn = false;
	m_plstInfoEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
	Set�Ai_ListCtrlEx(m_plstInfoEx);

	bool m_bGridLinesStyle = true;
	if (!m_bGridLinesStyle)
		m_lstInfo.SetExtendedStyle(m_lstInfo.GetExtendedStyle() & ~LVS_EX_GRIDLINES);

	ns_InfAb::g_cols.nParamName = m_plstInfoEx->SetColumnEx(L"��������", LVCFMT_LEFT);
	m_plstInfoEx->SetColumnWidthStyle(ns_InfAb::g_cols.nParamName, CS_WIDTH_FIX, 350);
	ns_InfAb::g_cols.nMeaning = m_plstInfoEx->SetColumnEx(L"��������"/* + CString(C_MATCH_WIDTH)*/, LVCFMT_LEFT);
	m_plstInfoEx->SetColumnWidthStyle(ns_InfAb::g_cols.nMeaning, CS_WIDTH_FIX, 350);
	m_plstInfoEx->MatchColumns();

	int nNodeIdx = m_plstInfoEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

	vector<CString> textOfCells(2);

	auto lmbAddRow = [&](CString s, int ft)->void
	{
		textOfCells[0] = s;
		int nRow_inList = m_plstInfoEx->SetChild(nNodeIdx, &textOfCells);
		m_plstInfoEx->SetCellFlag(nRow_inList, ns_InfAb::g_cols.nMeaning, ft);
		ns_InfAb::g_rows.push_back(nRow_inList);
	};

	lmbAddRow(L"��� ��������", ROW_IDTYPE_DEV_INF_TYPE);
	lmbAddRow(L"�������� �����", ROW_IDTYPE_DEV_INF_SER_N);
	lmbAddRow(L"���� ������������", ROW_IDTYPE_DEV_INF_CR_DATE);
	lmbAddRow(L"������ �/�", ROW_IDTYPE_DEV_INF_VER_SW);
	lmbAddRow(L"������ �/�", ROW_IDTYPE_DEV_INF_VER_FW);
	lmbAddRow(L"����������� ����������", ROW_IDTYPE_DEV_INF_NOM_U);
	lmbAddRow(L"��� ������� / ����������� (������������)", ROW_IDTYPE_DEV_INF_NOM_M_I);
	lmbAddRow(L"���������� ��������", ROW_IDTYPE_DEV_INF_CONST);
	lmbAddRow(L"���� ���������� ��������", ROW_IDTYPE_DEV_INF_R_OFF);
	lmbAddRow(L"��������� 1", ROW_IDTYPE_DEV_INF_COM1);
	lmbAddRow(L"��������� \"��������� 1\"", ROW_IDTYPE_DEV_INF_COM1_PR);
	lmbAddRow(L"��������� 2", ROW_IDTYPE_DEV_INF_COM2);
	lmbAddRow(L"��������� \"��������� 2\"", ROW_IDTYPE_DEV_INF_COM2_PR);
	lmbAddRow(L"��������� 3", ROW_IDTYPE_DEV_INF_COM3);
	lmbAddRow(L"��������� \"��������� 3\"", ROW_IDTYPE_DEV_INF_COM3_PR);
	lmbAddRow(L"��������� 4", ROW_IDTYPE_DEV_INF_COM4);
	lmbAddRow(L"��������� \"��������� 4\"", ROW_IDTYPE_DEV_INF_COM4_PR);

	lmbAddRow(L"��������������� ������", FRAME_TYPE_ALL_ERRORS);

	m_lstInfo.SetSpecColorColumn(ns_InfAb::g_cols.nParamName);
	m_plstInfoEx->UpdateView();

	AddCtrlForPositioning(&m_lstInfo, movbehConstTopAndBottomMargins, movbehConstLeftAndRigthMargins);

	CheckMeaningsViewByConnectionStatus();
	UpdateParamMeaningsByReceivedDeviceData();

	WatchForCtrState(&m_btnLoadInfoFromDevice, rightCommon);

	SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_InfoAboutDevice::BeforeDestroyClass()
{
}

void CView_InfoAboutDevice::OnSize_()
{
}

void CView_InfoAboutDevice::OnDraw(CDC* pDC)
{
	return;
}

bool CView_InfoAboutDevice::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
	return true;
}

void CView_InfoAboutDevice::OnBnClickedBtnLoadInfoFromDevice()
{
	ns_InfAb::g_data.clear();
	m_plstInfoEx->SelectAndShowChild(�Ai_ListCtrlEx::shaschSpecified, 0, false_d(L"mouse move"));
	m_plstInfoEx->ClearColumn(ns_InfAb::g_cols.nMeaning);
	m_plstInfoEx->UpdateView();
	// ������� ������������ ��������� � ��
	//FormListForClearReceivedData(FRAME_TYPE_DEV_INF_CNT);
	//int nStart = static_cast<int>(FRAME_TYPE_DEV_INF_TYPE);
	//int nEnd = static_cast<int>(FRAME_TYPE_DEV_INF_COM4_PR);
	//for (int i = nStart; i <= nEnd; ++i)
	//  FormListForClearReceivedData(static_cast<teFrameTypes>(i));
	ClearReceivedData(true_d(L"����� ������� ���������, ��������� � DoAfterClearLoadData()"));
}

void CView_InfoAboutDevice::DoAfterClearLoadData()
{
	SetParamTypeForRead(FRAME_TYPE_DEV_INF_STRUCT, true);
	SetParamTypeForRead(FRAME_TYPE_ALL_ERRORS, true);

}

void CView_InfoAboutDevice::OnConnectionStateWasChanged()
{
	CheckMeaningsViewByConnectionStatus();

	bool bEnable = IsConectionAdminAccess();
	CButton *pbtn = (CButton*)GetDlgItem(IDC_BUTTON_CLEAR_ERR);
	pbtn->EnableWindow(bEnable);
}

void CView_InfoAboutDevice::CheckMeaningsViewByConnectionStatus()
{
}

void CView_InfoAboutDevice::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
	if (WasErrorMsgBeShowed())
	{
		SetWaitCursor_answer(false);
		return;
	}
	varMeaning_t m = FindReceivedData(ft);
	if (m == L"\xF")
	{
		ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
		SetWaitCursor_answer(false);
		AfxMessageBox(L"������ �������� ���������� � ��������!", MB_ICONERROR);
		return;
	}
	if (ft == FRAME_TYPE_DEV_INF_STRUCT) // �������� ��������� � ����� � ��������
	{
		sCfg cfg = CCounterInfo::AsciiToCfg(m);
		for (auto& row_inList : ns_InfAb::g_rows)
		{
			teFrameTypes ftFlag = static_cast<teFrameTypes>(m_plstInfoEx->GetCellFlag(row_inList, ns_InfAb::g_cols.nMeaning));
			m_plstInfoEx->SetCellText(row_inList, ns_InfAb::g_cols.nMeaning, CCounterInfo::GetFieldNameFromeCfg(ftFlag, cfg));
		}
		m_plstInfoEx->UpdateView();
		SetWaitCursor_answer(false);
		return;
	}

	if (ft == FRAME_TYPE_ALL_ERRORS) {
		//		��� ����� ��������� ������ �������� ������ � ����� ������
		CString tmp;

		m.Format(_T("%x"), _wtoi(m.GetBuffer()));
		m_plstInfoEx->SetCellText(*(ns_InfAb::g_rows.end() - 1), ns_InfAb::g_cols.nMeaning, m);
		m_plstInfoEx->UpdateView();
	}

}

void CView_InfoAboutDevice::OnLvnItemchangedLstDeviceAbout(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CView_InfoAboutDevice::OnBnClickedButtonClearErr()
{

	SendDataToDevice(FRAME_TYPE_CLEAR_ERRORS);
	OnBnClickedBtnLoadInfoFromDevice();
	// TODO: Add your control notification handler code here
}
