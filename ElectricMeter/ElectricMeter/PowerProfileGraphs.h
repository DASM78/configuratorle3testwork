#pragma once

#include "Ai_ColorStatic.h"
#include "Ai_GdiPlus.h"
#include "PowerProfileGraphsData.h"
#include <vector>
#include <gdiplus.h>

class CPowerProfileGraphs: public CWnd
{
  DECLARE_DYNAMIC(CPowerProfileGraphs)

public:
  CPowerProfileGraphs();           // protected constructor used by dynamic creation
  virtual ~CPowerProfileGraphs();

private:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()

#ifdef _DEBUG
  virtual void AssertValid() const;
#ifndef _WIN32_WCE
  virtual void Dump(CDumpContext& dc) const;
#endif
#endif

  // Data

public:
  void SetDataObj(CPowerProfileGraphsData* p);
private:
  CPowerProfileGraphsData* m_pDataObj = nullptr;

public:
  bool SetCurrView(bool bCurViewIsEnergy, bool bDraw_AP, bool bDraw_RQ_plus, bool bDraw_RQ_minus);
  bool IsCurViewEnergy();

  // Paint

public:
  void SetColorForAR(CAi_ColorStatic* pA, CAi_ColorStatic* pR_plus, CAi_ColorStatic* pR_minus);
  
  void ClearCurves();

  void MoveWindow_(CRect& r);

private:
  virtual void OnDraw(CDC* /*pDC*/)
  { }
  virtual void PostNcDestroy()
  {
    // �������������� �� ��������� ������ 'delete this' ������ CView::PostNcDestroy()
    //  CScrollView::PostNcDestroy();
  }

  // Draw

  afx_msg BOOL OnEraseBkgnd(CDC* pDC);
  afx_msg void OnPaint();
  void DrawCurvePoints(
    Gdiplus::Graphics& g,
    Gdiplus::Pen* pPen,
    Gdiplus::Brush* pBrush,
    std::vector<std::tuple<Gdiplus::PointF, Gdiplus::PointF, bool>>& pointf);

  void CreateCoordinateAxises();
  void DrawYGuidlines(Gdiplus::Graphics& g);
  void DrawXGuidline(Gdiplus::Graphics& g, CString& sDate, CString& sTime, int nX);

  // Scrollings

  void CalcPointCount();
  void SetScrollings();
  int GetYBottomHide();

  // Zoom

public:
  void SetZoomX(int z);
  void SetZoomY(int z);

private:
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};