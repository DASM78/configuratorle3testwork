#include "stdafx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Defines.h"
#include "View_Energy.h"

using namespace std;
using namespace mdl;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CView_Energy, CCommonViewForm)

namespace ns_Inlay_Energy
{
  #define UID_CURRENT_DATA_TAB 0
  #define UID_DAILY_DATA_TAB 1
  #define UID_MONTHLY_DATA_TAB 2
  #define UID_DATA_FOR_YEARS_TAB 3

} // ns_Inlay_Energy

using namespace ns_Inlay_Energy;

CView_Energy::CView_Energy()
  : CCommonViewForm(CView_Energy::IDD, L"CView_Energy")
{
}

CView_Energy::~CView_Energy()
{
  if (m_plstCurrentDataEx)
  {
    m_plstCurrentDataEx->Destroy();
    delete m_plstCurrentDataEx;
  }
  if (m_plstCurrentData)
    delete m_plstCurrentData;

  if (m_plstDailyDataEx)
  {
    m_plstDailyDataEx->Destroy();
    delete m_plstDailyDataEx;
  }
  if (m_plstDailyData)
    delete m_plstDailyData;

  if (m_plstMonthlyDataEx)
  {
    m_plstMonthlyDataEx->Destroy();
    delete m_plstMonthlyDataEx;
  }
  if (m_plstMonthlyData)
    delete m_plstMonthlyData;

  if (m_plstDateForYearsEx)
  {
    m_plstDateForYearsEx->Destroy();
    delete m_plstDateForYearsEx;
  }
  if (m_plstDateForYears)
    delete m_plstDateForYears;
}

BEGIN_MESSAGE_MAP(CView_Energy, CCommonViewForm)
  ON_REGISTERED_MESSAGE(AFX_WM_CHANGE_ACTIVE_TAB, OnChangeActiveTab)
  ON_BN_CLICKED(IDC_BTN_LOAD_ENERGY_FROM_DEV, &CView_Energy::OnBnClickedBtnLoadEnergyFromDevice)
END_MESSAGE_MAP()

void CView_Energy::DoDataExchange(CDataExchange* pDX)
{
  CCommonViewForm::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STC_ENARGY_TAB_SPACE, m_stcTabSpace);
  DDX_Control(pDX, IDC_BTN_LOAD_ENERGY_FROM_DEV, m_btnLoadEnergyFromDevice);
}

void CView_Energy::OnInitialUpdate()
{
  CCommonViewForm::OnInitialUpdate();

  SetInitialUpdateFlag(false_d(L"��� ������� �� ����������"));

  CRect rectTab;
  m_stcTabSpace.GetWindowRect(&rectTab);
  ScreenToClient(&rectTab);

  m_tabs.Create(CMFCTabCtrl::STYLE_3D_ROUNDED, rectTab, this, IDC_TAB_ENERGY, CMFCTabCtrl::LOCATION_TOP);

  CCommonViewDealer* pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plstCurrentData = new CCommonListCtrl(&m_plstCurrentDataEx, pCmnViewDlr);
  m_plstCurrentData->UseMultiRowColumns();
  pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plstDailyData = new CCommonListCtrl(&m_plstDailyDataEx, pCmnViewDlr);
  m_plstDailyData->UseMultiRowColumns();
  pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plstMonthlyData = new CCommonListCtrl(&m_plstMonthlyDataEx, pCmnViewDlr);
  m_plstMonthlyData->UseMultiRowColumns();
  pCmnViewDlr = dynamic_cast<CCommonViewDealer*>(this);
  m_plstDateForYears = new CCommonListCtrl(&m_plstDateForYearsEx, pCmnViewDlr);
  m_plstDateForYears->UseMultiRowColumns();

  int nListStyle = WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_ALIGNLEFT | WS_BORDER | WS_TABSTOP;
  for (int tab = 0; tab < 4; ++tab)
  {
    CCommonListCtrl* pListCtrl = nullptr;
    UINT nIDC = 0;
    switch (tab)
    {
      case UID_CURRENT_DATA_TAB:
        pListCtrl = m_plstCurrentData;
        nIDC = IDC_LST_CURRENT_DATA;
        break;
      case UID_DAILY_DATA_TAB:
        pListCtrl = m_plstDailyData;
        nIDC = IDC_LST_DAILY_DATA;
        break;
      case UID_MONTHLY_DATA_TAB:
        nIDC = IDC_LST_MONTHLY_DATA;
        pListCtrl = m_plstMonthlyData;
        break;
      case UID_DATA_FOR_YEARS_TAB:
        nIDC = IDC_LST_DATA_FOR_YEARS;
        pListCtrl = m_plstDateForYears;
        break;
      default:
        ASSERT(false);
        return;
        break;
    }
    pListCtrl->Create(nListStyle, CRect(0, 0, 0, 0), &m_tabs, nIDC);
  }

  m_tabs.SetImageList(IDB_ENERGY_TABS, 16, RGB(255, 0, 255));
  
  m_tabs.AddTab(m_plstCurrentData, L"�������", 0);
  m_tabs.AddTab(m_plstDailyData, L"�� ����", 1);
  m_tabs.AddTab(m_plstMonthlyData, L"�� �������", 2);
  m_tabs.AddTab(m_plstDateForYears, L"�� �����", 3);

  CMFCTabCtrl::Location location = CMFCTabCtrl::LOCATION_TOP;
  m_tabs.SetLocation(location);
  m_tabs.EnableAutoColor(FALSE);
  m_tabs.RecalcLayout();

  AddCtrlForPositioning_Stretchable(&m_stcTabSpace);
  AddCtrlForPositioning_Stretchable(&m_tabs);

  CreateCurrentDataTab();
  CreateDMY_DataTab(dmyDaily);
  FillInDMY_DataTab(dmyDaily);
  CreateDMY_DataTab(dmyMonthly);
  FillInDMY_DataTab(dmyMonthly);
  CreateDMY_DataTab(dmyForYears);
  FillInDMY_DataTab(dmyForYears);

  WatchForCtrState(&m_btnLoadEnergyFromDevice, rightCommon);

  CheckMeaningsViewByConnectionStatus();
  UpdateParamMeaningsByReceivedDeviceData();

  SetInitialUpdateFlag(true_d(L"��� ������� ����������"));
}

void CView_Energy::BeforeDestroyClass()
{
}

void CView_Energy::OnSize_()
{
  if (!m_tabs.m_hWnd)
    return;
  m_tabs.RecalcLayout();
  m_tabs.RedrawWindow();
}

void CView_Energy::OnDraw(CDC* pDC)
{
}

bool CView_Energy::OnNMCustomdrawLst(CListCtrl* pCtrl, int nRow_inView, int nCell)
{
  return true;
}

#ifdef _DEBUG
void CView_Energy::AssertValid() const
{
  CCommonViewForm::AssertValid();
}

void CView_Energy::Dump(CDumpContext& dc) const
{
  CCommonViewForm::Dump(dc);
}
#endif //_DEBUG

void CView_Energy::OnContextMenu(CWnd*, CPoint point)
{
}

LRESULT CView_Energy::OnChangeActiveTab(WPARAM wparam, LPARAM lparam)
{
  m_nCurrEnergyTabIdx = (int)wparam;
  Set_AFX_WM_CHANGE_ACTIVE_TAB_Was(bSetVariable_d);

  //BOOL bEnableLoadButton = IsConnection();

  switch (m_nCurrEnergyTabIdx)
  {
    case UID_CURRENT_DATA_TAB:
      //bEnableLoadButton = FALSE;
      StartCurrentDataMonitoring();
      break;
    case UID_DAILY_DATA_TAB: break;
    case UID_MONTHLY_DATA_TAB: break;
    case UID_DATA_FOR_YEARS_TAB: break;
  }
  if (m_nCurrEnergyTabIdx_prev == UID_CURRENT_DATA_TAB)
  {
    m_nCurrEnergyTabIdx_prev = -1;
    StopCurrentDataMonitoring();
  }
  //m_btnLoadEnergyFromDevice.EnableWindow(bEnableLoadButton);

  m_nCurrEnergyTabIdx_prev = m_nCurrEnergyTabIdx;

  return 0;
}

void CView_Energy::OnConnectionStateWasChanged()
{
  CheckMeaningsViewByConnectionStatus();
}

void CView_Energy::CheckMeaningsViewByConnectionStatus()
{
  CheckMeaningsViewByConnectionStatus_InCurrentTbl();
  CheckMeaningsViewByConnectionStatus_InDMY_Tbl(dmyDaily);
  CheckMeaningsViewByConnectionStatus_InDMY_Tbl(dmyMonthly);
  CheckMeaningsViewByConnectionStatus_InDMY_Tbl(dmyForYears);
}

void CView_Energy::UpdateParamMeaningsByReceivedDeviceData(teFrameTypes ft)
{
  if (ft == FRAME_TYPE_EMPTY
      || (ft >= FRAME_TYPE_ENERGY_ACT_SUM && ft <= FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE8))
  {
    UpdMeaningsByRecvdDeviceData_InCurrentTbl(ft);
    return;
  }

  if (WasErrorMsgBeShowed())
  {
    SetWaitCursor_answer(false);
    return;
  }

  varMeaning_t m = FindReceivedData(ft);
  if (m == L"\xF")
  {
    ErrorMsgWasShowed(true_d(L"������ ��� ����������������"));
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �������� ������ �� �������!", MB_ICONERROR);
    return;
  }

  DMY_List dmy = dmyNone;
  if (ft >= FRAME_TYPE_DAY_ENERGY_CNT && ft <= FRAME_TYPE_DAY_ENERGY_128)
    dmy = dmyDaily;
  if (ft >= FRAME_TYPE_MONTH_ENERGY_CNT && ft <= FRAME_TYPE_MONTH_ENERGY_32)
    dmy = dmyMonthly;
  if (ft >= FRAME_TYPE_YEAR_ENERGY_CNT && ft <= FRAME_TYPE_YEAR_ENERGY_10)
    dmy = dmyForYears;

  if (dmy != dmyNone)
  {
    UpdMeaningsByRecvdDeviceData_InDMY_Tbl(ft, dmy);

    if (ft == FRAME_TYPE_YEAR_ENERGY_CNT)
    {
      if (IsReqItems_ForDMY_Tbl())
      {
        BeginFormingReadPack(true);

        FormAllReqsOfItemsOfDMY_Tbl(dmyDaily);
        FormAllReqsOfItemsOfDMY_Tbl(dmyMonthly);
        FormAllReqsOfItemsOfDMY_Tbl(dmyForYears);

        BeginFormingReadPack(false);
      }
      else
      {
        SetWaitCursor_answer(false);
        AfxMessageBox(L"������� �� �������� ������ �� �������!", MB_ICONINFORMATION);
      }
    }
    else
      if (IsRecreateDMY_Tbl(dmy))
      {
        FillInDMY_DataTab(dmy);
        ShowLoadingSuccessResultMsg(dmy);
      }
  }

}

void CView_Energy::OnBnClickedBtnLoadEnergyFromDevice()
{
  ClearAllReqDataInDB_ForDMY_Tbl();
  // ����� ������� ���������� �-� DoAfterClearLoadData
}

void CView_Energy::DoAfterClearLoadData()
{
  BeginFormingReadPack(true);
  ErrorMsgWasShowed(false_d(L"������ �� ����� ��� �� ����������������"));
  SetWaitCursor_answer(true);

  ClearAllDMY_Rows(dmyDaily);
  ClearAllDMY_Rows(dmyMonthly);
  ClearAllDMY_Rows(dmyForYears);

  ReceiveData_ForDMY_Tbl(dmyDaily);
  ReceiveData_ForDMY_Tbl(dmyMonthly);
  ReceiveData_ForDMY_Tbl(dmyForYears);

  BeginFormingReadPack(false);
}

void CView_Energy::ShowLoadingSuccessResultMsg(DMY_List dmy)
{
  bool bShowMsg = false;

  switch (dmy)
  {
    case dmyDaily:
      bShowMsg = (GetReqItemsFrom_ForDMY_Tbl(dmyMonthly) > 0
                  || GetReqItemsFrom_ForDMY_Tbl(dmyForYears) > 0) ? false : true;
      break;

    case dmyMonthly:
      bShowMsg = (GetReqItemsFrom_ForDMY_Tbl(dmyForYears) > 0) ? false : true; 
      break;

    case dmyForYears: bShowMsg = true; break;
  }

  if (bShowMsg)
  {
    SetWaitCursor_answer(false);
    AfxMessageBox(L"������ �� ������� ������� ��������� �� ��������!", MB_ICONINFORMATION);
  }
}
