#include "StdAfx.h"
#include "Resource.h"
#include "Ai_Str.h"
#include "Defines.h"
#include "View_Energy.h"

using namespace mdl;
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

namespace ns_Tab_CurrentData
{
  #define COUNT_OF_ENERGY_TARIFFS 8

  struct COLUMNS
  {
    int nEnergy = -1;
    int tariffs[COUNT_OF_ENERGY_TARIFFS];
    int nSum = -1;
  }
  g_columns;

  struct ROWS
  {
    int nA_plus = -1;
    int nR_plus = -1;
    int nR_minus = -1;
  }
  g_rows;

} // ns_Tab_CurrentData

using namespace ns_Tab_CurrentData;

void CView_Energy::CreateCurrentDataTab()
{
  m_plstCurrentData->ShowHorizScroll(false);
  m_plstCurrentData->EnableInterlacedColorScheme(true);

  m_plstCurrentDataEx = new �Ai_ListCtrlEx();
  m_plstCurrentDataEx->SetList(m_plstCurrentData, m_hWnd);
  LRESULT lResult = ::SendMessage(m_hWnd, CCM_GETVERSION, 0, 0);
  lResult = lResult;

  DWORD nStyleEx =
    LVS_EX_FULLROWSELECT
    | LVS_EX_SUBITEMIMAGES
    | LVS_EX_BORDERSELECT
    | LVS_EX_DOUBLEBUFFER
    | LVS_EX_GRIDLINES;

  bool bCheckBoxesInFirstColumn = false;
  m_plstCurrentDataEx->CreateList(bCheckBoxesInFirstColumn, nStyleEx);
  Set�Ai_ListCtrlEx(m_plstCurrentDataEx);

  bool m_bGridLinesStyle = true;
  if (!m_bGridLinesStyle)
    m_plstCurrentData->SetExtendedStyle(m_plstCurrentData->GetExtendedStyle() & ~LVS_EX_GRIDLINES);

  m_plstCurrentDataEx->SetCommonStyle(CMNS_DISABLE_ROW_IF_CHKBX_OFF);

  g_columns.nEnergy = m_plstCurrentDataEx->SetColumnEx(L"�������", LVCFMT_CENTER);
  m_plstCurrentDataEx->SetColumnWidthStyle(g_columns.nEnergy, CS_WIDTH_FIX, 100);
  for (int i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
  {
    g_columns.tariffs[i] = m_plstCurrentDataEx->SetColumnEx(L"������\n" + CAi_Str::ToString(i + 1), LVCFMT_CENTER);
    m_plstCurrentDataEx->SetColumnWidthStyle(g_columns.tariffs[i], CS_WIDTH_FIX, 60);
  }
  g_columns.nSum = m_plstCurrentDataEx->SetColumnEx(L"�����", LVCFMT_CENTER);
  m_plstCurrentDataEx->SetColumnWidthStyle(g_columns.nSum, CS_WIDTH_FIX, 100);

  m_plstCurrentDataEx->MatchColumns();

  int nNodeIdx = m_plstCurrentDataEx->SetNode(nullptr, nullptr, true); // ��� ������ � �����. ������� nRow_inList ���� ������ + 1 - �.�. ������ �������������� ������ ���� ������ � ���������

  vector<CString> textOfCells(1);
  textOfCells[0] = L"A+ (���*�)";
  g_rows.nA_plus = m_plstCurrentDataEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = L"R+ (����*�)";
  g_rows.nR_plus = m_plstCurrentDataEx->SetChild(nNodeIdx, &textOfCells);
  textOfCells[0] = L"R- (����*�)";
  g_rows.nR_minus = m_plstCurrentDataEx->SetChild(nNodeIdx, &textOfCells);

  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    SetParamTypeForCell(g_rows.nA_plus, g_columns.tariffs[i], (teFrameTypes)(FRAME_TYPE_ENERGY_ACT_SUM_RATE1 + i));
  SetParamTypeForCell(g_rows.nA_plus, g_columns.nSum, FRAME_TYPE_ENERGY_ACT_SUM);
  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    SetParamTypeForCell(g_rows.nR_plus, g_columns.tariffs[i], (teFrameTypes)(FRAME_TYPE_ENERGY_REACT_SUM_RATE1 + i));
  SetParamTypeForCell(g_rows.nR_plus, g_columns.nSum, FRAME_TYPE_ENERGY_REACT_SUM);
  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    SetParamTypeForCell(g_rows.nR_minus, g_columns.tariffs[i], (teFrameTypes)(FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE1 + i));
  SetParamTypeForCell(g_rows.nR_minus, g_columns.nSum, FRAME_TYPE_ENERGY_REACT_NEG_SUM);

  m_plstCurrentData->SetSpecColorColumn(g_columns.nEnergy);
  m_plstCurrentDataEx->UpdateView();
  m_plstCurrentData->CtrlWasCreated();
}

void CView_Energy::StopCurrentDataMonitoring()
{
  BeginFormingReadPack(true);
  ClearAllReadingReq();
  BeginFormingReadPack(false);
}

void CView_Energy::StartCurrentDataMonitoring()
{
  BeginFormingReadPack(true);

  int nStart = static_cast<int>(FRAME_TYPE_ENERGY_ACT_SUM);
  int nEnd = static_cast<int>(FRAME_TYPE_ENERGY_REACT_NEG_SUM_RATE8);
  for (int i = nStart; i <= nEnd; ++i)
    SetParamTypeForRead(static_cast<teFrameTypes>(i));

  BeginFormingReadPack(false);
}

void CView_Energy::CheckMeaningsViewByConnectionStatus_InCurrentTbl()
{
  vector<int> inactiveTextInColumns;
  if (!IsConnection())
  {
    for (int i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
      inactiveTextInColumns.push_back(g_columns.tariffs[i]);
    inactiveTextInColumns.push_back(g_columns.nSum);
  }
  m_plstCurrentData->SetInactiveTextInColumns(&inactiveTextInColumns);
  m_plstCurrentDataEx->UpdateView();
}

void CView_Energy::UpdMeaningsByRecvdDeviceData_InCurrentTbl(teFrameTypes& ft)
{
  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    UpdateParamMeaningByReceivedDeviceData(g_rows.nA_plus, g_columns.tariffs[i]);
  UpdateParamMeaningByReceivedDeviceData(g_rows.nA_plus, g_columns.nSum);
  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    UpdateParamMeaningByReceivedDeviceData(g_rows.nR_plus, g_columns.tariffs[i]);
  UpdateParamMeaningByReceivedDeviceData(g_rows.nR_plus, g_columns.nSum);
  for (size_t i = 0; i < COUNT_OF_ENERGY_TARIFFS; ++i)
    UpdateParamMeaningByReceivedDeviceData(g_rows.nR_minus, g_columns.tariffs[i]);
  UpdateParamMeaningByReceivedDeviceData(g_rows.nR_minus, g_columns.nSum);

  m_plstCurrentDataEx->UpdateView();
}