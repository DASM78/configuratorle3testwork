#pragma once

#include "afxwin.h"
#include "Ai_TabCtrl.h"
#include "Ai_ColorStatic.h" 
#include "CommonViewForm.h"
#include "CommonViewDealer.h"
#include "XmlAid.h"
#include "Ai_EditCtrlCharFilter.h"
#include "Connection_Task_InterfaceChoice_RS.h"
#include "Connection_Task_InterfaceChoice_USB.h"

class CAi_VertTabCtrl_My: public CAi_VertTabCtrl
{
public:
  CAi_VertTabCtrl_My()
  {
  }
  ~CAi_VertTabCtrl_My()
  {
  }

private:
  virtual void OnSelchanged(int nSelectedItem, CString sPageName)
  {
  }
};

class CView_Connection: public CCommonViewForm, public CCommonViewDealer
{
  DECLARE_DYNCREATE(CView_Connection)

public:
  CView_Connection();
  virtual ~CView_Connection();

  DECLARE_MESSAGE_MAP()

  CDocument* GetDocument();
private:
  enum
  {
    IDD = IDD_CONNECTION_SETTINGS
  };

  CParamsTreeNode* m_pConnectionNode = nullptr;
  
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

#ifdef _DEBUG
  virtual void AssertValid() const override final;
  virtual void Dump(CDumpContext& dc) const override final;
#endif

  /*
  virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
  virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
  virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
  afx_msg void OnFilePrintPreview();
  */

  virtual void OnInitialUpdate();
  virtual void BeforeDestroyClass() override final;
  virtual void OnSize_();

  virtual void OnDraw(CDC* pDC);  // overridden to draw this view

  bool m_bCanSave = false;

  // Device choice

  CAi_ColorStatic m_stcDevicesCap;
  CComboBox m_cbxDevices;

  void FormTask_DeviceChoice();
  void FillTask_DeviceChoiceFromXml(CParamsTreeNode* pDevTypeNode);
  void SaveDeviceChoiceValueToXml();
  afx_msg void OnCbxChangeEdtDeviceAddress();

  // Access level

public:
  void PasswordWasChanged();

private:
  CAi_ColorStatic m_stcAccessLevelCap;
  CComboBox m_cbxLogin;
  CAi_EditCtrlCharFilter m_edtPassword;
  CButton m_chkShowPassword;

  void FormTask_AccessChoice();
  void FillTask_AccessChoiceFromRegedit();
  void SaveAccessChoiceValueToRegedit();
  afx_msg void OnCbnSelchangeCbxLogin();
  afx_msg void OnBnClickedChkShowPassword();

  // Interface choice

  CAi_ColorStatic m_stcInterfacesCap;

  void FormTask_InterfaceChoice();
  CAi_VertTabCtrl_My m_tabInterfaces;
  CConnection_Task_InterfaceChoice_RS m_ConnTask_Prot_RS;
  CConnection_Task_InterfaceChoice_USB m_ConnTask_Prot_USB;
  void UpdateInterfaceChoice();
  void UpdateInterfaceChoiceInStatusBar();

  //

  virtual void OnConnectionStateWasChanged() override final;
};

#ifndef _DEBUG  // debug version in ElectricMeterView.cpp
/*inline CElectricMeterDoc* CView_Connection::GetDocument()
{
  return (CElectricMeterDoc*)m_pDocument;
}*/
#endif
